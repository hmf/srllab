package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Config() extends Layouts {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val sendData: PBool = PBool(
    dflt = Some(true),
    description = Some(
      """If *showLink* is true, does it contain data just link to a plot.ly file?"""
    )
  )

  /* §#
   * generateAny(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val setBackground: PAny = PAny(
    dflt = Some("""transparent"""),
    description = Some(
      """Set function to add the background color (i.e. `layout.paper_color`) to a different container. This function take the graph div as first argument and the current background color as second argument. Alternatively, set to string *opaque* to ensure there is white behind it."""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val displaylogo: PBool = PBool(
    dflt = Some(true),
    description = Some(
      """Determines whether or not the plotly logo is displayed on the end of the mode bar."""
    )
  )

  object displayModeBar extends JSON {
    /* §#
     * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dflt: String = """hover"""
    val description: Option[String] = Some(
      """Determines the mode bar display mode. If *true*, the mode bar is always visible. If *false*, the mode bar is always hidden. If *hover*, the mode bar is visible while the mouse cursor is on the graph container."""
    )

    private var selected: OfPrimitive = NAPrimitive
    //allDeclarations

    def json(): String = {
      /* §#
       * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      selected match {
        case NAPrimitive => ""
        case TString(t)  => "\"" + t.toString + "\""
        case TBoolean(t) => t.toString
        case TDouble(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TDouble(0.0)"""
          )
        case TInt(t) =>
          throw new RuntimeException("""Unexpected OfPrimitive type: TInt(0)""")
        case TImpliedEdits(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TImpliedEdits()"""
          )
        case TAttrRegexps(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TAttrRegexps()"""
          )
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err(j: ujson.Value): OfPrimitive =
        throw new RuntimeException(s"""Parsing of $j failed: $in""")
      val read = in match {
        case Str(s)  => TString(s)
        case Bool(s) => TBoolean(s)
        case Num(s)  => err(in)
        case _       => err(in)
      }
      selected = read
      this
    }

    def hover(): Unit = { selected = TString("hover") }

    def true_(): Unit = { selected = TBoolean(true) }

    def false_(): Unit = { selected = TBoolean(false) }

  }

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val fillFrame: PBool = PBool(
    dflt = Some(false),
    description = Some(
      """When `layout.autosize` is turned on, determines whether the graph fills the container (the default) or the screen (if set to *true*)."""
    )
  )

  /* §#
   * generateIntDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val logging: PInt = PInt(
    min = Some(0),
    max = Some(2),
    dflt = Some(1),
    description = Some(
      """Turn all console logging on or off (errors will be thrown) This should ONLY be set via Plotly.setPlotConfig Available levels: 0: no logs 1: warnings and errors, but not informational messages 2: verbose logs"""
    )
  )

  /* §#
   * generateStringDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val topojsonURL: PChars = PChars(
    dflt = Some("""https://cdn.plot.ly/"""),
    description = Some(
      """Set the URL to topojson used in geo charts. By default, the topojson files are fetched from cdn.plot.ly. For example, set this option to: <path-to-plotly.js>/dist/topojson/ to render geographical feature using the topojson files that ship with the plotly.js module."""
    )
  )

  /* §#
   * generateStringDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val linkText: PChars = PChars(
    dflt = Some("""Edit chart"""),
    description = Some("""Sets the text appearing in the `showLink` link.""")
  )

  /* §#
   * generateAny(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val modeBarButtonsToAdd: PAny = PAny(
    dflt = Some("""[Ljava.lang.Object;@79da8dc5"""),
    description = Some(
      """Add mode bar button using config objects See ./components/modebar/buttons.js for list of arguments."""
    )
  )

  /* §#
   * generateNumberDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val frameMargins: PReal = PReal(
    min = Some(0.0),
    max = Some(0.5),
    dflt = Some("""0.0"""),
    description = Some(
      """When `layout.autosize` is turned on, set the frame margins in fraction of the graph size."""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val showLink: PBool = PBool(
    dflt = Some(false),
    description = Some(
      """Determines whether a link to plot.ly is displayed at the bottom right corner of resulting graphs. Use with `sendData` and `linkText`."""
    )
  )

  /* §#
   * generateNumberDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val plotGlPixelRatio: PReal = PReal(
    min = Some(1.0),
    max = Some(4.0),
    dflt = Some("""2.0"""),
    description = Some(
      """Set the pixel ratio during WebGL image export. This config option was formerly named `plot3dPixelRatio` which is now deprecated."""
    )
  )

  /* §#
   * generateAny(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val globalTransforms: PAny = PAny(
    dflt = Some("""[Ljava.lang.Object;@696da30b"""),
    description = Some(
      """Set global transform to be applied to all traces with no specification needed"""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val showSendToCloud: PBool = PBool(
    dflt = Some(false),
    description = Some(
      """Should we include a ModeBar button, labeled "Edit in Chart Studio", that sends this chart to plot.ly or another plotly server as specified by `plotlyServerURL` for editing, export, etc? Prior to version 1.43.0 this button was included by default, now it is opt-in using this flag. Note that this button can (depending on `plotlyServerURL`) send your data to an external server. However that server does not persist your data until you arrive at the Chart Studio and explicitly click "Save"."""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val autosizable: PBool = PBool(
    dflt = Some(false),
    description = Some(
      """Determines whether the graphs are plotted with respect to layout.autosize:true and infer its container size."""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val showAxisDragHandles: PBool = PBool(
    dflt = Some(true),
    description =
      Some("""Set to *false* to omit cartesian axis pan/zoom drag handles.""")
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val showTips: PBool = PBool(
    dflt = Some(true),
    description = Some(
      """Determines whether or not tips are shown while interacting with the resulting graphs."""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val showAxisRangeEntryBoxes: PBool = PBool(
    dflt = Some(true),
    description = Some(
      """Set to *false* to omit direct range entry at the pan/zoom drag points, note that `showAxisDragHandles` must be enabled to have an effect."""
    )
  )

  /* §#
   * generateStringDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val locale: PChars = PChars(
    dflt = Some("""en-US"""),
    description = Some(
      """Which localization should we use? Should be a string like 'en' or 'en-US'."""
    )
  )

  object scrollZoom extends JSON {
    /* §#
     * generateFlagListDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dflt: Option[String] = Some("""gl3d+geo+mapbox""")
    val description: Option[String] = Some(
      """Determines whether mouse wheel or two-finger scroll zooms is enable. Turned on by default for gl3d, geo and mapbox subplots (as these subplot types do not have zoombox via pan), but turned off by default for cartesian subplots. Set `scrollZoom` to *false* to disable scrolling for all subplots."""
    )

    private val flag_ : mutable.Set[String] = mutable.Set()
    //dflt.map(e => flag_ + e)
    def flag: String = flag_.mkString("+")
    def json(): String = {
      if (flag.contains("true"))
        "true"
      else if (flag.contains("false"))
        "false"
      else if (flag.length > 0)
        "\"" + flag + "\""
      else
        ""
    }

    override def fromJson(in: Value): this.type = {
      /* §#
       * generateFlagListFromJson()
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err(j: ujson.Value): OfPrimitive =
        throw new RuntimeException(s"""Parsing of $j failed: $in""")
      in match {
        case Str(value) =>
          flag_ += value
          this
        case Bool(value) =>
          flag_ += value.toString
          this
        case _ =>
          err(in)
          this
      }
    }

    def mapboxOn(): Unit     = { flag_ + "mapbox" }
    def mapboxOff(): Unit    = { flag_ - "mapbox" }
    def geoOn(): Unit        = { flag_ + "geo" }
    def geoOff(): Unit       = { flag_ - "geo" }
    def gl3dOn(): Unit       = { flag_ + "gl3d" }
    def gl3dOff(): Unit      = { flag_ - "gl3d" }
    def cartesianOn(): Unit  = { flag_ + "cartesian" }
    def cartesianOff(): Unit = { flag_ - "cartesian" }
    def false_On(): Unit     = { flag_ + "false" }
    def false_Off(): Unit    = { flag_ - "false" }
    def true_On(): Unit      = { flag_ + "true" }
    def true_Off(): Unit     = { flag_ - "true" }
  }

  /* §#
   * generateAny(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val modeBarButtonsToRemove: PAny = PAny(
    dflt = Some("""[Ljava.lang.Object;@120f102b"""),
    description = Some(
      """Remove mode bar buttons by name. See ./components/modebar/buttons.js for the list of names."""
    )
  )

  /* §#
   * generateAny(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val showSources: PAny = PAny(
    dflt = Some("""false"""),
    description = Some(
      """Adds a source-displaying function to show sources on the resulting graphs."""
    )
  )

  /* §#
   * generateStringDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val mapboxAccessToken: PChars = PChars(
    dflt = None,
    description = Some(
      """Mapbox access token (required to plot mapbox trace types) If using an Mapbox Atlas server, set this option to '' so that plotly.js won't attempt to authenticate to the public Mapbox server."""
    )
  )

  /* §#
   * generateAny(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val modeBarButtons: PAny = PAny(
    dflt = Some("""false"""),
    description = Some(
      """Define fully custom mode bar buttons as nested array, where the outer arrays represents button groups, and the inner arrays have buttons config objects or names of default buttons See ./components/modebar/buttons.js for more info."""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val staticPlot: PBool = PBool(
    dflt = Some(false),
    description = Some(
      """Determines whether the graphs are interactive or not. If *false*, no interactivity, for export or image generation."""
    )
  )

  /* §#
   * generateIntDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val queueLength: PInt = PInt(
    min = Some(0),
    max = None,
    dflt = Some(0),
    description = Some("""Sets the length of the undo/redo queue.""")
  )

  /* §#
   * generateAny(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val locales: PAny = PAny(
    dflt = None,
    description = Some(
      """Localization definitions Locales can be provided either here (specific to one chart) or globally by registering them as modules. Should be an object of objects {locale: {dictionary: {...}, format: {...}}} {   da: {       dictionary: {'Reset axes': 'Nulstil aksler', ...},       format: {months: [...], shortMonths: [...]}   },   ... } All parts are optional. When looking for translation or format fields, we look first for an exact match in a config locale, then in a registered module. If those fail, we strip off any regionalization ('en-US' -> 'en') and try each (config, registry) again. The final fallback for translation is untranslated (which is US English) and for formats is the base English (the only consequence being the last fallback date format %x is DD/MM/YYYY instead of MM/DD/YYYY). Currently `grouping` and `currency` are ignored for our automatic number formatting, but can be used in custom formats."""
    )
  )

  /* §#
   * generateAny(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val toImageButtonOptions: PAny = PAny(
    dflt = None,
    description = Some(
      """Statically override options for toImage modebar button allowed keys are format, filename, width, height, scale see ../components/modebar/buttons.js"""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val watermark: PBool = PBool(
    dflt = Some(false),
    description = Some("""watermark the images with the company's logo""")
  )

  /* §#
   * generateStringDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val plotlyServerURL: PChars = PChars(
    dflt = Some("""https://plot.ly"""),
    description = Some(
      """Sets base URL for the 'Edit in Chart Studio' (aka sendDataToCloud) mode bar button and the showLink/sendData on-graph link"""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val responsive: PBool = PBool(
    dflt = Some(false),
    description = Some(
      """Determines whether to change the layout size when window is resized. In v2, this option will be removed and will always be true."""
    )
  )

  object doubleClick extends JSON {
    /* §#
     * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dflt: String = """reset+autosize"""
    val description: Option[String] = Some(
      """Sets the double click interaction mode. Has an effect only in cartesian plots. If *false*, double click is disable. If *reset*, double click resets the axis ranges to their initial values. If *autosize*, double click set the axis ranges to their autorange values. If *reset+autosize*, the odd double clicks resets the axis ranges to their initial values and even double clicks set the axis ranges to their autorange values."""
    )

    private var selected: OfPrimitive = NAPrimitive
    //allDeclarations

    def json(): String = {
      /* §#
       * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      selected match {
        case NAPrimitive => ""
        case TString(t)  => "\"" + t.toString + "\""
        case TBoolean(t) => t.toString
        case TDouble(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TDouble(0.0)"""
          )
        case TInt(t) =>
          throw new RuntimeException("""Unexpected OfPrimitive type: TInt(0)""")
        case TImpliedEdits(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TImpliedEdits()"""
          )
        case TAttrRegexps(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TAttrRegexps()"""
          )
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err(j: ujson.Value): OfPrimitive =
        throw new RuntimeException(s"""Parsing of $j failed: $in""")
      val read = in match {
        case Str(s)  => TString(s)
        case Bool(s) => TBoolean(s)
        case Num(s)  => err(in)
        case _       => err(in)
      }
      selected = read
      this
    }

    def false_(): Unit = { selected = TBoolean(false) }

    def reset(): Unit = { selected = TString("reset") }

    def autosize(): Unit = { selected = TString("autosize") }

    def `reset+autosize`(): Unit = { selected = TString("reset+autosize") }

  }

  /* §#
   * generateIntDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val notifyOnLogging: PInt = PInt(
    min = Some(0),
    max = Some(2),
    dflt = Some(0),
    description = Some(
      """Set on-graph logging (notifier) level This should ONLY be set via Plotly.setPlotConfig Available levels: 0: no on-graph logs 1: warnings and errors, but not informational messages 2: verbose logs"""
    )
  )

  /* §#
   * generateNumberDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val doubleClickDelay: PReal = PReal(
    min = Some(0.0),
    max = None,
    dflt = Some("""300.0"""),
    description = Some(
      """Sets the delay for registering a double-click in ms. This is the time interval (in ms) between first mousedown and 2nd mouseup to constitute a double-click. This setting propagates to all on-subplot double clicks (except for geo and mapbox) and on-legend double clicks."""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val editable: PBool = PBool(
    dflt = Some(false),
    description = Some(
      """Determines whether the graph is editable or not. Sets all pieces of `edits` unless a separate `edits` config item overrides individual parts."""
    )
  )

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val showEditInChartStudio: PBool = PBool(
    dflt = Some(false),
    description = Some(
      """Same as `showSendToCloud`, but use a pencil icon instead of a floppy-disk. Note that if both `showSendToCloud` and `showEditInChartStudio` are turned, only `showEditInChartStudio` will be honored."""
    )
  )

  case object edits extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val role: String = """object"""

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val legendPosition: PBool = PBool(
      dflt = Some(false),
      description = Some("""Enables moving the legend.""")
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val titleText: PBool = PBool(
      dflt = Some(false),
      description = Some("""Enables editing the global layout title.""")
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val annotationText: PBool = PBool(
      dflt = Some(false),
      description = Some("""Enables editing annotation text.""")
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val colorbarTitleText: PBool = PBool(
      dflt = Some(false),
      description = Some("""Enables editing colorbar title text.""")
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val annotationPosition: PBool = PBool(
      dflt = Some(false),
      description = Some(
        """Determines if the main anchor of the annotation is editable. The main anchor corresponds to the text (if no arrow) or the arrow (which drags the whole thing leaving the arrow length & direction unchanged)."""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val annotationTail: PBool = PBool(
      dflt = Some(false),
      description = Some(
        """Has only an effect for annotations with arrows. Enables changing the length and direction of the arrow."""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val shapePosition: PBool = PBool(
      dflt = Some(false),
      description = Some("""Enables moving shapes.""")
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val axisTitleText: PBool = PBool(
      dflt = Some(false),
      description = Some("""Enables editing axis title text.""")
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val legendText: PBool = PBool(
      dflt = Some(false),
      description =
        Some("""Enables editing the trace name fields from the legend""")
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val colorbarPosition: PBool = PBool(
      dflt = Some(false),
      description = Some("""Enables moving colorbars.""")
    )

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (legendPosition.json().length > 0)
          "\"legendPosition\"" + ":" + legendPosition.json()
        else ""
      val v1 =
        if (titleText.json().length > 0)
          "\"titleText\"" + ":" + titleText.json()
        else ""
      val v2 =
        if (annotationText.json().length > 0)
          "\"annotationText\"" + ":" + annotationText.json()
        else ""
      val v3 =
        if (colorbarTitleText.json().length > 0)
          "\"colorbarTitleText\"" + ":" + colorbarTitleText.json()
        else ""
      val v4 =
        if (annotationPosition.json().length > 0)
          "\"annotationPosition\"" + ":" + annotationPosition.json()
        else ""
      val v5 =
        if (annotationTail.json().length > 0)
          "\"annotationTail\"" + ":" + annotationTail.json()
        else ""
      val v6 =
        if (shapePosition.json().length > 0)
          "\"shapePosition\"" + ":" + shapePosition.json()
        else ""
      val v7 =
        if (axisTitleText.json().length > 0)
          "\"axisTitleText\"" + ":" + axisTitleText.json()
        else ""
      val v8 =
        if (legendText.json().length > 0)
          "\"legendText\"" + ":" + legendText.json()
        else ""
      val v9 =
        if (colorbarPosition.json().length > 0)
          "\"colorbarPosition\"" + ":" + colorbarPosition.json()
        else ""
      val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6, v7, v8, v9)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("legendPosition")
        in0.map(ve => legendPosition.fromJson(ve))

        val in1 = mapKV.get("titleText")
        in1.map(ve => titleText.fromJson(ve))

        val in2 = mapKV.get("annotationText")
        in2.map(ve => annotationText.fromJson(ve))

        val in3 = mapKV.get("colorbarTitleText")
        in3.map(ve => colorbarTitleText.fromJson(ve))

        val in4 = mapKV.get("annotationPosition")
        in4.map(ve => annotationPosition.fromJson(ve))

        val in5 = mapKV.get("annotationTail")
        in5.map(ve => annotationTail.fromJson(ve))

        val in6 = mapKV.get("shapePosition")
        in6.map(ve => shapePosition.fromJson(ve))

        val in7 = mapKV.get("axisTitleText")
        in7.map(ve => axisTitleText.fromJson(ve))

        val in8 = mapKV.get("legendText")
        in8.map(ve => legendText.fromJson(ve))

        val in9 = mapKV.get("colorbarPosition")
        in9.map(ve => colorbarPosition.fromJson(ve))

      }
      this
    }

  }

  object layoutAttributes extends JSON {
    def json(): String                 = ""
    def fromJson(in: Value): this.type = this
  }

}
