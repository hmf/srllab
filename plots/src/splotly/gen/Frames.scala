package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Frames() extends Layouts {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val role: String = """object"""

  case object items extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/

    case object frames_entry extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val name: PChars = PChars(
        dflt = None,
        description = Some("""A label by which to identify the frame""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val baseframe: PChars = PChars(
        dflt = None,
        description = Some(
          """The name of the frame into which this frame's properties are merged before applying. This is used to unify properties and avoid needing to specify the same values for the same properties in multiple frames."""
        )
      )

      /* §#
       * generateAny(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val data: PAny = PAny(
        dflt = None,
        description = Some(
          """A list of traces this frame modifies. The format is identical to the normal trace definition."""
        )
      )

      /* §#
       * generateAny(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val traces: PAny = PAny(
        dflt = None,
        description = Some(
          """A list of trace indices that identify the respective traces in the data attribute"""
        )
      )

      /* §#
       * generateAny(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val layout: PAny = PAny(
        dflt = None,
        description = Some(
          """Layout properties which this frame modifies. The format is identical to the normal layout definition."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val group: PChars = PChars(
        dflt = None,
        description = Some(
          """An identifier that specifies the group to which the frame belongs, used by animate to select a subset of frames."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (name.json().length > 0) "\"name\"" + ":" + name.json() else ""
        val v1 =
          if (baseframe.json().length > 0)
            "\"baseframe\"" + ":" + baseframe.json()
          else ""
        val v2 =
          if (data.json().length > 0) "\"data\"" + ":" + data.json() else ""
        val v3 =
          if (traces.json().length > 0) "\"traces\"" + ":" + traces.json()
          else ""
        val v4 =
          if (layout.json().length > 0) "\"layout\"" + ":" + layout.json()
          else ""
        val v5 =
          if (group.json().length > 0) "\"group\"" + ":" + group.json() else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("name")
          in0.map(ve => name.fromJson(ve))

          val in1 = mapKV.get("baseframe")
          in1.map(ve => baseframe.fromJson(ve))

          val in2 = mapKV.get("data")
          in2.map(ve => data.fromJson(ve))

          val in3 = mapKV.get("traces")
          in3.map(ve => traces.fromJson(ve))

          val in4 = mapKV.get("layout")
          in4.map(ve => layout.fromJson(ve))

          val in5 = mapKV.get("group")
          in5.map(ve => group.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (frames_entry.json().length > 0)
          "\"frames_entry\"" + ":" + frames_entry.json()
        else ""
      val vars: List[String] = List(v0)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("frames_entry")
        in0.map(ve => frames_entry.fromJson(ve))

      }
      this
    }

  }

  object layoutAttributes extends JSON {
    def json(): String                 = ""
    def fromJson(in: Value): this.type = this
  }

}
