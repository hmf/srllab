package splotly

import ujson._

/**
 *
 * mill mill.scalalib.GenIdea/idea
 *
 * mill mdocs.compile
 * mill mdocs.run
 * mill mdocs.run noGraal
 *
 * mill mdocs.{compile, run}
 * mill --watch mdocs.run
 *
 * mill -i mdocs.console
 * mill -i mdocs.repl
 *
 * mill -i plots.runMain plotly.Examples
 * mill --watch mill -i plots.runMain plotly.Examples
 */
object Examples {

  // JSON

  def ex1(): Unit = {
    val dict = Obj(
      "tag_name" -> "tag_name_values",
      "name" -> "name_value",
      "body" -> s"body_value"
    )

    assert( dict.toString() == """{"tag_name":"tag_name_values","name":"name_value","body":"body_value"}""" )
  }

  def ex2(): Unit = {
    val nums = Arr(1, 2, 3)
    assert(nums.toString() == "[1,2,3]")
    assert(nums(1).toString() == "2")

    val nested = Arr(
      Obj("myFieldA" -> 1, "myFieldB" -> "g"),
      Obj("myFieldA" -> 2, "myFieldB" -> "k")
    )
    assert( nested.toString() == """[{"myFieldA":1,"myFieldB":"g"},{"myFieldA":2,"myFieldB":"k"}]""")

    nested(0)("myFieldA") = 123
    assert(nested.toString == """[{"myFieldA":123,"myFieldB":"g"},{"myFieldA":2,"myFieldB":"k"}]""")
  }

  def ex3(): Unit = {
    val data = ujson.read("""[{"myFieldA":123,"myFieldB":"g"},{"myFieldA":2,"myFieldB":"k"}]""")
    assert(data.toString() == """[{"myFieldA":123,"myFieldB":"g"},{"myFieldA":2,"myFieldB":"k"}]""")
    data(0)("myFieldA") = 1

    assert(data.toString() == """[{"myFieldA":1,"myFieldB":"g"},{"myFieldA":2,"myFieldB":"k"}]""")
    assert(ujson.write(data,1) ==
      """[
        | {
        |  "myFieldA": 1,
        |  "myFieldB": "g"
        | },
        | {
        |  "myFieldA": 2,
        |  "myFieldB": "k"
        | }
        |]""".stripMargin)
  }

  def ex4(): Unit = {
    import ujson.circe.CirceJson

    val circeJson: io.circe.Json = CirceJson(
      """["hello", "world"]"""
    )

    val updatedCirceJson =
      circeJson.mapArray(_.map(x => x.mapString(_.toUpperCase)))

    import ujson.play.PlayJson
    import _root_.play.api.libs.json._

    val playJson: JsValue = CirceJson.transform(
      updatedCirceJson,
      PlayJson
    )

    val updatedPlayJson = JsArray(
      for(v <- playJson.as[JsArray].value)
        yield JsString(v.as[String].reverse)
    )

    val stringified = PlayJson.transform(updatedPlayJson, StringRenderer()).toString

    val expected = """["OLLEH","DLROW"]"""
    assert( expected == stringified)
  }

  // Pickle
  def ex5():Unit = {
    import upickle.default._

    assert( write(1) == "1" )

    assert( write(Seq(1, 2, 3)) == "[1,2,3]" )

    assert( read[Seq[Int]]("[1,2,3]") == List(1, 2, 3) )

    assert( write((1, "omg", true)) == """[1,"omg",true]""" )

    assert( read[(Int, String, Boolean)]("""[1,"omg",true]""") == (1, "omg", true) )
  }

  def ex6(): Unit = {
    import upickle.default._

    assert( writeBinary(1) sameElements Array(1) )

    assert( writeBinary(Seq(1, 2, 3)) sameElements Array(0x93.toByte, 1, 2, 3) )

    assert( readBinary[Seq[Int]](Array[Byte](0x93.toByte, 1, 2, 3)) == List(1, 2, 3) )

    val serializedTuple = Array[Byte](0x93.toByte, 1, 0xa3.toByte, 111, 109, 103, 0xc3.toByte)

    assert( writeBinary((1, "omg", true)) sameElements serializedTuple )

    assert( readBinary[(Int, String, Boolean)](serializedTuple) == (1, "omg", true) )
  }

  // Basic
  def basics(): Unit = {
    import upickle.default._

    assert( write(true: Boolean) == "true" )
    assert( write(false: Boolean) == "false" )

    assert( write(12: Int) == "12" )
    assert( write(12: Short) == "12" )
    assert( write(12: Byte) == "12" )
    assert( write(Int.MaxValue) == "2147483647" )
    assert( write(Int.MinValue) == "-2147483648" )
    assert( write(12.5f: Float) == "12.5" )
    assert( write(12.5: Double) == "12.5" )

    // Long is too large for JavaScript, so its converted to a string
    assert( write(12: Long) == "12" )
    assert( write(4000000000000L: Long) == "4000000000000" )
    // large longs are written as strings, to avoid floating point rounding
    assert( write(9223372036854775807L: Long) == "\"9223372036854775807\"" )

    assert( write(1.0/0: Double) == "\"Infinity\"" )
    assert( write(Float.PositiveInfinity) == "\"Infinity\"" )
    assert( write(Float.NegativeInfinity) == "\"-Infinity\"" )

    assert( write('o') == "\"o\"" ) //
    assert( write("omg") == "\"omg\"" )

    assert( write(Array(1, 2, 3)) == "[1,2,3]" )

    // You can pass in an `indent` parameter to format it nicely
    assert( write(Array(1, 2, 3), indent = 4)  ==
      """[
        |    1,
        |    2,
        |    3
        |]""".stripMargin)

    assert( write(Seq(1, 2, 3)) == "[1,2,3]" )
    assert( write(Vector(1, 2, 3)) == "[1,2,3]" )
    assert( write(List(1, 2, 3)) == "[1,2,3]" )

    import collection.immutable.SortedSet
    assert( write(SortedSet(1, 2, 3)) == "[1,2,3]" )

    assert( write((1, "omg"): (Int, String) ) == """[1,"omg"]""" )
    assert( write((1, "omg", true)) == """[1,"omg",true]""" )
  }

  // Need to declare these at the module level otherwise `macroRW` will fail
  // See scalac bug (SI-7567)
  import upickle.default.{ReadWriter => RW, macroRW}

  case class Thing(myFieldA: Int, myFieldB: String)
  object Thing{
    implicit val rw: RW[Thing] = macroRW
  }
  case class Big(i: Int, b: Boolean, str: String, c: Char, t: Thing)
  object Big{
    implicit val rw: RW[Big] = macroRW
  }

  sealed trait IntOrTuple
  object IntOrTuple{
    implicit val rw: RW[IntOrTuple] = RW.merge(IntThing.rw, TupleThing.rw)
  }
  case class IntThing(i: Int) extends IntOrTuple
  object IntThing{
    implicit val rw: RW[IntThing] = macroRW
  }
  case class TupleThing(name: String, t: (Int, Int)) extends IntOrTuple
  object TupleThing{
    implicit val rw: RW[TupleThing] = macroRW
  }


  def caseClasses(): Unit = {
    import upickle.default._

    // begin serializing the case class
    // see declarations at the module level

    // macros
    import upickle._

    assert( write(Thing(1, "gg")) == """{"myFieldA":1,"myFieldB":"gg"}""" )

    assert( read[Thing]("""{"myFieldA":1,"myFieldB":"gg"}""") == Thing(1, "gg") )

    assert(
      write(Big(1, true, "lol", 'Z', Thing(7, "")))
      ==
      """{"i":1,"b":true,"str":"lol","c":"Z","t":{"myFieldA":7,"myFieldB":""}}"""
    )

    assert(
      write(Big(1, true, "lol", 'Z', Thing(7, "")), indent = 4) ==
      """{
        |    "i": 1,
        |    "b": true,
        |    "str": "lol",
        |    "c": "Z",
        |    "t": {
        |        "myFieldA": 7,
        |        "myFieldB": ""
        |    }
        |}""".stripMargin
    )

    // Sealed hierarchies are tagged with the full path name

    assert( write(IntThing(1)) == """{"$type":"plotly.Examples.IntThing","i":1}""" )

    assert(
      write(TupleThing("naeem", (1, 2)))
        ==
      """{"$type":"plotly.Examples.TupleThing","name":"naeem","t":[1,2]}"""
    )

    // You can read tagged value without knowing its
    // type in advance, just use type of the sealed trait
    assert(
      read[IntOrTuple]("""{"$type":"plotly.Examples.IntThing","i":1}""")
        ==
      IntThing(1)
    )
  }

  def loadSources(): Unit = {
    import upickle.default._

    val original = """{"myFieldA":1,"myFieldB":"gg"}"""

    // from strings, chars and bytes
    assert( read[Thing](original) == Thing(1, "gg") )
    assert( read[Thing](original: CharSequence) == Thing(1, "gg") )
    assert( read[Thing](original.getBytes) == Thing(1, "gg") )

    // from files
    import java.nio.file.Files
    val f = Files.createTempFile("", "")
    Files.write(f, original.getBytes)

    assert( read[Thing](f) == Thing(1, "gg") )
    assert( read[Thing](f.toFile) == Thing(1, "gg") )
    assert( read[Thing](Files.newByteChannel(f)) == Thing(1, "gg") )
  }


  case class FooDefault(i: Int = 10, s: String = "lol", o: Option[Float] = None)
  object FooDefault{
    implicit val rw: RW[FooDefault] = macroRW
  }

  def defaults(): Unit = {
    import upickle.default._

    // macros
    import upickle._

    // If a field is missing upon deserialization, uPickle uses the default value if one exists
    assert( read[FooDefault]("{}") == FooDefault(10, "lol", None) )
    assert( read[FooDefault]("""{"i": 123}""") == FooDefault(123,"lol", None) )

    assert( write(FooDefault(i = 11, s = "lol"))  == """{"i":11}""" )
    assert( write(FooDefault(i = 10, s = "lol"))  == """{}""" )
    assert( write(FooDefault())                   == """{}""" )
  }

  def customPickle(): Unit = {
    import upickle.default._

    case class Wrap(i: Int)
    implicit val fooReadWrite: ReadWriter[Wrap] =
      readwriter[Int].bimap[Wrap](_.i, Wrap)

    assert( write(Seq(Wrap(1), Wrap(10), Wrap(100))) == "[1,10,100]" )
    assert( read[Seq[Wrap]]("[1,10,100]") == Seq(Wrap(1), Wrap(10), Wrap(100)) )

    // Using to/from String
    class CustomThing2(val i: Int, val s: String){
      override def equals(obj: Any): Boolean = {
        if (!obj.isInstanceOf[CustomThing2])
          false
        else {
          val tmp = obj.asInstanceOf[CustomThing2]
          (tmp.i == this.i) && (tmp.s == this.s)
        }
      }
    }
    object CustomThing2 {
      implicit val rw = upickle.default.readwriter[String].bimap[CustomThing2](
        x => x.i.toString + " " + x.s,
        str => {
          val Array(i, s) = str.split(" ", 2)
          new CustomThing2(i.toInt, s)
        }
      )
    }

    val things = Seq(
      new CustomThing2(1, "one"),
      new CustomThing2(10, "ten"),
      new CustomThing2(100, "hundred")
    )
    assert( write( things ) == """["1 one","10 ten","100 hundred"]""" )
    assert( read[Seq[CustomThing2]]("""["1 one","10 ten","100 hundred"]""") equals things )

    // Using the JSON AST
    class Bar(val i: Int, val s: String){
      override def equals(obj: Any): Boolean = {
        if (!obj.isInstanceOf[Bar])
          false
        else {
          val tmp = obj.asInstanceOf[Bar]
          (tmp.i == this.i) && (tmp.s == this.s)
        }
      }
    }

    implicit val barReadWrite: ReadWriter[Bar] =
      readwriter[ujson.Value].bimap[Bar](
        x => ujson.Arr(x.s, x.i),
        json => new Bar(json(1).num.toInt, json(0).str)
      )

    assert( write(new Bar(123, "abc")) == """["abc",123]""" )
    assert( read[Bar]("""["abc",123]""") == new Bar(123, "abc") )
  }

  // scalac bug (SI-7567)
  case class KeyBar(@upickle.implicits.key("hehehe") kekeke: Int)
  object KeyBar{
    implicit val rw: RW[KeyBar] = macroRW
  }


  // scalac bug (SI-7567)
  sealed trait A
  object A{
    implicit val rw: RW[A] = RW.merge(B.rw, macroRW[C.type])
  }
  @upickle.implicits.key("Bee") case class B(i: Int) extends A
  object B{
    implicit val rw: RW[B] = macroRW
  }
  case object C extends A


  /**
   * Allows backward compatibility with previous JSON versions
   */
  def customKeys(): Unit = {
    import upickle.default._

    // macros
    import upickle._

    assert( write(KeyBar(10)) == """{"hehehe":10}""" )
    assert( read[KeyBar]("""{"hehehe": 10}""") == KeyBar(10) )

    // Trait hierarchies have class name in JSON
    assert( write(B(10)) == """{"$type":"Bee","i":10}""" )
    assert( read[B]("""{"$type":"Bee","i":10}""") == B(10) )
  }

  def customConfig(): Unit = {
    object SnakePickle extends upickle.AttributeTagged{
      def camelToSnake(s: String): String = {
        // ?= is a positive lookahead, a type of zero-width assertion
        // https://www.regular-expressions.info/lookaround.html
        // http://symbolhound.com/
        // https://www.google.com/search?q=?=
        s.split("(?=[A-Z])", -1).map(_.toLowerCase).mkString("_")
      }
      def snakeToCamel(s: String): String = {
        val res = s.split("_", -1).map(x => x(0).toUpper.toString + x.drop(1)).mkString
        s(0).toLower.toString + res.drop(1)
      }

      override def objectAttributeKeyReadMap(s: CharSequence): String =
        snakeToCamel(s.toString)
      override def objectAttributeKeyWriteMap(s: CharSequence): String =
        camelToSnake(s.toString)

      override def objectTypeKeyReadMap(s: CharSequence): String =
        snakeToCamel(s.toString)
      override def objectTypeKeyWriteMap(s: CharSequence): String =
        camelToSnake(s.toString)
    }

    // Default read-writing
    assert( upickle.default.write(Thing(1, "gg")) ==  """{"myFieldA":1,"myFieldB":"gg"}""" )

    assert( upickle.default.read[Thing]("""{"myFieldA":1,"myFieldB":"gg"}""") ==  Thing(1, "gg") )

    implicit def thingRW: SnakePickle.ReadWriter[Thing] = SnakePickle.macroRW

    // snake_case_keys read-writing
    assert( SnakePickle.write(Thing(1, "gg")) == """{"my_field_a":1,"my_field_b":"gg"}""" )
    assert( SnakePickle.read[Thing]("""{"my_field_a":1,"my_field_b":"gg"}""") == Thing(1, "gg") )

    // map Option[T]s to nulls when the option is None
    object OptionPickler extends upickle.AttributeTagged {
      override implicit def OptionWriter[T: Writer]: Writer[Option[T]] =
        implicitly[Writer[T]].comap[Option[T]] {
          case None => null.asInstanceOf[T]
          case Some(x) => x
        }

      override implicit def OptionReader[T: Reader]: Reader[Option[T]] = {
        new Reader.Delegate[Any, Option[T]](implicitly[Reader[T]].map(Some(_))){
          override def visitNull(index: Int) = None
        }
      }
    }

    import OptionPickler._
    implicit def rw: OptionPickler.ReadWriter[Thing] = OptionPickler.macroRW

    assert( write[None.type](None) == "null" )
    assert( read[None.type]("null").isEmpty )

    // Handling Longs that are not supported by JSON/JavaScript
    assert( upickle.default.write(123: Long) == "123" )
    assert( upickle.default.write(Long.MaxValue) == "\"9223372036854775807\"" )

    import upickle.core.Visitor

    // Always as strings
    object StringLongs extends upickle.AttributeTagged{
      override implicit val LongWriter = new Writer[Long] {
        def write0[V](out: Visitor[_, V], v: Long): V = out.visitString(v.toString, -1)
      }
    }

    assert( StringLongs.write(123: Long) == "\"123\"" )
    assert( StringLongs.write(Long.MaxValue) == "\"9223372036854775807\"" )

    // Always as longs
    object NumLongs extends upickle.AttributeTagged{
      override implicit val LongWriter = new Writer[Long] {
        def write0[V](out: Visitor[_, V], v: Long): V = out.visitFloat64String(v.toString, -1)
      }
    }

    assert( NumLongs.write(123: Long) == "123" )
    assert( NumLongs.write(Long.MaxValue) == "9223372036854775807" )

  }

  def jsonConstruct(): Unit = {
    import ujson.Value

    val json0 = ujson.Arr(
      ujson.Obj("myFieldA" -> ujson.Num(1), "myFieldB" -> ujson.Str("g")),
      ujson.Obj("myFieldA" -> ujson.Num(2), "myFieldB" -> ujson.Str("k"))
    )

    val json1 = ujson.Arr(
      // The `ujson.Num` and `ujson.Str` calls are optional
      ujson.Obj("myFieldA" -> 1, "myFieldB" -> "g"),
      ujson.Obj("myFieldA" -> 2, "myFieldB" -> "k")
    )

    assert( json0 == json1)

    // parsing them from strings, byte arrays or files
    val str = """[{"myFieldA":1,"myFieldB":"g"},{"myFieldA":2,"myFieldB":"k"}]"""
    val json = ujson.read(str)
    assert( json(0)("myFieldA").num == 1 )
    assert( json(0)("myFieldB").str == "g" )
    assert( json(1)("myFieldA").num == 2 )
    assert( json(1)("myFieldB").str == "k" )
    assert( ujson.write(json) == str )

    // ujson.Js ASTs are mutable
    json.arr.remove(1)
    json(0)("myFieldA") = 1337
    json(0)("myFieldB") = json(0)("myFieldB").str + "lols"
    assert(ujson.write(json) == """[{"myFieldA":1337,"myFieldB":"glols"}]""" )

    json(0)("myFieldA") = _.num + 100
    json(0)("myFieldB") = _.str + "olo"
    assert(ujson.write(json) == """[{"myFieldA":1437,"myFieldB":"glolsolo"}]""" )

    // case classes
    val data = Seq(Thing(1, "g"), Thing(2, "k"))
    val json3 = upickle.default.writeJs(data)

    json3.arr.remove(1)
    json3(0)("myFieldA") = 1337

    assert( upickle.default.read[Seq[Thing]](json3) == Seq(Thing(1337, "g")) )
  }

  def jsonTransform():Unit = {
    import ujson.Value

    // using the ujson.transform(source, dest) function

    // It can be used for parsing JSON into an AST
    val exampleAst = ujson.Arr(1, 2, 3)

    assert( ujson.transform("[1, 2, 3]", Value) == exampleAst )

    // Rendering the AST to a string
    assert( ujson.transform(exampleAst, StringRenderer()).toString == "[1,2,3]" )

    // Or to a byte array
    assert( ujson.transform(exampleAst, BytesRenderer()).toBytes sameElements  "[1,2,3]".getBytes )

    // Re-formatting JSON, either compacting it
    assert( ujson.transform("[1, 2, 3]", StringRenderer()).toString == "[1,2,3]" )

    // or indenting it
    assert( ujson.transform("[1, 2, 3]", StringRenderer(indent = 4)).toString ==
      """[
        |    1,
        |    2,
        |    3
        |]""".stripMargin )

    // `transform` takes any `Transformable`, including byte arrays and files
    assert( ujson.transform("[1, 2, 3]".getBytes, StringRenderer()).toString == "[1,2,3]" )

    // Use ujson.transform to validate JSON in a streaming fashion
    import upickle.core.NoOpVisitor

    ujson.transform("[1, 2, 3]", NoOpVisitor)

    try {
      ujson.transform("[1, 2, 3", NoOpVisitor)
      assert(false)
    } catch {
      case _:IncompleteParseException => ()
    }

    try {
      ujson.transform("[1, 2, 3]]", NoOpVisitor)
      assert(false)
    } catch {
    case _:ParseException => ()
    }

    // upickle.default.transform(foo) as a upickle.default.reader[Foo]
    assert(
      ujson.transform("[1, 2, 3]", upickle.default.reader[Seq[Int]])
      ==
      Seq(1, 2, 3) )

    assert(
      ujson.transform(upickle.default.transform(Seq(1, 2, 3)), StringRenderer()).toString
      ==
      "[1,2,3]" )
  }

  def jsonArgonaut(): Unit = {

    import ujson.argonaut.ArgonautJson
    import _root_.argonaut.Json

    val argJson: Json = ArgonautJson("""["hello", "world"]""")

    val updatedArgJson = argJson.withArray(_.map(_.withString(_.toUpperCase)))

    val items: Seq[String] = ArgonautJson.transform(
      updatedArgJson,
      upickle.default.reader[Seq[String]]
    )

    assert( items == Seq("HELLO", "WORLD") )

    val rewritten = upickle.default.transform(items).to(ArgonautJson)
    val stringified = ArgonautJson.transform(rewritten, StringRenderer()).toString

    assert( stringified == """["HELLO","WORLD"]""" )
  }

  def jsonCirce(): Unit = {
    import ujson.circe.CirceJson
    val circeJson: io.circe.Json = CirceJson(
      """["hello", "world"]"""
    )

    val updatedCirceJson =
      circeJson.mapArray(_.map(x => x.mapString(_.toUpperCase)))

    val items: Seq[String] = CirceJson.transform(
      updatedCirceJson,
      upickle.default.reader[Seq[String]]
    )

    assert( items == Seq("HELLO", "WORLD") )

    val rewritten = upickle.default.transform(items).to(CirceJson)
    val stringified = CirceJson.transform(rewritten, StringRenderer()).toString

    assert( stringified == """["HELLO","WORLD"]""" )
  }

  def jsonPlay(): Unit = {
    import ujson.play.PlayJson
    import _root_.play.api.libs.json._

    val playJson: JsValue = PlayJson( """["hello", "world"]""" )

    val updatedPlayJson = JsArray(
      for(v <- playJson.as[JsArray].value)
        yield JsString(v.as[String].toUpperCase())
    )

    val items: Seq[String] = PlayJson.transform(
      updatedPlayJson,
      upickle.default.reader[Seq[String]]
    )

    assert( items == Seq("HELLO", "WORLD") )

    val rewritten = upickle.default.transform(items).to(PlayJson)
    val stringified = PlayJson.transform(rewritten, StringRenderer()).toString

    assert( stringified == """["HELLO","WORLD"]""" )
  }

  def Json4s(): Unit = {
    import org.json4s.JsonAST
    import ujson.json4s.Json4sJson

    val json4sJson = Json4sJson( """["hello", "world"]""" )

    val updatedJson4sJson = JsonAST.JArray(
      for(v <- json4sJson.children)
        yield JsonAST.JString(v.values.toString.toUpperCase())
    )

    val items: Seq[String] = Json4sJson.transform(
      updatedJson4sJson,
      upickle.default.reader[Seq[String]]
    )

    assert( items == Seq("HELLO", "WORLD") )

    val rewritten = upickle.default.transform(items).to(Json4sJson)
    val stringified = Json4sJson.transform(rewritten, StringRenderer()).toString

    assert( stringified == """["HELLO","WORLD!"]""" )
  }

  def jsonConvert(): Unit = {
    import ujson.circe.CirceJson

    val circeJson: io.circe.Json = CirceJson(
      """["hello", "world"]"""
    )

    val updatedCirceJson =
      circeJson.mapArray(_.map(x => x.mapString(_.toUpperCase)))

    import ujson.play.PlayJson
    import _root_.play.api.libs.json._

    val playJson: JsValue = CirceJson.transform(
      updatedCirceJson,
      PlayJson
    )

    val updatedPlayJson = JsArray(
      for(v <- playJson.as[JsArray].value)
        yield JsString(v.as[String].reverse)
    )

    val stringified = PlayJson.transform(updatedPlayJson, StringRenderer()).toString

    val expected = """["OLLEH","DLROW"]"""
    assert( expected == stringified)
  }

  def pack(): Unit = {
    val msg0 = upack.Arr(
      upack.Obj(upack.Str("myFieldA") -> upack.Int32(1), upack.Str("myFieldB") -> upack.Str("g")),
      upack.Obj(upack.Str("myFieldA") -> upack.Int32(2), upack.Str("myFieldB") -> upack.Str("k"))
    )

    val binary0: Array[Byte] = upack.write(msg0)
    val read = upack.read(binary0)
    assert(msg0 == read)

    // read/write Scala values to upack.Msgs using readBinary/writeMsg

    val big = Big(1, true, "lol", 'Z', Thing(7, ""))
    val msg1 = upickle.default.writeMsg(big)
    assert( upickle.default.readBinary[Big](msg1) == big )

    // include upack.Msgs inside Seqs, case-classes and other data structures

    val msgSeq = Seq[upack.Msg](
      upack.Str("hello world"),
      upack.Arr(upack.Int32(1), upack.Int32(2))
    )

    val binary3: Array[Byte] = upickle.default.writeBinary(msgSeq)
    assert( upickle.default.readBinary[Seq[upack.Msg]](binary3) == msgSeq )

    // convert the uPack messages or binaries to ujson.Values via upack.transform.
    // Handy for debugging binary messages

    val msg4 = upack.Arr(
      upack.Obj(upack.Str("myFieldA") -> upack.Int32(1), upack.Str("myFieldB") -> upack.Str("g")),
      upack.Obj(upack.Str("myFieldA") -> upack.Int32(2), upack.Str("myFieldB") -> upack.Str("k"))
    )

    val binary: Array[Byte] = upack.write(msg4)

    // Can pretty-print starting from either the upack.Msg structs,
    // or the raw binary data
    assert(
      upack.transform(msg4, new ujson.StringRenderer()).toString
        ==
      """[{"myFieldA":1,"myFieldB":"g"},{"myFieldA":2,"myFieldB":"k"}]"""
    )

    assert(
      upack.transform(binary, new ujson.StringRenderer()).toString
        ==
      """[{"myFieldA":1,"myFieldB":"g"},{"myFieldA":2,"myFieldB":"k"}]""" )

    // Some messagepack structs cannot be converted to valid JSON, e.g.
    // they may have maps with non-string keys. These can still be pretty-printed:
    val msg2 = upack.Obj(upack.Arr(upack.Int32(1), upack.Int32(2)) -> upack.Int32(1))
    assert(
      upack.transform(msg2, new ujson.StringRenderer()).toString
        ==
      """{[1,2]:1}"""
    )
  }

  /**
   * uPickle, uJson and uPack examples.
   *
   * @see http://www.lihaoyi.com/upickle/
   *
   * @param args command line options
   */
  def main(args: Array[String]): Unit = {
    ex1()
    ex2()
    ex3()
    ex4()

    ex5()
    ex6()

    // uPickle

    basics()
    caseClasses()
    loadSources()
    defaults()

    customPickle()
    customKeys()
    customConfig()

    // uJason
    jsonConstruct()
    jsonTransform()
    jsonArgonaut()
    jsonCirce()
    jsonPlay()
    jsonConvert()

    // uPack
    pack()

  }

}
