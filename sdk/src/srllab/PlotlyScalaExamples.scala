package srllab

import webkit.Utils

/**
 * mill mill.scalalib.GenIdea/idea
 *
 * mill sdk.compile
 * mill sdk.run
 * mill sdk.{compile, run}
 * mill --watch sdk.run
 *
 * mill -i sdk.console
 * mill -i sdk.repl
 *
 * mill -i sdk.runMain srllab.PlotlyScalaExamples
 * mill --watch sdk.runMain srllab.PlotlyScalaExamples
 *
 */
object PlotlyScalaExamples {
/*
  def sample1(): Unit = {

    val labels = Seq("Banana", "Banano", "Grapefruit")
    val valuesA = labels.map(_ => util.Random.nextGaussian())
    val valuesB = labels.map(_ => 0.5 + util.Random.nextGaussian())

    val property = "java.io.tmpdir"
    val tempDir = System.getProperty(property)
    val file1 = Utils.temporaryFile(tempDir, "_plotlyscala1.html").toJava

    Seq(
      Bar(labels, valuesA, name = "A"),
      Bar(labels, valuesB, name = "B")
    ).plot(
      title = "Level",
      path = file1.getPath
    )

  }


  def sample2(): String = {

    val labels = Seq("Banana", "Banano", "Grapefruit")
    val valuesA = labels.map(_ => util.Random.nextGaussian())
    val valuesB = labels.map(_ => 0.5 + util.Random.nextGaussian())

    val traces: Seq[Trace] = Seq(
      Bar(labels, valuesA, name = "A"),
      Bar(labels, valuesB, name = "B")
    )
    val playout: Layout = Layout()
    val jsSnippet = Plotly.jsSnippet("sample2", traces, playout)
    jsSnippet
  }


  def sample3(): java.io.File = {

    val labels = Seq("Banana", "Banano", "Grapefruit")
    val valuesA = labels.map(_ => util.Random.nextGaussian())
    val valuesB = labels.map(_ => 0.5 + util.Random.nextGaussian())

    val traces: Seq[Trace] = Seq(
      Bar(labels, valuesA, name = "A"),
      Bar(labels, valuesB, name = "B")
    )

    val property = "java.io.tmpdir"
    val tempDir = System.getProperty(property)
    val file1 = Utils.temporaryFile(tempDir, "_plotlyscala3.html").toJava

    val playout: Layout = Layout()
    Plotly.plot(file1.getAbsolutePath, traces, playout)
  }
*/

  def main(args: Array[String]): Unit = {
    /* TODO: remove?
    val jsSnippet = "saasasa"
    println(s"""
            |```
            |$jsSnippet
            |```
            """.stripMargin)

    //sample1()
    val snippet = sample2()
    println(s"Plotly-scala sample2: $snippet")
    println(s"Plotly-scala sample2: $snippet")
    println(s"Plotly-scala sample2: ${sample2()}")
    //println(s"Plotly-scala sample3: ${sample3()}")
    */
  }
}
