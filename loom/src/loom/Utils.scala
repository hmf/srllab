package loom

import java.io.File

object Utils {

  /**
   * Combines 2 sets of module paths used as JVM arguments. This is used to ensure that
   * the JVM loads the required libraries via the JVM's parameter
   *
   * @param args1 first list of required modules
   * @param args2 second list of required modules
   * @return
   */
  def jvmModuleArgs(args1: Seq[String], args2: Seq[String]): Seq[String] = {
    val MODULE_PATH = "--module-path="
    val (module_path1, rest1) = args1.partition(_.replaceAll(" +", "").trim.toLowerCase.contains(MODULE_PATH))
    val (module_path2, rest2) = args2.partition(_.replaceAll(" +", "").trim.toLowerCase.contains(MODULE_PATH))
    if (module_path1.length > 1) throw new RuntimeException(s"args1:$args1 must only have one $MODULE_PATH")
    if (module_path2.length > 1) throw new RuntimeException(s"args2:$args2 must only have one $MODULE_PATH")
    //println(module_path1.head.drop(MODULE_PATH.length))
    //println(module_path2.head.drop(MODULE_PATH.length))
    val module_path = "--module-path=" +
      module_path1.head.drop(MODULE_PATH.length) +
      File.pathSeparator +
      module_path2.head.drop(MODULE_PATH.length)
    Seq(module_path) ++ rest1 ++ rest2
  }

}
