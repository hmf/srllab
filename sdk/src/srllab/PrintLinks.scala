package srllab

import better.files._
import File._
import java.io.{File => JFile}
import better.files.Dsl._

/**
 * mill mill.scalalib.GenIdea/idea
 *
 * mill sdk.compile
 * mill sdk.run
 * mill sdk.{compile, run}
 * mill --watch sdk.run
 *
 * mill -i sdk.console
 * mill -i sdk.repl
 *
 * mill -i sdk.runMain srllab.PrintLinks
 * mill --watch sdk.runMain srllab.PrintLinks
 *
 */
object PrintLinks {

  // https://unix.stackexchange.com/questions/168569/printing-webpage-using-browser-via-cli
  // https://superuser.com/questions/592974/how-to-print-to-save-as-pdf-from-a-command-line-with-chrome-or-chromium
  // chrome --headless --print-to-pdf="d:\\{{path and file name}}.pdf" https://google.com

  def main(args: Array[String]): Unit = {
    // Change all currency to english numeric (, -> .)
    // Remove all , from description (, -> ;)
    // Save as CSV, select filter and make separator $
    val file = home / "projects" / "iilab" / "mobile_v8.csv"

    // Running the above command
    val run  = Runtime.getRuntime

    val lines = file.lineIterator
    while (lines.hasNext){
      val line = lines.next()
      val tokens = line.split("\\$")
      //if (line.contains("https://www.johnmorrisgroup.com/"))
      //  println(s"""Tokens -- ${tokens.mkString(",")}""")
      if (tokens.size >= 7){
        val link = tokens(6)
        //if (link.startsWith("htt") && !link.toLowerCase.contains("flir")){
        if (link.startsWith("htt") ){
          val product = tokens(2)
          if (product == null || product.isEmpty)
            println(s"""No ID for $link""")
          val fileName = home / "projects" / "iilab" / "links1" / (product + ".pdf")
          val command = s"wkhtmltopdf $link $fileName"
          //val fileName = home / "projects" / "iilab" / "links2" / product + ".pdf"
          //val command = s"""google-chrome --headless --disable-gpu --print-to-pdf="$fileName" $link"""
          //println(s"""$fileName -- $link""")
          println(command)
          val proc = run.exec(command)
          /*val exitCode = proc.waitFor()
          if (exitCode != 0)
            println(s"ExitCode($exitCode) for $command")

           */
        } // link
      } // CSV tokens
    }

  }
}
