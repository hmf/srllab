package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Parcoords() extends Trace {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val type_ : String      = """parcoords"""
  val animatable: Boolean = false
  val categories: Array[String] =
    Array("""noHover""", """noOpacity""", """regl""", """gl""")

  case object meta extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val description: String =
      """Parallel coordinates for multidimensional exploratory data analysis. The samples are specified in `dimensions`. The colors are set in `line.color`."""

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val vars: List[String] = List()
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV => }
      this
    }

  }

  case object attributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val type_ : String = """parcoords"""

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val name: PChars = PChars(
      dflt = None,
      description = Some(
        """Sets the trace name. The trace name appear as the legend item and on hover."""
      )
    )

    object visible extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Boolean = true
      val description: Option[String] = Some(
        """Determines whether or not this trace is visible. If *legendonly*, the trace is not drawn, but can appear as a legend item (provided that the legend itself is visible)."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) => t.toString
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => TBoolean(s)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def true_(): Unit = { selected = TBoolean(true) }

      def false_(): Unit = { selected = TBoolean(false) }

      def legendonly(): Unit = { selected = TString("legendonly") }

    }

    object labelside extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """top"""
      val description: Option[String] = Some(
        """Specifies the location of the `label`. *top* positions labels above, next to the title *bottom* positions labels below the graph Tilted labels with *labelangle* may be positioned better inside margins when `labelposition` is set to *bottom*."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def bottom(): Unit = { selected = TString("bottom") }

      def top(): Unit = { selected = TString("top") }

    }

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uirevision: PAny = PAny(
      dflt = None,
      description = Some(
        """Controls persistence of some user-driven changes to the trace: `constraintrange` in `parcoords` traces, as well as some `editable: true` modifications such as `name` and `colorbar.title`. Defaults to `layout.uirevision`. Note that other user-driven trace attribute changes are controlled by `layout` attributes: `trace.visible` is controlled by `layout.legend.uirevision`, `selectedpoints` is controlled by `layout.selectionrevision`, and `colorbar.(x|y)` (accessible with `config: {editable: true}`) is controlled by `layout.editrevision`. Trace changes are tracked by `uid`, which only falls back on trace index if no `uid` is provided. So if your app can add/remove traces before the end of the `data` array, such that the same trace has a different index, you can still preserve user-driven changes if you give each trace a `uid` that stays with it as it moves."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val idssrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  ids .""")
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val meta: PAny = PAny(
      dflt = None,
      description = Some(
        """Assigns extra meta information associated with this trace that can be used in various text attributes. Attributes such as trace `name`, graph, axis and colorbar `title.text`, annotation `text` `rangeselector`, `updatemenues` and `sliders` `label` text all support `meta`. To access the trace `meta` values in an attribute in the same trace, simply use `%{meta[i]}` where `i` is the index or key of the `meta` item in question. To access trace `meta` in layout attributes, use `%{data[n[.meta[i]}` where `i` is the index or key of the `meta` and `n` is the trace index."""
      )
    )

    /* §#
     * generateAngleDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val labelangle: PAngle = PAngle(
      dflt = Some("""0.0"""),
      description = Some(
        """Sets the angle of the labels with respect to the horizontal. For example, a `tickangle` of -90 draws the labels vertically. Tilted labels with *labelangle* may be positioned better inside margins when `labelposition` is set to *bottom*."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uid: PChars = PChars(
      dflt = None,
      description = Some(
        """Assign an id to this trace, Use this to provide object constancy between traces during animations and transitions."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ids: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns id labels to each datum. These ids for object constancy of data points during animation. Should be an array of strings, not numbers or any other type."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val metasrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  meta .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdata: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns extra data each datum. This may be useful when listening to hover, click and selection events. Note that, *scatter* traces also appends customdata items in the markers DOM elements"""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdatasrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  customdata .""")
    )

    case object line extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val cmax: PReal = PReal(
        min = None,
        max = None,
        dflt = None,
        description = Some(
          """Sets the upper bound of the color domain. Has an effect only if in `line.color`is set to a numerical array. Value should have the same units as in `line.color` and if set, `line.cmin` must be set as well."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val cmin: PReal = PReal(
        min = None,
        max = None,
        dflt = None,
        description = Some(
          """Sets the lower bound of the color domain. Has an effect only if in `line.color`is set to a numerical array. Value should have the same units as in `line.color` and if set, `line.cmax` must be set as well."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  color .""")
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val autocolorscale: PBool = PBool(
        dflt = Some(false),
        description = Some(
          """Determines whether the colorscale is a default palette (`autocolorscale: true`) or the palette determined by `line.colorscale`. Has an effect only if in `line.color`is set to a numerical array. In case `colorscale` is unspecified or `autocolorscale` is true, the default  palette will be chosen according to whether numbers in the `color` array are all positive, all negative or mixed."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val reversescale: PBool = PBool(
        dflt = Some(false),
        description = Some(
          """Reverses the color mapping if true. Has an effect only if in `line.color`is set to a numerical array. If true, `line.cmin` will correspond to the last color in the array and `line.cmax` will correspond to the first color."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val showscale: PBool = PBool(
        dflt = Some(false),
        description = Some(
          """Determines whether or not a colorbar is displayed for this trace. Has an effect only if in `line.color`is set to a numerical array."""
        )
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = Some(
          """Sets thelinecolor. It accepts either a specific color or an array of numbers that are mapped to the colorscale relative to the max and min values of the array or relative to `line.cmin` and `line.cmax` if set."""
        )
      )

      /* §#
       * generateSubPlotIDDeclaration(in: PrimitiveDefinition
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val coloraxis: SubPlotID = SubPlotID(
        dflt = None,
        description = Some(
          """Sets a reference to a shared color axis. References to these shared color axes are *coloraxis*, *coloraxis2*, *coloraxis3*, etc. Settings for these shared color axes are set in the layout, under `layout.coloraxis`, `layout.coloraxis2`, etc. Note that multiple color scales can be linked to the same color axis."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val cmid: PReal = PReal(
        min = None,
        max = None,
        dflt = None,
        description = Some(
          """Sets the mid-point of the color domain by scaling `line.cmin` and/or `line.cmax` to be equidistant to this point. Has an effect only if in `line.color`is set to a numerical array. Value should have the same units as in `line.color`. Has no effect when `line.cauto` is `false`."""
        )
      )

      /* §#
       * generateColorScaleDeclaration(in: PrimitiveDefinition
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorscale: PColorScale = PColorScale(
        dflt = Some(
          Array(
            (0.0, """#440154"""),
            (0.06274509803921569, """#48186a"""),
            (0.12549019607843137, """#472d7b"""),
            (0.18823529411764706, """#424086"""),
            (0.25098039215686274, """#3b528b"""),
            (0.3137254901960784, """#33638d"""),
            (0.3764705882352941, """#2c728e"""),
            (0.4392156862745098, """#26828e"""),
            (0.5019607843137255, """#21918c"""),
            (0.5647058823529412, """#1fa088"""),
            (0.6274509803921569, """#28ae80"""),
            (0.6901960784313725, """#3fbc73"""),
            (0.7529411764705882, """#5ec962"""),
            (0.8156862745098039, """#84d44b"""),
            (0.8784313725490196, """#addc30"""),
            (0.9411764705882353, """#d8e219""")
          )
        ),
        description = Some(
          """Sets the colorscale. Has an effect only if in `line.color`is set to a numerical array. The colorscale must be an array containing arrays mapping a normalized value to an rgb, rgba, hex, hsl, hsv, or named color string. At minimum, a mapping for the lowest (0) and highest (1) values are required. For example, `[[0, 'rgb(0,0,255)'], [1, 'rgb(255,0,0)']]`. To control the bounds of the colorscale in color space, use`line.cmin` and `line.cmax`. Alternatively, `colorscale` may be a palette name string of the following list: Greys,YlGnBu,Greens,YlOrRd,Bluered,RdBu,Reds,Blues,Picnic,Rainbow,Portland,Jet,Hot,Blackbody,Earth,Electric,Viridis,Cividis."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val cauto: PBool = PBool(
        dflt = Some(true),
        description = Some(
          """Determines whether or not the color domain is computed with respect to the input data (here in `line.color`) or the bounds set in `line.cmin` and `line.cmax`  Has an effect only if in `line.color`is set to a numerical array. Defaults to `false` when `line.cmin` and `line.cmax` are set by the user."""
        )
      )

      case object colorbar extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """colorbars"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val x: PReal = PReal(
          min = Some(-2.0),
          max = Some(3.0),
          dflt = Some("""1.02"""),
          description =
            Some("""Sets the x position of the color bar (in plot fraction).""")
        )

        /* §#
         * generateAny(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tick0: PAny = PAny(
          dflt = None,
          description = Some(
            """Sets the placement of the first tick on this axis. Use with `dtick`. If the axis `type` is *log*, then you must take the log of your starting tick (e.g. to set the starting tick to 100, set the `tick0` to 2) except when `dtick`=*L<f>* (see `dtick` for more info). If the axis `type` is *date*, it should be a date string, like date data. If the axis `type` is *category*, it should be a number, using the scale where each category is assigned a serial number from zero in the order it appears."""
          )
        )

        /* §#
         * generateIntDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val nticks: PInt = PInt(
          min = Some(0),
          max = None,
          dflt = Some(0),
          description = Some(
            """Specifies the maximum number of ticks for the particular axis. The actual number of ticks will be chosen automatically to be less than or equal to `nticks`. Has an effect only if `tickmode` is set to *auto*."""
          )
        )

        /* §#
         * generateAngleDeclaration(in: PrimitiveDefinition
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickangle: PAngle = PAngle(
          dflt = Some("""auto"""),
          description = Some(
            """Sets the angle of the tick labels with respect to the horizontal. For example, a `tickangle` of -90 draws the tick labels vertically."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val outlinewidth: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""1.0"""),
          description = Some("""Sets the width (in px) of the axis line.""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val bgcolor: PColor = PColor(
          dflt = Some("""rgba(0,0,0,0)"""),
          description = Some("""Sets the color of padded area.""")
        )

        object showexponent extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """all"""
          val description: Option[String] = Some(
            """If *all*, all exponents are shown besides their significands. If *first*, only the exponent of the first tick is shown. If *last*, only the exponent of the last tick is shown. If *none*, no exponents appear."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def none(): Unit = { selected = TString("none") }

          def last(): Unit = { selected = TString("last") }

          def first(): Unit = { selected = TString("first") }

          def all(): Unit = { selected = TString("all") }

        }

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ypad: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""10.0"""),
          description = Some(
            """Sets the amount of padding (in px) along the y direction."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val y: PReal = PReal(
          min = Some(-2.0),
          max = Some(3.0),
          dflt = Some("""0.5"""),
          description =
            Some("""Sets the y position of the color bar (in plot fraction).""")
        )

        object yanchor extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """middle"""
          val description: Option[String] = Some(
            """Sets this color bar's vertical position anchor This anchor binds the `y` position to the *top*, *middle* or *bottom* of the color bar."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def bottom(): Unit = { selected = TString("bottom") }

          def middle(): Unit = { selected = TString("middle") }

          def top(): Unit = { selected = TString("top") }

        }

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticksuffix: PChars = PChars(
          dflt = Some(""""""),
          description = Some("""Sets a tick label suffix.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickwidth: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""1.0"""),
          description = Some("""Sets the tick width (in px).""")
        )

        /* §#
         * generateDataArrayDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickvals: DataArray = DataArray(
          dflt = None,
          description = Some(
            """Sets the values at which ticks on this axis appear. Only has an effect if `tickmode` is set to *array*. Used with `ticktext`."""
          )
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val bordercolor: PColor = PColor(
          dflt = Some("""#444"""),
          description = Some("""Sets the axis line color.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val thickness: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""30.0"""),
          description = Some(
            """Sets the thickness of the color bar This measure excludes the size of the padding, ticks and labels."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticklen: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""5.0"""),
          description = Some("""Sets the tick length (in px).""")
        )

        object tickmode extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val description: Option[String] = Some(
            """Sets the tick mode for this axis. If *auto*, the number of ticks is set via `nticks`. If *linear*, the placement of the ticks is determined by a starting position `tick0` and a tick step `dtick` (*linear* is the default value if `tick0` and `dtick` are provided). If *array*, the placement of the ticks is set via `tickvals` and the tick text is `ticktext`. (*array* is the default value if `tickvals` is provided)."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def array(): Unit = { selected = TString("array") }

          def linear(): Unit = { selected = TString("linear") }

          def auto(): Unit = { selected = TString("auto") }

        }

        /* §#
         * generateAny(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dtick: PAny = PAny(
          dflt = None,
          description = Some(
            """Sets the step in-between ticks on this axis. Use with `tick0`. Must be a positive number, or special strings available to *log* and *date* axes. If the axis `type` is *log*, then ticks are set every 10^(n*dtick) where n is the tick number. For example, to set a tick mark at 1, 10, 100, 1000, ... set dtick to 1. To set tick marks at 1, 100, 10000, ... set dtick to 2. To set tick marks at 1, 5, 25, 125, 625, 3125, ... set dtick to log_10(5), or 0.69897000433. *log* has several special values; *L<f>*, where `f` is a positive number, gives ticks linearly spaced in value (but not position). For example `tick0` = 0.1, `dtick` = *L0.5* will put ticks at 0.1, 0.6, 1.1, 1.6 etc. To show powers of 10 plus small digits between, use *D1* (all digits) or *D2* (only 2 and 5). `tick0` is ignored for *D1* and *D2*. If the axis `type` is *date*, then you must convert the time to milliseconds. For example, to set the interval between ticks to one day, set `dtick` to 86400000.0. *date* also has special values *M<n>* gives ticks spaced by a number of months. `n` must be a positive integer. To set ticks on the 15th of every third month, set `tick0` to *2000-01-15* and `dtick` to *M3*. To set ticks every 4 years, set `dtick` to *M48*"""
          )
        )

        object lenmode extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """fraction"""
          val description: Option[String] = Some(
            """Determines whether this color bar's length (i.e. the measure in the color variation direction) is set in units of plot *fraction* or in *pixels. Use `len` to set the value."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def pixels(): Unit = { selected = TString("pixels") }

          def fraction(): Unit = { selected = TString("fraction") }

        }

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickvalssrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  tickvals .""")
        )

        object showticksuffix extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """all"""
          val description: Option[String] = Some(
            """Same as `showtickprefix` but for tick suffixes."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def none(): Unit = { selected = TString("none") }

          def last(): Unit = { selected = TString("last") }

          def first(): Unit = { selected = TString("first") }

          def all(): Unit = { selected = TString("all") }

        }

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val xpad: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""10.0"""),
          description = Some(
            """Sets the amount of padding (in px) along the x direction."""
          )
        )

        /* §#
         * generateDataArrayDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticktext: DataArray = DataArray(
          dflt = None,
          description = Some(
            """Sets the text displayed at the ticks position via `tickvals`. Only has an effect if `tickmode` is set to *array*. Used with `tickvals`."""
          )
        )

        object ticks extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """"""
          val description: Option[String] = Some(
            """Determines whether ticks are drawn or not. If **, this axis' ticks are not drawn. If *outside* (*inside*), this axis' are drawn outside (inside) the axis lines."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def default(): Unit = { selected = TString("") }

          def inside(): Unit = { selected = TString("inside") }

          def outside(): Unit = { selected = TString("outside") }

        }

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickcolor: PColor = PColor(
          dflt = Some("""#444"""),
          description = Some("""Sets the tick color.""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticktextsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  ticktext .""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val outlinecolor: PColor = PColor(
          dflt = Some("""#444"""),
          description = Some("""Sets the axis line color.""")
        )

        object exponentformat extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """B"""
          val description: Option[String] = Some(
            """Determines a formatting rule for the tick exponents. For example, consider the number 1,000,000,000. If *none*, it appears as 1,000,000,000. If *e*, 1e+9. If *E*, 1E+9. If *power*, 1x10^9 (with 9 in a super script). If *SI*, 1G. If *B*, 1B."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def B(): Unit = { selected = TString("B") }

          def SI(): Unit = { selected = TString("SI") }

          def power(): Unit = { selected = TString("power") }

          def E(): Unit = { selected = TString("E") }

          def e(): Unit = { selected = TString("e") }

          def none(): Unit = { selected = TString("none") }

        }

        object showtickprefix extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """all"""
          val description: Option[String] = Some(
            """If *all*, all tick labels are displayed with a prefix. If *first*, only the first tick is displayed with a prefix. If *last*, only the last tick is displayed with a suffix. If *none*, tick prefixes are hidden."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def none(): Unit = { selected = TString("none") }

          def last(): Unit = { selected = TString("last") }

          def first(): Unit = { selected = TString("first") }

          def all(): Unit = { selected = TString("all") }

        }

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickprefix: PChars = PChars(
          dflt = Some(""""""),
          description = Some("""Sets a tick label prefix.""")
        )

        object xanchor extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """left"""
          val description: Option[String] = Some(
            """Sets this color bar's horizontal position anchor. This anchor binds the `x` position to the *left*, *center* or *right* of the color bar."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def right(): Unit = { selected = TString("right") }

          def center(): Unit = { selected = TString("center") }

          def left(): Unit = { selected = TString("left") }

        }

        object thicknessmode extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """pixels"""
          val description: Option[String] = Some(
            """Determines whether this color bar's thickness (i.e. the measure in the constant color direction) is set in units of plot *fraction* or in *pixels*. Use `thickness` to set the value."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def pixels(): Unit = { selected = TString("pixels") }

          def fraction(): Unit = { selected = TString("fraction") }

        }

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val separatethousands: PBool = PBool(
          dflt = Some(false),
          description =
            Some("""If "true", even 4-digit integers are separated""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickformat: PChars = PChars(
          dflt = Some(""""""),
          description = Some(
            """Sets the tick label formatting rule using d3 formatting mini-languages which are very similar to those in Python. For numbers, see: https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format And for dates see: https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format We add one item to d3's date formatter: *%{n}f* for fractional seconds with n digits. For example, *2016-10-13 09:15:23.456* with tickformat *%H~%M~%S.%2f* would display *09~15~23.46*"""
          )
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val showticklabels: PBool = PBool(
          dflt = Some(true),
          description =
            Some("""Determines whether or not the tick labels are drawn.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val len: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the length of the color bar This measure excludes the padding of both ends. That is, the color bar length is this length minus the padding on both ends."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val borderwidth: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""0.0"""),
          description = Some(
            """Sets the width (in px) or the border enclosing this color bar."""
          )
        )

        case object tickfont extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String        = """object"""
          val description: String = """Sets the color bar's tick label font"""
          val editType: String    = """colorbars"""

          /* §#
           * generateColor(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val color: PColor = PColor(
            dflt = None,
            description = None
          )

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val family: PChars = PChars(
            dflt = None,
            description = Some(
              """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
            )
          )

          /* §#
           * generateNumberDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val size: PReal =
            PReal(min = Some(1.0), max = None, dflt = None, description = None)

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (color.json().length > 0) "\"color\"" + ":" + color.json()
              else ""
            val v1 =
              if (family.json().length > 0) "\"family\"" + ":" + family.json()
              else ""
            val v2 =
              if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
            val vars: List[String] = List(v0, v1, v2)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("color")
              in0.map(ve => color.fromJson(ve))

              val in1 = mapKV.get("family")
              in1.map(ve => family.fromJson(ve))

              val in2 = mapKV.get("size")
              in2.map(ve => size.fromJson(ve))

            }
            this
          }

        }

        case object title extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String     = """object"""
          val editType: String = """colorbars"""

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val text: PChars = PChars(
            dflt = None,
            description = Some(
              """Sets the title of the color bar. Note that before the existence of `title.text`, the title's contents used to be defined as the `title` attribute itself. This behavior has been deprecated."""
            )
          )

          object side extends JSON {
            /* §#
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val dflt: String = """top"""
            val description: Option[String] = Some(
              """Determines the location of color bar's title with respect to the color bar. Note that the title's location used to be set by the now deprecated `titleside` attribute."""
            )

            private var selected: OfPrimitive = NAPrimitive
            //allDeclarations

            def json(): String = {
              /* §#
               * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
               * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              selected match {
                case NAPrimitive => ""
                case TString(t)  => "\"" + t.toString + "\""
                case TBoolean(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TBoolean(false)"""
                  )
                case TDouble(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TDouble(0.0)"""
                  )
                case TInt(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TInt(0)"""
                  )
                case TImpliedEdits(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TImpliedEdits()"""
                  )
                case TAttrRegexps(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TAttrRegexps()"""
                  )
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
               * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err(j: ujson.Value): OfPrimitive =
                throw new RuntimeException(s"""Parsing of $j failed: $in""")
              val read = in match {
                case Str(s)  => TString(s)
                case Bool(s) => err(in)
                case Num(s)  => err(in)
                case _       => err(in)
              }
              selected = read
              this
            }

            def bottom(): Unit = { selected = TString("bottom") }

            def top(): Unit = { selected = TString("top") }

            def right(): Unit = { selected = TString("right") }

          }

          case object font extends JSON {
            /* §#
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val role: String = """object"""
            val description: String =
              """Sets this color bar's title font. Note that the title's font used to be set by the now deprecated `titlefont` attribute."""
            val editType: String = """colorbars"""

            /* §#
             * generateColor(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val color: PColor = PColor(
              dflt = None,
              description = None
            )

            /* §#
             * generateStringDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val family: PChars = PChars(
              dflt = None,
              description = Some(
                """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
              )
            )

            /* §#
             * generateNumberDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val size: PReal = PReal(
              min = Some(1.0),
              max = None,
              dflt = None,
              description = None
            )

            def json(): String = {
              /* §#
               * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/

              val v0 =
                if (color.json().length > 0) "\"color\"" + ":" + color.json()
                else ""
              val v1 =
                if (family.json().length > 0) "\"family\"" + ":" + family.json()
                else ""
              val v2 =
                if (size.json().length > 0) "\"size\"" + ":" + size.json()
                else ""
              val vars: List[String] = List(v0, v1, v2)
              val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
              if (cleanVars.length > 0) {
                // values set
                "{" + cleanVars + "}"
              } else {
                // no values set
                ""
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err =
                throw new RuntimeException(s"Expected an Obj but got $in")
              in.objOpt.fold(err) { mapKV =>
                val in0 = mapKV.get("color")
                in0.map(ve => color.fromJson(ve))

                val in1 = mapKV.get("family")
                in1.map(ve => family.fromJson(ve))

                val in2 = mapKV.get("size")
                in2.map(ve => size.fromJson(ve))

              }
              this
            }

          }

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
            val v1 =
              if (text.json().length > 0) "\"text\"" + ":" + text.json() else ""
            val v2 =
              if (side.json().length > 0) "\"side\"" + ":" + side.json() else ""
            val vars: List[String] = List(v0, v1, v2)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("font")
              in0.map(ve => font.fromJson(ve))

              val in1 = mapKV.get("text")
              in1.map(ve => text.fromJson(ve))

              val in2 = mapKV.get("side")
              in2.map(ve => side.fromJson(ve))

            }
            this
          }

        }

        case object tickformatstops extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""

          case object items extends JSON {
            /* §#
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            case object tickformatstop extends JSON {
              /* §#
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val role: String     = """object"""
              val editType: String = """colorbars"""

              object dtickrange extends JSON {
                /* §#
                 * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                val description: Option[String] = Some(
                  """range [*min*, *max*], where *min*, *max* - dtick values which describe some zoom level, it is possible to omit *min* or *max* value by passing *null*"""
                )

                private var string: Array[String]           = Array.fill(2)("")
                private var boolean: Array[Boolean]         = Array.fill(2)(false)
                private var double: Array[Double]           = Array.fill(2)(0.0)
                private var int: Array[Int]                 = Array.fill(2)(0)
                private var selected: Map[Int, OfPrimitive] = Map()
                private val n: Integer                      = 2

                def json(): String = {
                  /* §#
                   * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
                   * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generate(obj: CompoundDefinition)
                   * generate(obj: Either[Error, CompoundDefinition])*/
                  val nulls = "null"
                  val values = (0 until n) map { i =>
                    val t = selected.get(i)
                    t.fold(nulls)({
                      case NAPrimitive =>
                        throw new RuntimeException(s"Unexpected type: $t")
                      case TString(_)  => string(i)
                      case TBoolean(_) => boolean(i).toString
                      case TDouble(_)  => double(i).toString
                      case TInt(_)     => int(i).toString
                      case TImpliedEdits(_) =>
                        throw new RuntimeException(s"Unexpected type: $t")
                      case TAttrRegexps(_) =>
                        throw new RuntimeException(s"Unexpected type: $t")
                    })
                  }
                  val allNull = values.forall(_.toLowerCase.equals(nulls))
                  if (allNull)
                    ""
                  else {
                    values.filter(_.length > 0).mkString("[", ",", "]")
                  }
                }

                override def fromJson(in: Value): this.type = {
                  /* §#
                   * generateInfoArrayFromJson()
                   * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generate(obj: CompoundDefinition)
                   * generate(obj: Either[Error, CompoundDefinition])*/
                  def err(j: ujson.Value): OfPrimitive =
                    throw new RuntimeException(s"""Parsing of $j failed: $in""")
                  in match {
                    case Arr(values) =>
                      values.zipWithIndex.foreach { kv =>
                        val v = kv._1
                        val i = kv._2
                        v match {
                          case Bool(v) =>
                            boolean(i) = v; selected += (i -> TBoolean(v))
                          case Str(v) =>
                            string(i) = v; selected += (i -> TString(v))

                          case Num(v) =>
                            if (v.isValidInt) {
                              int(i) = v.toInt; selected += (i -> TInt(v.toInt))
                            } else {
                              double(i) = v; selected += (i -> TDouble(v))
                            }

                          case Null => () // nulls are valid
                          case _    => err(in)
                        }
                      }
                      this
                    case _ =>
                      err(in)
                      this
                  }
                }

// Set the value
                def string_0(v: String): Unit = {
                  string(0) = v; selected += (0 -> TString(v))
                }
// Delete the value (use default)
                def string_0(): Unit = { selected -= 0 }

// Set the value
                def boolean_0(v: Boolean): Unit = {
                  boolean(0) = v; selected += (0 -> TBoolean(v))
                }
// Delete the value (use default)
                def boolean_0(): Unit = { selected -= 0 }

// Set the value
                def double_0(v: Double): Unit = {
                  double(0) = v; selected += (0 -> TDouble(v))
                }
// Delete the value (use default)
                def double_0(): Unit = { selected -= 0 }

// Set the value
                def int_0(v: Int): Unit = {
                  int(0) = v; selected += (0 -> TInt(v))
                }
// Delete the value (use default)
                def int_0(): Unit = { selected -= 0 }

// Set the value
                def string_1(v: String): Unit = {
                  string(1) = v; selected += (1 -> TString(v))
                }
// Delete the value (use default)
                def string_1(): Unit = { selected -= 1 }

// Set the value
                def boolean_1(v: Boolean): Unit = {
                  boolean(1) = v; selected += (1 -> TBoolean(v))
                }
// Delete the value (use default)
                def boolean_1(): Unit = { selected -= 1 }

// Set the value
                def double_1(v: Double): Unit = {
                  double(1) = v; selected += (1 -> TDouble(v))
                }
// Delete the value (use default)
                def double_1(): Unit = { selected -= 1 }

// Set the value
                def int_1(v: Int): Unit = {
                  int(1) = v; selected += (1 -> TInt(v))
                }
// Delete the value (use default)
                def int_1(): Unit = { selected -= 1 }

              }

              /* §#
               * generateStringDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val name: PChars = PChars(
                dflt = None,
                description = Some(
                  """When used in a template, named items are created in the output figure in addition to any items the figure already has in this array. You can modify these items in the output figure by making your own item with `templateitemname` matching this `name` alongside your modifications (including `visible: false` or `enabled: false` to hide it). Has no effect outside of a template."""
                )
              )

              /* §#
               * generateBooleanDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val enabled: PBool = PBool(
                dflt = Some(true),
                description = Some(
                  """Determines whether or not this stop is used. If `false`, this stop is ignored even within its `dtickrange`."""
                )
              )

              /* §#
               * generateStringDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val value: PChars = PChars(
                dflt = Some(""""""),
                description = Some(
                  """string - dtickformat for described zoom level, the same as *tickformat*"""
                )
              )

              /* §#
               * generateStringDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val templateitemname: PChars = PChars(
                dflt = None,
                description = Some(
                  """Used to refer to a named item in this array in the template. Named items from the template will be created even without a matching item in the input figure, but you can modify one by making an item with `templateitemname` matching its `name`, alongside your modifications (including `visible: false` or `enabled: false` to hide it). If there is no template or no matching item, this item will be hidden unless you explicitly show it with `visible: true`."""
                )
              )

              def json(): String = {
                /* §#
                 * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
                 * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/

                val v0 =
                  if (dtickrange.json().length > 0)
                    "\"dtickrange\"" + ":" + dtickrange.json()
                  else ""
                val v1 =
                  if (name.json().length > 0) "\"name\"" + ":" + name.json()
                  else ""
                val v2 =
                  if (enabled.json().length > 0)
                    "\"enabled\"" + ":" + enabled.json()
                  else ""
                val v3 =
                  if (value.json().length > 0) "\"value\"" + ":" + value.json()
                  else ""
                val v4 =
                  if (templateitemname.json().length > 0)
                    "\"templateitemname\"" + ":" + templateitemname.json()
                  else ""
                val vars: List[String] = List(v0, v1, v2, v3, v4)
                val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
                if (cleanVars.length > 0) {
                  // values set
                  "{" + cleanVars + "}"
                } else {
                  // no values set
                  ""
                }
              }

              def fromJson(in: Value): this.type = {
                /* §#
                 * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
                 * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                def err =
                  throw new RuntimeException(s"Expected an Obj but got $in")
                in.objOpt.fold(err) { mapKV =>
                  val in0 = mapKV.get("dtickrange")
                  in0.map(ve => dtickrange.fromJson(ve))

                  val in1 = mapKV.get("name")
                  in1.map(ve => name.fromJson(ve))

                  val in2 = mapKV.get("enabled")
                  in2.map(ve => enabled.fromJson(ve))

                  val in3 = mapKV.get("value")
                  in3.map(ve => value.fromJson(ve))

                  val in4 = mapKV.get("templateitemname")
                  in4.map(ve => templateitemname.fromJson(ve))

                }
                this
              }

            }

            def json(): String = {
              /* §#
               * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/

              val v0 =
                if (tickformatstop.json().length > 0)
                  "\"tickformatstop\"" + ":" + tickformatstop.json()
                else ""
              val vars: List[String] = List(v0)
              val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
              if (cleanVars.length > 0) {
                // values set
                "{" + cleanVars + "}"
              } else {
                // no values set
                ""
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err =
                throw new RuntimeException(s"Expected an Obj but got $in")
              in.objOpt.fold(err) { mapKV =>
                val in0 = mapKV.get("tickformatstop")
                in0.map(ve => tickformatstop.fromJson(ve))

              }
              this
            }

          }

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (items.json().length > 0) "\"items\"" + ":" + items.json()
              else ""
            val vars: List[String] = List(v0)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("items")
              in0.map(ve => items.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (tickfont.json().length > 0)
              "\"tickfont\"" + ":" + tickfont.json()
            else ""
          val v1 =
            if (title.json().length > 0) "\"title\"" + ":" + title.json()
            else ""
          val v2 =
            if (tickformatstops.json().length > 0)
              "\"tickformatstops\"" + ":" + tickformatstops.json()
            else ""
          val v3 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
          val v4 =
            if (tick0.json().length > 0) "\"tick0\"" + ":" + tick0.json()
            else ""
          val v5 =
            if (nticks.json().length > 0) "\"nticks\"" + ":" + nticks.json()
            else ""
          val v6 =
            if (tickangle.json().length > 0)
              "\"tickangle\"" + ":" + tickangle.json()
            else ""
          val v7 =
            if (outlinewidth.json().length > 0)
              "\"outlinewidth\"" + ":" + outlinewidth.json()
            else ""
          val v8 =
            if (bgcolor.json().length > 0) "\"bgcolor\"" + ":" + bgcolor.json()
            else ""
          val v9 =
            if (showexponent.json().length > 0)
              "\"showexponent\"" + ":" + showexponent.json()
            else ""
          val v10 =
            if (ypad.json().length > 0) "\"ypad\"" + ":" + ypad.json() else ""
          val v11 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
          val v12 =
            if (yanchor.json().length > 0) "\"yanchor\"" + ":" + yanchor.json()
            else ""
          val v13 =
            if (ticksuffix.json().length > 0)
              "\"ticksuffix\"" + ":" + ticksuffix.json()
            else ""
          val v14 =
            if (tickwidth.json().length > 0)
              "\"tickwidth\"" + ":" + tickwidth.json()
            else ""
          val v15 =
            if (tickvals.json().length > 0)
              "\"tickvals\"" + ":" + tickvals.json()
            else ""
          val v16 =
            if (bordercolor.json().length > 0)
              "\"bordercolor\"" + ":" + bordercolor.json()
            else ""
          val v17 =
            if (thickness.json().length > 0)
              "\"thickness\"" + ":" + thickness.json()
            else ""
          val v18 =
            if (ticklen.json().length > 0) "\"ticklen\"" + ":" + ticklen.json()
            else ""
          val v19 =
            if (tickmode.json().length > 0)
              "\"tickmode\"" + ":" + tickmode.json()
            else ""
          val v20 =
            if (dtick.json().length > 0) "\"dtick\"" + ":" + dtick.json()
            else ""
          val v21 =
            if (lenmode.json().length > 0) "\"lenmode\"" + ":" + lenmode.json()
            else ""
          val v22 =
            if (tickvalssrc.json().length > 0)
              "\"tickvalssrc\"" + ":" + tickvalssrc.json()
            else ""
          val v23 =
            if (showticksuffix.json().length > 0)
              "\"showticksuffix\"" + ":" + showticksuffix.json()
            else ""
          val v24 =
            if (xpad.json().length > 0) "\"xpad\"" + ":" + xpad.json() else ""
          val v25 =
            if (ticktext.json().length > 0)
              "\"ticktext\"" + ":" + ticktext.json()
            else ""
          val v26 =
            if (ticks.json().length > 0) "\"ticks\"" + ":" + ticks.json()
            else ""
          val v27 =
            if (tickcolor.json().length > 0)
              "\"tickcolor\"" + ":" + tickcolor.json()
            else ""
          val v28 =
            if (ticktextsrc.json().length > 0)
              "\"ticktextsrc\"" + ":" + ticktextsrc.json()
            else ""
          val v29 =
            if (outlinecolor.json().length > 0)
              "\"outlinecolor\"" + ":" + outlinecolor.json()
            else ""
          val v30 =
            if (exponentformat.json().length > 0)
              "\"exponentformat\"" + ":" + exponentformat.json()
            else ""
          val v31 =
            if (showtickprefix.json().length > 0)
              "\"showtickprefix\"" + ":" + showtickprefix.json()
            else ""
          val v32 =
            if (tickprefix.json().length > 0)
              "\"tickprefix\"" + ":" + tickprefix.json()
            else ""
          val v33 =
            if (xanchor.json().length > 0) "\"xanchor\"" + ":" + xanchor.json()
            else ""
          val v34 =
            if (thicknessmode.json().length > 0)
              "\"thicknessmode\"" + ":" + thicknessmode.json()
            else ""
          val v35 =
            if (separatethousands.json().length > 0)
              "\"separatethousands\"" + ":" + separatethousands.json()
            else ""
          val v36 =
            if (tickformat.json().length > 0)
              "\"tickformat\"" + ":" + tickformat.json()
            else ""
          val v37 =
            if (showticklabels.json().length > 0)
              "\"showticklabels\"" + ":" + showticklabels.json()
            else ""
          val v38 =
            if (len.json().length > 0) "\"len\"" + ":" + len.json() else ""
          val v39 =
            if (borderwidth.json().length > 0)
              "\"borderwidth\"" + ":" + borderwidth.json()
            else ""
          val vars: List[String] = List(
            v0,
            v1,
            v2,
            v3,
            v4,
            v5,
            v6,
            v7,
            v8,
            v9,
            v10,
            v11,
            v12,
            v13,
            v14,
            v15,
            v16,
            v17,
            v18,
            v19,
            v20,
            v21,
            v22,
            v23,
            v24,
            v25,
            v26,
            v27,
            v28,
            v29,
            v30,
            v31,
            v32,
            v33,
            v34,
            v35,
            v36,
            v37,
            v38,
            v39
          )
          val cleanVars = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("tickfont")
            in0.map(ve => tickfont.fromJson(ve))

            val in1 = mapKV.get("title")
            in1.map(ve => title.fromJson(ve))

            val in2 = mapKV.get("tickformatstops")
            in2.map(ve => tickformatstops.fromJson(ve))

            val in3 = mapKV.get("x")
            in3.map(ve => x.fromJson(ve))

            val in4 = mapKV.get("tick0")
            in4.map(ve => tick0.fromJson(ve))

            val in5 = mapKV.get("nticks")
            in5.map(ve => nticks.fromJson(ve))

            val in6 = mapKV.get("tickangle")
            in6.map(ve => tickangle.fromJson(ve))

            val in7 = mapKV.get("outlinewidth")
            in7.map(ve => outlinewidth.fromJson(ve))

            val in8 = mapKV.get("bgcolor")
            in8.map(ve => bgcolor.fromJson(ve))

            val in9 = mapKV.get("showexponent")
            in9.map(ve => showexponent.fromJson(ve))

            val in10 = mapKV.get("ypad")
            in10.map(ve => ypad.fromJson(ve))

            val in11 = mapKV.get("y")
            in11.map(ve => y.fromJson(ve))

            val in12 = mapKV.get("yanchor")
            in12.map(ve => yanchor.fromJson(ve))

            val in13 = mapKV.get("ticksuffix")
            in13.map(ve => ticksuffix.fromJson(ve))

            val in14 = mapKV.get("tickwidth")
            in14.map(ve => tickwidth.fromJson(ve))

            val in15 = mapKV.get("tickvals")
            in15.map(ve => tickvals.fromJson(ve))

            val in16 = mapKV.get("bordercolor")
            in16.map(ve => bordercolor.fromJson(ve))

            val in17 = mapKV.get("thickness")
            in17.map(ve => thickness.fromJson(ve))

            val in18 = mapKV.get("ticklen")
            in18.map(ve => ticklen.fromJson(ve))

            val in19 = mapKV.get("tickmode")
            in19.map(ve => tickmode.fromJson(ve))

            val in20 = mapKV.get("dtick")
            in20.map(ve => dtick.fromJson(ve))

            val in21 = mapKV.get("lenmode")
            in21.map(ve => lenmode.fromJson(ve))

            val in22 = mapKV.get("tickvalssrc")
            in22.map(ve => tickvalssrc.fromJson(ve))

            val in23 = mapKV.get("showticksuffix")
            in23.map(ve => showticksuffix.fromJson(ve))

            val in24 = mapKV.get("xpad")
            in24.map(ve => xpad.fromJson(ve))

            val in25 = mapKV.get("ticktext")
            in25.map(ve => ticktext.fromJson(ve))

            val in26 = mapKV.get("ticks")
            in26.map(ve => ticks.fromJson(ve))

            val in27 = mapKV.get("tickcolor")
            in27.map(ve => tickcolor.fromJson(ve))

            val in28 = mapKV.get("ticktextsrc")
            in28.map(ve => ticktextsrc.fromJson(ve))

            val in29 = mapKV.get("outlinecolor")
            in29.map(ve => outlinecolor.fromJson(ve))

            val in30 = mapKV.get("exponentformat")
            in30.map(ve => exponentformat.fromJson(ve))

            val in31 = mapKV.get("showtickprefix")
            in31.map(ve => showtickprefix.fromJson(ve))

            val in32 = mapKV.get("tickprefix")
            in32.map(ve => tickprefix.fromJson(ve))

            val in33 = mapKV.get("xanchor")
            in33.map(ve => xanchor.fromJson(ve))

            val in34 = mapKV.get("thicknessmode")
            in34.map(ve => thicknessmode.fromJson(ve))

            val in35 = mapKV.get("separatethousands")
            in35.map(ve => separatethousands.fromJson(ve))

            val in36 = mapKV.get("tickformat")
            in36.map(ve => tickformat.fromJson(ve))

            val in37 = mapKV.get("showticklabels")
            in37.map(ve => showticklabels.fromJson(ve))

            val in38 = mapKV.get("len")
            in38.map(ve => len.fromJson(ve))

            val in39 = mapKV.get("borderwidth")
            in39.map(ve => borderwidth.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (colorbar.json().length > 0) "\"colorbar\"" + ":" + colorbar.json()
          else ""
        val v1 =
          if (cmax.json().length > 0) "\"cmax\"" + ":" + cmax.json() else ""
        val v2 =
          if (cmin.json().length > 0) "\"cmin\"" + ":" + cmin.json() else ""
        val v3 =
          if (colorsrc.json().length > 0) "\"colorsrc\"" + ":" + colorsrc.json()
          else ""
        val v4 =
          if (autocolorscale.json().length > 0)
            "\"autocolorscale\"" + ":" + autocolorscale.json()
          else ""
        val v5 =
          if (reversescale.json().length > 0)
            "\"reversescale\"" + ":" + reversescale.json()
          else ""
        val v6 =
          if (showscale.json().length > 0)
            "\"showscale\"" + ":" + showscale.json()
          else ""
        val v7 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v8 =
          if (coloraxis.json().length > 0)
            "\"coloraxis\"" + ":" + coloraxis.json()
          else ""
        val v9 =
          if (cmid.json().length > 0) "\"cmid\"" + ":" + cmid.json() else ""
        val v10 =
          if (colorscale.json().length > 0)
            "\"colorscale\"" + ":" + colorscale.json()
          else ""
        val v11 =
          if (cauto.json().length > 0) "\"cauto\"" + ":" + cauto.json() else ""
        val vars: List[String] =
          List(v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11)
        val cleanVars = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("colorbar")
          in0.map(ve => colorbar.fromJson(ve))

          val in1 = mapKV.get("cmax")
          in1.map(ve => cmax.fromJson(ve))

          val in2 = mapKV.get("cmin")
          in2.map(ve => cmin.fromJson(ve))

          val in3 = mapKV.get("colorsrc")
          in3.map(ve => colorsrc.fromJson(ve))

          val in4 = mapKV.get("autocolorscale")
          in4.map(ve => autocolorscale.fromJson(ve))

          val in5 = mapKV.get("reversescale")
          in5.map(ve => reversescale.fromJson(ve))

          val in6 = mapKV.get("showscale")
          in6.map(ve => showscale.fromJson(ve))

          val in7 = mapKV.get("color")
          in7.map(ve => color.fromJson(ve))

          val in8 = mapKV.get("coloraxis")
          in8.map(ve => coloraxis.fromJson(ve))

          val in9 = mapKV.get("cmid")
          in9.map(ve => cmid.fromJson(ve))

          val in10 = mapKV.get("colorscale")
          in10.map(ve => colorscale.fromJson(ve))

          val in11 = mapKV.get("cauto")
          in11.map(ve => cauto.fromJson(ve))

        }
        this
      }

    }

    case object domain extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """plot"""

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val column: PInt = PInt(
        min = Some(0),
        max = None,
        dflt = Some(0),
        description = Some(
          """If there is a layout grid, use the domain for this column in the grid for this parcoords trace ."""
        )
      )

      object y extends JSON {
        /* §#
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val description: Option[String] = Some(
          """Sets the vertical domain of this parcoords trace (in plot fraction)."""
        )

        private var int: Array[Int]                 = Array.fill(2)(0)
        private var double: Array[Double]           = Array.fill(2)(0.0)
        private var selected: Map[Int, OfPrimitive] = Map()
        private val n: Integer                      = 2

        def json(): String = {
          /* §#
           * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val nulls = "null"
          val values = (0 until n) map { i =>
            val t = selected.get(i)
            t.fold(nulls)({
              case NAPrimitive =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TString(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TBoolean(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TDouble(_) => double(i).toString
              case TInt(_)    => int(i).toString
              case TImpliedEdits(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TAttrRegexps(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
            })
          }
          val allNull = values.forall(_.toLowerCase.equals(nulls))
          if (allNull)
            ""
          else {
            values.filter(_.length > 0).mkString("[", ",", "]")
          }
        }

        override def fromJson(in: Value): this.type = {
          /* §#
           * generateInfoArrayFromJson()
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          in match {
            case Arr(values) =>
              values.zipWithIndex.foreach { kv =>
                val v = kv._1
                val i = kv._2
                v match {

                  case Num(v) =>
                    if (v.isValidInt) {
                      int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                    } else { double(i) = v; selected += (i -> TDouble(v)) }

                  case Null => () // nulls are valid
                  case _    => err(in)
                }
              }
              this
            case _ =>
              err(in)
              this
          }
        }

// Set the value
        def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
        def int_0(): Unit = { selected -= 0 }

// Set the value
        def double_0(v: Double): Unit = {
          double(0) = v; selected += (0 -> TDouble(v))
        }
// Delete the value (use default)
        def double_0(): Unit = { selected -= 0 }

// Set the value
        def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
        def int_1(): Unit = { selected -= 1 }

// Set the value
        def double_1(v: Double): Unit = {
          double(1) = v; selected += (1 -> TDouble(v))
        }
// Delete the value (use default)
        def double_1(): Unit = { selected -= 1 }

      }

      object x extends JSON {
        /* §#
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val description: Option[String] = Some(
          """Sets the horizontal domain of this parcoords trace (in plot fraction)."""
        )

        private var int: Array[Int]                 = Array.fill(2)(0)
        private var double: Array[Double]           = Array.fill(2)(0.0)
        private var selected: Map[Int, OfPrimitive] = Map()
        private val n: Integer                      = 2

        def json(): String = {
          /* §#
           * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val nulls = "null"
          val values = (0 until n) map { i =>
            val t = selected.get(i)
            t.fold(nulls)({
              case NAPrimitive =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TString(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TBoolean(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TDouble(_) => double(i).toString
              case TInt(_)    => int(i).toString
              case TImpliedEdits(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TAttrRegexps(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
            })
          }
          val allNull = values.forall(_.toLowerCase.equals(nulls))
          if (allNull)
            ""
          else {
            values.filter(_.length > 0).mkString("[", ",", "]")
          }
        }

        override def fromJson(in: Value): this.type = {
          /* §#
           * generateInfoArrayFromJson()
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          in match {
            case Arr(values) =>
              values.zipWithIndex.foreach { kv =>
                val v = kv._1
                val i = kv._2
                v match {

                  case Num(v) =>
                    if (v.isValidInt) {
                      int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                    } else { double(i) = v; selected += (i -> TDouble(v)) }

                  case Null => () // nulls are valid
                  case _    => err(in)
                }
              }
              this
            case _ =>
              err(in)
              this
          }
        }

// Set the value
        def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
        def int_0(): Unit = { selected -= 0 }

// Set the value
        def double_0(v: Double): Unit = {
          double(0) = v; selected += (0 -> TDouble(v))
        }
// Delete the value (use default)
        def double_0(): Unit = { selected -= 0 }

// Set the value
        def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
        def int_1(): Unit = { selected -= 1 }

// Set the value
        def double_1(v: Double): Unit = {
          double(1) = v; selected += (1 -> TDouble(v))
        }
// Delete the value (use default)
        def double_1(): Unit = { selected -= 1 }

      }

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val row: PInt = PInt(
        min = Some(0),
        max = None,
        dflt = Some(0),
        description = Some(
          """If there is a layout grid, use the domain for this row in the grid for this parcoords trace ."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (column.json().length > 0) "\"column\"" + ":" + column.json()
          else ""
        val v1                 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
        val v2                 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
        val v3                 = if (row.json().length > 0) "\"row\"" + ":" + row.json() else ""
        val vars: List[String] = List(v0, v1, v2, v3)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("column")
          in0.map(ve => column.fromJson(ve))

          val in1 = mapKV.get("y")
          in1.map(ve => y.fromJson(ve))

          val in2 = mapKV.get("x")
          in2.map(ve => x.fromJson(ve))

          val in3 = mapKV.get("row")
          in3.map(ve => row.fromJson(ve))

        }
        this
      }

    }

    case object stream extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val maxpoints: PReal = PReal(
        min = Some(0.0),
        max = Some(10000.0),
        dflt = Some("""500.0"""),
        description = Some(
          """Sets the maximum number of points to keep on the plots from an incoming stream. If `maxpoints` is set to *50*, only the newest 50 points will be displayed on the plot."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val token: PChars = PChars(
        dflt = None,
        description = Some(
          """The stream id number links a data trace on a plot with a stream. See https://plot.ly/settings for more details."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (maxpoints.json().length > 0)
            "\"maxpoints\"" + ":" + maxpoints.json()
          else ""
        val v1 =
          if (token.json().length > 0) "\"token\"" + ":" + token.json() else ""
        val vars: List[String] = List(v0, v1)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("maxpoints")
          in0.map(ve => maxpoints.fromJson(ve))

          val in1 = mapKV.get("token")
          in1.map(ve => token.fromJson(ve))

        }
        this
      }

    }

    case object dimensions extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""

      case object items extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        case object dimension extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""
          val description: String =
            """The dimensions (variables) of the parallel coordinates chart. 2..60 dimensions are supported."""
          val editType: String = """calc"""

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val name: PChars = PChars(
            dflt = None,
            description = Some(
              """When used in a template, named items are created in the output figure in addition to any items the figure already has in this array. You can modify these items in the output figure by making your own item with `templateitemname` matching this `name` alongside your modifications (including `visible: false` or `enabled: false` to hide it). Has no effect outside of a template."""
            )
          )

          /* §#
           * generateBooleanDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val visible: PBool = PBool(
            dflt = Some(true),
            description = Some(
              """Shows the dimension when set to `true` (the default). Hides the dimension for `false`."""
            )
          )

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val label: PChars = PChars(
            dflt = None,
            description = Some("""The shown name of the dimension.""")
          )

          /* §#
           * generateDataArrayDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val tickvals: DataArray = DataArray(
            dflt = None,
            description =
              Some("""Sets the values at which ticks on this axis appear.""")
          )

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val valuessrc: PChars = PChars(
            dflt = None,
            description =
              Some("""Sets the source reference on plot.ly for  values .""")
          )

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val tickvalssrc: PChars = PChars(
            dflt = None,
            description =
              Some("""Sets the source reference on plot.ly for  tickvals .""")
          )

          object range extends JSON {
            /* §#
             * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val description: Option[String] = Some(
              """The domain range that represents the full, shown axis extent. Defaults to the `values` extent. Must be an array of `[fromValue, toValue]` with finite numbers as elements."""
            )

            private var int: Array[Int]                 = Array.fill(2)(0)
            private var double: Array[Double]           = Array.fill(2)(0.0)
            private var selected: Map[Int, OfPrimitive] = Map()
            private val n: Integer                      = 2

            def json(): String = {
              /* §#
               * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
               * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val nulls = "null"
              val values = (0 until n) map { i =>
                val t = selected.get(i)
                t.fold(nulls)({
                  case NAPrimitive =>
                    throw new RuntimeException(s"Unexpected type: $t")
                  case TString(_) =>
                    throw new RuntimeException(s"Unexpected type: $t")
                  case TBoolean(_) =>
                    throw new RuntimeException(s"Unexpected type: $t")
                  case TDouble(_) => double(i).toString
                  case TInt(_)    => int(i).toString
                  case TImpliedEdits(_) =>
                    throw new RuntimeException(s"Unexpected type: $t")
                  case TAttrRegexps(_) =>
                    throw new RuntimeException(s"Unexpected type: $t")
                })
              }
              val allNull = values.forall(_.toLowerCase.equals(nulls))
              if (allNull)
                ""
              else {
                values.filter(_.length > 0).mkString("[", ",", "]")
              }
            }

            override def fromJson(in: Value): this.type = {
              /* §#
               * generateInfoArrayFromJson()
               * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err(j: ujson.Value): OfPrimitive =
                throw new RuntimeException(s"""Parsing of $j failed: $in""")
              in match {
                case Arr(values) =>
                  values.zipWithIndex.foreach { kv =>
                    val v = kv._1
                    val i = kv._2
                    v match {

                      case Num(v) =>
                        if (v.isValidInt) {
                          int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                        } else { double(i) = v; selected += (i -> TDouble(v)) }

                      case Null => () // nulls are valid
                      case _    => err(in)
                    }
                  }
                  this
                case _ =>
                  err(in)
                  this
              }
            }

// Set the value
            def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
            def int_0(): Unit = { selected -= 0 }

// Set the value
            def double_0(v: Double): Unit = {
              double(0) = v; selected += (0 -> TDouble(v))
            }
// Delete the value (use default)
            def double_0(): Unit = { selected -= 0 }

// Set the value
            def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
            def int_1(): Unit = { selected -= 1 }

// Set the value
            def double_1(v: Double): Unit = {
              double(1) = v; selected += (1 -> TDouble(v))
            }
// Delete the value (use default)
            def double_1(): Unit = { selected -= 1 }

          }

          /* §#
           * generateDataArrayDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val ticktext: DataArray = DataArray(
            dflt = None,
            description = Some(
              """Sets the text displayed at the ticks position via `tickvals`."""
            )
          )

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val ticktextsrc: PChars = PChars(
            dflt = None,
            description =
              Some("""Sets the source reference on plot.ly for  ticktext .""")
          )

          /* §#
           * generateDataArrayDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val values: DataArray = DataArray(
            dflt = None,
            description = Some(
              """Dimension values. `values[n]` represents the value of the `n`th point in the dataset, therefore the `values` vector for all dimensions must be the same (longer vectors will be truncated). Each value must be a finite number."""
            )
          )

          object constraintrange extends JSON {
            /* §#
             * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val description: Option[String] = Some(
              """The domain range to which the filter on the dimension is constrained. Must be an array of `[fromValue, toValue]` with `fromValue <= toValue`, or if `multiselect` is not disabled, you may give an array of arrays, where each inner array is `[fromValue, toValue]`."""
            )

            private var int: Array[Int]                 = Array.fill(2)(0)
            private var double: Array[Double]           = Array.fill(2)(0.0)
            private var selected: Map[Int, OfPrimitive] = Map()
            private val n: Integer                      = 2

            def json(): String = {
              /* §#
               * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
               * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val nulls = "null"
              val values = (0 until n) map { i =>
                val t = selected.get(i)
                t.fold(nulls)({
                  case NAPrimitive =>
                    throw new RuntimeException(s"Unexpected type: $t")
                  case TString(_) =>
                    throw new RuntimeException(s"Unexpected type: $t")
                  case TBoolean(_) =>
                    throw new RuntimeException(s"Unexpected type: $t")
                  case TDouble(_) => double(i).toString
                  case TInt(_)    => int(i).toString
                  case TImpliedEdits(_) =>
                    throw new RuntimeException(s"Unexpected type: $t")
                  case TAttrRegexps(_) =>
                    throw new RuntimeException(s"Unexpected type: $t")
                })
              }
              val allNull = values.forall(_.toLowerCase.equals(nulls))
              if (allNull)
                ""
              else {
                values.filter(_.length > 0).mkString("[", ",", "]")
              }
            }

            override def fromJson(in: Value): this.type = {
              /* §#
               * generateInfoArrayFromJson()
               * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err(j: ujson.Value): OfPrimitive =
                throw new RuntimeException(s"""Parsing of $j failed: $in""")
              in match {
                case Arr(values) =>
                  values.zipWithIndex.foreach { kv =>
                    val v = kv._1
                    val i = kv._2
                    v match {

                      case Num(v) =>
                        if (v.isValidInt) {
                          int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                        } else { double(i) = v; selected += (i -> TDouble(v)) }

                      case Null => () // nulls are valid
                      case _    => err(in)
                    }
                  }
                  this
                case _ =>
                  err(in)
                  this
              }
            }

// Set the value
            def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
            def int_0(): Unit = { selected -= 0 }

// Set the value
            def double_0(v: Double): Unit = {
              double(0) = v; selected += (0 -> TDouble(v))
            }
// Delete the value (use default)
            def double_0(): Unit = { selected -= 0 }

// Set the value
            def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
            def int_1(): Unit = { selected -= 1 }

// Set the value
            def double_1(v: Double): Unit = {
              double(1) = v; selected += (1 -> TDouble(v))
            }
// Delete the value (use default)
            def double_1(): Unit = { selected -= 1 }

          }

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val tickformat: PChars = PChars(
            dflt = Some(""""""),
            description = Some(
              """Sets the tick label formatting rule using d3 formatting mini-languages which are very similar to those in Python. For numbers, see: https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format And for dates see: https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format We add one item to d3's date formatter: *%{n}f* for fractional seconds with n digits. For example, *2016-10-13 09:15:23.456* with tickformat *%H~%M~%S.%2f* would display *09~15~23.46*"""
            )
          )

          /* §#
           * generateBooleanDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val multiselect: PBool = PBool(
            dflt = Some(true),
            description = Some(
              """Do we allow multiple selection ranges or just a single range?"""
            )
          )

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val templateitemname: PChars = PChars(
            dflt = None,
            description = Some(
              """Used to refer to a named item in this array in the template. Named items from the template will be created even without a matching item in the input figure, but you can modify one by making an item with `templateitemname` matching its `name`, alongside your modifications (including `visible: false` or `enabled: false` to hide it). If there is no template or no matching item, this item will be hidden unless you explicitly show it with `visible: true`."""
            )
          )

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (name.json().length > 0) "\"name\"" + ":" + name.json() else ""
            val v1 =
              if (visible.json().length > 0)
                "\"visible\"" + ":" + visible.json()
              else ""
            val v2 =
              if (label.json().length > 0) "\"label\"" + ":" + label.json()
              else ""
            val v3 =
              if (tickvals.json().length > 0)
                "\"tickvals\"" + ":" + tickvals.json()
              else ""
            val v4 =
              if (valuessrc.json().length > 0)
                "\"valuessrc\"" + ":" + valuessrc.json()
              else ""
            val v5 =
              if (tickvalssrc.json().length > 0)
                "\"tickvalssrc\"" + ":" + tickvalssrc.json()
              else ""
            val v6 =
              if (range.json().length > 0) "\"range\"" + ":" + range.json()
              else ""
            val v7 =
              if (ticktext.json().length > 0)
                "\"ticktext\"" + ":" + ticktext.json()
              else ""
            val v8 =
              if (ticktextsrc.json().length > 0)
                "\"ticktextsrc\"" + ":" + ticktextsrc.json()
              else ""
            val v9 =
              if (values.json().length > 0) "\"values\"" + ":" + values.json()
              else ""
            val v10 =
              if (constraintrange.json().length > 0)
                "\"constraintrange\"" + ":" + constraintrange.json()
              else ""
            val v11 =
              if (tickformat.json().length > 0)
                "\"tickformat\"" + ":" + tickformat.json()
              else ""
            val v12 =
              if (multiselect.json().length > 0)
                "\"multiselect\"" + ":" + multiselect.json()
              else ""
            val v13 =
              if (templateitemname.json().length > 0)
                "\"templateitemname\"" + ":" + templateitemname.json()
              else ""
            val vars: List[String] =
              List(v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13)
            val cleanVars = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("name")
              in0.map(ve => name.fromJson(ve))

              val in1 = mapKV.get("visible")
              in1.map(ve => visible.fromJson(ve))

              val in2 = mapKV.get("label")
              in2.map(ve => label.fromJson(ve))

              val in3 = mapKV.get("tickvals")
              in3.map(ve => tickvals.fromJson(ve))

              val in4 = mapKV.get("valuessrc")
              in4.map(ve => valuessrc.fromJson(ve))

              val in5 = mapKV.get("tickvalssrc")
              in5.map(ve => tickvalssrc.fromJson(ve))

              val in6 = mapKV.get("range")
              in6.map(ve => range.fromJson(ve))

              val in7 = mapKV.get("ticktext")
              in7.map(ve => ticktext.fromJson(ve))

              val in8 = mapKV.get("ticktextsrc")
              in8.map(ve => ticktextsrc.fromJson(ve))

              val in9 = mapKV.get("values")
              in9.map(ve => values.fromJson(ve))

              val in10 = mapKV.get("constraintrange")
              in10.map(ve => constraintrange.fromJson(ve))

              val in11 = mapKV.get("tickformat")
              in11.map(ve => tickformat.fromJson(ve))

              val in12 = mapKV.get("multiselect")
              in12.map(ve => multiselect.fromJson(ve))

              val in13 = mapKV.get("templateitemname")
              in13.map(ve => templateitemname.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (dimension.json().length > 0)
              "\"dimension\"" + ":" + dimension.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("dimension")
            in0.map(ve => dimension.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (items.json().length > 0) "\"items\"" + ":" + items.json() else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("items")
          in0.map(ve => items.fromJson(ve))

        }
        this
      }

    }

    case object rangefont extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""
      val description: String =
        """Sets the font for the `dimension` range values."""
      val editType: String = """plot"""

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = None
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val family: PChars = PChars(
        dflt = None,
        description = Some(
          """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal =
        PReal(min = Some(1.0), max = None, dflt = None, description = None)

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v1 =
          if (family.json().length > 0) "\"family\"" + ":" + family.json()
          else ""
        val v2 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val vars: List[String] = List(v0, v1, v2)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("color")
          in0.map(ve => color.fromJson(ve))

          val in1 = mapKV.get("family")
          in1.map(ve => family.fromJson(ve))

          val in2 = mapKV.get("size")
          in2.map(ve => size.fromJson(ve))

        }
        this
      }

    }

    case object labelfont extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String        = """object"""
      val description: String = """Sets the font for the `dimension` labels."""
      val editType: String    = """plot"""

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = None
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val family: PChars = PChars(
        dflt = None,
        description = Some(
          """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal =
        PReal(min = Some(1.0), max = None, dflt = None, description = None)

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v1 =
          if (family.json().length > 0) "\"family\"" + ":" + family.json()
          else ""
        val v2 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val vars: List[String] = List(v0, v1, v2)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("color")
          in0.map(ve => color.fromJson(ve))

          val in1 = mapKV.get("family")
          in1.map(ve => family.fromJson(ve))

          val in2 = mapKV.get("size")
          in2.map(ve => size.fromJson(ve))

        }
        this
      }

    }

    case object transforms extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""

      case object items extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        case object transform extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""
          val description: String =
            """An array of operations that manipulate the trace data, for example filtering or sorting the data arrays."""
          val editType: String = """calc"""

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val vars: List[String] = List()
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV => }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (transform.json().length > 0)
              "\"transform\"" + ":" + transform.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("transform")
            in0.map(ve => transform.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (items.json().length > 0) "\"items\"" + ":" + items.json() else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("items")
          in0.map(ve => items.fromJson(ve))

        }
        this
      }

    }

    case object tickfont extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""
      val description: String =
        """Sets the font for the `dimension` tick values."""
      val editType: String = """plot"""

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = None
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val family: PChars = PChars(
        dflt = None,
        description = Some(
          """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal =
        PReal(min = Some(1.0), max = None, dflt = None, description = None)

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v1 =
          if (family.json().length > 0) "\"family\"" + ":" + family.json()
          else ""
        val v2 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val vars: List[String] = List(v0, v1, v2)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("color")
          in0.map(ve => color.fromJson(ve))

          val in1 = mapKV.get("family")
          in1.map(ve => family.fromJson(ve))

          val in2 = mapKV.get("size")
          in2.map(ve => size.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val v20 = "\"type\" : \"parcoords\""
      val v0 =
        if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
      val v1 =
        if (domain.json().length > 0) "\"domain\"" + ":" + domain.json() else ""
      val v2 =
        if (stream.json().length > 0) "\"stream\"" + ":" + stream.json() else ""
      val v3 =
        if (dimensions.json().length > 0)
          "\"dimensions\"" + ":" + dimensions.json()
        else ""
      val v4 =
        if (rangefont.json().length > 0)
          "\"rangefont\"" + ":" + rangefont.json()
        else ""
      val v5 =
        if (labelfont.json().length > 0)
          "\"labelfont\"" + ":" + labelfont.json()
        else ""
      val v6 =
        if (transforms.json().length > 0)
          "\"transforms\"" + ":" + transforms.json()
        else ""
      val v7 =
        if (tickfont.json().length > 0) "\"tickfont\"" + ":" + tickfont.json()
        else ""
      val v8 =
        if (name.json().length > 0) "\"name\"" + ":" + name.json() else ""
      val v9 =
        if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
        else ""
      val v10 =
        if (labelside.json().length > 0)
          "\"labelside\"" + ":" + labelside.json()
        else ""
      val v11 =
        if (uirevision.json().length > 0)
          "\"uirevision\"" + ":" + uirevision.json()
        else ""
      val v12 =
        if (idssrc.json().length > 0) "\"idssrc\"" + ":" + idssrc.json() else ""
      val v13 =
        if (meta.json().length > 0) "\"meta\"" + ":" + meta.json() else ""
      val v14 =
        if (labelangle.json().length > 0)
          "\"labelangle\"" + ":" + labelangle.json()
        else ""
      val v15 = if (uid.json().length > 0) "\"uid\"" + ":" + uid.json() else ""
      val v16 = if (ids.json().length > 0) "\"ids\"" + ":" + ids.json() else ""
      val v17 =
        if (metasrc.json().length > 0) "\"metasrc\"" + ":" + metasrc.json()
        else ""
      val v18 =
        if (customdata.json().length > 0)
          "\"customdata\"" + ":" + customdata.json()
        else ""
      val v19 =
        if (customdatasrc.json().length > 0)
          "\"customdatasrc\"" + ":" + customdatasrc.json()
        else ""
      val vars: List[String] = List(
        v20,
        v0,
        v1,
        v2,
        v3,
        v4,
        v5,
        v6,
        v7,
        v8,
        v9,
        v10,
        v11,
        v12,
        v13,
        v14,
        v15,
        v16,
        v17,
        v18,
        v19
      )
      val cleanVars = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("line")
        in0.map(ve => line.fromJson(ve))

        val in1 = mapKV.get("domain")
        in1.map(ve => domain.fromJson(ve))

        val in2 = mapKV.get("stream")
        in2.map(ve => stream.fromJson(ve))

        val in3 = mapKV.get("dimensions")
        in3.map(ve => dimensions.fromJson(ve))

        val in4 = mapKV.get("rangefont")
        in4.map(ve => rangefont.fromJson(ve))

        val in5 = mapKV.get("labelfont")
        in5.map(ve => labelfont.fromJson(ve))

        val in6 = mapKV.get("transforms")
        in6.map(ve => transforms.fromJson(ve))

        val in7 = mapKV.get("tickfont")
        in7.map(ve => tickfont.fromJson(ve))

        val in8 = mapKV.get("name")
        in8.map(ve => name.fromJson(ve))

        val in9 = mapKV.get("visible")
        in9.map(ve => visible.fromJson(ve))

        val in10 = mapKV.get("labelside")
        in10.map(ve => labelside.fromJson(ve))

        val in11 = mapKV.get("uirevision")
        in11.map(ve => uirevision.fromJson(ve))

        val in12 = mapKV.get("idssrc")
        in12.map(ve => idssrc.fromJson(ve))

        val in13 = mapKV.get("meta")
        in13.map(ve => meta.fromJson(ve))

        val in14 = mapKV.get("labelangle")
        in14.map(ve => labelangle.fromJson(ve))

        val in15 = mapKV.get("uid")
        in15.map(ve => uid.fromJson(ve))

        val in16 = mapKV.get("ids")
        in16.map(ve => ids.fromJson(ve))

        val in17 = mapKV.get("metasrc")
        in17.map(ve => metasrc.fromJson(ve))

        val in18 = mapKV.get("customdata")
        in18.map(ve => customdata.fromJson(ve))

        val in19 = mapKV.get("customdatasrc")
        in19.map(ve => customdatasrc.fromJson(ve))

      }
      this
    }

  }

  object layoutAttributes extends JSON {
    def json(): String                 = ""
    def fromJson(in: Value): this.type = this
  }

}
