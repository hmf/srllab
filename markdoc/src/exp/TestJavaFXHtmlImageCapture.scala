package exp

import java.awt.image.BufferedImage
import java.util.concurrent.locks.ReentrantLock
import java.util.{Timer, TimerTask}

import javafx.animation.{KeyFrame, Timeline}
import javafx.application.{Application, Platform}
import javafx.concurrent.Worker
import javafx.embed.swing.SwingFXUtils
import javafx.event.ActionEvent
import javafx.scene.Scene
import javafx.scene.image.WritableImage
import javafx.scene.layout.VBox
import javafx.scene.web.WebView
import javafx.stage.Stage
import javafx.util.Duration
import javax.imageio.ImageIO
import webkit.Utils

// https://medium.com/@danielnenkov/run-testfx-tests-on-jenkins-running-on-ec2-d68197e10e2b
// https://github.com/TestFX/TestFX
// https://github.com/TestFX/Monocle
// https://mvnrepository.com/artifact/org.testfx/openjfx-monocle/jdk-11+26
// https://wiki.openjdk.java.net/display/OpenJFX/Monocle
// https://wiki.openjdk.java.net/display/OpenJFX/Building+OpenJFX
// https://wiki.openjdk.java.net/display/OpenJFX/Building+the+OpenJFX+embedded+stack+for+Linux+desktop

// http://nm.mathforcollege.com
// https://www.youtube.com/watch?v=83x0gtaJgq0 @4:38

// https://github.com/GSI-CS-CO/chart-fx/issues/6
// https://github.com/GSI-CS-CO/chart-fx#working-on-the-source
// Add Monocle modules

/*
/home/hmf/.cache/coursier/v1/https/oss.sonatype.org/content/repositories/releases/org/openjfx/javafx-monocle
/home/hmf/.cache/coursier/v1/https/oss.sonatype.org/content/repositories/releases/org/openjfx/javafx-monocle/12.0.2
/home/hmf/.cache/coursier/v1/https/oss.sonatype.org/content/repositories/releases/org/testfx/openjfx-monocle
/home/hmf/.cache/coursier/v1/https/oss.sonatype.org/content/repositories/releases/org/testfx/openjfx-monocle/jdk-11%2B2600
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/openjfx/javafx-monocle
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/openjfx/javafx-monocle/12.0.2
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B26
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B2600
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B26/.openjfx-monocle-jdk-11%2B26.jar.checked
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B26/.openjfx-monocle-jdk-11%2B26.jar.sha1.checked
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B26/.openjfx-monocle-jdk-11%2B26.pom.checked
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B26/.openjfx-monocle-jdk-11%2B26.pom.sha1.checked
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B26/openjfx-monocle-jdk-11%2B26.jar
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B26/openjfx-monocle-jdk-11%2B26.jar.sha1
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B26/openjfx-monocle-jdk-11%2B26.pom
/home/hmf/.cache/coursier/v1/https/repo1.maven.org/maven2/org/testfx/openjfx-monocle/jdk-11%2B26/openjfx-monocle-jdk-11%2B26.pom.sha1
/home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/TestHeadlessMonocle.scala
/home/hmf/IdeaProjects/srllab/out/mdocs/compile/dest/classes/mdocs/TestHeadlessMonocle$.class
/home/hmf/IdeaProjects/srllab/out/mdocs/compile/dest/classes/mdocs/TestHeadlessMonocle.class

 */

// https://github.com/TestFX/Monocle/issues/50
// https://github.com/TestFX/Monocle/issues/61
// "Automatic-Module-Name": "org.testfx.monocle",
//https://github.com/TestFX/TestFX/issues/553
// patch-module
// https://github.com/brcolow/TestFX/blob/386ac7342f164944b0ca8162c4102ace2b05717b/subprojects/testfx-core/testfx-core.gradle#L75
// https://github.com/TestFX/TestFX/blob/master/subprojects/testfx-core/testfx-core.gradle#L166


/**
 *
 * mill mill.scalalib.GenIdea/idea
 *
 * mill mdocs.compile
 * mill mdocs.run
 * mill mdocs.run noGraal
 *
 * mill mdocs.{compile, run}
 * mill --watch mdocs.run
 *
 * mill -i mdocs.console
 * mill -i mdocs.repl
 *
 * mill -i mdocs.runMain mdocs.TestJavaFXHtmlImageCapture
 * mill --watch mill -i mdocs.runMain mdocs.TestJavaFXHtmlImageCapture
 */
object TestJavaFXHtmlImageCapture {

  /**
   * This method takes a snapshot image an saves it to a file. JavaFX/OpenFX
   * supports the following output formats: JPEG, PNG, GIF, BMP and WBMP
   * Additional formats may be available as plugins.
   *
   * @param image an image containing a snapshot
   * @return true if an image was successfully saved otherwise false
   */
  def saveSnapshot(image:BufferedImage): Boolean = {
    // Only as PNG
    val property = "java.io.tmpdir"
    val tempDir = System.getProperty(property)
    // JPEG, PNG, GIF, BMP and WBMP
    val file1 = Utils.temporaryFile(tempDir, "_testwebkit_1.png").toJava
    val r1 = ImageIO.write(image, "png", file1)
    println(s"$file1: $r1")

    // https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/imageio/package-summary.html
    // No JPEG
    // https://bugs.openjdk.java.net/browse/JDK-8204188
    // https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/imageio/package-use.html
    val file2 = Utils.temporaryFile(tempDir, "_testwebkit_1.tiff").toJava
    val r2 = ImageIO.write(image, "tiff", file2)
    println(s"$file2: $r2")

    val file3 = Utils.temporaryFile(tempDir, "_testwebkit_1.gif").toJava
    val r3 = ImageIO.write(image, "gif", file3)
    println(s"$file3: $r3")

    // BMP and WBMP do not work
    val file4 = Utils.temporaryFile(tempDir, "_testwebkit_1.bmp").toJava
    val r4 = ImageIO.write(image, "bmp", file4)
    println(s"$file4: $r4")

    val file5 = Utils.temporaryFile(tempDir, "_testwebkit_1.wbmp").toJava
    val r5 = ImageIO.write(image, "wbmp", file5)
    println(s"$file5: $r5")

    r1 && r2 && r3 && r4 && r5
    //r1 && r2 && r3
  }


  def main(args: Array[String]): Unit = {
    new JavaFXHtmlImageCapture().initialize()
    val image: BufferedImage = JavaFXHtmlImageCapture.captureHtml("""file:///home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test.html""")
    println(s"image.getWidth = ${image.getWidth}")
    println(s"image.getHeight = ${image.getHeight}")
    saveSnapshot(image)
    println()
    System.exit(0)
  }
}
