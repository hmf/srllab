package splotly

import ujson.Value

case class DataArray(dflt: Option[Array[Double]],
                     description: Option[String]
                ) extends JSON {

  private var value: Array[OfPrimitive] = Array()

  def setValue(data: Array[Double]): Unit = value = data.map(TDouble)
  def setValue(data: Array[String]): Unit = value = data.map(TString)
  def setValue(data: Array[Int]): Unit = value = data.map(TInt)
  def setDefault(): Unit = value = dflt.fold(Array[OfPrimitive]())(e => e.map( v => TString(v.toString)))
  def getValue: Array[OfPrimitive] = value

  override def json(): String = {
    if (value.length > 0) {
      value(0) match {
        case _:TDouble => value.map(OfType.stringValue).mkString("[",",","]")
        case _:TInt => value.map(OfType.stringValue).mkString("[",",","]")
        case _:TString => value.map(OfType.stringValue).mkString("[",",","]")
        case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected type ${value(0)}")
      }
    }
    else
      ""
  }

  override def fromJson(in: Value): DataArray.this.type = {
    in match {
      case ujson.Arr(v) =>
        val validNum = v.forall { case ujson.Num(_) => true; case _ => false }
        val validStr = v.forall { case ujson.Str(_) => true; case _ => false }
        if (validNum) {
          // ujson does not discern between floats and integers
          val validInt = v.forall { case ujson.Num(e) => e.isValidInt; case _ => false }
          if (validInt)
            setValue(v.toArray.map{ case ujson.Num(v) => v.toInt; case _ => Int.MinValue})
          else
            setValue(v.toArray.map{ case ujson.Num(v) => v; case _ => Double.NaN})
        } else if (validStr)
          setValue(v.toArray.map{ case ujson.Str(v) => v; case _ => ""})
        else
          throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
      case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
    }
    this
  }
}
