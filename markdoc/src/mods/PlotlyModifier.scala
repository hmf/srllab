package mods

import better.files._
import java.io.{File => JFile}
import java.nio.file.{Files, Path, Paths}
import java.util.UUID

import better.files.Dsl.cwd
import mdoc._

import scala.meta.inputs.Position
import plotly._
import layout._
import webkit.{HeadlessWebKit, Utils}

import scala.collection.immutable.ListMap

case class Div(caption:String = "", name:String=UUID.randomUUID.toString, height: String = "500px", width: String = "900px")
case class ScalaPlotly(traces:Seq[Trace],
                       layout: Layout,
                       dic: Div = Div())

object PlotlyModifier {

  // https://docs.oracle.com/javase/7/docs/api/java/util/ServiceLoader.html
  // https://docs.scala-lang.org/sips/improved-lazy-val-initialization.html
  // https://stackoverflow.com/questions/24221601/scala-object-initialization
  def initWebKit():Unit = {
    //java.awt.Toolkit.getDefaultToolkit.beep()
    HeadlessWebKit.launchBackground(Array[String]())
    //HeadlessWebKit.launchBackgroundNoLock(Array[String]())
  }
}

/**
 * mill mill.scalalib.GenIdea/idea
 *
 * mill mdocs.compile
 * mill mdocs.run
 * mill mdocs.{compile, run}
 * mill --watch mdocs.run
 *
 * mill -i mdocs.console
 * mill -i mdocs.repl
 *
 * mill -i sdk.laika
 * mill -i sdk.laikaLocal
 * mill -i sdk.mDoc
 * mill -i sdk.mDocLocal
 *
 */
class PlotlyModifier extends PostModifier {
  override val name: String = "plotly"

  private val ASSETS = "assets"

  /* TODO
  println(s"PlotlyModifier init")
  java.awt.Toolkit.getDefaultToolkit.beep()
  PlotlyModifier.initWebKit()
  java.awt.Toolkit.getDefaultToolkit.beep()
  println(s"PlotlyModifier init done")
  HeadlessWebKit.waitStart()
  println(s"PlotlyModifier started")
  HeadlessWebKit.stop()
  println(s"PlotlyModifier stopped")

  // TODO: we must only initialize jFX once. We also must stop the jFX app otherwise the
  // TODO: spawned MDoc process will not terminate.
  // TODO: How/when do we stop the jFX app? It must only be stopped at the end of the Mill/Mdoc task
  // TODO: can we have a PostModifier to init() and close() ?
*/

  def generateFrame(fileName: String, div: Div): String = {
    s"""
       |<div style="width:100%; height:100%; text-align:center">
       | <iframe id="${div.name}" src="$fileName" height="${div.height}" width="${div.width}"></iframe>
       | <p style="text-align:center; font-weight: bold;">
       |  ${div.caption}
       | </p>
       |</div>
       |""".stripMargin
  }

  def generateHTMLJSDiv(fileName: String, div: Div): String = {
    s"""
        |<div style="width:100%; height:100%;">
        |  <div id="${div.name}" style="margin:0 auto; height:${div.height}; width:${div.width}; border:1px solid black;">
        |   <!-- Plotly chart will be drawn inside this DIV -->
        |  </div>
        |  <script src="${fileName}"></script>
        |  <p style="text-align:center; font-weight: bold;">
        |    ${div.caption}
        |  </p>
        |</div>""".stripMargin
  }

  def generateJSDiv(script: String, div: Div) : String = {
    s"""
       |<div style="width:100%; height:100%; text-align:center;">
       |  <div id="${div.name}" style="margin:0 auto; height:${div.height}; width:${div.width}; border:1px solid black;">
       |    <!-- Plotly chart will be drawn inside this DIV --></div>
       |    <script type="text/javascript" defer>
       |      <!-- JAVASCRIPT CODE GOES HERE -->
       |      $script
       |    </script>
       |  <p style="text-align:center; font-weight: bold;">
       |     ${div.caption}
       |  </p>
       |</div>
       |""".stripMargin
  }

  trait Flag { def flag: String }
  private def contains[T <: Flag](flag: T)(s: String): Boolean = s.contains(flag.flag)
  private def makeContains[T <: Flag](flag: T): (T, String => Boolean) = (flag, contains(flag))
  private def removeFlag[T <: Flag](flag: T, str:String): String = str.replaceAll(flag.flag, "")
  private def removeAllFlag[T <: Flag,K](flags: Map[T, K], str:String): String = {
    flags.foldLeft(str){ case(acc,(f,_)) => removeFlag(f, acc) }
  }

  sealed trait RenderOp extends Flag
  final case object HTMLOp   extends RenderOp { val flag = ":html" }
  final case object HTMLJSOp extends RenderOp { val flag = ":hjs" }
  final case object JSDIVOp  extends RenderOp { val flag = ":js" }
  final case object PNGOp    extends RenderOp { val flag = ":png" }
  final case object GIFOp    extends RenderOp { val flag = ":giff" }
  final case object TIFFOp   extends RenderOp { val flag = ":tiff" }
  final case object TIFOp    extends RenderOp { val flag = ":tif" }
  final case object SVGOp    extends RenderOp { val flag = ":svg" }


  // or TreeSeqMap to retain order
  // Priority determined by order
  private val searchOpFlags: Map[RenderOp, String => Boolean] = ListMap(
    makeContains(HTMLOp),
    makeContains(HTMLJSOp),
    makeContains(JSDIVOp),
    makeContains(PNGOp),
    makeContains(GIFOp),
    makeContains(TIFFOp),
    makeContains(TIFOp),
    makeContains(SVGOp)
  )

  private def getActiveFlag[K](info: String, flags: Map[K,String => Boolean]): Option[(K, Boolean)] = {
    val activated = flags
      .map{ case (flag, test) => (flag, test(info)) }
      .filter( _._2 == true)
    activated.headOption
  }

  sealed trait ModFlag extends Flag
  final case object Debug       extends ModFlag { val flag = ":debug" }
  final case object Passthrough extends ModFlag { val flag = ":passthrough" }
  final case object NoMod       extends ModFlag { val flag = "" }

  // or TreeSeqMap to retain order
  // Priority determined by order
  private val searchModFlags: Map[ModFlag, String => Boolean] = ListMap(
    makeContains(Passthrough),
    makeContains(Debug)
  )

  private def getMod(info:String): (ModFlag, String) = {
    val activeFlag: Option[(ModFlag, Boolean)] = getActiveFlag(info, searchModFlags)
    val strippedInfo: String = removeAllFlag(searchModFlags, info)
    activeFlag.fold( (NoMod:ModFlag, info ))(e => (e._1, strippedInfo))
  }

  private def processMod(flag: ModFlag, ctx: PostModifierContext, transformedCode: String): String = {
    flag match {
      case NoMod =>
        transformedCode
      case Debug =>
        val block = s"""
                       |```
                       |scala mdoc:plotly:${ctx.info}
                       |$transformedCode
                       |```
                       |""".stripMargin
        block + transformedCode
      case Passthrough =>
        val block = s"""
                       |```
                       |scala mdoc:plotly:${ctx.info}
                       |${ctx.originalCode.text}
                       |```
                       |""".stripMargin
        block + transformedCode
    }
  }

  override def process(ctx: PostModifierContext): String = {
    ctx.reporter.info(s"PostModifier $name successfully called.")
    ctx.reporter.info(s"PostModifier ctx.info: ${ctx.info}")
    val (flag, fileExt) = getMod(ctx.info)
    val fileName = webkit.Utils.temporaryFilename(ASSETS) + "." + fileExt
    val relpath: Path = Paths.get(fileName)
    ctx.reporter.info(s"PostModifier $name relpath: $relpath")
    val out = ctx.outputFile.toNIO.getParent.resolve(relpath)
    ctx.reporter.info(s"PostModifier $name out: $out")
    ctx.reporter.info(s"ctx.lastValue = ${ctx.lastValue}")
    ctx.reporter.info(s"ctx.inDirectory = ${ctx.inDirectory}")
    ctx.reporter.info(s"ctx.relativePath = ${ctx.relativePath}")
    //ctx.reporter.info(s"ctx.originalCode.text = ${ctx.originalCode.text}")
    //val variables = ctx.variables.mkString(";\n")
    //ctx.reporter.info(s"ctx.variables = $variables")


    ctx.lastValue match {
      case ScalaPlotly(traces, layout, div) =>
        Files.createDirectories(out.getParent)
        if (!Files.isDirectory(out)) {
          val f: JFile = out.toFile
          val ext = Utils.fileExtension(f).trim.toLowerCase
          ext match {
            case "html" =>
              Plotly.plot(f.getAbsolutePath, traces, layout, openInBrowser = false)
              val frame = generateFrame(relpath.toString, div)
              processMod(flag, ctx, frame)

            case "hjs" =>
              val jsSnippet = Plotly.jsSnippet(div.name, traces, layout)
              val ff = File(out)
              //ctx.reporter.info(s"jsSnippet = $jsSnippet")
              //ff.overwrite(frame)(charset = UnicodeCharset("UTF-8", writeByteOrderMarkers = true)) // buffer error
              ff.overwrite(jsSnippet)(charset = UnicodeCharset("UTF-8"))
              val embeddedDiv = generateHTMLJSDiv(fileName.toString, div)
              processMod(flag, ctx, embeddedDiv)

            case "js" =>
              val jsSnippet = Plotly.jsSnippet(div.name, traces, layout)
              val embeddedDiv = generateJSDiv(jsSnippet, div)
              processMod(flag, ctx, embeddedDiv)

            case "jpeg" =>
              s"**NOT SUPPORTED: extension $ext no supported for $out**\n"

            case "png" =>
              //java.awt.Toolkit.getDefaultToolkit.beep()
              /*
              // We could generate the HTML ans use that, but the library does not have this option
              Plotly.plot(f.getAbsolutePath, traces, layout, openInBrowser = false)
              val imageOk = HeadlessWebKit.capture(s"""file://${f.getAbsolutePath}""", out)
              ctx.reporter.info(s"png imageOk = $imageOk file = ${f.getAbsolutePath}")
              // TODO: add debug
              if (imageOk)
                s"![${div.caption}](${ctx.info})"
              else
                s"**Failed to create file: extension $ext no supported for $out**\n"
               */
              s"**BUG: extension $ext in $out**\n"

            case "gif" =>
              s"![PostModified gif](${ctx.info})"
            case "tiff" =>
              s"![PostModified tiff](${ctx.info})"
            case "svg" =>
              s"![PostModified svg](${ctx.info})"
            case _ =>
              s"**PostModified unknown/unsupported file type**: ${ctx.info}"
          }
        }
        else {
          s"PostModified Error: not a valid file name - ${out.getFileName.toString}"
        }
      case _ =>
        val (pos, obtained) = ctx.variables.lastOption match {
          case Some(variable) =>
            val prettyObtained = s"${variable.staticType} = ${variable.runtimeValue}"
            (variable.pos, prettyObtained)
          case None =>
            (Position.Range(ctx.originalCode, 0, 0), "nothing")
        }
        ctx.reporter.error(
          pos,
          s"""type mismatch:
  expected: mdocs.ScalaPlotly
  obtained: $obtained"""
        )
        ""
    }
  }
}
