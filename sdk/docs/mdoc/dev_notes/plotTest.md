# Plotting Tests 


```scala mdoc:reset:silent
    import plotly._, layout._, Plotly._
    
    val labels = Seq("Banana", "Banano", "Grapefruit")
    val valuesA = labels.map(_ => util.Random.nextGaussian())
    val valuesB = labels.map(_ => 0.5 + util.Random.nextGaussian())
    
    val traces = Seq(
      Bar(labels, valuesA, name = "A"),
      Bar(labels, valuesB, name = "B")
    )
    val playout = Layout()
    val jsSnippet = Plotly.jsSnippet("sample2", traces, playout)
```



Embedding to an HTML file 

```scala mdoc:plotly:html
    import mdocs.ScalaPlotly
    import mdocs.Div

    ScalaPlotly(traces, playout, 
                 Div("sample5", 
                     "Figure 5: Embedded bar plot example via a Plotly-Scala generated script"))
```

Embedding to an HTML snippet with a JS file

```scala mdoc:plotly:hjs
    ScalaPlotly(traces, playout, 
                 Div("sample6", 
                     "Figure 6: Embedded bar plot example via a Plotly-Scala generated script"))
```

Embedding to an HTML snippet with JS code

```scala mdoc:plotly:js
    ScalaPlotly(traces, playout, 
                 Div("sample7", 
                     "Figure 7: Embedded bar plot example via a Plotly-Scala generated script"))
```

Linking to an PNG image

```scala mdoc:plotly:png
    ScalaPlotly(traces, playout, 
                 Div("sample8", 
                     "Figure 8: Embedded bar plot example via a Plotly-Scala generated script"))
```


Linking to an JPEG image

```scala mdoc:plotly:jpeg
    ScalaPlotly(traces, playout, 
                 Div("sample9", 
                     "Figure 9: Embedded bar plot example via a Plotly-Scala generated script"))
```

TODO: WebP


Linking to an SVG image

```scala mdoc:plotly:svg
    ScalaPlotly(traces, playout, 
                 Div("sample10", 
                     "Figure 10: Embedded bar plot example via a Plotly-Scala generated script"))
```

TODO: PDF
TODO: EPS


Manual example

<div style="width:100%; height:100%;">
    <div id="myDiv2"
         style="margin:0 auto; height:500px; width:900px; border:1px solid black;">
        <!-- Plotly chart will be drawn inside this DIV -->
    </div>
    <script src="scatterplotexample.js"></script>
    <p style="text-align:center; font-weight: bold;">
        Figure 2: Embedded scatter plot example via a script file
    </p>
</div>

End of document.

<div style="width:100%; height:100%;">
    <div id="myDiv3"
         style="margin:0 auto; height:500px; width:900px; border:1px solid black;">
        <!-- Plotly chart will be drawn inside this DIV -->
    </div>
    <script src="scatterplotexample.js"></script>
    <p style="text-align:center; font-weight: bold;">
        Figure 2: Embedded scatter plot example via a script file
    </p>
</div>

End of document 2. 

