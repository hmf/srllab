package webkit

import java.lang.reflect.{Constructor, Field}
import java.nio.file.Paths
import java.util.concurrent.locks.ReentrantLock
import java.util.{Timer, TimerTask}

import better.files._
import javafx.animation.{KeyFrame, Timeline}
import javafx.application.{Application, Platform}
import javafx.concurrent.Worker
import javafx.embed.swing.SwingFXUtils
import javafx.event.ActionEvent
import javafx.geometry.Rectangle2D
import javafx.scene.{Scene, SnapshotParameters}
import javafx.scene.image.WritableImage
import javafx.scene.layout.VBox
import javafx.scene.web.{WebEngine, WebView}
import javafx.stage.{Stage, StageStyle}
import javafx.util.Duration
import javax.imageio.ImageIO

/**
 * This class is required because I had issue launching the JavFX application.
 * It seems like the companion object does not provide a
 * `mdocs.HeadlessWebKit$.<init>()` method. I would get the exception:
 * `Exception in thread "Thread-1" java.lang.RuntimeException: Unable to construct Application instance: class mdocs.HeadlessWebKit$
 *   at javafx.graphics/com.sun.javafx.application.LauncherImpl.launchApplication1(LauncherImpl.java:890)
 *   at javafx.graphics/com.sun.javafx.application.LauncherImpl.lambda$launchApplication$2(LauncherImpl.java:195)
 *   at java.base/java.lang.Thread.run(Thread.java:834)
 * Caused by: java.lang.NoSuchMethodException: mdocs.HeadlessWebKit$.<init>()
 *   at java.base/java.lang.Class.getConstructor0(Class.java:3349)
 *   at java.base/java.lang.Class.getConstructor(Class.java:2151)
 *   at javafx.graphics/com.sun.javafx.application.LauncherImpl.lambda$launchApplication1$8(LauncherImpl.java:801)
 *   at javafx.graphics/com.sun.javafx.application.PlatformImpl.lambda$runAndWait$12(PlatformImpl.java:455)
 *   at javafx.graphics/com.sun.javafx.application.PlatformImpl.lambda$runLater$10(PlatformImpl.java:428)
 *   at java.base/java.security.AccessController.doPrivileged(Native Method)
 *   at javafx.graphics/com.sun.javafx.application.PlatformImpl.lambda$runLater$11(PlatformImpl.java:427)
 *   at ... `
 *
 */
class  HeadlessWebKit extends Application {
  val statics = HeadlessWebKit
  println("HeadlessWebKit Init -------------------- 1")
  //statics.initMonocleHeadless(true)
  //println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
  override def start(primaryStage: Stage): Unit = {
    println("HeadlessWebKit Start --------------------")
    HeadlessWebKit.start(primaryStage)
    println("---------------------- HeadlessWebKit Start")
  }
}

/**
 * This is a WebKit application that is used to render HTML pages and take a
 * snapshot of those pages. Tests (done in [[TestWebKitCapture]]) show that we
 * can run and take snapshot from a vanilla WebKit instance (JavaFX windows
 * are displayed). However we need to use this in headless mode. To do this we
 * use a compiled version of OpenJDK's Monocle that is made available by the
 * TestFX project. We then *patch* the application so that Webkit renders to
 * Monocle invisibly.
 * Note that we must ensure that (see the build.sc file):
 * 1. OpenFX/JavaFX is installed (separate from OpenJDK)
 * 2. The application imports the JavFX modules
 * 3. We must patch javafx.graphics with the JavaFX modules and the Monocle
 * module
 * 4. We must add monocle to the list of exports`javafx.graphics/com.sun.glass.ui=org.testfx.monocle`
 *
 * The application on start-up must declare environment variables to ensure
 * correct Monocle software rendering is done and initializes the Monocles
 * headless mode via [[HeadlessWebKit.initMonocleHeadless]]. This code was
 * lifted from the TextFX code.
 *
 * WebKit is a native application with it own thread. in addition to this
 * JavaFX also has its own thread. We need to communicate with webKit using
 * the JavaFX methods. Because of this we need to synchronize with blocking.
 * More concretely the main method [[HeadlessWebKit.capture]] will block until
 * the page has loaded and the snapshot taken. The snapshot wait is done by
 * time so it may be necessary to increase the delay.
 *
 * @see https://medium.com/@danielnenkov/run-testfx-tests-on-jenkins-running-on-ec2-d68197e10e2b
 *      https://github.com/TestFX/TestFX
 *      https://github.com/TestFX/Monocle
 *      https://mvnrepository.com/artifact/org.testfx/openjfx-monocle/jdk-11+26
 *      https://wiki.openjdk.java.net/display/OpenJFX/Monocle
 *      https://wiki.openjdk.java.net/display/OpenJFX/Building+OpenJFX
 *      https://wiki.openjdk.java.net/display/OpenJFX/Building+the+OpenJFX+embedded+stack+for+Linux+desktop
 *
 */
object HeadlessWebKit extends Application {

  private var delay = 50 // delay to wait for snapshot (in milliseconds)

  private var webView: WebView = _
  private var webEngine: WebEngine = _
  private var vBox: VBox = _
  private var primaryStage: Stage = _
  private var scene : Scene = _
  private var savedOK: Boolean = false

  // https://stackoverflow.com/questions/10711447/is-there-a-non-reentrant-readwritelock-i-can-use : Semaphore
  //
  private val loadLock = new ReentrantLock                 // Locks webkit while loading
  private val launched = loadLock.newCondition             // Signal when the launch thread has finished
  private val loaded = loadLock.newCondition               // Signal when the web engine has loaded the page
  private var started = false                              // Only after the JFX platform has started can we capture

  private val snapshotLock = new ReentrantLock             //Locks webkit while taking a snapshot
  private val snapShotTaken = snapshotLock.newCondition    // Signal when the web engine has taken snapshot

  private var loadState: Worker.State = Worker.State.READY // state machine that indicates when page has been loaded
  private var file: File = _                               // File were image capture is saves

  private var width: Option[Double] = None                 // width of the WebView
  private var height: Option[Double] = None                // height of the WebView
  private var hideScrollBar: Boolean = false               // If true the scroll bars are removed

  case class ViewPort(x:Option[Double] = None,
                      y:Option[Double] = None,
                      width:Option[Double] = None,
                      height:Option[Double] = None){
    def snapshotParameters(): SnapshotParameters = {
      val offsetX = x.fold(0.0)(v => v)
      val offsetY = y.fold(0.0)(v => v)
      val w = width.fold(webView.getWidth)(v => v)
      val h = height.fold(webView.getHeight)(v => v)
      (x, y, width, height) match {
        case (None, None, None, None) => null
        case _ =>
          val params = new SnapshotParameters()
          params.setViewport( new Rectangle2D(offsetX,offsetY, w, h))
          params
      }
    }
  }
  var vp = ViewPort()

  println("Start Monocle")
  this.synchronized {
    // Set JavaFX and Monocle variables
    //System.setProperty("javafx.macosx.embedded", "true")
    //System.setProperty("javafx.linux.embedded", "true")
    System.setProperty("testfx.robot", "glass")
    System.setProperty("testfx.headless", "true")
    System.setProperty("glass.platform", "Monocle")
    System.setProperty("java.awt.headless", "true")
    System.setProperty("monocle.platform", "Headless")
    System.setProperty("prism.order", "sw")
    System.setProperty("prism.text", "t2k")
    //System.setProperty("headless.geometry", "1600x1200-64")

    // Initialize Monocle software rendering
    initMonocleHeadless(true)
    println("Initiated Monocle")
  }

  private def tprintln(str: String): Unit = {
    println(s"$str $this @ ${Thread.currentThread().getId}")
  }

  /**
   * This method takes a snapshot image an saves it to a file. JavaFX/OpenFX
   * supports the following output formats: JPEG, PNG, GIF, BMP and WBMP
   * Additional formats may be available as plugins.
   *
   * @see https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/imageio/package-summary.html
   *      No JPEG support
   * @see https://bugs.openjdk.java.net/browse/JDK-8204188
   * @see https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/imageio/package-use.html
   *
   *      BMP and WBMP do not work in Linux
   * @param image an image containing a snapshot
   * @return true if an image was successfully saved otherwise false
   */
  private def saveSnapshot(image: WritableImage, file: File): Boolean = {
    try {
      // JPEG, PNG, GIF, BMP and WBMP
      val fileType = file.extension.fold(".?")(s => s).drop(1)
      ImageIO.write(SwingFXUtils.fromFXImage(image, null), fileType, file.toJava)
    } catch {
      case e: Throwable => false
    }
  }

  /**
   * Take a snapshot of the WebView that was placed into the Stage. We need to
   * take the snapshot after a small delay. If not WebView will not have been
   * rendered and we get a white (blank) image.
   *
   * NOTE 1: when we use `java.Timer` we need to use `Platform.runLater`. If
   * not, we will get an exception that another thread i executing outside the
   * JavaFX thread.
   *
   * NOTE 2: If we use a timer, we in essence create a thread. This thread
   * stays alive by default. This means that the JavaFX will not close even if
   * explicitly call `Platform.exit()`. Implicitly the `primaryStage.close()`
   * call will trigger the `Application.stop()`. We need to terminate the timer
   * with a call to `timer.cancel()`. In such a case if we only have one stage
   * then `Platform.exit()` need not be called - the app will be implicitly
   * terminate.
   *
   * NOTE 3: Another solution suggests using 2 pulses in an `AnimationTimer`
   * https://stackoverflow.com/questions/59803411/when-is-a-webview-ready-for-a-snapshot
   *
   * @see https://stackoverflow.com/questions/26916640/javafx-not-on-fx-application-thread-when-using-timer
   * @param primaryStage JavaFX root stage
   * @param webView      OpenFX WebView (based on WebKit)
   */
  private def takeDelayedSnapshotTimer(primaryStage: Stage, webView: WebView, delay: Long, file: File): Unit = {
    val timer = new Timer()
    timer.schedule(new TimerTask {
      override def run(): Unit = {
        Platform.runLater { () =>
          try {
            snapshotLock.lock()
            // val image = webView.snapshot(null, null)
            // Clips if required
            val image = webView.snapshot(vp.snapshotParameters(), null)
            // This will be the scene size
            savedOK = saveSnapshot(image, file)
            timer.cancel()
            //primaryStage.close()
          }
          catch {
            case _: Throwable =>
              savedOK = false
          } finally {
            snapShotTaken.signal()
            snapshotLock.unlock()
          }
        }
      }
    }, delay)
  }

  /**
   * We seem to need an offset for a closer match with Plotly's raster rendering.
   * This could be due to the decorations creating insets. Here we estimate what
   * those insets are (as per the indication in Stackoverflow). We find that
   * we always get value 0. In addition to this using the stage seems to return
   * constants only for the X and Y (seem to be a position).
   *
   * Note that `scene.getWindow` points to `Stage` parent. With Monocle it seems
   * that the scene and stage dimensions are the same.
   *
   * @see https://stackoverflow.com/questions/26711474/javafx-8-stage-insets-window-decoration-thickness
   */
  def insets(): (Double, Double, Double, Double) = {
    val right = scene.getWindow.getWidth- scene.getWidth- scene.getX
    val bottom = scene.getWindow.getHeight - scene.getHeight - scene.getY
    //val right = primaryStage.getWidth - scene.getWidth- scene.getX
    //val bottom = primaryStage.getHeight - scene.getHeight - scene.getY
    val left = scene.getX
    val top = scene.getY
    println(s"Left = $left ; Right = $top")
    println(s"Right = $right ; Bottom = $bottom")
    (left, top, right, bottom)
  }

  /**
   * Take a snapshot of the WebView that was placed into the Stage. We need to
   * take the snapshot after a small delay. If not WebView will not have been
   * rendered and we get a white (blank) image.
   *
   * NOTE 1: when we use `java.Timer` we need to use `Platform.runLater`. If
   * not, we will get an exception that another thread i executing outside the
   * JavaFX thread.
   *
   * NOTE 2: If we use a timer, we in essence create a thread. This thread
   * stays alive by default. This means that the JavaFX will not close even if
   * explicitly call `Platform.exit()`. Implicitly the `primaryStage.close()`
   * call will trigger the `Application.stop()`. We need to terminate the timer
   * with a call to `timer.cancel()`. In such a case if we only have one stage
   * then `Platform.exit()` need not be called - the app will be implicitly
   * terminate.
   *
   * NOTE 3: Another solution suggests using 2 pulses in an `AnimationTimer`
   * https://stackoverflow.com/questions/59803411/when-is-a-webview-ready-for-a-snapshot
   *
   * @see https://stackoverflow.com/questions/26916640/javafx-not-on-fx-application-thread-when-using-timer
   * @param primaryStage JavaFX root stage
   * @param webView      OpenFX WebView (based on WebKit)
   */
  private def takeDelayedSnapshotTimeLine(primaryStage: Stage, webView: WebView, delay: Long, file: File): Unit = {

    new Timeline(
      new KeyFrame(
        Duration.millis(delay),
        (ae: ActionEvent) => {
          try {
            snapshotLock.lock()
            val params = new SnapshotParameters()
            //val image = webView.snapshot(null, null)
            // Clips if required
            val image = webView.snapshot(vp.snapshotParameters(), null)
            // This will be the scene size
            savedOK = saveSnapshot(image, file)
            //primaryStage.close()
          } catch {
            case _: Throwable =>
              savedOK = false
          }
          finally {
            snapShotTaken.signal()
            snapshotLock.unlock()
          }
        })
    ).play();
  }

  /**
   * Used for debugging. When we use re-entrant locks the same thread will have
   * to unlock as many times as it locked.
   */
  private def unlockAll(): Unit = {
    while (loadLock.getHoldCount > 0) {
      loadLock.unlock()
    }
  }

  /**
   * This is the JavaFX entry point. Here we launch the WebKit engine and place
   * it into a Virtual Box. Currently we are not setting the size of the window.
   * We can set it to a fixed size such as (960, 600) but opted to set WebView
   * to autoSize. However one can force the size. We can also force the removal
   * of the scroll bars - this allows us to test the image output.
   * The a loader callback is installed. When the callback is installed it
   * locks this instances capture operations. When the page is loaded it then
   * launches the snapshot thread but does not wait for it to complete. This
   * means tha capture is not thread safe. If you want to use it from several
   * threads, synchronize on this object.
   *
   * @see [[setSize()]][[hideScrollBar()]]
   * @param primarystage this application's primary (and only) stage.
   * @see javafx.concurrent.Task
   *      https://docs.oracle.com/javase/8/javafx/api/javafx/concurrent/Task.html
   *      https://docs.oracle.com/javase/8/javafx/interoperability-tutorial/concurrency.htm#JFXIP546
   */
  def start(primarystage: Stage): Unit = {
    this.synchronized {
      try {
        tprintln("Start try to lock -------------------")
        primarystage.setTitle("JavaFX WebView - TestFX & Monocle")
        loadLock.lock()
        tprintln("Start has the lock -------------------")

        // Start the webkit
        primaryStage = primarystage
        webView = new WebView()
        webEngine = webView.getEngine

        // Add a scene into which webkit will render
        //val vBox = new VBox(webView)
        vBox = new VBox(webView)
        // This is the size of the image
        //val scene = new Scene(vBox, 960, 600)
        scene = new Scene(vBox)
        primaryStage.setScene(scene)
        primaryStage.setResizable(true)
        // Has no effect
        //primaryStage.initStyle(StageStyle.UNDECORATED)
        // We need to show in order to ensure rendering
        primaryStage.show()
        tprintln("Start show on -------------------")

        // Create a callback that will load the pages and launch the snapshot thread
        webEngine.getLoadWorker.stateProperty().addListener((observable, oldState, newState) => {
          try {
            /*if (lock.isHeldByCurrentThread)*/ loadLock.lock()

            newState match {
              case Worker.State.READY => ()

              case Worker.State.SCHEDULED => ()

              case Worker.State.RUNNING => ()

              case Worker.State.SUCCEEDED =>
                // We can resize the window. This causes overflow which
                // results in scroll bars. Remove these if desired
                // https://stackoverflow.com/questions/15498287/remove-the-scrollbar-in-the-webview-javafx
                //webEngine.executeScript("document.style.overflow = 'hidden';")
                if (hideScrollBar)
                  webEngine.executeScript("document.body.style.overflow = 'hidden';")

                // We need to delay this till after rendering has occurred
                // No event seems to exist for this. We can force rendering
                // via pulses, but the API is protected/private.
                takeDelayedSnapshotTimeLine(primaryStage, webView, delay, file)
                loadState = newState
                loaded.signal()
                unlockAll()

              case Worker.State.FAILED =>
                loadState = newState
                loaded.signal()
                unlockAll()

              case Worker.State.CANCELLED =>
                loaded.signal()
                unlockAll()
            }

          } catch {
            case _: Throwable =>
              loaded.signal()
              unlockAll()
          }
        })

        /*
        // TODO: review, see : webEngine.getLoadWorker.stateProperty().addListener
        webEngine.setOnResized(h => {
          try {
            println(s"webEngine.setOnResized 44444444444444444444 $this")
            lock.lock()
            println(s"h.getData = ${h.getData}")
          } finally {
            notResized.signal()
            lock.unlock()
          }
        })*/

      } finally {
        started = true
        println("Start launch signal ??????????????????????????????")
        launched.signal()
        println("Start LoadLock unlock ??????????????????????????????")
        loadLock.unlock()
        println("Start signaled done ??????????????????????????????")
      }
    }
  }

  def restart(): Unit= {
    Platform.runLater { () =>
      start( new Stage() )
    }
  }

  /**
   * Current working directory
   *
   * @return absolute path of `cwd`.
   */
  def cwd: String = Paths.get(".").toAbsolutePath.normalize().toString

  /**
   * Generates a`file://` URL. It ensures that the the path is absolute so
   * that loading succeeds. Relative paths are only valid is relative to
   * a previous load.
   *
   * @param uri of file to load. Can ve absolute or relative
   * @return absolute file URL
   */
  def file(uri: String): String = {
    val relativeTo = if (uri.startsWith(".")) cwd + "/" else ""
    s"""file://$relativeTo$uri"""
  }

  /**
   * Adds the `http://` protocol identifier to a URI.
   *
   * @param uri absolute path to a site.
   * @return HTTP absolute path to a site.
   */
  def http(uri: String): String = s"""http://$uri"""

  /**
   * Adds the `https://` protocol identifier to a URI.
   *
   * @param uri absolute path to a site.
   * @return HTTPS absolute path to a site.
   */
  def https(uri: String): String = s"""https://$uri"""


  /**
   * This method takes a snapshot image an saves it to a file. JavaFX/OpenFX
   * supports the following output formats: JPEG, PNG, GIF, BMP and WBMP
   * Additional formats may be available as plugins. Seems like only PNG,
   * GIF and TIFF are supported on Linux.
   *
   * This call is asynchronous. It will block to wait for loading and snapshot.
   * Both these actions are synchronized - snapshot only after the loading.
   * Note however that once loading is complete a new capture may start. It is
   * therefore not threadsafe.
   *
   * @param html    link to an HTML source (HTTP or FILE) or a string with HTML
   *                code
   * @param fileOut name of the file to save to. The file extension indicates
   *                the image format type
   */
  def capture(html: String, fileOut: File): Boolean = {
    this.synchronized {
      var oldState = Worker.State.READY

      try {
        // Only one snap-shot at a time
        loadLock.lock()
        while (!started) {
          //launched.await(10, TimeUnit.MILLISECONDS)
          launched.await()
        }

        file = fileOut
        savedOK = false

        // Start asynchronous loading
        Platform.runLater { () =>
          if (html.startsWith("http") || html.startsWith("file")) {
            webView.getEngine.load(html)
          } else {
            webView.getEngine.loadContent(html, "text/html")
          }
          webView.autosize()
          // This doe not work
          //webView.resize(800, 440)
          // This works
          // vBox.resize(810, 440)
          // This works
          //primaryStage.setWidth(800)
          //primaryStage.setHeight(440)
          width.foreach(primaryStage.setWidth)
          height.foreach(primaryStage.setHeight)
          primaryStage.show()
        }

        //webEngine.onStatusChangedProperty()
        //webEngine.setOnVisibilityChanged()

        // Wait for pages to load
        loaded.await()
        oldState = loadState
        // Next state
        loadState = Worker.State.READY

        // Only if we loaded with success
        if (oldState == Worker.State.SUCCEEDED) {
          // Wait for the snapshot
          snapshotLock.lock()
          snapShotTaken.await()
        } else {
          // otherwise assume failed snapshot
          savedOK = false
        }
        // Make a copy of volatile value
        val tmp = if (savedOK) true else false
        tmp
      }
      finally {
        loadLock.unlock()
        // Only if we locked do we unlock
        if (oldState == Worker.State.SUCCEEDED) snapshotLock.unlock()
      }
    }
  }

  /**
   * Resizes the output window to a give size. This allows one to
   * capture images of a given size that can be used for testing.
   * If size is not set the webview will auto-size. If no resize
   * is desired just set to None.
   *
   * See also [[setViewport]]
   *
   * @param width Some(width) otherwise None
   * @param height Some(height) otherwise None
   */
  def setSize(width: Option[Double], height: Option[Double]): Unit = {
    this.synchronized {
      this.width = width
      this.height = height
    }
  }

  /**
   * Allows one to hide the scroll bar. This can be useful when we
   * want to capture an image for documentation or comparison.
   *
   * @param hideScrollBar True to hide else false to leave as is.
   */
  def hideScrollBar(hideScrollBar: Boolean): Unit = {
    this.synchronized {
      this.hideScrollBar = hideScrollBar
    }
  }

  /**
   * Sets the view port that allows us to capture a square portion of the
   * WebView window. This is defined by an (x,y) position relative to the top
   * left corner of the app window. The size of the clipping vew port also
   * has a width and height. If any of these element are not provided (use
   * the `None` value) a default will be used. The position defaults to 0.
   * The size defaults to the current window size. Note that th window size
   * may also be set (see [[setSize]].
   *
   * If no values are set then no clipping viewport is used.
   *
   * @param vp [[ViewPort]] dimension
   */
  def setViewport(vp: ViewPort) : Unit = {
    this.synchronized {
      this.vp = vp
    }
  }

  /**
   * Wait until the platform and application have started.
   *
   * NOTE: we cannot block on this object. If we do, the following will happen.
   * Assume we initialize jFX but we call this method before the start method
   * executes. It will block on this object. The start method will ot execute.
   * The start flag will therefore never be set. This method in effect
   * dead-locked.
   */
  def waitStart(): Unit = {
    //this.synchronized {
      try {
        tprintln("withStart locking")
        // Only one snap-shot at a time
        loadLock.lock()
        tprintln("withStart locked")
        while (!started) {
          //launched.await(500, TimeUnit.MILLISECONDS)
          //tprintln("withStart waiting")
          launched.await()
        }
      }
      finally {
        loadLock.unlock()
        tprintln("withStart unlocked")
      }
    //}
  }

  /**
   * Stop the application. If any blocking is held the application will not
   * terminate. To force an exit use a system exit. We currently deactivate
   * the auto-close. So this method only stops the application but does not
   * stop the JFX daemon thread.
   *
   * @see [[kill()]]
   */
  override def stop(): Unit = {
    this.synchronized {
      super.stop()
      Platform.runLater { () =>
        started = false
        primaryStage.close()
      }
    }
  }

  /**
   *
   *
   * @see [[stop()]]
   */
  def kill(): Unit = {
    this.synchronized {
      Platform.setImplicitExit(true)
      Platform.exit()
    }
  }

  /**
   * This launches the application. Not working. See [[launchBackground]].
   * @see https://stackoverflow.com/questions/12124657/getting-started-on-scala-javafx-desktop-application-development
   */
  def launchAndBlock(): Unit = {
    this.synchronized {
      Application.launch()
    }
  }

  /**
   * Launches the OpenFX/JavaFX application. Here we launch the JavaFX
   * application in its own thread so that a client may call the capture
   * method. Because it is launched as a thread we must force initialization
   * before a capture is executed. We do this via
   * `com.sun.javafx.application.PlatformImpl.startup`.
   *
   * Also note that we required the declaration of the singletons class
   * because we need to use the class instantion methods to start the
   * application. Scala objects are not 100% compatible with Java static
   * classes.
   *
   * @param args command line arguments for the application
   */
  def launchBackground(args: Array[String]): Unit = {
    this.synchronized {
      // https://stackoverflow.com/questions/11273773/javafx-2-1-toolkit-not-initialized
      // Force initialization (because we launch the application in a thread, we
      // may call the capture method before the javaFX Toolkit has had a chance
      // to initialize).
      /* We may get the following exception when we call the `PlatformImpl.startup`:
        [43/44] mdocs.runMain.overriden.mill.scalalib.JavaModule.runMain
         Exception in thread "main" java.lang.IllegalAccessError: class mdocs.HeadlessWebKit (in unnamed module @0x6093dd95) cannot access class com.sun.javafx.application.PlatformImpl (in module javafx.graphics) because module javafx.graphics does not export com.sun.javafx.application to unnamed module @0x6093dd95
         To solve this add the following lin eto the `openFXArgs` method on the build.sc file (forkArgs):
        "--add-opens=javafx.graphics/com.sun.javafx.application=ALL-UNNAMED",
       */
      com.sun.javafx.application.PlatformImpl.startup(() => {
        /* This block will be executed on JavaFX Thread */
      })

      // Default is true. It means that if we stop the only active stage,
      // the JavaFX application daemon thread is terminated. Otherwise it
      // stays active. We set this to false so that platform remains active
      // and the application queue continues to be processed. This way we can
      // restart the application by simply setting a new primary Stage.
      Platform.setImplicitExit(false)

      val thread = new Thread {
        override def run() : Unit = {
          Application.launch(classOf[HeadlessWebKit], args: _*)
          //Application.launch()
        }
      }
      thread.start()
    }
  }

  def launchBackgroundNoLock(args: Array[String]): Unit = {
    // https://stackoverflow.com/questions/11273773/javafx-2-1-toolkit-not-initialized
    // Force initialization (because we launch the application in a thread, we
    // may call the capture method before the javaFX Toolkit has had a chance
    // to initialize).
    /* We may get the following exception when we call the `PlatformImpl.startup`:
      [43/44] mdocs.runMain.overriden.mill.scalalib.JavaModule.runMain
       Exception in thread "main" java.lang.IllegalAccessError: class mdocs.HeadlessWebKit (in unnamed module @0x6093dd95) cannot access class com.sun.javafx.application.PlatformImpl (in module javafx.graphics) because module javafx.graphics does not export com.sun.javafx.application to unnamed module @0x6093dd95
       To solve this add the following lin eto the `openFXArgs` method on the build.sc file (forkArgs):
      "--add-opens=javafx.graphics/com.sun.javafx.application=ALL-UNNAMED",
     */
    com.sun.javafx.application.PlatformImpl.startup(() => {
      /* This block will be executed on JavaFX Thread */
    })
    val thread = new Thread {
      override def run() : Unit = {
        Application.launch(classOf[HeadlessWebKit], args: _*)
        //Application.launch()
      }
    }
    thread.start()
  }

  /** **************************************
   * From TestFX
   * https://github.com/TestFX/TestFX/blob/master/subprojects/testfx-core/src/main/java/org/testfx/toolkit/impl/ApplicationLauncherImpl.java#L33
   * ***************************************/

  private def initMonocleHeadless(headless: Boolean): Unit = {
    if (checkSystemPropertyEquals("testfx.headless", "true") || headless) {
      try {
        assignMonoclePlatform()
        assignHeadlessPlatform()
      } catch {
        case var3: ClassNotFoundException =>
          throw new IllegalStateException("Monocle headless platform not found", var3)
        case var4: Exception =>
          throw new RuntimeException(var4)
      }
    }

  }

  private def checkSystemPropertyEquals(propertyName: String, valueOrNull: String): Boolean =
    System.getProperty(propertyName, null).equals(valueOrNull)

  private def assignMonoclePlatform(): Unit = {
    val platformFactoryClass = Class.forName("com.sun.glass.ui.PlatformFactory");
    val platformFactoryImpl = Class
      .forName("com.sun.glass.ui.monocle.MonoclePlatformFactory")
      .getDeclaredConstructor().newInstance().asInstanceOf[Object]
    assignPrivateStaticField(platformFactoryClass, "instance", platformFactoryImpl);
  }

  private def assignHeadlessPlatform(): Unit = {
    val nativePlatformFactoryClass = Class.forName("com.sun.glass.ui.monocle.NativePlatformFactory")
    try {
      val nativePlatformCtor = Class.forName("com.sun.glass.ui.monocle.HeadlessPlatform").getDeclaredConstructor()
      nativePlatformCtor.setAccessible(true)
      assignPrivateStaticField(nativePlatformFactoryClass, "platform", nativePlatformCtor.newInstance().asInstanceOf[Object])
    }
    catch {
      case exception: ClassNotFoundException =>
        // Before Java 8u40 HeadlessPlatform was located inside of a "headless" package.
        val nativePlatformCtor: Constructor[_] = Class.forName("com.sun.glass.ui.monocle.headless.HeadlessPlatform").getDeclaredConstructor()
        nativePlatformCtor.setAccessible(true)
        assignPrivateStaticField(nativePlatformFactoryClass, "platform", nativePlatformCtor.newInstance().asInstanceOf[Object])
    }
  }

  private def assignPrivateStaticField(clazz: Class[_], name: String, value: Object): Unit = {
    val field: Field = clazz.getDeclaredField(name)
    field.setAccessible(true)
    field.set(clazz, value)
    field.setAccessible(false)
  }


}
