package splotly

import ujson.Value

// TODO: override toString to generate an object with methods?
case class SubPlotID(dflt: Option[String],
                     description: Option[String]
                ) extends JSON {

  private var value: Option[String] = None

  // TODO: can only set x1, x2, and so on x's and y1, y2 and so on for y's
  def setValue(string: String): Unit = {
    val s = string.substring(0,1).toLowerCase
    val n = string.substring(1)
    if (s != "x" || s != "y") throw new RuntimeException("First char must be 'x' or 'y'")
    try {
      n.toInt
    } catch {
      case _: Throwable => throw new RuntimeException("Last characters must be an integer - plot ID")
    }

    value = Some(string)
  }
  def setDefault(): Unit = value = dflt
  def getValue: Option[String] = value

  override def json(): String = value.fold("")(e => s""""$e"""")

  override def fromJson(in: Value): SubPlotID.this.type = ???
}
