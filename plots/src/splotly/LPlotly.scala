package splotly

import better.files._
import File._
import java.io.{File => JFile}
import java.nio.file.{Files => JFiles, Paths => JPaths}
import java.util.UUID

import splotly.gen.{Layout, Traces}
import ujson.{Arr, Obj, Value}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer


object LPlotly {

  def tempJFile(fileName: String): JFile = {
    val property = "java.io.tmpdir"
    val sysDir = System.getProperty(property)
    val uuid = this.synchronized(UUID.randomUUID.toString)
    val tmpDir = JPaths.get(sysDir, uuid).toString
    val file = new JFile(tmpDir + fileName)
    file
  }
  def tempFile(fileName: String): File = tempJFile(fileName).toScala

  def tempHTMLJFile(): JFile = tempJFile("_plotlyscala.html")

  def tempHTMLFile(): File = tempHTMLJFile().toScala

  def jsSnippet(div: String, data: Seq[Trace], layout: Layout): String = {

    val mergedLayout = mergeLayout(layout, data:_*)
    val jsonLayOut = mergedLayout.fold(
        // If error, write that to the HTML
        e => e.errs.mkString(","),
      { e =>
        // Otherwise use merged layout info
        val json = ujson.read( e )
        json.render(2)
      }
    )

    val b = new StringBuilder

    b ++= "(function () {\n"

    for ((d, idx) <- data.zipWithIndex) {
      val json = ujson.read( d.attributes.json() )
      val formatted = json.render(2)
      b ++= s"  var data$idx = "
      b ++= formatted
      b ++= ";\n"
    }

    b ++= "\n  "
    b ++= data.indices
      .map(idx => s"data$idx")
      .mkString("var data = [", ", ", "];")
    b ++= "\n"
    b ++= "  var layout = "
    b ++= jsonLayOut
    b ++= ";\n\n  Plotly.plot('"
    b ++= div.replaceAll("'", "\\'")
    b ++= "', data, layout);\n"

    b ++= "})();"

    b.result()
  }

  sealed trait PlotlyJS
  final case class Local(url:String) extends PlotlyJS
  final case class UseCDN(url:String) extends PlotlyJS
  final case class UseCDNVersion(version:String) extends PlotlyJS
  final case object UseLatestCDN extends PlotlyJS

  // TODO: new plot
  // TODO: plot that returns HTML directly
  def plot( divId: String,
            traces: Seq[Trace],
            layout: Layout,
            path: Option[File] = None,
            useCdn: PlotlyJS = UseLatestCDN,
            openInBrowser: Boolean = true,
            overwrite: Boolean = true
          ): File = {

    val tmp = path.fold(tempHTMLFile())(f => f)
    val htmlFile: File = if (tmp.exists() && !overwrite)
      throw new Exception(s"$tmp already exists")
    else
        tmp

    // s"""<script src="https://cdn.plot.ly/plotly-$plotlyVersion.min.js"></script>"""
    val plotlyHeader = useCdn match {
      case Local(url) => s"""<script src="$url"></script>"""
      case UseCDNVersion(plotlyVersion) => s"""<script src="https://cdn.plot.ly/plotly-$plotlyVersion.min.js"></script>"""
      case UseCDN(url) => s"""<script src="https://cdn.plot.ly/$url"></script>"""
      case UseLatestCDN => """<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>"""
    }

    val title: Option[String] = layout.layoutAttributes.title.text.getValue
    val html =
      s"""<!DOCTYPE html>
         |<html>
         |<head>
         |<title>${title.getOrElse("Plotly.s Plot")}</title>
         |$plotlyHeader
         |</head>
         |<body>
         |<div id="$divId"></div>
         |<script>
         |${jsSnippet(divId, traces, layout)}
         |</script>
         |</body>
         |</html>
       """.stripMargin

    JFiles.write(htmlFile.toJava.toPath, html.getBytes("UTF-8"))

    if (openInBrowser) {
      val cmdOpt = sys.props.get("os.name").map(_.toLowerCase) match {
        case Some("mac os x") =>
          Some(Seq("open", htmlFile.toJava.getAbsolutePath))
        case Some(win) if win.startsWith("windows") =>
          Some(Seq("cmd", s"start ${htmlFile.toJava.getAbsolutePath}"))
        case Some(lin) if lin.indexOf("linux") >= 0 =>
          Some(Seq("xdg-open", htmlFile.toJava.getAbsolutePath))
        case other =>
          None
      }

      cmdOpt match {
        case Some(cmd) =>
          sys.process.Process(cmd).!
        case None =>
          Console.err.println(s"Don't know how to open ${htmlFile.toJava.getAbsolutePath}")
      }
    }

    htmlFile
  }

  /**
   * Plotly JSON data consists of 2 sections: an array of Traces and
   * a Layout object. The layout attributes contain both the general
   * and trace specific layout attributes. This method takes the
   * wrappers general and trace specific attributes and merges them
   * into a single JSON document. This will form Plotly's layout
   * attributes.
   *
   * To do the inverse see [[parserData]]
   *
   * @see [[mergeLayout]][[mergeData()]]
   * @param layout General layout attributes
   * @param traces Traces with their own specific layout attributes
   * @return the Plotly compatible JSON layout object
   */
  def mergeJsonLayout(layout: Layouts, traces:Trace*): Either[Error, Obj] = {
    val plots = traces.map( trace => ujson.read( trace.layoutAttributes.json() ) )
    val zero = new mutable.LinkedHashMap[String, Value]()
    val plot = plots.foldLeft(zero){ (acc, value) =>
      value match {
        case ujson.Obj(o) =>
          acc ++= o
          acc
        case _ => throw new RuntimeException(s"mergeLayout: expected to JSON object but got $value")
      }
    }
    val layOut = ujson.read( layout.layoutAttributes.json() )
    (layOut, plot) match {
      case (ujson.Obj(o1), o2) =>
        val json = ujson.Obj.from( o1 ++ o2 )
        Right(json)
      case _ => Left(Error(s"mergeLayout: expected to JSON object but got $layOut"))
    }
  }

  /**
   * Plotly JSON data consists of 2 sections: an array of Traces and
   * a Layout object. The layout attributes contain both the general
   * and trace specific layout attributes. This method takes the
   * wrappers general and trace specific attributes and merges them
   * into a single JSON document. This will form Plotly's layout
   * attributes.
   *
   * @see [[mergeJsonLayout]][[mergeJsonData()]]
   * @param layout General layout attributes
   * @param traces Traces with their own specific layout attributes
   * @return the Plotly compatible JSON layout object
   */
  def mergeLayout(layout: Layouts, traces:Trace*) : Either[Error, String] = {
    mergeJsonLayout(layout, traces:_*).map(_.toString)
  }

  /**
   * Plotly JSON data consists of 2 sections: an array of Traces and
   * a Layout object. The layout attributes contain both the general
   * and trace specific layout attributes. This method takes the
   * trace (non-layout) attributes and merges them into a single JSON
   * document. This will form Plotly's trace data attributes.
   *
   * @see [[mergeData]]
   * @param traces Plotly traces
   * @return the Plotly compatible JSON traces data array
   */
  def mergeJsonData(traces:Trace*) : Either[Error, Arr] = {
    val plots = traces.map( trace => ujson.read( trace.attributes.json() ) )
    val json = ujson.Arr.from( plots )
    Right(json)
  }

  /**
   * Plotly JSON data consists of 2 sections: an array of Traces and
   * a Layout object. The layout attributes contain both the general
   * and trace specific layout attributes. This method takes the
   * trace (non-layout) attributes and merges them into a single JSON
   * document. This will form Plotly's trace data attributes.
   *
   * @see [[mergeJsonData]]
   * @param traces Plotly traces
   * @return the Plotly compatible JSON traces data array
   */
  def mergeData(traces:Trace*) : Either[Error, String] = mergeJsonData(traces:_*).map(_.toString)

  /**
   * Plotly JSON data consists of 2 sections: an array of Traces and
   * a Layout object. The layout attributes contain both the general
   * and trace specific layout attributes. This method takes a valid
   * Plotly JSON document, parsers it and loads the corresponding
   * Scala wrappers. These consist of a list o Trace objects and a
   * single general Layout wrapper.
   *
   * To perform the inverse see [[mergeLayout]]
   *
   * @param json
   * @return
   */
  def parserData(json: Value): Either[Error, (List[Trace], gen.Layout)] = {
    // Get the traces and data
    val data = json("data")
    val err: Either[Error, ArrayBuffer[Trace]] = Left(Error(s"parserData: expected Arr but got: $data"))
    // Load the traces to the wrapper Trace(s)
    val tmp = data.arrOpt.fold(err){
      values =>
        // The traces are listed in an array
        val traces = values.flatMap{ d =>
          d.objOpt.map{ trace =>
            // For each trace find out what the trace is
            val typeName = trace(Constants.TYPE).str
            val traceClass = Traces.build(typeName)
            // Instantiate the trace
            val traceObj: Trace = traceClass()
            // Load the json data
            traceObj.attributes.fromJson(trace)
            traceObj
          }
        }
        Right(traces)
    }
    // Get the layout data
    val layoutJson = json("layout")
    // Select one type of each Trace to load layouts
    val uniqueTraces = tmp.map(_.toSet)
    // Load the Trace specific layout attributes
    uniqueTraces.map{ traces =>
      traces.map(_.layoutAttributes.fromJson(layoutJson))
    }
    // Read in the general layout info
    val layout = splotly.gen.Layout()
    layout.layoutAttributes.fromJson(layoutJson)
    tmp.map(e => (e.toList, layout))
  }

}
