/*
   We want to execute Plotly JS scripts that require a Browser's environment.
   We need to add a dummy DOM model and we do tis by importing the React JS
   library. We also require some timon functions that we provide here.
   See PlotModifier.evalJS() for more details.
*/

// https://github.com/jsdom/jsdom
//load("https://unpkg.com/react@16/umd/react.development.js");
//load("https://unpkg.com/react-dom@16/umd/react-dom.development.js");
//load("https://cdn.plot.ly/plotly-latest.min.js");
//load("https://cdn.jsdelivr.net/npm/jsdom@15.1.1/lib/api.min.js") // issue with JS engine
// https://github.com/jazdw/domino/tree/master/lib
// https://jscompress.com/
//load("https://cdn.jsdelivr.net/npm/domino@2.1.3/lib/*.js")


var Platform = Java.type("javafx.application.Platform");
var Timer    = Java.type("java.util.Timer");

function setTimerRequest(handler, delay, interval, args) {
    handler = handler || function() {};
    delay = delay || 0;
    interval = interval || 0;
    var applyHandler = function() handler.apply(this, args);
    var runLater = function() Platform.runLater(applyHandler);
    var timer = new Timer("setTimerRequest", true);
    if (interval > 0) {
        timer.schedule(runLater, delay, interval);
    } else {
        timer.schedule(runLater, delay);
    }
    return timer;
}

function clearTimerRequest(timer) {
    timer.cancel();
}

function setInterval() {
    var args = Array.prototype.slice.call(arguments);
    var handler = args.shift();
    var ms = args.shift();
    return setTimerRequest(handler, ms, ms, args);
}

function clearInterval(timer) {
    clearTimerRequest(timer);
}

function setTimeout() {
    var args = Array.prototype.slice.call(arguments);
    var handler = args.shift();
    var ms = args.shift();

    return setTimerRequest(handler, ms, 0, args);
}
function clearTimeout(timer) {
    clearTimerRequest(timer);
}

function setImmediate() {
    var args = Array.prototype.slice.call(arguments);
    var handler = args.shift();

    return setTimerRequest(handler, 0, 0, args);
}

function clearImmediate(timer) {
    clearTimerRequest(timer);
}
