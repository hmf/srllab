package splotly

import ujson.Value

/**
 *
 * @see https://github.com/plotly/plotly.js/issues/4625
 * @param min
 * @param max
 * @param dflt
 * @param description
 */
case class PReal(min: Option[Double],
                 max: Option[Double],
                 dflt: Option[String],
                 //dflt: Option[Double],
                 description: Option[String]
                ) extends JSON {

  private var value: Option[OfPrimitive] = None

  def setValue(v: Double): Unit = {
    value = Some(TDouble(v))
  }
  def setValue(v: Int): Unit = {
    value = Some(TInt(v))
  }
  def setValue(v: String): Unit = {
    value = Some(TString(v))
  }
  def setDefault(): Unit = {
    value = dflt.map(TString)
  }

  def getValue: Option[OfPrimitive] = value

  def getDoubleValue: Option[Double] = value.map {
    case TDouble(v) => v
    case TInt(v) => v.toDouble
    case e@_ =>
      throw new RuntimeException(s"${getClass.getCanonicalName}.getDoubleValue unexpected type: $e")
  }

  def getIntValue: Option[Int] = value.map {
    case TInt(v) => v
    case e@_ =>
      throw new RuntimeException(s"${getClass.getCanonicalName}.getIntValue unexpected type: $e")
  }

  def getStringValue: Option[String] = value.map {
    case TString(v) => v
    case e@_ =>
      throw new RuntimeException(s"${getClass.getCanonicalName}.getStringValue unexpected type: $e")
  }

  override def json(): String = {
    value match {
      case None => ""
      case Some(TString(e)) => s"""${e.toString}"""
      case Some(TDouble(e)) => e.toString
      case Some(TInt(e)) => e.toString
      case Some(v) =>
        throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected type $v.")
    }
    //value.fold("")(e => s"""${e.toString}""")
  }

  override def fromJson(in: Value): PReal.this.type = {
    in match {
      case ujson.Num(v) => if (v.isValidInt) setValue(v.toInt) else setValue(v)
      case ujson.Str(v) => setValue(v)
      case ujson.Bool(v) => setValue(v.toString)
      case _ =>
        throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
    }
    this
  }
}
