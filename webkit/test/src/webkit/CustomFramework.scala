package webkit

/**
 * This class is usd to start and stop the JavaFX (OpenFX) Platform and
 * WebKit application. It is also used to stop the JavaFX application daemon
 * thread. If this thread is not stopped the tests will block the Mill test
 * task.
 */
class CustomFramework extends utest.runner.Framework with JFXPlatform {
  override def setup(): Unit = super[JFXPlatform].setup()
  override def teardown(): Unit = super[JFXPlatform].teardown()
}
