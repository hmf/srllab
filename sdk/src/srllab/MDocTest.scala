package srllab

/**
 * mill mill.scalalib.GenIdea/idea
 *
 * mill sdk.compile
 * mill sdk.run
 * mill sdk.{compile, run}
 * mill --watch sdk.run
 *
 * mill -i sdk.console
 * mill -i sdk.repl
 *
 * mill -i sdk.runMain srllab.MDocTest
 * mill --watch sdk.runMain srllab.MDocTest
 *
 *
 */
object MDocTest {

  def main(args: Array[String]): Unit = {
    import mdoc._

    val sources = os.pwd / "sdk" / "docs" / "mdoc"
    val sink = os.pwd / "out" / "sdk" / "mDoc" / "dest"
    println(s"sources = $sources")
    println(s"sink = $sink")

    val args = Array("--verbose",
      "--in", sources.toString,
      "-out", sink.toString)
    val settings = mdoc.MainSettings()
      .withSiteVariables(Map("VERSION" -> "1.0.0"))
      //.withIn(workingDirectory)
      //.withOut(T.ctx().dest.toNIO)
      .withArgs(args.toList) // for CLI only
    // generate out/readme.md from working directory
    val exitCode = mdoc.Main.process(settings)
    // (optional) exit the main function with exit code 0 (success) or 1 (error)
    if (exitCode != 0) println(s"error = $exitCode")
  }

}
