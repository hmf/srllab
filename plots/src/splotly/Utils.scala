package splotly

import org.json4s.Diff
import splotly.Updater.renderJson
import ujson.{Arr, Bool, Null, Num, Obj, Str, Value}
import java.awt.image.DataBuffer

import better.files.Dsl._
import better.files.{File, _}
import java.nio.file.{Paths => JPaths}
import java.io.{File => JFile}

import javax.imageio.ImageIO
import org.json4s._
import org.json4s.jackson.JsonMethods._
import ujson.Value
import utest.assert
import webkit.HeadlessWebKit
import splotly.gen.Traces

object Utils {

  def sameJson(d1: ujson.Value, d2: ujson.Value, show: Boolean = false, offset: Int = 2): Boolean = {
    (d1, d2) match {
      case (Null, Null) => true
      case (Bool(v1), Bool(v2)) =>
        val r = v1 == v2
        renderJson(r, d1, d2, show, offset)
        r
      case (Str(v1), Str(v2)) =>
        //println(s"Str($v1), Str($v2)")
        val r = v1.trim.toLowerCase == v2.trim.toLowerCase
        renderJson(r, d1, d2, show, offset)
        r
      case (Num(v1), Num(v2)) =>
        val r = v1 == v2
        renderJson(r, d1, d2, show, offset)
        r
      case (Arr(v1), Arr(v2)) =>
        val n1 = v1.length
        val n2 = v2.length
        val n = Math.max(n1, n2)
        val ids = 0 until n
        ids.forall( k => (k < n1) && (k < n2) && sameJson(v1(k), v2(k), show, offset))
        val r = v1.zip(v2).forall( e => sameJson(e._1, e._2, show, offset) )
        renderJson(r, d1, d2, show, offset)
        r
      case (Obj(v1), Obj(v2)) =>
        //println(s"Obj($v1), Obj($v2)")
        val ks = v1.keys.toSet ++ v2.keys.toSet
        val r = ks.forall{ k => v1.contains(k) && v2.contains(k) && sameJson(v1(k), v2(k), show, offset) }
        renderJson(r, d1, d2, show, offset)
        r
      case _ =>
        //showJsonAST(d1)
        //showJsonAST(d2)
        renderJson(r = false, d1, d2, show, offset)
        false
    }

  }

  def checkJsonDifference(original: String, newTxt : String): Unit = {
    // Create AST
    val outJson = parse(newTxt)
    val inJson = parse(original)
    //println(pretty(render(outJson)))
    // Calculate the difference between the original and the output using AST
    val Diff(changed, added, deleted) = inJson diff outJson
    // We should have no differences
    /*println(s"Changed:\n${pretty(render(changed))}")
    println(s"Added:\n${pretty(render(added))}")
    println(s"Deleted:\n${pretty(render(deleted))}")*/
    assert(changed == JNothing)
    assert(added == JNothing)
    assert(deleted == JNothing)
  }

  def checkJsonDiff(original: Value, new_ : Either[Error, String]): Unit = {
    // It should have succeeded
    assert(new_.isRight)
    val newTxt = new_.right.get
    val inJson = original.render(2)
    checkJsonDifference(inJson, newTxt)
  }

  def checkLayoutDiff(json: Value, layout: gen.Layout, traces: List[Trace]): Unit = {
    // Lets check if the layout information is correct
    // Merge the general and trace plots layout information
    // Save this information to a JSON document
    val mergedLayout = LPlotly.mergeLayout(layout, traces:_*)

    // This is the original JSON document
    val layoutJson = json("layout")
    // Compare original with new one
    checkJsonDiff(layoutJson, mergedLayout)
  }

  def checkDataDiff(json: Value, traces: List[Trace]): Unit = {
    // Now test the traces
    // Merge the general and trace plot data information
    // Save this information to a JSON document
    val mergedTraces = LPlotly.mergeData(traces:_*)

    // This is the original JSON document
    val dataJson = json("data")
    // Compare original with new one
    checkJsonDiff(dataJson, mergedTraces)
  }

}
