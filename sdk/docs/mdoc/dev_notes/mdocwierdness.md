# Plotting Tests 


```scala mdoc:reset:silent
    import plotly._, layout._, Plotly._
    
    val labels = Seq("Banana", "Banano", "Grapefruit")
    val valuesA = labels.map(_ => util.Random.nextGaussian())
    val valuesB = labels.map(_ => 0.5 + util.Random.nextGaussian())
    
    val traces = Seq(
      Bar(labels, valuesA, name = "A"),
      Bar(labels, valuesB, name = "B")
    )
    val playout = Layout()
    val jsSnippet = Plotly.jsSnippet("sample2", traces, playout)
```

```scala mdoc
println(jsSnippet)
```

```scala mdoc:passthrough
    println(s"""
            |```
            |$jsSnippet
            |```
            """.stripMargin)
```

