package loom

/**
 * Mill global configuration
 *
 * This configuration is shared by all projects using the Loom module.
 */
object Versions {

  val gScalaMillVersion = "2.12.10"

  // Because of mill plug-ins
  //val gScalaVersion = "2.13.1"
  val gScalaVersion = "2.12.10"
  val gMdocVersion = "2.0.3"
  val gMdocVerbose = 1
  val gLaikaVersion = "0.14.0"
  val gLaikaVerbose = 1

  val millVersion: String = mill.BuildInfo.millVersion // "0.5.2"
  val utestVersion = "0.7.2"
  val osLibVersion = "0.6.2"
  val tableSawVersion = "0.36.0"
  val plotlyScalaVersion = "0.7.3"  // IMPORTANT: this must be the same version as ivy for mdoc.PlotlyModifier
  val betterFilesVersion = "3.8.0"
  val graalvmVersion = "19.3.0"

  val webkitVersion = "0.1.0"
  val plotsVersion = "0.1.0"

}
