package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Indicator() extends Trace {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val type_ : String      = """indicator"""
  val animatable: Boolean = true
  val categories: Array[String] =
    Array("""noHover""", """noOpacity""", """svg""")

  case object meta extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val description: String =
      """An indicator is used to visualize a single `value` along with some contextual information such as `steps` or a `threshold`, using a combination of three visual elements: a number, a delta, and/or a gauge. Deltas are taken with respect to a `reference`. Gauges can be either angular or bullet (aka linear) gauges."""

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val vars: List[String] = List()
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV => }
      this
    }

  }

  case object attributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val type_ : String = """indicator"""

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val name: PChars = PChars(
      dflt = None,
      description = Some(
        """Sets the trace name. The trace name appear as the legend item and on hover."""
      )
    )

    object visible extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Boolean = true
      val description: Option[String] = Some(
        """Determines whether or not this trace is visible. If *legendonly*, the trace is not drawn, but can appear as a legend item (provided that the legend itself is visible)."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) => t.toString
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => TBoolean(s)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def true_(): Unit = { selected = TBoolean(true) }

      def false_(): Unit = { selected = TBoolean(false) }

      def legendonly(): Unit = { selected = TString("legendonly") }

    }

    object align extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val description: Option[String] = Some(
        """Sets the horizontal alignment of the `text` within the box. Note that this attribute has no effect if an angular gauge is displayed: in this case, it is always centered"""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def right(): Unit = { selected = TString("right") }

      def center(): Unit = { selected = TString("center") }

      def left(): Unit = { selected = TString("left") }

    }

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uirevision: PAny = PAny(
      dflt = None,
      description = Some(
        """Controls persistence of some user-driven changes to the trace: `constraintrange` in `parcoords` traces, as well as some `editable: true` modifications such as `name` and `colorbar.title`. Defaults to `layout.uirevision`. Note that other user-driven trace attribute changes are controlled by `layout` attributes: `trace.visible` is controlled by `layout.legend.uirevision`, `selectedpoints` is controlled by `layout.selectionrevision`, and `colorbar.(x|y)` (accessible with `config: {editable: true}`) is controlled by `layout.editrevision`. Trace changes are tracked by `uid`, which only falls back on trace index if no `uid` is provided. So if your app can add/remove traces before the end of the `data` array, such that the same trace has a different index, you can still preserve user-driven changes if you give each trace a `uid` that stays with it as it moves."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val idssrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  ids .""")
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val meta: PAny = PAny(
      dflt = None,
      description = Some(
        """Assigns extra meta information associated with this trace that can be used in various text attributes. Attributes such as trace `name`, graph, axis and colorbar `title.text`, annotation `text` `rangeselector`, `updatemenues` and `sliders` `label` text all support `meta`. To access the trace `meta` values in an attribute in the same trace, simply use `%{meta[i]}` where `i` is the index or key of the `meta` item in question. To access trace `meta` in layout attributes, use `%{data[n[.meta[i]}` where `i` is the index or key of the `meta` and `n` is the trace index."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uid: PChars = PChars(
      dflt = None,
      description = Some(
        """Assign an id to this trace, Use this to provide object constancy between traces during animations and transitions."""
      )
    )

    object mode extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = Some("""number""")
      val description: Option[String] = Some(
        """Determines how the value is displayed on the graph. `number` displays the value numerically in text. `delta` displays the difference to a reference value in text. Finally, `gauge` displays the value graphically on an axis."""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def gaugeOn(): Unit   = { flag_ + "gauge" }
      def gaugeOff(): Unit  = { flag_ - "gauge" }
      def deltaOn(): Unit   = { flag_ + "delta" }
      def deltaOff(): Unit  = { flag_ - "delta" }
      def numberOn(): Unit  = { flag_ + "number" }
      def numberOff(): Unit = { flag_ - "number" }

    }

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ids: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns id labels to each datum. These ids for object constancy of data points during animation. Should be an array of strings, not numbers or any other type."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val metasrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  meta .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdata: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns extra data each datum. This may be useful when listening to hover, click and selection events. Note that, *scatter* traces also appends customdata items in the markers DOM elements"""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val value: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description = Some("""Sets the number to be displayed.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdatasrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  customdata .""")
    )

    case object number extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """plot"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val suffix: PChars = PChars(
        dflt = Some(""""""),
        description = Some("""Sets a suffix appearing next to the number.""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val prefix: PChars = PChars(
        dflt = Some(""""""),
        description = Some("""Sets a prefix appearing before the number.""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val valueformat: PChars = PChars(
        dflt = Some(""""""),
        description = Some(
          """Sets the value formatting rule using d3 formatting mini-language which is similar to those of Python. See https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format"""
        )
      )

      case object font extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Set the font used to display main number"""
        val editType: String    = """plot"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v1 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val v2 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("color")
            in0.map(ve => color.fromJson(ve))

            val in1 = mapKV.get("family")
            in1.map(ve => family.fromJson(ve))

            val in2 = mapKV.get("size")
            in2.map(ve => size.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
        val v1 =
          if (suffix.json().length > 0) "\"suffix\"" + ":" + suffix.json()
          else ""
        val v2 =
          if (prefix.json().length > 0) "\"prefix\"" + ":" + prefix.json()
          else ""
        val v3 =
          if (valueformat.json().length > 0)
            "\"valueformat\"" + ":" + valueformat.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("font")
          in0.map(ve => font.fromJson(ve))

          val in1 = mapKV.get("suffix")
          in1.map(ve => suffix.fromJson(ve))

          val in2 = mapKV.get("prefix")
          in2.map(ve => prefix.fromJson(ve))

          val in3 = mapKV.get("valueformat")
          in3.map(ve => valueformat.fromJson(ve))

        }
        this
      }

    }

    case object domain extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val column: PInt = PInt(
        min = Some(0),
        max = None,
        dflt = Some(0),
        description = Some(
          """If there is a layout grid, use the domain for this column in the grid for this indicator trace ."""
        )
      )

      object y extends JSON {
        /* §#
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val description: Option[String] = Some(
          """Sets the vertical domain of this indicator trace (in plot fraction)."""
        )

        private var int: Array[Int]                 = Array.fill(2)(0)
        private var double: Array[Double]           = Array.fill(2)(0.0)
        private var selected: Map[Int, OfPrimitive] = Map()
        private val n: Integer                      = 2

        def json(): String = {
          /* §#
           * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val nulls = "null"
          val values = (0 until n) map { i =>
            val t = selected.get(i)
            t.fold(nulls)({
              case NAPrimitive =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TString(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TBoolean(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TDouble(_) => double(i).toString
              case TInt(_)    => int(i).toString
              case TImpliedEdits(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TAttrRegexps(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
            })
          }
          val allNull = values.forall(_.toLowerCase.equals(nulls))
          if (allNull)
            ""
          else {
            values.filter(_.length > 0).mkString("[", ",", "]")
          }
        }

        override def fromJson(in: Value): this.type = {
          /* §#
           * generateInfoArrayFromJson()
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          in match {
            case Arr(values) =>
              values.zipWithIndex.foreach { kv =>
                val v = kv._1
                val i = kv._2
                v match {

                  case Num(v) =>
                    if (v.isValidInt) {
                      int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                    } else { double(i) = v; selected += (i -> TDouble(v)) }

                  case Null => () // nulls are valid
                  case _    => err(in)
                }
              }
              this
            case _ =>
              err(in)
              this
          }
        }

// Set the value
        def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
        def int_0(): Unit = { selected -= 0 }

// Set the value
        def double_0(v: Double): Unit = {
          double(0) = v; selected += (0 -> TDouble(v))
        }
// Delete the value (use default)
        def double_0(): Unit = { selected -= 0 }

// Set the value
        def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
        def int_1(): Unit = { selected -= 1 }

// Set the value
        def double_1(v: Double): Unit = {
          double(1) = v; selected += (1 -> TDouble(v))
        }
// Delete the value (use default)
        def double_1(): Unit = { selected -= 1 }

      }

      object x extends JSON {
        /* §#
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val description: Option[String] = Some(
          """Sets the horizontal domain of this indicator trace (in plot fraction)."""
        )

        private var int: Array[Int]                 = Array.fill(2)(0)
        private var double: Array[Double]           = Array.fill(2)(0.0)
        private var selected: Map[Int, OfPrimitive] = Map()
        private val n: Integer                      = 2

        def json(): String = {
          /* §#
           * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val nulls = "null"
          val values = (0 until n) map { i =>
            val t = selected.get(i)
            t.fold(nulls)({
              case NAPrimitive =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TString(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TBoolean(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TDouble(_) => double(i).toString
              case TInt(_)    => int(i).toString
              case TImpliedEdits(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TAttrRegexps(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
            })
          }
          val allNull = values.forall(_.toLowerCase.equals(nulls))
          if (allNull)
            ""
          else {
            values.filter(_.length > 0).mkString("[", ",", "]")
          }
        }

        override def fromJson(in: Value): this.type = {
          /* §#
           * generateInfoArrayFromJson()
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          in match {
            case Arr(values) =>
              values.zipWithIndex.foreach { kv =>
                val v = kv._1
                val i = kv._2
                v match {

                  case Num(v) =>
                    if (v.isValidInt) {
                      int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                    } else { double(i) = v; selected += (i -> TDouble(v)) }

                  case Null => () // nulls are valid
                  case _    => err(in)
                }
              }
              this
            case _ =>
              err(in)
              this
          }
        }

// Set the value
        def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
        def int_0(): Unit = { selected -= 0 }

// Set the value
        def double_0(v: Double): Unit = {
          double(0) = v; selected += (0 -> TDouble(v))
        }
// Delete the value (use default)
        def double_0(): Unit = { selected -= 0 }

// Set the value
        def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
        def int_1(): Unit = { selected -= 1 }

// Set the value
        def double_1(v: Double): Unit = {
          double(1) = v; selected += (1 -> TDouble(v))
        }
// Delete the value (use default)
        def double_1(): Unit = { selected -= 1 }

      }

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val row: PInt = PInt(
        min = Some(0),
        max = None,
        dflt = Some(0),
        description = Some(
          """If there is a layout grid, use the domain for this row in the grid for this indicator trace ."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (column.json().length > 0) "\"column\"" + ":" + column.json()
          else ""
        val v1                 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
        val v2                 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
        val v3                 = if (row.json().length > 0) "\"row\"" + ":" + row.json() else ""
        val vars: List[String] = List(v0, v1, v2, v3)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("column")
          in0.map(ve => column.fromJson(ve))

          val in1 = mapKV.get("y")
          in1.map(ve => y.fromJson(ve))

          val in2 = mapKV.get("x")
          in2.map(ve => x.fromJson(ve))

          val in3 = mapKV.get("row")
          in3.map(ve => row.fromJson(ve))

        }
        this
      }

    }

    case object stream extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val maxpoints: PReal = PReal(
        min = Some(0.0),
        max = Some(10000.0),
        dflt = Some("""500.0"""),
        description = Some(
          """Sets the maximum number of points to keep on the plots from an incoming stream. If `maxpoints` is set to *50*, only the newest 50 points will be displayed on the plot."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val token: PChars = PChars(
        dflt = None,
        description = Some(
          """The stream id number links a data trace on a plot with a stream. See https://plot.ly/settings for more details."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (maxpoints.json().length > 0)
            "\"maxpoints\"" + ":" + maxpoints.json()
          else ""
        val v1 =
          if (token.json().length > 0) "\"token\"" + ":" + token.json() else ""
        val vars: List[String] = List(v0, v1)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("maxpoints")
          in0.map(ve => maxpoints.fromJson(ve))

          val in1 = mapKV.get("token")
          in1.map(ve => token.fromJson(ve))

        }
        this
      }

    }

    case object delta extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      object position extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """bottom"""
        val description: Option[String] = Some(
          """Sets the position of delta with respect to the number."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def right(): Unit = { selected = TString("right") }

        def left(): Unit = { selected = TString("left") }

        def bottom(): Unit = { selected = TString("bottom") }

        def top(): Unit = { selected = TString("top") }

      }

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val valueformat: PChars = PChars(
        dflt = None,
        description = Some(
          """Sets the value formatting rule using d3 formatting mini-language which is similar to those of Python. See https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format"""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val reference: PReal = PReal(
        min = None,
        max = None,
        dflt = None,
        description = Some(
          """Sets the reference value to compute the delta. By default, it is set to the current value."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val relative: PBool = PBool(
        dflt = Some(false),
        description = Some("""Show relative change""")
      )

      case object decreasing extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """plot"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = Some("""#FF4136"""),
          description = Some("""Sets the color for increasing value.""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val symbol: PChars = PChars(
          dflt = Some("""▼"""),
          description =
            Some("""Sets the symbol to display for increasing value""")
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v1 =
            if (symbol.json().length > 0) "\"symbol\"" + ":" + symbol.json()
            else ""
          val vars: List[String] = List(v0, v1)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("color")
            in0.map(ve => color.fromJson(ve))

            val in1 = mapKV.get("symbol")
            in1.map(ve => symbol.fromJson(ve))

          }
          this
        }

      }

      case object increasing extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """plot"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = Some("""#3D9970"""),
          description = Some("""Sets the color for increasing value.""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val symbol: PChars = PChars(
          dflt = Some("""▲"""),
          description =
            Some("""Sets the symbol to display for increasing value""")
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v1 =
            if (symbol.json().length > 0) "\"symbol\"" + ":" + symbol.json()
            else ""
          val vars: List[String] = List(v0, v1)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("color")
            in0.map(ve => color.fromJson(ve))

            val in1 = mapKV.get("symbol")
            in1.map(ve => symbol.fromJson(ve))

          }
          this
        }

      }

      case object font extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Set the font used to display the delta"""
        val editType: String    = """plot"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v1 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val v2 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("color")
            in0.map(ve => color.fromJson(ve))

            val in1 = mapKV.get("family")
            in1.map(ve => family.fromJson(ve))

            val in2 = mapKV.get("size")
            in2.map(ve => size.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (decreasing.json().length > 0)
            "\"decreasing\"" + ":" + decreasing.json()
          else ""
        val v1 =
          if (increasing.json().length > 0)
            "\"increasing\"" + ":" + increasing.json()
          else ""
        val v2 =
          if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
        val v3 =
          if (position.json().length > 0) "\"position\"" + ":" + position.json()
          else ""
        val v4 =
          if (valueformat.json().length > 0)
            "\"valueformat\"" + ":" + valueformat.json()
          else ""
        val v5 =
          if (reference.json().length > 0)
            "\"reference\"" + ":" + reference.json()
          else ""
        val v6 =
          if (relative.json().length > 0) "\"relative\"" + ":" + relative.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("decreasing")
          in0.map(ve => decreasing.fromJson(ve))

          val in1 = mapKV.get("increasing")
          in1.map(ve => increasing.fromJson(ve))

          val in2 = mapKV.get("font")
          in2.map(ve => font.fromJson(ve))

          val in3 = mapKV.get("position")
          in3.map(ve => position.fromJson(ve))

          val in4 = mapKV.get("valueformat")
          in4.map(ve => valueformat.fromJson(ve))

          val in5 = mapKV.get("reference")
          in5.map(ve => reference.fromJson(ve))

          val in6 = mapKV.get("relative")
          in6.map(ve => relative.fromJson(ve))

        }
        this
      }

    }

    case object gauge extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String        = """object"""
      val description: String = """The gauge of the Indicator plot."""
      val editType: String    = """plot"""

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolor: PColor = PColor(
        dflt = None,
        description = Some("""Sets the gauge background color.""")
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val borderwidth: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""1.0"""),
        description =
          Some("""Sets the width (in px) of the border enclosing the gauge.""")
      )

      object shape extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String                = """angular"""
        val description: Option[String] = Some("""Set the shape of the gauge""")

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def bullet(): Unit = { selected = TString("bullet") }

        def angular(): Unit = { selected = TString("angular") }

      }

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolor: PColor = PColor(
        dflt = Some("""#444"""),
        description =
          Some("""Sets the color of the border enclosing the gauge.""")
      )

      case object threshold extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """plot"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val value: PReal = PReal(
          min = None,
          max = None,
          dflt = Some("""false"""),
          description = Some("""Sets a treshold value drawn as a line.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val thickness: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = Some("""0.85"""),
          description = Some(
            """Sets the thickness of the threshold line as a fraction of the thickness of the gauge."""
          )
        )

        case object line extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String     = """object"""
          val editType: String = """plot"""

          /* §#
           * generateColor(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val color: PColor = PColor(
            dflt = Some("""#444"""),
            description = Some("""Sets the color of the threshold line.""")
          )

          /* §#
           * generateNumberDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val width: PReal = PReal(
            min = Some(0.0),
            max = None,
            dflt = Some("""1.0"""),
            description =
              Some("""Sets the width (in px) of the threshold line.""")
          )

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (color.json().length > 0) "\"color\"" + ":" + color.json()
              else ""
            val v1 =
              if (width.json().length > 0) "\"width\"" + ":" + width.json()
              else ""
            val vars: List[String] = List(v0, v1)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("color")
              in0.map(ve => color.fromJson(ve))

              val in1 = mapKV.get("width")
              in1.map(ve => width.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
          val v1 =
            if (value.json().length > 0) "\"value\"" + ":" + value.json()
            else ""
          val v2 =
            if (thickness.json().length > 0)
              "\"thickness\"" + ":" + thickness.json()
            else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("line")
            in0.map(ve => line.fromJson(ve))

            val in1 = mapKV.get("value")
            in1.map(ve => value.fromJson(ve))

            val in2 = mapKV.get("thickness")
            in2.map(ve => thickness.fromJson(ve))

          }
          this
        }

      }

      case object axis extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """plot"""

        /* §#
         * generateAny(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tick0: PAny = PAny(
          dflt = None,
          description = Some(
            """Sets the placement of the first tick on this axis. Use with `dtick`. If the axis `type` is *log*, then you must take the log of your starting tick (e.g. to set the starting tick to 100, set the `tick0` to 2) except when `dtick`=*L<f>* (see `dtick` for more info). If the axis `type` is *date*, it should be a date string, like date data. If the axis `type` is *category*, it should be a number, using the scale where each category is assigned a serial number from zero in the order it appears."""
          )
        )

        /* §#
         * generateIntDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val nticks: PInt = PInt(
          min = Some(0),
          max = None,
          dflt = Some(0),
          description = Some(
            """Specifies the maximum number of ticks for the particular axis. The actual number of ticks will be chosen automatically to be less than or equal to `nticks`. Has an effect only if `tickmode` is set to *auto*."""
          )
        )

        /* §#
         * generateAngleDeclaration(in: PrimitiveDefinition
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickangle: PAngle = PAngle(
          dflt = Some("""auto"""),
          description = Some(
            """Sets the angle of the tick labels with respect to the horizontal. For example, a `tickangle` of -90 draws the tick labels vertically."""
          )
        )

        object showexponent extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """all"""
          val description: Option[String] = Some(
            """If *all*, all exponents are shown besides their significands. If *first*, only the exponent of the first tick is shown. If *last*, only the exponent of the last tick is shown. If *none*, no exponents appear."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def none(): Unit = { selected = TString("none") }

          def last(): Unit = { selected = TString("last") }

          def first(): Unit = { selected = TString("first") }

          def all(): Unit = { selected = TString("all") }

        }

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val visible: PBool = PBool(
          dflt = Some(true),
          description = Some(
            """A single toggle to hide the axis while preserving interaction like dragging. Default is true when a cheater plot is present on the axis, otherwise false"""
          )
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticksuffix: PChars = PChars(
          dflt = Some(""""""),
          description = Some("""Sets a tick label suffix.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickwidth: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""1.0"""),
          description = Some("""Sets the tick width (in px).""")
        )

        /* §#
         * generateDataArrayDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickvals: DataArray = DataArray(
          dflt = None,
          description = Some(
            """Sets the values at which ticks on this axis appear. Only has an effect if `tickmode` is set to *array*. Used with `ticktext`."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticklen: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""5.0"""),
          description = Some("""Sets the tick length (in px).""")
        )

        object tickmode extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val description: Option[String] = Some(
            """Sets the tick mode for this axis. If *auto*, the number of ticks is set via `nticks`. If *linear*, the placement of the ticks is determined by a starting position `tick0` and a tick step `dtick` (*linear* is the default value if `tick0` and `dtick` are provided). If *array*, the placement of the ticks is set via `tickvals` and the tick text is `ticktext`. (*array* is the default value if `tickvals` is provided)."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def array(): Unit = { selected = TString("array") }

          def linear(): Unit = { selected = TString("linear") }

          def auto(): Unit = { selected = TString("auto") }

        }

        /* §#
         * generateAny(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dtick: PAny = PAny(
          dflt = None,
          description = Some(
            """Sets the step in-between ticks on this axis. Use with `tick0`. Must be a positive number, or special strings available to *log* and *date* axes. If the axis `type` is *log*, then ticks are set every 10^(n*dtick) where n is the tick number. For example, to set a tick mark at 1, 10, 100, 1000, ... set dtick to 1. To set tick marks at 1, 100, 10000, ... set dtick to 2. To set tick marks at 1, 5, 25, 125, 625, 3125, ... set dtick to log_10(5), or 0.69897000433. *log* has several special values; *L<f>*, where `f` is a positive number, gives ticks linearly spaced in value (but not position). For example `tick0` = 0.1, `dtick` = *L0.5* will put ticks at 0.1, 0.6, 1.1, 1.6 etc. To show powers of 10 plus small digits between, use *D1* (all digits) or *D2* (only 2 and 5). `tick0` is ignored for *D1* and *D2*. If the axis `type` is *date*, then you must convert the time to milliseconds. For example, to set the interval between ticks to one day, set `dtick` to 86400000.0. *date* also has special values *M<n>* gives ticks spaced by a number of months. `n` must be a positive integer. To set ticks on the 15th of every third month, set `tick0` to *2000-01-15* and `dtick` to *M3*. To set ticks every 4 years, set `dtick` to *M48*"""
          )
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickvalssrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  tickvals .""")
        )

        object range extends JSON {
          /* §#
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val description: Option[String] = Some(
            """Sets the range of this axis."""
          )

          private var int: Array[Int]                 = Array.fill(2)(0)
          private var double: Array[Double]           = Array.fill(2)(0.0)
          private var selected: Map[Int, OfPrimitive] = Map()
          private val n: Integer                      = 2

          def json(): String = {
            /* §#
             * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
             * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val nulls = "null"
            val values = (0 until n) map { i =>
              val t = selected.get(i)
              t.fold(nulls)({
                case NAPrimitive =>
                  throw new RuntimeException(s"Unexpected type: $t")
                case TString(_) =>
                  throw new RuntimeException(s"Unexpected type: $t")
                case TBoolean(_) =>
                  throw new RuntimeException(s"Unexpected type: $t")
                case TDouble(_) => double(i).toString
                case TInt(_)    => int(i).toString
                case TImpliedEdits(_) =>
                  throw new RuntimeException(s"Unexpected type: $t")
                case TAttrRegexps(_) =>
                  throw new RuntimeException(s"Unexpected type: $t")
              })
            }
            val allNull = values.forall(_.toLowerCase.equals(nulls))
            if (allNull)
              ""
            else {
              values.filter(_.length > 0).mkString("[", ",", "]")
            }
          }

          override def fromJson(in: Value): this.type = {
            /* §#
             * generateInfoArrayFromJson()
             * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            in match {
              case Arr(values) =>
                values.zipWithIndex.foreach { kv =>
                  val v = kv._1
                  val i = kv._2
                  v match {

                    case Num(v) =>
                      if (v.isValidInt) {
                        int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                      } else { double(i) = v; selected += (i -> TDouble(v)) }

                    case Null => () // nulls are valid
                    case _    => err(in)
                  }
                }
                this
              case _ =>
                err(in)
                this
            }
          }

// Set the value
          def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
          def int_0(): Unit = { selected -= 0 }

// Set the value
          def double_0(v: Double): Unit = {
            double(0) = v; selected += (0 -> TDouble(v))
          }
// Delete the value (use default)
          def double_0(): Unit = { selected -= 0 }

// Set the value
          def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
          def int_1(): Unit = { selected -= 1 }

// Set the value
          def double_1(v: Double): Unit = {
            double(1) = v; selected += (1 -> TDouble(v))
          }
// Delete the value (use default)
          def double_1(): Unit = { selected -= 1 }

        }

        object showticksuffix extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """all"""
          val description: Option[String] = Some(
            """Same as `showtickprefix` but for tick suffixes."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def none(): Unit = { selected = TString("none") }

          def last(): Unit = { selected = TString("last") }

          def first(): Unit = { selected = TString("first") }

          def all(): Unit = { selected = TString("all") }

        }

        /* §#
         * generateDataArrayDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticktext: DataArray = DataArray(
          dflt = None,
          description = Some(
            """Sets the text displayed at the ticks position via `tickvals`. Only has an effect if `tickmode` is set to *array*. Used with `tickvals`."""
          )
        )

        object ticks extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """outside"""
          val description: Option[String] = Some(
            """Determines whether ticks are drawn or not. If **, this axis' ticks are not drawn. If *outside* (*inside*), this axis' are drawn outside (inside) the axis lines."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def default(): Unit = { selected = TString("") }

          def inside(): Unit = { selected = TString("inside") }

          def outside(): Unit = { selected = TString("outside") }

        }

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickcolor: PColor = PColor(
          dflt = Some("""#444"""),
          description = Some("""Sets the tick color.""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticktextsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  ticktext .""")
        )

        object exponentformat extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """B"""
          val description: Option[String] = Some(
            """Determines a formatting rule for the tick exponents. For example, consider the number 1,000,000,000. If *none*, it appears as 1,000,000,000. If *e*, 1e+9. If *E*, 1E+9. If *power*, 1x10^9 (with 9 in a super script). If *SI*, 1G. If *B*, 1B."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def B(): Unit = { selected = TString("B") }

          def SI(): Unit = { selected = TString("SI") }

          def power(): Unit = { selected = TString("power") }

          def E(): Unit = { selected = TString("E") }

          def e(): Unit = { selected = TString("e") }

          def none(): Unit = { selected = TString("none") }

        }

        object showtickprefix extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """all"""
          val description: Option[String] = Some(
            """If *all*, all tick labels are displayed with a prefix. If *first*, only the first tick is displayed with a prefix. If *last*, only the last tick is displayed with a suffix. If *none*, tick prefixes are hidden."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def none(): Unit = { selected = TString("none") }

          def last(): Unit = { selected = TString("last") }

          def first(): Unit = { selected = TString("first") }

          def all(): Unit = { selected = TString("all") }

        }

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickprefix: PChars = PChars(
          dflt = Some(""""""),
          description = Some("""Sets a tick label prefix.""")
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val separatethousands: PBool = PBool(
          dflt = Some(false),
          description =
            Some("""If "true", even 4-digit integers are separated""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickformat: PChars = PChars(
          dflt = Some(""""""),
          description = Some(
            """Sets the tick label formatting rule using d3 formatting mini-languages which are very similar to those in Python. For numbers, see: https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format And for dates see: https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format We add one item to d3's date formatter: *%{n}f* for fractional seconds with n digits. For example, *2016-10-13 09:15:23.456* with tickformat *%H~%M~%S.%2f* would display *09~15~23.46*"""
          )
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val showticklabels: PBool = PBool(
          dflt = Some(true),
          description =
            Some("""Determines whether or not the tick labels are drawn.""")
        )

        case object tickfont extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String        = """object"""
          val description: String = """Sets the color bar's tick label font"""
          val editType: String    = """plot"""

          /* §#
           * generateColor(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val color: PColor = PColor(
            dflt = None,
            description = None
          )

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val family: PChars = PChars(
            dflt = None,
            description = Some(
              """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
            )
          )

          /* §#
           * generateNumberDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val size: PReal =
            PReal(min = Some(1.0), max = None, dflt = None, description = None)

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (color.json().length > 0) "\"color\"" + ":" + color.json()
              else ""
            val v1 =
              if (family.json().length > 0) "\"family\"" + ":" + family.json()
              else ""
            val v2 =
              if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
            val vars: List[String] = List(v0, v1, v2)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("color")
              in0.map(ve => color.fromJson(ve))

              val in1 = mapKV.get("family")
              in1.map(ve => family.fromJson(ve))

              val in2 = mapKV.get("size")
              in2.map(ve => size.fromJson(ve))

            }
            this
          }

        }

        case object tickformatstops extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""

          case object items extends JSON {
            /* §#
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            case object tickformatstop extends JSON {
              /* §#
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val role: String     = """object"""
              val editType: String = """plot"""

              object dtickrange extends JSON {
                /* §#
                 * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                val description: Option[String] = Some(
                  """range [*min*, *max*], where *min*, *max* - dtick values which describe some zoom level, it is possible to omit *min* or *max* value by passing *null*"""
                )

                private var string: Array[String]           = Array.fill(2)("")
                private var boolean: Array[Boolean]         = Array.fill(2)(false)
                private var double: Array[Double]           = Array.fill(2)(0.0)
                private var int: Array[Int]                 = Array.fill(2)(0)
                private var selected: Map[Int, OfPrimitive] = Map()
                private val n: Integer                      = 2

                def json(): String = {
                  /* §#
                   * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
                   * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generate(obj: CompoundDefinition)
                   * generate(obj: Either[Error, CompoundDefinition])*/
                  val nulls = "null"
                  val values = (0 until n) map { i =>
                    val t = selected.get(i)
                    t.fold(nulls)({
                      case NAPrimitive =>
                        throw new RuntimeException(s"Unexpected type: $t")
                      case TString(_)  => string(i)
                      case TBoolean(_) => boolean(i).toString
                      case TDouble(_)  => double(i).toString
                      case TInt(_)     => int(i).toString
                      case TImpliedEdits(_) =>
                        throw new RuntimeException(s"Unexpected type: $t")
                      case TAttrRegexps(_) =>
                        throw new RuntimeException(s"Unexpected type: $t")
                    })
                  }
                  val allNull = values.forall(_.toLowerCase.equals(nulls))
                  if (allNull)
                    ""
                  else {
                    values.filter(_.length > 0).mkString("[", ",", "]")
                  }
                }

                override def fromJson(in: Value): this.type = {
                  /* §#
                   * generateInfoArrayFromJson()
                   * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generate(obj: CompoundDefinition)
                   * generate(obj: Either[Error, CompoundDefinition])*/
                  def err(j: ujson.Value): OfPrimitive =
                    throw new RuntimeException(s"""Parsing of $j failed: $in""")
                  in match {
                    case Arr(values) =>
                      values.zipWithIndex.foreach { kv =>
                        val v = kv._1
                        val i = kv._2
                        v match {
                          case Bool(v) =>
                            boolean(i) = v; selected += (i -> TBoolean(v))
                          case Str(v) =>
                            string(i) = v; selected += (i -> TString(v))

                          case Num(v) =>
                            if (v.isValidInt) {
                              int(i) = v.toInt; selected += (i -> TInt(v.toInt))
                            } else {
                              double(i) = v; selected += (i -> TDouble(v))
                            }

                          case Null => () // nulls are valid
                          case _    => err(in)
                        }
                      }
                      this
                    case _ =>
                      err(in)
                      this
                  }
                }

// Set the value
                def string_0(v: String): Unit = {
                  string(0) = v; selected += (0 -> TString(v))
                }
// Delete the value (use default)
                def string_0(): Unit = { selected -= 0 }

// Set the value
                def boolean_0(v: Boolean): Unit = {
                  boolean(0) = v; selected += (0 -> TBoolean(v))
                }
// Delete the value (use default)
                def boolean_0(): Unit = { selected -= 0 }

// Set the value
                def double_0(v: Double): Unit = {
                  double(0) = v; selected += (0 -> TDouble(v))
                }
// Delete the value (use default)
                def double_0(): Unit = { selected -= 0 }

// Set the value
                def int_0(v: Int): Unit = {
                  int(0) = v; selected += (0 -> TInt(v))
                }
// Delete the value (use default)
                def int_0(): Unit = { selected -= 0 }

// Set the value
                def string_1(v: String): Unit = {
                  string(1) = v; selected += (1 -> TString(v))
                }
// Delete the value (use default)
                def string_1(): Unit = { selected -= 1 }

// Set the value
                def boolean_1(v: Boolean): Unit = {
                  boolean(1) = v; selected += (1 -> TBoolean(v))
                }
// Delete the value (use default)
                def boolean_1(): Unit = { selected -= 1 }

// Set the value
                def double_1(v: Double): Unit = {
                  double(1) = v; selected += (1 -> TDouble(v))
                }
// Delete the value (use default)
                def double_1(): Unit = { selected -= 1 }

// Set the value
                def int_1(v: Int): Unit = {
                  int(1) = v; selected += (1 -> TInt(v))
                }
// Delete the value (use default)
                def int_1(): Unit = { selected -= 1 }

              }

              /* §#
               * generateStringDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val name: PChars = PChars(
                dflt = None,
                description = Some(
                  """When used in a template, named items are created in the output figure in addition to any items the figure already has in this array. You can modify these items in the output figure by making your own item with `templateitemname` matching this `name` alongside your modifications (including `visible: false` or `enabled: false` to hide it). Has no effect outside of a template."""
                )
              )

              /* §#
               * generateBooleanDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val enabled: PBool = PBool(
                dflt = Some(true),
                description = Some(
                  """Determines whether or not this stop is used. If `false`, this stop is ignored even within its `dtickrange`."""
                )
              )

              /* §#
               * generateStringDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val value: PChars = PChars(
                dflt = Some(""""""),
                description = Some(
                  """string - dtickformat for described zoom level, the same as *tickformat*"""
                )
              )

              /* §#
               * generateStringDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val templateitemname: PChars = PChars(
                dflt = None,
                description = Some(
                  """Used to refer to a named item in this array in the template. Named items from the template will be created even without a matching item in the input figure, but you can modify one by making an item with `templateitemname` matching its `name`, alongside your modifications (including `visible: false` or `enabled: false` to hide it). If there is no template or no matching item, this item will be hidden unless you explicitly show it with `visible: true`."""
                )
              )

              def json(): String = {
                /* §#
                 * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
                 * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/

                val v0 =
                  if (dtickrange.json().length > 0)
                    "\"dtickrange\"" + ":" + dtickrange.json()
                  else ""
                val v1 =
                  if (name.json().length > 0) "\"name\"" + ":" + name.json()
                  else ""
                val v2 =
                  if (enabled.json().length > 0)
                    "\"enabled\"" + ":" + enabled.json()
                  else ""
                val v3 =
                  if (value.json().length > 0) "\"value\"" + ":" + value.json()
                  else ""
                val v4 =
                  if (templateitemname.json().length > 0)
                    "\"templateitemname\"" + ":" + templateitemname.json()
                  else ""
                val vars: List[String] = List(v0, v1, v2, v3, v4)
                val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
                if (cleanVars.length > 0) {
                  // values set
                  "{" + cleanVars + "}"
                } else {
                  // no values set
                  ""
                }
              }

              def fromJson(in: Value): this.type = {
                /* §#
                 * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
                 * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                def err =
                  throw new RuntimeException(s"Expected an Obj but got $in")
                in.objOpt.fold(err) { mapKV =>
                  val in0 = mapKV.get("dtickrange")
                  in0.map(ve => dtickrange.fromJson(ve))

                  val in1 = mapKV.get("name")
                  in1.map(ve => name.fromJson(ve))

                  val in2 = mapKV.get("enabled")
                  in2.map(ve => enabled.fromJson(ve))

                  val in3 = mapKV.get("value")
                  in3.map(ve => value.fromJson(ve))

                  val in4 = mapKV.get("templateitemname")
                  in4.map(ve => templateitemname.fromJson(ve))

                }
                this
              }

            }

            def json(): String = {
              /* §#
               * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/

              val v0 =
                if (tickformatstop.json().length > 0)
                  "\"tickformatstop\"" + ":" + tickformatstop.json()
                else ""
              val vars: List[String] = List(v0)
              val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
              if (cleanVars.length > 0) {
                // values set
                "{" + cleanVars + "}"
              } else {
                // no values set
                ""
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err =
                throw new RuntimeException(s"Expected an Obj but got $in")
              in.objOpt.fold(err) { mapKV =>
                val in0 = mapKV.get("tickformatstop")
                in0.map(ve => tickformatstop.fromJson(ve))

              }
              this
            }

          }

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (items.json().length > 0) "\"items\"" + ":" + items.json()
              else ""
            val vars: List[String] = List(v0)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("items")
              in0.map(ve => items.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (tickfont.json().length > 0)
              "\"tickfont\"" + ":" + tickfont.json()
            else ""
          val v1 =
            if (tickformatstops.json().length > 0)
              "\"tickformatstops\"" + ":" + tickformatstops.json()
            else ""
          val v2 =
            if (tick0.json().length > 0) "\"tick0\"" + ":" + tick0.json()
            else ""
          val v3 =
            if (nticks.json().length > 0) "\"nticks\"" + ":" + nticks.json()
            else ""
          val v4 =
            if (tickangle.json().length > 0)
              "\"tickangle\"" + ":" + tickangle.json()
            else ""
          val v5 =
            if (showexponent.json().length > 0)
              "\"showexponent\"" + ":" + showexponent.json()
            else ""
          val v6 =
            if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
            else ""
          val v7 =
            if (ticksuffix.json().length > 0)
              "\"ticksuffix\"" + ":" + ticksuffix.json()
            else ""
          val v8 =
            if (tickwidth.json().length > 0)
              "\"tickwidth\"" + ":" + tickwidth.json()
            else ""
          val v9 =
            if (tickvals.json().length > 0)
              "\"tickvals\"" + ":" + tickvals.json()
            else ""
          val v10 =
            if (ticklen.json().length > 0) "\"ticklen\"" + ":" + ticklen.json()
            else ""
          val v11 =
            if (tickmode.json().length > 0)
              "\"tickmode\"" + ":" + tickmode.json()
            else ""
          val v12 =
            if (dtick.json().length > 0) "\"dtick\"" + ":" + dtick.json()
            else ""
          val v13 =
            if (tickvalssrc.json().length > 0)
              "\"tickvalssrc\"" + ":" + tickvalssrc.json()
            else ""
          val v14 =
            if (range.json().length > 0) "\"range\"" + ":" + range.json()
            else ""
          val v15 =
            if (showticksuffix.json().length > 0)
              "\"showticksuffix\"" + ":" + showticksuffix.json()
            else ""
          val v16 =
            if (ticktext.json().length > 0)
              "\"ticktext\"" + ":" + ticktext.json()
            else ""
          val v17 =
            if (ticks.json().length > 0) "\"ticks\"" + ":" + ticks.json()
            else ""
          val v18 =
            if (tickcolor.json().length > 0)
              "\"tickcolor\"" + ":" + tickcolor.json()
            else ""
          val v19 =
            if (ticktextsrc.json().length > 0)
              "\"ticktextsrc\"" + ":" + ticktextsrc.json()
            else ""
          val v20 =
            if (exponentformat.json().length > 0)
              "\"exponentformat\"" + ":" + exponentformat.json()
            else ""
          val v21 =
            if (showtickprefix.json().length > 0)
              "\"showtickprefix\"" + ":" + showtickprefix.json()
            else ""
          val v22 =
            if (tickprefix.json().length > 0)
              "\"tickprefix\"" + ":" + tickprefix.json()
            else ""
          val v23 =
            if (separatethousands.json().length > 0)
              "\"separatethousands\"" + ":" + separatethousands.json()
            else ""
          val v24 =
            if (tickformat.json().length > 0)
              "\"tickformat\"" + ":" + tickformat.json()
            else ""
          val v25 =
            if (showticklabels.json().length > 0)
              "\"showticklabels\"" + ":" + showticklabels.json()
            else ""
          val vars: List[String] = List(
            v0,
            v1,
            v2,
            v3,
            v4,
            v5,
            v6,
            v7,
            v8,
            v9,
            v10,
            v11,
            v12,
            v13,
            v14,
            v15,
            v16,
            v17,
            v18,
            v19,
            v20,
            v21,
            v22,
            v23,
            v24,
            v25
          )
          val cleanVars = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("tickfont")
            in0.map(ve => tickfont.fromJson(ve))

            val in1 = mapKV.get("tickformatstops")
            in1.map(ve => tickformatstops.fromJson(ve))

            val in2 = mapKV.get("tick0")
            in2.map(ve => tick0.fromJson(ve))

            val in3 = mapKV.get("nticks")
            in3.map(ve => nticks.fromJson(ve))

            val in4 = mapKV.get("tickangle")
            in4.map(ve => tickangle.fromJson(ve))

            val in5 = mapKV.get("showexponent")
            in5.map(ve => showexponent.fromJson(ve))

            val in6 = mapKV.get("visible")
            in6.map(ve => visible.fromJson(ve))

            val in7 = mapKV.get("ticksuffix")
            in7.map(ve => ticksuffix.fromJson(ve))

            val in8 = mapKV.get("tickwidth")
            in8.map(ve => tickwidth.fromJson(ve))

            val in9 = mapKV.get("tickvals")
            in9.map(ve => tickvals.fromJson(ve))

            val in10 = mapKV.get("ticklen")
            in10.map(ve => ticklen.fromJson(ve))

            val in11 = mapKV.get("tickmode")
            in11.map(ve => tickmode.fromJson(ve))

            val in12 = mapKV.get("dtick")
            in12.map(ve => dtick.fromJson(ve))

            val in13 = mapKV.get("tickvalssrc")
            in13.map(ve => tickvalssrc.fromJson(ve))

            val in14 = mapKV.get("range")
            in14.map(ve => range.fromJson(ve))

            val in15 = mapKV.get("showticksuffix")
            in15.map(ve => showticksuffix.fromJson(ve))

            val in16 = mapKV.get("ticktext")
            in16.map(ve => ticktext.fromJson(ve))

            val in17 = mapKV.get("ticks")
            in17.map(ve => ticks.fromJson(ve))

            val in18 = mapKV.get("tickcolor")
            in18.map(ve => tickcolor.fromJson(ve))

            val in19 = mapKV.get("ticktextsrc")
            in19.map(ve => ticktextsrc.fromJson(ve))

            val in20 = mapKV.get("exponentformat")
            in20.map(ve => exponentformat.fromJson(ve))

            val in21 = mapKV.get("showtickprefix")
            in21.map(ve => showtickprefix.fromJson(ve))

            val in22 = mapKV.get("tickprefix")
            in22.map(ve => tickprefix.fromJson(ve))

            val in23 = mapKV.get("separatethousands")
            in23.map(ve => separatethousands.fromJson(ve))

            val in24 = mapKV.get("tickformat")
            in24.map(ve => tickformat.fromJson(ve))

            val in25 = mapKV.get("showticklabels")
            in25.map(ve => showticklabels.fromJson(ve))

          }
          this
        }

      }

      case object steps extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String = """object"""

        case object items extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          case object step extends JSON {
            /* §#
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val role: String     = """object"""
            val editType: String = """calc"""

            /* §#
             * generateStringDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val name: PChars = PChars(
              dflt = None,
              description = Some(
                """When used in a template, named items are created in the output figure in addition to any items the figure already has in this array. You can modify these items in the output figure by making your own item with `templateitemname` matching this `name` alongside your modifications (including `visible: false` or `enabled: false` to hide it). Has no effect outside of a template."""
              )
            )

            /* §#
             * generateColor(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val color: PColor = PColor(
              dflt = None,
              description = Some("""Sets the background color of the arc.""")
            )

            /* §#
             * generateNumberDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val thickness: PReal = PReal(
              min = Some(0.0),
              max = Some(1.0),
              dflt = Some("""1.0"""),
              description = Some(
                """Sets the thickness of the bar as a fraction of the total thickness of the gauge."""
              )
            )

            object range extends JSON {
              /* §#
               * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val description: Option[String] = Some(
                """Sets the range of this axis."""
              )

              private var int: Array[Int]                 = Array.fill(2)(0)
              private var double: Array[Double]           = Array.fill(2)(0.0)
              private var selected: Map[Int, OfPrimitive] = Map()
              private val n: Integer                      = 2

              def json(): String = {
                /* §#
                 * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
                 * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                val nulls = "null"
                val values = (0 until n) map { i =>
                  val t = selected.get(i)
                  t.fold(nulls)({
                    case NAPrimitive =>
                      throw new RuntimeException(s"Unexpected type: $t")
                    case TString(_) =>
                      throw new RuntimeException(s"Unexpected type: $t")
                    case TBoolean(_) =>
                      throw new RuntimeException(s"Unexpected type: $t")
                    case TDouble(_) => double(i).toString
                    case TInt(_)    => int(i).toString
                    case TImpliedEdits(_) =>
                      throw new RuntimeException(s"Unexpected type: $t")
                    case TAttrRegexps(_) =>
                      throw new RuntimeException(s"Unexpected type: $t")
                  })
                }
                val allNull = values.forall(_.toLowerCase.equals(nulls))
                if (allNull)
                  ""
                else {
                  values.filter(_.length > 0).mkString("[", ",", "]")
                }
              }

              override def fromJson(in: Value): this.type = {
                /* §#
                 * generateInfoArrayFromJson()
                 * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                def err(j: ujson.Value): OfPrimitive =
                  throw new RuntimeException(s"""Parsing of $j failed: $in""")
                in match {
                  case Arr(values) =>
                    values.zipWithIndex.foreach { kv =>
                      val v = kv._1
                      val i = kv._2
                      v match {

                        case Num(v) =>
                          if (v.isValidInt) {
                            int(i) = v.toInt; selected += (i -> TInt(v.toInt))
                          } else {
                            double(i) = v; selected += (i -> TDouble(v))
                          }

                        case Null => () // nulls are valid
                        case _    => err(in)
                      }
                    }
                    this
                  case _ =>
                    err(in)
                    this
                }
              }

// Set the value
              def int_0(v: Int): Unit = {
                int(0) = v; selected += (0 -> TInt(v))
              }
// Delete the value (use default)
              def int_0(): Unit = { selected -= 0 }

// Set the value
              def double_0(v: Double): Unit = {
                double(0) = v; selected += (0 -> TDouble(v))
              }
// Delete the value (use default)
              def double_0(): Unit = { selected -= 0 }

// Set the value
              def int_1(v: Int): Unit = {
                int(1) = v; selected += (1 -> TInt(v))
              }
// Delete the value (use default)
              def int_1(): Unit = { selected -= 1 }

// Set the value
              def double_1(v: Double): Unit = {
                double(1) = v; selected += (1 -> TDouble(v))
              }
// Delete the value (use default)
              def double_1(): Unit = { selected -= 1 }

            }

            /* §#
             * generateStringDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val templateitemname: PChars = PChars(
              dflt = None,
              description = Some(
                """Used to refer to a named item in this array in the template. Named items from the template will be created even without a matching item in the input figure, but you can modify one by making an item with `templateitemname` matching its `name`, alongside your modifications (including `visible: false` or `enabled: false` to hide it). If there is no template or no matching item, this item will be hidden unless you explicitly show it with `visible: true`."""
              )
            )

            case object line extends JSON {
              /* §#
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val role: String     = """object"""
              val editType: String = """calc"""

              /* §#
               * generateColor(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val color: PColor = PColor(
                dflt = Some("""#444"""),
                description =
                  Some("""Sets the color of the line enclosing each sector.""")
              )

              /* §#
               * generateNumberDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val width: PReal = PReal(
                min = Some(0.0),
                max = None,
                dflt = Some("""0.0"""),
                description = Some(
                  """Sets the width (in px) of the line enclosing each sector."""
                )
              )

              def json(): String = {
                /* §#
                 * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
                 * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/

                val v0 =
                  if (color.json().length > 0) "\"color\"" + ":" + color.json()
                  else ""
                val v1 =
                  if (width.json().length > 0) "\"width\"" + ":" + width.json()
                  else ""
                val vars: List[String] = List(v0, v1)
                val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
                if (cleanVars.length > 0) {
                  // values set
                  "{" + cleanVars + "}"
                } else {
                  // no values set
                  ""
                }
              }

              def fromJson(in: Value): this.type = {
                /* §#
                 * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
                 * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                def err =
                  throw new RuntimeException(s"Expected an Obj but got $in")
                in.objOpt.fold(err) { mapKV =>
                  val in0 = mapKV.get("color")
                  in0.map(ve => color.fromJson(ve))

                  val in1 = mapKV.get("width")
                  in1.map(ve => width.fromJson(ve))

                }
                this
              }

            }

            def json(): String = {
              /* §#
               * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/

              val v0 =
                if (line.json().length > 0) "\"line\"" + ":" + line.json()
                else ""
              val v1 =
                if (name.json().length > 0) "\"name\"" + ":" + name.json()
                else ""
              val v2 =
                if (color.json().length > 0) "\"color\"" + ":" + color.json()
                else ""
              val v3 =
                if (thickness.json().length > 0)
                  "\"thickness\"" + ":" + thickness.json()
                else ""
              val v4 =
                if (range.json().length > 0) "\"range\"" + ":" + range.json()
                else ""
              val v5 =
                if (templateitemname.json().length > 0)
                  "\"templateitemname\"" + ":" + templateitemname.json()
                else ""
              val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
              val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
              if (cleanVars.length > 0) {
                // values set
                "{" + cleanVars + "}"
              } else {
                // no values set
                ""
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err =
                throw new RuntimeException(s"Expected an Obj but got $in")
              in.objOpt.fold(err) { mapKV =>
                val in0 = mapKV.get("line")
                in0.map(ve => line.fromJson(ve))

                val in1 = mapKV.get("name")
                in1.map(ve => name.fromJson(ve))

                val in2 = mapKV.get("color")
                in2.map(ve => color.fromJson(ve))

                val in3 = mapKV.get("thickness")
                in3.map(ve => thickness.fromJson(ve))

                val in4 = mapKV.get("range")
                in4.map(ve => range.fromJson(ve))

                val in5 = mapKV.get("templateitemname")
                in5.map(ve => templateitemname.fromJson(ve))

              }
              this
            }

          }

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (step.json().length > 0) "\"step\"" + ":" + step.json() else ""
            val vars: List[String] = List(v0)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("step")
              in0.map(ve => step.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (items.json().length > 0) "\"items\"" + ":" + items.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("items")
            in0.map(ve => items.fromJson(ve))

          }
          this
        }

      }

      case object bar extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Set the appearance of the gauge's value"""
        val editType: String    = """calc"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = Some("""green"""),
          description = Some("""Sets the background color of the arc.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val thickness: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the thickness of the bar as a fraction of the total thickness of the gauge."""
          )
        )

        case object line extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String     = """object"""
          val editType: String = """calc"""

          /* §#
           * generateColor(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val color: PColor = PColor(
            dflt = Some("""#444"""),
            description =
              Some("""Sets the color of the line enclosing each sector.""")
          )

          /* §#
           * generateNumberDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val width: PReal = PReal(
            min = Some(0.0),
            max = None,
            dflt = Some("""0.0"""),
            description = Some(
              """Sets the width (in px) of the line enclosing each sector."""
            )
          )

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (color.json().length > 0) "\"color\"" + ":" + color.json()
              else ""
            val v1 =
              if (width.json().length > 0) "\"width\"" + ":" + width.json()
              else ""
            val vars: List[String] = List(v0, v1)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("color")
              in0.map(ve => color.fromJson(ve))

              val in1 = mapKV.get("width")
              in1.map(ve => width.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
          val v1 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v2 =
            if (thickness.json().length > 0)
              "\"thickness\"" + ":" + thickness.json()
            else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("line")
            in0.map(ve => line.fromJson(ve))

            val in1 = mapKV.get("color")
            in1.map(ve => color.fromJson(ve))

            val in2 = mapKV.get("thickness")
            in2.map(ve => thickness.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (threshold.json().length > 0)
            "\"threshold\"" + ":" + threshold.json()
          else ""
        val v1 =
          if (axis.json().length > 0) "\"axis\"" + ":" + axis.json() else ""
        val v2 =
          if (steps.json().length > 0) "\"steps\"" + ":" + steps.json() else ""
        val v3 = if (bar.json().length > 0) "\"bar\"" + ":" + bar.json() else ""
        val v4 =
          if (bgcolor.json().length > 0) "\"bgcolor\"" + ":" + bgcolor.json()
          else ""
        val v5 =
          if (borderwidth.json().length > 0)
            "\"borderwidth\"" + ":" + borderwidth.json()
          else ""
        val v6 =
          if (shape.json().length > 0) "\"shape\"" + ":" + shape.json() else ""
        val v7 =
          if (bordercolor.json().length > 0)
            "\"bordercolor\"" + ":" + bordercolor.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6, v7)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("threshold")
          in0.map(ve => threshold.fromJson(ve))

          val in1 = mapKV.get("axis")
          in1.map(ve => axis.fromJson(ve))

          val in2 = mapKV.get("steps")
          in2.map(ve => steps.fromJson(ve))

          val in3 = mapKV.get("bar")
          in3.map(ve => bar.fromJson(ve))

          val in4 = mapKV.get("bgcolor")
          in4.map(ve => bgcolor.fromJson(ve))

          val in5 = mapKV.get("borderwidth")
          in5.map(ve => borderwidth.fromJson(ve))

          val in6 = mapKV.get("shape")
          in6.map(ve => shape.fromJson(ve))

          val in7 = mapKV.get("bordercolor")
          in7.map(ve => bordercolor.fromJson(ve))

        }
        this
      }

    }

    case object transforms extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""

      case object items extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        case object transform extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""
          val description: String =
            """An array of operations that manipulate the trace data, for example filtering or sorting the data arrays."""
          val editType: String = """calc"""

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val vars: List[String] = List()
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV => }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (transform.json().length > 0)
              "\"transform\"" + ":" + transform.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("transform")
            in0.map(ve => transform.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (items.json().length > 0) "\"items\"" + ":" + items.json() else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("items")
          in0.map(ve => items.fromJson(ve))

        }
        this
      }

    }

    case object title extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """plot"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val text: PChars = PChars(
        dflt = None,
        description = Some("""Sets the title of this indicator.""")
      )

      object align extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val description: Option[String] = Some(
          """Sets the horizontal alignment of the title. It defaults to `center` except for bullet charts for which it defaults to right."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def right(): Unit = { selected = TString("right") }

        def center(): Unit = { selected = TString("center") }

        def left(): Unit = { selected = TString("left") }

      }

      case object font extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Set the font used to display the title"""
        val editType: String    = """plot"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v1 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val v2 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("color")
            in0.map(ve => color.fromJson(ve))

            val in1 = mapKV.get("family")
            in1.map(ve => family.fromJson(ve))

            val in2 = mapKV.get("size")
            in2.map(ve => size.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
        val v1 =
          if (text.json().length > 0) "\"text\"" + ":" + text.json() else ""
        val v2 =
          if (align.json().length > 0) "\"align\"" + ":" + align.json() else ""
        val vars: List[String] = List(v0, v1, v2)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("font")
          in0.map(ve => font.fromJson(ve))

          val in1 = mapKV.get("text")
          in1.map(ve => text.fromJson(ve))

          val in2 = mapKV.get("align")
          in2.map(ve => align.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val v20 = "\"type\" : \"indicator\""
      val v0 =
        if (number.json().length > 0) "\"number\"" + ":" + number.json() else ""
      val v1 =
        if (domain.json().length > 0) "\"domain\"" + ":" + domain.json() else ""
      val v2 =
        if (stream.json().length > 0) "\"stream\"" + ":" + stream.json() else ""
      val v3 =
        if (delta.json().length > 0) "\"delta\"" + ":" + delta.json() else ""
      val v4 =
        if (gauge.json().length > 0) "\"gauge\"" + ":" + gauge.json() else ""
      val v5 =
        if (transforms.json().length > 0)
          "\"transforms\"" + ":" + transforms.json()
        else ""
      val v6 =
        if (title.json().length > 0) "\"title\"" + ":" + title.json() else ""
      val v7 =
        if (name.json().length > 0) "\"name\"" + ":" + name.json() else ""
      val v8 =
        if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
        else ""
      val v9 =
        if (align.json().length > 0) "\"align\"" + ":" + align.json() else ""
      val v10 =
        if (uirevision.json().length > 0)
          "\"uirevision\"" + ":" + uirevision.json()
        else ""
      val v11 =
        if (idssrc.json().length > 0) "\"idssrc\"" + ":" + idssrc.json() else ""
      val v12 =
        if (meta.json().length > 0) "\"meta\"" + ":" + meta.json() else ""
      val v13 = if (uid.json().length > 0) "\"uid\"" + ":" + uid.json() else ""
      val v14 =
        if (mode.json().length > 0) "\"mode\"" + ":" + mode.json() else ""
      val v15 = if (ids.json().length > 0) "\"ids\"" + ":" + ids.json() else ""
      val v16 =
        if (metasrc.json().length > 0) "\"metasrc\"" + ":" + metasrc.json()
        else ""
      val v17 =
        if (customdata.json().length > 0)
          "\"customdata\"" + ":" + customdata.json()
        else ""
      val v18 =
        if (value.json().length > 0) "\"value\"" + ":" + value.json() else ""
      val v19 =
        if (customdatasrc.json().length > 0)
          "\"customdatasrc\"" + ":" + customdatasrc.json()
        else ""
      val vars: List[String] = List(
        v20,
        v0,
        v1,
        v2,
        v3,
        v4,
        v5,
        v6,
        v7,
        v8,
        v9,
        v10,
        v11,
        v12,
        v13,
        v14,
        v15,
        v16,
        v17,
        v18,
        v19
      )
      val cleanVars = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("number")
        in0.map(ve => number.fromJson(ve))

        val in1 = mapKV.get("domain")
        in1.map(ve => domain.fromJson(ve))

        val in2 = mapKV.get("stream")
        in2.map(ve => stream.fromJson(ve))

        val in3 = mapKV.get("delta")
        in3.map(ve => delta.fromJson(ve))

        val in4 = mapKV.get("gauge")
        in4.map(ve => gauge.fromJson(ve))

        val in5 = mapKV.get("transforms")
        in5.map(ve => transforms.fromJson(ve))

        val in6 = mapKV.get("title")
        in6.map(ve => title.fromJson(ve))

        val in7 = mapKV.get("name")
        in7.map(ve => name.fromJson(ve))

        val in8 = mapKV.get("visible")
        in8.map(ve => visible.fromJson(ve))

        val in9 = mapKV.get("align")
        in9.map(ve => align.fromJson(ve))

        val in10 = mapKV.get("uirevision")
        in10.map(ve => uirevision.fromJson(ve))

        val in11 = mapKV.get("idssrc")
        in11.map(ve => idssrc.fromJson(ve))

        val in12 = mapKV.get("meta")
        in12.map(ve => meta.fromJson(ve))

        val in13 = mapKV.get("uid")
        in13.map(ve => uid.fromJson(ve))

        val in14 = mapKV.get("mode")
        in14.map(ve => mode.fromJson(ve))

        val in15 = mapKV.get("ids")
        in15.map(ve => ids.fromJson(ve))

        val in16 = mapKV.get("metasrc")
        in16.map(ve => metasrc.fromJson(ve))

        val in17 = mapKV.get("customdata")
        in17.map(ve => customdata.fromJson(ve))

        val in18 = mapKV.get("value")
        in18.map(ve => value.fromJson(ve))

        val in19 = mapKV.get("customdatasrc")
        in19.map(ve => customdatasrc.fromJson(ve))

      }
      this
    }

  }

  object layoutAttributes extends JSON {
    def json(): String                 = ""
    def fromJson(in: Value): this.type = this
  }

}
