import java.io.File

import mill._
import mill.scalalib._
import mill.define.{Command, Target, Task}
import $exec.plugins
import loom.{GraalModules, MDocModule, OpenFXModules, Utils, Versions}
import $ivy.`com.lihaoyi::utest:0.7.1`
//import utest._ // already imported by mill scalalib
import coursier.MavenRepository
//import mill.util.EnclosingClass
//import upickle.default.{ReadWriter => RW, Reader => R, Writer => W}
//import ammonite.main.Router.Overrides

import loom.Versions._

object mdoc extends MDocModule with GraalModules with OpenFXModules {
  // These must be set MillIntegrationTestModule to be placed in the Ivy Repo
  // Once there, we must explicitly load these
  //override def moduleDeps: Seq[JavaModule] = Seq(plots, webkit)

  val appVersion = "0.1.0"

  override def scalaVersion = gScalaVersion

  // PlugIns

  // Markdown documentation: Scala code parsing
  override def mdocVersion: Target[String] = gMdocVersion
  override def mdocVerbose: Int = gMdocVerbose
  override def mdocScalacOptions: T[Seq[String]] = super.mdocScalacOptions()
  // ++ List("-Ylog-classpath") // prints the classpath
  override def mdocOpts: T[Seq[String]] = super.mdocOpts() // ++ List("--verbose")

  override def scalacOptions = Seq("-deprecation", "-feature")

  /**
   * Note that the GraalVM recalculate the arguments because the run and test
   * commands may change them.
   *
   * @return
   */
  override def forkArgs: Target[Seq[String]] = T {
    Seq("-Dprism.verbose = true", "-ea") ++ // JavaFX
      Utils.jvmModuleArgs(super[GraalModules].forkArgs(), super[OpenFXModules].forkArgs()) // GraavVM + OpenFX
  }


  override def forkEnv: Target[Map[String, String]] =
    Map("prism.verbose" -> "true") // -Dprism.verbose = true, // JavaFX

  // repositories :+ MavenRepository(s"https://oss.sonatype.org/content/repositories/releases"),
  override def repositories = super.repositories ++ Seq(
    MavenRepository("https://dl.bintray.com/cibotech/public")
  )

  override def ivyDeps = T {
    Agg(
      ivy"com.github.pathikrit::better-files:$betterFilesVersion",
      // TODO: do we need this
      //ivy"org.typelevel::cats-core:2.0.0",
      //ivy"org.typelevel::cats-effect:2.0.0",
      ivy"org.scalameta::mdoc:${mdocVersion()}", // https://scalameta.org/mdoc/
      //ivy"com.lihaoyi:mill:$millVersion",                     // https://github.com/lihaoyi/mill
      ivy"com.lihaoyi::os-lib:$osLibVersion", // https://github.com/lihaoyi/os-lib
      ivy"tech.tablesaw:tablesaw-core:$tableSawVersion", // https://github.com/jtablesaw/tablesaw
      ivy"tech.tablesaw:tablesaw-aggregate:$tableSawVersion",
      ivy"org.plotly-scala::plotly-core:$plotlyScalaVersion", // https://github.com/alexarchambault/plotly-scala (http://plotly-scala.org)
      ivy"org.plotly-scala::plotly-render:$plotlyScalaVersion",
      ivy"com.gitlab.hmf::webkit:$webkitVersion",
      ivy"com.gitlab.hmf::plots:$plotsVersion"
    ) ++
      super[GraalModules].ivyDeps() ++
      super[OpenFXModules].ivyDeps()
  }

  /*
  override def compileIvyDeps = Agg(
    ivy"org.scala-lang:scala-library:2.12.8"
  )

  override def scalacPluginIvyDeps = Agg{
    ivy"org.scala-lang:scala-library:2.12.8"
  }*/

  def genJFXMill: Target[Unit] = T {
    val params = forkArgs()
    println(params)
    println(os.pwd)
    val script = "jfxmill.sh"
    val wd = os.pwd
    val header = "#!/bin/bash"
    val content =
      s"""
         |$header
         |JAVA_OPTS="${params.mkString(" ")}"
         |#echo $$JAVA_OPTS
         |./mill "$$@"
         |""".stripMargin

    os.write.over(wd / script, content, "rwxrw-r--")
  }

  trait uTests extends TestModule {
    override def ivyDeps = Agg(ivy"com.lihaoyi::utest::$utestVersion")
    def testFrameworks: Target[Seq[String]] = Seq("utest.runner.Framework")

    override def forkArgs: Target[Seq[String]] = mdoc.forkArgs()
    override def forkEnv: Target[Map[String, String]] = mdoc.forkEnv()

    override def test(oargs: String*): Command[(String, Seq[TestRunner.Result])] = T.command {
      val args = setForkArgs(oargs)
      super.test(args: _*)
    }
  }

  object test extends Tests with uTests

}

def verify(): Command[Unit] = T.command {
  mdoc.test.test()()
  /*
  // [69/87] mdoc.test.test.overriden.mill.scalalib.TestModule.test
  // [71/87] mdoc.mdDocPostModifierPaths
  // [77/87] mdoc.scalaDocOptions
  println("mdoc.compile()") // does not work
  val cr = mdoc.compile()
  println("mdoc.test.test()()")
  mdoc.test.test()()
  println("mdoc.mDocLocal()")
  val n = mdoc.mDocLocal()
  println(s"mdoc.mDocLocal() = $n")
  println("end.")
  */

  ()
}