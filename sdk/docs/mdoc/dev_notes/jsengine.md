# Introduction

This section servers as a repository of notes and experiments used to execute
plotting JavaScript libraries like [PlotlyJS](https://github.com/plotly/plotly.js/)
and [Vega-Lite](https://vega.github.io/vega-lite/). Usually we want to embed the
plots into an HTML document. However sometimes we just need to execute the library
to generate and save to a local file as is done for example the 
[Julia version of Vega-lite](https://github.com/queryverse/VegaLite.jl/blob/master/docs/src/userguide/vlspec.md).
In addition to this we want to limit use to the Java and Scala ecosystems. 
This facilitates CI/CD and package management. Ideally a user need only get 
the build script and execute that with access tp the Internet. 

More information on the Vega [ecosystem](https://vega.github.io/vega-lite/ecosystem.html),
the [embedding](https://github.com/vega/vega-embed) of plots and its general 
[usage](https://vega.github.io/vega/usage/) is available. Current issues with
Vegas and Vega:

* Currently Vegas updates are not being published nor any pull requests 
accepted;
* Vegas depends on Spark which is holding it updates back;
* The Vega and Vega-lite JS libraries have not been checked to see if we can
save the plots to file (although it does support embedding)     
  
# Possible solutions
   
Here are some possible solutions to the problem (from the most desired to the 
least flexible but possibly more doable solutions):
    
* Implement all of the browser functionality as Java calls in the GraalVM JS
engine. All of the interfacing is done via JS. We can use GraalJS 
interoperability mapping between JS and Java to do this. This is a multi-year,
*billion* dollar endeavour we will leave to Oracle or some other company.         
    
* Implement all of the Node.js API as Java calls in the GraalVM JS
engine. All of the interfacing is done via JS. We can use GraalJS 
interoperability mapping between JS and Java to do this. Here we would 
also need to provide server-side rendering capabilities to generate the images
(via the Canvas DOM for example). This is a multi-year, *billion* dollar 
endeavour we will leave to Oracle or some other company.  

* Load all of the Node.js API from existing Node JS modules. [jsdom](https://github.com/jsdom/jsdom)
is a candidate for this. Note however that jsdom uses the `require` to load 
dependent libraries. Can we hack an equivalent function that uses Graal's 
`load`? If we can how do we manage all the dependencies? This is especially
critical because jsdom depends on many `npm` managed libraries. One solution
is to use node to generate and download all dependent libraries to the local 
modules project directory. 
    
* An alternate solution to the above one is to execute a [browserified](http://browserify.org/) 
script that simulates all of Node.js in the [graaljs](https://github.com/graalvm/graaljs) engine. 
Their are several issues with this solution. First we need to simulate the Node.js API.
Second, Node.js does not provide rendering functionality as does the browser. 
To provide much of the Node.js API in Javascript we have [jsdom](https://github.com/jsdom/jsdom).
To provide [jsdom rendering](https://github.com/jsdom/jsdom#canvas-support) 
(via the HTML Canvas), we need to install the OS dependent [canvas](https://www.npmjs.com/package/canvas)
package. Additionally experiments with GraalJS and the browserify version of 
jdom shows that the [HTML Window](https://developer.mozilla.org/en-US/docs/Web/API/Window) 
also need to be provided and Plotly needs 
[CSSStyleSheet](https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleSheet) interface.   

* Use a [headerless browser](https://github.com/dhamaniasad/HeadlessBrowsers) 
to execute the script. These solution don't always work well and may depend 
on OS installed applications or libraries. Some possible solutions include: 
[jbrowserdriver](https://github.com/machinepublishers/jbrowserdriver) and 
[JavaFX webview](https://docs.oracle.com/javafx/2/webview/jfxpub-webview.htm)
now part of [OpenJFX](https://openjfx.io/javadoc/11/javafx.web/javafx/scene/web/WebView.html).
OpenJFX may be the best compromise between functional completeness and ease of 
integration. Note that JavaFX is based on [WebKit](https://webkit.org/) but 
rendering is done via Java. So the results may not be the best. >Because we 
are only interested in the SVG and raster image output, this may not be an 
issue. We also have [HTMLUnit](http://htmlunit.sourceforge.net/) that is based 
on the [Rhino](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/Rhino)
JS engine. The [ui4j](https://github.com/webfolderio/ui4j) is like the HTMLUnit
library but uses the JDK's WebKit. [Jaunt-Api](https://jaunt-api.com/) 

* Use the [GraaVM node version](https://www.graalvm.org/docs/reference-manual/languages/js/).
The GraalVM team have [their version](https://github.com/graalvm/graaljs) of [Node.js](https://nodejs.org/en/) 
whose original Google [V8 JavaScript engine](https://v8.dev/) has been 
[swapped out](https://github.com/graalvm/graaljs/blob/master/docs/user/RunOnJDK.md) 
for GraalJS. This may be a good solution if SVG and Canvas (png, jpeg) 
rendering works. Currently (date: November 2019) the stock JDK11 install does 
not include the `node` (nor the `graaljs`) command line tools. 

* Use the `node.js` command line to run the script. Here we still need to add 
server side rendering support. This requires we install `node.js`, `npm` 
(package manager) and several packages such as `browserify` (for development), 
`jsdom` and `canvas` (for building, testing and running/generating the output).
This solution also brings another issue - we want to embed the results of the
JS code into an HTML or Markdown file, so this has to be integrated to via
additional process piping between processes.    

# Other Non-JVM Options

The goal of the project is to have a [knitr](https://yihui.name/knitr/) like 
tool using only the Jaca ecosystem and the JVM for its installation execution.
The notebook options also exist and includes for example:

* [Polynote](https://polynote.org/)
* [Jupyter](https://jupyter.org/) (has Java kernel [Almond](https://github.com/almond-sh/almond) that uses Plotly)  
* [Apache Zeppelin](http://zeppelin.apache.org/)
* [Spark Notebook](https://github.com/spark-notebook/spark-notebook)
* [BeakerX](http://beakerx.com/) (based on Jupyter)
* [Sagemath](http://www.sagemath.org/) (not quite ML orientated nor We based 
as the others, but has a large community. See [this](https://ask.sagemath.org/questions/) and [this](http://sage.brandoncurtis.com/))


# Executing JavaScript in the JVM

In order to execute JS scripts we need Java based JS engines. From the 
following [list](https://en.wikipedia.org/wiki/List_of_ECMAScript_engines)
and Internet search we have these Java based solutions:

* [Nashorn](https://en.wikipedia.org/wiki/Nashorn_(JavaScript_engine)
* [Rhino](https://en.wikipedia.org/wiki/Rhino_(JavaScript_engine)
* [GraalJS](https://github.com/graalvm/graaljs)

The [Nashorn](https://wiki.openjdk.java.net/display/Nashorn/Nashorn+extensions) 
option is not valid because it will be discontinued. This work will focus on 
the use of GraalJS. According to the [ECMA Script compatibility table](https://kangax.github.io/compat-table/es6/)
it is one of the more complete solutions. GraalJS also uses the the sophisticated 
GraalVM JIT compiler for optimal code execution. It also provides a flag to 
activate [Nashorn compatability](https://github.com/graalvm/graaljs/blob/master/docs/user/JavaScriptCompatibility.md#nashorn-scripting-mode)

The use of jsdom and Domino within a JS engines were obtained [here](https://stackoverflow.com/questions/37425346/running-jsdom-in-nashorn).

## JavaScript on the JVM Projects

The only other project that seems to have what we want is [ringojs](https://ringojs.org/).
The [source code](https://github.com/ringo/ringojs) is open source and seems 
to be actively maintained. It is still unclear if thus project, that is based 
on Mozilla's Rhino JavaScript engine will allow the execution of hre plotting 
libraries. More specifically if it has the required browser emulation 
libraries. A quick search for example does not show up anything for server side
rendering One interesting point is that it supports CommonJS `require` 
statements.    

Another project is [PurpleJS](http://purplejs.io/) but its [repository](https://github.com/purplejs/purplejs)
shows no activity. So it can be considered dead.  

## Node.js on the JVM Projects

A couple of projects that aim to provide a Node.js compatible environments 
based on the JVM. [Nodyn](https://www.nodyn.io/) is one such project that is
[dead](https://github.com/nodyn/nodyn). It depends on the [dynJS](http://dynjs.org/)
project, which also seems to be [dead](https://github.com/dynjs/dynjs). 
Interestingly enough a recent (2019) [project](https://github.com/psanders/graaljs-with-npm) 
uses GraalVM with the [Nodyn](https://github.com/nodyn/jvm-npm) package 
manager (which depends on the Node.js `npm`).

## The GraaJS Engine

The GraalJS is a GraalVM [polyglot implementation](https://github.com/graalvm/graaljs)
of the GraalVM project. One important issue when executing GraalJS via command 
line or programmatically is to set-up its configuration correctly. This allows
setting the compatibility with other engines and norms and allowing the loading
of scripts from URLs and/or the file system. 

A list of parameters can be found [here](https://www.graalvm.org/docs/reference-manual/languages/js/#graalvm-javascript-options).
If you have GraalVM installed (you need to download install the full Graal 
JDK), then you can execute: 

```
graalvm/bin/js --help:languages.
```
 
which prints something like:
 
```
User language options:
  JavaScript:
    --js.annex-b=<Boolean>                       Enable ECMAScript Annex B features. (default:true)
    --js.array-sort-inherited=<Boolean>          Sort inherited keys in Array.protoype.sort. (default:true)
    --js.atomics=<Boolean>                       Enable ES2017 Atomics. (default:true)
    --js.bigint=<Boolean>                        Provide an implementation of the BigInt proposal. (default:true)
    --js.console=<Boolean>                       Provide 'console' global property. (default:true)
    --js.direct-byte-buffer                      Use direct (off-heap) byte buffer for typed arrays. (default:false)
    --js.ecmascript-version=<ecmascript-version> ECMAScript Version. (default:10)
    --js.global-property=<Boolean>               Provide 'global' global property. (default:true)
    --js.graal-builtin=<Boolean>                 Provide 'Graal' global property. (default:true)
    --js.intl-402                                Enable ECMAScript Internationalization API. (default:false)
    --js.java-package-globals=<Boolean>          Provide Java package globals: Packages, java, javafx, javax, com, org, edu. (default:true)
    --js.load-from-url                           Allow 'load' to access URLs. (default:false)
    --js.load=<Boolean>                          Provide 'load' global function. (default:true)
    --js.nashorn-compat                          Provide compatibility with the OpenJDK Nashorn engine. (default:false)
    --js.performance=<Boolean>                   Provide 'performance' global property. (default:true)
    --js.polyglot-builtin=<Boolean>              Provide 'Polyglot' global property. (default:true)
    --js.polyglot-evalfile=<Boolean>             Provide 'Polyglot.evalFile' function. (default:true)
    --js.print=<Boolean>                         Provide 'print' global function. (default:true)
    --js.regexp-static-result=<Boolean>          Provide last RegExp match in RegExp global var, e.g. RegExp.$1. (default:true)
    --js.scripting                               Enable scripting features (Nashorn compatibility option). (default:false)
    --js.shared-array-buffer=<Boolean>           Enable ES2017 SharedArrayBuffer. (default:true)
    --js.shebang                                 Allow parsing files starting with #!. (default:false)
    --js.shell                                   Provide global functions for js shell. (default:false)
    --js.stack-trace-limit=<Integer>             Number of stack frames to capture. (default:10)
    --js.strict                                  Enforce strict mode. (default:false)
    --js.syntax-extensions                       Enable Nashorn syntax extensions. (default:false)
    --js.timer-resolution=<Long>                 Resolution of timers (performance.now() and Date built-ins) in nanoseconds. Fuzzy time is used when set to 0.
                                                 (default:1000000)
    --js.timezone=<String>                       Set custom timezone. (default:)
    --js.v8-compat                               Provide compatibility with the Google V8 engine. (default:false)
```

Additional information can be found [here](https://github.com/graalvm/graaljs/blob/master/docs/user/Options.md).

## Executing the GraalVM JS code

In order to execute the [GraalJS](https://github.com/graalvm/graaljs) 
programmatically we first need to set-up a JDK and corresponding JVM project.
We have [JDK 11 support](https://github.com/oracle/graal/issues/651) for the
stock OpenJDK 11 install. This does not includes several utilities such as
the Graal version of Node.js nor the the command line version of GraalJS.  
A [Maven based project](https://github.com/graalvm/graal-js-jdk11-maven-demo), 
that is described [here](https://medium.com/graalvm/graalvms-javascript-engine-on-jdk11-with-high-performance-3e79f968a819).
served as a reference for the Scala projects. 
 
Additional information can be found in the GraalVM site such as the 
[JS engine](https://www.graalvm.org/docs/reference-manual/languages/js/) or 
questions can be posted in the [community forums](https://www.graalvm.org/community/).
Graal JS language compatibility cam be found [here](https://github.com/graalvm/graaljs/blob/master/docs/user/JavaScriptCompatibility.md).  
 
In order to execute a GraalVM languge compiler we need to instantiate a 
`Context` and indicate which of the supported languages we want to use.
The following is an example on Scala:

```scala
val jsOptions: OptionDescriptors = Context.newBuilder("js").build()
                .getEngine()
                .getLanguages()
                .get("js")
                .getOptions();
```

The `org.graalvm.polyglot.Language` allows us to get a [list of options](https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/Language.html#getOptions--).
A list of the supported languages can also be obtained programmatically via the
[org.graalvm.polyglot.Engine](https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/Engine.html#getLanguages--).
More information is available on both the [Engine](https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/Engine.Builder.html)
and the [Context](https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/Context.Builder.html).
For those that worked with Nashorn additional migration notes can be found [here](https://github.com/graalvm/graaljs/blob/master/docs/user/NashornMigrationGuide.md).

Although [modules are supported](https://github.com/graalvm/graaljs/issues/92), this
work used the `load` command to import one or more JS libraries. GraalJS also 
provides bear seamless java [interoperability](https://github.com/graalvm/graaljs/blob/master/docs/user/JavaInterop.md).  

# Emulating the Browser Environment

One way to generate the plot is toi emulate the browser's environment. In GraalJS
one could conceivably implement the browser within the JS engine by declaring
and implementing the full [HTML specications](https://html.spec.whatwg.org/multipage/).
This represents an effort of many thousands of man hours. The idea is to find 
(preferabnly) JS or Java libraries to implement the minimal set of HTML for
use with the plotting library.    

## Shims and Polyfills

Several JS libraries exist that are used to provide missing stubs of missing
functionality to browsers (polyfills) or implementations to be used within
other environments such as Node.js (shims). One way is to collect and package
all of the required functionality into a single JS script. More
[concretely](https://stackoverflow.com/questions/35062852/npm-vs-bower-vs-browserify-vs-gulp-vs-grunt-vs-webpack)
we can use the following two Node.js applications to collect and package
the required libraries for use in Browser:     

* [WebPack](https://webpack.js.org/). See [WebPack node configuration](https://webpack.js.org/configuration/node/);
* [Browserify](http://browserify.org/). See the [Browserify handbook](https://github.com/browserify/browserify-handbook).

These systems can take Node.js code, scan the `requires` declarations and fetches
and concatenates everything nto a single script. Unfortunately any code that
is not imported via a require (because its expected to exist in the 
environment), will not identified and imported. This means that we have to 
find other libraries such as

* [Domino](https://github.com/fgnass/domino): a partial implementation of the 
of the HTML standards. It is based on the, now defunct, [dom.js](https://github.com/andreasgal/dom.js)
library. It is *maintained* by the [Wikimedia Foundation](https://www.mediawiki.org/wiki/Parsoid).
* [CSSOM](https://github.com/NV/CSSOM) that is a partial implementation of the 
CSS Object Model.   

The goal is to use a set of libraries and Browserify to create a set of 
scripts to execute the plotting library code and generate the corresponding
static images for use in static documents. Note that for HTML documents we
can use the browser. 

# Emulating the browser timing functions and call backs

One of the API's that the browsers provide are the `setInterval` and 
`setTimeout` functions. These [setInterval and setTimeout JavaScript Functions](https://blogs.oracle.com/nashorn/setinterval-and-settimeout-javascript-functions) 
were implemented for Nashorn. The following code can be loaded and referenced 
successfully within the Graal VM JS context. However it has not been tested 
yet.

```javascript
var Platform = Java.type("javafx.application.Platform");
var Timer    = Java.type("java.util.Timer");

function setTimerRequest(handler, delay, interval, args) {
    handler = handler || function() {};
    delay = delay || 0;
    interval = interval || 0;
    var applyHandler = function(){ handler.apply(this, args); }
    var runLater = function(){ Platform.runLater(applyHandler); }
    var timer = new Timer("setTimerRequest", true);
    if (interval > 0) {
        timer.schedule(runLater, delay, interval);
    } else {
        timer.schedule(runLater, delay);
    }
    return timer;
}

function clearTimerRequest(timer) {
    timer.cancel();
}

function setInterval() {
    var args = Array.prototype.slice.call(arguments);
    var handler = args.shift();
    var ms = args.shift();
    return setTimerRequest(handler, ms, ms, args);
}

function clearInterval(timer) {
    clearTimerRequest(timer);
}

function setTimeout() {
    var args = Array.prototype.slice.call(arguments);
    var handler = args.shift();
    var ms = args.shift();

    return setTimerRequest(handler, ms, 0, args);
}
function clearTimeout(timer) {
    clearTimerRequest(timer);
}

function setImmediate() {
    var args = Array.prototype.slice.call(arguments);
    var handler = args.shift();

    return setTimerRequest(handler, 0, 0, args);
}

function clearImmediate(timer) {
    clearTimerRequest(timer);
}

```

## Emulating the NODE.js Environment

### Plotly Specific Experiments

We have [plotly-nodejs](https://github.com/plotly/plotly-nodejs) but it is 
wrapper for a REST API to Plotly's servers. As per the [sugestion](https://github.com/plotly/plotly-nodejs/issues/35),
we need to load the Plotly library from [NW.js/node-webkit](https://nwjs.io/) or [electron](https://electronjs.org/). 
One important issue is that the non-minified (uglyfied) version of the Plotly 
library should be used (at least during debugging). 

More up-tp-date information can be found [here](https://community.plot.ly/t/how-to-perform-server-side-manipulation-using-plotly-js/1077/3).
* [Gist with an example](https://gist.github.com/etpinard/d27a44bd5dbee5490f20)
* [Running a headless nwjs](https://github.com/nwjs/nw.js/issues/769#issuecomment-40787394)
* No generation of PDF is possible. A 3rd party library will be necessary. Here
are some possibilities:
  * [SVGtoPDF](https://github.com/CBiX/svgToPdf.js/). May be unmaintained. 
  Depends on [jsPDF](https://github.com/MrRio/jsPDF) and [jQuery](https://jquery.org/).
  * Use [jsPDF](https://github.com/MrRio/jsPDF) directly
  * Linux command line application such as `convert`
Interestingly the [referenced issue](https://github.com/jsdom/jsdom/issues/1368)
has been solved so this technique or one similar to it should work. However 
this is specifically catering for Node.js without browserify. An example of
accessing and testing som eof the the browser's HTML API ca be found [here](https://gist.github.com/etpinard/f328c38536360ff37c0de62172c01617)
using a headless chromium browser. Tests using jasmine are interesting. One 
could consider running tests such as these in the GraaVM JS engine. It also
shows how WebGL can be used. [This](https://gist.github.com/etpinard/ac6402406afcd1ce38a4) 
is another example using thr Karma Chrome launcher. And [this one](https://gist.github.com/etpinard/58a9e054b9ca7c0ca4c39976fc8bbf8a)
uses the combination of Node.js and jsdom but not everything works correctly.

**These results seem to indicate the jsdom is not eough to execute the plotting
library code.**

### Using GraalVM's node from the JVM
The GraalVM provides its own version of node.js (see [this section](#possible-solutions).
If we are prepared to use thr GraalVM SDK, then we have a node command line in
which we can execute JS scripts using the GraalJS engine. This requires that 
we invoke the command line on the script. Alternatively we can use 
[this project](https://github.com/mikehearn/nodejvm) to invoke the GraalVM 
version of node.js from the JVM and interface with it directly. This is 
described [here](https://blog.plan99.net/vertical-architecture-734495f129c4) and 
[here](https://mikehearn.github.io/nodejvm/).

### Using Browserified jsdm 

The first exercise is to load a browserified version of the [Jsdom](https://github.com/jsdom/jsdom) 
library. To do this we must first install nodejs. After that we can install the
Node.js package manager (`npm`) so:

    sudo apt-get install nodejs
    sudo npm install -g npm
    sudo npm install -g browserify
    
Note that the `-g` means a global (system wide) installation, which is why 
administrative privileges are required (`sudo`). This also means that these
applications are available via command line directly (no need to invoke 
node.js). Note also that the first command install the `node`and `npm` 
commands. The second command is used to update the packer manager.          
    
### Install jsdom from the source code    

we can install a local version from the source code. The following commands
download the source code and unzip it for use: 
     
    wget https://github.com/jsdom/jsdom/archive/15.2.0.zip
    unzip 15.2.0.zip
    cd /home/user/Downloads/jsdom-15.2.0

The source code has its build commands described in the following JSON 
document:

    https://github.com/jsdom/jsdom/blob/master/package.json

One of the things we have to do is generate the interfaces based on the HTML 
specification's Web IDL descriptions. The following command 

    "convert-idl": "node ./scripts/webidl/convert"

is used [here](https://github.com/jsdom/jsdom/blob/b4bb1c5248e05762aa3c4c14faa71ef3adf10657/package.json)
and [here](https://github.com/jsdom/jsdom/blob/e3744f5ceffbf06a83b8e0c11b1641f4e229ea18/.travis.yml).
We can execute this command in one of the following 3 ways: 

* `npm run-script convert-idl`
* `npm run convert-idl`
* `node ./scripts/webidl/convert`
* yarn test-browser

At this point we get this error for the first 3 commands:
    
    Error: Cannot find module 'rimraf'

The last command fails because no `yarn` was installed.
    
This means that the module above is not installed or was not found in the 
expected directory. Node.js will look for library modules in the following 
paths:

1. `$HOME/.node_modules`
1. `$HOME/.node_libraries`
1. `$PREFIX/lib/node`
1. A relative or absolute path you use in the `require` command    

We can update the local module by executing thr following command in the 
project root:
        
    npm update
    
so:

    user@machine:~/Downloads/jsdom-15.2.0$ npm update
    npm notice created a lockfile as package-lock.json. You should commit this file.
    + acorn@7.1.0
    + abab@2.0.2
    + cssom@0.4.1
    + acorn-globals@4.3.4
    + data-urls@1.1.0
    + cssstyle@2.0.0
    + domexception@1.0.1
    + escodegen@1.12.0
    + html-encoding-sniffer@1.0.2
    + array-equal@1.0.0
    + nwsapi@2.1.4
    + parse5@5.1.0
    + symbol-tree@3.2.4
    + request@2.88.0
    + saxes@3.1.11
    + request-promise-native@1.0.7
    + pn@1.1.0
    + w3c-hr-time@1.0.1
    + tough-cookie@3.0.1
    + webidl-conversions@4.0.2
    + whatwg-encoding@1.0.5
    + w3c-xmlserializer@1.1.2
    + whatwg-url@7.0.0
    + ws@7.2.0
    + whatwg-mimetype@2.3.0
    + xml-name-validator@3.0.0
    added 98 packages from 123 contributors in 12.205s
   
We had some issues updating the modules. It seems we must also explicitly
indicate that we wan to update the development libraries also. We can do 
this with either one pf the following coimmands:     
    
    npm update --save-dev    
    npm update -D
    
Unfortunately we get the following error:    
    
    0 info it worked if it ends with ok
    1 verbose cli [ '/usr/bin/node', '/usr/bin/npm', 'update', '--save-dev' ]
    2 info using npm@6.12.0
    3 info using node@v12.11.1
    4 verbose npm-session 44e5e3373c5b6819
    5 verbose update computing outdated modules to update
    6 verbose stack Error: Unsupported URL Type "link:": link:./scripts/eslint-plugin
    6 verbose stack     at unsupportedURLType (/usr/lib/node_modules/npm/node_modules/npm-package-arg/npa.js:200:15)
    6 verbose stack     at fromURL (/usr/lib/node_modules/npm/node_modules/npm-package-arg/npa.js:258:13)
    6 verbose stack     at Function.resolve (/usr/lib/node_modules/npm/node_modules/npm-package-arg/npa.js:77:12)
    6 verbose stack     at shouldUpdate (/usr/lib/node_modules/npm/lib/outdated.js:361:20)
    6 verbose stack     at /usr/lib/node_modules/npm/lib/outdated.js:329:23
    6 verbose stack     at /usr/lib/node_modules/npm/node_modules/slide/lib/async-map.js:52:35
    6 verbose stack     at Array.forEach (<anonymous>)
    6 verbose stack     at /usr/lib/node_modules/npm/node_modules/slide/lib/async-map.js:52:11
    6 verbose stack     at Array.forEach (<anonymous>)
    6 verbose stack     at asyncMap (/usr/lib/node_modules/npm/node_modules/slide/lib/async-map.js:51:8)
    6 verbose stack     at outdated_ (/usr/lib/node_modules/npm/lib/outdated.js:316:3)
    6 verbose stack     at /usr/lib/node_modules/npm/lib/outdated.js:99:5
    6 verbose stack     at /usr/lib/node_modules/npm/lib/outdated.js:81:5
    6 verbose stack     at /usr/lib/node_modules/npm/node_modules/read-package-tree/rpt.js:162:20
    7 verbose cwd /home/user/Downloads/jsdom-15.2.0
    8 verbose Linux 5.0.0-31-generic
    9 verbose argv "/usr/bin/node" "/usr/bin/npm" "update" "--save-dev"
    10 verbose node v12.11.1
    11 verbose npm  v6.12.0
    12 error code EUNSUPPORTEDPROTOCOL
    13 error Unsupported URL Type "link:": link:./scripts/eslint-plugin
    14 verbose exit [ 1, true ]    
    
    
A first try was to use [these command](https://www.npmjs.com/package/npm-check-updates) 
to update all of thr libraries. This however was not the problem.


    sudo npm install -g npm-check-updates
    ncu -u
    
    Upgrading /home/user/Downloads/jsdom-15.2.0/package.json
    [====================] 52/52 100%
    
     abab                ^2.0.0  →   ^2.0.2 
     acorn-globals       ^4.3.2  →   ^4.3.4 
     escodegen          ^1.11.1  →  ^1.12.0 
     saxes               ^3.1.9  →   ^4.0.2 
     symbol-tree         ^3.2.2  →   ^3.2.4 
     ws                  ^7.0.0  →   ^7.2.0 
     browserify         ^16.2.3  →  ^16.5.0 
     eslint-find-rules   ^3.3.1  →   ^3.4.0 
     karma               ^4.3.0  →   ^4.4.1 
     mocha               ^6.2.1  →   ^6.2.2 
     portfinder         ^1.0.20  →  ^1.0.25 
     wd                 ^1.11.2  →  ^1.11.4 
     canvas              ^2.5.0  →   ^2.6.0 
    
    Run npm install to install new versions.

And as indicated in the suggestion above tried to update the libraries with 
several equivalent commands: 

    npm install --save-dev
    npm install --save
    npm install
    npm install --only=dev
     
The result was the same error that was previously described:     
     
    0 info it worked if it ends with ok
    1 verbose cli [ '/usr/bin/node', '/usr/bin/npm', 'install' ]
    2 info using npm@6.12.0
    3 info using node@v12.11.1
    4 verbose npm-session 460013e33d538db5
    5 silly install runPreinstallTopLevelLifecycles
    6 silly preinstall jsdom@15.2.0
    7 info lifecycle jsdom@15.2.0~preinstall: jsdom@15.2.0
    8 silly install loadCurrentTree
    9 silly install readLocalPackageData
    10 timing stage:loadCurrentTree Completed in 147ms
    11 silly install loadIdealTree
    12 silly install cloneCurrentTreeToIdealTree
    13 timing stage:loadIdealTree:cloneCurrentTree Completed in 0ms
    14 silly install loadShrinkwrap
    15 timing stage:loadIdealTree:loadShrinkwrap Completed in 59ms
    16 silly install loadAllDepsIntoIdealTree
    17 timing stage:rollbackFailedOptional Completed in 0ms
    18 timing stage:runTopLevelLifecycles Completed in 223ms
    19 silly saveTree jsdom@15.2.0
    19 silly saveTree ├── abab@2.0.2
    19 silly saveTree ├─┬ acorn-globals@4.3.4
    19 silly saveTree │ ├── acorn-walk@6.2.0
    19 silly saveTree │ └── acorn@6.3.0
    19 silly saveTree ├── acorn@7.1.0
    19 silly saveTree ├── array-equal@1.0.0
    19 silly saveTree ├── cssom@0.4.1
    19 silly saveTree ├─┬ cssstyle@2.0.0
    19 silly saveTree │ └── cssom@0.3.8
    19 silly saveTree ├─┬ data-urls@1.1.0
    19 silly saveTree │ ├── whatwg-mimetype@2.3.0
    19 silly saveTree │ └─┬ whatwg-url@7.0.0
    19 silly saveTree │   ├── lodash.sortby@4.7.0
    19 silly saveTree │   ├─┬ tr46@1.0.1
    19 silly saveTree │   │ └── punycode@2.1.1
    19 silly saveTree │   └── webidl-conversions@4.0.2
    19 silly saveTree ├── domexception@1.0.1
    19 silly saveTree ├─┬ escodegen@1.12.0
    19 silly saveTree │ ├── esprima@3.1.3
    19 silly saveTree │ ├── estraverse@4.3.0
    19 silly saveTree │ ├── esutils@2.0.3
    19 silly saveTree │ ├─┬ optionator@0.8.2
    19 silly saveTree │ │ ├── deep-is@0.1.3
    19 silly saveTree │ │ ├── fast-levenshtein@2.0.6
    19 silly saveTree │ │ ├─┬ levn@0.3.0
    19 silly saveTree │ │ │ ├── prelude-ls@1.1.2
    19 silly saveTree │ │ │ └── type-check@0.3.2
    19 silly saveTree │ │ ├── prelude-ls@1.1.2
    19 silly saveTree │ │ ├── type-check@0.3.2
    19 silly saveTree │ │ └── wordwrap@1.0.0
    19 silly saveTree │ └── source-map@0.6.1
    19 silly saveTree ├─┬ html-encoding-sniffer@1.0.2
    19 silly saveTree │ └─┬ whatwg-encoding@1.0.5
    19 silly saveTree │   └─┬ iconv-lite@0.4.24
    19 silly saveTree │     └── safer-buffer@2.1.2
    19 silly saveTree ├── nwsapi@2.1.4
    19 silly saveTree ├── parse5@5.1.0
    19 silly saveTree ├── pn@1.1.0
    19 silly saveTree ├─┬ request-promise-native@1.0.7
    19 silly saveTree │ ├─┬ request-promise-core@1.1.2
    19 silly saveTree │ │ └── lodash@4.17.15
    19 silly saveTree │ ├── stealthy-require@1.1.1
    19 silly saveTree │ └─┬ tough-cookie@2.5.0
    19 silly saveTree │   └── psl@1.4.0
    19 silly saveTree ├─┬ request@2.88.0
    19 silly saveTree │ ├── aws-sign2@0.7.0
    19 silly saveTree │ ├── aws4@1.8.0
    19 silly saveTree │ ├── caseless@0.12.0
    19 silly saveTree │ ├─┬ combined-stream@1.0.8
    19 silly saveTree │ │ └── delayed-stream@1.0.0
    19 silly saveTree │ ├── extend@3.0.2
    19 silly saveTree │ ├── forever-agent@0.6.1
    19 silly saveTree │ ├─┬ form-data@2.3.3
    19 silly saveTree │ │ ├── asynckit@0.4.0
    19 silly saveTree │ │ └─┬ mime-types@2.1.24
    19 silly saveTree │ │   └── mime-db@1.40.0
    19 silly saveTree │ ├─┬ har-validator@5.1.3
    19 silly saveTree │ │ ├─┬ ajv@6.10.2
    19 silly saveTree │ │ │ ├── fast-deep-equal@2.0.1
    19 silly saveTree │ │ │ ├── fast-json-stable-stringify@2.0.0
    19 silly saveTree │ │ │ ├── json-schema-traverse@0.4.1
    19 silly saveTree │ │ │ └── uri-js@4.2.2
    19 silly saveTree │ │ └── har-schema@2.0.0
    19 silly saveTree │ ├─┬ http-signature@1.2.0
    19 silly saveTree │ │ ├── assert-plus@1.0.0
    19 silly saveTree │ │ ├─┬ jsprim@1.4.1
    19 silly saveTree │ │ │ ├── extsprintf@1.3.0
    19 silly saveTree │ │ │ ├── json-schema@0.2.3
    19 silly saveTree │ │ │ └─┬ verror@1.10.0
    19 silly saveTree │ │ │   └── core-util-is@1.0.2
    19 silly saveTree │ │ └─┬ sshpk@1.16.1
    19 silly saveTree │ │   ├── asn1@0.2.4
    19 silly saveTree │ │   ├─┬ bcrypt-pbkdf@1.0.2
    19 silly saveTree │ │   │ └── tweetnacl@0.14.5
    19 silly saveTree │ │   ├── dashdash@1.14.1
    19 silly saveTree │ │   ├─┬ ecc-jsbn@0.1.2
    19 silly saveTree │ │   │ └── jsbn@0.1.1
    19 silly saveTree │ │   ├── getpass@0.1.7
    19 silly saveTree │ │   ├── jsbn@0.1.1
    19 silly saveTree │ │   └── tweetnacl@0.14.5
    19 silly saveTree │ ├── is-typedarray@1.0.0
    19 silly saveTree │ ├── isstream@0.1.2
    19 silly saveTree │ ├── json-stringify-safe@5.0.1
    19 silly saveTree │ ├── mime-types@2.1.24
    19 silly saveTree │ ├── oauth-sign@0.9.0
    19 silly saveTree │ ├── performance-now@2.1.0
    19 silly saveTree │ ├── qs@6.5.2
    19 silly saveTree │ ├── safe-buffer@5.2.0
    19 silly saveTree │ ├─┬ tough-cookie@2.4.3
    19 silly saveTree │ │ └── punycode@1.4.1
    19 silly saveTree │ ├── tunnel-agent@0.6.0
    19 silly saveTree │ └── uuid@3.3.3
    19 silly saveTree ├─┬ saxes@3.1.11
    19 silly saveTree │ └── xmlchars@2.2.0
    19 silly saveTree ├── symbol-tree@3.2.4
    19 silly saveTree ├─┬ tough-cookie@3.0.1
    19 silly saveTree │ └── ip-regex@2.1.0
    19 silly saveTree ├─┬ w3c-hr-time@1.0.1
    19 silly saveTree │ └── browser-process-hrtime@0.1.3
    19 silly saveTree ├─┬ w3c-xmlserializer@1.1.2
    19 silly saveTree │ └── xml-name-validator@3.0.0
    19 silly saveTree ├── webidl-conversions@4.0.2
    19 silly saveTree ├── whatwg-encoding@1.0.5
    19 silly saveTree ├── whatwg-mimetype@2.3.0
    19 silly saveTree ├── whatwg-url@7.0.0
    19 silly saveTree ├─┬ ws@7.2.0
    19 silly saveTree │ └── async-limiter@1.0.1
    19 silly saveTree └── xml-name-validator@3.0.0
    20 verbose stack Error: Unsupported URL Type "link:": link:./scripts/eslint-plugin
    20 verbose stack     at unsupportedURLType (/usr/lib/node_modules/npm/node_modules/npm-package-arg/npa.js:200:15)
    20 verbose stack     at fromURL (/usr/lib/node_modules/npm/node_modules/npm-package-arg/npa.js:258:13)
    20 verbose stack     at Function.resolve (/usr/lib/node_modules/npm/node_modules/npm-package-arg/npa.js:77:12)
    20 verbose stack     at /usr/lib/node_modules/npm/lib/install/deps.js:484:20
    20 verbose stack     at Array.map (<anonymous>)
    20 verbose stack     at /usr/lib/node_modules/npm/lib/install/deps.js:483:47
    20 verbose stack     at tryCatcher (/usr/lib/node_modules/npm/node_modules/bluebird/js/release/util.js:16:23)
    20 verbose stack     at Promise._settlePromiseFromHandler (/usr/lib/node_modules/npm/node_modules/bluebird/js/release/promise.js:517:31)
    20 verbose stack     at Promise._settlePromise (/usr/lib/node_modules/npm/node_modules/bluebird/js/release/promise.js:574:18)
    20 verbose stack     at Promise._settlePromiseCtx (/usr/lib/node_modules/npm/node_modules/bluebird/js/release/promise.js:611:10)
    20 verbose stack     at _drainQueueStep (/usr/lib/node_modules/npm/node_modules/bluebird/js/release/async.js:142:12)
    20 verbose stack     at _drainQueue (/usr/lib/node_modules/npm/node_modules/bluebird/js/release/async.js:131:9)
    20 verbose stack     at Async._drainQueues (/usr/lib/node_modules/npm/node_modules/bluebird/js/release/async.js:147:5)
    20 verbose stack     at Immediate.Async.drainQueues [as _onImmediate] (/usr/lib/node_modules/npm/node_modules/bluebird/js/release/async.js:17:14)
    20 verbose stack     at processImmediate (internal/timers.js:439:21)
    21 verbose cwd /home/user/Downloads/jsdom-15.2.0
    22 verbose Linux 5.0.0-31-generic
    23 verbose argv "/usr/bin/node" "/usr/bin/npm" "install"
    24 verbose node v12.11.1
    25 verbose npm  v6.12.0
    26 error code EUNSUPPORTEDPROTOCOL
    27 error Unsupported URL Type "link:": link:./scripts/eslint-plugin
    28 verbose exit [ 1, true ]    

The issue is [known](https://github.com/facebook/react/issues/13824) and the 
`npm` application supports [local paths](https://stackoverflow.com/questions/14381898/local-dependency-in-package-json/14387210#14387210).
Apparently the original path [works with Yarn](https://github.com/elastic/kibana/issues/16980).
So what we can do is change the build script and avoid the installation of 
the Yarn package. To do this we:  

    gedit package.json

and the [line](https://github.com/jsdom/jsdom/blob/master/package.json#L111) is 
changed from:

    "eslint-plugin-jsdom-internal": "link:./scripts/eslint-plugin",

to:

    "eslint-plugin-jsdom-internal": "file:./scripts/eslint-plugin",

and we now repeat the modules update command:

     npm update -D
    
and get (**take note were Node.js installs the modules locally**):    
    
    > wd@1.11.4 install /home/user/Downloads/jsdom-15.2.0/node_modules/wd
    > node scripts/build-browser-scripts
    
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.9 (node_modules/watchify/node_modules/fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.9: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.1.1 (node_modules/fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.1: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
    
    + eslint-plugin-jsdom-internal@0.0.0
    + karma-browserify@6.1.0
    + browserify@16.5.0
    + karma-mocha-webworker@1.3.0
    + chai@4.2.0
    + eslint-find-rules@3.4.0
    + karma-chrome-launcher@3.1.0
    + eslint-plugin-html@6.0.0
    + js-yaml@3.13.1
    + eslint@6.5.1
    + karma@4.4.1
    + karma-mocha@1.3.0
    + benchmark@2.1.4
    + mocha-sugar-free@1.4.0
    + mocha@6.2.2
    + portfinder@1.0.25
    + q@1.5.1
    + server-destroy@1.0.1
    + rimraf@3.0.0
    + webidl2js@10.0.0
    + wd@1.11.4
    + minimatch@3.0.4
    + st@1.2.2
    + optimist@0.6.1
    + watchify@3.11.1
    added 662 packages from 1217 contributors in 27.229s

Once we have all of the modukles (runtime and development) we can generate, we
can generate the *living HTML* intergaces: 

    npm run convert-idl

that are found here (from the root od the project):

    ls lib/jsdom/living/generated/
    ls lib/jsdom/living/generated/ | wc -l

At this point we can now bundle the scripts into a single module. First go to 
the library code:

    user@machine:~/Downloads/jsdom-15.2.0$ cd lib/
    user@machine:~/Downloads/jsdom-15.2.0/lib$ pwd
    /home/user/Downloads/jsdom-15.2.0/lib

and the execute an equivalent [browserify command for jsdom](https://github.com/jsdom/jsdom/issues/245#issuecomment-256615808): 

    browserify -r ./api.js -s jsdom > jsdom_bundle.js

or    

    browserify -r ./api.js -s jsdom -o jsdom.js
    
If you want to add debug information that can be used to track the origin of 
the source code, use the `--debug` option:
    
    browserify -r ./api.js -s jsdom_bundle --debug -o jsdom_bundle.js
    
Note that ths will make (the already large) file twice as large. One can also
*minify* the library bit this will make debugging practically impossible. For
the original source code we get:
    
    user@machine:~/Downloads/jsdom-15.2.0/lib$ ls -lh
    total 6,3M
    -rw-rw-r-- 1 hmf hmf  11K out 14 14:39 api.js
    drwxrwxr-x 6 hmf hmf 4,0K out 14 14:39 jsdom
    -rw-r--r-- 1 hmf hmf 6,3M out 21 17:10 jsdom_bundle.js

At this point we can execute the tests and confirm that all is working 
correctly. The command is:

    npm run test-browser-worker
    
It is possible to customize our jsdm library by changing the library code. For
example, we can copy the main library entry file `api.js` and copy that to the
`myapi.js` file. This library can then be used and tested as follows:   
     
    user@machine:~/Downloads/jsdom-15.2.0/lib$ gedit myapi.js
    user@machine:~/Downloads/jsdom-15.2.0/lib$ browserify -r ./myapi.js -s jsdom --debug -o jsdom.js
    user@machine:~/Downloads/jsdom-15.2.0/lib$ mv jsdom.js ~/IdeaProjects/srllab/mdocs/src/mdocs/
    user@machine:~/IdeaProjects/srllab$ mill -i mdocs.runMain mdocs.TestGraalJSDOM

     
### Install from the npm module

The better and ore direct way to install jsdom is by first installing it 
locally. We do this as follows:

    user@machine:~/Downloads/browserify_exp$ npm install jsdom
    npm WARN saveError ENOENT: no such file or directory, open '/home/user/Downloads/browserify_exp/package.json'
    npm notice created a lockfile as package-lock.json. You should commit this file.
    npm WARN enoent ENOENT: no such file or directory, open '/home/user/Downloads/browserify_exp/package.json'
    npm WARN jsdom@15.2.0 requires a peer of canvas@^2.5.0 but none is installed. You must install peer dependencies yourself.
    npm WARN browserify_exp No description
    npm WARN browserify_exp No repository field.
    npm WARN browserify_exp No README data
    npm WARN browserify_exp No license field.
    
    + jsdom@15.2.0
    added 99 packages from 123 contributors and audited 140 packages in 2.664s
    found 0 vulnerabilities

Notice how the above output indicates `canvas@^2.5.0` as a peer requirement 
that was not installed. This module will emulate the HTM canvas element using 
an OS native module (see [this section](#adding-cssom-to-domino-dom)). This
will allow canvas rendering. If jsdom were globally installed we could issue
the command:

    browserify -r jsdom -s jsdom -o jsdom.js

Here we need to make sure that we are in the root were the local modules were
installed and execute the equivalent: 
   
    user@machine:~/Downloads/browserify_exp$ browserify -r jsdom -s jsdom -o jsdom.js

and you should have the following:

    user@machine:~/Downloads/browserify_exp$ ls -lh
    total 6,3M
    -rw-r--r--  1 hmf hmf 6,3M out 22 13:34 jsdom.js
    drwxr-xr-x 97 hmf hmf 4,0K out 22 13:32 node_modules
    -rw-r--r--  1 hmf hmf  28K out 22 13:32 package-lock.json

For the debug version execute:

    browserify -r jsdom -s jsdom --debug -o jsdom.js

and you should have the following:

    user@machine:~/Downloads/browserify_exp$ ls
    jsdom.js  node_modules  package-lock.json
    user@machine:~/Downloads/browserify_exp$ ls -lh
    total 17M
    -rw-r--r--  1 hmf hmf  17M out 22 13:38 jsdom.js
    drwxr-xr-x 97 hmf hmf 4,0K out 22 13:32 node_modules
    -rw-r--r--  1 hmf hmf  28K out 22 13:32 package-lock.json
    
Notice how the module `jsdom.js` more than doubled in size. Now that we have 
the module installed we can test it on a browser. One way is to create the
following HTML document (`testjsdom.html`):

```
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <script src="jsdom.js"></script>
    </head>
    <body>
        <p>hi</p>
    </body>
</html>
```
in the root directory were the jsdom.js moldule is found: 
    
    user@machine:~/Downloads/browserify_exp$ 

Then open this file in a browser, activate the developer tools and in the 
console execute the following commands

```
  >> const { JSDOM } = jsdom;
  undefined
  >> const dom = new JSDOM(`<!DOCTYPE html><p>Hello world</p>`);
  undefined
  >> console.log(dom.window.document.querySelector("p").textContent); // "Hello world"
  undefined
```

Alternatively add the commands as a JS script and conform the console's 
output is correct: 

````
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- <script>
           //load("jsdom.js")
           //require("jsdom")
        </script> -->
        <script src="jsdom.js"></script>
    </head>
    <body>
        <p>hi</p>
        <script>
           const { JSDOM } = jsdom;
           const dom = new JSDOM(`<!DOCTYPE html><p>Hello world</p>`);
           console.log(dom.window.document.querySelector("p").textContent); // "Hello world"
        </script>
    </body>
</html>
````

For more information on `npm` see the commands:

    npm bin
    npm doctor
    npm outdated
    npm ls
    npm root
    npm search
    npm test



--------------------------------------------

Missing window af FormatData

https://stackoverflow.com/questions/30398825/eslint-window-is-not-defined-how-to-allow-global-variables-in-package-json
https://github.com/request/request/issues/2794
 . https://github.com/form-data/form-data/issues/371
   . https://stackoverflow.com/questions/33001237/webpack-not-excluding-node-modules/35820388#35820388
https://github.com/hellosign/hellosign-embedded/issues/107

http://rollupjs.org/guide/en/ ??   
   
Web worker????
https://github.com/jindw/xmldom
https://github.com/ampproject/worker-dom
https://github.com/AshleyScirra/via.js

---------------------------     
     
     
31238
},{"http":961,"https":888,"net":783,"tls":783,"util":987}],548:[function(require,module,exports){
/* eslint-env browser */
module.exports = typeof self == 'object' ? self.FormData : window.FormData;

https://github.com/jsdom/jsdom/issues/1284
https://github.com/rstacruz/jsdom-global
https://github.com/modosc/global-jsdom

     
    
https://github.com/jsdom/jsdom/issues/1284
https://github.com/jsdom/jsdom/issues/245
https://www.pika.dev/npm/global-jsdom
https://github.com/jsdom/jsdom/blob/master/test/karma.conf.js


------------------------------------------


# Issue Overloading window in Browserify Loading of jsdom in GraalJS

In order to execute JS scripts that require the HTML DOM we can use jsdom. 
jsdom expects to be used in the `Node.js` or the `browser environment`. The 
GraalVM is neither. And herein lies the rub. To have a Node.js environment
we need to implement all of the Node.js API. If we don't then we need to 
implement all of the browser's functionality. We use the Browserified version
of jsdom to provide the Node.js environment but this does not seem to work.
Jsdom may be doing more sophisticate checks.   

If we execute the following script in GraalJS (no browser window available):    

```
load("./mdocs/src/mdocs/jsdom.js")
const dom = new jsdom.JSDOM(`<!DOCTYPE html><p>Hello world</p>`);
//console.log(dom);
if (dom == 'undefined' || dom == null) throw "dom creation failed.";
```

we get the following error:

```
},{"http":961,"https":888,"net":783,"tls":783,"util":987}],140:[function(require,module,exports){
/* eslint-env browser */
module.exports = typeof self == 'object' ? self.FormData : window.FormData;

},{}],141:[function(require,module,exports){
```

This code represent the `form-data` module that jsdom, requires:

```
},{"./lib/auth":669,"./lib/cookies":670,"./lib/getProxyFromURI":671,"./lib/har":672,"./lib/hawk":673,"./lib/helpers":674,"./lib/multipart":675,"./lib/oauth":676,"./lib/querystring":677,"./lib/redirect":678,"./lib/tunnel":679,"_process":922,"aws-sign2":59,"aws4":60,"caseless":64,"extend":135,"forever-agent":139,"form-data":140,"http":961,"http-signature":163,"https":888,"is-typedarray":188,"isstream":189,"mime-types":619,"performance-now":646,"safe-buffer":688,"stream":960,"url":982,"util":987,"zlib":834}],
```

## Adding the Browser's environment

So lets try an emulate the browse's environment.  In order to add the browser's 
environment we can add the DOM model. The following libraries were found 
(server-side DOM implementation):

* [Domino](https://github.com/jazdw/domino)
* [dom.js](https://github.com/andreasgal/dom.js)

We create a `window` global as it should exist in a browser (logs let us check
that the definitions are loaded and available):

```
// Make sure we have a working DOM for jsdom
load("./mdocs/src/mdocs/domino.js")
console.log(domino);
console.log(domino.createWindow);

// Create a global window for jsdom
var window = domino.createWindow('<h1>Hello world</h1>', 'http://example.com');
var document = window.document;
``` 

Unfortunately the more up-to-date Domino dos not have the a Style Sheet 
implementation. Although such an [error](https://github.com/jsdom/jsdom/issues/2122)
in jsdom, this is different. jsdom seems to have an implementation of the
style sheet, but it may be delegating it to the browser's environment or only
overrides this in the Node.js environment (which we seem unable to emulate
through browserify). Note that it is `Plotly` (loaded at after jsdom) that
causes a problem:  

```
  Exception in thread "main" TypeError: Cannot read property 'insertRule' of undefined
      at <js> addRelatedStyleRule(../../../../plotly-latest.js:105918:4004593-4004613)
      at <js> addStyleRule(../../../../plotly-latest.js:105899:4004023-4004074)
      at <js> 1(../../../../plotly-latest.js:72:5956-6002)
```

This is the code snippet that causes the problem: 

```
    /**
     * for dynamically adding style rules
     * to a stylesheet uniquely identified by a uid
     */
    function addRelatedStyleRule(uid, selector, styleString) {
        var id = 'plotly.js-style-' + uid;
        var style = document.getElementById(id);
        if(!style) {
            style = document.createElement('style');
            style.setAttribute('id', id);
            // WebKit hack :(
            style.appendChild(document.createTextNode(''));
            document.head.appendChild(style);
        }
        var styleSheet = style.sheet;
    
[105918]if(styleSheet.insertRule) {  
            styleSheet.insertRule(selector + '{' + styleString + '}', 0);
        } else if(styleSheet.addRule) {
            styleSheet.addRule(selector, styleString, 0);
        } else loggers.warn('addStyleRule failed');
    }

```

I only found some [remotely related](https://github.com/plotly/plotly.js/issues/2355) 
[issues](https://stackoverflow.com/questions/28930846/how-to-use-cssstylesheet-insertrule-properly).
We need to monkey patch the 


## Adding CSSOM to Domino DOM

Here are the references to the CSS StyleSheet elements we are interested in:   

* [CSSStyleSheet](https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleSheet)
* [insertRule](https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleSheet/insertRule)
* [StyleSheet](https://developer.mozilla.org/en-US/docs/Web/API/StyleSheet)
* [csswg.org CSSOM](https://drafts.csswg.org/cssom/#cssstylesheet)
* [CSS Object Model](https://developer.mozilla.org/en-US/docs/Web/API/CSS_Object_Model)
  
A last one [reference](https://stackoverflow.com/questions/28930846/how-to-use-cssstylesheet-insertrule-properly)
shows that the Plotly use of `insertRule` is correct. As such the following 
JavaScript (partial) implementation of the CSS Object Model was used: 

* [CSSOM](https://github.com/NV/CSSOM) 

First we need to obtain the CSSOM library an browserify it so that no additional
loading is required. We use `npm` to install a local copy of the library so: 

```
user@machine:~$ cd Downloads/
user@machine:~/Downloads$ mkdir cssom
user@machine:~/Downloads$ cd cssom/
user@machine:~/Downloads/cssom$ npm install cssom
npm WARN saveError ENOENT: no such file or directory, open '/home/user/Downloads/cssom/package.json'
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN enoent ENOENT: no such file or directory, open '/home/user/Downloads/cssom/package.json'
npm WARN cssom No description
npm WARN cssom No repository field.
npm WARN cssom No README data
npm WARN cssom No license field.

+ cssom@0.4.1
added 1 package from 1 contributor and audited 1 package in 0.507s
found 0 vulnerabilities

user@machine:~/Downloads/cssom$ ls -lh
total 8,0K
drwxr-xr-x 3 hmf hmf 4,0K nov  1 15:57 node_modules
-rw-r--r-- 1 hmf hmf  308 nov  1 15:57 package-lock.json
```

Next we browserify the code to be used in the browser. 

```
user@machine:~/Downloads/cssom$ browserify -r cssom -s cssom -o cssom.js
user@machine:~/Downloads/cssom$ ls -lh
total 56K
-rw-r--r-- 1 hmf hmf  48K nov  1 16:01 cssom.js
drwxr-xr-x 3 hmf hmf 4,0K nov  1 15:57 node_modules
-rw-r--r-- 1 hmf hmf  308 nov  1 15:57 package-lock.json
```

The resulting file is then copied to the project folder were it will be used:

```
cp cssom.js /home/user/IdeaProjects/srllab/mdocs/src/mdocs
```

The following test JS code is used:

```js
load("./mdocs/src/mdocs/cssom.js")
load("./mdocs/src/mdocs/domino.js")

var window = domino.createWindow('<h1>Hello world</h1>', 'http://example.com');
var document = window.document;

var s = new cssom.CSSStyleSheet;
s.insertRule("a {color: blue}", 0);
// Only prints something if a style has been added
console.log('styleSheet s = ' + s);

var uid = 'global'
var id = 'plotly.js-style-' + uid;
var style = document.getElementById(id);
if(!style) {
  //console.log('No CSS found!');
  style = document.createElement('style');
  style.setAttribute('id', id);
  // WebKit hack :(
  style.appendChild(document.createTextNode(''));
  document.head.appendChild(style);
  style.sheet = s;
}
var styleSheet = style.sheet;
style.sheet.insertRule('.foo{color:red;}', 0);
//console.log('styleSheet style.sheet = ' + styleSheet);

//load("https://cdn.plot.ly/plotly-latest.min.js");
//load("https://cdn.plot.ly/plotly-latest.js");
load("./mdocs/src/mdocs/plotly-latest.js");
```

It loads the CSSOM and Domino libraries. It then creates a global `window`.
A simple test is then made by creating and printing a CSS rule. The next
section creates a document element `'style'` and adds it to the global. This 
codes is essentially the same one as what is found in the Plotly library. 
However we use the statement `style.sheet = s;` to add the global document. 
We should consider monkey patching if this is not enough.  And after that, once
again the style sheet is extracted and used. 

Note that we debug the code in this [line](https://github.com/plotly/plotly.js/blob/c1ef6911da054f3b16a7abe8fb2d56019988ba14/src/lib/dom.js#L79)
in order to determine that the `uid="global"`. If we had monkey patched Domino's
`Document` the call to `document.getElementById(id)` would not have failed. 
 
Once we have this resolved, Plotly causes the following error:

```
Exception in thread "main" ReferenceError: DOMParser is not defined
	at <js> 654(mdocs/src/mdocs/plotly-latest.js:96518:3717750-3717758)
	at <js> o(mdocs/src/mdocs/plotly-latest.js:7:741-828)
```

This issue can be seen [here](https://community.plot.ly/t/plotly-v1-45-3-domparser-is-not-defined-when-running-tests/21353)
and [here](https://github.com/plotly/plotly.js/issues/1675). Plotly's call to the 
parser is found [here](https://github.com/plotly/plotly.js/blob/56dd829fbaa2ba5e254bc6870b380ef333bf30e5/src/components/modebar/modebar.js#L17).

The DOMParser either exists *natively* in the browser or we can get Node.js 
libraries. Jsdom has such a [parser](https://github.com/jsdom/jsdom/issues/1368#issuecomment-230118260).
And even has its own internal XML parser even though other such parsers 
[exist](https://github.com/harrison-ifeanyichukwu/xml-serializer).   

Here are some Node.js parsers:
* [dom-parser](https://www.npmjs.com/package/dom-parser)
* [Cheerio](https://cheerio.js.org/)
* [HtmlParser2](https://github.com/fb55/htmlparser2)
* [Parse5](https://github.com/inikulin/parse5)
* [jsdom](https://github.com/jsdom/jsdom)

You also have some Java versions in case you want to implement it as an interface:
* [OpenJfx](https://openjfx.io/javadoc/11/javafx.web/javafx/scene/web/WebEngine.html)
* [JSoup](https://jsoup.org/)
* [HTMLCleaner](http://htmlcleaner.sourceforge.net/index.php)
* [Lagarto](https://jodd.org/lagarto/) and [Jerry](https://jodd.org/jerry/)

and Java CSS parsers:
* [OpenJfx](https://openjfx.io/javadoc/11/javafx.graphics/javafx/css/CssParser.html)
* [ph-css](https://github.com/phax/ph-css)
* [jStyleParser](https://github.com/radkovo/jStyleParser)
* [CSSParser](http://cssparser.sourceforge.net/)

In order to circumvents this, a parser was globally declare as an empty object so 
that tests could be performed with minimum effort:

```
var DOMParser = function() {};
```

Now the following error occurs when executing the JS script:

```
Exception in thread "main" Error: NotYetImplemented
	at <js> exports.nyi(mdocs/src/mdocs/domino.js:23327:759865-759894)
	at <js> 551(mdocs/src/mdocs/plotly-latest.js:76405:2989460-2989489)
```

The code in Domino throws this exception is as follows:

```
exports.nyi = function() {
  throw new Error("NotYetImplemented");
};
```

which stems from the Punimplemented* Domino `HTMLHTMLCanvasElement`: 

```
define({
  tag: 'canvas',
  ctor: function HTMLCanvasElement(doc, localName, prefix) {
    HTMLElement.call(this, doc, localName, prefix);
  },
  props: {
    getContext: { value: utils.nyi },
    probablySupportsContext: { value: utils.nyi },
    setContext: { value: utils.nyi },
    transferControlToProxy: { value: utils.nyi },
    toDataURL: { value: utils.nyi },
    toBlob: { value: utils.nyi }
  },
  attributes: {
    width: { type: "unsigned long", default: 300},
    height: { type: "unsigned long", default: 150}
  }
});
```

Plotly uses the canvas to draw the plots on. More specifically we have:

```
"use strict"

module.exports = createText

var vectorizeText = _dereq_("./lib/vtext")
var defaultCanvas = null
var defaultContext = null

if(typeof document !== 'undefined') {
  defaultCanvas = document.createElement('canvas')
  defaultCanvas.width = 8192
  defaultCanvas.height = 1024
  defaultContext = defaultCanvas.getContext("2d")
}
```
This is expected and [others](https://github.com/fgnass/domino/issues/113) 
have reported this issue to the Domino author and maintainer. The problem here
is that the canvas represents a GUI element that is OS dependent. So it makes 
no sense implementing this unless we have a way of recording and later 
extracting the contents of the canvas. Jsdom does [allow mocking](https://github.com/jsdom/jsdom/issues/1782)
using [monkey patching](https://github.com/jsdom/jsdom/issues/1782#issuecomment-337656878)
but strangely enough this **did not work in GraalJS**.    

Jsdom's [solution](https://www.npmjs.com/package/jsdom#canvas-support) is to 
provide an OS dependent [Canvas](https://www.npmjs.com/package/canvas) based 
on Node.js's C++ interface. This [project](https://github.com/Automattic/node-canvas)
makes the pre-compiled implementations available [elswhere](https://www.npmjs.com/package/canvas-prebuilt).
It requires [cairo](ttps://www.cairographics.org/) which is native to the OS. 
This code cannot be used for the browser or the GraalVM due to its Node.js 
specific interface. Jsdom also has Canvas [tests](https://github.com/jsdom/jsdom/blob/78dabf1f39d6a08b0e6a314e507e3f3e42c903ed/test/to-port-to-wpts/htmlcanvaselement.js)
but I don't think it provides 2d canvas elements. However we 

Note however that the jsdom pages state: 

*"jsdom includes support for using the canvas package to extend any `<canvas>` 
elements with the canvas API. To make this work, you need to include canvas as
a dependency in your project, as a peer of jsdom. If jsdom can find the 
canvas package, it will use it, but if it's not present, then `<canvas>` 
elements will behave like `<div>`s. Since jsdom v13, version 2.x of canvas
is required; version 1.x is no longer supported."*

So we should find a replacement shim. Note that even if we used jsdom 
successfully, its `div` will not provide the plot image that is required.
Here are some links to possible shims:

[HTML5 Cross Browser Polyfills](https://github.com/Modernizr/Modernizr/wiki/HTML5-Cross-Browser-Polyfills) 
[Google Canvas 5 Polyfill](https://github.com/google/canvas-5-polyfill)

## Loading jsdom

It would be interesting if we could execute the Node.js code. This has already 
been discussed in the GraalVM issue tracker. For example [here](https://github.com/graalvm/graaljs/issues/2) 
and [here](https://github.com/graalvm/graaljs/issues/50). One [solution](https://github.com/mikehearn/nodejvm)
is to use the Graal SDK Node.js version. The Graal Node.js can be found [here](https://github.com/graalvm/graaljs/tree/master/graal-nodejs).

After dome experimenting we found that jsdom expects `window` when using its 
browserified version. Trying to provide one seems to raise other issues. We 
have prepared a `jsdom_2.js` file that has a minimal browserified file that
declares a `JSDOM` class. The goal here is to see if we can load and access
this object in the GraalJS engine. We encountered the following problem: 

* When using jsdom we need a browser window;
* We provide one via `Domino` instantiating a global `window`; 
* `Domino` however does not provide steel sheets `insertRule` (required by Plotly);
* We use CSSOM to create a style sheet and add it to the `window` 
- Theoretically this window should be global and be used by Plotly
- jsdom already imports CSSOM that provides `insertRule` but it is not available;
- jsdom loading should wrap new `window`, but this does not seem to work;
- the Browserify prelude does not work as expected: Plotly can load but no
jsdom is available in `global`   

Here is the restructured prelude of `jsdom_2.js` (the prelude is minified and
unreadable):

```js
(function(f){
  if (typeof exports==="object"&&typeof module!=="undefined"){
    module.exports=f()
  }
  else if (typeof define==="function" && define.amd){
    define([],f)
  }
  else {
    var g;
    if (typeof window!=="undefined"){
      console.log("g = window = "+window)
      g=window
    } else if (typeof global!=="undefined"){
      console.log("g = global = "+global)
      g=global
    } else if (typeof self!=="undefined"){
      console.log("g = self = "+self)
      g=self
    } else {
      console.log("g = this = "+this)
      g=this
    }
    //console.log("g.jsdom = f()")
    //console.log(f)
    g.jsdom = f()
  }
  })
  (function(){
    var define,module,exports;
    return (function(){
              function r(e,n,t){
                function o(i,f){
                  if (!n[i]) {
                    if(!e[i]) {
                       var c="function"==typeof require&&require;
                       if (!f&&c) {
                         console.log("c = "+c)
                         return c(i,!0);
                       }
                       if(u) {
                         console.log("u = "+u)
                         return u(i,!0);
                       }
                       var a = new Error("Cannot find module '"+i+"'");
                       throw a.code="MODULE_NOT_FOUND",a
                    }
                    var p=n[i]={exports:{}};
                    console.log("n[i] = "+n[i])
                    console.log("e[i][0] = "+e[i][0])
                    e[i][0].call( p.exports,
                                 function(r){
                                   var n=e[i][1][r];
                                   return o(n||r)
                                 }, p, p.exports,r,e,n,t)
                 }
                 console.log("n[i].exports = " + n[i].exports)
                 return n[i].exports
              }
              for(var u="function"==typeof require&&require,i=0; i<t.length; i++) {
                console.log("o(t[i]) = " + o(t[i]))
                o(t[i]);
              }
             return o
           }
           console.log("r = " + r)
           return r
         }
       )()(
           {
            "jsdom":[
              function(require,module,exports){
                 (
                  function (String){
                     "use strict";

                     class JSDOM {
                       constructor(input, options = {}) {}
                     }

                     exports.JSDOM = JSDOM;
                  }
                ).call(this,"buffer")
              },
              {} ]

          },{},[]
         )("jsdom")
  }
 );

``` 

We have searched for possible solutions such as [this](https://github.com/nodejs/node/issues/1043),
but have had no success. What seems to be happening is that if we replace or
extend the GraalJS global so using Domino:

```
w = domino.createWindow('<h1>Hello world</h1>', 'http://example.com'); 
global.window = w
```

and follow that with:

```
load("./mdocs/src/mdocs/jsdomt_2.js")
console.log(global);
console.log(global.window);
console.log(global.jsdom);
if (jsdom == 'undefined' || jsdom == null) throw "jsdom not loaded.";
console.log(jsdom.JSDOM);
```    

we get:

```
[object global]
new Window(document)
undefined
Exception in thread "main" ReferenceError: jsdom is not defined
	at <js> :program(Unnamed:23:850-854)
```

But if we comment out the assignment `global.window = w` we get:

```
[object global]
undefined
[object Object]
class JSDOM {
                       constructor(input, options = {}) {}
                     }
```

So the question is: why does GraalJS not keep the jsdom class definition
after we add a member to `global`? The same thing happens when we assign 
a window to a global `var`.  
   

# Java Browser Research

One of the options we have is use a fully-fledged browser in [headless mode](https://en.wikipedia.org/wiki/Headless_browser)
or [simulations of browsers](https://github.com/dhamaniasad/HeadlessBrowsers). 
These browsers may be based on well established platforms such as:
* [Chromium](https://www.chromium.org/) (Uses the [Blink](https://www.chromium.org/blink) rendering engine)
* [Chrome](https://www.google.com/chrome/)
* [FireFox](https://www.mozilla.org/en-US/firefox/)

Or they may use a platform such as [WebKit](https://webkit.org/):
* [PhantomJS](https://phantomjs.org/) (based on WebKit, dead)
* [HTMLUnit](http://htmlunit.sourceforge.net/) (based on WebKit)
* [SlimerJS](https://slimerjs.org/) (Javascript + Firefox)

Or we have emulators of the browser API:
* [jsdom](https://github.com/jsdom/jsdom)
* [Zombie](http://zombie.js.org/) (Requires Node.js)
* [Nightwatchjs](https://nightwatchjs.org/) (Requires Node.js, full test suite)
* [Splash](https://splash.readthedocs.io/en/stable/). [Uses](https://github.com/scrapinghub/splash) (Python + HTTP)
* [Jaunt](https://jaunt-api.com/) and [Jauntium](https://jauntium.com/) (Not open source)
* [EnvJS](https://github.com/thatcher/env-js) (Javascript using Rhino, dead)

Or a browser platform:
* [WebKit](https://webkit.org/) (requires the installation of a test module or 
browser)
  * [OpenJFX WebView](https://openjfx.io/javadoc/11/javafx.web/javafx/scene/web/WebView.html)

Or pure Java browser:
* [cssbox](http://cssbox.sourceforge.net/) (X)HTML/CSS rendering engine written in pure Java. 
See te [manual](http://cssbox.sourceforge.net/jstyleparser/manual.php)
* https://github.com/uprootlabs/gngr
* https://github.com/oswetto/LoboEvolution 
* https://github.com/lobobrowser/LoboBrowser (dead)

**The best solution is to use a fully fledged cross-platform browser that is 
supported by the plotting library.** This has the disadvantage that the 
platform or browser needs to be installed in the system. It must also be easy
to install this in the virtual machine for the CI/CD pipeline with minimal 
effort. In addition to this we must ensure that proper rendering is supported. 

For more information on Plotly specific experiment using these sub-systems see
[this section](#plotly-specific-experiments). The use of the 
[**OpenJFX WebView**](https://openjfx.io/javadoc/11/javafx.web/javafx/scene/web/WebView.html)
library may be the **best Java option**. It is based on WebKit, cross platform
and easy to install via Coursier as a JDK module from Maven. WebKit is also 
WebGL compliant. This allows us to use GL for large complex plots. The 
disadvantage is that it does not use the latest WebKit version and some 
rendering may not be correct.

In addition to the above we also have the following Java libraries that may be
of interest for HTML testing and automaton (creating our images):

* [jsoup](https://jsoup.org/) Java HTML Parser ([WHATWG HTML5](https://html.spec.whatwg.org/multipage/) compatible)
* [Xerces](https://xerces.apache.org/xerces2-j/faq-dom.html) XML Parser
* [Lagarto](https://jodd.org/lagarto/) Java HTML Parser
* [ph-css](https://github.com/phax/ph-css) Java CSS 2 and CSS 3 parser and builder
* [Java API support](http://cr.openjdk.java.net/~iris/se/11/latestSpec/api/java.xml/org/w3c/dom/package-summary.html)

[Selenium](https://www.seleniumhq.org/) does not seem to be of interest but is
recorded here for future reference.

**This solution is limited HTML documents (no Laika PDF output) if the browser
in **not** headless.**

## Save Browser Image

Assuming we do use a compatible browser or have a specifications compliant 
browser we still ned to *extract and save* the canvas to a file. The [standard](https://www.w3schools.com/tags/canvas_getimagedata.asp)
allows one to grab parts of the canvas via JS. What we need is to [save](https://stackoverflow.com/questions/11112321/how-to-save-canvas-as-png-image)
the [canvas](https://stackoverflow.com/questions/2483919/how-to-save-svg-canvas-to-local-filesystem) 
to a file, which usually requires a button when using the 
[`toDatURL` mehtod](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toDataURL).

[Fortunately](https://eligrey.com/blog/saving-generated-files-on-the-client-side/) 
we have a the [FileSaver.js library](https://github.com/eligrey/FileSaver.js) 
that allows us to save files on the client side. It also uses the [latest spec](https://html.spec.whatwg.org/multipage/links.html#attr-hyperlink-download)
regarding the download attribute that indicates the intention of downloading a 
hyperlinked resource. We even have a library based on this module that 
specifically [supports image files](https://github.com/kaxi1993/canvas-to-image)
although this is [not strictly required](https://stackoverflow.com/questions/11112321/how-to-save-canvas-as-png-image/11113329#11113329).
This has the advantage that [pure JS libraries](https://github.com/fabricjs/fabric.js/tree/master) 
can be used o the HTML canvas.

Here is a example snippet:

```
var canvas = document.getElementById("my-canvas");
// draw to canvas...
canvas.toBlob(function(blob) {
    saveAs(blob, "pretty image.png");
});
```   

**This solution is limited HTML documents (no Laika PDF output) if the browser
in **not** headless.**

## Using a hidden `Div` and `Canvas` 

We want to execute JS scripts using a browser. If this browser is not headless
we may still be able to generate, using MDoc's processing, files of the 
rendered plots. More specifically we may set-up a [hidden](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/hidden)
canvas or `div`. If hiding does not trigger rendering, we may also 
[temporarily](https://www.w3schools.com/howto/howto_js_toggle_hide_show.asp]
toggle the canvas on, just for rendering and saving the image.  

Some additional information on hiding elements is available online:
* [Hiding elements](https://allyjs.io/tutorials/hiding-elements.html)
* [Display property](https://www.geeksforgeeks.org/hide-or-show-elements-in-html-using-display-property/)
* [Using CSS](https://www.sitepoint.com/five-ways-to-hide-elements-in-css/)
 
**Issue with this solution is that it is limited HTML documents (no Laika PDF
output).**

## WebIDL, Web tests and keeping the API up to date

Some thoughts on what it takes to maintain this Web mess up to date and in 
conformance with the (large comity) standards. Same may be applicable for the
Plotly and Vega libraries that have their own tests. 

[Here](https://github.com/w3c/testing-how-to) we can find information on *web
spec testing*. Th actual spec tests are [here](https://github.com/web-platform-tests/wpt).
The [HTML living standard](https://html.spec.whatwg.org/) provides a 
description of the specifications. Several frameworks exists for web 
and Javascript testing:

* [Mocha](https://mochajs.org/)
* [Jasmine](https://jasmine.github.io/index.html)
* [Ava](https://github.com/avajs/ava)
* [Jest](https://jestjs.io/)
* [Karma](https://karma-runner.github.io/3.0/) (see [Github project](https://github.com/karma-runner/karma))
* [Tape](https://github.com/substack/tape)
* [Cypress](https://www.cypress.io/)
* [Puppeteer](https://github.com/GoogleChrome/puppeteer)
* [ChaiJS](https://www.chaijs.com/)
* [QunitJS](https://qunitjs.com/)
* [SinonJS](https://sinonjs.org/)
* [TestDouble](https://github.com/testdouble/testdouble.js/)

Another important issue is identifying and updating changes to the 
specification. It is important to automate this as much as possible. The 
WebSpec provide IDL descriptions of the API used in the HTML specs. One should
be able to download these IDL specs and generate the corresponding JS and/or
Java interfaces. We should also be able to download, update and execute the 
Web spec tests.    

We can use jsdom as a base for testing. They seem to generate the JS 
interfaces based on the IDL and also execute tests using unit testing. 
More concretely a browser emulator (that has only canvas rendering for 
example) can be tested for compliance in this way.

# JavaScript Plot library testing

We want to provide a Scala wrapper for the plotting libraries. These API should
follow exactly the same API of the original library. So it is important to 
execute the same tests that are executed by the library developers. Scala-plotly
uses the documentation pages for testing. It does not use basic image testing of
the original Plotly library. We could think of contributing such tests in
order to implement missing plots more easily.  

## Vegas IDL from JSON specs

The Scala interface [Vegas](https://www.vegas-viz.org/) (found [here](https://github.com/vegas-viz/Vegas))
uses the Vegas JSON specifications. Vega (and Vega-lite) use a plotting 
*language* that is specified in JSON. The Vegas projects is associate with
[another project](https://github.com/aishfenton/Argus) that uses these JASON
files to generate the necessary interfaces. This may serve as a starting point
if we want to use Vega or Vega-lite. 

# Rendering Math Expressions

We need to consider using and rendering math expression in the documentation. 

## Rendering Math Expressions

Rendering expression and referencing these expression is important. We need 
to consider rendering these in the [back-end](https://stackoverflow.com/questions/9880179/java-or-scala-library-to-parse-latex-or-mathml-string).
in [Java](https://tex.stackexchange.com/questions/41609/tex-rendering-in-a-java-application) or Scala.
If we are able to do this, we can use MDoc to render PS and/or PDF documents. 
Otherwise we will be limited to HTML.  

Here are some possible solutions for **Java**:
* [JLaTeXMath](https://github.com/opencollab/jlatexmath)
* [JMathTex](http://jmathtex.sourceforge.net/) (JLaTeXMath was forked from this library)
* Tex parser [Eclipse Texlipse](https://github.com/eclipse/texlipse)
* [SnuggleTex](https://sourceforge.net/projects/snuggletex/) (converts to HTML and MathML)
* [JEuclid](http://jeuclid.sourceforge.net/) (MathML rendering)

For HTML pages we may use JavaScript solutions. Here are some JavaScript 
options: 
* [MathJax](https://www.mathjax.org/)
* [Katex](https://katex.org/)
* [AsciiMath](http://asciimath.org/) (uses MathJax for rendering)

Note that it seems like MathTex can be run in a Node.js backend. Also note
that if these libraries generate HTML, we may also employ the same technique
we use for the plotting libraries to save te output to image files (SVG would
be nice).  

## Calculating (CAS)

Some of these libraries may provide symbolic math manipulation and rendering
that we can use for documentation purposes. Here are some possibilities: 

* [Symja](https://bitbucket.org/axelclk/symja_android_library/wiki/Home)
* [yacas](http://www.yacas.org/)
* [Jasymca](https://webuser.hs-furtwangen.de/~dersch/)
* [JAS](http://krum.rz.uni-mannheim.de/jas/)
* [Apache Commons Math](http://commons.apache.org/proper/commons-math/) 
(see also its [fork Hipparchus](https://www.hipparchus.org/))
* [MathParser](http://mathparser.org/)
 
# Rendering Diagrams
 
## Rendering diagrams in HTML
 
[Mermaid](https://github.com/mermaid-js/mermaid)

# Interesting projects

## Math notebook software 

These Java based solutions may provide libraries and APIs of interest: 
* [SciLab](https://www.scilab.org/)
* [SageMath](http://www.sagemath.org/)

Here we have a book library:
* [sciteka](https://jwork.org/sciteka/)

## IDE editor components

For writing the documentation can use IntelliJ's Markdown plugins. Not only 
does it provide rendering so we can use to check on the output, but also 
provides spell-checking and auto-completion. We could also consider our own 
edit based on the many solutions that exist today. Here are some projects that
may allow us to *quickly* wip up a prototype to test out ideas:
 
* [Metals](https://scalameta.org/metals/) Scala language server with rich IDE features
(uses [bloop](https://scalacenter.github.io/bloop/))
* [Ammonite REPL](https://ammonite.io/)  (also has auto-completion)
* [Atom's auto-complete](https://atom.io/packages/autocomplete-java)
* [Java auto-complete library](https://github.com/fmmfonseca/completely)
* [AutoComplete](https://github.com/bobbylight/AutoComplete)
* [JavaIME](ttps://github.com/tushortz/JavaIME)
* [Command line autocomplete](https://picocli.info/autocomplete.html)
* [Language Server Protocol implementations](https://langserver.org/)
* [Ayntax highlighting, code folding text component for Java Swing](https://bobbylight.github.io/RSyntaxTextArea/)
* [Java language server using the Java compiler API ](https://github.com/georgewfraser/java-language-server)
* [Java language server](https://github.com/eclipse/eclipse.jdt.ls)
* [Monaco Editor is the code editor that powers VS Code](https://microsoft.github.io/monaco-editor/)

One should also consider alternate IDEs sch as:
* [Atom](https://ide.atom.io/)
* [VisualStudio](https://code.visualstudio.com/)
* [Metals VsCode](https://github.com/scalameta/metals-vscode) (see [this](https://dotty.epfl.ch/docs/usage/ide-support.html))

Polyglot programming may also require support for other languages:
* [Java Embedded Python](https://github.com/ninia/jep)
* [Autocompletion/static analysis library for Python](https://github.com/davidhalter/jedi)

Static Type checker for JavaScript:
* [Flow](https://flow.org/)
* [Flow source](https://github.com/facebook/flow)

## JavasScript Odds and Ends

* [Cheeriojs](https://github.com/cheeriojs/cheerio)
* [SystemJS](https://github.com/systemjs/systemjs)
* [Canvas Constructor](https://canvasconstructor.js.org/#/) see [source](https://github.com/kyranet/canvasConstructor#readme)


## Site generation alternatives
* [mkdocs](https://www.mkdocs.org/) (Python based)
* [mkdocs-material](https://squidfunk.github.io/mkdocs-material/)





