package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Waterfall() extends Trace {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val type_ : String      = """waterfall"""
  val animatable: Boolean = false
  val categories: Array[String] = Array(
    """zoomScale""",
    """showLegend""",
    """oriented""",
    """svg""",
    """cartesian""",
    """bar-like"""
  )

  case object layoutAttributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val waterfallgroupgap: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = Some("""0.0"""),
      description = Some(
        """Sets the gap (in plot fraction) between bars of the same location coordinate."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val waterfallgap: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = None,
      description = Some(
        """Sets the gap (in plot fraction) between bars of adjacent location coordinates."""
      )
    )

    object waterfallmode extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """group"""
      val description: Option[String] = Some(
        """Determines how bars at the same location coordinate are displayed on the graph. With *group*, the bars are plotted next to one another centered around the shared location. With *overlay*, the bars are plotted over one another, you might need to an *opacity* to see multiple bars."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def overlay(): Unit = { selected = TString("overlay") }

      def group(): Unit = { selected = TString("group") }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (waterfallgroupgap.json().length > 0)
          "\"waterfallgroupgap\"" + ":" + waterfallgroupgap.json()
        else ""
      val v1 =
        if (waterfallgap.json().length > 0)
          "\"waterfallgap\"" + ":" + waterfallgap.json()
        else ""
      val v2 =
        if (waterfallmode.json().length > 0)
          "\"waterfallmode\"" + ":" + waterfallmode.json()
        else ""
      val vars: List[String] = List(v0, v1, v2)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("waterfallgroupgap")
        in0.map(ve => waterfallgroupgap.fromJson(ve))

        val in1 = mapKV.get("waterfallgap")
        in1.map(ve => waterfallgap.fromJson(ve))

        val in2 = mapKV.get("waterfallmode")
        in2.map(ve => waterfallmode.fromJson(ve))

      }
      this
    }

  }

  case object meta extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val description: String =
      """Draws waterfall trace which is useful graph to displays the contribution of various elements (either positive or negative) in a bar chart. The data visualized by the span of the bars is set in `y` if `orientation` is set th *v* (the default) and the labels are set in `x`. By setting `orientation` to *h*, the roles are interchanged."""

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val vars: List[String] = List()
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV => }
      this
    }

  }

  case object attributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val type_ : String = """waterfall"""

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplate: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Template string used for rendering the information that appear on hover box. Note that this will override `hoverinfo`. Variables are inserted using %{variable}, for example "y: %{y}". Numbers are formatted using d3-format's syntax %{variable:d3-format}, for example "Price: %{y:$.2f}". https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format for details on the formatting syntax. Dates are formatted using d3-time-format's syntax %{variable|d3-time-format}, for example "Day: %{2019-01-01|%A}". https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format for details on the date formatting syntax. The variables available in `hovertemplate` are the ones emitted as event data described at this link https://plot.ly/javascript/plotlyjs-events/#event-data. Additionally, every attributes that can be specified per-point (the ones that are `arrayOk: true`) are available. variables `initial`, `delta` and `final`. Anything contained in tag `<extra>` is displayed in the secondary box, for example "<extra>{fullData.name}</extra>". To hide the secondary box completely, use an empty tag `<extra></extra>`."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val measure: DataArray = DataArray(
      dflt = Some(Array()),
      description = Some(
        """An array containing types of values. By default the values are considered as 'relative'. However; it is possible to use 'total' to compute the sums. Also 'absolute' could be applied to reset the computed total or to declare an initial value where needed."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val xsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  x .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val x: DataArray = DataArray(
      dflt = None,
      description = Some("""Sets the x coordinates.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val name: PChars = PChars(
      dflt = None,
      description = Some(
        """Sets the trace name. The trace name appear as the legend item and on hover."""
      )
    )

    /* §#
     * generateAngleDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val textangle: PAngle = PAngle(
      dflt = Some("""auto"""),
      description = Some(
        """Sets the angle of the tick labels with respect to the bar. For example, a `tickangle` of -90 draws the tick labels vertically. With *auto* the texts may automatically be rotated to fit with the maximum size in bars."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dy: PReal = PReal(
      min = None,
      max = None,
      dflt = Some("""1.0"""),
      description =
        Some("""Sets the y coordinate step. See `y0` for more info.""")
    )

    object insidetextanchor extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """end"""
      val description: Option[String] = Some(
        """Determines if texts are kept at center or start/end points in `textposition` *inside* mode."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def start(): Unit = { selected = TString("start") }

      def middle(): Unit = { selected = TString("middle") }

      def end(): Unit = { selected = TString("end") }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplatesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertemplate .""")
    )

    object hoverinfo extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = Some("""all""")
      val description: Option[String] = Some(
        """Determines which trace information appear on hover. If `none` or `skip` are set, no information is displayed upon hovering. But, if `none` is set, click and hover events are still fired."""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def final_On(): Unit   = { flag_ + "final" }
      def final_Off(): Unit  = { flag_ - "final" }
      def deltaOn(): Unit    = { flag_ + "delta" }
      def deltaOff(): Unit   = { flag_ - "delta" }
      def initialOn(): Unit  = { flag_ + "initial" }
      def initialOff(): Unit = { flag_ - "initial" }
      def textOn(): Unit     = { flag_ + "text" }
      def textOff(): Unit    = { flag_ - "text" }
      def yOn(): Unit        = { flag_ + "y" }
      def yOff(): Unit       = { flag_ - "y" }
      def xOn(): Unit        = { flag_ + "x" }
      def xOff(): Unit       = { flag_ - "x" }
      def nameOn(): Unit     = { flag_ + "name" }
      def nameOff(): Unit    = { flag_ - "name" }
      def skipOn(): Unit     = { flag_ + "skip" }
      def skipOff(): Unit    = { flag_ - "skip" }
      def noneOn(): Unit     = { flag_ + "none" }
      def noneOff(): Unit    = { flag_ - "none" }
      def allOn(): Unit      = { flag_ + "all" }
      def allOff(): Unit     = { flag_ - "all" }
    }

    object visible extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Boolean = true
      val description: Option[String] = Some(
        """Determines whether or not this trace is visible. If *legendonly*, the trace is not drawn, but can appear as a legend item (provided that the legend itself is visible)."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) => t.toString
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => TBoolean(s)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def true_(): Unit = { selected = TBoolean(true) }

      def false_(): Unit = { selected = TBoolean(false) }

      def legendonly(): Unit = { selected = TString("legendonly") }

    }

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val y: DataArray = DataArray(
      dflt = None,
      description = Some("""Sets the y coordinates.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val textpositionsrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  textposition .""")
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uirevision: PAny = PAny(
      dflt = None,
      description = Some(
        """Controls persistence of some user-driven changes to the trace: `constraintrange` in `parcoords` traces, as well as some `editable: true` modifications such as `name` and `colorbar.title`. Defaults to `layout.uirevision`. Note that other user-driven trace attribute changes are controlled by `layout` attributes: `trace.visible` is controlled by `layout.legend.uirevision`, `selectedpoints` is controlled by `layout.selectionrevision`, and `colorbar.(x|y)` (accessible with `config: {editable: true}`) is controlled by `layout.editrevision`. Trace changes are tracked by `uid`, which only falls back on trace index if no `uid` is provided. So if your app can add/remove traces before the end of the `data` array, such that the same trace has a different index, you can still preserve user-driven changes if you give each trace a `uid` that stays with it as it moves."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val y0: PAny = PAny(
      dflt = Some("""0.0"""),
      description = Some(
        """Alternate to `y`. Builds a linear space of y coordinates. Use with `dy` where `y0` is the starting coordinate and `dy` the step."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val measuresrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  measure .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ysrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  y .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val offsetsrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  offset .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val idssrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  ids .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val text: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Sets text elements associated with each (x,y) pair. If a single string, the same string appears over all the data points. If an array of string, the items are mapped in order to the this trace's (x,y) coordinates. If trace `hoverinfo` contains a *text* flag and *hovertext* is not set, these elements will be seen in the hover labels."""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val cliponaxis: PBool = PBool(
      dflt = Some(true),
      description = Some(
        """Determines whether the text nodes are clipped about the subplot axes. To show the text nodes above axis lines and tick labels, make sure to set `xaxis.layer` and `yaxis.layer` to *below traces*."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hoverinfosrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hoverinfo .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertextsrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertext .""")
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val base: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description =
        Some("""Sets where the bar base is drawn (in position axis units).""")
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val x0: PAny = PAny(
      dflt = Some("""0.0"""),
      description = Some(
        """Alternate to `x`. Builds a linear space of x coordinates. Use with `dx` where `x0` is the starting coordinate and `dx` the step."""
      )
    )

    object textposition extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """none"""
      val description: Option[String] = Some(
        """Specifies the location of the `text`. *inside* positions `text` inside, next to the bar end (rotated and scaled if needed). *outside* positions `text` outside, next to the bar end (scaled if needed), unless there is another bar stacked on this one, then the text gets pushed inside. *auto* tries to position `text` inside the bar, but if the bar is too small and no bar is stacked on this one the text is moved outside."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def none(): Unit = { selected = TString("none") }

      def auto(): Unit = { selected = TString("auto") }

      def outside(): Unit = { selected = TString("outside") }

      def inside(): Unit = { selected = TString("inside") }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val alignmentgroup: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Set several traces linked to the same position axis or matching axes to the same alignmentgroup. This controls whether bars compute their positional range dependently or independently."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val selectedpoints: PAny = PAny(
      dflt = None,
      description = Some(
        """Array containing integer indices of selected points. Has an effect only for traces that support selections. Note that an empty array means an empty selection where the `unselected` are turned on for all points, whereas, any other non-array values means no selection all where the `selected` and `unselected` styles have no effect."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val meta: PAny = PAny(
      dflt = None,
      description = Some(
        """Assigns extra meta information associated with this trace that can be used in various text attributes. Attributes such as trace `name`, graph, axis and colorbar `title.text`, annotation `text` `rangeselector`, `updatemenues` and `sliders` `label` text all support `meta`. To access the trace `meta` values in an attribute in the same trace, simply use `%{meta[i]}` where `i` is the index or key of the `meta` item in question. To access trace `meta` in layout attributes, use `%{data[n[.meta[i]}` where `i` is the index or key of the `meta` and `n` is the trace index."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val offset: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description = Some(
        """Shifts the position where the bar is drawn (in position axis units). In *group* barmode, traces that set *offset* will be excluded and drawn in *overlay* mode instead."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val offsetgroup: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Set several traces linked to the same position axis or matching axes to the same offsetgroup where bars of the same position coordinate will line up."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val texttemplatesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  texttemplate .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val textsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  text .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertext: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Sets hover text elements associated with each (x,y) pair. If a single string, the same string appears over all the data points. If an array of string, the items are mapped in order to the this trace's (x,y) coordinates. To be seen, trace `hoverinfo` must contain a *text* flag."""
      )
    )

    object constraintext extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """both"""
      val description: Option[String] = Some(
        """Constrain the size of text inside or outside a bar to be no larger than the bar itself."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def none(): Unit = { selected = TString("none") }

      def both(): Unit = { selected = TString("both") }

      def outside(): Unit = { selected = TString("outside") }

      def inside(): Unit = { selected = TString("inside") }

    }

    object orientation extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val description: Option[String] = Some(
        """Sets the orientation of the bars. With *v* (*h*), the value of the each bar spans along the vertical (horizontal)."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def h(): Unit = { selected = TString("h") }

      def v(): Unit = { selected = TString("v") }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uid: PChars = PChars(
      dflt = None,
      description = Some(
        """Assign an id to this trace, Use this to provide object constancy between traces during animations and transitions."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val legendgroup: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Sets the legend group for this trace. Traces part of the same legend group hide/show at the same time when toggling legend items."""
      )
    )

    object textinfo extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = None
      val description: Option[String] = Some(
        """Determines which trace information appear on the graph. In the case of having multiple waterfalls, totals are computed separately (per trace)."""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def final_On(): Unit   = { flag_ + "final" }
      def final_Off(): Unit  = { flag_ - "final" }
      def deltaOn(): Unit    = { flag_ + "delta" }
      def deltaOff(): Unit   = { flag_ - "delta" }
      def initialOn(): Unit  = { flag_ + "initial" }
      def initialOff(): Unit = { flag_ - "initial" }
      def textOn(): Unit     = { flag_ + "text" }
      def textOff(): Unit    = { flag_ - "text" }
      def labelOn(): Unit    = { flag_ + "label" }
      def labelOff(): Unit   = { flag_ - "label" }
      def noneOn(): Unit     = { flag_ + "none" }
      def noneOff(): Unit    = { flag_ - "none" }
    }

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val opacity: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = Some("""1.0"""),
      description = Some("""Sets the opacity of the trace.""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ids: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns id labels to each datum. These ids for object constancy of data points during animation. Should be an array of strings, not numbers or any other type."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dx: PReal = PReal(
      min = None,
      max = None,
      dflt = Some("""1.0"""),
      description =
        Some("""Sets the x coordinate step. See `x0` for more info.""")
    )

    /* §#
     * generateSubPlotIDDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val yaxis: SubPlotID = SubPlotID(
      dflt = Some("""y"""),
      description = Some(
        """Sets a reference between this trace's y coordinates and a 2D cartesian y axis. If *y* (the default value), the y coordinates refer to `layout.yaxis`. If *y2*, the y coordinates refer to `layout.yaxis2`, and so on."""
      )
    )

    /* §#
     * generateSubPlotIDDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val xaxis: SubPlotID = SubPlotID(
      dflt = Some("""x"""),
      description = Some(
        """Sets a reference between this trace's x coordinates and a 2D cartesian x axis. If *x* (the default value), the x coordinates refer to `layout.xaxis`. If *x2*, the x coordinates refer to `layout.xaxis2`, and so on."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val width: PReal = PReal(
      min = Some(0.0),
      max = None,
      dflt = None,
      description = Some("""Sets the bar width (in position axis units).""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val metasrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  meta .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdata: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns extra data each datum. This may be useful when listening to hover, click and selection events. Note that, *scatter* traces also appends customdata items in the markers DOM elements"""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val showlegend: PBool = PBool(
      dflt = Some(true),
      description = Some(
        """Determines whether or not an item corresponding to this trace is shown in the legend."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val widthsrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  width .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val texttemplate: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Template string used for rendering the information text that appear on points. Note that this will override `textinfo`. Variables are inserted using %{variable}, for example "y: %{y}". Numbers are formatted using d3-format's syntax %{variable:d3-format}, for example "Price: %{y:$.2f}". https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format for details on the formatting syntax. Dates are formatted using d3-time-format's syntax %{variable|d3-time-format}, for example "Day: %{2019-01-01|%A}". https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format for details on the date formatting syntax. Every attributes that can be specified per-point (the ones that are `arrayOk: true`) are available. variables `initial`, `delta`, `final` and `label`."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdatasrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  customdata .""")
    )

    case object insidetextfont extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""
      val description: String =
        """Sets the font used for `text` lying inside the bar."""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal =
        PReal(min = Some(1.0), max = None, dflt = None, description = None)

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val familysrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  family .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  color .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = None
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val sizesrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  size .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val family: PChars = PChars(
        dflt = None,
        description = Some(
          """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val v1 =
          if (familysrc.json().length > 0)
            "\"familysrc\"" + ":" + familysrc.json()
          else ""
        val v2 =
          if (colorsrc.json().length > 0) "\"colorsrc\"" + ":" + colorsrc.json()
          else ""
        val v3 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v4 =
          if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
          else ""
        val v5 =
          if (family.json().length > 0) "\"family\"" + ":" + family.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("size")
          in0.map(ve => size.fromJson(ve))

          val in1 = mapKV.get("familysrc")
          in1.map(ve => familysrc.fromJson(ve))

          val in2 = mapKV.get("colorsrc")
          in2.map(ve => colorsrc.fromJson(ve))

          val in3 = mapKV.get("color")
          in3.map(ve => color.fromJson(ve))

          val in4 = mapKV.get("sizesrc")
          in4.map(ve => sizesrc.fromJson(ve))

          val in5 = mapKV.get("family")
          in5.map(ve => family.fromJson(ve))

        }
        this
      }

    }

    case object increasing extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """style"""

      case object marker extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """style"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description =
            Some("""Sets the marker color of all increasing values.""")
        )

        case object line extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String     = """object"""
          val editType: String = """style"""

          /* §#
           * generateColor(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val color: PColor = PColor(
            dflt = None,
            description =
              Some("""Sets the line color of all increasing values.""")
          )

          /* §#
           * generateNumberDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val width: PReal = PReal(
            min = Some(0.0),
            max = None,
            dflt = Some("""0.0"""),
            description =
              Some("""Sets the line width of all increasing values.""")
          )

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (color.json().length > 0) "\"color\"" + ":" + color.json()
              else ""
            val v1 =
              if (width.json().length > 0) "\"width\"" + ":" + width.json()
              else ""
            val vars: List[String] = List(v0, v1)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("color")
              in0.map(ve => color.fromJson(ve))

              val in1 = mapKV.get("width")
              in1.map(ve => width.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
          val v1 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val vars: List[String] = List(v0, v1)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("line")
            in0.map(ve => line.fromJson(ve))

            val in1 = mapKV.get("color")
            in1.map(ve => color.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (marker.json().length > 0) "\"marker\"" + ":" + marker.json()
          else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("marker")
          in0.map(ve => marker.fromJson(ve))

        }
        this
      }

    }

    case object textfont extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String        = """object"""
      val description: String = """Sets the font used for `text`."""
      val editType: String    = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal =
        PReal(min = Some(1.0), max = None, dflt = None, description = None)

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val familysrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  family .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  color .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = None
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val sizesrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  size .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val family: PChars = PChars(
        dflt = None,
        description = Some(
          """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val v1 =
          if (familysrc.json().length > 0)
            "\"familysrc\"" + ":" + familysrc.json()
          else ""
        val v2 =
          if (colorsrc.json().length > 0) "\"colorsrc\"" + ":" + colorsrc.json()
          else ""
        val v3 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v4 =
          if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
          else ""
        val v5 =
          if (family.json().length > 0) "\"family\"" + ":" + family.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("size")
          in0.map(ve => size.fromJson(ve))

          val in1 = mapKV.get("familysrc")
          in1.map(ve => familysrc.fromJson(ve))

          val in2 = mapKV.get("colorsrc")
          in2.map(ve => colorsrc.fromJson(ve))

          val in3 = mapKV.get("color")
          in3.map(ve => color.fromJson(ve))

          val in4 = mapKV.get("sizesrc")
          in4.map(ve => sizesrc.fromJson(ve))

          val in5 = mapKV.get("family")
          in5.map(ve => family.fromJson(ve))

        }
        this
      }

    }

    case object stream extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val maxpoints: PReal = PReal(
        min = Some(0.0),
        max = Some(10000.0),
        dflt = Some("""500.0"""),
        description = Some(
          """Sets the maximum number of points to keep on the plots from an incoming stream. If `maxpoints` is set to *50*, only the newest 50 points will be displayed on the plot."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val token: PChars = PChars(
        dflt = None,
        description = Some(
          """The stream id number links a data trace on a plot with a stream. See https://plot.ly/settings for more details."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (maxpoints.json().length > 0)
            "\"maxpoints\"" + ":" + maxpoints.json()
          else ""
        val v1 =
          if (token.json().length > 0) "\"token\"" + ":" + token.json() else ""
        val vars: List[String] = List(v0, v1)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("maxpoints")
          in0.map(ve => maxpoints.fromJson(ve))

          val in1 = mapKV.get("token")
          in1.map(ve => token.fromJson(ve))

        }
        this
      }

    }

    case object hoverlabel extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """none"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val alignsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  align .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolor: PColor = PColor(
        dflt = None,
        description = Some(
          """Sets the background color of the hover labels for this trace"""
        )
      )

      object align extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """auto"""
        val description: Option[String] = Some(
          """Sets the horizontal alignment of the text content within hover label box. Has an effect only if the hover label text spans more two or more lines"""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def auto(): Unit = { selected = TString("auto") }

        def right(): Unit = { selected = TString("right") }

        def left(): Unit = { selected = TString("left") }

      }

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelength: PInt = PInt(
        min = Some(-1),
        max = None,
        dflt = Some(15),
        description = Some(
          """Sets the default length (in number of characters) of the trace name in the hover labels for all traces. -1 shows the whole name regardless of length. 0-3 shows the first 0-3 characters, and an integer >3 will show the whole name if it is less than that many characters, but if it is longer, will truncate to `namelength - 3` characters and add an ellipsis."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelengthsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  namelength .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolor: PColor = PColor(
        dflt = None,
        description =
          Some("""Sets the border color of the hover labels for this trace.""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bordercolor .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bgcolor .""")
      )

      case object font extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Sets the font used in hover labels."""
        val editType: String    = """none"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val familysrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  family .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val colorsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  color .""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val sizesrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  size .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val v1 =
            if (familysrc.json().length > 0)
              "\"familysrc\"" + ":" + familysrc.json()
            else ""
          val v2 =
            if (colorsrc.json().length > 0)
              "\"colorsrc\"" + ":" + colorsrc.json()
            else ""
          val v3 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v4 =
            if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
            else ""
          val v5 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("size")
            in0.map(ve => size.fromJson(ve))

            val in1 = mapKV.get("familysrc")
            in1.map(ve => familysrc.fromJson(ve))

            val in2 = mapKV.get("colorsrc")
            in2.map(ve => colorsrc.fromJson(ve))

            val in3 = mapKV.get("color")
            in3.map(ve => color.fromJson(ve))

            val in4 = mapKV.get("sizesrc")
            in4.map(ve => sizesrc.fromJson(ve))

            val in5 = mapKV.get("family")
            in5.map(ve => family.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
        val v1 =
          if (alignsrc.json().length > 0) "\"alignsrc\"" + ":" + alignsrc.json()
          else ""
        val v2 =
          if (bgcolor.json().length > 0) "\"bgcolor\"" + ":" + bgcolor.json()
          else ""
        val v3 =
          if (align.json().length > 0) "\"align\"" + ":" + align.json() else ""
        val v4 =
          if (namelength.json().length > 0)
            "\"namelength\"" + ":" + namelength.json()
          else ""
        val v5 =
          if (namelengthsrc.json().length > 0)
            "\"namelengthsrc\"" + ":" + namelengthsrc.json()
          else ""
        val v6 =
          if (bordercolor.json().length > 0)
            "\"bordercolor\"" + ":" + bordercolor.json()
          else ""
        val v7 =
          if (bordercolorsrc.json().length > 0)
            "\"bordercolorsrc\"" + ":" + bordercolorsrc.json()
          else ""
        val v8 =
          if (bgcolorsrc.json().length > 0)
            "\"bgcolorsrc\"" + ":" + bgcolorsrc.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6, v7, v8)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("font")
          in0.map(ve => font.fromJson(ve))

          val in1 = mapKV.get("alignsrc")
          in1.map(ve => alignsrc.fromJson(ve))

          val in2 = mapKV.get("bgcolor")
          in2.map(ve => bgcolor.fromJson(ve))

          val in3 = mapKV.get("align")
          in3.map(ve => align.fromJson(ve))

          val in4 = mapKV.get("namelength")
          in4.map(ve => namelength.fromJson(ve))

          val in5 = mapKV.get("namelengthsrc")
          in5.map(ve => namelengthsrc.fromJson(ve))

          val in6 = mapKV.get("bordercolor")
          in6.map(ve => bordercolor.fromJson(ve))

          val in7 = mapKV.get("bordercolorsrc")
          in7.map(ve => bordercolorsrc.fromJson(ve))

          val in8 = mapKV.get("bgcolorsrc")
          in8.map(ve => bgcolorsrc.fromJson(ve))

        }
        this
      }

    }

    case object connector extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """plot"""

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val visible: PBool = PBool(
        dflt = Some(true),
        description = Some("""Determines if connector lines are drawn. """)
      )

      object mode extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """between"""
        val description: Option[String] = Some(
          """Sets the shape of connector lines."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def between(): Unit = { selected = TString("between") }

        def spanning(): Unit = { selected = TString("spanning") }

      }

      case object line extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """plot"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = Some("""#444"""),
          description = Some("""Sets the line color.""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dash: PChars = PChars(
          dflt = Some("""solid"""),
          description = Some(
            """Sets the dash style of lines. Set to a dash type string (*solid*, *dot*, *dash*, *longdash*, *dashdot*, or *longdashdot*) or a dash length list in px (eg *5px,10px,2px,2px*)."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val width: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""2.0"""),
          description = Some("""Sets the line width (in px).""")
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v1 =
            if (dash.json().length > 0) "\"dash\"" + ":" + dash.json() else ""
          val v2 =
            if (width.json().length > 0) "\"width\"" + ":" + width.json()
            else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("color")
            in0.map(ve => color.fromJson(ve))

            val in1 = mapKV.get("dash")
            in1.map(ve => dash.fromJson(ve))

            val in2 = mapKV.get("width")
            in2.map(ve => width.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
        val v1 =
          if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
          else ""
        val v2 =
          if (mode.json().length > 0) "\"mode\"" + ":" + mode.json() else ""
        val vars: List[String] = List(v0, v1, v2)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("line")
          in0.map(ve => line.fromJson(ve))

          val in1 = mapKV.get("visible")
          in1.map(ve => visible.fromJson(ve))

          val in2 = mapKV.get("mode")
          in2.map(ve => mode.fromJson(ve))

        }
        this
      }

    }

    case object transforms extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""

      case object items extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        case object transform extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""
          val description: String =
            """An array of operations that manipulate the trace data, for example filtering or sorting the data arrays."""
          val editType: String = """calc"""

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val vars: List[String] = List()
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV => }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (transform.json().length > 0)
              "\"transform\"" + ":" + transform.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("transform")
            in0.map(ve => transform.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (items.json().length > 0) "\"items\"" + ":" + items.json() else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("items")
          in0.map(ve => items.fromJson(ve))

        }
        this
      }

    }

    case object decreasing extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """style"""

      case object marker extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """style"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description =
            Some("""Sets the marker color of all decreasing values.""")
        )

        case object line extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String     = """object"""
          val editType: String = """style"""

          /* §#
           * generateColor(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val color: PColor = PColor(
            dflt = None,
            description =
              Some("""Sets the line color of all decreasing values.""")
          )

          /* §#
           * generateNumberDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val width: PReal = PReal(
            min = Some(0.0),
            max = None,
            dflt = Some("""0.0"""),
            description =
              Some("""Sets the line width of all decreasing values.""")
          )

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (color.json().length > 0) "\"color\"" + ":" + color.json()
              else ""
            val v1 =
              if (width.json().length > 0) "\"width\"" + ":" + width.json()
              else ""
            val vars: List[String] = List(v0, v1)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("color")
              in0.map(ve => color.fromJson(ve))

              val in1 = mapKV.get("width")
              in1.map(ve => width.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
          val v1 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val vars: List[String] = List(v0, v1)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("line")
            in0.map(ve => line.fromJson(ve))

            val in1 = mapKV.get("color")
            in1.map(ve => color.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (marker.json().length > 0) "\"marker\"" + ":" + marker.json()
          else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("marker")
          in0.map(ve => marker.fromJson(ve))

        }
        this
      }

    }

    case object totals extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """style"""

      case object marker extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """style"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = Some(
            """Sets the marker color of all intermediate sums and total values."""
          )
        )

        case object line extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String     = """object"""
          val editType: String = """style"""

          /* §#
           * generateColor(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val color: PColor = PColor(
            dflt = None,
            description = Some(
              """Sets the line color of all intermediate sums and total values."""
            )
          )

          /* §#
           * generateNumberDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val width: PReal = PReal(
            min = Some(0.0),
            max = None,
            dflt = Some("""0.0"""),
            description = Some(
              """Sets the line width of all intermediate sums and total values."""
            )
          )

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (color.json().length > 0) "\"color\"" + ":" + color.json()
              else ""
            val v1 =
              if (width.json().length > 0) "\"width\"" + ":" + width.json()
              else ""
            val vars: List[String] = List(v0, v1)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("color")
              in0.map(ve => color.fromJson(ve))

              val in1 = mapKV.get("width")
              in1.map(ve => width.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
          val v1 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val vars: List[String] = List(v0, v1)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("line")
            in0.map(ve => line.fromJson(ve))

            val in1 = mapKV.get("color")
            in1.map(ve => color.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (marker.json().length > 0) "\"marker\"" + ":" + marker.json()
          else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("marker")
          in0.map(ve => marker.fromJson(ve))

        }
        this
      }

    }

    case object outsidetextfont extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""
      val description: String =
        """Sets the font used for `text` lying outside the bar."""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal =
        PReal(min = Some(1.0), max = None, dflt = None, description = None)

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val familysrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  family .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  color .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = None
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val sizesrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  size .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val family: PChars = PChars(
        dflt = None,
        description = Some(
          """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val v1 =
          if (familysrc.json().length > 0)
            "\"familysrc\"" + ":" + familysrc.json()
          else ""
        val v2 =
          if (colorsrc.json().length > 0) "\"colorsrc\"" + ":" + colorsrc.json()
          else ""
        val v3 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v4 =
          if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
          else ""
        val v5 =
          if (family.json().length > 0) "\"family\"" + ":" + family.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("size")
          in0.map(ve => size.fromJson(ve))

          val in1 = mapKV.get("familysrc")
          in1.map(ve => familysrc.fromJson(ve))

          val in2 = mapKV.get("colorsrc")
          in2.map(ve => colorsrc.fromJson(ve))

          val in3 = mapKV.get("color")
          in3.map(ve => color.fromJson(ve))

          val in4 = mapKV.get("sizesrc")
          in4.map(ve => sizesrc.fromJson(ve))

          val in5 = mapKV.get("family")
          in5.map(ve => family.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val v61 = "\"type\" : \"waterfall\""
      val v0 =
        if (insidetextfont.json().length > 0)
          "\"insidetextfont\"" + ":" + insidetextfont.json()
        else ""
      val v1 =
        if (increasing.json().length > 0)
          "\"increasing\"" + ":" + increasing.json()
        else ""
      val v2 =
        if (textfont.json().length > 0) "\"textfont\"" + ":" + textfont.json()
        else ""
      val v3 =
        if (stream.json().length > 0) "\"stream\"" + ":" + stream.json() else ""
      val v4 =
        if (hoverlabel.json().length > 0)
          "\"hoverlabel\"" + ":" + hoverlabel.json()
        else ""
      val v5 =
        if (connector.json().length > 0)
          "\"connector\"" + ":" + connector.json()
        else ""
      val v6 =
        if (transforms.json().length > 0)
          "\"transforms\"" + ":" + transforms.json()
        else ""
      val v7 =
        if (decreasing.json().length > 0)
          "\"decreasing\"" + ":" + decreasing.json()
        else ""
      val v8 =
        if (totals.json().length > 0) "\"totals\"" + ":" + totals.json() else ""
      val v9 =
        if (outsidetextfont.json().length > 0)
          "\"outsidetextfont\"" + ":" + outsidetextfont.json()
        else ""
      val v10 =
        if (hovertemplate.json().length > 0)
          "\"hovertemplate\"" + ":" + hovertemplate.json()
        else ""
      val v11 =
        if (measure.json().length > 0) "\"measure\"" + ":" + measure.json()
        else ""
      val v12 =
        if (xsrc.json().length > 0) "\"xsrc\"" + ":" + xsrc.json() else ""
      val v13 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
      val v14 =
        if (name.json().length > 0) "\"name\"" + ":" + name.json() else ""
      val v15 =
        if (textangle.json().length > 0)
          "\"textangle\"" + ":" + textangle.json()
        else ""
      val v16 = if (dy.json().length > 0) "\"dy\"" + ":" + dy.json() else ""
      val v17 =
        if (insidetextanchor.json().length > 0)
          "\"insidetextanchor\"" + ":" + insidetextanchor.json()
        else ""
      val v18 =
        if (hovertemplatesrc.json().length > 0)
          "\"hovertemplatesrc\"" + ":" + hovertemplatesrc.json()
        else ""
      val v19 =
        if (hoverinfo.json().length > 0)
          "\"hoverinfo\"" + ":" + hoverinfo.json()
        else ""
      val v20 =
        if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
        else ""
      val v21 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
      val v22 =
        if (textpositionsrc.json().length > 0)
          "\"textpositionsrc\"" + ":" + textpositionsrc.json()
        else ""
      val v23 =
        if (uirevision.json().length > 0)
          "\"uirevision\"" + ":" + uirevision.json()
        else ""
      val v24 = if (y0.json().length > 0) "\"y0\"" + ":" + y0.json() else ""
      val v25 =
        if (measuresrc.json().length > 0)
          "\"measuresrc\"" + ":" + measuresrc.json()
        else ""
      val v26 =
        if (ysrc.json().length > 0) "\"ysrc\"" + ":" + ysrc.json() else ""
      val v27 =
        if (offsetsrc.json().length > 0)
          "\"offsetsrc\"" + ":" + offsetsrc.json()
        else ""
      val v28 =
        if (idssrc.json().length > 0) "\"idssrc\"" + ":" + idssrc.json() else ""
      val v29 =
        if (text.json().length > 0) "\"text\"" + ":" + text.json() else ""
      val v30 =
        if (cliponaxis.json().length > 0)
          "\"cliponaxis\"" + ":" + cliponaxis.json()
        else ""
      val v31 =
        if (hoverinfosrc.json().length > 0)
          "\"hoverinfosrc\"" + ":" + hoverinfosrc.json()
        else ""
      val v32 =
        if (hovertextsrc.json().length > 0)
          "\"hovertextsrc\"" + ":" + hovertextsrc.json()
        else ""
      val v33 =
        if (base.json().length > 0) "\"base\"" + ":" + base.json() else ""
      val v34 = if (x0.json().length > 0) "\"x0\"" + ":" + x0.json() else ""
      val v35 =
        if (textposition.json().length > 0)
          "\"textposition\"" + ":" + textposition.json()
        else ""
      val v36 =
        if (alignmentgroup.json().length > 0)
          "\"alignmentgroup\"" + ":" + alignmentgroup.json()
        else ""
      val v37 =
        if (selectedpoints.json().length > 0)
          "\"selectedpoints\"" + ":" + selectedpoints.json()
        else ""
      val v38 =
        if (meta.json().length > 0) "\"meta\"" + ":" + meta.json() else ""
      val v39 =
        if (offset.json().length > 0) "\"offset\"" + ":" + offset.json() else ""
      val v40 =
        if (offsetgroup.json().length > 0)
          "\"offsetgroup\"" + ":" + offsetgroup.json()
        else ""
      val v41 =
        if (texttemplatesrc.json().length > 0)
          "\"texttemplatesrc\"" + ":" + texttemplatesrc.json()
        else ""
      val v42 =
        if (textsrc.json().length > 0) "\"textsrc\"" + ":" + textsrc.json()
        else ""
      val v43 =
        if (hovertext.json().length > 0)
          "\"hovertext\"" + ":" + hovertext.json()
        else ""
      val v44 =
        if (constraintext.json().length > 0)
          "\"constraintext\"" + ":" + constraintext.json()
        else ""
      val v45 =
        if (orientation.json().length > 0)
          "\"orientation\"" + ":" + orientation.json()
        else ""
      val v46 = if (uid.json().length > 0) "\"uid\"" + ":" + uid.json() else ""
      val v47 =
        if (legendgroup.json().length > 0)
          "\"legendgroup\"" + ":" + legendgroup.json()
        else ""
      val v48 =
        if (textinfo.json().length > 0) "\"textinfo\"" + ":" + textinfo.json()
        else ""
      val v49 =
        if (opacity.json().length > 0) "\"opacity\"" + ":" + opacity.json()
        else ""
      val v50 = if (ids.json().length > 0) "\"ids\"" + ":" + ids.json() else ""
      val v51 = if (dx.json().length > 0) "\"dx\"" + ":" + dx.json() else ""
      val v52 =
        if (yaxis.json().length > 0) "\"yaxis\"" + ":" + yaxis.json() else ""
      val v53 =
        if (xaxis.json().length > 0) "\"xaxis\"" + ":" + xaxis.json() else ""
      val v54 =
        if (width.json().length > 0) "\"width\"" + ":" + width.json() else ""
      val v55 =
        if (metasrc.json().length > 0) "\"metasrc\"" + ":" + metasrc.json()
        else ""
      val v56 =
        if (customdata.json().length > 0)
          "\"customdata\"" + ":" + customdata.json()
        else ""
      val v57 =
        if (showlegend.json().length > 0)
          "\"showlegend\"" + ":" + showlegend.json()
        else ""
      val v58 =
        if (widthsrc.json().length > 0) "\"widthsrc\"" + ":" + widthsrc.json()
        else ""
      val v59 =
        if (texttemplate.json().length > 0)
          "\"texttemplate\"" + ":" + texttemplate.json()
        else ""
      val v60 =
        if (customdatasrc.json().length > 0)
          "\"customdatasrc\"" + ":" + customdatasrc.json()
        else ""
      val vars: List[String] = List(
        v61,
        v0,
        v1,
        v2,
        v3,
        v4,
        v5,
        v6,
        v7,
        v8,
        v9,
        v10,
        v11,
        v12,
        v13,
        v14,
        v15,
        v16,
        v17,
        v18,
        v19,
        v20,
        v21,
        v22,
        v23,
        v24,
        v25,
        v26,
        v27,
        v28,
        v29,
        v30,
        v31,
        v32,
        v33,
        v34,
        v35,
        v36,
        v37,
        v38,
        v39,
        v40,
        v41,
        v42,
        v43,
        v44,
        v45,
        v46,
        v47,
        v48,
        v49,
        v50,
        v51,
        v52,
        v53,
        v54,
        v55,
        v56,
        v57,
        v58,
        v59,
        v60
      )
      val cleanVars = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("insidetextfont")
        in0.map(ve => insidetextfont.fromJson(ve))

        val in1 = mapKV.get("increasing")
        in1.map(ve => increasing.fromJson(ve))

        val in2 = mapKV.get("textfont")
        in2.map(ve => textfont.fromJson(ve))

        val in3 = mapKV.get("stream")
        in3.map(ve => stream.fromJson(ve))

        val in4 = mapKV.get("hoverlabel")
        in4.map(ve => hoverlabel.fromJson(ve))

        val in5 = mapKV.get("connector")
        in5.map(ve => connector.fromJson(ve))

        val in6 = mapKV.get("transforms")
        in6.map(ve => transforms.fromJson(ve))

        val in7 = mapKV.get("decreasing")
        in7.map(ve => decreasing.fromJson(ve))

        val in8 = mapKV.get("totals")
        in8.map(ve => totals.fromJson(ve))

        val in9 = mapKV.get("outsidetextfont")
        in9.map(ve => outsidetextfont.fromJson(ve))

        val in10 = mapKV.get("hovertemplate")
        in10.map(ve => hovertemplate.fromJson(ve))

        val in11 = mapKV.get("measure")
        in11.map(ve => measure.fromJson(ve))

        val in12 = mapKV.get("xsrc")
        in12.map(ve => xsrc.fromJson(ve))

        val in13 = mapKV.get("x")
        in13.map(ve => x.fromJson(ve))

        val in14 = mapKV.get("name")
        in14.map(ve => name.fromJson(ve))

        val in15 = mapKV.get("textangle")
        in15.map(ve => textangle.fromJson(ve))

        val in16 = mapKV.get("dy")
        in16.map(ve => dy.fromJson(ve))

        val in17 = mapKV.get("insidetextanchor")
        in17.map(ve => insidetextanchor.fromJson(ve))

        val in18 = mapKV.get("hovertemplatesrc")
        in18.map(ve => hovertemplatesrc.fromJson(ve))

        val in19 = mapKV.get("hoverinfo")
        in19.map(ve => hoverinfo.fromJson(ve))

        val in20 = mapKV.get("visible")
        in20.map(ve => visible.fromJson(ve))

        val in21 = mapKV.get("y")
        in21.map(ve => y.fromJson(ve))

        val in22 = mapKV.get("textpositionsrc")
        in22.map(ve => textpositionsrc.fromJson(ve))

        val in23 = mapKV.get("uirevision")
        in23.map(ve => uirevision.fromJson(ve))

        val in24 = mapKV.get("y0")
        in24.map(ve => y0.fromJson(ve))

        val in25 = mapKV.get("measuresrc")
        in25.map(ve => measuresrc.fromJson(ve))

        val in26 = mapKV.get("ysrc")
        in26.map(ve => ysrc.fromJson(ve))

        val in27 = mapKV.get("offsetsrc")
        in27.map(ve => offsetsrc.fromJson(ve))

        val in28 = mapKV.get("idssrc")
        in28.map(ve => idssrc.fromJson(ve))

        val in29 = mapKV.get("text")
        in29.map(ve => text.fromJson(ve))

        val in30 = mapKV.get("cliponaxis")
        in30.map(ve => cliponaxis.fromJson(ve))

        val in31 = mapKV.get("hoverinfosrc")
        in31.map(ve => hoverinfosrc.fromJson(ve))

        val in32 = mapKV.get("hovertextsrc")
        in32.map(ve => hovertextsrc.fromJson(ve))

        val in33 = mapKV.get("base")
        in33.map(ve => base.fromJson(ve))

        val in34 = mapKV.get("x0")
        in34.map(ve => x0.fromJson(ve))

        val in35 = mapKV.get("textposition")
        in35.map(ve => textposition.fromJson(ve))

        val in36 = mapKV.get("alignmentgroup")
        in36.map(ve => alignmentgroup.fromJson(ve))

        val in37 = mapKV.get("selectedpoints")
        in37.map(ve => selectedpoints.fromJson(ve))

        val in38 = mapKV.get("meta")
        in38.map(ve => meta.fromJson(ve))

        val in39 = mapKV.get("offset")
        in39.map(ve => offset.fromJson(ve))

        val in40 = mapKV.get("offsetgroup")
        in40.map(ve => offsetgroup.fromJson(ve))

        val in41 = mapKV.get("texttemplatesrc")
        in41.map(ve => texttemplatesrc.fromJson(ve))

        val in42 = mapKV.get("textsrc")
        in42.map(ve => textsrc.fromJson(ve))

        val in43 = mapKV.get("hovertext")
        in43.map(ve => hovertext.fromJson(ve))

        val in44 = mapKV.get("constraintext")
        in44.map(ve => constraintext.fromJson(ve))

        val in45 = mapKV.get("orientation")
        in45.map(ve => orientation.fromJson(ve))

        val in46 = mapKV.get("uid")
        in46.map(ve => uid.fromJson(ve))

        val in47 = mapKV.get("legendgroup")
        in47.map(ve => legendgroup.fromJson(ve))

        val in48 = mapKV.get("textinfo")
        in48.map(ve => textinfo.fromJson(ve))

        val in49 = mapKV.get("opacity")
        in49.map(ve => opacity.fromJson(ve))

        val in50 = mapKV.get("ids")
        in50.map(ve => ids.fromJson(ve))

        val in51 = mapKV.get("dx")
        in51.map(ve => dx.fromJson(ve))

        val in52 = mapKV.get("yaxis")
        in52.map(ve => yaxis.fromJson(ve))

        val in53 = mapKV.get("xaxis")
        in53.map(ve => xaxis.fromJson(ve))

        val in54 = mapKV.get("width")
        in54.map(ve => width.fromJson(ve))

        val in55 = mapKV.get("metasrc")
        in55.map(ve => metasrc.fromJson(ve))

        val in56 = mapKV.get("customdata")
        in56.map(ve => customdata.fromJson(ve))

        val in57 = mapKV.get("showlegend")
        in57.map(ve => showlegend.fromJson(ve))

        val in58 = mapKV.get("widthsrc")
        in58.map(ve => widthsrc.fromJson(ve))

        val in59 = mapKV.get("texttemplate")
        in59.map(ve => texttemplate.fromJson(ve))

        val in60 = mapKV.get("customdatasrc")
        in60.map(ve => customdatasrc.fromJson(ve))

      }
      this
    }

  }

}
