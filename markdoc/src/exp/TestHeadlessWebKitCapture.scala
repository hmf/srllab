package exp

import java.nio.file.Paths
import webkit.HeadlessWebKit
import better.files._

/**
 *
 * mill mill.scalalib.GenIdea/idea
 *
 * mill mdocs.compile
 * mill mdocs.run
 * mill mdocs.run noGraal
 *
 * mill mdocs.{compile, run}
 * mill --watch mdocs.run
 *
 * mill -i mdocs.console
 * mill -i mdocs.repl
 *
 * mill -i mdocs.runMain mdocs.TestHeadlessWebKitCapture
 * mill --watch mill -i mdocs.runMain mdocs.TestHeadlessWebKitCapture
 *
 * Example of using the headless webkit utility class to take snapshots of a
 * site.
 */
object TestHeadlessWebKitCapture {


  def main(args: Array[String]): Unit = {
    val cwd = Paths.get(".").toAbsolutePath.normalize().toString
    println(cwd)
    HeadlessWebKit.launchBackground(args)

    val property = "java.io.tmpdir"
    val tempDir = System.getProperty(property)
    val tmp = tempDir

    // Test for HTTP URL
    val image0 = HeadlessWebKit.capture("""https://www.google.com/""", tmp / "google.png")
    println(s"HeadlessWebKit.capture image0 = $image0 ------------")

    // Test for absolute file URI
    //val image1 = HeadlessWebKit.capture("""file:///home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test.html""", tmp + "test1", "png")
    //println(s"HeadlessWebKit.capture image1 = $image1 ------------")

    // Test for relative file URI
    val image2 = HeadlessWebKit.capture(s"""file://$cwd/./mdocs/src/mdocs/test.html""", tmp / "test2.png")
    println(s"HeadlessWebKit.capture image1 = $image2 ------------")

    val relativeToCWD = HeadlessWebKit.file("exp/src/mdocs/test.html")
    println(relativeToCWD)
    println(s"""file://$cwd/./mdocs/src/mdocs/test.html""")

    val absoluteFile = HeadlessWebKit.file("/home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test.html")
    println(absoluteFile)
    println("""file:///home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test.html""")

    HeadlessWebKit.stop()
    println()
    //System.exit(0)
  }
}
