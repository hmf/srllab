package splotly

sealed trait IR {
  def parent: SchemaPath
  def name: String
}

// TODO: make IR and descendants OfCompound
// TODO: add parent name to of OfType?
class CompoundDefinition(val depth: Int,
                         override val parent: SchemaPath,
                         override val name: String,
                         val objs: Map[String, CompoundDefinition] = Map[String, ObjectDefinition](),
                         val member: Map[String, PrimitiveDefinition] = Map[String, PrimitiveDefinition](),
                         val metaValues: Map[String, ValueDefinition] = Map[String, ValueDefinition](),
                         val arrs: Map[String, List[ValueDefinition]] = Map[String, List[ValueDefinition]](),
                         val classType: Option[String]
                        ) extends IR {
  val IRType = "CompoundDefinition"

  override def toString: String = {
    s"""
       |--------------------------------
       |${IRType}(
       | parent     = $parent
       | typeName   = $name
       | objs:
       | ...........
       | ${objs.mkString(";\n")}
       | members:
       | ...........
       | ${member.mkString(";\n")}
       | values:
       | ...........
       | ${metaValues.mkString(";\n")}
       | arrs:
       | ...........
       | ${arrs.mkString(";\n")}
       |)
       |------------------------------------""".stripMargin
  }
}

case class ObjectDefinition(override val depth: Int,
                            override val parent: SchemaPath,
                            override val name: String,
                            override val objs: Map[String, CompoundDefinition] = Map[String, ObjectDefinition](),
                            override val member: Map[String, PrimitiveDefinition] = Map[String, PrimitiveDefinition](),
                            override val metaValues: Map[String, ValueDefinition] = Map[String, ValueDefinition](),
                            override val arrs: Map[String, List[ValueDefinition]] = Map[String, List[ValueDefinition]](),
                            override val classType: Option[String]
                           ) extends CompoundDefinition(depth, parent, name, objs, member,metaValues, arrs, classType) {

  override val IRType = "ObjectDefinition"

  def addMember(name:String, value: PrimitiveDefinition): ObjectDefinition = {
    copy(member = member + (name -> value))
  }

  def addObj(name:String, value: ObjectDefinition): ObjectDefinition = {
    copy(objs = objs + (name -> value))
  }

  def addArrs(name:String, value: List[ValueDefinition]): ObjectDefinition = {
    copy(arrs = arrs + (name -> value))
  }
}

case class ClassDefinition(override val depth: Int,
                           override val parent: SchemaPath,
                           override val name: String,
                           override val objs: Map[String, CompoundDefinition] = Map[String, ObjectDefinition](),
                           override val member: Map[String, PrimitiveDefinition] = Map[String, PrimitiveDefinition](),
                           override val metaValues: Map[String, ValueDefinition] = Map[String, ValueDefinition](),
                           override val arrs: Map[String, List[ValueDefinition]] = Map[String, List[ValueDefinition]](),
                           override val classType: Option[String]
                           ) extends CompoundDefinition(depth, parent, name, objs, member,metaValues, arrs, classType)  {

  override val IRType = "ClassDefinition"

  def addMember(name:String, value: PrimitiveDefinition): ClassDefinition = {
    copy(member = member + (name -> value))
  }

  def addObj(name:String, value: ObjectDefinition): ClassDefinition = {
    copy(objs = objs + (name -> value))
  }

  def addArrs(name:String, value: List[ValueDefinition]): ClassDefinition = {
    copy(arrs = arrs + (name -> value))
  }
}

case class ValueDefinition(override val parent: SchemaPath,
                           override val name: String,
                           member: OfType = NA)
  extends IR {

  override def toString: String = {
    s"""
       |ValueDefinition(
       | parent     = $parent
       | typeName   = $name
       | members    = $member
       |)""".stripMargin
  }
}

object ValueDefinition {
  def apply(parent: SchemaPath, typeName: String): ValueDefinition =
    new ValueDefinition(parent, typeName)
}

object Members {

  def att(p:IR) = s"@ ${from(p.name, p.parent)}"

  def optionDouble(e: OfType): Either[Error, Option[Double]] = {
    e match {
      case TDouble(e) => Right(Some(e))
      case NAPrimitive => Right(None)
      case _ => Left(Error(s"Expected a TDouble but got $e"))
    }
  }

  def optionInt(e: OfType): Either[Error, Option[Int]] = {
    e match {
      case TInt(e) => Right(Some(e))
      case NAPrimitive => Right(None)
      case _ => Left(Error(s"Expected a TInt but got $e"))
    }
  }

  def optionBoolean(e: OfType): Either[Error, Option[Boolean]] = {
    e match {
      case TBoolean(e) => Right(Some(e))
      case NAPrimitive => Right(None)
      case _ => Left(Error(s"Expected a TBoolean but got $e"))
    }
  }

  def optionString(e: OfType): Either[Error, Option[String]] = {
    e match {
      case TString(e) => Right(Some(e))
      case NAPrimitive => Right(None)
      case _ => Left(Error(s"Expected a TString but got $e"))
    }
  }

  def optionStringOrDouble(e: OfType): Either[Error, Option[String]] = {
    e match {
      case TString(e) => Right(Some(e))
      case TDouble(e) => Right(Some(e.toString))
      case NAPrimitive => Right(None)
      case _ => Left(Error(s"Expected a TString or TDouble but got $e"))
    }
  }

  def optionStringDoubleOrBoolean(e: OfType): Either[Error, Option[String]] = {
    e match {
      case TString(e) => Right(Some("\"" + e + "\""))
      case TDouble(e) => Right(Some(e.toString))
      case TBoolean(e) => Right(Some(e.toString))
      case NAPrimitive => Right(None)
      case _ => Left(Error(s"Expected a TString or TDouble but got $e"))
    }
  }

  def optionAny(e: OfType): Either[Error, Option[Any]] = {
    e match {
      case TDouble(e) => Right(Some(e))
      case TInt(e) => Right(Some(e))
      case TBoolean(e) => Right(Some(e))
      case TString(e) => Right(Some(e))
      case NAPrimitive => Right(None)
      case TPrimtivesArray(e) =>
        // Assume the array is empty. Otherwise we need to map the elements
        if (e.length == 0)
          // So assume we have doubles
          Right(Some(Array()))
        else
          Left(Error(s"error: expected empty TPrimtivesArray but got a non-empty $e"))
      case _ => Left(Error(s"Expected a Any but got $e"))
    }
  }

  def optionDoubleArray(e: OfType): Either[Error, Option[Array[Double]]] = {
    e match {
      case TDoubleArray(e) => Right(Some(e))
      case TPrimtivesArray(e) =>
        // If an array is empty we cannot determine the type
        if (e.length == 0)
          // So assume we have doubles
          Right(Some(Array()))
        else
          Left(Error(s"error: expected empty TDoubleArray but got a non-empty $e"))
      case _ => Left(Error(s"error: expected TDoubleArray got $e"))
    }
  }

  def optionStringArray(e: OfType): Either[Error, Option[Array[String]]] = {
    e match {
      case TStringArray(a) => Right(Some(a))
      case TPrimtivesArray(e) =>
        // If an array is empty we cannot determine the type
        if (e.length == 0)
          // So assume we have strings
          Right(Some(Array()))
        else
          Left(Error(s"error: expected empty TStringArray but got a non-empty $e"))
      case _               => Left(Error(s"error: expected TStringArray got $e"))
    }
  }

  def optionStringOrBooleanArray(e: OfType): Either[Error, Option[Array[String]]] = {
    e match {
      case TStringArray(a) => Right(Some(a))
      case TBooleanArray(a) => Right(Some(a.map(_.toString)))
      case TPrimtivesArray(e) =>
        // If an array is empty we cannot determine the type
        if (e.length == 0)
        // So assume we have strings
        Right(Some(Array()))
        else
        Left(Error(s"error: expected empty TStringArray but got a non-empty $e"))
      case _               => Left(Error(s"error: expected TStringArray got $e"))
    }
  }

  def getValue[T]( in: PrimitiveDefinition,
                   name: String,
                   get: OfType => Either[Error, T],
                   defaultValue: T): Either[Error, T] = {
    in.member
      .get(name)
      .fold(Right(defaultValue): Either[Error, T])(get)
      .fold(
        e => Left(Error(s"getValue: Error processing $name in ${in.getClass.getTypeName} ${att(in)}" :: e.errs)),
        e => Right(e))
  }

}

// TODO: member should only be visible in the Parser
sealed abstract class PrimitiveDefinition(override val parent: SchemaPath,
                          override val name: String,
                          val member: Map[String, OfType] = Map[String, OfType]())
  extends IR {

  val typeName = "PrimitiveDefinition"

  override def toString: String = {
    s"""
       |$typeName(
       | parent     = $parent
       | typeName   = $name
       | members
       | ${member.mkString(";\n")}
       |)""".stripMargin
  }
}

case class PrimitiveNumber(override val parent: SchemaPath,
                           override val name: String,
                           override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveNumber"

  val min: Either[Error, Option[Double]] = Members.getValue(this, Constants.MIN, Members.optionDouble, None)
  val max: Either[Error, Option[Double]] = Members.getValue(this, Constants.MAX, Members.optionDouble, None)
  // @see https://github.com/plotly/plotly.js/issues/4625
  val dflt: Either[Error, Option[String]] = Members.getValue(this, Constants.DFLT, Members.optionStringDoubleOrBoolean, None)
  //val dflt: Either[Error, Option[String]] = Members.getValue(this, Constants.DFLT, Members.optionStringOrDouble, None)
  //val dflt: Either[Error, Option[Double]] = Members.getValue(this, Constants.DFLT, Members.optionDouble, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)

}

case class PrimitiveInt(override val parent: SchemaPath,
                           override val name: String,
                           override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveInt"

  val min: Either[Error, Option[Int]] = Members.getValue(this, Constants.MIN, Members.optionInt, None)
  val max: Either[Error, Option[Int]] = Members.getValue(this, Constants.MAX, Members.optionInt, None)
  val dflt: Either[Error, Option[Int]] = Members.getValue(this, Constants.DFLT, Members.optionInt, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)

}

case class PrimitiveBoolean(override val parent: SchemaPath,
                           override val name: String,
                           override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveBoolean"

  val dflt: Either[Error, Option[Boolean]] = Members.getValue(this, Constants.DFLT, Members.optionBoolean, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)

}


case class PrimitiveString(override val parent: SchemaPath,
                            override val name: String,
                            override val member: Map[String, OfType] = Map[String, OfPrimitiveOrArray]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveString"

  val dflt: Either[Error, Option[String]] = Members.getValue(this, Constants.DFLT, Members.optionString, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)

}


case class PrimitiveDataArray(override val parent: SchemaPath,
                              override val name: String,
                              override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveDataArray"

  val dflt: Either[Error, Option[Array[Double]]] = Members.getValue(this, Constants.DFLT, Members.optionDoubleArray, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)

}


// TODO: change all member: Map[String, OfType] to member: Map[String, OfPrimitiveOrArray]
case class PrimitiveInfoArray(override val parent: SchemaPath,
                              override val name: String,
                              override val member: Map[String, OfType] = Map[String, OfType](),
                              items: Map[Int, List[OfPrimitive]] = Map[Int, List[OfPrimitive]]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveInfoArray"

  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)
  // items not set-up here because it is not a primitive or array (it is an object with valtType)
  // TODO: can we place the items set-up here?
}

object PrimitiveInfoArray {
  def apply(parent: SchemaPath,
            name: String,
            member: Map[String, OfType]): PrimitiveInfoArray = new PrimitiveInfoArray(parent, name, member)
}


class BaseEnumerate(override val parent: SchemaPath,
                    override val name: String,
                    override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "BaseEnumerate"

  // TODO: all members should be of OfPrimitive, conversion to code must be in the generator only!!
  val dflt: Either[Error, Option[OfType]] = Members.getValue(this, Constants.DFLT, e => Right(Some(e)), None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)
  val values: Either[Error, Option[OfArray]] = Members.getValue(this, Constants.VALUES,
    {
      case e: OfArray => Right(Some(e))
      case e => Left(Error(s"Expected OfPrimitiveOrArray but got $e"))
    }, None)
  val hasSwitch: Boolean = description.fold(_ => false, { s => s.fold(false)(_.toLowerCase.contains(Constants.SWITCH)) })
}

case class PrimitiveEnumerate(override val parent: SchemaPath,
                              override val name: String,
                              override val member: Map[String, OfType] = Map[String, OfType]())
  extends BaseEnumerate(parent, name, member) {

  override  val typeName = "PrimitiveEnumerate"
}

case class PrimitiveRegExEnumerate(override val parent: SchemaPath,
                                  override val name: String,
                                  override val member: Map[String, OfType] = Map[String, OfType]())
  extends BaseEnumerate(parent, name, member) {

  override  val typeName = "PrimitiveRegExEnumerate"
}

case class PrimitiveSubPlotId(override val parent: SchemaPath,
                              override val name: String,
                              override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveSubPlotId"

  val dflt: Either[Error, Option[String]] = Members.getValue(this, Constants.DFLT, Members.optionString, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)
}

case class PrimitiveFlagList(override val parent: SchemaPath,
                              override val name: String,
                              override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveFlagList"

  val dflt: Either[Error, Option[String]] = Members.getValue(this, Constants.DFLT, Members.optionString, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)
  val flagsArr: Either[Error, Option[Array[String]]] = Members.getValue(this, Constants.FLAGS, Members.optionStringArray, None)
  // Hack for Config.ScrollZoom
  //val extrasArr: Either[Error, Option[Array[String]]] = Members.getValue(this, Constants.EXTRAS, Members.optionStringArray, None)
  val extrasArr: Either[Error, Option[Array[String]]] = Members.getValue(this, Constants.EXTRAS, Members.optionStringOrBooleanArray, None)
}

case class PrimitiveAngle(override val parent: SchemaPath,
                             override val name: String,
                             override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveAngle"

  val dflt: Either[Error, Option[String]] = Members.getValue(this, Constants.DFLT, Members.optionStringOrDouble, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)
}

case class PrimitiveAny(override val parent: SchemaPath,
                          override val name: String,
                          override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveAny"

  val dflt: Either[Error, Option[Any]] = Members.getValue(this, Constants.DFLT, Members.optionAny, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)
}

case class PrimitiveColorScale(override val parent: SchemaPath,
                        override val name: String,
                        override val member: Map[String, OfType] = Map[String, OfType](),
                        dflts: Map[Int, (TDouble, TString)] = Map[Int, (TDouble, TString)]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveColorScale"

  val dflt: Either[Error, Option[Array[(Double, String)]]] = {
    if (dflts.nonEmpty) {
      val n = dflts.keys.max
      val idxs = 0 until n
      val dflt = idxs.indices.map(i => (dflts(i)._1.t, dflts(i)._2.t)).toArray
      Right(Some(dflt))
    } else {
      Right(None)
    }
  }
  //Members.getValue(this, Constants.DFLT, Members.optionDouble, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)
}


object PrimitiveColorScale {
  def apply(parent: SchemaPath,
            name: String,
            member: Map[String, OfType]): PrimitiveColorScale = new PrimitiveColorScale(parent, name, member)
}


case class PrimitiveColorList(override val parent: SchemaPath,
                               override val name: String,
                               override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveColorList"

  val dflt: Either[Error, Option[Array[String]]] = Members.getValue(this, Constants.DFLT, Members.optionStringArray, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)

}

case class PrimitiveColor(override val parent: SchemaPath,
                              override val name: String,
                              override val member: Map[String, OfType] = Map[String, OfType]())
  extends PrimitiveDefinition(parent, name, member) {

  override  val typeName = "PrimitiveColor"

  val dflt: Either[Error, Option[String]] = Members.getValue(this, Constants.DFLT, Members.optionString, None)
  val description: Either[Error, Option[String]] = Members.getValue(this, Constants.DESCRIPTION, Members.optionString, None)
}
