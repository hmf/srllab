package exp


import java.util.{Timer, TimerTask}

import javafx.animation.{KeyFrame, Timeline}
import javafx.application.{Application, Platform}
import javafx.concurrent.Worker
import javafx.embed.swing.SwingFXUtils
import javafx.event.{ActionEvent, EventHandler}
import javafx.scene.Scene
import javafx.scene.image.{PixelWriter, WritableImage}
import javafx.scene.layout.VBox
import javafx.scene.web.WebEngine
import javafx.scene.web.WebView
import javafx.stage.Stage
import javafx.util.Duration
import javax.imageio.ImageIO
import org.w3c.dom.{Node, NodeList, Text}
import org.w3c.dom.html.HTMLDivElement
import webkit.Utils


/**
 *
 * mill mill.scalalib.GenIdea/idea
 *
 * mill mdocs.compile
 * mill mdocs.run
 * mill mdocs.run noGraal
 *
 * mill mdocs.{compile, run}
 * mill --watch mdocs.run
 *
 * mill -i mdocs.console
 * mill -i mdocs.repl
 *
 * mill -i mdocs.runMain mdocs.TestWebKitCapture
 * mill --watch mill -i mdocs.runMain mdocs.TestWebKitCapture
 *
 * Options:
 * 1. WebView snapshot
 * 2. WebView DOM get the Java Canvas
 * 3. Javascript save to file
 *
 *
 * TODO: download of the Plotly snapshot not working
 * https://stackoverflow.com/questions/26135589/downloads-with-javafx-webview
 * https://bugs.openjdk.java.net/browse/JDK-8090318
 *
 * https://github.com/vsch/Javafx-WebView-Debugger
 *
 * https://stackoverflow.com/questions/19420753/how-to-call-a-javascript-function-from-a-javafx-webview-on-button-click
 *
 * https://stackoverflow.com/questions/56166267/how-do-i-get-java-fx-running-with-openjdk-8-on-ubuntu-18-04-2-lts
 * https://github.com/JabRef/user-documentation/issues/204
 * https://stackoverflow.com/questions/53319350/error-could-not-find-or-load-main-class-application-main-caused-by-java-lang-n/57946745#57946745
 * sudo apt install openjfx
 * dpkg-query -L openjfx
 * java --module-path /usr/share/openjfx/lib --add-modules=javafx.controls,javafx.fxml,javafx.base,javafx.media,javafx.web,javafx.swing -jar '/home/lotfi/Documents/MyAppfolder/my_application.jar'
 */
class TestWebKitCapture extends Application {

  var forceClose = false

  /**
   * Simple test of launching teh WebView, loading a URL and rendering it via
   * a visible JavaFX/OpenFX application.
   *
   * @param primaryStage JavaFX root stage
   * @param source URL to load. Can be a URL (HTTP://) or a local file (file://).
   */
  def test1(primaryStage: Stage, source: String): Unit = {
    primaryStage.setTitle("JavaFX WebView Test 1")

    val webView = new WebView()
    val webEngine = webView.getEngine

    //webEngine.load("http://google.com")
    //webEngine.load("""http:/google.com""")
    //webEngine.loadContent(content, "text/html")
    webEngine.load(source)

    val vBox = new VBox(webView)
    val scene = new Scene(vBox, 960, 600)
    primaryStage.setScene(scene)
    primaryStage.show()
  }

  def printText(node: Node): Unit = {
    val text = node.asInstanceOf[Text]
    println(s"text = $text")
    println(s"text.getNodeName = ${text.getNodeName}")
    println(s"text.getNodeType = ${text.getNodeType}")
    println(s"text.getNodeValue = '${text.getNodeValue}'")
    println(s"text.getWholeText = '${text.getWholeText}'")
  }

  def printNode(node:Node): Unit = {
    //println(s"node.getBaseURI = ${node.getBaseURI}")
    println(s"node.getNodeName = ${node.getNodeName}")
    println(s"node.getNodeType = ${node.getNodeType}")
    println(s"node.getNodeValue = '${node.getNodeValue}'")
    node match {
      case _:HTMLDivElement =>
        val div = node.asInstanceOf[HTMLDivElement]
        println(s"div.getClassName = ${div.getClassName}")
      case _ =>
        ()
    }
  }

  def printNodeList(nodeList: NodeList): Unit = {
    println(s"children.getLength = ${nodeList.getLength}")
    val idxs = 0 until nodeList.getLength
    idxs.foreach{i =>
      val n = nodeList.item(i)
      println(n)
      printNode(n)
    }
  }

  /**
   * Simple test to get the nodes of the document. If we have access to the
   * Canvas node we could try to render and take a snapshot of it. The problem
   * is that we can get the DOM node (W3C). An attempt was made to get the
   * underlying JavaFX/OpenFX Node children. However we only have the
   * `webView.getChildrenUnmodifiable` that seems to have only one child. And
   * this child seems to be a scroll bar. How do we access the Canvas Node?
   *
   * @param primaryStage JavaFX root stage
   * @param source URL to load. Can be a URL (HTTP://) or a local file (file://).
   */
  def test2(primaryStage: Stage, source: String): Unit = {
    primaryStage.setTitle("JavaFX WebView Test 2")

    val webView = new WebView()
    val webEngine = webView.getEngine

    //webEngine.load("""http:/google.com""")
    //webEngine.loadContent(content, "text/html")
    webEngine.load(source)

    val vBox = new VBox(webView)
    val scene = new Scene(vBox, 960, 600)
    primaryStage.setScene(scene)
    primaryStage.show()

    webEngine.getLoadWorker.stateProperty().addListener((observable, oldState, newState) => {
      if (newState == Worker.State.SUCCEEDED) {
        println(s"newState = $newState")
        val doc = webEngine.getDocument
        println(s"doc = $doc")
        val docElement = doc.getDocumentElement
        println(s"docElement = $docElement")
        val plotElement = doc.getElementById("myDiv")
        println(s"plotElement = $plotElement")
        val plotDiv = plotElement.asInstanceOf[HTMLDivElement]
        println(s"plotDiv = $plotElement")
        val node: Node = plotDiv.getFirstChild
        println(s"node = $node")
        //printText(node)
        val children: NodeList = plotDiv.getChildNodes
        printNodeList(children)

        /*
        @Override protected ObservableList<Node> getChildren() {
          return super.getChildren();
        }*/
        val t = webView.getChildrenUnmodifiable.toArray
        println(t.mkString(","))
        println()

      } else if (newState == Worker.State.FAILED) {
        println(s"newState = $newState")

      } else
        println(s"newState = $newState")
    });
  }

  /**
   * This method takes a snapshot image an saves it to a file. JavaFX/OpenFX
   * supports the following output formats: JPEG, PNG, GIF, BMP and WBMP
   * Additional formats may be available as plugins.
   *
   * @param image an image containing a snapshot
   * @return true if an image was successfully saved otherwise false
   */
  def saveSnapshot(image:WritableImage): Boolean = {
    // Only as PNG
    val property = "java.io.tmpdir"
    val tempDir = System.getProperty(property)
    // JPEG, PNG, GIF, BMP and WBMP
    val file1 = Utils.temporaryFile(tempDir, "_testwebkit_1.png").toJava
    val r1 = ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file1)
    println(s"$file1: $r1")

    // https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/imageio/package-summary.html
    // No JPEG
    // https://bugs.openjdk.java.net/browse/JDK-8204188
    // https://docs.oracle.com/en/java/javase/11/docs/api/java.desktop/javax/imageio/package-use.html
    val file2 = Utils.temporaryFile(tempDir, "_testwebkit_1.tiff").toJava
    val r2 = ImageIO.write(SwingFXUtils.fromFXImage(image, null), "tiff", file2)
    println(s"$file2: $r2")

    val file3 = Utils.temporaryFile(tempDir, "_testwebkit_1.gif").toJava
    val r3 = ImageIO.write(SwingFXUtils.fromFXImage(image, null), "gif", file3)
    println(s"$file3: $r3")

    // BMP and WBMP do not work
    //val file4 = Utils.temporaryFile(tempDir, "_testwebkit_1.bmp").toJava
    //val r4 = ImageIO.write(SwingFXUtils.fromFXImage(image, null), "bmp", file4)
    //val file4 = Utils.temporaryFile(tempDir, "_testwebkit_1.wbmp").toJava
    //val r4 = ImageIO.write(SwingFXUtils.fromFXImage(image, null), "wbmp", file4)
    //println(s"$file4: $r4")

    //r1 && r2 && r3 && r4
    r1 && r2 && r3
  }

  /**
   * Take a snapshot of the WebView that was placed into the Stage. We need to
   * take the snapshot after a small delay. If not WebView will not have been
   * rendered and we get a white (blank) image.
   *
   * NOTE 1: when we use `java.Timer` we need to use `Platform.runLater`. If
   * not, we will get an exception that another thread i executing outside the
   * JavaFX thread.
   *
   * NOTE 2: If we use a timer, we in essence create a thread. This thread
   * stays alive by default. This means that the JavaFX will not close even if
   * explicitly call `Platform.exit()`. Implicitly the `primaryStage.close()`
   * call will trigger the `Application.stop()`. We need to terminate the timer
   * with a call to `timer.cancel()`. In such a case if we only have one stage
   * then `Platform.exit()` need not be called - the app will be implicitly
   * terminate.
   *
   * @see https://stackoverflow.com/questions/26916640/javafx-not-on-fx-application-thread-when-using-timer
   *
   * @param primaryStage JavaFX root stage
   * @param webView OpenFX WebView (based on WebKit)
   */
  def takeDelayedSnapshotTimer(primaryStage: Stage, webView: WebView, delay: Long): Unit = {
      val timer = new Timer()
      timer.schedule(new TimerTask {
        override def run(): Unit = {
          Platform.runLater{ () =>
            val image  = webView.snapshot(null, null)
            // This will be the scene size
            println(s"image.getWidth = ${image.getWidth}")
            println(s"image.getHeight = ${image.getHeight}")
            println("Taking snapshot")
            saveSnapshot(image)
            println("Closing")
            timer.cancel()
            primaryStage.close()
          }
        }
      }, delay)
  }

  /**
   * Take a snapshot of the WebView that was placed into the Stage. We need to
   * take the snapshot after a small delay. If not WebView will not have been
   * rendered and we get a white (blank) image.
   *
   * NOTE 1: when we use `java.Timer` we need to use `Platform.runLater`. If
   * not, we will get an exception that another thread i executing outside the
   * JavaFX thread.
   *
   * NOTE 2: If we use a timer, we in essence create a thread. This thread
   * stays alive by default. This means that the JavaFX will not close even if
   * explicitly call `Platform.exit()`. Implicitly the `primaryStage.close()`
   * call will trigger the `Application.stop()`. We need to terminate the timer
   * with a call to `timer.cancel()`. In such a case if we only have one stage
   * then `Platform.exit()` need not be called - the app will be implicitly
   * terminate.
   *
   * @see https://stackoverflow.com/questions/26916640/javafx-not-on-fx-application-thread-when-using-timer
   *
   * @param primaryStage JavaFX root stage
   * @param webView OpenFX WebView (based on WebKit)
   */
  def takeDelayedSnapshotTimeLine(primaryStage: Stage, webView: WebView, delay: Long): Unit = {

    new Timeline(
      new KeyFrame(
        Duration.millis(delay),
        (ae: ActionEvent) => {
          println(ae)
          val image  = webView.snapshot(null, null)
          // This will be the scene size
          println(s"image.getWidth = ${image.getWidth}")
          println(s"image.getHeight = ${image.getHeight}")
          println("Taking snapshot")
          saveSnapshot(image)
          println("Closing")
          primaryStage.close()
        } )
     ).play();
  }

  /**
   * This test opens up a WebView, shows it to force rendering, waits via a
   * timer for rendering to finish and the takes a snapshot of the browser,
   * which is then saved to a file. Snapshot delay is done using `java.Timer`
   * via [[takeDelayedSnapshotTimer]]
   *
   * @see https://stackoverflow.com/questions/19785130/javafx-event-when-objects-are-rendered
   *      https://stackoverflow.com/questions/47211852/using-filechooser-to-save-a-writableimage-image
   *      http://www.java2s.com/Tutorials/Java/JavaFX_How_to/Image/Save_an_Image_to_a_file.htm
   *      https://community.oracle.com/thread/2450090?start=0&tstart=0
   *      https://bugs.openjdk.java.net/browse/JDK-8087569 <-- need to wait. Pulses not available
   *      https://gist.github.com/jewelsea/5632958
   *      https://www.javatips.net/api/URL-pad-master/src/main/java/fetcher/model/Snapshotter.java
   *      https://stackoverflow.com/questions/9966136/javafx-periodic-background-task
   *      https://stackoverflow.com/questions/35512648/adding-a-timer-to-my-program-javafx  <----
   *      PauseTransition
   *      javafx.animation.Timeline
   *      javafx.concurrent.ScheduledService
   *      Headless
   *      https://wiki.openjdk.java.net/display/OpenJFX/Monocle
   *      https://stackoverflow.com/questions/30121204/javafx-how-to-create-snapshot-screenshot-of-invisble-webview <--
   *      https://gist.github.com/danialfarid/2ddbab04803ae4fd2dca <---- monocle use
   *
   * @param primaryStage JavaFX root stage
   * @param source URL to load. Can be a URL (HTTP://) or a local file (file://).
   */
  def test3(primaryStage: Stage, source: String): Unit = {
    primaryStage.setTitle("JavaFX WebView Test 3")

    // By default the app closes when the last Stage closes
    Platform.setImplicitExit(true)

    val webView = new WebView()
    val webEngine = webView.getEngine

    webEngine.load(source)
    //webEngine.loadContent(content, "text/html")

    // We get this event **before** the web engine's loader has finished
    // So rendering has not completed. If we attempt a snapshot here we
    // get a blank (white) image.
    primaryStage.setOnShown{winEvent =>
      println("onShown")
      //val image:WritableImage  = webView.snapshot(null, null)
      //saveSnapshot(image)
    }

    val vBox = new VBox(webView)
    // This is the size of the image
    val scene = new Scene(vBox, 960, 600)
    primaryStage.setScene(scene)
    // We need to show in order to ensure rendering
    primaryStage.show()

    webEngine.getLoadWorker.stateProperty().addListener((observable, oldState, newState) => {
      if (newState == Worker.State.SUCCEEDED) {
        println(s"newState = $newState")
        println(s"webView.getChildrenUnmodifiable.size() = ${webView.getChildrenUnmodifiable.size()}")

        // Snapshot here will not work.
        /*
        val image:WritableImage  = webView.snapshot(null, null)
        // This will be the scene size
        println(s"image.getWidth = ${image.getWidth}")
        println(s"image.getHeight = ${image.getHeight}")
        saveSnapshot(image)
        */
        // We need to delay this till after rendering has occurred
        // No event seems to exist for this. We can force rendering
        // via pulses, but the API is protected/private.
        takeDelayedSnapshotTimer(primaryStage, webView, 10)

        // Mill eats up a line
        println()
        // Need to close
        //primaryStage.close()
        //System.exit(0)

      } else if (newState == Worker.State.FAILED) {
        println(s"newState = $newState")
        println()

      } else
        println(s"newState = $newState")
        println()
    })

  }

  /**
   * This test opens up a WebView, shows it to force rendering, waits via a
   * timer for rendering to finish and the takes a snapshot of the browser,
   * which is then saved to a file. Snapshot delay is done using `java.Timer`
   * via [[takeDelayedSnapshotTimeLine]]
   *
   * @param primaryStage JavaFX root stage
   * @param source URL to load. Can be a URL (HTTP://) or a local file (file://).
   */
  def test4(primaryStage: Stage, source: String): Unit = {
    primaryStage.setTitle("JavaFX WebView Test 4")

    val webView = new WebView()
    val webEngine = webView.getEngine

    webEngine.load(source)
    //webEngine.loadContent(content, "text/html")

    val vBox = new VBox(webView)
    // This is the size of the image
    val scene = new Scene(vBox, 960, 600)
    primaryStage.setScene(scene)
    // We need to show in order to ensure rendering
    primaryStage.show()

    webEngine.getLoadWorker.stateProperty().addListener((observable, oldState, newState) => {
      if (newState == Worker.State.SUCCEEDED) {
        println(s"newState = $newState")
        println(s"webView.getChildrenUnmodifiable.size() = ${webView.getChildrenUnmodifiable.size()}")

        // We need to delay this till after rendering has occurred
        // No event seems to exist for this. We can force rendering
        // via pulses, but the API is protected/private.
        takeDelayedSnapshotTimeLine(primaryStage, webView, 10)

        // Mill eats up a line
        println()
        // Need to close but only after delay
        //primaryStage.close()
        //System.exit(0)

      } else if (newState == Worker.State.FAILED) {
        println(s"newState = $newState")
        println()

      } else
        println(s"newState = $newState")
      println()
    })

  }


  override def start(primaryStage: Stage): Unit = {
    // The save image button uses the web plotAPI.
    //test1(primaryStage, """file:///home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test.html""")
    //test2(primaryStage, """file:///home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test.html""")
    // Uses web plotAPI to get server rendered image
    //test2(primaryStage, """file:///home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test2.html""")
    //test3(primaryStage, """file:///home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test.html""")
    //test3(primaryStage, """file:///home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test2.html""")
    test4(primaryStage, """file:///home/hmf/IdeaProjects/srllab/mdocs/src/mdocs/test.html""")
  }

  override def stop(): Unit = {
    println("Stop requested")
    super.stop()
    // Does not work
    //Platform.exit()
    //if (forceClose) System.exit(0)
  }
}


object TestWebKitCapture {

  def main(args: Array[String]): Unit = {
    println("Main 0")
    Application.launch(classOf[TestWebKitCapture], args: _*)
    // Below initializes IDE twice
    //val fx = new TestWebKit()
    //fx.launch(args:_*)
  }
}
