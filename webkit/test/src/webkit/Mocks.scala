package webkit

import better.files._
import better.files.Dsl._

object Mocks {

  val baseMockDir: File = cwd / "webkit" / "test" / "image" / "mocks"
  val baseDir: File = cwd / "webkit" / "test" / "image" / "baselines"

  case class TestFiles(fileName: String) {
    val htmlFile: File = baseMockDir / s"$fileName.html"
    val pngFile = s"$fileName.png"
    val gifFile = s"$fileName.gif"
    val tifFile = s"$fileName.tif"
    val jpegFile = s"$fileName.jpeg"
    val bmpFile = s"$fileName.bmp"
    val wbmpFile = s"$fileName.wbmp"
    val pngFileTmp: File = Utils.temporaryDirFile(pngFile)
    val gifFileTmp: File = Utils.temporaryDirFile(gifFile)
    val tifFileTmp: File = Utils.temporaryDirFile(tifFile)
    val jpegFileTmp: File = Utils.temporaryDirFile(jpegFile)
    val bmpFileTmp: File = Utils.temporaryDirFile(bmpFile)
    val wbmpFileTmp: File = Utils.temporaryDirFile(wbmpFile)
    val goldPng: File =  baseDir / pngFile
    val goldGif: File =  baseDir / gifFile
    val goldTif: File =  baseDir / tifFile
    val goldJpeg: File =  baseDir / jpegFile
    val goldBmp: File =  baseDir / bmpFile
    val goldWbmp: File =  baseDir / wbmpFile
  }

  object SVGcolor {
    val files: TestFiles = TestFiles("test1")
  }

  object SVG_BW {
    val files: TestFiles = TestFiles("test2")
  }

  object SVG_Plot {
    val files: TestFiles = TestFiles("test3")
  }
}
