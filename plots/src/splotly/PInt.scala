package splotly

import ujson.Value

case class PInt(min: Option[Int],
                max: Option[Int],
                dflt: Option[Int],
                description: Option[String]
                ) extends JSON {

  private var value: Option[Int] = None

  def setValue(double: Int): Unit = value = Some(double)
  def setDefault(): Unit = value = dflt
  def getValue: Option[Int] = value

  override def json(): String = value.fold("")(e => s"""${e.toString}""")

  override def fromJson(in: Value): PInt.this.type =  {
    in match {
      case ujson.Num(v) =>
        if (v.isValidInt)
          setValue(v.toInt)
        else
          throw new RuntimeException(s"${getClass.getCanonicalName}: value is not a valid integer $in")
      case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
    }
    this
  }
}
