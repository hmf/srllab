package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Transforms() extends Layouts {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/

  case object filter extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/

    case object attributes extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val editType: String = """calc"""

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val preservegaps: PBool = PBool(
        dflt = Some(false),
        description = Some(
          """Determines whether or not gaps in data arrays produced by the filter operation are preserved. Setting this to *true* might be useful when plotting a line chart with `connectgaps` set to *false*."""
        )
      )

      object targetcalendar extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """gregorian"""
        val description: Option[String] = Some(
          """Sets the calendar system to use for `target`, if it is an array of dates. If `target` is a string (eg *x*) we use the corresponding trace attribute (eg `xcalendar`) if it exists, even if `targetcalendar` is provided."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def ummalqura(): Unit = { selected = TString("ummalqura") }

        def thai(): Unit = { selected = TString("thai") }

        def taiwan(): Unit = { selected = TString("taiwan") }

        def jalali(): Unit = { selected = TString("jalali") }

        def persian(): Unit = { selected = TString("persian") }

        def nepali(): Unit = { selected = TString("nepali") }

        def nanakshahi(): Unit = { selected = TString("nanakshahi") }

        def mayan(): Unit = { selected = TString("mayan") }

        def julian(): Unit = { selected = TString("julian") }

        def islamic(): Unit = { selected = TString("islamic") }

        def hebrew(): Unit = { selected = TString("hebrew") }

        def ethiopian(): Unit = { selected = TString("ethiopian") }

        def discworld(): Unit = { selected = TString("discworld") }

        def coptic(): Unit = { selected = TString("coptic") }

        def chinese(): Unit = { selected = TString("chinese") }

        def gregorian(): Unit = { selected = TString("gregorian") }

      }

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val enabled: PBool = PBool(
        dflt = Some(true),
        description = Some(
          """Determines whether this filter transform is enabled or disabled."""
        )
      )

      object valuecalendar extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """gregorian"""
        val description: Option[String] = Some(
          """Sets the calendar system to use for `value`, if it is a date."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def ummalqura(): Unit = { selected = TString("ummalqura") }

        def thai(): Unit = { selected = TString("thai") }

        def taiwan(): Unit = { selected = TString("taiwan") }

        def jalali(): Unit = { selected = TString("jalali") }

        def persian(): Unit = { selected = TString("persian") }

        def nepali(): Unit = { selected = TString("nepali") }

        def nanakshahi(): Unit = { selected = TString("nanakshahi") }

        def mayan(): Unit = { selected = TString("mayan") }

        def julian(): Unit = { selected = TString("julian") }

        def islamic(): Unit = { selected = TString("islamic") }

        def hebrew(): Unit = { selected = TString("hebrew") }

        def ethiopian(): Unit = { selected = TString("ethiopian") }

        def discworld(): Unit = { selected = TString("discworld") }

        def coptic(): Unit = { selected = TString("coptic") }

        def chinese(): Unit = { selected = TString("chinese") }

        def gregorian(): Unit = { selected = TString("gregorian") }

      }

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val targetsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  target .""")
      )

      object operation extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """="""
        val description: Option[String] = Some(
          """Sets the filter operation. *=* keeps items equal to `value` *!=* keeps items not equal to `value` *<* keeps items less than `value` *<=* keeps items less than or equal to `value` *>* keeps items greater than `value` *>=* keeps items greater than or equal to `value` *[]* keeps items inside `value[0]` to `value[1]` including both bounds *()* keeps items inside `value[0]` to `value[1]` excluding both bounds *[)* keeps items inside `value[0]` to `value[1]` including `value[0]` but excluding `value[1] *(]* keeps items inside `value[0]` to `value[1]` excluding `value[0]` but including `value[1] *][* keeps items outside `value[0]` to `value[1]` and equal to both bounds *)(* keeps items outside `value[0]` to `value[1]` *](* keeps items outside `value[0]` to `value[1]` and equal to `value[0]` *)[* keeps items outside `value[0]` to `value[1]` and equal to `value[1]` *{}* keeps items present in a set of values *}{* keeps items not present in a set of values"""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def `}{`(): Unit = { selected = TString("}{") }

        def `{}`(): Unit = { selected = TString("{}") }

        def `)[`(): Unit = { selected = TString(")[") }

        def `](`(): Unit = { selected = TString("](") }

        def `)(`(): Unit = { selected = TString(")(") }

        def `][`(): Unit = { selected = TString("][") }

        def `(]`(): Unit = { selected = TString("(]") }

        def `[)`(): Unit = { selected = TString("[)") }

        def `()`(): Unit = { selected = TString("()") }

        def `[]`(): Unit = { selected = TString("[]") }

        def <=(): Unit = { selected = TString("<=") }

        def >(): Unit = { selected = TString(">") }

        def >=(): Unit = { selected = TString(">=") }

        def <(): Unit = { selected = TString("<") }

        def !=(): Unit = { selected = TString("!=") }

        def `=`(): Unit = { selected = TString("=") }

      }

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val target: PChars = PChars(
        dflt = Some("""x"""),
        description = Some(
          """Sets the filter target by which the filter is applied. If a string, `target` is assumed to be a reference to a data array in the parent trace object. To filter about nested variables, use *.* to access them. For example, set `target` to *marker.color* to filter about the marker color array. If an array, `target` is then the data array by which the filter is applied."""
        )
      )

      /* §#
       * generateAny(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val value: PAny = PAny(
        dflt = Some("""0.0"""),
        description = Some(
          """Sets the value or values by which to filter. Values are expected to be in the same type as the data linked to `target`. When `operation` is set to one of the comparison values (=,!=,<,>=,>,<=) `value` is expected to be a number or a string. When `operation` is set to one of the interval values ([],(),[),(],][,)(,](,)[) `value` is expected to be 2-item array where the first item is the lower bound and the second item is the upper bound. When `operation`, is set to one of the set values ({},}{) `value` is expected to be an array with as many items as the desired set elements."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (preservegaps.json().length > 0)
            "\"preservegaps\"" + ":" + preservegaps.json()
          else ""
        val v1 =
          if (targetcalendar.json().length > 0)
            "\"targetcalendar\"" + ":" + targetcalendar.json()
          else ""
        val v2 =
          if (enabled.json().length > 0) "\"enabled\"" + ":" + enabled.json()
          else ""
        val v3 =
          if (valuecalendar.json().length > 0)
            "\"valuecalendar\"" + ":" + valuecalendar.json()
          else ""
        val v4 =
          if (targetsrc.json().length > 0)
            "\"targetsrc\"" + ":" + targetsrc.json()
          else ""
        val v5 =
          if (operation.json().length > 0)
            "\"operation\"" + ":" + operation.json()
          else ""
        val v6 =
          if (target.json().length > 0) "\"target\"" + ":" + target.json()
          else ""
        val v7 =
          if (value.json().length > 0) "\"value\"" + ":" + value.json() else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6, v7)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("preservegaps")
          in0.map(ve => preservegaps.fromJson(ve))

          val in1 = mapKV.get("targetcalendar")
          in1.map(ve => targetcalendar.fromJson(ve))

          val in2 = mapKV.get("enabled")
          in2.map(ve => enabled.fromJson(ve))

          val in3 = mapKV.get("valuecalendar")
          in3.map(ve => valuecalendar.fromJson(ve))

          val in4 = mapKV.get("targetsrc")
          in4.map(ve => targetsrc.fromJson(ve))

          val in5 = mapKV.get("operation")
          in5.map(ve => operation.fromJson(ve))

          val in6 = mapKV.get("target")
          in6.map(ve => target.fromJson(ve))

          val in7 = mapKV.get("value")
          in7.map(ve => value.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (attributes.json().length > 0)
          "\"attributes\"" + ":" + attributes.json()
        else ""
      val vars: List[String] = List(v0)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("attributes")
        in0.map(ve => attributes.fromJson(ve))

      }
      this
    }

  }

  case object aggregate extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/

    case object attributes extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val editType: String = """calc"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val groups: PChars = PChars(
        dflt = Some("""x"""),
        description = Some(
          """Sets the grouping target to which the aggregation is applied. Data points with matching group values will be coalesced into one point, using the supplied aggregation functions to reduce data in other data arrays. If a string, `groups` is assumed to be a reference to a data array in the parent trace object. To aggregate by nested variables, use *.* to access them. For example, set `groups` to *marker.color* to aggregate about the marker color array. If an array, `groups` is itself the data array by which we aggregate."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val groupssrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  groups .""")
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val enabled: PBool = PBool(
        dflt = Some(true),
        description = Some(
          """Determines whether this aggregate transform is enabled or disabled."""
        )
      )

      case object aggregations extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String = """object"""

        case object items extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          case object aggregation extends JSON {
            /* §#
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val role: String     = """object"""
            val editType: String = """calc"""

            object func extends JSON {
              /* §#
               * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val dflt: String = """first"""
              val description: Option[String] = Some(
                """Sets the aggregation function. All values from the linked `target`, corresponding to the same value in the `groups` array, are collected and reduced by this function. *count* is simply the number of values in the `groups` array, so does not even require the linked array to exist. *first* (*last*) is just the first (last) linked value. Invalid values are ignored, so for example in *avg* they do not contribute to either the numerator or the denominator. Any data type (numeric, date, category) may be aggregated with any function, even though in certain cases it is unlikely to make sense, for example a sum of dates or average of categories. *median* will return the average of the two central values if there is an even count. *mode* will return the first value to reach the maximum count, in case of a tie. *change* will return the difference between the first and last linked values. *range* will return the difference between the min and max linked values."""
              )

              private var selected: OfPrimitive = NAPrimitive
              //allDeclarations

              def json(): String = {
                /* §#
                 * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
                 * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                selected match {
                  case NAPrimitive => ""
                  case TString(t)  => "\"" + t.toString + "\""
                  case TBoolean(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TBoolean(false)"""
                    )
                  case TDouble(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TDouble(0.0)"""
                    )
                  case TInt(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TInt(0)"""
                    )
                  case TImpliedEdits(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TImpliedEdits()"""
                    )
                  case TAttrRegexps(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TAttrRegexps()"""
                    )
                }
              }

              def fromJson(in: Value): this.type = {
                /* §#
                 * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
                 * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                def err(j: ujson.Value): OfPrimitive =
                  throw new RuntimeException(s"""Parsing of $j failed: $in""")
                val read = in match {
                  case Str(s)  => TString(s)
                  case Bool(s) => err(in)
                  case Num(s)  => err(in)
                  case _       => err(in)
                }
                selected = read
                this
              }

              def range(): Unit = { selected = TString("range") }

              def change(): Unit = { selected = TString("change") }

              def last(): Unit = { selected = TString("last") }

              def first(): Unit = { selected = TString("first") }

              def max(): Unit = { selected = TString("max") }

              def min(): Unit = { selected = TString("min") }

              def stddev(): Unit = { selected = TString("stddev") }

              def rms(): Unit = { selected = TString("rms") }

              def mode(): Unit = { selected = TString("mode") }

              def median(): Unit = { selected = TString("median") }

              def avg(): Unit = { selected = TString("avg") }

              def sum(): Unit = { selected = TString("sum") }

              def count(): Unit = { selected = TString("count") }

            }

            /* §#
             * generateStringDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val target: PChars = PChars(
              dflt = None,
              description = Some(
                """A reference to the data array in the parent trace to aggregate. To aggregate by nested variables, use *.* to access them. For example, set `groups` to *marker.color* to aggregate over the marker color array. The referenced array must already exist, unless `func` is *count*, and each array may only be referenced once."""
              )
            )

            object funcmode extends JSON {
              /* §#
               * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val dflt: String = """sample"""
              val description: Option[String] = Some(
                """*stddev* supports two formula variants: *sample* (normalize by N-1) and *population* (normalize by N)."""
              )

              private var selected: OfPrimitive = NAPrimitive
              //allDeclarations

              def json(): String = {
                /* §#
                 * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
                 * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                selected match {
                  case NAPrimitive => ""
                  case TString(t)  => "\"" + t.toString + "\""
                  case TBoolean(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TBoolean(false)"""
                    )
                  case TDouble(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TDouble(0.0)"""
                    )
                  case TInt(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TInt(0)"""
                    )
                  case TImpliedEdits(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TImpliedEdits()"""
                    )
                  case TAttrRegexps(t) =>
                    throw new RuntimeException(
                      """Unexpected OfPrimitive type: TAttrRegexps()"""
                    )
                }
              }

              def fromJson(in: Value): this.type = {
                /* §#
                 * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
                 * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                def err(j: ujson.Value): OfPrimitive =
                  throw new RuntimeException(s"""Parsing of $j failed: $in""")
                val read = in match {
                  case Str(s)  => TString(s)
                  case Bool(s) => err(in)
                  case Num(s)  => err(in)
                  case _       => err(in)
                }
                selected = read
                this
              }

              def population(): Unit = { selected = TString("population") }

              def sample(): Unit = { selected = TString("sample") }

            }

            /* §#
             * generateBooleanDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val enabled: PBool = PBool(
              dflt = Some(true),
              description = Some(
                """Determines whether this aggregation function is enabled or disabled."""
              )
            )

            def json(): String = {
              /* §#
               * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/

              val v0 =
                if (func.json().length > 0) "\"func\"" + ":" + func.json()
                else ""
              val v1 =
                if (target.json().length > 0) "\"target\"" + ":" + target.json()
                else ""
              val v2 =
                if (funcmode.json().length > 0)
                  "\"funcmode\"" + ":" + funcmode.json()
                else ""
              val v3 =
                if (enabled.json().length > 0)
                  "\"enabled\"" + ":" + enabled.json()
                else ""
              val vars: List[String] = List(v0, v1, v2, v3)
              val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
              if (cleanVars.length > 0) {
                // values set
                "{" + cleanVars + "}"
              } else {
                // no values set
                ""
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err =
                throw new RuntimeException(s"Expected an Obj but got $in")
              in.objOpt.fold(err) { mapKV =>
                val in0 = mapKV.get("func")
                in0.map(ve => func.fromJson(ve))

                val in1 = mapKV.get("target")
                in1.map(ve => target.fromJson(ve))

                val in2 = mapKV.get("funcmode")
                in2.map(ve => funcmode.fromJson(ve))

                val in3 = mapKV.get("enabled")
                in3.map(ve => enabled.fromJson(ve))

              }
              this
            }

          }

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (aggregation.json().length > 0)
                "\"aggregation\"" + ":" + aggregation.json()
              else ""
            val vars: List[String] = List(v0)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("aggregation")
              in0.map(ve => aggregation.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (items.json().length > 0) "\"items\"" + ":" + items.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("items")
            in0.map(ve => items.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (aggregations.json().length > 0)
            "\"aggregations\"" + ":" + aggregations.json()
          else ""
        val v1 =
          if (groups.json().length > 0) "\"groups\"" + ":" + groups.json()
          else ""
        val v2 =
          if (groupssrc.json().length > 0)
            "\"groupssrc\"" + ":" + groupssrc.json()
          else ""
        val v3 =
          if (enabled.json().length > 0) "\"enabled\"" + ":" + enabled.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("aggregations")
          in0.map(ve => aggregations.fromJson(ve))

          val in1 = mapKV.get("groups")
          in1.map(ve => groups.fromJson(ve))

          val in2 = mapKV.get("groupssrc")
          in2.map(ve => groupssrc.fromJson(ve))

          val in3 = mapKV.get("enabled")
          in3.map(ve => enabled.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (attributes.json().length > 0)
          "\"attributes\"" + ":" + attributes.json()
        else ""
      val vars: List[String] = List(v0)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("attributes")
        in0.map(ve => attributes.fromJson(ve))

      }
      this
    }

  }

  case object sort extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/

    case object attributes extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val editType: String = """calc"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val targetsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  target .""")
      )

      object order extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """ascending"""
        val description: Option[String] = Some(
          """Sets the sort transform order."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def descending(): Unit = { selected = TString("descending") }

        def ascending(): Unit = { selected = TString("ascending") }

      }

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val target: PChars = PChars(
        dflt = Some("""x"""),
        description = Some(
          """Sets the target by which the sort transform is applied. If a string, *target* is assumed to be a reference to a data array in the parent trace object. To sort about nested variables, use *.* to access them. For example, set `target` to *marker.size* to sort about the marker size array. If an array, *target* is then the data array by which the sort transform is applied."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val enabled: PBool = PBool(
        dflt = Some(true),
        description = Some(
          """Determines whether this sort transform is enabled or disabled."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (targetsrc.json().length > 0)
            "\"targetsrc\"" + ":" + targetsrc.json()
          else ""
        val v1 =
          if (order.json().length > 0) "\"order\"" + ":" + order.json() else ""
        val v2 =
          if (target.json().length > 0) "\"target\"" + ":" + target.json()
          else ""
        val v3 =
          if (enabled.json().length > 0) "\"enabled\"" + ":" + enabled.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("targetsrc")
          in0.map(ve => targetsrc.fromJson(ve))

          val in1 = mapKV.get("order")
          in1.map(ve => order.fromJson(ve))

          val in2 = mapKV.get("target")
          in2.map(ve => target.fromJson(ve))

          val in3 = mapKV.get("enabled")
          in3.map(ve => enabled.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (attributes.json().length > 0)
          "\"attributes\"" + ":" + attributes.json()
        else ""
      val vars: List[String] = List(v0)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("attributes")
        in0.map(ve => attributes.fromJson(ve))

      }
      this
    }

  }

  case object groupby extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/

    case object attributes extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val editType: String = """calc"""

      /* §#
       * generateDataArrayDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val groups: DataArray = DataArray(
        dflt = Some(Array()),
        description = Some(
          """Sets the groups in which the trace data will be split. For example, with `x` set to *[1, 2, 3, 4]* and `groups` set to *['a', 'b', 'a', 'b']*, the groupby transform with split in one trace with `x` [1, 3] and one trace with `x` [2, 4]."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val groupssrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  groups .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val nameformat: PChars = PChars(
        dflt = None,
        description = Some(
          """Pattern by which grouped traces are named. If only one trace is present, defaults to the group name (`"%{group}"`), otherwise defaults to the group name with trace name (`"%{group} (%{trace})"`). Available escape sequences are `%{group}`, which inserts the group name, and `%{trace}`, which inserts the trace name. If grouping GDP data by country when more than one trace is present, for example, the default "%{group} (%{trace})" would return "Monaco (GDP per capita)"."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val enabled: PBool = PBool(
        dflt = Some(true),
        description = Some(
          """Determines whether this group-by transform is enabled or disabled."""
        )
      )

      case object styles extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String = """object"""

        case object items extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          case object style extends JSON {
            /* §#
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val role: String     = """object"""
            val editType: String = """calc"""

            /* §#
             * generateStringDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val target: PChars = PChars(
              dflt = None,
              description =
                Some("""The group value which receives these styles.""")
            )

            /* §#
             * generateAny(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val value: PAny = PAny(
              dflt = None,
              description = Some(
                """Sets each group styles. For example, with `groups` set to *['a', 'b', 'a', 'b']* and `styles` set to *[{target: 'a', value: { marker: { color: 'red' } }}] marker points in group *'a'* will be drawn in red."""
              )
            )

            def json(): String = {
              /* §#
               * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/

              val v0 =
                if (target.json().length > 0) "\"target\"" + ":" + target.json()
                else ""
              val v1 =
                if (value.json().length > 0) "\"value\"" + ":" + value.json()
                else ""
              val vars: List[String] = List(v0, v1)
              val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
              if (cleanVars.length > 0) {
                // values set
                "{" + cleanVars + "}"
              } else {
                // no values set
                ""
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err =
                throw new RuntimeException(s"Expected an Obj but got $in")
              in.objOpt.fold(err) { mapKV =>
                val in0 = mapKV.get("target")
                in0.map(ve => target.fromJson(ve))

                val in1 = mapKV.get("value")
                in1.map(ve => value.fromJson(ve))

              }
              this
            }

          }

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (style.json().length > 0) "\"style\"" + ":" + style.json()
              else ""
            val vars: List[String] = List(v0)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("style")
              in0.map(ve => style.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (items.json().length > 0) "\"items\"" + ":" + items.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("items")
            in0.map(ve => items.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (styles.json().length > 0) "\"styles\"" + ":" + styles.json()
          else ""
        val v1 =
          if (groups.json().length > 0) "\"groups\"" + ":" + groups.json()
          else ""
        val v2 =
          if (groupssrc.json().length > 0)
            "\"groupssrc\"" + ":" + groupssrc.json()
          else ""
        val v3 =
          if (nameformat.json().length > 0)
            "\"nameformat\"" + ":" + nameformat.json()
          else ""
        val v4 =
          if (enabled.json().length > 0) "\"enabled\"" + ":" + enabled.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("styles")
          in0.map(ve => styles.fromJson(ve))

          val in1 = mapKV.get("groups")
          in1.map(ve => groups.fromJson(ve))

          val in2 = mapKV.get("groupssrc")
          in2.map(ve => groupssrc.fromJson(ve))

          val in3 = mapKV.get("nameformat")
          in3.map(ve => nameformat.fromJson(ve))

          val in4 = mapKV.get("enabled")
          in4.map(ve => enabled.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (attributes.json().length > 0)
          "\"attributes\"" + ":" + attributes.json()
        else ""
      val vars: List[String] = List(v0)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("attributes")
        in0.map(ve => attributes.fromJson(ve))

      }
      this
    }

  }

  object layoutAttributes extends JSON {
    def json(): String                 = ""
    def fromJson(in: Value): this.type = this
  }

}
