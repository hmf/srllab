package splotly

import java.io.{File => JFile}
import java.nio.file.{DirectoryStream, FileSystem, FileSystems, Files, Path, PathMatcher}

import better.files.Dsl._
import better.files._
import splotly.LPlotly.Local
import utest._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import splotly.gen.Traces
import ujson.Value
import webkit.{HeadlessWebKit, ImageTestUtils}

import scala.collection.mutable.ArrayBuffer

/**
 * Here we run WebKit to load and take snapshots of several plots.
 * These tests are executed synchronously. It is important that the tests use
 * uTests's assert method.
 *
 * IMPORTANT NOTE: JavaFX or OpenJFX applications can only be instantiated once.
 * There is a RobotFX. But for now we use a custom uTest [[CustomFramework]] to
 * launch and use a single application.
 *
 * ./mill mill.scalalib.GenIdea/idea
 *
 * ./mill -i plots.test
 * ./mill -i plots.test.testLocal
 * ./mill -i plots.test splotly.ImageSpec
 *
 * Note: use this version to access the JavaFX libraries
 * ./jfxmill.sh -i plots.test "splotly.ImageSpec.Plotly Image Mocks.By Underscore"
 *
 */
object ImageSpec extends TestSuite {

  val tests: Tests = Tests {

    import Mocks._

    test ("Plotly Image Mocks") {

      test ("violin data") {

        // Get data
        val csv = CSV("./plots/data/violin_data.csv")
        val total_bill = csv("total_bill")
        val y = total_bill.map(_.map(_.toDouble))

        // Lets plot a violin
        val trace = splotly.gen.Violin()

        // Set-up the attrubutes
        trace.attributes.name.setValue("trace")
        trace.attributes.span.boolean_0(true)
        // Record the data to plot
        y.foreach( d => trace.attributes.y.setValue(d) )
        trace.attributes.points.false_()
        trace.attributes.box.visible.setValue(true)
        trace.attributes.line.color.setValue("black")
        trace.attributes.meanline.visible.setValue(true)
        trace.attributes.fillcolor.setValue("#8dd3c7")
        trace.attributes.opacity.setValue(0.6)
        trace.attributes.x0.setValue("Total Bill")
        // Violin specific layout attribute
        trace.layoutAttributes.violinmode.group()

        // No general Layout set
        val layout = gen.Layout()

        val violinMockDir: File =  base / "data"
        val violinImageDir: File = base / "data"
        val sample = Mocks.TestFiles("violin_data", violinMockDir, violinImageDir)

        // Check if static image is correct
        // Generate the plot using local Plotly JS script
        val htmlOut = LPlotly.plot("div1", List(trace), layout,
          useCdn = localScripts,
          openInBrowser = false)

        // Capture the image and test. Note that we never get a perfect match
        ImageTestUtils.captureAndCompareImage(htmlOut, sample.pngFileTmp, sample.goldPng)
    }

      /*
       * The code that is generated does not deal with deprecated JSON.
       * It was necessary to alter the original definition. Here are the
       * changes:
       * - "title" and "titlefont" are merged into a single "title" object
       * - in "xaxis" and "yaxis" "autotick": true is changed to
       *   "tickmode": "auto"
       * - "boxmode": "overlay" removed because it is only used by "box" and
       * "candlestick" traces ("barmode" already exists)
       */
      test ("0.json") {
        /*val zero = baseMockDir / "0.json"
        val json = ujson.read(zero.toJava)*/
        val zero = Mocks.TestFiles("0")
        val json = ujson.read(zero.jsonFile.toJava)

        // Parse and load the data into the wrappers
        val maybe = LPlotly.parserData(json)
        // It should succeed
        assert(maybe.isRight)
        val (traces, layout) = maybe.right.get
        // We have 3 Bar plots
        assert( traces.length == 3 )
        assert( traces.forall( t => t.isInstanceOf[gen.Bar]))

        // Make a round-trip test
        Utils.checkLayoutDiff(json, layout, traces)
        Utils.checkDataDiff(json, traces)

        // Check if static image is correct
        // Generate the plot using local Plotly JS script
        val htmlOut = LPlotly.plot("div1", traces, layout,
                                    useCdn = localScripts,
                                    openInBrowser = false)

        // We need to check against the PLotly tests
        // We must therefore clip to make the images match as closely as possible
        // The (8,8) offset makes the matches closer
        val vp = HeadlessWebKit.ViewPort(
          Some(8.0), Some(8.0),
          layout.layoutAttributes.width.getDoubleValue,
          layout.layoutAttributes.height.getDoubleValue)
        // Set the clipping area
        HeadlessWebKit.setViewport(vp)
        // Capture the image and test. Note that we never get a perfect match
        //ImageTestUtils.captureAndCompareImage(htmlOut, zero.pngFileTmp, zero.goldPng)
        ImageTestUtils.captureAndDiffImage(htmlOut, zero.pngFileTmp, zero.goldPng, 0.06)

      }

      test ("1.json") {
      }

      test ("By ID") {
        //val matches: Iterator[File] = mocks.globRegex("^\\d+.json".r)
        //val matches: Iterator[File] = mocks.glob("*.*")
        //val matches: List[File] = mocks.glob("**.json").toList
        //val matches: List[File] = mocks.glob("[0-9]**.json").toList
        /*val matches: List[File] = mocks.globRegex("\\d+.json".r).toList
        println(matches.mkString(";\n"))
        println(matches.length)*/

        /*
        val fs = FileSystems.getDefault
        //val pathMatcher: PathMatcher = fs.getPathMatcher("regex:.*")
        //val pathMatcher: PathMatcher = fs.getPathMatcher("regex:.*[.]json")
        //val pathMatcher: PathMatcher = fs.getPathMatcher("regex:[0-9]*.*")
        //val pathMatcher: PathMatcher = fs.getPathMatcher("glob:**.json") // single * does NOT work
        //val pathMatcher: PathMatcher = fs.getPathMatcher("glob:*.json") // * works if we use Filter for filename
        //val pathMatcher: PathMatcher = fs.getPathMatcher("glob:[0-9]*.json") // * works with filename Filter
        val pathMatcher: PathMatcher = fs.getPathMatcher("glob:[0-9]*[.]json") // * works with filename Filter
        val path: Path = mocks.path
        val filter = new DirectoryStream.Filter[Path](){
          override def accept (entry: Path): Boolean = {
            val tmp = pathMatcher.matches(entry.getFileName)
            //println(s"${entry.getFileName} = $tmp")
            tmp
          }
        }

        //val dirStream = Files.newDirectoryStream(path, pathMatcher.matches(_) )
        val dirStream = Files.newDirectoryStream(path, filter )
        dirStream.forEach(path => println("J: " + path))
        println(s"----------------------- ${path.toString}")
         */

      }

      /*test ("By Underscore") {
        val matches: List[File] = mocks.globRegex("(.*)_(.*).json".r).toList
        println(matches.mkString(";\n"))
        println(matches.length)
      }*/

      test ("By Hyphen") {
      }

      test ("All others") {
      }

    }

  }

}
