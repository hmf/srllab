package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Animation() extends Layouts {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/

  object mode extends JSON {
    /* §#
     * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dflt: String = """afterall"""
    val description: Option[String] = Some(
      """Describes how a new animate call interacts with currently-running animations. If `immediate`, current animations are interrupted and the new animation is started. If `next`, the current frame is allowed to complete, after which the new animation is started. If `afterall` all existing frames are animated to completion before the new animation is started."""
    )

    private var selected: OfPrimitive = NAPrimitive
    //allDeclarations

    def json(): String = {
      /* §#
       * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      selected match {
        case NAPrimitive => ""
        case TString(t)  => "\"" + t.toString + "\""
        case TBoolean(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TBoolean(false)"""
          )
        case TDouble(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TDouble(0.0)"""
          )
        case TInt(t) =>
          throw new RuntimeException("""Unexpected OfPrimitive type: TInt(0)""")
        case TImpliedEdits(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TImpliedEdits()"""
          )
        case TAttrRegexps(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TAttrRegexps()"""
          )
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err(j: ujson.Value): OfPrimitive =
        throw new RuntimeException(s"""Parsing of $j failed: $in""")
      val read = in match {
        case Str(s)  => TString(s)
        case Bool(s) => err(in)
        case Num(s)  => err(in)
        case _       => err(in)
      }
      selected = read
      this
    }

    def afterall(): Unit = { selected = TString("afterall") }

    def next(): Unit = { selected = TString("next") }

    def immediate(): Unit = { selected = TString("immediate") }

  }

  /* §#
   * generateBooleanDeclaration(in: PrimitiveDefinition)
   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val fromcurrent: PBool = PBool(
    dflt = Some(false),
    description = Some(
      """Play frames starting at the current frame instead of the beginning."""
    )
  )

  object direction extends JSON {
    /* §#
     * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dflt: String = """forward"""
    val description: Option[String] = Some(
      """The direction in which to play the frames triggered by the animation call"""
    )

    private var selected: OfPrimitive = NAPrimitive
    //allDeclarations

    def json(): String = {
      /* §#
       * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      selected match {
        case NAPrimitive => ""
        case TString(t)  => "\"" + t.toString + "\""
        case TBoolean(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TBoolean(false)"""
          )
        case TDouble(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TDouble(0.0)"""
          )
        case TInt(t) =>
          throw new RuntimeException("""Unexpected OfPrimitive type: TInt(0)""")
        case TImpliedEdits(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TImpliedEdits()"""
          )
        case TAttrRegexps(t) =>
          throw new RuntimeException(
            """Unexpected OfPrimitive type: TAttrRegexps()"""
          )
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err(j: ujson.Value): OfPrimitive =
        throw new RuntimeException(s"""Parsing of $j failed: $in""")
      val read = in match {
        case Str(s)  => TString(s)
        case Bool(s) => err(in)
        case Num(s)  => err(in)
        case _       => err(in)
      }
      selected = read
      this
    }

    def reverse(): Unit = { selected = TString("reverse") }

    def forward(): Unit = { selected = TString("forward") }

  }

  case object frame extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val role: String = """object"""

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val redraw: PBool = PBool(
      dflt = Some(true),
      description = Some(
        """Redraw the plot at completion of the transition. This is desirable for transitions that include properties that cannot be transitioned, but may significantly slow down updates that do not require a full redraw of the plot"""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val duration: PReal = PReal(
      min = Some(0.0),
      max = None,
      dflt = Some("""500.0"""),
      description = Some(
        """The duration in milliseconds of each frame. If greater than the frame duration, it will be limited to the frame duration."""
      )
    )

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (redraw.json().length > 0) "\"redraw\"" + ":" + redraw.json() else ""
      val v1 =
        if (duration.json().length > 0) "\"duration\"" + ":" + duration.json()
        else ""
      val vars: List[String] = List(v0, v1)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("redraw")
        in0.map(ve => redraw.fromJson(ve))

        val in1 = mapKV.get("duration")
        in1.map(ve => duration.fromJson(ve))

      }
      this
    }

  }

  case object transition extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val role: String = """object"""

    object ordering extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """layout first"""
      val description: Option[String] = Some(
        """Determines whether the figure's layout or traces smoothly transitions during updates that make both traces and layout change."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def traces_first(): Unit = { selected = TString("traces first") }

      def layout_first(): Unit = { selected = TString("layout first") }

    }

    object easing extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """cubic-in-out"""
      val description: Option[String] = Some(
        """The easing function used for the transition"""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def bounce_in_out(): Unit = { selected = TString("bounce-in-out") }

      def back_in_out(): Unit = { selected = TString("back-in-out") }

      def elastic_in_out(): Unit = { selected = TString("elastic-in-out") }

      def circle_in_out(): Unit = { selected = TString("circle-in-out") }

      def exp_in_out(): Unit = { selected = TString("exp-in-out") }

      def sin_in_out(): Unit = { selected = TString("sin-in-out") }

      def cubic_in_out(): Unit = { selected = TString("cubic-in-out") }

      def quad_in_out(): Unit = { selected = TString("quad-in-out") }

      def linear_in_out(): Unit = { selected = TString("linear-in-out") }

      def bounce_out(): Unit = { selected = TString("bounce-out") }

      def back_out(): Unit = { selected = TString("back-out") }

      def elastic_out(): Unit = { selected = TString("elastic-out") }

      def circle_out(): Unit = { selected = TString("circle-out") }

      def exp_out(): Unit = { selected = TString("exp-out") }

      def sin_out(): Unit = { selected = TString("sin-out") }

      def cubic_out(): Unit = { selected = TString("cubic-out") }

      def quad_out(): Unit = { selected = TString("quad-out") }

      def linear_out(): Unit = { selected = TString("linear-out") }

      def bounce_in(): Unit = { selected = TString("bounce-in") }

      def back_in(): Unit = { selected = TString("back-in") }

      def elastic_in(): Unit = { selected = TString("elastic-in") }

      def circle_in(): Unit = { selected = TString("circle-in") }

      def exp_in(): Unit = { selected = TString("exp-in") }

      def sin_in(): Unit = { selected = TString("sin-in") }

      def cubic_in(): Unit = { selected = TString("cubic-in") }

      def quad_in(): Unit = { selected = TString("quad-in") }

      def linear_in(): Unit = { selected = TString("linear-in") }

      def bounce(): Unit = { selected = TString("bounce") }

      def back(): Unit = { selected = TString("back") }

      def elastic(): Unit = { selected = TString("elastic") }

      def circle(): Unit = { selected = TString("circle") }

      def exp(): Unit = { selected = TString("exp") }

      def sin(): Unit = { selected = TString("sin") }

      def cubic(): Unit = { selected = TString("cubic") }

      def quad(): Unit = { selected = TString("quad") }

      def linear(): Unit = { selected = TString("linear") }

    }

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val duration: PReal = PReal(
      min = Some(0.0),
      max = None,
      dflt = Some("""500.0"""),
      description = Some(
        """The duration of the transition, in milliseconds. If equal to zero, updates are synchronous."""
      )
    )

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (ordering.json().length > 0) "\"ordering\"" + ":" + ordering.json()
        else ""
      val v1 =
        if (easing.json().length > 0) "\"easing\"" + ":" + easing.json() else ""
      val v2 =
        if (duration.json().length > 0) "\"duration\"" + ":" + duration.json()
        else ""
      val vars: List[String] = List(v0, v1, v2)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("ordering")
        in0.map(ve => ordering.fromJson(ve))

        val in1 = mapKV.get("easing")
        in1.map(ve => easing.fromJson(ve))

        val in2 = mapKV.get("duration")
        in2.map(ve => duration.fromJson(ve))

      }
      this
    }

  }

  object layoutAttributes extends JSON {
    def json(): String                 = ""
    def fromJson(in: Value): this.type = this
  }

}
