# Mill Build System

## Introduction

We use the [Mill](https://github.com/lihaoyi/mill) build system, which is 
documented [here](http://www.lihaoyi.com/mill/). Here we list some commands to
help you get started. Make sure your current working directory is the root of 
the project where the `build.sc` file is present. In these examples we assume 
that you are using `Linux` so that the `mill` boot script found in the project 
root can be used. This script will ensure that the correct version of Mill is 
downloaded and executed. The Mill version used is configured in the hidden file
`.mill-version`, also located in the the project root. If you are not using 
Windows make sure that you have installed the Mill application first and call 
Mill **without** the explicit path.   

## Some Useful Commands

Here we list some of the more useful commands for this project. In order to see 
what commands are available we can execute: 

```bash
./mill -i resolve _
```

More specifically we can see what we have made available in the SDK. 

```bash
./mill -i resolve sdk._
```

You can also use the `inspect` command that is a more detailed version of 
`resolve`. 

```bash
./mill inspect sdk.mDoc
```

If you want to check all of the metadata associated with a target or variable use the following command:

```bash
./mill show sdk.mDoc
```

Note that all of the above commands accept wild cards. In Mill this wildcard is an underscore. We have already 
seen the use of these wildcards in the `resolve` command.  

To clean all the targets and restart from scratch, use the following command:

```bash
./mill clean
```

You can also clean a specific module so:

```bash
./mill clean sdk
```

To generate the Scaladoc and Javadoc API documentation execute the following command:

```bash
./mill -i sdk.docJar
```

We can also generate an IntelliJ project with the following command:

```bash
./mill mill.scalalib.GenIdea/idea
```

Whenever you add or remove a library from the `build.sc` file, make sure to run the above command so that 
the IDE can be used correctly.  

Assume we have a module `foo`. The following is a list of useful commands for compiling, executing and packaging
the module (taken from the Mill documentation site):  

* `mill foo.compile` compile sources into class files
* `mill foo.run` run the main method, if one exists
* `mill foo.runBackground` run the main method in the background, if one exists
* `mill foo.launcher` prepares a `foo/launcher/dest/run` you can run later
* `mill foo.jar` bundles the class files into a jar
* `mill foo.assembly` bundle class files and all dependencies into a jar
* `mill -i foo.console` start a Scala console within your project (in interactive mode: "-i")
* `mill -i foo.repl` start an Ammonite REPL within your project (in interactive mode: "-i")

The following are a list of executable classes (have a main method): 

```bash
./mill -i sdk.runMain srllab.PlotlyScalaExamples
./mill mill.scalalib.GenIdea/idea
./mill -i sdk.runMain srllab.EvilPlotExamples
./mill -i sdk.runMain srllab.TablesawPlotExamples 
./mill -i sdk.runMain bugs.MissingValues 
./mill -i sdk.runMain srllab.SRLLab
```

Our `build.sc` file also provides the [laika](laika.html) and [mdoc](mdoc.html) targets that can be used to 
generate documentation and notebooks. You can include plots in the notebooks. This documentation was generated 
using those targets.     

