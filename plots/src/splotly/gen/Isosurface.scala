package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Isosurface() extends Trace {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val type_ : String            = """isosurface"""
  val animatable: Boolean       = false
  val categories: Array[String] = Array("""showLegend""", """gl3d""")

  case object meta extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val description: String =
      """Draws isosurfaces between iso-min and iso-max values with coordinates given by four 1-dimensional arrays containing the `value`, `x`, `y` and `z` of every vertex of a uniform or non-uniform 3-D grid. Horizontal or vertical slices, caps as well as spaceframe between iso-min and iso-max values could also be drawn using this trace."""

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val vars: List[String] = List()
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV => }
      this
    }

  }

  case object attributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val type_ : String = """isosurface"""

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplate: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Template string used for rendering the information that appear on hover box. Note that this will override `hoverinfo`. Variables are inserted using %{variable}, for example "y: %{y}". Numbers are formatted using d3-format's syntax %{variable:d3-format}, for example "Price: %{y:$.2f}". https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format for details on the formatting syntax. Dates are formatted using d3-time-format's syntax %{variable|d3-time-format}, for example "Day: %{2019-01-01|%A}". https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format for details on the date formatting syntax. The variables available in `hovertemplate` are the ones emitted as event data described at this link https://plot.ly/javascript/plotlyjs-events/#event-data. Additionally, every attributes that can be specified per-point (the ones that are `arrayOk: true`) are available.  Anything contained in tag `<extra>` is displayed in the secondary box, for example "<extra>{fullData.name}</extra>". To hide the secondary box completely, use an empty tag `<extra></extra>`."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val xsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  x .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val x: DataArray = DataArray(
      dflt = None,
      description =
        Some("""Sets the X coordinates of the vertices on X axis.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val name: PChars = PChars(
      dflt = None,
      description = Some(
        """Sets the trace name. The trace name appear as the legend item and on hover."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val cmax: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description = Some(
        """Sets the upper bound of the color domain. Value should have the same units as `value` and if set, `cmin` must be set as well."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplatesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertemplate .""")
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val isomin: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description = Some("""Sets the minimum boundary for iso-surface plot.""")
    )

    object hoverinfo extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = Some("""all""")
      val description: Option[String] = Some(
        """Determines which trace information appear on hover. If `none` or `skip` are set, no information is displayed upon hovering. But, if `none` is set, click and hover events are still fired."""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def nameOn(): Unit  = { flag_ + "name" }
      def nameOff(): Unit = { flag_ - "name" }
      def textOn(): Unit  = { flag_ + "text" }
      def textOff(): Unit = { flag_ - "text" }
      def zOn(): Unit     = { flag_ + "z" }
      def zOff(): Unit    = { flag_ - "z" }
      def yOn(): Unit     = { flag_ + "y" }
      def yOff(): Unit    = { flag_ - "y" }
      def xOn(): Unit     = { flag_ + "x" }
      def xOff(): Unit    = { flag_ - "x" }
      def skipOn(): Unit  = { flag_ + "skip" }
      def skipOff(): Unit = { flag_ - "skip" }
      def noneOn(): Unit  = { flag_ + "none" }
      def noneOff(): Unit = { flag_ - "none" }
      def allOn(): Unit   = { flag_ + "all" }
      def allOff(): Unit  = { flag_ - "all" }
    }

    object visible extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Boolean = true
      val description: Option[String] = Some(
        """Determines whether or not this trace is visible. If *legendonly*, the trace is not drawn, but can appear as a legend item (provided that the legend itself is visible)."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) => t.toString
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => TBoolean(s)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def true_(): Unit = { selected = TBoolean(true) }

      def false_(): Unit = { selected = TBoolean(false) }

      def legendonly(): Unit = { selected = TString("legendonly") }

    }

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val y: DataArray = DataArray(
      dflt = None,
      description =
        Some("""Sets the Y coordinates of the vertices on Y axis.""")
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val cmin: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description = Some(
        """Sets the lower bound of the color domain. Value should have the same units as `value` and if set, `cmax` must be set as well."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uirevision: PAny = PAny(
      dflt = None,
      description = Some(
        """Controls persistence of some user-driven changes to the trace: `constraintrange` in `parcoords` traces, as well as some `editable: true` modifications such as `name` and `colorbar.title`. Defaults to `layout.uirevision`. Note that other user-driven trace attribute changes are controlled by `layout` attributes: `trace.visible` is controlled by `layout.legend.uirevision`, `selectedpoints` is controlled by `layout.selectionrevision`, and `colorbar.(x|y)` (accessible with `config: {editable: true}`) is controlled by `layout.editrevision`. Trace changes are tracked by `uid`, which only falls back on trace index if no `uid` is provided. So if your app can add/remove traces before the end of the `data` array, such that the same trace has a different index, you can still preserve user-driven changes if you give each trace a `uid` that stays with it as it moves."""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val autocolorscale: PBool = PBool(
      dflt = Some(true),
      description = Some(
        """Determines whether the colorscale is a default palette (`autocolorscale: true`) or the palette determined by `colorscale`. In case `colorscale` is unspecified or `autocolorscale` is true, the default  palette will be chosen according to whether numbers in the `color` array are all positive, all negative or mixed."""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val reversescale: PBool = PBool(
      dflt = Some(false),
      description = Some(
        """Reverses the color mapping if true. If true, `cmin` will correspond to the last color in the array and `cmax` will correspond to the first color."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ysrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  y .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val idssrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  ids .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val text: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Sets the text elements associated with the vertices. If trace `hoverinfo` contains a *text* flag and *hovertext* is not set, these elements will be seen in the hover labels."""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val showscale: PBool = PBool(
      dflt = Some(true),
      description = Some(
        """Determines whether or not a colorbar is displayed for this trace."""
      )
    )

    /* §#
     * generateSubPlotIDDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val coloraxis: SubPlotID = SubPlotID(
      dflt = None,
      description = Some(
        """Sets a reference to a shared color axis. References to these shared color axes are *coloraxis*, *coloraxis2*, *coloraxis3*, etc. Settings for these shared color axes are set in the layout, under `layout.coloraxis`, `layout.coloraxis2`, etc. Note that multiple color scales can be linked to the same color axis."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hoverinfosrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hoverinfo .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertextsrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertext .""")
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val meta: PAny = PAny(
      dflt = None,
      description = Some(
        """Assigns extra meta information associated with this trace that can be used in various text attributes. Attributes such as trace `name`, graph, axis and colorbar `title.text`, annotation `text` `rangeselector`, `updatemenues` and `sliders` `label` text all support `meta`. To access the trace `meta` values in an attribute in the same trace, simply use `%{meta[i]}` where `i` is the index or key of the `meta` item in question. To access trace `meta` in layout attributes, use `%{data[n[.meta[i]}` where `i` is the index or key of the `meta` and `n` is the trace index."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val textsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  text .""")
    )

    /* §#
     * generateSubPlotIDDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val scene: SubPlotID = SubPlotID(
      dflt = Some("""scene"""),
      description = Some(
        """Sets a reference between this trace's 3D coordinate system and a 3D scene. If *scene* (the default value), the (x,y,z) coordinates refer to `layout.scene`. If *scene2*, the (x,y,z) coordinates refer to `layout.scene2`, and so on."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertext: PChars = PChars(
      dflt = Some(""""""),
      description = Some("""Same as `text`.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uid: PChars = PChars(
      dflt = None,
      description = Some(
        """Assign an id to this trace, Use this to provide object constancy between traces during animations and transitions."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val legendgroup: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Sets the legend group for this trace. Traces part of the same legend group hide/show at the same time when toggling legend items."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val isomax: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description = Some("""Sets the maximum boundary for iso-surface plot.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val zsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  z .""")
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val opacity: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = Some("""1.0"""),
      description = Some(
        """Sets the opacity of the surface. Please note that in the case of using high `opacity` values for example a value greater than or equal to 0.5 on two surfaces (and 0.25 with four surfaces), an overlay of multiple transparent surfaces may not perfectly be sorted in depth by the webgl API. This behavior may be improved in the near future and is subject to change."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val cmid: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description = Some(
        """Sets the mid-point of the color domain by scaling `cmin` and/or `cmax` to be equidistant to this point. Value should have the same units as `value`. Has no effect when `cauto` is `false`."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ids: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns id labels to each datum. These ids for object constancy of data points during animation. Should be an array of strings, not numbers or any other type."""
      )
    )

    /* §#
     * generateColorScaleDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val colorscale: PColorScale = PColorScale(
      dflt = None,
      description = Some(
        """Sets the colorscale. The colorscale must be an array containing arrays mapping a normalized value to an rgb, rgba, hex, hsl, hsv, or named color string. At minimum, a mapping for the lowest (0) and highest (1) values are required. For example, `[[0, 'rgb(0,0,255)'], [1, 'rgb(255,0,0)']]`. To control the bounds of the colorscale in color space, use`cmin` and `cmax`. Alternatively, `colorscale` may be a palette name string of the following list: Greys,YlGnBu,Greens,YlOrRd,Bluered,RdBu,Reds,Blues,Picnic,Rainbow,Portland,Jet,Hot,Blackbody,Earth,Electric,Viridis,Cividis."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val metasrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  meta .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdata: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns extra data each datum. This may be useful when listening to hover, click and selection events. Note that, *scatter* traces also appends customdata items in the markers DOM elements"""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val value: DataArray = DataArray(
      dflt = None,
      description = Some("""Sets the 4th dimension (value) of the vertices.""")
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val showlegend: PBool = PBool(
      dflt = Some(false),
      description = Some(
        """Determines whether or not an item corresponding to this trace is shown in the legend."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val valuesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  value .""")
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val cauto: PBool = PBool(
      dflt = Some(true),
      description = Some(
        """Determines whether or not the color domain is computed with respect to the input data (here `value`) or the bounds set in `cmin` and `cmax`  Defaults to `false` when `cmin` and `cmax` are set by the user."""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val flatshading: PBool = PBool(
      dflt = Some(true),
      description = Some(
        """Determines whether or not normal smoothing is applied to the meshes, creating meshes with an angular, low-poly look via flat reflections."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val z: DataArray = DataArray(
      dflt = None,
      description =
        Some("""Sets the Z coordinates of the vertices on Z axis.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdatasrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  customdata .""")
    )

    case object surface extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val count: PInt = PInt(
        min = Some(1),
        max = None,
        dflt = Some(2),
        description = Some(
          """Sets the number of iso-surfaces between minimum and maximum iso-values. By default this value is 2 meaning that only minimum and maximum surfaces would be drawn."""
        )
      )

      object pattern extends JSON {
        /* §#
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: Option[String] = Some("""all""")
        val description: Option[String] = Some(
          """Sets the surface pattern of the iso-surface 3-D sections. The default pattern of the surface is `all` meaning that the rest of surface elements would be shaded. The check options (either 1 or 2) could be used to draw half of the squares on the surface. Using various combinations of capital `A`, `B`, `C`, `D` and `E` may also be used to reduce the number of triangles on the iso-surfaces and creating other patterns of interest."""
        )

        private val flag_ : mutable.Set[String] = mutable.Set()
        //dflt.map(e => flag_ + e)
        def flag: String = flag_.mkString("+")
        def json(): String = {
          if (flag.contains("true"))
            "true"
          else if (flag.contains("false"))
            "false"
          else if (flag.length > 0)
            "\"" + flag + "\""
          else
            ""
        }

        override def fromJson(in: Value): this.type = {
          /* §#
           * generateFlagListFromJson()
           * generateFlagListDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          in match {
            case Str(value) =>
              flag_ += value
              this
            case Bool(value) =>
              flag_ += value.toString
              this
            case _ =>
              err(in)
              this
          }
        }

        def EOn(): Unit     = { flag_ + "E" }
        def EOff(): Unit    = { flag_ - "E" }
        def DOn(): Unit     = { flag_ + "D" }
        def DOff(): Unit    = { flag_ - "D" }
        def COn(): Unit     = { flag_ + "C" }
        def COff(): Unit    = { flag_ - "C" }
        def BOn(): Unit     = { flag_ + "B" }
        def BOff(): Unit    = { flag_ - "B" }
        def AOn(): Unit     = { flag_ + "A" }
        def AOff(): Unit    = { flag_ - "A" }
        def evenOn(): Unit  = { flag_ + "even" }
        def evenOff(): Unit = { flag_ - "even" }
        def oddOn(): Unit   = { flag_ + "odd" }
        def oddOff(): Unit  = { flag_ - "odd" }
        def allOn(): Unit   = { flag_ + "all" }
        def allOff(): Unit  = { flag_ - "all" }
      }

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val fill: PReal = PReal(
        min = Some(0.0),
        max = Some(1.0),
        dflt = Some("""1.0"""),
        description = Some(
          """Sets the fill ratio of the iso-surface. The default fill value of the surface is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val show: PBool = PBool(
        dflt = Some(true),
        description = Some(
          """Hides/displays surfaces between minimum and maximum iso-values."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (count.json().length > 0) "\"count\"" + ":" + count.json() else ""
        val v1 =
          if (pattern.json().length > 0) "\"pattern\"" + ":" + pattern.json()
          else ""
        val v2 =
          if (fill.json().length > 0) "\"fill\"" + ":" + fill.json() else ""
        val v3 =
          if (show.json().length > 0) "\"show\"" + ":" + show.json() else ""
        val vars: List[String] = List(v0, v1, v2, v3)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("count")
          in0.map(ve => count.fromJson(ve))

          val in1 = mapKV.get("pattern")
          in1.map(ve => pattern.fromJson(ve))

          val in2 = mapKV.get("fill")
          in2.map(ve => fill.fromJson(ve))

          val in3 = mapKV.get("show")
          in3.map(ve => show.fromJson(ve))

        }
        this
      }

    }

    case object lightposition extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val z: PReal = PReal(
        min = Some(-100000.0),
        max = Some(100000.0),
        dflt = Some("""0.0"""),
        description = Some(
          """Numeric vector, representing the Z coordinate for each vertex."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val y: PReal = PReal(
        min = Some(-100000.0),
        max = Some(100000.0),
        dflt = Some("""100000.0"""),
        description = Some(
          """Numeric vector, representing the Y coordinate for each vertex."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val x: PReal = PReal(
        min = Some(-100000.0),
        max = Some(100000.0),
        dflt = Some("""100000.0"""),
        description = Some(
          """Numeric vector, representing the X coordinate for each vertex."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0                 = if (z.json().length > 0) "\"z\"" + ":" + z.json() else ""
        val v1                 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
        val v2                 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
        val vars: List[String] = List(v0, v1, v2)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("z")
          in0.map(ve => z.fromJson(ve))

          val in1 = mapKV.get("y")
          in1.map(ve => y.fromJson(ve))

          val in2 = mapKV.get("x")
          in2.map(ve => x.fromJson(ve))

        }
        this
      }

    }

    case object lighting extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val vertexnormalsepsilon: PReal = PReal(
        min = Some(0.0),
        max = Some(1.0),
        dflt = Some("""1.0E-12"""),
        description = Some(
          """Epsilon for vertex normals calculation avoids math issues arising from degenerate geometry."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val facenormalsepsilon: PReal = PReal(
        min = Some(0.0),
        max = Some(1.0),
        dflt = Some("""0.0"""),
        description = Some(
          """Epsilon for face normals calculation avoids math issues arising from degenerate geometry."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val fresnel: PReal = PReal(
        min = Some(0.0),
        max = Some(5.0),
        dflt = Some("""0.2"""),
        description = Some(
          """Represents the reflectance as a dependency of the viewing angle; e.g. paper is reflective when viewing it from the edge of the paper (almost 90 degrees), causing shine."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val diffuse: PReal = PReal(
        min = Some(0.0),
        max = Some(1.0),
        dflt = Some("""0.8"""),
        description = Some(
          """Represents the extent that incident rays are reflected in a range of angles."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val roughness: PReal = PReal(
        min = Some(0.0),
        max = Some(1.0),
        dflt = Some("""0.5"""),
        description = Some(
          """Alters specular reflection; the rougher the surface, the wider and less contrasty the shine."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val specular: PReal = PReal(
        min = Some(0.0),
        max = Some(2.0),
        dflt = Some("""0.05"""),
        description = Some(
          """Represents the level that incident rays are reflected in a single direction, causing shine."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val ambient: PReal = PReal(
        min = Some(0.0),
        max = Some(1.0),
        dflt = Some("""0.8"""),
        description = Some(
          """Ambient light increases overall color visibility but can wash out the image."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (vertexnormalsepsilon.json().length > 0)
            "\"vertexnormalsepsilon\"" + ":" + vertexnormalsepsilon.json()
          else ""
        val v1 =
          if (facenormalsepsilon.json().length > 0)
            "\"facenormalsepsilon\"" + ":" + facenormalsepsilon.json()
          else ""
        val v2 =
          if (fresnel.json().length > 0) "\"fresnel\"" + ":" + fresnel.json()
          else ""
        val v3 =
          if (diffuse.json().length > 0) "\"diffuse\"" + ":" + diffuse.json()
          else ""
        val v4 =
          if (roughness.json().length > 0)
            "\"roughness\"" + ":" + roughness.json()
          else ""
        val v5 =
          if (specular.json().length > 0) "\"specular\"" + ":" + specular.json()
          else ""
        val v6 =
          if (ambient.json().length > 0) "\"ambient\"" + ":" + ambient.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("vertexnormalsepsilon")
          in0.map(ve => vertexnormalsepsilon.fromJson(ve))

          val in1 = mapKV.get("facenormalsepsilon")
          in1.map(ve => facenormalsepsilon.fromJson(ve))

          val in2 = mapKV.get("fresnel")
          in2.map(ve => fresnel.fromJson(ve))

          val in3 = mapKV.get("diffuse")
          in3.map(ve => diffuse.fromJson(ve))

          val in4 = mapKV.get("roughness")
          in4.map(ve => roughness.fromJson(ve))

          val in5 = mapKV.get("specular")
          in5.map(ve => specular.fromJson(ve))

          val in6 = mapKV.get("ambient")
          in6.map(ve => ambient.fromJson(ve))

        }
        this
      }

    }

    case object spaceframe extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val fill: PReal = PReal(
        min = Some(0.0),
        max = Some(1.0),
        dflt = Some("""0.15"""),
        description = Some(
          """Sets the fill ratio of the `spaceframe` elements. The default fill value is 0.15 meaning that only 15% of the area of every faces of tetras would be shaded. Applying a greater `fill` ratio would allow the creation of stronger elements or could be sued to have entirely closed areas (in case of using 1)."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val show: PBool = PBool(
        dflt = Some(false),
        description = Some(
          """Displays/hides tetrahedron shapes between minimum and maximum iso-values. Often useful when either caps or surfaces are disabled or filled with values less than 1."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (fill.json().length > 0) "\"fill\"" + ":" + fill.json() else ""
        val v1 =
          if (show.json().length > 0) "\"show\"" + ":" + show.json() else ""
        val vars: List[String] = List(v0, v1)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("fill")
          in0.map(ve => fill.fromJson(ve))

          val in1 = mapKV.get("show")
          in1.map(ve => show.fromJson(ve))

        }
        this
      }

    }

    case object colorbar extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val x: PReal = PReal(
        min = Some(-2.0),
        max = Some(3.0),
        dflt = Some("""1.02"""),
        description =
          Some("""Sets the x position of the color bar (in plot fraction).""")
      )

      /* §#
       * generateAny(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val tick0: PAny = PAny(
        dflt = None,
        description = Some(
          """Sets the placement of the first tick on this axis. Use with `dtick`. If the axis `type` is *log*, then you must take the log of your starting tick (e.g. to set the starting tick to 100, set the `tick0` to 2) except when `dtick`=*L<f>* (see `dtick` for more info). If the axis `type` is *date*, it should be a date string, like date data. If the axis `type` is *category*, it should be a number, using the scale where each category is assigned a serial number from zero in the order it appears."""
        )
      )

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val nticks: PInt = PInt(
        min = Some(0),
        max = None,
        dflt = Some(0),
        description = Some(
          """Specifies the maximum number of ticks for the particular axis. The actual number of ticks will be chosen automatically to be less than or equal to `nticks`. Has an effect only if `tickmode` is set to *auto*."""
        )
      )

      /* §#
       * generateAngleDeclaration(in: PrimitiveDefinition
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val tickangle: PAngle = PAngle(
        dflt = Some("""auto"""),
        description = Some(
          """Sets the angle of the tick labels with respect to the horizontal. For example, a `tickangle` of -90 draws the tick labels vertically."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val outlinewidth: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""1.0"""),
        description = Some("""Sets the width (in px) of the axis line.""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolor: PColor = PColor(
        dflt = Some("""rgba(0,0,0,0)"""),
        description = Some("""Sets the color of padded area.""")
      )

      object showexponent extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """all"""
        val description: Option[String] = Some(
          """If *all*, all exponents are shown besides their significands. If *first*, only the exponent of the first tick is shown. If *last*, only the exponent of the last tick is shown. If *none*, no exponents appear."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def none(): Unit = { selected = TString("none") }

        def last(): Unit = { selected = TString("last") }

        def first(): Unit = { selected = TString("first") }

        def all(): Unit = { selected = TString("all") }

      }

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val ypad: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""10.0"""),
        description =
          Some("""Sets the amount of padding (in px) along the y direction.""")
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val y: PReal = PReal(
        min = Some(-2.0),
        max = Some(3.0),
        dflt = Some("""0.5"""),
        description =
          Some("""Sets the y position of the color bar (in plot fraction).""")
      )

      object yanchor extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """middle"""
        val description: Option[String] = Some(
          """Sets this color bar's vertical position anchor This anchor binds the `y` position to the *top*, *middle* or *bottom* of the color bar."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def bottom(): Unit = { selected = TString("bottom") }

        def middle(): Unit = { selected = TString("middle") }

        def top(): Unit = { selected = TString("top") }

      }

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val ticksuffix: PChars = PChars(
        dflt = Some(""""""),
        description = Some("""Sets a tick label suffix.""")
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val tickwidth: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""1.0"""),
        description = Some("""Sets the tick width (in px).""")
      )

      /* §#
       * generateDataArrayDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val tickvals: DataArray = DataArray(
        dflt = None,
        description = Some(
          """Sets the values at which ticks on this axis appear. Only has an effect if `tickmode` is set to *array*. Used with `ticktext`."""
        )
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolor: PColor = PColor(
        dflt = Some("""#444"""),
        description = Some("""Sets the axis line color.""")
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val thickness: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""30.0"""),
        description = Some(
          """Sets the thickness of the color bar This measure excludes the size of the padding, ticks and labels."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val ticklen: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""5.0"""),
        description = Some("""Sets the tick length (in px).""")
      )

      object tickmode extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val description: Option[String] = Some(
          """Sets the tick mode for this axis. If *auto*, the number of ticks is set via `nticks`. If *linear*, the placement of the ticks is determined by a starting position `tick0` and a tick step `dtick` (*linear* is the default value if `tick0` and `dtick` are provided). If *array*, the placement of the ticks is set via `tickvals` and the tick text is `ticktext`. (*array* is the default value if `tickvals` is provided)."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def array(): Unit = { selected = TString("array") }

        def linear(): Unit = { selected = TString("linear") }

        def auto(): Unit = { selected = TString("auto") }

      }

      /* §#
       * generateAny(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dtick: PAny = PAny(
        dflt = None,
        description = Some(
          """Sets the step in-between ticks on this axis. Use with `tick0`. Must be a positive number, or special strings available to *log* and *date* axes. If the axis `type` is *log*, then ticks are set every 10^(n*dtick) where n is the tick number. For example, to set a tick mark at 1, 10, 100, 1000, ... set dtick to 1. To set tick marks at 1, 100, 10000, ... set dtick to 2. To set tick marks at 1, 5, 25, 125, 625, 3125, ... set dtick to log_10(5), or 0.69897000433. *log* has several special values; *L<f>*, where `f` is a positive number, gives ticks linearly spaced in value (but not position). For example `tick0` = 0.1, `dtick` = *L0.5* will put ticks at 0.1, 0.6, 1.1, 1.6 etc. To show powers of 10 plus small digits between, use *D1* (all digits) or *D2* (only 2 and 5). `tick0` is ignored for *D1* and *D2*. If the axis `type` is *date*, then you must convert the time to milliseconds. For example, to set the interval between ticks to one day, set `dtick` to 86400000.0. *date* also has special values *M<n>* gives ticks spaced by a number of months. `n` must be a positive integer. To set ticks on the 15th of every third month, set `tick0` to *2000-01-15* and `dtick` to *M3*. To set ticks every 4 years, set `dtick` to *M48*"""
        )
      )

      object lenmode extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """fraction"""
        val description: Option[String] = Some(
          """Determines whether this color bar's length (i.e. the measure in the color variation direction) is set in units of plot *fraction* or in *pixels. Use `len` to set the value."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def pixels(): Unit = { selected = TString("pixels") }

        def fraction(): Unit = { selected = TString("fraction") }

      }

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val tickvalssrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  tickvals .""")
      )

      object showticksuffix extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """all"""
        val description: Option[String] = Some(
          """Same as `showtickprefix` but for tick suffixes."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def none(): Unit = { selected = TString("none") }

        def last(): Unit = { selected = TString("last") }

        def first(): Unit = { selected = TString("first") }

        def all(): Unit = { selected = TString("all") }

      }

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val xpad: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""10.0"""),
        description =
          Some("""Sets the amount of padding (in px) along the x direction.""")
      )

      /* §#
       * generateDataArrayDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val ticktext: DataArray = DataArray(
        dflt = None,
        description = Some(
          """Sets the text displayed at the ticks position via `tickvals`. Only has an effect if `tickmode` is set to *array*. Used with `tickvals`."""
        )
      )

      object ticks extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """"""
        val description: Option[String] = Some(
          """Determines whether ticks are drawn or not. If **, this axis' ticks are not drawn. If *outside* (*inside*), this axis' are drawn outside (inside) the axis lines."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def default(): Unit = { selected = TString("") }

        def inside(): Unit = { selected = TString("inside") }

        def outside(): Unit = { selected = TString("outside") }

      }

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val tickcolor: PColor = PColor(
        dflt = Some("""#444"""),
        description = Some("""Sets the tick color.""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val ticktextsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  ticktext .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val outlinecolor: PColor = PColor(
        dflt = Some("""#444"""),
        description = Some("""Sets the axis line color.""")
      )

      object exponentformat extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """B"""
        val description: Option[String] = Some(
          """Determines a formatting rule for the tick exponents. For example, consider the number 1,000,000,000. If *none*, it appears as 1,000,000,000. If *e*, 1e+9. If *E*, 1E+9. If *power*, 1x10^9 (with 9 in a super script). If *SI*, 1G. If *B*, 1B."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def B(): Unit = { selected = TString("B") }

        def SI(): Unit = { selected = TString("SI") }

        def power(): Unit = { selected = TString("power") }

        def E(): Unit = { selected = TString("E") }

        def e(): Unit = { selected = TString("e") }

        def none(): Unit = { selected = TString("none") }

      }

      object showtickprefix extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """all"""
        val description: Option[String] = Some(
          """If *all*, all tick labels are displayed with a prefix. If *first*, only the first tick is displayed with a prefix. If *last*, only the last tick is displayed with a suffix. If *none*, tick prefixes are hidden."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def none(): Unit = { selected = TString("none") }

        def last(): Unit = { selected = TString("last") }

        def first(): Unit = { selected = TString("first") }

        def all(): Unit = { selected = TString("all") }

      }

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val tickprefix: PChars = PChars(
        dflt = Some(""""""),
        description = Some("""Sets a tick label prefix.""")
      )

      object xanchor extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """left"""
        val description: Option[String] = Some(
          """Sets this color bar's horizontal position anchor. This anchor binds the `x` position to the *left*, *center* or *right* of the color bar."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def right(): Unit = { selected = TString("right") }

        def center(): Unit = { selected = TString("center") }

        def left(): Unit = { selected = TString("left") }

      }

      object thicknessmode extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """pixels"""
        val description: Option[String] = Some(
          """Determines whether this color bar's thickness (i.e. the measure in the constant color direction) is set in units of plot *fraction* or in *pixels*. Use `thickness` to set the value."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def pixels(): Unit = { selected = TString("pixels") }

        def fraction(): Unit = { selected = TString("fraction") }

      }

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val separatethousands: PBool = PBool(
        dflt = Some(false),
        description = Some("""If "true", even 4-digit integers are separated""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val tickformat: PChars = PChars(
        dflt = Some(""""""),
        description = Some(
          """Sets the tick label formatting rule using d3 formatting mini-languages which are very similar to those in Python. For numbers, see: https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format And for dates see: https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format We add one item to d3's date formatter: *%{n}f* for fractional seconds with n digits. For example, *2016-10-13 09:15:23.456* with tickformat *%H~%M~%S.%2f* would display *09~15~23.46*"""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val showticklabels: PBool = PBool(
        dflt = Some(true),
        description =
          Some("""Determines whether or not the tick labels are drawn.""")
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val len: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""1.0"""),
        description = Some(
          """Sets the length of the color bar This measure excludes the padding of both ends. That is, the color bar length is this length minus the padding on both ends."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val borderwidth: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""0.0"""),
        description = Some(
          """Sets the width (in px) or the border enclosing this color bar."""
        )
      )

      case object tickfont extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Sets the color bar's tick label font"""
        val editType: String    = """calc"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v1 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val v2 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("color")
            in0.map(ve => color.fromJson(ve))

            val in1 = mapKV.get("family")
            in1.map(ve => family.fromJson(ve))

            val in2 = mapKV.get("size")
            in2.map(ve => size.fromJson(ve))

          }
          this
        }

      }

      case object title extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """calc"""

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val text: PChars = PChars(
          dflt = None,
          description = Some(
            """Sets the title of the color bar. Note that before the existence of `title.text`, the title's contents used to be defined as the `title` attribute itself. This behavior has been deprecated."""
          )
        )

        object side extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """top"""
          val description: Option[String] = Some(
            """Determines the location of color bar's title with respect to the color bar. Note that the title's location used to be set by the now deprecated `titleside` attribute."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def bottom(): Unit = { selected = TString("bottom") }

          def top(): Unit = { selected = TString("top") }

          def right(): Unit = { selected = TString("right") }

        }

        case object font extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""
          val description: String =
            """Sets this color bar's title font. Note that the title's font used to be set by the now deprecated `titlefont` attribute."""
          val editType: String = """calc"""

          /* §#
           * generateColor(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val color: PColor = PColor(
            dflt = None,
            description = None
          )

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val family: PChars = PChars(
            dflt = None,
            description = Some(
              """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
            )
          )

          /* §#
           * generateNumberDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val size: PReal =
            PReal(min = Some(1.0), max = None, dflt = None, description = None)

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (color.json().length > 0) "\"color\"" + ":" + color.json()
              else ""
            val v1 =
              if (family.json().length > 0) "\"family\"" + ":" + family.json()
              else ""
            val v2 =
              if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
            val vars: List[String] = List(v0, v1, v2)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("color")
              in0.map(ve => color.fromJson(ve))

              val in1 = mapKV.get("family")
              in1.map(ve => family.fromJson(ve))

              val in2 = mapKV.get("size")
              in2.map(ve => size.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
          val v1 =
            if (text.json().length > 0) "\"text\"" + ":" + text.json() else ""
          val v2 =
            if (side.json().length > 0) "\"side\"" + ":" + side.json() else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("font")
            in0.map(ve => font.fromJson(ve))

            val in1 = mapKV.get("text")
            in1.map(ve => text.fromJson(ve))

            val in2 = mapKV.get("side")
            in2.map(ve => side.fromJson(ve))

          }
          this
        }

      }

      case object tickformatstops extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String = """object"""

        case object items extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          case object tickformatstop extends JSON {
            /* §#
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val role: String     = """object"""
            val editType: String = """calc"""

            object dtickrange extends JSON {
              /* §#
               * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val description: Option[String] = Some(
                """range [*min*, *max*], where *min*, *max* - dtick values which describe some zoom level, it is possible to omit *min* or *max* value by passing *null*"""
              )

              private var string: Array[String]           = Array.fill(2)("")
              private var boolean: Array[Boolean]         = Array.fill(2)(false)
              private var double: Array[Double]           = Array.fill(2)(0.0)
              private var int: Array[Int]                 = Array.fill(2)(0)
              private var selected: Map[Int, OfPrimitive] = Map()
              private val n: Integer                      = 2

              def json(): String = {
                /* §#
                 * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
                 * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                val nulls = "null"
                val values = (0 until n) map { i =>
                  val t = selected.get(i)
                  t.fold(nulls)({
                    case NAPrimitive =>
                      throw new RuntimeException(s"Unexpected type: $t")
                    case TString(_)  => string(i)
                    case TBoolean(_) => boolean(i).toString
                    case TDouble(_)  => double(i).toString
                    case TInt(_)     => int(i).toString
                    case TImpliedEdits(_) =>
                      throw new RuntimeException(s"Unexpected type: $t")
                    case TAttrRegexps(_) =>
                      throw new RuntimeException(s"Unexpected type: $t")
                  })
                }
                val allNull = values.forall(_.toLowerCase.equals(nulls))
                if (allNull)
                  ""
                else {
                  values.filter(_.length > 0).mkString("[", ",", "]")
                }
              }

              override def fromJson(in: Value): this.type = {
                /* §#
                 * generateInfoArrayFromJson()
                 * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                def err(j: ujson.Value): OfPrimitive =
                  throw new RuntimeException(s"""Parsing of $j failed: $in""")
                in match {
                  case Arr(values) =>
                    values.zipWithIndex.foreach { kv =>
                      val v = kv._1
                      val i = kv._2
                      v match {
                        case Bool(v) =>
                          boolean(i) = v; selected += (i -> TBoolean(v))
                        case Str(v) =>
                          string(i) = v; selected += (i -> TString(v))

                        case Num(v) =>
                          if (v.isValidInt) {
                            int(i) = v.toInt; selected += (i -> TInt(v.toInt))
                          } else {
                            double(i) = v; selected += (i -> TDouble(v))
                          }

                        case Null => () // nulls are valid
                        case _    => err(in)
                      }
                    }
                    this
                  case _ =>
                    err(in)
                    this
                }
              }

// Set the value
              def string_0(v: String): Unit = {
                string(0) = v; selected += (0 -> TString(v))
              }
// Delete the value (use default)
              def string_0(): Unit = { selected -= 0 }

// Set the value
              def boolean_0(v: Boolean): Unit = {
                boolean(0) = v; selected += (0 -> TBoolean(v))
              }
// Delete the value (use default)
              def boolean_0(): Unit = { selected -= 0 }

// Set the value
              def double_0(v: Double): Unit = {
                double(0) = v; selected += (0 -> TDouble(v))
              }
// Delete the value (use default)
              def double_0(): Unit = { selected -= 0 }

// Set the value
              def int_0(v: Int): Unit = {
                int(0) = v; selected += (0 -> TInt(v))
              }
// Delete the value (use default)
              def int_0(): Unit = { selected -= 0 }

// Set the value
              def string_1(v: String): Unit = {
                string(1) = v; selected += (1 -> TString(v))
              }
// Delete the value (use default)
              def string_1(): Unit = { selected -= 1 }

// Set the value
              def boolean_1(v: Boolean): Unit = {
                boolean(1) = v; selected += (1 -> TBoolean(v))
              }
// Delete the value (use default)
              def boolean_1(): Unit = { selected -= 1 }

// Set the value
              def double_1(v: Double): Unit = {
                double(1) = v; selected += (1 -> TDouble(v))
              }
// Delete the value (use default)
              def double_1(): Unit = { selected -= 1 }

// Set the value
              def int_1(v: Int): Unit = {
                int(1) = v; selected += (1 -> TInt(v))
              }
// Delete the value (use default)
              def int_1(): Unit = { selected -= 1 }

            }

            /* §#
             * generateStringDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val name: PChars = PChars(
              dflt = None,
              description = Some(
                """When used in a template, named items are created in the output figure in addition to any items the figure already has in this array. You can modify these items in the output figure by making your own item with `templateitemname` matching this `name` alongside your modifications (including `visible: false` or `enabled: false` to hide it). Has no effect outside of a template."""
              )
            )

            /* §#
             * generateBooleanDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val enabled: PBool = PBool(
              dflt = Some(true),
              description = Some(
                """Determines whether or not this stop is used. If `false`, this stop is ignored even within its `dtickrange`."""
              )
            )

            /* §#
             * generateStringDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val value: PChars = PChars(
              dflt = Some(""""""),
              description = Some(
                """string - dtickformat for described zoom level, the same as *tickformat*"""
              )
            )

            /* §#
             * generateStringDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val templateitemname: PChars = PChars(
              dflt = None,
              description = Some(
                """Used to refer to a named item in this array in the template. Named items from the template will be created even without a matching item in the input figure, but you can modify one by making an item with `templateitemname` matching its `name`, alongside your modifications (including `visible: false` or `enabled: false` to hide it). If there is no template or no matching item, this item will be hidden unless you explicitly show it with `visible: true`."""
              )
            )

            def json(): String = {
              /* §#
               * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/

              val v0 =
                if (dtickrange.json().length > 0)
                  "\"dtickrange\"" + ":" + dtickrange.json()
                else ""
              val v1 =
                if (name.json().length > 0) "\"name\"" + ":" + name.json()
                else ""
              val v2 =
                if (enabled.json().length > 0)
                  "\"enabled\"" + ":" + enabled.json()
                else ""
              val v3 =
                if (value.json().length > 0) "\"value\"" + ":" + value.json()
                else ""
              val v4 =
                if (templateitemname.json().length > 0)
                  "\"templateitemname\"" + ":" + templateitemname.json()
                else ""
              val vars: List[String] = List(v0, v1, v2, v3, v4)
              val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
              if (cleanVars.length > 0) {
                // values set
                "{" + cleanVars + "}"
              } else {
                // no values set
                ""
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err =
                throw new RuntimeException(s"Expected an Obj but got $in")
              in.objOpt.fold(err) { mapKV =>
                val in0 = mapKV.get("dtickrange")
                in0.map(ve => dtickrange.fromJson(ve))

                val in1 = mapKV.get("name")
                in1.map(ve => name.fromJson(ve))

                val in2 = mapKV.get("enabled")
                in2.map(ve => enabled.fromJson(ve))

                val in3 = mapKV.get("value")
                in3.map(ve => value.fromJson(ve))

                val in4 = mapKV.get("templateitemname")
                in4.map(ve => templateitemname.fromJson(ve))

              }
              this
            }

          }

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (tickformatstop.json().length > 0)
                "\"tickformatstop\"" + ":" + tickformatstop.json()
              else ""
            val vars: List[String] = List(v0)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("tickformatstop")
              in0.map(ve => tickformatstop.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (items.json().length > 0) "\"items\"" + ":" + items.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("items")
            in0.map(ve => items.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (tickfont.json().length > 0) "\"tickfont\"" + ":" + tickfont.json()
          else ""
        val v1 =
          if (title.json().length > 0) "\"title\"" + ":" + title.json() else ""
        val v2 =
          if (tickformatstops.json().length > 0)
            "\"tickformatstops\"" + ":" + tickformatstops.json()
          else ""
        val v3 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
        val v4 =
          if (tick0.json().length > 0) "\"tick0\"" + ":" + tick0.json() else ""
        val v5 =
          if (nticks.json().length > 0) "\"nticks\"" + ":" + nticks.json()
          else ""
        val v6 =
          if (tickangle.json().length > 0)
            "\"tickangle\"" + ":" + tickangle.json()
          else ""
        val v7 =
          if (outlinewidth.json().length > 0)
            "\"outlinewidth\"" + ":" + outlinewidth.json()
          else ""
        val v8 =
          if (bgcolor.json().length > 0) "\"bgcolor\"" + ":" + bgcolor.json()
          else ""
        val v9 =
          if (showexponent.json().length > 0)
            "\"showexponent\"" + ":" + showexponent.json()
          else ""
        val v10 =
          if (ypad.json().length > 0) "\"ypad\"" + ":" + ypad.json() else ""
        val v11 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
        val v12 =
          if (yanchor.json().length > 0) "\"yanchor\"" + ":" + yanchor.json()
          else ""
        val v13 =
          if (ticksuffix.json().length > 0)
            "\"ticksuffix\"" + ":" + ticksuffix.json()
          else ""
        val v14 =
          if (tickwidth.json().length > 0)
            "\"tickwidth\"" + ":" + tickwidth.json()
          else ""
        val v15 =
          if (tickvals.json().length > 0) "\"tickvals\"" + ":" + tickvals.json()
          else ""
        val v16 =
          if (bordercolor.json().length > 0)
            "\"bordercolor\"" + ":" + bordercolor.json()
          else ""
        val v17 =
          if (thickness.json().length > 0)
            "\"thickness\"" + ":" + thickness.json()
          else ""
        val v18 =
          if (ticklen.json().length > 0) "\"ticklen\"" + ":" + ticklen.json()
          else ""
        val v19 =
          if (tickmode.json().length > 0) "\"tickmode\"" + ":" + tickmode.json()
          else ""
        val v20 =
          if (dtick.json().length > 0) "\"dtick\"" + ":" + dtick.json() else ""
        val v21 =
          if (lenmode.json().length > 0) "\"lenmode\"" + ":" + lenmode.json()
          else ""
        val v22 =
          if (tickvalssrc.json().length > 0)
            "\"tickvalssrc\"" + ":" + tickvalssrc.json()
          else ""
        val v23 =
          if (showticksuffix.json().length > 0)
            "\"showticksuffix\"" + ":" + showticksuffix.json()
          else ""
        val v24 =
          if (xpad.json().length > 0) "\"xpad\"" + ":" + xpad.json() else ""
        val v25 =
          if (ticktext.json().length > 0) "\"ticktext\"" + ":" + ticktext.json()
          else ""
        val v26 =
          if (ticks.json().length > 0) "\"ticks\"" + ":" + ticks.json() else ""
        val v27 =
          if (tickcolor.json().length > 0)
            "\"tickcolor\"" + ":" + tickcolor.json()
          else ""
        val v28 =
          if (ticktextsrc.json().length > 0)
            "\"ticktextsrc\"" + ":" + ticktextsrc.json()
          else ""
        val v29 =
          if (outlinecolor.json().length > 0)
            "\"outlinecolor\"" + ":" + outlinecolor.json()
          else ""
        val v30 =
          if (exponentformat.json().length > 0)
            "\"exponentformat\"" + ":" + exponentformat.json()
          else ""
        val v31 =
          if (showtickprefix.json().length > 0)
            "\"showtickprefix\"" + ":" + showtickprefix.json()
          else ""
        val v32 =
          if (tickprefix.json().length > 0)
            "\"tickprefix\"" + ":" + tickprefix.json()
          else ""
        val v33 =
          if (xanchor.json().length > 0) "\"xanchor\"" + ":" + xanchor.json()
          else ""
        val v34 =
          if (thicknessmode.json().length > 0)
            "\"thicknessmode\"" + ":" + thicknessmode.json()
          else ""
        val v35 =
          if (separatethousands.json().length > 0)
            "\"separatethousands\"" + ":" + separatethousands.json()
          else ""
        val v36 =
          if (tickformat.json().length > 0)
            "\"tickformat\"" + ":" + tickformat.json()
          else ""
        val v37 =
          if (showticklabels.json().length > 0)
            "\"showticklabels\"" + ":" + showticklabels.json()
          else ""
        val v38 =
          if (len.json().length > 0) "\"len\"" + ":" + len.json() else ""
        val v39 =
          if (borderwidth.json().length > 0)
            "\"borderwidth\"" + ":" + borderwidth.json()
          else ""
        val vars: List[String] = List(
          v0,
          v1,
          v2,
          v3,
          v4,
          v5,
          v6,
          v7,
          v8,
          v9,
          v10,
          v11,
          v12,
          v13,
          v14,
          v15,
          v16,
          v17,
          v18,
          v19,
          v20,
          v21,
          v22,
          v23,
          v24,
          v25,
          v26,
          v27,
          v28,
          v29,
          v30,
          v31,
          v32,
          v33,
          v34,
          v35,
          v36,
          v37,
          v38,
          v39
        )
        val cleanVars = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("tickfont")
          in0.map(ve => tickfont.fromJson(ve))

          val in1 = mapKV.get("title")
          in1.map(ve => title.fromJson(ve))

          val in2 = mapKV.get("tickformatstops")
          in2.map(ve => tickformatstops.fromJson(ve))

          val in3 = mapKV.get("x")
          in3.map(ve => x.fromJson(ve))

          val in4 = mapKV.get("tick0")
          in4.map(ve => tick0.fromJson(ve))

          val in5 = mapKV.get("nticks")
          in5.map(ve => nticks.fromJson(ve))

          val in6 = mapKV.get("tickangle")
          in6.map(ve => tickangle.fromJson(ve))

          val in7 = mapKV.get("outlinewidth")
          in7.map(ve => outlinewidth.fromJson(ve))

          val in8 = mapKV.get("bgcolor")
          in8.map(ve => bgcolor.fromJson(ve))

          val in9 = mapKV.get("showexponent")
          in9.map(ve => showexponent.fromJson(ve))

          val in10 = mapKV.get("ypad")
          in10.map(ve => ypad.fromJson(ve))

          val in11 = mapKV.get("y")
          in11.map(ve => y.fromJson(ve))

          val in12 = mapKV.get("yanchor")
          in12.map(ve => yanchor.fromJson(ve))

          val in13 = mapKV.get("ticksuffix")
          in13.map(ve => ticksuffix.fromJson(ve))

          val in14 = mapKV.get("tickwidth")
          in14.map(ve => tickwidth.fromJson(ve))

          val in15 = mapKV.get("tickvals")
          in15.map(ve => tickvals.fromJson(ve))

          val in16 = mapKV.get("bordercolor")
          in16.map(ve => bordercolor.fromJson(ve))

          val in17 = mapKV.get("thickness")
          in17.map(ve => thickness.fromJson(ve))

          val in18 = mapKV.get("ticklen")
          in18.map(ve => ticklen.fromJson(ve))

          val in19 = mapKV.get("tickmode")
          in19.map(ve => tickmode.fromJson(ve))

          val in20 = mapKV.get("dtick")
          in20.map(ve => dtick.fromJson(ve))

          val in21 = mapKV.get("lenmode")
          in21.map(ve => lenmode.fromJson(ve))

          val in22 = mapKV.get("tickvalssrc")
          in22.map(ve => tickvalssrc.fromJson(ve))

          val in23 = mapKV.get("showticksuffix")
          in23.map(ve => showticksuffix.fromJson(ve))

          val in24 = mapKV.get("xpad")
          in24.map(ve => xpad.fromJson(ve))

          val in25 = mapKV.get("ticktext")
          in25.map(ve => ticktext.fromJson(ve))

          val in26 = mapKV.get("ticks")
          in26.map(ve => ticks.fromJson(ve))

          val in27 = mapKV.get("tickcolor")
          in27.map(ve => tickcolor.fromJson(ve))

          val in28 = mapKV.get("ticktextsrc")
          in28.map(ve => ticktextsrc.fromJson(ve))

          val in29 = mapKV.get("outlinecolor")
          in29.map(ve => outlinecolor.fromJson(ve))

          val in30 = mapKV.get("exponentformat")
          in30.map(ve => exponentformat.fromJson(ve))

          val in31 = mapKV.get("showtickprefix")
          in31.map(ve => showtickprefix.fromJson(ve))

          val in32 = mapKV.get("tickprefix")
          in32.map(ve => tickprefix.fromJson(ve))

          val in33 = mapKV.get("xanchor")
          in33.map(ve => xanchor.fromJson(ve))

          val in34 = mapKV.get("thicknessmode")
          in34.map(ve => thicknessmode.fromJson(ve))

          val in35 = mapKV.get("separatethousands")
          in35.map(ve => separatethousands.fromJson(ve))

          val in36 = mapKV.get("tickformat")
          in36.map(ve => tickformat.fromJson(ve))

          val in37 = mapKV.get("showticklabels")
          in37.map(ve => showticklabels.fromJson(ve))

          val in38 = mapKV.get("len")
          in38.map(ve => len.fromJson(ve))

          val in39 = mapKV.get("borderwidth")
          in39.map(ve => borderwidth.fromJson(ve))

        }
        this
      }

    }

    case object contour extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = Some("""#444"""),
        description = Some("""Sets the color of the contour lines.""")
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val width: PReal = PReal(
        min = Some(1.0),
        max = Some(16.0),
        dflt = Some("""2.0"""),
        description = Some("""Sets the width of the contour lines.""")
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val show: PBool = PBool(
        dflt = Some(false),
        description =
          Some("""Sets whether or not dynamic contours are shown on hover""")
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v1 =
          if (width.json().length > 0) "\"width\"" + ":" + width.json() else ""
        val v2 =
          if (show.json().length > 0) "\"show\"" + ":" + show.json() else ""
        val vars: List[String] = List(v0, v1, v2)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("color")
          in0.map(ve => color.fromJson(ve))

          val in1 = mapKV.get("width")
          in1.map(ve => width.fromJson(ve))

          val in2 = mapKV.get("show")
          in2.map(ve => show.fromJson(ve))

        }
        this
      }

    }

    case object stream extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val maxpoints: PReal = PReal(
        min = Some(0.0),
        max = Some(10000.0),
        dflt = Some("""500.0"""),
        description = Some(
          """Sets the maximum number of points to keep on the plots from an incoming stream. If `maxpoints` is set to *50*, only the newest 50 points will be displayed on the plot."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val token: PChars = PChars(
        dflt = None,
        description = Some(
          """The stream id number links a data trace on a plot with a stream. See https://plot.ly/settings for more details."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (maxpoints.json().length > 0)
            "\"maxpoints\"" + ":" + maxpoints.json()
          else ""
        val v1 =
          if (token.json().length > 0) "\"token\"" + ":" + token.json() else ""
        val vars: List[String] = List(v0, v1)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("maxpoints")
          in0.map(ve => maxpoints.fromJson(ve))

          val in1 = mapKV.get("token")
          in1.map(ve => token.fromJson(ve))

        }
        this
      }

    }

    case object hoverlabel extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """none"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val alignsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  align .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolor: PColor = PColor(
        dflt = None,
        description = Some(
          """Sets the background color of the hover labels for this trace"""
        )
      )

      object align extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """auto"""
        val description: Option[String] = Some(
          """Sets the horizontal alignment of the text content within hover label box. Has an effect only if the hover label text spans more two or more lines"""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def auto(): Unit = { selected = TString("auto") }

        def right(): Unit = { selected = TString("right") }

        def left(): Unit = { selected = TString("left") }

      }

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelength: PInt = PInt(
        min = Some(-1),
        max = None,
        dflt = Some(15),
        description = Some(
          """Sets the default length (in number of characters) of the trace name in the hover labels for all traces. -1 shows the whole name regardless of length. 0-3 shows the first 0-3 characters, and an integer >3 will show the whole name if it is less than that many characters, but if it is longer, will truncate to `namelength - 3` characters and add an ellipsis."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelengthsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  namelength .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolor: PColor = PColor(
        dflt = None,
        description =
          Some("""Sets the border color of the hover labels for this trace.""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bordercolor .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bgcolor .""")
      )

      case object font extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Sets the font used in hover labels."""
        val editType: String    = """none"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val familysrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  family .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val colorsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  color .""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val sizesrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  size .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val v1 =
            if (familysrc.json().length > 0)
              "\"familysrc\"" + ":" + familysrc.json()
            else ""
          val v2 =
            if (colorsrc.json().length > 0)
              "\"colorsrc\"" + ":" + colorsrc.json()
            else ""
          val v3 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v4 =
            if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
            else ""
          val v5 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("size")
            in0.map(ve => size.fromJson(ve))

            val in1 = mapKV.get("familysrc")
            in1.map(ve => familysrc.fromJson(ve))

            val in2 = mapKV.get("colorsrc")
            in2.map(ve => colorsrc.fromJson(ve))

            val in3 = mapKV.get("color")
            in3.map(ve => color.fromJson(ve))

            val in4 = mapKV.get("sizesrc")
            in4.map(ve => sizesrc.fromJson(ve))

            val in5 = mapKV.get("family")
            in5.map(ve => family.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
        val v1 =
          if (alignsrc.json().length > 0) "\"alignsrc\"" + ":" + alignsrc.json()
          else ""
        val v2 =
          if (bgcolor.json().length > 0) "\"bgcolor\"" + ":" + bgcolor.json()
          else ""
        val v3 =
          if (align.json().length > 0) "\"align\"" + ":" + align.json() else ""
        val v4 =
          if (namelength.json().length > 0)
            "\"namelength\"" + ":" + namelength.json()
          else ""
        val v5 =
          if (namelengthsrc.json().length > 0)
            "\"namelengthsrc\"" + ":" + namelengthsrc.json()
          else ""
        val v6 =
          if (bordercolor.json().length > 0)
            "\"bordercolor\"" + ":" + bordercolor.json()
          else ""
        val v7 =
          if (bordercolorsrc.json().length > 0)
            "\"bordercolorsrc\"" + ":" + bordercolorsrc.json()
          else ""
        val v8 =
          if (bgcolorsrc.json().length > 0)
            "\"bgcolorsrc\"" + ":" + bgcolorsrc.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6, v7, v8)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("font")
          in0.map(ve => font.fromJson(ve))

          val in1 = mapKV.get("alignsrc")
          in1.map(ve => alignsrc.fromJson(ve))

          val in2 = mapKV.get("bgcolor")
          in2.map(ve => bgcolor.fromJson(ve))

          val in3 = mapKV.get("align")
          in3.map(ve => align.fromJson(ve))

          val in4 = mapKV.get("namelength")
          in4.map(ve => namelength.fromJson(ve))

          val in5 = mapKV.get("namelengthsrc")
          in5.map(ve => namelengthsrc.fromJson(ve))

          val in6 = mapKV.get("bordercolor")
          in6.map(ve => bordercolor.fromJson(ve))

          val in7 = mapKV.get("bordercolorsrc")
          in7.map(ve => bordercolorsrc.fromJson(ve))

          val in8 = mapKV.get("bgcolorsrc")
          in8.map(ve => bgcolorsrc.fromJson(ve))

        }
        this
      }

    }

    case object slices extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      case object z extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """calc"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val fill: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the fill ratio of the `slices`. The default fill value of the `slices` is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
          )
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val show: PBool = PBool(
          dflt = Some(false),
          description = Some(
            """Determines whether or not slice planes about the z dimension are drawn."""
          )
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val locationssrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  locations .""")
        )

        /* §#
         * generateDataArrayDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val locations: DataArray = DataArray(
          dflt = Some(Array()),
          description = Some(
            """Specifies the location(s) of slices on the axis. When not specified slices would be created for all points of the axis z except start and end."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (fill.json().length > 0) "\"fill\"" + ":" + fill.json() else ""
          val v1 =
            if (show.json().length > 0) "\"show\"" + ":" + show.json() else ""
          val v2 =
            if (locationssrc.json().length > 0)
              "\"locationssrc\"" + ":" + locationssrc.json()
            else ""
          val v3 =
            if (locations.json().length > 0)
              "\"locations\"" + ":" + locations.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("fill")
            in0.map(ve => fill.fromJson(ve))

            val in1 = mapKV.get("show")
            in1.map(ve => show.fromJson(ve))

            val in2 = mapKV.get("locationssrc")
            in2.map(ve => locationssrc.fromJson(ve))

            val in3 = mapKV.get("locations")
            in3.map(ve => locations.fromJson(ve))

          }
          this
        }

      }

      case object y extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """calc"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val fill: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the fill ratio of the `slices`. The default fill value of the `slices` is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
          )
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val show: PBool = PBool(
          dflt = Some(false),
          description = Some(
            """Determines whether or not slice planes about the y dimension are drawn."""
          )
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val locationssrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  locations .""")
        )

        /* §#
         * generateDataArrayDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val locations: DataArray = DataArray(
          dflt = Some(Array()),
          description = Some(
            """Specifies the location(s) of slices on the axis. When not specified slices would be created for all points of the axis y except start and end."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (fill.json().length > 0) "\"fill\"" + ":" + fill.json() else ""
          val v1 =
            if (show.json().length > 0) "\"show\"" + ":" + show.json() else ""
          val v2 =
            if (locationssrc.json().length > 0)
              "\"locationssrc\"" + ":" + locationssrc.json()
            else ""
          val v3 =
            if (locations.json().length > 0)
              "\"locations\"" + ":" + locations.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("fill")
            in0.map(ve => fill.fromJson(ve))

            val in1 = mapKV.get("show")
            in1.map(ve => show.fromJson(ve))

            val in2 = mapKV.get("locationssrc")
            in2.map(ve => locationssrc.fromJson(ve))

            val in3 = mapKV.get("locations")
            in3.map(ve => locations.fromJson(ve))

          }
          this
        }

      }

      case object x extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """calc"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val fill: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the fill ratio of the `slices`. The default fill value of the `slices` is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
          )
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val show: PBool = PBool(
          dflt = Some(false),
          description = Some(
            """Determines whether or not slice planes about the x dimension are drawn."""
          )
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val locationssrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  locations .""")
        )

        /* §#
         * generateDataArrayDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val locations: DataArray = DataArray(
          dflt = Some(Array()),
          description = Some(
            """Specifies the location(s) of slices on the axis. When not specified slices would be created for all points of the axis x except start and end."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (fill.json().length > 0) "\"fill\"" + ":" + fill.json() else ""
          val v1 =
            if (show.json().length > 0) "\"show\"" + ":" + show.json() else ""
          val v2 =
            if (locationssrc.json().length > 0)
              "\"locationssrc\"" + ":" + locationssrc.json()
            else ""
          val v3 =
            if (locations.json().length > 0)
              "\"locations\"" + ":" + locations.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("fill")
            in0.map(ve => fill.fromJson(ve))

            val in1 = mapKV.get("show")
            in1.map(ve => show.fromJson(ve))

            val in2 = mapKV.get("locationssrc")
            in2.map(ve => locationssrc.fromJson(ve))

            val in3 = mapKV.get("locations")
            in3.map(ve => locations.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0                 = if (z.json().length > 0) "\"z\"" + ":" + z.json() else ""
        val v1                 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
        val v2                 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
        val vars: List[String] = List(v0, v1, v2)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("z")
          in0.map(ve => z.fromJson(ve))

          val in1 = mapKV.get("y")
          in1.map(ve => y.fromJson(ve))

          val in2 = mapKV.get("x")
          in2.map(ve => x.fromJson(ve))

        }
        this
      }

    }

    case object caps extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      case object z extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """calc"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val fill: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the fill ratio of the `caps`. The default fill value of the `caps` is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
          )
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val show: PBool = PBool(
          dflt = Some(true),
          description = Some(
            """Sets the fill ratio of the `slices`. The default fill value of the z `slices` is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (fill.json().length > 0) "\"fill\"" + ":" + fill.json() else ""
          val v1 =
            if (show.json().length > 0) "\"show\"" + ":" + show.json() else ""
          val vars: List[String] = List(v0, v1)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("fill")
            in0.map(ve => fill.fromJson(ve))

            val in1 = mapKV.get("show")
            in1.map(ve => show.fromJson(ve))

          }
          this
        }

      }

      case object y extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """calc"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val fill: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the fill ratio of the `caps`. The default fill value of the `caps` is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
          )
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val show: PBool = PBool(
          dflt = Some(true),
          description = Some(
            """Sets the fill ratio of the `slices`. The default fill value of the y `slices` is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (fill.json().length > 0) "\"fill\"" + ":" + fill.json() else ""
          val v1 =
            if (show.json().length > 0) "\"show\"" + ":" + show.json() else ""
          val vars: List[String] = List(v0, v1)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("fill")
            in0.map(ve => fill.fromJson(ve))

            val in1 = mapKV.get("show")
            in1.map(ve => show.fromJson(ve))

          }
          this
        }

      }

      case object x extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """calc"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val fill: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the fill ratio of the `caps`. The default fill value of the `caps` is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
          )
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val show: PBool = PBool(
          dflt = Some(true),
          description = Some(
            """Sets the fill ratio of the `slices`. The default fill value of the x `slices` is 1 meaning that they are entirely shaded. On the other hand Applying a `fill` ratio less than one would allow the creation of openings parallel to the edges."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (fill.json().length > 0) "\"fill\"" + ":" + fill.json() else ""
          val v1 =
            if (show.json().length > 0) "\"show\"" + ":" + show.json() else ""
          val vars: List[String] = List(v0, v1)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("fill")
            in0.map(ve => fill.fromJson(ve))

            val in1 = mapKV.get("show")
            in1.map(ve => show.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0                 = if (z.json().length > 0) "\"z\"" + ":" + z.json() else ""
        val v1                 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
        val v2                 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
        val vars: List[String] = List(v0, v1, v2)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("z")
          in0.map(ve => z.fromJson(ve))

          val in1 = mapKV.get("y")
          in1.map(ve => y.fromJson(ve))

          val in2 = mapKV.get("x")
          in2.map(ve => x.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val v52 = "\"type\" : \"isosurface\""
      val v0 =
        if (surface.json().length > 0) "\"surface\"" + ":" + surface.json()
        else ""
      val v1 =
        if (lightposition.json().length > 0)
          "\"lightposition\"" + ":" + lightposition.json()
        else ""
      val v2 =
        if (lighting.json().length > 0) "\"lighting\"" + ":" + lighting.json()
        else ""
      val v3 =
        if (spaceframe.json().length > 0)
          "\"spaceframe\"" + ":" + spaceframe.json()
        else ""
      val v4 =
        if (colorbar.json().length > 0) "\"colorbar\"" + ":" + colorbar.json()
        else ""
      val v5 =
        if (contour.json().length > 0) "\"contour\"" + ":" + contour.json()
        else ""
      val v6 =
        if (stream.json().length > 0) "\"stream\"" + ":" + stream.json() else ""
      val v7 =
        if (hoverlabel.json().length > 0)
          "\"hoverlabel\"" + ":" + hoverlabel.json()
        else ""
      val v8 =
        if (slices.json().length > 0) "\"slices\"" + ":" + slices.json() else ""
      val v9 =
        if (caps.json().length > 0) "\"caps\"" + ":" + caps.json() else ""
      val v10 =
        if (hovertemplate.json().length > 0)
          "\"hovertemplate\"" + ":" + hovertemplate.json()
        else ""
      val v11 =
        if (xsrc.json().length > 0) "\"xsrc\"" + ":" + xsrc.json() else ""
      val v12 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
      val v13 =
        if (name.json().length > 0) "\"name\"" + ":" + name.json() else ""
      val v14 =
        if (cmax.json().length > 0) "\"cmax\"" + ":" + cmax.json() else ""
      val v15 =
        if (hovertemplatesrc.json().length > 0)
          "\"hovertemplatesrc\"" + ":" + hovertemplatesrc.json()
        else ""
      val v16 =
        if (isomin.json().length > 0) "\"isomin\"" + ":" + isomin.json() else ""
      val v17 =
        if (hoverinfo.json().length > 0)
          "\"hoverinfo\"" + ":" + hoverinfo.json()
        else ""
      val v18 =
        if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
        else ""
      val v19 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
      val v20 =
        if (cmin.json().length > 0) "\"cmin\"" + ":" + cmin.json() else ""
      val v21 =
        if (uirevision.json().length > 0)
          "\"uirevision\"" + ":" + uirevision.json()
        else ""
      val v22 =
        if (autocolorscale.json().length > 0)
          "\"autocolorscale\"" + ":" + autocolorscale.json()
        else ""
      val v23 =
        if (reversescale.json().length > 0)
          "\"reversescale\"" + ":" + reversescale.json()
        else ""
      val v24 =
        if (ysrc.json().length > 0) "\"ysrc\"" + ":" + ysrc.json() else ""
      val v25 =
        if (idssrc.json().length > 0) "\"idssrc\"" + ":" + idssrc.json() else ""
      val v26 =
        if (text.json().length > 0) "\"text\"" + ":" + text.json() else ""
      val v27 =
        if (showscale.json().length > 0)
          "\"showscale\"" + ":" + showscale.json()
        else ""
      val v28 =
        if (coloraxis.json().length > 0)
          "\"coloraxis\"" + ":" + coloraxis.json()
        else ""
      val v29 =
        if (hoverinfosrc.json().length > 0)
          "\"hoverinfosrc\"" + ":" + hoverinfosrc.json()
        else ""
      val v30 =
        if (hovertextsrc.json().length > 0)
          "\"hovertextsrc\"" + ":" + hovertextsrc.json()
        else ""
      val v31 =
        if (meta.json().length > 0) "\"meta\"" + ":" + meta.json() else ""
      val v32 =
        if (textsrc.json().length > 0) "\"textsrc\"" + ":" + textsrc.json()
        else ""
      val v33 =
        if (scene.json().length > 0) "\"scene\"" + ":" + scene.json() else ""
      val v34 =
        if (hovertext.json().length > 0)
          "\"hovertext\"" + ":" + hovertext.json()
        else ""
      val v35 = if (uid.json().length > 0) "\"uid\"" + ":" + uid.json() else ""
      val v36 =
        if (legendgroup.json().length > 0)
          "\"legendgroup\"" + ":" + legendgroup.json()
        else ""
      val v37 =
        if (isomax.json().length > 0) "\"isomax\"" + ":" + isomax.json() else ""
      val v38 =
        if (zsrc.json().length > 0) "\"zsrc\"" + ":" + zsrc.json() else ""
      val v39 =
        if (opacity.json().length > 0) "\"opacity\"" + ":" + opacity.json()
        else ""
      val v40 =
        if (cmid.json().length > 0) "\"cmid\"" + ":" + cmid.json() else ""
      val v41 = if (ids.json().length > 0) "\"ids\"" + ":" + ids.json() else ""
      val v42 =
        if (colorscale.json().length > 0)
          "\"colorscale\"" + ":" + colorscale.json()
        else ""
      val v43 =
        if (metasrc.json().length > 0) "\"metasrc\"" + ":" + metasrc.json()
        else ""
      val v44 =
        if (customdata.json().length > 0)
          "\"customdata\"" + ":" + customdata.json()
        else ""
      val v45 =
        if (value.json().length > 0) "\"value\"" + ":" + value.json() else ""
      val v46 =
        if (showlegend.json().length > 0)
          "\"showlegend\"" + ":" + showlegend.json()
        else ""
      val v47 =
        if (valuesrc.json().length > 0) "\"valuesrc\"" + ":" + valuesrc.json()
        else ""
      val v48 =
        if (cauto.json().length > 0) "\"cauto\"" + ":" + cauto.json() else ""
      val v49 =
        if (flatshading.json().length > 0)
          "\"flatshading\"" + ":" + flatshading.json()
        else ""
      val v50 = if (z.json().length > 0) "\"z\"" + ":" + z.json() else ""
      val v51 =
        if (customdatasrc.json().length > 0)
          "\"customdatasrc\"" + ":" + customdatasrc.json()
        else ""
      val vars: List[String] = List(
        v52,
        v0,
        v1,
        v2,
        v3,
        v4,
        v5,
        v6,
        v7,
        v8,
        v9,
        v10,
        v11,
        v12,
        v13,
        v14,
        v15,
        v16,
        v17,
        v18,
        v19,
        v20,
        v21,
        v22,
        v23,
        v24,
        v25,
        v26,
        v27,
        v28,
        v29,
        v30,
        v31,
        v32,
        v33,
        v34,
        v35,
        v36,
        v37,
        v38,
        v39,
        v40,
        v41,
        v42,
        v43,
        v44,
        v45,
        v46,
        v47,
        v48,
        v49,
        v50,
        v51
      )
      val cleanVars = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("surface")
        in0.map(ve => surface.fromJson(ve))

        val in1 = mapKV.get("lightposition")
        in1.map(ve => lightposition.fromJson(ve))

        val in2 = mapKV.get("lighting")
        in2.map(ve => lighting.fromJson(ve))

        val in3 = mapKV.get("spaceframe")
        in3.map(ve => spaceframe.fromJson(ve))

        val in4 = mapKV.get("colorbar")
        in4.map(ve => colorbar.fromJson(ve))

        val in5 = mapKV.get("contour")
        in5.map(ve => contour.fromJson(ve))

        val in6 = mapKV.get("stream")
        in6.map(ve => stream.fromJson(ve))

        val in7 = mapKV.get("hoverlabel")
        in7.map(ve => hoverlabel.fromJson(ve))

        val in8 = mapKV.get("slices")
        in8.map(ve => slices.fromJson(ve))

        val in9 = mapKV.get("caps")
        in9.map(ve => caps.fromJson(ve))

        val in10 = mapKV.get("hovertemplate")
        in10.map(ve => hovertemplate.fromJson(ve))

        val in11 = mapKV.get("xsrc")
        in11.map(ve => xsrc.fromJson(ve))

        val in12 = mapKV.get("x")
        in12.map(ve => x.fromJson(ve))

        val in13 = mapKV.get("name")
        in13.map(ve => name.fromJson(ve))

        val in14 = mapKV.get("cmax")
        in14.map(ve => cmax.fromJson(ve))

        val in15 = mapKV.get("hovertemplatesrc")
        in15.map(ve => hovertemplatesrc.fromJson(ve))

        val in16 = mapKV.get("isomin")
        in16.map(ve => isomin.fromJson(ve))

        val in17 = mapKV.get("hoverinfo")
        in17.map(ve => hoverinfo.fromJson(ve))

        val in18 = mapKV.get("visible")
        in18.map(ve => visible.fromJson(ve))

        val in19 = mapKV.get("y")
        in19.map(ve => y.fromJson(ve))

        val in20 = mapKV.get("cmin")
        in20.map(ve => cmin.fromJson(ve))

        val in21 = mapKV.get("uirevision")
        in21.map(ve => uirevision.fromJson(ve))

        val in22 = mapKV.get("autocolorscale")
        in22.map(ve => autocolorscale.fromJson(ve))

        val in23 = mapKV.get("reversescale")
        in23.map(ve => reversescale.fromJson(ve))

        val in24 = mapKV.get("ysrc")
        in24.map(ve => ysrc.fromJson(ve))

        val in25 = mapKV.get("idssrc")
        in25.map(ve => idssrc.fromJson(ve))

        val in26 = mapKV.get("text")
        in26.map(ve => text.fromJson(ve))

        val in27 = mapKV.get("showscale")
        in27.map(ve => showscale.fromJson(ve))

        val in28 = mapKV.get("coloraxis")
        in28.map(ve => coloraxis.fromJson(ve))

        val in29 = mapKV.get("hoverinfosrc")
        in29.map(ve => hoverinfosrc.fromJson(ve))

        val in30 = mapKV.get("hovertextsrc")
        in30.map(ve => hovertextsrc.fromJson(ve))

        val in31 = mapKV.get("meta")
        in31.map(ve => meta.fromJson(ve))

        val in32 = mapKV.get("textsrc")
        in32.map(ve => textsrc.fromJson(ve))

        val in33 = mapKV.get("scene")
        in33.map(ve => scene.fromJson(ve))

        val in34 = mapKV.get("hovertext")
        in34.map(ve => hovertext.fromJson(ve))

        val in35 = mapKV.get("uid")
        in35.map(ve => uid.fromJson(ve))

        val in36 = mapKV.get("legendgroup")
        in36.map(ve => legendgroup.fromJson(ve))

        val in37 = mapKV.get("isomax")
        in37.map(ve => isomax.fromJson(ve))

        val in38 = mapKV.get("zsrc")
        in38.map(ve => zsrc.fromJson(ve))

        val in39 = mapKV.get("opacity")
        in39.map(ve => opacity.fromJson(ve))

        val in40 = mapKV.get("cmid")
        in40.map(ve => cmid.fromJson(ve))

        val in41 = mapKV.get("ids")
        in41.map(ve => ids.fromJson(ve))

        val in42 = mapKV.get("colorscale")
        in42.map(ve => colorscale.fromJson(ve))

        val in43 = mapKV.get("metasrc")
        in43.map(ve => metasrc.fromJson(ve))

        val in44 = mapKV.get("customdata")
        in44.map(ve => customdata.fromJson(ve))

        val in45 = mapKV.get("value")
        in45.map(ve => value.fromJson(ve))

        val in46 = mapKV.get("showlegend")
        in46.map(ve => showlegend.fromJson(ve))

        val in47 = mapKV.get("valuesrc")
        in47.map(ve => valuesrc.fromJson(ve))

        val in48 = mapKV.get("cauto")
        in48.map(ve => cauto.fromJson(ve))

        val in49 = mapKV.get("flatshading")
        in49.map(ve => flatshading.fromJson(ve))

        val in50 = mapKV.get("z")
        in50.map(ve => z.fromJson(ve))

        val in51 = mapKV.get("customdatasrc")
        in51.map(ve => customdatasrc.fromJson(ve))

      }
      this
    }

  }

  object layoutAttributes extends JSON {
    def json(): String                 = ""
    def fromJson(in: Value): this.type = this
  }

}
