package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Treemap() extends Trace {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val type_ : String      = """treemap"""
  val animatable: Boolean = true

  case object layoutAttributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val extendtreemapcolors: PBool = PBool(
      dflt = Some(true),
      description = Some(
        """If `true`, the treemap slice colors (whether given by `treemapcolorway` or inherited from `colorway`) will be extended to three times its original length by first repeating every color 20% lighter then each color 20% darker. This is intended to reduce the likelihood of reusing the same color when you have many slices, but you can set `false` to disable. Colors provided in the trace, using `marker.colors`, are never extended."""
      )
    )

    /* §#
     * generateColorListDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val treemapcolorway: PColorList = PColorList(
      dflt = None,
      description = Some(
        """Sets the default treemap slice colors. Defaults to the main `colorway` used for trace colors. If you specify a new list here it can still be extended with lighter and darker colors, see `extendtreemapcolors`."""
      )
    )

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (extendtreemapcolors.json().length > 0)
          "\"extendtreemapcolors\"" + ":" + extendtreemapcolors.json()
        else ""
      val v1 =
        if (treemapcolorway.json().length > 0)
          "\"treemapcolorway\"" + ":" + treemapcolorway.json()
        else ""
      val vars: List[String] = List(v0, v1)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("extendtreemapcolors")
        in0.map(ve => extendtreemapcolors.fromJson(ve))

        val in1 = mapKV.get("treemapcolorway")
        in1.map(ve => treemapcolorway.fromJson(ve))

      }
      this
    }

  }

  case object meta extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val description: String =
      """Visualize hierarchal data from leaves (and/or outer branches) towards root with rectangles. The treemap sectors are determined by the entries in *labels* or *ids* and in *parents*."""

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val vars: List[String] = List()
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV => }
      this
    }

  }

  case object attributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val type_ : String = """treemap"""

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplate: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Template string used for rendering the information that appear on hover box. Note that this will override `hoverinfo`. Variables are inserted using %{variable}, for example "y: %{y}". Numbers are formatted using d3-format's syntax %{variable:d3-format}, for example "Price: %{y:$.2f}". https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format for details on the formatting syntax. Dates are formatted using d3-time-format's syntax %{variable|d3-time-format}, for example "Day: %{2019-01-01|%A}". https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format for details on the date formatting syntax. The variables available in `hovertemplate` are the ones emitted as event data described at this link https://plot.ly/javascript/plotlyjs-events/#event-data. Additionally, every attributes that can be specified per-point (the ones that are `arrayOk: true`) are available. variables `currentPath`, `root`, `entry`, `percentRoot`, `percentEntry` and `percentParent`. Anything contained in tag `<extra>` is displayed in the secondary box, for example "<extra>{fullData.name}</extra>". To hide the secondary box completely, use an empty tag `<extra></extra>`."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val name: PChars = PChars(
      dflt = None,
      description = Some(
        """Sets the trace name. The trace name appear as the legend item and on hover."""
      )
    )

    object count extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = Some("""leaves""")
      val description: Option[String] = Some(
        """Determines default for `values` when it is not provided, by inferring a 1 for each of the *leaves* and/or *branches*, otherwise 0."""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def leavesOn(): Unit    = { flag_ + "leaves" }
      def leavesOff(): Unit   = { flag_ - "leaves" }
      def branchesOn(): Unit  = { flag_ + "branches" }
      def branchesOff(): Unit = { flag_ - "branches" }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplatesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertemplate .""")
    )

    object hoverinfo extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = Some("""label+text+value+name""")
      val description: Option[String] = Some(
        """Determines which trace information appear on hover. If `none` or `skip` are set, no information is displayed upon hovering. But, if `none` is set, click and hover events are still fired."""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def percent_parentOn(): Unit  = { flag_ + "percent parent" }
      def percent_parentOff(): Unit = { flag_ - "percent parent" }
      def percent_entryOn(): Unit   = { flag_ + "percent entry" }
      def percent_entryOff(): Unit  = { flag_ - "percent entry" }
      def percent_rootOn(): Unit    = { flag_ + "percent root" }
      def percent_rootOff(): Unit   = { flag_ - "percent root" }
      def current_pathOn(): Unit    = { flag_ + "current path" }
      def current_pathOff(): Unit   = { flag_ - "current path" }
      def nameOn(): Unit            = { flag_ + "name" }
      def nameOff(): Unit           = { flag_ - "name" }
      def valueOn(): Unit           = { flag_ + "value" }
      def valueOff(): Unit          = { flag_ - "value" }
      def textOn(): Unit            = { flag_ + "text" }
      def textOff(): Unit           = { flag_ - "text" }
      def labelOn(): Unit           = { flag_ + "label" }
      def labelOff(): Unit          = { flag_ - "label" }
      def skipOn(): Unit            = { flag_ + "skip" }
      def skipOff(): Unit           = { flag_ - "skip" }
      def noneOn(): Unit            = { flag_ + "none" }
      def noneOff(): Unit           = { flag_ - "none" }
      def allOn(): Unit             = { flag_ + "all" }
      def allOff(): Unit            = { flag_ - "all" }
    }

    /* §#
     * generateIntDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val maxdepth: PInt = PInt(
      min = None,
      max = None,
      dflt = Some(-1),
      description = Some(
        """Sets the number of rendered sectors from any given `level`. Set `maxdepth` to *-1* to render all the levels in the hierarchy."""
      )
    )

    object visible extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Boolean = true
      val description: Option[String] = Some(
        """Determines whether or not this trace is visible. If *legendonly*, the trace is not drawn, but can appear as a legend item (provided that the legend itself is visible)."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) => t.toString
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => TBoolean(s)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def true_(): Unit = { selected = TBoolean(true) }

      def false_(): Unit = { selected = TBoolean(false) }

      def legendonly(): Unit = { selected = TString("legendonly") }

    }

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val parents: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the parent sectors for each of the sectors. Empty string items '' are understood to reference the root node in the hierarchy. If `ids` is filled, `parents` items are understood to be "ids" themselves. When `ids` is not set, plotly attempts to find matching items in `labels`, but beware they must be unique."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uirevision: PAny = PAny(
      dflt = None,
      description = Some(
        """Controls persistence of some user-driven changes to the trace: `constraintrange` in `parcoords` traces, as well as some `editable: true` modifications such as `name` and `colorbar.title`. Defaults to `layout.uirevision`. Note that other user-driven trace attribute changes are controlled by `layout` attributes: `trace.visible` is controlled by `layout.legend.uirevision`, `selectedpoints` is controlled by `layout.selectionrevision`, and `colorbar.(x|y)` (accessible with `config: {editable: true}`) is controlled by `layout.editrevision`. Trace changes are tracked by `uid`, which only falls back on trace index if no `uid` is provided. So if your app can add/remove traces before the end of the `data` array, such that the same trace has a different index, you can still preserve user-driven changes if you give each trace a `uid` that stays with it as it moves."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val idssrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  ids .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val text: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets text elements associated with each sector. If trace `textinfo` contains a *text* flag, these elements will be seen on the chart. If trace `hoverinfo` contains a *text* flag and *hovertext* is not set, these elements will be seen in the hover labels."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val parentssrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  parents .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hoverinfosrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hoverinfo .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertextsrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertext .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val valuessrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  values .""")
    )

    object textposition extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """top left"""
      val description: Option[String] = Some(
        """Sets the positions of the `text` elements."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def bottom_right(): Unit = { selected = TString("bottom right") }

      def bottom_center(): Unit = { selected = TString("bottom center") }

      def bottom_left(): Unit = { selected = TString("bottom left") }

      def middle_right(): Unit = { selected = TString("middle right") }

      def middle_center(): Unit = { selected = TString("middle center") }

      def middle_left(): Unit = { selected = TString("middle left") }

      def top_right(): Unit = { selected = TString("top right") }

      def top_center(): Unit = { selected = TString("top center") }

      def top_left(): Unit = { selected = TString("top left") }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val labelssrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  labels .""")
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val meta: PAny = PAny(
      dflt = None,
      description = Some(
        """Assigns extra meta information associated with this trace that can be used in various text attributes. Attributes such as trace `name`, graph, axis and colorbar `title.text`, annotation `text` `rangeselector`, `updatemenues` and `sliders` `label` text all support `meta`. To access the trace `meta` values in an attribute in the same trace, simply use `%{meta[i]}` where `i` is the index or key of the `meta` item in question. To access trace `meta` in layout attributes, use `%{data[n[.meta[i]}` where `i` is the index or key of the `meta` and `n` is the trace index."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val labels: DataArray = DataArray(
      dflt = None,
      description = Some("""Sets the labels of each of the sectors.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val texttemplatesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  texttemplate .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val textsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  text .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertext: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Sets hover text elements associated with each sector. If a single string, the same string appears for all data points. If an array of string, the items are mapped in order of this trace's sectors. To be seen, trace `hoverinfo` must contain a *text* flag."""
      )
    )

    object branchvalues extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """remainder"""
      val description: Option[String] = Some(
        """Determines how the items in `values` are summed. When set to *total*, items in `values` are taken to be value of all its descendants. When set to *remainder*, items in `values` corresponding to the root and the branches sectors are taken to be the extra part not part of the sum of the values at their leaves."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def total(): Unit = { selected = TString("total") }

      def remainder(): Unit = { selected = TString("remainder") }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uid: PChars = PChars(
      dflt = None,
      description = Some(
        """Assign an id to this trace, Use this to provide object constancy between traces during animations and transitions."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val values: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the values associated with each of the sectors. Use with `branchvalues` to determine how the values are summed."""
      )
    )

    object textinfo extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = None
      val description: Option[String] = Some(
        """Determines which trace information appear on the graph."""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def percent_parentOn(): Unit  = { flag_ + "percent parent" }
      def percent_parentOff(): Unit = { flag_ - "percent parent" }
      def percent_entryOn(): Unit   = { flag_ + "percent entry" }
      def percent_entryOff(): Unit  = { flag_ - "percent entry" }
      def percent_rootOn(): Unit    = { flag_ + "percent root" }
      def percent_rootOff(): Unit   = { flag_ - "percent root" }
      def current_pathOn(): Unit    = { flag_ + "current path" }
      def current_pathOff(): Unit   = { flag_ - "current path" }
      def valueOn(): Unit           = { flag_ + "value" }
      def valueOff(): Unit          = { flag_ - "value" }
      def textOn(): Unit            = { flag_ + "text" }
      def textOff(): Unit           = { flag_ - "text" }
      def labelOn(): Unit           = { flag_ + "label" }
      def labelOff(): Unit          = { flag_ - "label" }
      def noneOn(): Unit            = { flag_ + "none" }
      def noneOff(): Unit           = { flag_ - "none" }
    }

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val opacity: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = Some("""1.0"""),
      description = Some("""Sets the opacity of the trace.""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ids: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns id labels to each datum. These ids for object constancy of data points during animation. Should be an array of strings, not numbers or any other type."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val metasrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  meta .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdata: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns extra data each datum. This may be useful when listening to hover, click and selection events. Note that, *scatter* traces also appends customdata items in the markers DOM elements"""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val texttemplate: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Template string used for rendering the information text that appear on points. Note that this will override `textinfo`. Variables are inserted using %{variable}, for example "y: %{y}". Numbers are formatted using d3-format's syntax %{variable:d3-format}, for example "Price: %{y:$.2f}". https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format for details on the formatting syntax. Dates are formatted using d3-time-format's syntax %{variable|d3-time-format}, for example "Day: %{2019-01-01|%A}". https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format for details on the date formatting syntax. Every attributes that can be specified per-point (the ones that are `arrayOk: true`) are available. variables `currentPath`, `root`, `entry`, `percentRoot`, `percentEntry`, `percentParent`, `label` and `value`."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val level: PAny = PAny(
      dflt = None,
      description = Some(
        """Sets the level from which this trace hierarchy is rendered. Set `level` to `''` to start from the root node in the hierarchy. Must be an "id" if `ids` is filled in, otherwise plotly attempts to find a matching item in `labels`."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdatasrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  customdata .""")
    )

    case object insidetextfont extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""
      val description: String =
        """Sets the font used for `textinfo` lying inside the sector."""
      val editType: String = """plot"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal =
        PReal(min = Some(1.0), max = None, dflt = None, description = None)

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val familysrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  family .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  color .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = None
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val sizesrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  size .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val family: PChars = PChars(
        dflt = None,
        description = Some(
          """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val v1 =
          if (familysrc.json().length > 0)
            "\"familysrc\"" + ":" + familysrc.json()
          else ""
        val v2 =
          if (colorsrc.json().length > 0) "\"colorsrc\"" + ":" + colorsrc.json()
          else ""
        val v3 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v4 =
          if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
          else ""
        val v5 =
          if (family.json().length > 0) "\"family\"" + ":" + family.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("size")
          in0.map(ve => size.fromJson(ve))

          val in1 = mapKV.get("familysrc")
          in1.map(ve => familysrc.fromJson(ve))

          val in2 = mapKV.get("colorsrc")
          in2.map(ve => colorsrc.fromJson(ve))

          val in3 = mapKV.get("color")
          in3.map(ve => color.fromJson(ve))

          val in4 = mapKV.get("sizesrc")
          in4.map(ve => sizesrc.fromJson(ve))

          val in5 = mapKV.get("family")
          in5.map(ve => family.fromJson(ve))

        }
        this
      }

    }

    case object domain extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val column: PInt = PInt(
        min = Some(0),
        max = None,
        dflt = Some(0),
        description = Some(
          """If there is a layout grid, use the domain for this column in the grid for this treemap trace ."""
        )
      )

      object y extends JSON {
        /* §#
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val description: Option[String] = Some(
          """Sets the vertical domain of this treemap trace (in plot fraction)."""
        )

        private var int: Array[Int]                 = Array.fill(2)(0)
        private var double: Array[Double]           = Array.fill(2)(0.0)
        private var selected: Map[Int, OfPrimitive] = Map()
        private val n: Integer                      = 2

        def json(): String = {
          /* §#
           * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val nulls = "null"
          val values = (0 until n) map { i =>
            val t = selected.get(i)
            t.fold(nulls)({
              case NAPrimitive =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TString(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TBoolean(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TDouble(_) => double(i).toString
              case TInt(_)    => int(i).toString
              case TImpliedEdits(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TAttrRegexps(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
            })
          }
          val allNull = values.forall(_.toLowerCase.equals(nulls))
          if (allNull)
            ""
          else {
            values.filter(_.length > 0).mkString("[", ",", "]")
          }
        }

        override def fromJson(in: Value): this.type = {
          /* §#
           * generateInfoArrayFromJson()
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          in match {
            case Arr(values) =>
              values.zipWithIndex.foreach { kv =>
                val v = kv._1
                val i = kv._2
                v match {

                  case Num(v) =>
                    if (v.isValidInt) {
                      int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                    } else { double(i) = v; selected += (i -> TDouble(v)) }

                  case Null => () // nulls are valid
                  case _    => err(in)
                }
              }
              this
            case _ =>
              err(in)
              this
          }
        }

// Set the value
        def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
        def int_0(): Unit = { selected -= 0 }

// Set the value
        def double_0(v: Double): Unit = {
          double(0) = v; selected += (0 -> TDouble(v))
        }
// Delete the value (use default)
        def double_0(): Unit = { selected -= 0 }

// Set the value
        def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
        def int_1(): Unit = { selected -= 1 }

// Set the value
        def double_1(v: Double): Unit = {
          double(1) = v; selected += (1 -> TDouble(v))
        }
// Delete the value (use default)
        def double_1(): Unit = { selected -= 1 }

      }

      object x extends JSON {
        /* §#
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val description: Option[String] = Some(
          """Sets the horizontal domain of this treemap trace (in plot fraction)."""
        )

        private var int: Array[Int]                 = Array.fill(2)(0)
        private var double: Array[Double]           = Array.fill(2)(0.0)
        private var selected: Map[Int, OfPrimitive] = Map()
        private val n: Integer                      = 2

        def json(): String = {
          /* §#
           * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val nulls = "null"
          val values = (0 until n) map { i =>
            val t = selected.get(i)
            t.fold(nulls)({
              case NAPrimitive =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TString(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TBoolean(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TDouble(_) => double(i).toString
              case TInt(_)    => int(i).toString
              case TImpliedEdits(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
              case TAttrRegexps(_) =>
                throw new RuntimeException(s"Unexpected type: $t")
            })
          }
          val allNull = values.forall(_.toLowerCase.equals(nulls))
          if (allNull)
            ""
          else {
            values.filter(_.length > 0).mkString("[", ",", "]")
          }
        }

        override def fromJson(in: Value): this.type = {
          /* §#
           * generateInfoArrayFromJson()
           * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          in match {
            case Arr(values) =>
              values.zipWithIndex.foreach { kv =>
                val v = kv._1
                val i = kv._2
                v match {

                  case Num(v) =>
                    if (v.isValidInt) {
                      int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                    } else { double(i) = v; selected += (i -> TDouble(v)) }

                  case Null => () // nulls are valid
                  case _    => err(in)
                }
              }
              this
            case _ =>
              err(in)
              this
          }
        }

// Set the value
        def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
        def int_0(): Unit = { selected -= 0 }

// Set the value
        def double_0(v: Double): Unit = {
          double(0) = v; selected += (0 -> TDouble(v))
        }
// Delete the value (use default)
        def double_0(): Unit = { selected -= 0 }

// Set the value
        def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
        def int_1(): Unit = { selected -= 1 }

// Set the value
        def double_1(v: Double): Unit = {
          double(1) = v; selected += (1 -> TDouble(v))
        }
// Delete the value (use default)
        def double_1(): Unit = { selected -= 1 }

      }

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val row: PInt = PInt(
        min = Some(0),
        max = None,
        dflt = Some(0),
        description = Some(
          """If there is a layout grid, use the domain for this row in the grid for this treemap trace ."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (column.json().length > 0) "\"column\"" + ":" + column.json()
          else ""
        val v1                 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
        val v2                 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
        val v3                 = if (row.json().length > 0) "\"row\"" + ":" + row.json() else ""
        val vars: List[String] = List(v0, v1, v2, v3)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("column")
          in0.map(ve => column.fromJson(ve))

          val in1 = mapKV.get("y")
          in1.map(ve => y.fromJson(ve))

          val in2 = mapKV.get("x")
          in2.map(ve => x.fromJson(ve))

          val in3 = mapKV.get("row")
          in3.map(ve => row.fromJson(ve))

        }
        this
      }

    }

    case object marker extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val cmax: PReal = PReal(
        min = None,
        max = None,
        dflt = None,
        description = Some(
          """Sets the upper bound of the color domain. Has an effect only if colorsis set to a numerical array. Value should have the same units as colors and if set, `marker.cmin` must be set as well."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val cmin: PReal = PReal(
        min = None,
        max = None,
        dflt = None,
        description = Some(
          """Sets the lower bound of the color domain. Has an effect only if colorsis set to a numerical array. Value should have the same units as colors and if set, `marker.cmax` must be set as well."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val autocolorscale: PBool = PBool(
        dflt = Some(true),
        description = Some(
          """Determines whether the colorscale is a default palette (`autocolorscale: true`) or the palette determined by `marker.colorscale`. Has an effect only if colorsis set to a numerical array. In case `colorscale` is unspecified or `autocolorscale` is true, the default  palette will be chosen according to whether numbers in the `color` array are all positive, all negative or mixed."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val reversescale: PBool = PBool(
        dflt = Some(false),
        description = Some(
          """Reverses the color mapping if true. Has an effect only if colorsis set to a numerical array. If true, `marker.cmin` will correspond to the last color in the array and `marker.cmax` will correspond to the first color."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val showscale: PBool = PBool(
        dflt = Some(false),
        description = Some(
          """Determines whether or not a colorbar is displayed for this trace. Has an effect only if colorsis set to a numerical array."""
        )
      )

      /* §#
       * generateSubPlotIDDeclaration(in: PrimitiveDefinition
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val coloraxis: SubPlotID = SubPlotID(
        dflt = None,
        description = Some(
          """Sets a reference to a shared color axis. References to these shared color axes are *coloraxis*, *coloraxis2*, *coloraxis3*, etc. Settings for these shared color axes are set in the layout, under `layout.coloraxis`, `layout.coloraxis2`, etc. Note that multiple color scales can be linked to the same color axis."""
        )
      )

      object depthfade extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val description: Option[String] = Some(
          """Determines if the sector colors are faded towards the background from the leaves up to the headers. This option is unavailable when a `colorscale` is present, defaults to false when `marker.colors` is set, but otherwise defaults to true. When set to *reversed*, the fading direction is inverted, that is the top elements within hierarchy are drawn with fully saturated colors while the leaves are faded towards the background color."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) => t.toString
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => TBoolean(s)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def true_(): Unit = { selected = TBoolean(true) }

        def false_(): Unit = { selected = TBoolean(false) }

        def reversed(): Unit = { selected = TString("reversed") }

      }

      /* §#
       * generateDataArrayDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colors: DataArray = DataArray(
        dflt = None,
        description = Some(
          """Sets the color of each sector of this trace. If not specified, the default trace color set is used to pick the sector colors."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorssrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  colors .""")
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val cmid: PReal = PReal(
        min = None,
        max = None,
        dflt = None,
        description = Some(
          """Sets the mid-point of the color domain by scaling `marker.cmin` and/or `marker.cmax` to be equidistant to this point. Has an effect only if colorsis set to a numerical array. Value should have the same units as colors. Has no effect when `marker.cauto` is `false`."""
        )
      )

      /* §#
       * generateColorScaleDeclaration(in: PrimitiveDefinition
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorscale: PColorScale = PColorScale(
        dflt = None,
        description = Some(
          """Sets the colorscale. Has an effect only if colorsis set to a numerical array. The colorscale must be an array containing arrays mapping a normalized value to an rgb, rgba, hex, hsl, hsv, or named color string. At minimum, a mapping for the lowest (0) and highest (1) values are required. For example, `[[0, 'rgb(0,0,255)'], [1, 'rgb(255,0,0)']]`. To control the bounds of the colorscale in color space, use`marker.cmin` and `marker.cmax`. Alternatively, `colorscale` may be a palette name string of the following list: Greys,YlGnBu,Greens,YlOrRd,Bluered,RdBu,Reds,Blues,Picnic,Rainbow,Portland,Jet,Hot,Blackbody,Earth,Electric,Viridis,Cividis."""
        )
      )

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val cauto: PBool = PBool(
        dflt = Some(true),
        description = Some(
          """Determines whether or not the color domain is computed with respect to the input data (here colors) or the bounds set in `marker.cmin` and `marker.cmax`  Has an effect only if colorsis set to a numerical array. Defaults to `false` when `marker.cmin` and `marker.cmax` are set by the user."""
        )
      )

      case object line extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """calc"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = Some(
            """Sets the color of the line enclosing each sector. Defaults to the `paper_bgcolor` value."""
          )
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val widthsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  width .""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val width: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the width (in px) of the line enclosing each sector."""
          )
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val colorsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  color .""")
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v1 =
            if (widthsrc.json().length > 0)
              "\"widthsrc\"" + ":" + widthsrc.json()
            else ""
          val v2 =
            if (width.json().length > 0) "\"width\"" + ":" + width.json()
            else ""
          val v3 =
            if (colorsrc.json().length > 0)
              "\"colorsrc\"" + ":" + colorsrc.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("color")
            in0.map(ve => color.fromJson(ve))

            val in1 = mapKV.get("widthsrc")
            in1.map(ve => widthsrc.fromJson(ve))

            val in2 = mapKV.get("width")
            in2.map(ve => width.fromJson(ve))

            val in3 = mapKV.get("colorsrc")
            in3.map(ve => colorsrc.fromJson(ve))

          }
          this
        }

      }

      case object colorbar extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """colorbars"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val x: PReal = PReal(
          min = Some(-2.0),
          max = Some(3.0),
          dflt = Some("""1.02"""),
          description =
            Some("""Sets the x position of the color bar (in plot fraction).""")
        )

        /* §#
         * generateAny(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tick0: PAny = PAny(
          dflt = None,
          description = Some(
            """Sets the placement of the first tick on this axis. Use with `dtick`. If the axis `type` is *log*, then you must take the log of your starting tick (e.g. to set the starting tick to 100, set the `tick0` to 2) except when `dtick`=*L<f>* (see `dtick` for more info). If the axis `type` is *date*, it should be a date string, like date data. If the axis `type` is *category*, it should be a number, using the scale where each category is assigned a serial number from zero in the order it appears."""
          )
        )

        /* §#
         * generateIntDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val nticks: PInt = PInt(
          min = Some(0),
          max = None,
          dflt = Some(0),
          description = Some(
            """Specifies the maximum number of ticks for the particular axis. The actual number of ticks will be chosen automatically to be less than or equal to `nticks`. Has an effect only if `tickmode` is set to *auto*."""
          )
        )

        /* §#
         * generateAngleDeclaration(in: PrimitiveDefinition
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickangle: PAngle = PAngle(
          dflt = Some("""auto"""),
          description = Some(
            """Sets the angle of the tick labels with respect to the horizontal. For example, a `tickangle` of -90 draws the tick labels vertically."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val outlinewidth: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""1.0"""),
          description = Some("""Sets the width (in px) of the axis line.""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val bgcolor: PColor = PColor(
          dflt = Some("""rgba(0,0,0,0)"""),
          description = Some("""Sets the color of padded area.""")
        )

        object showexponent extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """all"""
          val description: Option[String] = Some(
            """If *all*, all exponents are shown besides their significands. If *first*, only the exponent of the first tick is shown. If *last*, only the exponent of the last tick is shown. If *none*, no exponents appear."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def none(): Unit = { selected = TString("none") }

          def last(): Unit = { selected = TString("last") }

          def first(): Unit = { selected = TString("first") }

          def all(): Unit = { selected = TString("all") }

        }

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ypad: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""10.0"""),
          description = Some(
            """Sets the amount of padding (in px) along the y direction."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val y: PReal = PReal(
          min = Some(-2.0),
          max = Some(3.0),
          dflt = Some("""0.5"""),
          description =
            Some("""Sets the y position of the color bar (in plot fraction).""")
        )

        object yanchor extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """middle"""
          val description: Option[String] = Some(
            """Sets this color bar's vertical position anchor This anchor binds the `y` position to the *top*, *middle* or *bottom* of the color bar."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def bottom(): Unit = { selected = TString("bottom") }

          def middle(): Unit = { selected = TString("middle") }

          def top(): Unit = { selected = TString("top") }

        }

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticksuffix: PChars = PChars(
          dflt = Some(""""""),
          description = Some("""Sets a tick label suffix.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickwidth: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""1.0"""),
          description = Some("""Sets the tick width (in px).""")
        )

        /* §#
         * generateDataArrayDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickvals: DataArray = DataArray(
          dflt = None,
          description = Some(
            """Sets the values at which ticks on this axis appear. Only has an effect if `tickmode` is set to *array*. Used with `ticktext`."""
          )
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val bordercolor: PColor = PColor(
          dflt = Some("""#444"""),
          description = Some("""Sets the axis line color.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val thickness: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""30.0"""),
          description = Some(
            """Sets the thickness of the color bar This measure excludes the size of the padding, ticks and labels."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticklen: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""5.0"""),
          description = Some("""Sets the tick length (in px).""")
        )

        object tickmode extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val description: Option[String] = Some(
            """Sets the tick mode for this axis. If *auto*, the number of ticks is set via `nticks`. If *linear*, the placement of the ticks is determined by a starting position `tick0` and a tick step `dtick` (*linear* is the default value if `tick0` and `dtick` are provided). If *array*, the placement of the ticks is set via `tickvals` and the tick text is `ticktext`. (*array* is the default value if `tickvals` is provided)."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def array(): Unit = { selected = TString("array") }

          def linear(): Unit = { selected = TString("linear") }

          def auto(): Unit = { selected = TString("auto") }

        }

        /* §#
         * generateAny(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dtick: PAny = PAny(
          dflt = None,
          description = Some(
            """Sets the step in-between ticks on this axis. Use with `tick0`. Must be a positive number, or special strings available to *log* and *date* axes. If the axis `type` is *log*, then ticks are set every 10^(n*dtick) where n is the tick number. For example, to set a tick mark at 1, 10, 100, 1000, ... set dtick to 1. To set tick marks at 1, 100, 10000, ... set dtick to 2. To set tick marks at 1, 5, 25, 125, 625, 3125, ... set dtick to log_10(5), or 0.69897000433. *log* has several special values; *L<f>*, where `f` is a positive number, gives ticks linearly spaced in value (but not position). For example `tick0` = 0.1, `dtick` = *L0.5* will put ticks at 0.1, 0.6, 1.1, 1.6 etc. To show powers of 10 plus small digits between, use *D1* (all digits) or *D2* (only 2 and 5). `tick0` is ignored for *D1* and *D2*. If the axis `type` is *date*, then you must convert the time to milliseconds. For example, to set the interval between ticks to one day, set `dtick` to 86400000.0. *date* also has special values *M<n>* gives ticks spaced by a number of months. `n` must be a positive integer. To set ticks on the 15th of every third month, set `tick0` to *2000-01-15* and `dtick` to *M3*. To set ticks every 4 years, set `dtick` to *M48*"""
          )
        )

        object lenmode extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """fraction"""
          val description: Option[String] = Some(
            """Determines whether this color bar's length (i.e. the measure in the color variation direction) is set in units of plot *fraction* or in *pixels. Use `len` to set the value."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def pixels(): Unit = { selected = TString("pixels") }

          def fraction(): Unit = { selected = TString("fraction") }

        }

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickvalssrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  tickvals .""")
        )

        object showticksuffix extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """all"""
          val description: Option[String] = Some(
            """Same as `showtickprefix` but for tick suffixes."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def none(): Unit = { selected = TString("none") }

          def last(): Unit = { selected = TString("last") }

          def first(): Unit = { selected = TString("first") }

          def all(): Unit = { selected = TString("all") }

        }

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val xpad: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""10.0"""),
          description = Some(
            """Sets the amount of padding (in px) along the x direction."""
          )
        )

        /* §#
         * generateDataArrayDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticktext: DataArray = DataArray(
          dflt = None,
          description = Some(
            """Sets the text displayed at the ticks position via `tickvals`. Only has an effect if `tickmode` is set to *array*. Used with `tickvals`."""
          )
        )

        object ticks extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """"""
          val description: Option[String] = Some(
            """Determines whether ticks are drawn or not. If **, this axis' ticks are not drawn. If *outside* (*inside*), this axis' are drawn outside (inside) the axis lines."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def default(): Unit = { selected = TString("") }

          def inside(): Unit = { selected = TString("inside") }

          def outside(): Unit = { selected = TString("outside") }

        }

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickcolor: PColor = PColor(
          dflt = Some("""#444"""),
          description = Some("""Sets the tick color.""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val ticktextsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  ticktext .""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val outlinecolor: PColor = PColor(
          dflt = Some("""#444"""),
          description = Some("""Sets the axis line color.""")
        )

        object exponentformat extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """B"""
          val description: Option[String] = Some(
            """Determines a formatting rule for the tick exponents. For example, consider the number 1,000,000,000. If *none*, it appears as 1,000,000,000. If *e*, 1e+9. If *E*, 1E+9. If *power*, 1x10^9 (with 9 in a super script). If *SI*, 1G. If *B*, 1B."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def B(): Unit = { selected = TString("B") }

          def SI(): Unit = { selected = TString("SI") }

          def power(): Unit = { selected = TString("power") }

          def E(): Unit = { selected = TString("E") }

          def e(): Unit = { selected = TString("e") }

          def none(): Unit = { selected = TString("none") }

        }

        object showtickprefix extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """all"""
          val description: Option[String] = Some(
            """If *all*, all tick labels are displayed with a prefix. If *first*, only the first tick is displayed with a prefix. If *last*, only the last tick is displayed with a suffix. If *none*, tick prefixes are hidden."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def none(): Unit = { selected = TString("none") }

          def last(): Unit = { selected = TString("last") }

          def first(): Unit = { selected = TString("first") }

          def all(): Unit = { selected = TString("all") }

        }

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickprefix: PChars = PChars(
          dflt = Some(""""""),
          description = Some("""Sets a tick label prefix.""")
        )

        object xanchor extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """left"""
          val description: Option[String] = Some(
            """Sets this color bar's horizontal position anchor. This anchor binds the `x` position to the *left*, *center* or *right* of the color bar."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def right(): Unit = { selected = TString("right") }

          def center(): Unit = { selected = TString("center") }

          def left(): Unit = { selected = TString("left") }

        }

        object thicknessmode extends JSON {
          /* §#
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val dflt: String = """pixels"""
          val description: Option[String] = Some(
            """Determines whether this color bar's thickness (i.e. the measure in the constant color direction) is set in units of plot *fraction* or in *pixels*. Use `thickness` to set the value."""
          )

          private var selected: OfPrimitive = NAPrimitive
          //allDeclarations

          def json(): String = {
            /* §#
             * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            selected match {
              case NAPrimitive => ""
              case TString(t)  => "\"" + t.toString + "\""
              case TBoolean(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TBoolean(false)"""
                )
              case TDouble(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TDouble(0.0)"""
                )
              case TInt(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TInt(0)"""
                )
              case TImpliedEdits(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TImpliedEdits()"""
                )
              case TAttrRegexps(t) =>
                throw new RuntimeException(
                  """Unexpected OfPrimitive type: TAttrRegexps()"""
                )
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err(j: ujson.Value): OfPrimitive =
              throw new RuntimeException(s"""Parsing of $j failed: $in""")
            val read = in match {
              case Str(s)  => TString(s)
              case Bool(s) => err(in)
              case Num(s)  => err(in)
              case _       => err(in)
            }
            selected = read
            this
          }

          def pixels(): Unit = { selected = TString("pixels") }

          def fraction(): Unit = { selected = TString("fraction") }

        }

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val separatethousands: PBool = PBool(
          dflt = Some(false),
          description =
            Some("""If "true", even 4-digit integers are separated""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val tickformat: PChars = PChars(
          dflt = Some(""""""),
          description = Some(
            """Sets the tick label formatting rule using d3 formatting mini-languages which are very similar to those in Python. For numbers, see: https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format And for dates see: https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format We add one item to d3's date formatter: *%{n}f* for fractional seconds with n digits. For example, *2016-10-13 09:15:23.456* with tickformat *%H~%M~%S.%2f* would display *09~15~23.46*"""
          )
        )

        /* §#
         * generateBooleanDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val showticklabels: PBool = PBool(
          dflt = Some(true),
          description =
            Some("""Determines whether or not the tick labels are drawn.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val len: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the length of the color bar This measure excludes the padding of both ends. That is, the color bar length is this length minus the padding on both ends."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val borderwidth: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""0.0"""),
          description = Some(
            """Sets the width (in px) or the border enclosing this color bar."""
          )
        )

        case object tickfont extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String        = """object"""
          val description: String = """Sets the color bar's tick label font"""
          val editType: String    = """colorbars"""

          /* §#
           * generateColor(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val color: PColor = PColor(
            dflt = None,
            description = None
          )

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val family: PChars = PChars(
            dflt = None,
            description = Some(
              """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
            )
          )

          /* §#
           * generateNumberDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val size: PReal =
            PReal(min = Some(1.0), max = None, dflt = None, description = None)

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (color.json().length > 0) "\"color\"" + ":" + color.json()
              else ""
            val v1 =
              if (family.json().length > 0) "\"family\"" + ":" + family.json()
              else ""
            val v2 =
              if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
            val vars: List[String] = List(v0, v1, v2)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("color")
              in0.map(ve => color.fromJson(ve))

              val in1 = mapKV.get("family")
              in1.map(ve => family.fromJson(ve))

              val in2 = mapKV.get("size")
              in2.map(ve => size.fromJson(ve))

            }
            this
          }

        }

        case object title extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String     = """object"""
          val editType: String = """colorbars"""

          /* §#
           * generateStringDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val text: PChars = PChars(
            dflt = None,
            description = Some(
              """Sets the title of the color bar. Note that before the existence of `title.text`, the title's contents used to be defined as the `title` attribute itself. This behavior has been deprecated."""
            )
          )

          object side extends JSON {
            /* §#
             * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val dflt: String = """top"""
            val description: Option[String] = Some(
              """Determines the location of color bar's title with respect to the color bar. Note that the title's location used to be set by the now deprecated `titleside` attribute."""
            )

            private var selected: OfPrimitive = NAPrimitive
            //allDeclarations

            def json(): String = {
              /* §#
               * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
               * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              selected match {
                case NAPrimitive => ""
                case TString(t)  => "\"" + t.toString + "\""
                case TBoolean(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TBoolean(false)"""
                  )
                case TDouble(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TDouble(0.0)"""
                  )
                case TInt(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TInt(0)"""
                  )
                case TImpliedEdits(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TImpliedEdits()"""
                  )
                case TAttrRegexps(t) =>
                  throw new RuntimeException(
                    """Unexpected OfPrimitive type: TAttrRegexps()"""
                  )
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
               * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err(j: ujson.Value): OfPrimitive =
                throw new RuntimeException(s"""Parsing of $j failed: $in""")
              val read = in match {
                case Str(s)  => TString(s)
                case Bool(s) => err(in)
                case Num(s)  => err(in)
                case _       => err(in)
              }
              selected = read
              this
            }

            def bottom(): Unit = { selected = TString("bottom") }

            def top(): Unit = { selected = TString("top") }

            def right(): Unit = { selected = TString("right") }

          }

          case object font extends JSON {
            /* §#
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val role: String = """object"""
            val description: String =
              """Sets this color bar's title font. Note that the title's font used to be set by the now deprecated `titlefont` attribute."""
            val editType: String = """colorbars"""

            /* §#
             * generateColor(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val color: PColor = PColor(
              dflt = None,
              description = None
            )

            /* §#
             * generateStringDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val family: PChars = PChars(
              dflt = None,
              description = Some(
                """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
              )
            )

            /* §#
             * generateNumberDeclaration(in: PrimitiveDefinition)
             * generatePrimitiveDeclaration(in: PrimitiveDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            val size: PReal = PReal(
              min = Some(1.0),
              max = None,
              dflt = None,
              description = None
            )

            def json(): String = {
              /* §#
               * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/

              val v0 =
                if (color.json().length > 0) "\"color\"" + ":" + color.json()
                else ""
              val v1 =
                if (family.json().length > 0) "\"family\"" + ":" + family.json()
                else ""
              val v2 =
                if (size.json().length > 0) "\"size\"" + ":" + size.json()
                else ""
              val vars: List[String] = List(v0, v1, v2)
              val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
              if (cleanVars.length > 0) {
                // values set
                "{" + cleanVars + "}"
              } else {
                // no values set
                ""
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err =
                throw new RuntimeException(s"Expected an Obj but got $in")
              in.objOpt.fold(err) { mapKV =>
                val in0 = mapKV.get("color")
                in0.map(ve => color.fromJson(ve))

                val in1 = mapKV.get("family")
                in1.map(ve => family.fromJson(ve))

                val in2 = mapKV.get("size")
                in2.map(ve => size.fromJson(ve))

              }
              this
            }

          }

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
            val v1 =
              if (text.json().length > 0) "\"text\"" + ":" + text.json() else ""
            val v2 =
              if (side.json().length > 0) "\"side\"" + ":" + side.json() else ""
            val vars: List[String] = List(v0, v1, v2)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("font")
              in0.map(ve => font.fromJson(ve))

              val in1 = mapKV.get("text")
              in1.map(ve => text.fromJson(ve))

              val in2 = mapKV.get("side")
              in2.map(ve => side.fromJson(ve))

            }
            this
          }

        }

        case object tickformatstops extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""

          case object items extends JSON {
            /* §#
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            case object tickformatstop extends JSON {
              /* §#
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val role: String     = """object"""
              val editType: String = """colorbars"""

              object dtickrange extends JSON {
                /* §#
                 * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                 * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                val description: Option[String] = Some(
                  """range [*min*, *max*], where *min*, *max* - dtick values which describe some zoom level, it is possible to omit *min* or *max* value by passing *null*"""
                )

                private var string: Array[String]           = Array.fill(2)("")
                private var boolean: Array[Boolean]         = Array.fill(2)(false)
                private var double: Array[Double]           = Array.fill(2)(0.0)
                private var int: Array[Int]                 = Array.fill(2)(0)
                private var selected: Map[Int, OfPrimitive] = Map()
                private val n: Integer                      = 2

                def json(): String = {
                  /* §#
                   * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
                   * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generate(obj: CompoundDefinition)
                   * generate(obj: Either[Error, CompoundDefinition])*/
                  val nulls = "null"
                  val values = (0 until n) map { i =>
                    val t = selected.get(i)
                    t.fold(nulls)({
                      case NAPrimitive =>
                        throw new RuntimeException(s"Unexpected type: $t")
                      case TString(_)  => string(i)
                      case TBoolean(_) => boolean(i).toString
                      case TDouble(_)  => double(i).toString
                      case TInt(_)     => int(i).toString
                      case TImpliedEdits(_) =>
                        throw new RuntimeException(s"Unexpected type: $t")
                      case TAttrRegexps(_) =>
                        throw new RuntimeException(s"Unexpected type: $t")
                    })
                  }
                  val allNull = values.forall(_.toLowerCase.equals(nulls))
                  if (allNull)
                    ""
                  else {
                    values.filter(_.length > 0).mkString("[", ",", "]")
                  }
                }

                override def fromJson(in: Value): this.type = {
                  /* §#
                   * generateInfoArrayFromJson()
                   * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
                   * generatePrimitiveDeclaration(in: PrimitiveDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generateObjects(obj: ObjectDefinition)
                   * generateCompound(obj: ObjectDefinition)
                   * generate(obj: CompoundDefinition)
                   * generate(obj: Either[Error, CompoundDefinition])*/
                  def err(j: ujson.Value): OfPrimitive =
                    throw new RuntimeException(s"""Parsing of $j failed: $in""")
                  in match {
                    case Arr(values) =>
                      values.zipWithIndex.foreach { kv =>
                        val v = kv._1
                        val i = kv._2
                        v match {
                          case Bool(v) =>
                            boolean(i) = v; selected += (i -> TBoolean(v))
                          case Str(v) =>
                            string(i) = v; selected += (i -> TString(v))

                          case Num(v) =>
                            if (v.isValidInt) {
                              int(i) = v.toInt; selected += (i -> TInt(v.toInt))
                            } else {
                              double(i) = v; selected += (i -> TDouble(v))
                            }

                          case Null => () // nulls are valid
                          case _    => err(in)
                        }
                      }
                      this
                    case _ =>
                      err(in)
                      this
                  }
                }

// Set the value
                def string_0(v: String): Unit = {
                  string(0) = v; selected += (0 -> TString(v))
                }
// Delete the value (use default)
                def string_0(): Unit = { selected -= 0 }

// Set the value
                def boolean_0(v: Boolean): Unit = {
                  boolean(0) = v; selected += (0 -> TBoolean(v))
                }
// Delete the value (use default)
                def boolean_0(): Unit = { selected -= 0 }

// Set the value
                def double_0(v: Double): Unit = {
                  double(0) = v; selected += (0 -> TDouble(v))
                }
// Delete the value (use default)
                def double_0(): Unit = { selected -= 0 }

// Set the value
                def int_0(v: Int): Unit = {
                  int(0) = v; selected += (0 -> TInt(v))
                }
// Delete the value (use default)
                def int_0(): Unit = { selected -= 0 }

// Set the value
                def string_1(v: String): Unit = {
                  string(1) = v; selected += (1 -> TString(v))
                }
// Delete the value (use default)
                def string_1(): Unit = { selected -= 1 }

// Set the value
                def boolean_1(v: Boolean): Unit = {
                  boolean(1) = v; selected += (1 -> TBoolean(v))
                }
// Delete the value (use default)
                def boolean_1(): Unit = { selected -= 1 }

// Set the value
                def double_1(v: Double): Unit = {
                  double(1) = v; selected += (1 -> TDouble(v))
                }
// Delete the value (use default)
                def double_1(): Unit = { selected -= 1 }

// Set the value
                def int_1(v: Int): Unit = {
                  int(1) = v; selected += (1 -> TInt(v))
                }
// Delete the value (use default)
                def int_1(): Unit = { selected -= 1 }

              }

              /* §#
               * generateStringDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val name: PChars = PChars(
                dflt = None,
                description = Some(
                  """When used in a template, named items are created in the output figure in addition to any items the figure already has in this array. You can modify these items in the output figure by making your own item with `templateitemname` matching this `name` alongside your modifications (including `visible: false` or `enabled: false` to hide it). Has no effect outside of a template."""
                )
              )

              /* §#
               * generateBooleanDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val enabled: PBool = PBool(
                dflt = Some(true),
                description = Some(
                  """Determines whether or not this stop is used. If `false`, this stop is ignored even within its `dtickrange`."""
                )
              )

              /* §#
               * generateStringDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val value: PChars = PChars(
                dflt = Some(""""""),
                description = Some(
                  """string - dtickformat for described zoom level, the same as *tickformat*"""
                )
              )

              /* §#
               * generateStringDeclaration(in: PrimitiveDefinition)
               * generatePrimitiveDeclaration(in: PrimitiveDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              val templateitemname: PChars = PChars(
                dflt = None,
                description = Some(
                  """Used to refer to a named item in this array in the template. Named items from the template will be created even without a matching item in the input figure, but you can modify one by making an item with `templateitemname` matching its `name`, alongside your modifications (including `visible: false` or `enabled: false` to hide it). If there is no template or no matching item, this item will be hidden unless you explicitly show it with `visible: true`."""
                )
              )

              def json(): String = {
                /* §#
                 * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
                 * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/

                val v0 =
                  if (dtickrange.json().length > 0)
                    "\"dtickrange\"" + ":" + dtickrange.json()
                  else ""
                val v1 =
                  if (name.json().length > 0) "\"name\"" + ":" + name.json()
                  else ""
                val v2 =
                  if (enabled.json().length > 0)
                    "\"enabled\"" + ":" + enabled.json()
                  else ""
                val v3 =
                  if (value.json().length > 0) "\"value\"" + ":" + value.json()
                  else ""
                val v4 =
                  if (templateitemname.json().length > 0)
                    "\"templateitemname\"" + ":" + templateitemname.json()
                  else ""
                val vars: List[String] = List(v0, v1, v2, v3, v4)
                val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
                if (cleanVars.length > 0) {
                  // values set
                  "{" + cleanVars + "}"
                } else {
                  // no values set
                  ""
                }
              }

              def fromJson(in: Value): this.type = {
                /* §#
                 * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
                 * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generateObjects(obj: ObjectDefinition)
                 * generateCompound(obj: ObjectDefinition)
                 * generate(obj: CompoundDefinition)
                 * generate(obj: Either[Error, CompoundDefinition])*/
                def err =
                  throw new RuntimeException(s"Expected an Obj but got $in")
                in.objOpt.fold(err) { mapKV =>
                  val in0 = mapKV.get("dtickrange")
                  in0.map(ve => dtickrange.fromJson(ve))

                  val in1 = mapKV.get("name")
                  in1.map(ve => name.fromJson(ve))

                  val in2 = mapKV.get("enabled")
                  in2.map(ve => enabled.fromJson(ve))

                  val in3 = mapKV.get("value")
                  in3.map(ve => value.fromJson(ve))

                  val in4 = mapKV.get("templateitemname")
                  in4.map(ve => templateitemname.fromJson(ve))

                }
                this
              }

            }

            def json(): String = {
              /* §#
               * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/

              val v0 =
                if (tickformatstop.json().length > 0)
                  "\"tickformatstop\"" + ":" + tickformatstop.json()
                else ""
              val vars: List[String] = List(v0)
              val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
              if (cleanVars.length > 0) {
                // values set
                "{" + cleanVars + "}"
              } else {
                // no values set
                ""
              }
            }

            def fromJson(in: Value): this.type = {
              /* §#
               * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
               * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generateObjects(obj: ObjectDefinition)
               * generateCompound(obj: ObjectDefinition)
               * generate(obj: CompoundDefinition)
               * generate(obj: Either[Error, CompoundDefinition])*/
              def err =
                throw new RuntimeException(s"Expected an Obj but got $in")
              in.objOpt.fold(err) { mapKV =>
                val in0 = mapKV.get("tickformatstop")
                in0.map(ve => tickformatstop.fromJson(ve))

              }
              this
            }

          }

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val v0 =
              if (items.json().length > 0) "\"items\"" + ":" + items.json()
              else ""
            val vars: List[String] = List(v0)
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV =>
              val in0 = mapKV.get("items")
              in0.map(ve => items.fromJson(ve))

            }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (tickfont.json().length > 0)
              "\"tickfont\"" + ":" + tickfont.json()
            else ""
          val v1 =
            if (title.json().length > 0) "\"title\"" + ":" + title.json()
            else ""
          val v2 =
            if (tickformatstops.json().length > 0)
              "\"tickformatstops\"" + ":" + tickformatstops.json()
            else ""
          val v3 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
          val v4 =
            if (tick0.json().length > 0) "\"tick0\"" + ":" + tick0.json()
            else ""
          val v5 =
            if (nticks.json().length > 0) "\"nticks\"" + ":" + nticks.json()
            else ""
          val v6 =
            if (tickangle.json().length > 0)
              "\"tickangle\"" + ":" + tickangle.json()
            else ""
          val v7 =
            if (outlinewidth.json().length > 0)
              "\"outlinewidth\"" + ":" + outlinewidth.json()
            else ""
          val v8 =
            if (bgcolor.json().length > 0) "\"bgcolor\"" + ":" + bgcolor.json()
            else ""
          val v9 =
            if (showexponent.json().length > 0)
              "\"showexponent\"" + ":" + showexponent.json()
            else ""
          val v10 =
            if (ypad.json().length > 0) "\"ypad\"" + ":" + ypad.json() else ""
          val v11 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
          val v12 =
            if (yanchor.json().length > 0) "\"yanchor\"" + ":" + yanchor.json()
            else ""
          val v13 =
            if (ticksuffix.json().length > 0)
              "\"ticksuffix\"" + ":" + ticksuffix.json()
            else ""
          val v14 =
            if (tickwidth.json().length > 0)
              "\"tickwidth\"" + ":" + tickwidth.json()
            else ""
          val v15 =
            if (tickvals.json().length > 0)
              "\"tickvals\"" + ":" + tickvals.json()
            else ""
          val v16 =
            if (bordercolor.json().length > 0)
              "\"bordercolor\"" + ":" + bordercolor.json()
            else ""
          val v17 =
            if (thickness.json().length > 0)
              "\"thickness\"" + ":" + thickness.json()
            else ""
          val v18 =
            if (ticklen.json().length > 0) "\"ticklen\"" + ":" + ticklen.json()
            else ""
          val v19 =
            if (tickmode.json().length > 0)
              "\"tickmode\"" + ":" + tickmode.json()
            else ""
          val v20 =
            if (dtick.json().length > 0) "\"dtick\"" + ":" + dtick.json()
            else ""
          val v21 =
            if (lenmode.json().length > 0) "\"lenmode\"" + ":" + lenmode.json()
            else ""
          val v22 =
            if (tickvalssrc.json().length > 0)
              "\"tickvalssrc\"" + ":" + tickvalssrc.json()
            else ""
          val v23 =
            if (showticksuffix.json().length > 0)
              "\"showticksuffix\"" + ":" + showticksuffix.json()
            else ""
          val v24 =
            if (xpad.json().length > 0) "\"xpad\"" + ":" + xpad.json() else ""
          val v25 =
            if (ticktext.json().length > 0)
              "\"ticktext\"" + ":" + ticktext.json()
            else ""
          val v26 =
            if (ticks.json().length > 0) "\"ticks\"" + ":" + ticks.json()
            else ""
          val v27 =
            if (tickcolor.json().length > 0)
              "\"tickcolor\"" + ":" + tickcolor.json()
            else ""
          val v28 =
            if (ticktextsrc.json().length > 0)
              "\"ticktextsrc\"" + ":" + ticktextsrc.json()
            else ""
          val v29 =
            if (outlinecolor.json().length > 0)
              "\"outlinecolor\"" + ":" + outlinecolor.json()
            else ""
          val v30 =
            if (exponentformat.json().length > 0)
              "\"exponentformat\"" + ":" + exponentformat.json()
            else ""
          val v31 =
            if (showtickprefix.json().length > 0)
              "\"showtickprefix\"" + ":" + showtickprefix.json()
            else ""
          val v32 =
            if (tickprefix.json().length > 0)
              "\"tickprefix\"" + ":" + tickprefix.json()
            else ""
          val v33 =
            if (xanchor.json().length > 0) "\"xanchor\"" + ":" + xanchor.json()
            else ""
          val v34 =
            if (thicknessmode.json().length > 0)
              "\"thicknessmode\"" + ":" + thicknessmode.json()
            else ""
          val v35 =
            if (separatethousands.json().length > 0)
              "\"separatethousands\"" + ":" + separatethousands.json()
            else ""
          val v36 =
            if (tickformat.json().length > 0)
              "\"tickformat\"" + ":" + tickformat.json()
            else ""
          val v37 =
            if (showticklabels.json().length > 0)
              "\"showticklabels\"" + ":" + showticklabels.json()
            else ""
          val v38 =
            if (len.json().length > 0) "\"len\"" + ":" + len.json() else ""
          val v39 =
            if (borderwidth.json().length > 0)
              "\"borderwidth\"" + ":" + borderwidth.json()
            else ""
          val vars: List[String] = List(
            v0,
            v1,
            v2,
            v3,
            v4,
            v5,
            v6,
            v7,
            v8,
            v9,
            v10,
            v11,
            v12,
            v13,
            v14,
            v15,
            v16,
            v17,
            v18,
            v19,
            v20,
            v21,
            v22,
            v23,
            v24,
            v25,
            v26,
            v27,
            v28,
            v29,
            v30,
            v31,
            v32,
            v33,
            v34,
            v35,
            v36,
            v37,
            v38,
            v39
          )
          val cleanVars = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("tickfont")
            in0.map(ve => tickfont.fromJson(ve))

            val in1 = mapKV.get("title")
            in1.map(ve => title.fromJson(ve))

            val in2 = mapKV.get("tickformatstops")
            in2.map(ve => tickformatstops.fromJson(ve))

            val in3 = mapKV.get("x")
            in3.map(ve => x.fromJson(ve))

            val in4 = mapKV.get("tick0")
            in4.map(ve => tick0.fromJson(ve))

            val in5 = mapKV.get("nticks")
            in5.map(ve => nticks.fromJson(ve))

            val in6 = mapKV.get("tickangle")
            in6.map(ve => tickangle.fromJson(ve))

            val in7 = mapKV.get("outlinewidth")
            in7.map(ve => outlinewidth.fromJson(ve))

            val in8 = mapKV.get("bgcolor")
            in8.map(ve => bgcolor.fromJson(ve))

            val in9 = mapKV.get("showexponent")
            in9.map(ve => showexponent.fromJson(ve))

            val in10 = mapKV.get("ypad")
            in10.map(ve => ypad.fromJson(ve))

            val in11 = mapKV.get("y")
            in11.map(ve => y.fromJson(ve))

            val in12 = mapKV.get("yanchor")
            in12.map(ve => yanchor.fromJson(ve))

            val in13 = mapKV.get("ticksuffix")
            in13.map(ve => ticksuffix.fromJson(ve))

            val in14 = mapKV.get("tickwidth")
            in14.map(ve => tickwidth.fromJson(ve))

            val in15 = mapKV.get("tickvals")
            in15.map(ve => tickvals.fromJson(ve))

            val in16 = mapKV.get("bordercolor")
            in16.map(ve => bordercolor.fromJson(ve))

            val in17 = mapKV.get("thickness")
            in17.map(ve => thickness.fromJson(ve))

            val in18 = mapKV.get("ticklen")
            in18.map(ve => ticklen.fromJson(ve))

            val in19 = mapKV.get("tickmode")
            in19.map(ve => tickmode.fromJson(ve))

            val in20 = mapKV.get("dtick")
            in20.map(ve => dtick.fromJson(ve))

            val in21 = mapKV.get("lenmode")
            in21.map(ve => lenmode.fromJson(ve))

            val in22 = mapKV.get("tickvalssrc")
            in22.map(ve => tickvalssrc.fromJson(ve))

            val in23 = mapKV.get("showticksuffix")
            in23.map(ve => showticksuffix.fromJson(ve))

            val in24 = mapKV.get("xpad")
            in24.map(ve => xpad.fromJson(ve))

            val in25 = mapKV.get("ticktext")
            in25.map(ve => ticktext.fromJson(ve))

            val in26 = mapKV.get("ticks")
            in26.map(ve => ticks.fromJson(ve))

            val in27 = mapKV.get("tickcolor")
            in27.map(ve => tickcolor.fromJson(ve))

            val in28 = mapKV.get("ticktextsrc")
            in28.map(ve => ticktextsrc.fromJson(ve))

            val in29 = mapKV.get("outlinecolor")
            in29.map(ve => outlinecolor.fromJson(ve))

            val in30 = mapKV.get("exponentformat")
            in30.map(ve => exponentformat.fromJson(ve))

            val in31 = mapKV.get("showtickprefix")
            in31.map(ve => showtickprefix.fromJson(ve))

            val in32 = mapKV.get("tickprefix")
            in32.map(ve => tickprefix.fromJson(ve))

            val in33 = mapKV.get("xanchor")
            in33.map(ve => xanchor.fromJson(ve))

            val in34 = mapKV.get("thicknessmode")
            in34.map(ve => thicknessmode.fromJson(ve))

            val in35 = mapKV.get("separatethousands")
            in35.map(ve => separatethousands.fromJson(ve))

            val in36 = mapKV.get("tickformat")
            in36.map(ve => tickformat.fromJson(ve))

            val in37 = mapKV.get("showticklabels")
            in37.map(ve => showticklabels.fromJson(ve))

            val in38 = mapKV.get("len")
            in38.map(ve => len.fromJson(ve))

            val in39 = mapKV.get("borderwidth")
            in39.map(ve => borderwidth.fromJson(ve))

          }
          this
        }

      }

      case object pad extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """calc"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val b: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = None,
          description = Some("""Sets the padding form the bottom (in px).""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val t: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = None,
          description = Some("""Sets the padding form the top (in px).""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val r: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = None,
          description = Some("""Sets the padding form the right (in px).""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val l: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = None,
          description = Some("""Sets the padding form the left (in px).""")
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0                 = if (b.json().length > 0) "\"b\"" + ":" + b.json() else ""
          val v1                 = if (t.json().length > 0) "\"t\"" + ":" + t.json() else ""
          val v2                 = if (r.json().length > 0) "\"r\"" + ":" + r.json() else ""
          val v3                 = if (l.json().length > 0) "\"l\"" + ":" + l.json() else ""
          val vars: List[String] = List(v0, v1, v2, v3)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("b")
            in0.map(ve => b.fromJson(ve))

            val in1 = mapKV.get("t")
            in1.map(ve => t.fromJson(ve))

            val in2 = mapKV.get("r")
            in2.map(ve => r.fromJson(ve))

            val in3 = mapKV.get("l")
            in3.map(ve => l.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
        val v1 =
          if (colorbar.json().length > 0) "\"colorbar\"" + ":" + colorbar.json()
          else ""
        val v2 = if (pad.json().length > 0) "\"pad\"" + ":" + pad.json() else ""
        val v3 =
          if (cmax.json().length > 0) "\"cmax\"" + ":" + cmax.json() else ""
        val v4 =
          if (cmin.json().length > 0) "\"cmin\"" + ":" + cmin.json() else ""
        val v5 =
          if (autocolorscale.json().length > 0)
            "\"autocolorscale\"" + ":" + autocolorscale.json()
          else ""
        val v6 =
          if (reversescale.json().length > 0)
            "\"reversescale\"" + ":" + reversescale.json()
          else ""
        val v7 =
          if (showscale.json().length > 0)
            "\"showscale\"" + ":" + showscale.json()
          else ""
        val v8 =
          if (coloraxis.json().length > 0)
            "\"coloraxis\"" + ":" + coloraxis.json()
          else ""
        val v9 =
          if (depthfade.json().length > 0)
            "\"depthfade\"" + ":" + depthfade.json()
          else ""
        val v10 =
          if (colors.json().length > 0) "\"colors\"" + ":" + colors.json()
          else ""
        val v11 =
          if (colorssrc.json().length > 0)
            "\"colorssrc\"" + ":" + colorssrc.json()
          else ""
        val v12 =
          if (cmid.json().length > 0) "\"cmid\"" + ":" + cmid.json() else ""
        val v13 =
          if (colorscale.json().length > 0)
            "\"colorscale\"" + ":" + colorscale.json()
          else ""
        val v14 =
          if (cauto.json().length > 0) "\"cauto\"" + ":" + cauto.json() else ""
        val vars: List[String] =
          List(v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14)
        val cleanVars = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("line")
          in0.map(ve => line.fromJson(ve))

          val in1 = mapKV.get("colorbar")
          in1.map(ve => colorbar.fromJson(ve))

          val in2 = mapKV.get("pad")
          in2.map(ve => pad.fromJson(ve))

          val in3 = mapKV.get("cmax")
          in3.map(ve => cmax.fromJson(ve))

          val in4 = mapKV.get("cmin")
          in4.map(ve => cmin.fromJson(ve))

          val in5 = mapKV.get("autocolorscale")
          in5.map(ve => autocolorscale.fromJson(ve))

          val in6 = mapKV.get("reversescale")
          in6.map(ve => reversescale.fromJson(ve))

          val in7 = mapKV.get("showscale")
          in7.map(ve => showscale.fromJson(ve))

          val in8 = mapKV.get("coloraxis")
          in8.map(ve => coloraxis.fromJson(ve))

          val in9 = mapKV.get("depthfade")
          in9.map(ve => depthfade.fromJson(ve))

          val in10 = mapKV.get("colors")
          in10.map(ve => colors.fromJson(ve))

          val in11 = mapKV.get("colorssrc")
          in11.map(ve => colorssrc.fromJson(ve))

          val in12 = mapKV.get("cmid")
          in12.map(ve => cmid.fromJson(ve))

          val in13 = mapKV.get("colorscale")
          in13.map(ve => colorscale.fromJson(ve))

          val in14 = mapKV.get("cauto")
          in14.map(ve => cauto.fromJson(ve))

        }
        this
      }

    }

    case object tiling extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val squarifyratio: PReal = PReal(
        min = Some(1.0),
        max = None,
        dflt = Some("""1.0"""),
        description = Some(
          """When using *squarify* `packing` algorithm, according to https://github.com/d3/d3-hierarchy/blob/master/README.md#squarify_ratio this option specifies the desired aspect ratio of the generated rectangles. The ratio must be specified as a number greater than or equal to one. Note that the orientation of the generated rectangles (tall or wide) is not implied by the ratio; for example, a ratio of two will attempt to produce a mixture of rectangles whose width:height ratio is either 2:1 or 1:2. When using *squarify*, unlike d3 which uses the Golden Ratio i.e. 1.618034, Plotly applies 1 to increase squares in treemap layouts."""
        )
      )

      object flip extends JSON {
        /* §#
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: Option[String] = Some("""""")
        val description: Option[String] = Some(
          """Determines if the positions obtained from solver are flipped on each axis."""
        )

        private val flag_ : mutable.Set[String] = mutable.Set()
        //dflt.map(e => flag_ + e)
        def flag: String = flag_.mkString("+")
        def json(): String = {
          if (flag.contains("true"))
            "true"
          else if (flag.contains("false"))
            "false"
          else if (flag.length > 0)
            "\"" + flag + "\""
          else
            ""
        }

        override def fromJson(in: Value): this.type = {
          /* §#
           * generateFlagListFromJson()
           * generateFlagListDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          in match {
            case Str(value) =>
              flag_ += value
              this
            case Bool(value) =>
              flag_ += value.toString
              this
            case _ =>
              err(in)
              this
          }
        }

        def yOn(): Unit  = { flag_ + "y" }
        def yOff(): Unit = { flag_ - "y" }
        def xOn(): Unit  = { flag_ + "x" }
        def xOff(): Unit = { flag_ - "x" }

      }

      object packing extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """squarify"""
        val description: Option[String] = Some(
          """Determines d3 treemap solver. For more info please refer to https://github.com/d3/d3-hierarchy#treemap-tiling"""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def dice_slice(): Unit = { selected = TString("dice-slice") }

        def slice_dice(): Unit = { selected = TString("slice-dice") }

        def slice(): Unit = { selected = TString("slice") }

        def dice(): Unit = { selected = TString("dice") }

        def binary(): Unit = { selected = TString("binary") }

        def squarify(): Unit = { selected = TString("squarify") }

      }

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val pad: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""3.0"""),
        description = Some("""Sets the inner padding (in px).""")
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (squarifyratio.json().length > 0)
            "\"squarifyratio\"" + ":" + squarifyratio.json()
          else ""
        val v1 =
          if (flip.json().length > 0) "\"flip\"" + ":" + flip.json() else ""
        val v2 =
          if (packing.json().length > 0) "\"packing\"" + ":" + packing.json()
          else ""
        val v3                 = if (pad.json().length > 0) "\"pad\"" + ":" + pad.json() else ""
        val vars: List[String] = List(v0, v1, v2, v3)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("squarifyratio")
          in0.map(ve => squarifyratio.fromJson(ve))

          val in1 = mapKV.get("flip")
          in1.map(ve => flip.fromJson(ve))

          val in2 = mapKV.get("packing")
          in2.map(ve => packing.fromJson(ve))

          val in3 = mapKV.get("pad")
          in3.map(ve => pad.fromJson(ve))

        }
        this
      }

    }

    case object pathbar extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      object side extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """top"""
        val description: Option[String] = Some(
          """Determines on which side of the the treemap the `pathbar` should be presented."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def bottom(): Unit = { selected = TString("bottom") }

        def top(): Unit = { selected = TString("top") }

      }

      object edgeshape extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """>"""
        val description: Option[String] = Some(
          """Determines which shape is used for edges between `barpath` labels."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def \\(): Unit = { selected = TString("\\") }

        def /(): Unit = { selected = TString("/") }

        def |(): Unit = { selected = TString("|") }

        def <(): Unit = { selected = TString("<") }

        def >(): Unit = { selected = TString(">") }

      }

      /* §#
       * generateBooleanDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val visible: PBool = PBool(
        dflt = Some(true),
        description = Some(
          """Determines if the path bar is drawn i.e. outside the trace `domain` and with one pixel gap."""
        )
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val thickness: PReal = PReal(
        min = Some(12.0),
        max = None,
        dflt = None,
        description = Some(
          """Sets the thickness of `pathbar` (in px). If not specified the `pathbar.textfont.size` is used with 3 pixles extra padding on each side."""
        )
      )

      case object textfont extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Sets the font used inside `pathbar`."""
        val editType: String    = """plot"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val familysrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  family .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val colorsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  color .""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val sizesrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  size .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val v1 =
            if (familysrc.json().length > 0)
              "\"familysrc\"" + ":" + familysrc.json()
            else ""
          val v2 =
            if (colorsrc.json().length > 0)
              "\"colorsrc\"" + ":" + colorsrc.json()
            else ""
          val v3 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v4 =
            if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
            else ""
          val v5 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("size")
            in0.map(ve => size.fromJson(ve))

            val in1 = mapKV.get("familysrc")
            in1.map(ve => familysrc.fromJson(ve))

            val in2 = mapKV.get("colorsrc")
            in2.map(ve => colorsrc.fromJson(ve))

            val in3 = mapKV.get("color")
            in3.map(ve => color.fromJson(ve))

            val in4 = mapKV.get("sizesrc")
            in4.map(ve => sizesrc.fromJson(ve))

            val in5 = mapKV.get("family")
            in5.map(ve => family.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (textfont.json().length > 0) "\"textfont\"" + ":" + textfont.json()
          else ""
        val v1 =
          if (side.json().length > 0) "\"side\"" + ":" + side.json() else ""
        val v2 =
          if (edgeshape.json().length > 0)
            "\"edgeshape\"" + ":" + edgeshape.json()
          else ""
        val v3 =
          if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
          else ""
        val v4 =
          if (thickness.json().length > 0)
            "\"thickness\"" + ":" + thickness.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("textfont")
          in0.map(ve => textfont.fromJson(ve))

          val in1 = mapKV.get("side")
          in1.map(ve => side.fromJson(ve))

          val in2 = mapKV.get("edgeshape")
          in2.map(ve => edgeshape.fromJson(ve))

          val in3 = mapKV.get("visible")
          in3.map(ve => visible.fromJson(ve))

          val in4 = mapKV.get("thickness")
          in4.map(ve => thickness.fromJson(ve))

        }
        this
      }

    }

    case object textfont extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String        = """object"""
      val description: String = """Sets the font used for `textinfo`."""
      val editType: String    = """plot"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal =
        PReal(min = Some(1.0), max = None, dflt = None, description = None)

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val familysrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  family .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  color .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = None
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val sizesrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  size .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val family: PChars = PChars(
        dflt = None,
        description = Some(
          """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val v1 =
          if (familysrc.json().length > 0)
            "\"familysrc\"" + ":" + familysrc.json()
          else ""
        val v2 =
          if (colorsrc.json().length > 0) "\"colorsrc\"" + ":" + colorsrc.json()
          else ""
        val v3 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v4 =
          if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
          else ""
        val v5 =
          if (family.json().length > 0) "\"family\"" + ":" + family.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("size")
          in0.map(ve => size.fromJson(ve))

          val in1 = mapKV.get("familysrc")
          in1.map(ve => familysrc.fromJson(ve))

          val in2 = mapKV.get("colorsrc")
          in2.map(ve => colorsrc.fromJson(ve))

          val in3 = mapKV.get("color")
          in3.map(ve => color.fromJson(ve))

          val in4 = mapKV.get("sizesrc")
          in4.map(ve => sizesrc.fromJson(ve))

          val in5 = mapKV.get("family")
          in5.map(ve => family.fromJson(ve))

        }
        this
      }

    }

    case object stream extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val maxpoints: PReal = PReal(
        min = Some(0.0),
        max = Some(10000.0),
        dflt = Some("""500.0"""),
        description = Some(
          """Sets the maximum number of points to keep on the plots from an incoming stream. If `maxpoints` is set to *50*, only the newest 50 points will be displayed on the plot."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val token: PChars = PChars(
        dflt = None,
        description = Some(
          """The stream id number links a data trace on a plot with a stream. See https://plot.ly/settings for more details."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (maxpoints.json().length > 0)
            "\"maxpoints\"" + ":" + maxpoints.json()
          else ""
        val v1 =
          if (token.json().length > 0) "\"token\"" + ":" + token.json() else ""
        val vars: List[String] = List(v0, v1)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("maxpoints")
          in0.map(ve => maxpoints.fromJson(ve))

          val in1 = mapKV.get("token")
          in1.map(ve => token.fromJson(ve))

        }
        this
      }

    }

    case object hoverlabel extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """none"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val alignsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  align .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolor: PColor = PColor(
        dflt = None,
        description = Some(
          """Sets the background color of the hover labels for this trace"""
        )
      )

      object align extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """auto"""
        val description: Option[String] = Some(
          """Sets the horizontal alignment of the text content within hover label box. Has an effect only if the hover label text spans more two or more lines"""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def auto(): Unit = { selected = TString("auto") }

        def right(): Unit = { selected = TString("right") }

        def left(): Unit = { selected = TString("left") }

      }

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelength: PInt = PInt(
        min = Some(-1),
        max = None,
        dflt = Some(15),
        description = Some(
          """Sets the default length (in number of characters) of the trace name in the hover labels for all traces. -1 shows the whole name regardless of length. 0-3 shows the first 0-3 characters, and an integer >3 will show the whole name if it is less than that many characters, but if it is longer, will truncate to `namelength - 3` characters and add an ellipsis."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelengthsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  namelength .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolor: PColor = PColor(
        dflt = None,
        description =
          Some("""Sets the border color of the hover labels for this trace.""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bordercolor .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bgcolor .""")
      )

      case object font extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Sets the font used in hover labels."""
        val editType: String    = """none"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val familysrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  family .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val colorsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  color .""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val sizesrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  size .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val v1 =
            if (familysrc.json().length > 0)
              "\"familysrc\"" + ":" + familysrc.json()
            else ""
          val v2 =
            if (colorsrc.json().length > 0)
              "\"colorsrc\"" + ":" + colorsrc.json()
            else ""
          val v3 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v4 =
            if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
            else ""
          val v5 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("size")
            in0.map(ve => size.fromJson(ve))

            val in1 = mapKV.get("familysrc")
            in1.map(ve => familysrc.fromJson(ve))

            val in2 = mapKV.get("colorsrc")
            in2.map(ve => colorsrc.fromJson(ve))

            val in3 = mapKV.get("color")
            in3.map(ve => color.fromJson(ve))

            val in4 = mapKV.get("sizesrc")
            in4.map(ve => sizesrc.fromJson(ve))

            val in5 = mapKV.get("family")
            in5.map(ve => family.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
        val v1 =
          if (alignsrc.json().length > 0) "\"alignsrc\"" + ":" + alignsrc.json()
          else ""
        val v2 =
          if (bgcolor.json().length > 0) "\"bgcolor\"" + ":" + bgcolor.json()
          else ""
        val v3 =
          if (align.json().length > 0) "\"align\"" + ":" + align.json() else ""
        val v4 =
          if (namelength.json().length > 0)
            "\"namelength\"" + ":" + namelength.json()
          else ""
        val v5 =
          if (namelengthsrc.json().length > 0)
            "\"namelengthsrc\"" + ":" + namelengthsrc.json()
          else ""
        val v6 =
          if (bordercolor.json().length > 0)
            "\"bordercolor\"" + ":" + bordercolor.json()
          else ""
        val v7 =
          if (bordercolorsrc.json().length > 0)
            "\"bordercolorsrc\"" + ":" + bordercolorsrc.json()
          else ""
        val v8 =
          if (bgcolorsrc.json().length > 0)
            "\"bgcolorsrc\"" + ":" + bgcolorsrc.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6, v7, v8)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("font")
          in0.map(ve => font.fromJson(ve))

          val in1 = mapKV.get("alignsrc")
          in1.map(ve => alignsrc.fromJson(ve))

          val in2 = mapKV.get("bgcolor")
          in2.map(ve => bgcolor.fromJson(ve))

          val in3 = mapKV.get("align")
          in3.map(ve => align.fromJson(ve))

          val in4 = mapKV.get("namelength")
          in4.map(ve => namelength.fromJson(ve))

          val in5 = mapKV.get("namelengthsrc")
          in5.map(ve => namelengthsrc.fromJson(ve))

          val in6 = mapKV.get("bordercolor")
          in6.map(ve => bordercolor.fromJson(ve))

          val in7 = mapKV.get("bordercolorsrc")
          in7.map(ve => bordercolorsrc.fromJson(ve))

          val in8 = mapKV.get("bgcolorsrc")
          in8.map(ve => bgcolorsrc.fromJson(ve))

        }
        this
      }

    }

    case object transforms extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""

      case object items extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        case object transform extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""
          val description: String =
            """An array of operations that manipulate the trace data, for example filtering or sorting the data arrays."""
          val editType: String = """calc"""

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val vars: List[String] = List()
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV => }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (transform.json().length > 0)
              "\"transform\"" + ":" + transform.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("transform")
            in0.map(ve => transform.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (items.json().length > 0) "\"items\"" + ":" + items.json() else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("items")
          in0.map(ve => items.fromJson(ve))

        }
        this
      }

    }

    case object outsidetextfont extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""
      val description: String =
        """Sets the font used for `textinfo` lying outside the sector. This option refers to the root of the hierarchy presented on top left corner of a treemap graph. Please note that if a hierarchy has multiple root nodes, this option won't have any effect and `insidetextfont` would be used."""
      val editType: String = """plot"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal =
        PReal(min = Some(1.0), max = None, dflt = None, description = None)

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val familysrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  family .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val colorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  color .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = None
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val sizesrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  size .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val family: PChars = PChars(
        dflt = None,
        description = Some(
          """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val v1 =
          if (familysrc.json().length > 0)
            "\"familysrc\"" + ":" + familysrc.json()
          else ""
        val v2 =
          if (colorsrc.json().length > 0) "\"colorsrc\"" + ":" + colorsrc.json()
          else ""
        val v3 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v4 =
          if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
          else ""
        val v5 =
          if (family.json().length > 0) "\"family\"" + ":" + family.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("size")
          in0.map(ve => size.fromJson(ve))

          val in1 = mapKV.get("familysrc")
          in1.map(ve => familysrc.fromJson(ve))

          val in2 = mapKV.get("colorsrc")
          in2.map(ve => colorsrc.fromJson(ve))

          val in3 = mapKV.get("color")
          in3.map(ve => color.fromJson(ve))

          val in4 = mapKV.get("sizesrc")
          in4.map(ve => sizesrc.fromJson(ve))

          val in5 = mapKV.get("family")
          in5.map(ve => family.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val v43 = "\"type\" : \"treemap\""
      val v0 =
        if (insidetextfont.json().length > 0)
          "\"insidetextfont\"" + ":" + insidetextfont.json()
        else ""
      val v1 =
        if (domain.json().length > 0) "\"domain\"" + ":" + domain.json() else ""
      val v2 =
        if (marker.json().length > 0) "\"marker\"" + ":" + marker.json() else ""
      val v3 =
        if (tiling.json().length > 0) "\"tiling\"" + ":" + tiling.json() else ""
      val v4 =
        if (pathbar.json().length > 0) "\"pathbar\"" + ":" + pathbar.json()
        else ""
      val v5 =
        if (textfont.json().length > 0) "\"textfont\"" + ":" + textfont.json()
        else ""
      val v6 =
        if (stream.json().length > 0) "\"stream\"" + ":" + stream.json() else ""
      val v7 =
        if (hoverlabel.json().length > 0)
          "\"hoverlabel\"" + ":" + hoverlabel.json()
        else ""
      val v8 =
        if (transforms.json().length > 0)
          "\"transforms\"" + ":" + transforms.json()
        else ""
      val v9 =
        if (outsidetextfont.json().length > 0)
          "\"outsidetextfont\"" + ":" + outsidetextfont.json()
        else ""
      val v10 =
        if (hovertemplate.json().length > 0)
          "\"hovertemplate\"" + ":" + hovertemplate.json()
        else ""
      val v11 =
        if (name.json().length > 0) "\"name\"" + ":" + name.json() else ""
      val v12 =
        if (count.json().length > 0) "\"count\"" + ":" + count.json() else ""
      val v13 =
        if (hovertemplatesrc.json().length > 0)
          "\"hovertemplatesrc\"" + ":" + hovertemplatesrc.json()
        else ""
      val v14 =
        if (hoverinfo.json().length > 0)
          "\"hoverinfo\"" + ":" + hoverinfo.json()
        else ""
      val v15 =
        if (maxdepth.json().length > 0) "\"maxdepth\"" + ":" + maxdepth.json()
        else ""
      val v16 =
        if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
        else ""
      val v17 =
        if (parents.json().length > 0) "\"parents\"" + ":" + parents.json()
        else ""
      val v18 =
        if (uirevision.json().length > 0)
          "\"uirevision\"" + ":" + uirevision.json()
        else ""
      val v19 =
        if (idssrc.json().length > 0) "\"idssrc\"" + ":" + idssrc.json() else ""
      val v20 =
        if (text.json().length > 0) "\"text\"" + ":" + text.json() else ""
      val v21 =
        if (parentssrc.json().length > 0)
          "\"parentssrc\"" + ":" + parentssrc.json()
        else ""
      val v22 =
        if (hoverinfosrc.json().length > 0)
          "\"hoverinfosrc\"" + ":" + hoverinfosrc.json()
        else ""
      val v23 =
        if (hovertextsrc.json().length > 0)
          "\"hovertextsrc\"" + ":" + hovertextsrc.json()
        else ""
      val v24 =
        if (valuessrc.json().length > 0)
          "\"valuessrc\"" + ":" + valuessrc.json()
        else ""
      val v25 =
        if (textposition.json().length > 0)
          "\"textposition\"" + ":" + textposition.json()
        else ""
      val v26 =
        if (labelssrc.json().length > 0)
          "\"labelssrc\"" + ":" + labelssrc.json()
        else ""
      val v27 =
        if (meta.json().length > 0) "\"meta\"" + ":" + meta.json() else ""
      val v28 =
        if (labels.json().length > 0) "\"labels\"" + ":" + labels.json() else ""
      val v29 =
        if (texttemplatesrc.json().length > 0)
          "\"texttemplatesrc\"" + ":" + texttemplatesrc.json()
        else ""
      val v30 =
        if (textsrc.json().length > 0) "\"textsrc\"" + ":" + textsrc.json()
        else ""
      val v31 =
        if (hovertext.json().length > 0)
          "\"hovertext\"" + ":" + hovertext.json()
        else ""
      val v32 =
        if (branchvalues.json().length > 0)
          "\"branchvalues\"" + ":" + branchvalues.json()
        else ""
      val v33 = if (uid.json().length > 0) "\"uid\"" + ":" + uid.json() else ""
      val v34 =
        if (values.json().length > 0) "\"values\"" + ":" + values.json() else ""
      val v35 =
        if (textinfo.json().length > 0) "\"textinfo\"" + ":" + textinfo.json()
        else ""
      val v36 =
        if (opacity.json().length > 0) "\"opacity\"" + ":" + opacity.json()
        else ""
      val v37 = if (ids.json().length > 0) "\"ids\"" + ":" + ids.json() else ""
      val v38 =
        if (metasrc.json().length > 0) "\"metasrc\"" + ":" + metasrc.json()
        else ""
      val v39 =
        if (customdata.json().length > 0)
          "\"customdata\"" + ":" + customdata.json()
        else ""
      val v40 =
        if (texttemplate.json().length > 0)
          "\"texttemplate\"" + ":" + texttemplate.json()
        else ""
      val v41 =
        if (level.json().length > 0) "\"level\"" + ":" + level.json() else ""
      val v42 =
        if (customdatasrc.json().length > 0)
          "\"customdatasrc\"" + ":" + customdatasrc.json()
        else ""
      val vars: List[String] = List(
        v43,
        v0,
        v1,
        v2,
        v3,
        v4,
        v5,
        v6,
        v7,
        v8,
        v9,
        v10,
        v11,
        v12,
        v13,
        v14,
        v15,
        v16,
        v17,
        v18,
        v19,
        v20,
        v21,
        v22,
        v23,
        v24,
        v25,
        v26,
        v27,
        v28,
        v29,
        v30,
        v31,
        v32,
        v33,
        v34,
        v35,
        v36,
        v37,
        v38,
        v39,
        v40,
        v41,
        v42
      )
      val cleanVars = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("insidetextfont")
        in0.map(ve => insidetextfont.fromJson(ve))

        val in1 = mapKV.get("domain")
        in1.map(ve => domain.fromJson(ve))

        val in2 = mapKV.get("marker")
        in2.map(ve => marker.fromJson(ve))

        val in3 = mapKV.get("tiling")
        in3.map(ve => tiling.fromJson(ve))

        val in4 = mapKV.get("pathbar")
        in4.map(ve => pathbar.fromJson(ve))

        val in5 = mapKV.get("textfont")
        in5.map(ve => textfont.fromJson(ve))

        val in6 = mapKV.get("stream")
        in6.map(ve => stream.fromJson(ve))

        val in7 = mapKV.get("hoverlabel")
        in7.map(ve => hoverlabel.fromJson(ve))

        val in8 = mapKV.get("transforms")
        in8.map(ve => transforms.fromJson(ve))

        val in9 = mapKV.get("outsidetextfont")
        in9.map(ve => outsidetextfont.fromJson(ve))

        val in10 = mapKV.get("hovertemplate")
        in10.map(ve => hovertemplate.fromJson(ve))

        val in11 = mapKV.get("name")
        in11.map(ve => name.fromJson(ve))

        val in12 = mapKV.get("count")
        in12.map(ve => count.fromJson(ve))

        val in13 = mapKV.get("hovertemplatesrc")
        in13.map(ve => hovertemplatesrc.fromJson(ve))

        val in14 = mapKV.get("hoverinfo")
        in14.map(ve => hoverinfo.fromJson(ve))

        val in15 = mapKV.get("maxdepth")
        in15.map(ve => maxdepth.fromJson(ve))

        val in16 = mapKV.get("visible")
        in16.map(ve => visible.fromJson(ve))

        val in17 = mapKV.get("parents")
        in17.map(ve => parents.fromJson(ve))

        val in18 = mapKV.get("uirevision")
        in18.map(ve => uirevision.fromJson(ve))

        val in19 = mapKV.get("idssrc")
        in19.map(ve => idssrc.fromJson(ve))

        val in20 = mapKV.get("text")
        in20.map(ve => text.fromJson(ve))

        val in21 = mapKV.get("parentssrc")
        in21.map(ve => parentssrc.fromJson(ve))

        val in22 = mapKV.get("hoverinfosrc")
        in22.map(ve => hoverinfosrc.fromJson(ve))

        val in23 = mapKV.get("hovertextsrc")
        in23.map(ve => hovertextsrc.fromJson(ve))

        val in24 = mapKV.get("valuessrc")
        in24.map(ve => valuessrc.fromJson(ve))

        val in25 = mapKV.get("textposition")
        in25.map(ve => textposition.fromJson(ve))

        val in26 = mapKV.get("labelssrc")
        in26.map(ve => labelssrc.fromJson(ve))

        val in27 = mapKV.get("meta")
        in27.map(ve => meta.fromJson(ve))

        val in28 = mapKV.get("labels")
        in28.map(ve => labels.fromJson(ve))

        val in29 = mapKV.get("texttemplatesrc")
        in29.map(ve => texttemplatesrc.fromJson(ve))

        val in30 = mapKV.get("textsrc")
        in30.map(ve => textsrc.fromJson(ve))

        val in31 = mapKV.get("hovertext")
        in31.map(ve => hovertext.fromJson(ve))

        val in32 = mapKV.get("branchvalues")
        in32.map(ve => branchvalues.fromJson(ve))

        val in33 = mapKV.get("uid")
        in33.map(ve => uid.fromJson(ve))

        val in34 = mapKV.get("values")
        in34.map(ve => values.fromJson(ve))

        val in35 = mapKV.get("textinfo")
        in35.map(ve => textinfo.fromJson(ve))

        val in36 = mapKV.get("opacity")
        in36.map(ve => opacity.fromJson(ve))

        val in37 = mapKV.get("ids")
        in37.map(ve => ids.fromJson(ve))

        val in38 = mapKV.get("metasrc")
        in38.map(ve => metasrc.fromJson(ve))

        val in39 = mapKV.get("customdata")
        in39.map(ve => customdata.fromJson(ve))

        val in40 = mapKV.get("texttemplate")
        in40.map(ve => texttemplate.fromJson(ve))

        val in41 = mapKV.get("level")
        in41.map(ve => level.fromJson(ve))

        val in42 = mapKV.get("customdatasrc")
        in42.map(ve => customdatasrc.fromJson(ve))

      }
      this
    }

  }

}
