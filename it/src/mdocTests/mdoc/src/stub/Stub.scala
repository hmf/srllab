package stub

trait Stub {

  def helloWorld: String

  def sayHello()

}