package splotly

import ujson.Value

trait JSON {
  def json(): String
  def fromJson(in:Value): this.type
  def fromJson(in:String): this.type = {
    val inJson = ujson.read(in)
    fromJson(inJson)
  }
}
