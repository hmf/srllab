package exp

import java.io.File

import org.graalvm.polyglot.{Context, HostAccess, Source, Value}
import javax.script.ScriptEngineManager
import javax.script.ScriptEngine
import javax.script.Invocable
import java.io.IOException
import java.nio.file.Paths

/**
 * mill mill.scalalib.GenIdea/idea
 *
 * mill mdocs.compile
 * mill mdocs.run
 * mill mdocs.{compile, run}
 * mill --watch mdocs.run
 *
 * mill -i mdocs.console
 * mill -i mdocs.repl
 *
 * mill -i mdocs.runMain mdocs.TestGraalJSDOM
 * mill --watch mdocs.runMain mdocs.TestGraalJSDOM
 *
 * mill -i sdk.runMain mdocs.TestGraalJSDOM
 * mill --watch sdk.runMain mdocs.TestGraalJSDOM
 *
 * This is a test to check if the ECMAScript 6 is running correctly.
 * We need to have support for version 6 because we will execute the
 * Plotly JS library in the Nashorn JS engine. This library expects ES6.
 *
 * https://vertx.io/
 *
 * @see https://en.wikipedia.org/wiki/List_of_ECMAScript_engines
 *      https://github.com/graalvm/graaljs
 *      https://github.com/oracle/graal/issues/651
 *      https://medium.com/graalvm/graalvms-javascript-engine-on-jdk11-with-high-performance-3e79f968a819
 *      https://github.com/graalvm/graal-js-jdk11-maven-demo
 *      https://www.graalvm.org/community/
 *      https://www.graalvm.org/docs/reference-manual/languages/js/
 *      https://github.com/graalvm/graaljs/blob/master/docs/user/JavaScriptCompatibility.md
 *
 */
object TestGraalJSDOM {

  /**
   * Example of loading library from a URL. Here we load the React
   * library. We need to use `allowIO(true)` and the
   * `allowExperimentalOptions(true)` with the appropriate flag set to true
   * `option("js.load-from-url", "true")`.
   *
   * @see https://github.com/graalvm/graaljs/blob/master/docs/user/JavaScriptCompatibility.md#loadsource
   *      https://github.com/graalvm/graaljs/blob/master/docs/user/JavaInterop.md
   *      https://github.com/graalvm/graaljs/blob/master/docs/user/NashornMigrationGuide.md
   *      https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/Engine.Builder.html
   *      https://www.graalvm.org/sdk/javadoc/org/graalvm/polyglot/Context.Builder.html
   *      https://medium.com/backticks-tildes/introduction-to-es6-modules-49956f580da
   *      https://github.com/graalvm/graaljs/issues/92
   */
  val script_load =
  """
    |
    |load("https://unpkg.com/react@16/umd/react.development.js");
    |load("https://unpkg.com/react-dom@16/umd/react-dom.development.js");
    |""".stripMargin

  /**
   * This is an example of the GraalJS accessing the Java classes. To do this
   * it is necessary that the following options be used:
   *
   * {{
   * val context: Context = Context.newBuilder("js")
   * .allowHostAccess(HostAccess.ALL)      // Allow host access to all Java methods methods
   * .allowHostClassLookup( s => true)     // Allow all Java classes to be looked up by the host
   * }}
   *
   * `allowHostClassLookup( s => true)` filters which classes can be accessed.
   * Here we allow any class to be used. `allowHostAccess(HostAccess.ALL)`
   * indicates which methods can be used. Here we allow all methods. Use this
   * to sandbox the JS scripts in the host engine.
   *
   * @see https://github.com/graalvm/graaljs/blob/master/docs/user/JavaScriptCompatibility.md#java
   *      https://github.com/graalvm/graaljs/blob/master/docs/user/JavaInterop.md
   */
  val script_java_interop =
  """
    |var Platform = Java.type("javafx.application.Platform");
    |var Timer    = Java.type("java.util.Timer");
    |
    |function returnSomething() {
    |    var something = new (Java.type('java.lang.String'))("Something");
    |    return something;
    |}
    |
    |function returnSomethingElse() {
    |    return "SomethingElse";
    |}
    |
    |""".stripMargin

  /**
   *  Load a local standard browserified library.
   *  Here we load Domino (a JS DOM implementation)
   *  @see https://github.com/fgnass/domino
   */
  val script_load_browserify_domino =
    """
      |
      |load("./mdocs/src/mdocs/dominot.js")
      |//console.log(domino)
      |if (domino == 'undefined' || domino == null) throw "Domino not loaded."
      |
      |
      |""".stripMargin

  /**
   * Best solution we have. We use Domino and CSSOM to emulate the browser
   * functionality. We load plotly and it tries to use the canvas. However
   * Domino's canvas is not implemented. This results in a Domino exception.
   * We would need to implement something like node-canvas that is used by
   * jsdom. Node-canvas is based on Cairo graphics.
   *
   * Note that we also need to implement the timer functions that are available
   * in the browsers.
   * We also need a DOMParser that we bypassed.
   *
   * @see https://github.com/fgnass/domino
   *      https://github.com/NV/CSSOM
   *      https://www.npmjs.com/package/canvas
   *      https://github.com/jsdom/jsdom
   *      https://www.cairographics.org/
   */
  val script_load_domino_cssom_plotly =
  """
    |
    |var Platform = Java.type("javafx.application.Platform");
    |var Timer    = Java.type("java.util.Timer");
    |
    |function setTimerRequest(handler, delay, interval, args) {
    |    handler = handler || function() {};
    |    delay = delay || 0;
    |    interval = interval || 0;
    |    var applyHandler = function(){ handler.apply(this, args); }
    |    var runLater = function(){ Platform.runLater(applyHandler); }
    |    var timer = new Timer("setTimerRequest", true);
    |    if (interval > 0) {
    |        timer.schedule(runLater, delay, interval);
    |    } else {
    |        timer.schedule(runLater, delay);
    |    }
    |    return timer;
    |}
    |
    |function clearTimerRequest(timer) {
    |    timer.cancel();
    |}
    |
    |function setInterval() {
    |    var args = Array.prototype.slice.call(arguments);
    |    var handler = args.shift();
    |    var ms = args.shift();
    |    return setTimerRequest(handler, ms, ms, args);
    |}
    |
    |function clearInterval(timer) {
    |    clearTimerRequest(timer);
    |}
    |
    |function setTimeout() {
    |    var args = Array.prototype.slice.call(arguments);
    |    var handler = args.shift();
    |    var ms = args.shift();
    |
    |    return setTimerRequest(handler, ms, 0, args);
    |}
    |function clearTimeout(timer) {
    |    clearTimerRequest(timer);
    |}
    |
    |function setImmediate() {
    |    var args = Array.prototype.slice.call(arguments);
    |    var handler = args.shift();
    |
    |    return setTimerRequest(handler, 0, 0, args);
    |}
    |
    |function clearImmediate(timer) {
    |    clearTimerRequest(timer);
    |}
    |
    |// Make sure we have a CSSStyleSheet for PlotLy
    |// See https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleSheet/insertRule
    |load("./mdocs/src/mdocs/cssom.js")
    |
    |// Make sure we have a working DOM for jsdom
    |load("./mdocs/src/mdocs/domino.js")
    |//console.log(domino);
    |//console.log(domino.createWindow);
    |
    |// Create a global window for jsdom
    |var window = domino.createWindow('<h1>Hello world</h1>', 'http://example.com');
    |var document = window.document;
    |
    |// Add the style sheet to the window
    |var s = new cssom.CSSStyleSheet;
    |s.insertRule("a {color: blue}", 0);
    |// Only prints something if a style has been added
    |//console.log('styleSheet s = ' + s);
    |
    |// PlotLy requires a style sheet with a specific ID
    |var uid = 'global'
    |var id = 'plotly.js-style-' + uid;
    |var style = document.getElementById(id);
    |if(!style) {
    |  //console.log('No CSS found!');
    |  style = document.createElement('style');
    |  //console.log(style);
    |  style.setAttribute('id', id);
    |  // WebKit hack :(
    |  style.appendChild(document.createTextNode(''));
    |  document.head.appendChild(style);
    |  style.sheet = s;
    |}
    |var styleSheet = style.sheet;
    |style.sheet.insertRule('.foo{color:red;}', 0);
    |//console.log('styleSheet style.sheet = ' + styleSheet);
    |
    |// Add a shim for the DOMParser. PlotLy needs this. jsdom can provide this.
    |//window.DOMParser = function() {};
    |var DOMParser = function() {};
    |
    |/* This will fail with:
    |Exception in thread "main" Error: NotYetImplemented
    |	at <js> exports.nyi(mdocs/src/mdocs/domino.js:23327:759865-759894)
    |	at <js> 551(mdocs/src/mdocs/plotly-latest.js:76405:2989460-2989489)
    |
    | PlotLy expects a browser with a Canvas:
    |
    | if(typeof document !== 'undefined') {
    |  defaultCanvas = document.createElement('canvas')
    |  defaultCanvas.width = 8192
    |  defaultCanvas.height = 1024
    |  defaultContext = defaultCanvas.getContext("2d")
    |}
    |
    |Uses"vectorize-text":551
    |see https://github.com/mikolalysenko/vectorize-text
    |*/
    |
    |try {
    |
    |  //load("https://cdn.plot.ly/plotly-latest.min.js");
    |  //load("https://cdn.plot.ly/plotly-latest.js");
    |  load("./mdocs/src/mdocs/plotly-latest.js");
    |  throw "Should fail with 'Error: NotYetImplemented'";
    |}
    |catch {
    |  // Should fail
    |}
    |
    |
    |""".stripMargin

  /**
   * Same as [[script_load_domino_cssom_plotly]] but now we try and monkey
   * patch the canvas class. But tis is not working.
   *
   * @see https://github.com/fgnass/domino/issues/113
   */
  val script_canvas_plotly =
    """
      |
      |var Platform = Java.type("javafx.application.Platform");
      |var Timer    = Java.type("java.util.Timer");
      |
      |function setTimerRequest(handler, delay, interval, args) {
      |    handler = handler || function() {};
      |    delay = delay || 0;
      |    interval = interval || 0;
      |    var applyHandler = function(){ handler.apply(this, args); }
      |    var runLater = function(){ Platform.runLater(applyHandler); }
      |    var timer = new Timer("setTimerRequest", true);
      |    if (interval > 0) {
      |        timer.schedule(runLater, delay, interval);
      |    } else {
      |        timer.schedule(runLater, delay);
      |    }
      |    return timer;
      |}
      |
      |function clearTimerRequest(timer) {
      |    timer.cancel();
      |}
      |
      |function setInterval() {
      |    var args = Array.prototype.slice.call(arguments);
      |    var handler = args.shift();
      |    var ms = args.shift();
      |    return setTimerRequest(handler, ms, ms, args);
      |}
      |
      |function clearInterval(timer) {
      |    clearTimerRequest(timer);
      |}
      |
      |function setTimeout() {
      |    var args = Array.prototype.slice.call(arguments);
      |    var handler = args.shift();
      |    var ms = args.shift();
      |
      |    return setTimerRequest(handler, ms, 0, args);
      |}
      |function clearTimeout(timer) {
      |    clearTimerRequest(timer);
      |}
      |
      |function setImmediate() {
      |    var args = Array.prototype.slice.call(arguments);
      |    var handler = args.shift();
      |
      |    return setTimerRequest(handler, 0, 0, args);
      |}
      |
      |function clearImmediate(timer) {
      |    clearTimerRequest(timer);
      |}
      |
      |
      |//
      |// Mock Canvas / Context2D calls
      |//
      |function mockCanvas (win) {
      |    win.HTMLCanvasElement.prototype.getContext = function () {
      |        return {
      |            fillRect: function() {},
      |            clearRect: function(){},
      |            getImageData: function(x, y, w, h) {
      |                return  {
      |                    data: new Array(w*h*4)
      |                };
      |            },
      |            putImageData: function() {},
      |            createImageData: function(){ return []},
      |            setTransform: function(){},
      |            drawImage: function(){},
      |            save: function(){},
      |            fillText: function(){},
      |            restore: function(){},
      |            beginPath: function(){},
      |            moveTo: function(){},
      |            lineTo: function(){},
      |            closePath: function(){},
      |            stroke: function(){},
      |            translate: function(){},
      |            scale: function(){},
      |            rotate: function(){},
      |            arc: function(){},
      |            fill: function(){},
      |            measureText: function(){
      |                return { width: 0 };
      |            },
      |            transform: function(){},
      |            rect: function(){},
      |            clip: function(){},
      |        };
      |    }
      |
      |    win.HTMLCanvasElement.prototype.toDataURL = function () {
      |        return "";
      |    }
      |}
      |
      |// Make sure we have a CSSStyleSheet for PlotLy
      |// See https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleSheet/insertRule
      |load("./mdocs/src/mdocs/cssom.js")
      |
      |// Make sure we have a working DOM for jsdom
      |load("./mdocs/src/mdocs/domino.js")
      |//console.log(domino);
      |//console.log(domino.createWindow);
      |
      |// Create a global window for jsdom
      |var window = domino.createWindow('<h1>Hello world</h1>', 'http://example.com');
      |mockCanvas(window);
      |var document = window.document;
      |
      |// Add the style sheet to the window
      |var s = new cssom.CSSStyleSheet;
      |s.insertRule("a {color: blue}", 0);
      |// Only prints something if a style has been added
      |//console.log('styleSheet s = ' + s);
      |
      |// PlotLy requires a style sheet with a specific ID
      |var uid = 'global'
      |var id = 'plotly.js-style-' + uid;
      |var style = document.getElementById(id);
      |if(!style) {
      |  //console.log('No CSS found!');
      |  style = document.createElement('style');
      |  //console.log(style);
      |  style.setAttribute('id', id);
      |  // WebKit hack :(
      |  style.appendChild(document.createTextNode(''));
      |  document.head.appendChild(style);
      |  style.sheet = s;
      |}
      |var styleSheet = style.sheet;
      |style.sheet.insertRule('.foo{color:red;}', 0);
      |//console.log('styleSheet style.sheet = ' + styleSheet);
      |
      |// Add a shim for the DOMParser. PlotLy needs this. jsdom can provide this.
      |//window.DOMParser = function() {};
      |var DOMParser = function() {};
      |
      |/* This will fail with:
      |Exception in thread "main" Error: NotYetImplemented
      |	at <js> exports.nyi(mdocs/src/mdocs/domino.js:23327:759865-759894)
      |	at <js> 551(mdocs/src/mdocs/plotly-latest.js:76405:2989460-2989489)
      |
      | PlotLy expects a browser with a Canvas:
      |
      | if(typeof document !== 'undefined') {
      |  defaultCanvas = document.createElement('canvas')
      |  defaultCanvas.width = 8192
      |  defaultCanvas.height = 1024
      |  defaultContext = defaultCanvas.getContext("2d")
      |}
      |
      |Uses"vectorize-text":551
      |see https://github.com/mikolalysenko/vectorize-text
      |*/
      |
      |try {
      |
      |  //load("https://cdn.plot.ly/plotly-latest.min.js");
      |  //load("https://cdn.plot.ly/plotly-latest.js");
      |  load("./mdocs/src/mdocs/plotly-latest.js");
      |  throw "Should fail with 'Error: NotYetImplemented'";
      |}
      |catch {
      |  // Should fail
      |}
      |
      |""".stripMargin


  /**
   * Something goes wrong when we execute the Browserifed jsdom prelude when
   * we create a glabal window. When this prelude exists to load jsdom, the
   * JSDOM class is not visible anymore. What does this mean? It means that if
   * we simulate a global window, jsdom cannot be loaded to simulate the rest
   * of the browser/node.js environment.
   *
   * TODO: create a test in a single script
   *
   * @see https://javascript.info/global-object
   *      https://github.com/lukechilds/window
   */
  val script_load_jsdom_2 =
    """
      |
      |// Make sure we have a working DOM for jsdom
      |load("./mdocs/src/mdocs/dominot_1.js")
      |//console.log(domino);
      |//console.log(domino.createWindow);
      |
      |// Create a global window for jsdom
      |//var something = domino.createWindow('<h1>Hello world</h1>', 'http://example.com'); // ok
      |//var window = domino.createWindow('<h1>Hello world</h1>', 'http://example.com');    // jsdom is not defined
      |//var global = {}
      |//global.window = domino.createWindow('<h1>Hello world</h1>', 'http://example.com');    // jsdom is not defined
      |w = domino.createWindow('<h1>Hello world</h1>', 'http://example.com');    // ok but can we use it?
      |//Object.assign(global, w)
      |global.window = w
      |
      |load("./mdocs/src/mdocs/jsdomt_2.js")
      |console.log("       global = " + global);
      |console.log("global.window = " + global.window);
      |console.log(" global.jsdom = " + global.jsdom);
      |if (jsdom == 'undefined' || jsdom == null) console.log("jsdom not loaded.") //throw "jsdom not loaded.";
      |//console.log("  jsdom.JSDOM = " + jsdom.JSDOM);
      |if (jsdom == 'undefined' || jsdom == null) console.log("jsdom not loaded.") //throw "jsdom not loaded.";
      |
      |""".stripMargin


  def executePolyglotJS_T(script: String): Unit = {
    val currentRelativePath = Paths.get("")
    val root = currentRelativePath.toAbsolutePath.toString
    println(s"root = $root")
    val s = File.separatorChar
    val jsPath = root + s + "exp" + s + "src" + s + "exp"
    println(s"jsPath = $jsPath")


    val context: Context = Context.newBuilder("js")
      .allowIO(true)
      //.allowHostAccess(true)
      .allowHostAccess(HostAccess.ALL)      // Allow host access to all Java methods methods
      .allowHostClassLookup( s => true)     // Allow all Java classes to be looked up by the host
      .allowExperimentalOptions(true)
      .option("js.load-from-url", "true")   // Load from external URL
      //.option("js.scripting", "true")       // Nashorn comparability
      .build()
    context.eval("js", script)
    val setInterval: Value = context.getBindings("js").getMember("returnSomething")
    println(setInterval+"\n")
    //val out = setInterval.execute()
    //println(s"out = $out")
  }

  def main(args: Array[String]): Unit = {
    executePolyglotJS_T(script_load)                     // Ok
    executePolyglotJS_T(script_java_interop)             // Ok
    executePolyglotJS_T(script_load_browserify_domino)   // Ok
    executePolyglotJS_T(script_load_domino_cssom_plotly) // Ok load, but execution fails
    executePolyglotJS_T(script_canvas_plotly)            // Ok load, but execution fails
    executePolyglotJS_T(script_load_jsdom_2)             // NOT Ok, Graal global error?
  }
}
