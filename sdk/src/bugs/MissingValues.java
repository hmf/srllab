package bugs;

import tech.tablesaw.api.*;
import tech.tablesaw.columns.numbers.DoubleColumnType;
import tech.tablesaw.columns.numbers.IntColumnType;
import tech.tablesaw.plotly.Plot;
import tech.tablesaw.plotly.api.Histogram;

import java.io.IOException;
import java.util.Arrays;


/**
 * Examples to check the JTablesaw code.
 *
 *  mill -i sdk.runMain bugs.MissingValues
 */
public class MissingValues {

    static public void dropMissingValuesColumn() {
        IntColumn intColumn = IntColumn.create("");
        intColumn.append(0);
        intColumn.append(0);
        intColumn.append(1);
        //System.out.println("intColumn before: " + Arrays.toString(intColumn.asObjectArray()));
        double r[] = {0.0, 0.0, 1.0};
        assert(Arrays.equals(r, intColumn.asDoubleArray()));

        DoubleColumn doubleColumn = intColumn.asDoubleColumn();
        DoubleColumn doubleColumnMissing = intColumn.asDoubleColumn();

        intColumn = intColumn.unique();
        assert(intColumn.size() == 2 );
        //System.out.println("intColumn after unique: " + Arrays.toString(intColumn.asObjectArray()));
        r = new double[] {0.0, 1.0};
        assert(Arrays.equals(r, intColumn.asDoubleArray()));

        doubleColumn = doubleColumn.unique();
        assert(doubleColumn.size() == 2);
        //System.out.println("doubleColumn after unique: " + Arrays.toString(doubleColumn.asObjectArray()));
        r = new double[] {0.0, 1.0};
        assert(Arrays.equals(r, doubleColumn.asDoubleArray()));

        doubleColumnMissing.set(doubleColumnMissing.isEqualTo(0), DoubleColumnType.missingValueIndicator());
        //System.out.println("doubleColumnMissing before (print): |" + doubleColumnMissing.print()+"|");
        //System.out.println("doubleColumnMissing before (array): " + Arrays.toString(doubleColumnMissing.asObjectArray()));
        assert(doubleColumnMissing.size() == 3);
        Double d[] = {null, null, 1.0};
        assert(Arrays.equals(d, doubleColumnMissing.asObjectArray()));

        DoubleColumn doubleColumnMissingRemoved = doubleColumnMissing.removeMissing();
        //System.out.println("doubleColumnMissingRemoved before (array): " + Arrays.toString(doubleColumnMissingRemoved.asObjectArray()));
        assert(doubleColumnMissingRemoved.size() == 1);
        d = new Double[] {1.0};
        assert(Arrays.equals(d, doubleColumnMissingRemoved.asObjectArray()));

        doubleColumnMissing = doubleColumnMissing.unique();
        //System.out.println("doubleColumnMissing after (array): " + Arrays.toString(doubleColumnMissing.asObjectArray()));
        assert(doubleColumnMissing.size() == 1);
        assert(Arrays.equals(d, doubleColumnMissing.asObjectArray()));

    }

    /**
     * Example of accessing a column and marking missing values.
     *
     * @return
     * @throws java.io.IOException
     */
    static public Table dropMissingValuesTable() throws java.io.IOException {
        Table property = Table.read().csv("./data/tablesaw/sacramento_real_estate_transactions.csv");

        IntColumn sqft = property.intColumn("sq__ft");
        //System.out.println(property.rowCount());
        assert(property.rowCount() == 985);

        sqft.set(sqft.isEqualTo(0), IntColumnType.missingValueIndicator());
        property = property.dropRowsWithMissingValues();
        //System.out.println(property.rowCount());
        assert(property.rowCount() == 814);

        return property;
    }

    static public Table dropMissingValuesTable2() throws java.io.IOException {
        Table property = Table.read().csv("./data/tablesaw/sacramento_real_estate_transactions.csv");


        assert(property.column("sq__ft").type() == ColumnType.INTEGER);
        DoubleColumn sqft = property.numberColumn("sq__ft").asDoubleColumn();
        assert(property.rowCount() == 985);

        sqft.set(sqft.isEqualTo(0.0), DoubleColumnType.missingValueIndicator());
        property = property.dropRowsWithMissingValues();
        assert(property.rowCount() != 814);
        assert(property.rowCount() == 985);

        return property;
    }

    static public Table dropMissingValuesTable3() throws java.io.IOException {
        Table property = Table.read().csv("./data/tablesaw/sacramento_real_estate_transactions.csv");


        assert(property.column("sq__ft").type() == ColumnType.INTEGER);
        DoubleColumn sqft = property.intColumn("sq__ft").asDoubleColumn();
        assert(property.rowCount() == 985);

        sqft.set(sqft.isEqualTo(0.0), DoubleColumnType.missingValueIndicator());
        property = property.dropRowsWithMissingValues();
        assert(property.rowCount() != 814);
        assert(property.rowCount() == 985);

        return property;
    }

    static public Table dropMissingValuesTable4() throws java.io.IOException {
        Table property = Table.read().csv("./data/tablesaw/sacramento_real_estate_transactions.csv");


        assert(property.column("sq__ft").type() == ColumnType.INTEGER);
        NumericColumn sqft = property.numberColumn("sq__ft");
        //DoubleColumn sqft = property.doubleColumn("sq__ft");
        assert(property.rowCount() == 985);

        sqft.set(sqft.isEqualTo(0.0), IntColumnType.missingValueIndicator());
        //sqft.set(sqft.isEqualTo(0.0), DoubleColumnType.missingValueIndicator());
        property = property.dropRowsWithMissingValues();
        assert(property.rowCount() == 814);

        return property;
    }

    static public void countMissingValuesTable() throws java.io.IOException {
        Table property = Table.read().csv("./data/tablesaw/sacramento_real_estate_transactions.csv");

        IntColumn sqft = property.intColumn("sq__ft");
        //System.out.println(property.rowCount());
        assert(property.rowCount() == 985);

        sqft.set(sqft.isEqualTo(0), IntColumnType.missingValueIndicator());

        Table missing = property.missingValueCounts();
        DoubleColumn missing_sqft = missing.doubleColumn("Missing Values [sq__ft]");
        assert(missing_sqft.size() == 1);
        assert(missing_sqft.getDouble(0) == 171.0);
        String s = missing.printAll();
        String[] ss = s.split("\n");
        assert(ss[0].equals("                                                                                                                                              sacramento_real_estate_transactions.csv summary                                                                                                                                              "));
        assert(ss[1].equals(" Missing Values [zip]  |  Missing Values [city]  |  Missing Values [latitude]  |  Missing Values [type]  |  Missing Values [sq__ft]  |  Missing Values [baths]  |  Missing Values [sale_date]  |  Missing Values [street]  |  Missing Values [price]  |  Missing Values [state]  |  Missing Values [beds]  |  Missing Values [longitude]  |"));
        assert(ss[2].equals("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"));
        assert(ss[3].equals("                  0.0  |                    0.0  |                        0.0  |                    0.0  |                    171.0  |                     0.0  |                         0.0  |                      0.0  |                     0.0  |                     0.0  |                    0.0  |                         0.0  |"));
    }

    static public void plotMissingValuesTable() throws java.io.IOException {
        Table property = dropMissingValuesTable();
        Plot.show(Histogram.create("Distribution of property sizes", property, "sq__ft"));
    }

    public static void main(String[] args) throws java.io.IOException {
        //dropMissingValuesColumn();
        dropMissingValuesTable();
        dropMissingValuesTable2();
        dropMissingValuesTable3();
        dropMissingValuesTable4();
        countMissingValuesTable();
        plotMissingValuesTable();
    }
}
