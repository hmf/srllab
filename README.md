# sRLLab

Scala Reinforcement Learnng Lab

## Installation

### JDK Installation

Tested on JDK 11. because the JDK does not include the JavaFX run-time, this 
has to be installed. For Ubuntu we need to:

* sudo apt install openjfx
* dpkg-query -L openjfx
* java --module-path /usr/share/openjfx/lib --add-modules=javafx.controls,javafx.fxml,javafx.base,javafx.media,javafx.web,javafx.swing -jar '/home/lotfi/Documents/MyAppfolder/my_application.jar'

The `dpkg-query` provides us with the the path to the OpenJFX run-time. This 
path and the required modules must be added to the `java` comma nd line 
arguments.    

### Launching Mill with the OpenFX modules

The Mill MDoc ad Laika modules use JavaFX/OpenFX code. So Mill must be launched 
with the JVM parameters that allow it to find and load the JavaFX libraries 
during boot time. These parameters may be passed onto the Mill JVM via 
parameters. In order to facilitate this process we have the following command
that generates and passes the relevant parameters to the Mill application: 

```
./mill -i sdk.genJFXMill
```  

The above command generates the following script in the project root:

```
jfxmill.sh
```

Use the `jfxmill` CLI instead of the `./mill` command. This new script is a 
drop-in replacement for Mill. Use all Mill arguments as before. 

## Mill Module Development

### Testing the Mill  Module locally

The Mill modules must be made available as JAR archives in order to 
facilitate distribution and reuse. The following command compiles the Mill 
modules and generates a local Ivy cache using the 
[Integration test plugin for mill plugins](https://github.com/lefou/mill-integrationtest)
and execute the respective integration tests:

```
./jfxmill.sh -i testIt
```

The following command compiles the Mill modules and generates a local Ivy cache and 
execute the respective modules unit and integration tests:

```
./jfxmill.sh -i test
```

Currently the artifacts are published locally to the project (the `it` module's
destination path), for example:

```
/home/user/IdeaProjects/srllab/out/it/test/dest/ivyRepo/local
```

## Modules

### WebKit

Uses JFX's `WebKitView` to visualize plots in HMTL. It also allows us to 
capture those HTML pages as screen shots to raster format suich as PNG 
and TIFF.


### Plots

This is a wrapper for Plotly.Js. 

### MarkDoc

This module uses [Scalameta's](https://github.com/scalameta) 
[MDoc](https://github.com/scalameta/mdoc) utility to parse Scala code fences
and perform type checking, execution and result output generation. This is
used to embed code examples in the Markdown documents and ensure that they
are valid (compilable and executable) examples.
  

