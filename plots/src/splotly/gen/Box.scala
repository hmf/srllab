package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Box() extends Trace {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val type_ : String      = """box"""
  val animatable: Boolean = false
  val categories: Array[String] = Array(
    """zoomScale""",
    """boxLayout""",
    """showLegend""",
    """box-violin""",
    """oriented""",
    """symbols""",
    """svg""",
    """cartesian"""
  )

  case object layoutAttributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val boxgroupgap: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = Some("""0.3"""),
      description = Some(
        """Sets the gap (in plot fraction) between boxes of the same location coordinate. Has no effect on traces that have *width* set."""
      )
    )

    object boxmode extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """overlay"""
      val description: Option[String] = Some(
        """Determines how boxes at the same location coordinate are displayed on the graph. If *group*, the boxes are plotted next to one another centered around the shared location. If *overlay*, the boxes are plotted over one another, you might need to set *opacity* to see them multiple boxes. Has no effect on traces that have *width* set."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def overlay(): Unit = { selected = TString("overlay") }

      def group(): Unit = { selected = TString("group") }

    }

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val boxgap: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = Some("""0.3"""),
      description = Some(
        """Sets the gap (in plot fraction) between boxes of adjacent location coordinates. Has no effect on traces that have *width* set."""
      )
    )

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val v0 =
        if (boxgroupgap.json().length > 0)
          "\"boxgroupgap\"" + ":" + boxgroupgap.json()
        else ""
      val v1 =
        if (boxmode.json().length > 0) "\"boxmode\"" + ":" + boxmode.json()
        else ""
      val v2 =
        if (boxgap.json().length > 0) "\"boxgap\"" + ":" + boxgap.json() else ""
      val vars: List[String] = List(v0, v1, v2)
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("boxgroupgap")
        in0.map(ve => boxgroupgap.fromJson(ve))

        val in1 = mapKV.get("boxmode")
        in1.map(ve => boxmode.fromJson(ve))

        val in2 = mapKV.get("boxgap")
        in2.map(ve => boxgap.fromJson(ve))

      }
      this
    }

  }

  case object meta extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val description: String =
      """Each box spans from quartile 1 (Q1) to quartile 3 (Q3). The second quartile (Q2, i.e. the median) is marked by a line inside the box. The fences grow outward from the boxes' edges, by default they span +/- 1.5 times the interquartile range (IQR: Q3-Q1), The sample mean and standard deviation as well as notches and the sample, outlier and suspected outliers points can be optionally added to the box plot. The values and positions corresponding to each boxes can be input using two signatures. The first signature expects users to supply the sample values in the `y` data array for vertical boxes (`x` for horizontal boxes). By supplying an `x` (`y`) array, one box per distinct `x` (`y`) value is drawn If no `x` (`y`) {array} is provided, a single box is drawn. In this case, the box is positioned with the trace `name` or with `x0` (`y0`) if provided. The second signature expects users to supply the boxes corresponding Q1, median and Q3 statistics in the `q1`, `median` and `q3` data arrays respectively. Other box features relying on statistics namely `lowerfence`, `upperfence`, `notchspan` can be set directly by the users. To have plotly compute them or to show sample points besides the boxes, users can set the `y` data array for vertical boxes (`x` for horizontal boxes) to a 2D array with the outer length corresponding to the number of boxes in the traces and the inner length corresponding the sample size."""

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val vars: List[String] = List()
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV => }
      this
    }

  }

  case object attributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val type_ : String = """box"""

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplate: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Template string used for rendering the information that appear on hover box. Note that this will override `hoverinfo`. Variables are inserted using %{variable}, for example "y: %{y}". Numbers are formatted using d3-format's syntax %{variable:d3-format}, for example "Price: %{y:$.2f}". https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format for details on the formatting syntax. Dates are formatted using d3-time-format's syntax %{variable|d3-time-format}, for example "Day: %{2019-01-01|%A}". https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format for details on the date formatting syntax. The variables available in `hovertemplate` are the ones emitted as event data described at this link https://plot.ly/javascript/plotlyjs-events/#event-data. Additionally, every attributes that can be specified per-point (the ones that are `arrayOk: true`) are available.  Anything contained in tag `<extra>` is displayed in the secondary box, for example "<extra>{fullData.name}</extra>". To hide the secondary box completely, use an empty tag `<extra></extra>`."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val xsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  x .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val x: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the x sample data or coordinates. See overview for more info."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val name: PChars = PChars(
      dflt = None,
      description = Some(
        """Sets the trace name. The trace name appear as the legend item and on hover. For box traces, the name will also be used for the position coordinate, if `x` and `x0` (`y` and `y0` if horizontal) are missing and the position axis is categorical"""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val notched: PBool = PBool(
      dflt = None,
      description = Some(
        """Determines whether or not notches are drawn. Notches displays a confidence interval around the median. We compute the confidence interval as median +/- 1.57 * IQR / sqrt(N), where IQR is the interquartile range and N is the sample size. If two boxes' notches do not overlap there is 95% confidence their medians differ. See https://sites.google.com/site/davidsstatistics/home/notched-box-plots for more info. Defaults to *false* unless `notchwidth` or `notchspan` is set."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dy: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description = Some(
        """Sets the y coordinate step for multi-box traces set using q1/median/q3."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplatesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertemplate .""")
    )

    object ycalendar extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """gregorian"""
      val description: Option[String] = Some(
        """Sets the calendar system to use with `y` date data."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def ummalqura(): Unit = { selected = TString("ummalqura") }

      def thai(): Unit = { selected = TString("thai") }

      def taiwan(): Unit = { selected = TString("taiwan") }

      def jalali(): Unit = { selected = TString("jalali") }

      def persian(): Unit = { selected = TString("persian") }

      def nepali(): Unit = { selected = TString("nepali") }

      def nanakshahi(): Unit = { selected = TString("nanakshahi") }

      def mayan(): Unit = { selected = TString("mayan") }

      def julian(): Unit = { selected = TString("julian") }

      def islamic(): Unit = { selected = TString("islamic") }

      def hebrew(): Unit = { selected = TString("hebrew") }

      def ethiopian(): Unit = { selected = TString("ethiopian") }

      def discworld(): Unit = { selected = TString("discworld") }

      def coptic(): Unit = { selected = TString("coptic") }

      def chinese(): Unit = { selected = TString("chinese") }

      def gregorian(): Unit = { selected = TString("gregorian") }

    }

    object hoverinfo extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = Some("""all""")
      val description: Option[String] = Some(
        """Determines which trace information appear on hover. If `none` or `skip` are set, no information is displayed upon hovering. But, if `none` is set, click and hover events are still fired."""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def nameOn(): Unit  = { flag_ + "name" }
      def nameOff(): Unit = { flag_ - "name" }
      def textOn(): Unit  = { flag_ + "text" }
      def textOff(): Unit = { flag_ - "text" }
      def zOn(): Unit     = { flag_ + "z" }
      def zOff(): Unit    = { flag_ - "z" }
      def yOn(): Unit     = { flag_ + "y" }
      def yOff(): Unit    = { flag_ - "y" }
      def xOn(): Unit     = { flag_ + "x" }
      def xOff(): Unit    = { flag_ - "x" }
      def skipOn(): Unit  = { flag_ + "skip" }
      def skipOff(): Unit = { flag_ - "skip" }
      def noneOn(): Unit  = { flag_ + "none" }
      def noneOff(): Unit = { flag_ - "none" }
      def allOn(): Unit   = { flag_ + "all" }
      def allOff(): Unit  = { flag_ - "all" }
    }

    object visible extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Boolean = true
      val description: Option[String] = Some(
        """Determines whether or not this trace is visible. If *legendonly*, the trace is not drawn, but can appear as a legend item (provided that the legend itself is visible)."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) => t.toString
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => TBoolean(s)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def true_(): Unit = { selected = TBoolean(true) }

      def false_(): Unit = { selected = TBoolean(false) }

      def legendonly(): Unit = { selected = TString("legendonly") }

    }

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val y: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the y sample data or coordinates. See overview for more info."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val meansrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  mean .""")
    )

    object boxpoints extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val description: Option[String] = Some(
        """If *outliers*, only the sample points lying outside the whiskers are shown If *suspectedoutliers*, the outlier points are shown and points either less than 4*Q1-3*Q3 or greater than 4*Q3-3*Q1 are highlighted (see `outliercolor`) If *all*, all sample points are shown If *false*, only the box(es) are shown with no sample points Defaults to *suspectedoutliers* when `marker.outliercolor` or `marker.line.outliercolor` is set. Defaults to *all* under the q1/median/q3 signature. Otherwise defaults to *outliers*."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) => t.toString
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => TBoolean(s)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def all(): Unit = { selected = TString("all") }

      def outliers(): Unit = { selected = TString("outliers") }

      def suspectedoutliers(): Unit = {
        selected = TString("suspectedoutliers")
      }

      def false_(): Unit = { selected = TBoolean(false) }

    }

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val jitter: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = None,
      description = Some(
        """Sets the amount of jitter in the sample points drawn. If *0*, the sample points align along the distribution axis. If *1*, the sample points are drawn in a random jitter of width equal to the width of the box(es)."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uirevision: PAny = PAny(
      dflt = None,
      description = Some(
        """Controls persistence of some user-driven changes to the trace: `constraintrange` in `parcoords` traces, as well as some `editable: true` modifications such as `name` and `colorbar.title`. Defaults to `layout.uirevision`. Note that other user-driven trace attribute changes are controlled by `layout` attributes: `trace.visible` is controlled by `layout.legend.uirevision`, `selectedpoints` is controlled by `layout.selectionrevision`, and `colorbar.(x|y)` (accessible with `config: {editable: true}`) is controlled by `layout.editrevision`. Trace changes are tracked by `uid`, which only falls back on trace index if no `uid` is provided. So if your app can add/remove traces before the end of the `data` array, such that the same trace has a different index, you can still preserve user-driven changes if you give each trace a `uid` that stays with it as it moves."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val notchspansrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  notchspan .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val mean: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the mean values. There should be as many items as the number of boxes desired. This attribute has effect only under the q1/median/q3 signature. If `mean` is not provided but a sample (in `y` or `x`) is set, we compute the mean for each box using the sample values."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val y0: PAny = PAny(
      dflt = None,
      description = Some(
        """Sets the y coordinate for single-box traces or the starting coordinate for multi-box traces set using q1/median/q3. See overview for more info."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ysrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  y .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val idssrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  ids .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val lowerfencesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  lowerfence .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val text: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Sets the text elements associated with each sample value. If a single string, the same string appears over all the data points. If an array of string, the items are mapped in order to the this trace's (x,y) coordinates. To be seen, trace `hoverinfo` must contain a *text* flag."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val mediansrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  median .""")
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val whiskerwidth: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = Some("""0.5"""),
      description = Some(
        """Sets the width of the whiskers relative to the box' width. For example, with 1, the whiskers are as wide as the box(es)."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hoverinfosrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hoverinfo .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertextsrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertext .""")
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val x0: PAny = PAny(
      dflt = None,
      description = Some(
        """Sets the x coordinate for single-box traces or the starting coordinate for multi-box traces set using q1/median/q3. See overview for more info."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val alignmentgroup: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Set several traces linked to the same position axis or matching axes to the same alignmentgroup. This controls whether bars compute their positional range dependently or independently."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val selectedpoints: PAny = PAny(
      dflt = None,
      description = Some(
        """Array containing integer indices of selected points. Has an effect only for traces that support selections. Note that an empty array means an empty selection where the `unselected` are turned on for all points, whereas, any other non-array values means no selection all where the `selected` and `unselected` styles have no effect."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val notchwidth: PReal = PReal(
      min = Some(0.0),
      max = Some(0.5),
      dflt = Some("""0.25"""),
      description = Some(
        """Sets the width of the notches relative to the box' width. For example, with 0, the notches are as wide as the box(es)."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val pointpos: PReal = PReal(
      min = Some(-2.0),
      max = Some(2.0),
      dflt = None,
      description = Some(
        """Sets the position of the sample points in relation to the box(es). If *0*, the sample points are places over the center of the box(es). Positive (negative) values correspond to positions to the right (left) for vertical boxes and above (below) for horizontal boxes"""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val meta: PAny = PAny(
      dflt = None,
      description = Some(
        """Assigns extra meta information associated with this trace that can be used in various text attributes. Attributes such as trace `name`, graph, axis and colorbar `title.text`, annotation `text` `rangeselector`, `updatemenues` and `sliders` `label` text all support `meta`. To access the trace `meta` values in an attribute in the same trace, simply use `%{meta[i]}` where `i` is the index or key of the `meta` item in question. To access trace `meta` in layout attributes, use `%{data[n[.meta[i]}` where `i` is the index or key of the `meta` and `n` is the trace index."""
      )
    )

    object quartilemethod extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """linear"""
      val description: Option[String] = Some(
        """Sets the method used to compute the sample's Q1 and Q3 quartiles. The *linear* method uses the 25th percentile for Q1 and 75th percentile for Q3 as computed using method #10 (listed on http://www.amstat.org/publications/jse/v14n3/langford.html). The *exclusive* method uses the median to divide the ordered dataset into two halves if the sample is odd, it does not include the median in either half - Q1 is then the median of the lower half and Q3 the median of the upper half. The *inclusive* method also uses the median to divide the ordered dataset into two halves but if the sample is odd, it includes the median in both halves - Q1 is then the median of the lower half and Q3 the median of the upper half."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def inclusive(): Unit = { selected = TString("inclusive") }

      def exclusive(): Unit = { selected = TString("exclusive") }

      def linear(): Unit = { selected = TString("linear") }

    }

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val upperfence: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the upper fence values. There should be as many items as the number of boxes desired. This attribute has effect only under the q1/median/q3 signature. If `upperfence` is not provided but a sample (in `y` or `x`) is set, we compute the lower as the last sample point above 1.5 times the IQR."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val offsetgroup: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Set several traces linked to the same position axis or matching axes to the same offsetgroup where bars of the same position coordinate will line up."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val median: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the median values. There should be as many items as the number of boxes desired."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val textsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  text .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertext: PChars = PChars(
      dflt = Some(""""""),
      description = Some("""Same as `text`.""")
    )

    /* §#
     * generateColor(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val fillcolor: PColor = PColor(
      dflt = None,
      description = Some(
        """Sets the fill color. Defaults to a half-transparent variant of the line color, marker color, or marker line color, whichever is available."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val sdsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  sd .""")
    )

    object orientation extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val description: Option[String] = Some(
        """Sets the orientation of the box(es). If *v* (*h*), the distribution is visualized along the vertical (horizontal)."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def h(): Unit = { selected = TString("h") }

      def v(): Unit = { selected = TString("v") }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val q3src: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  q3 .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val q3: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the Quartile 3 values. There should be as many items as the number of boxes desired."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uid: PChars = PChars(
      dflt = None,
      description = Some(
        """Assign an id to this trace, Use this to provide object constancy between traces during animations and transitions."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val legendgroup: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Sets the legend group for this trace. Traces part of the same legend group hide/show at the same time when toggling legend items."""
      )
    )

    object hoveron extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = Some("""boxes+points""")
      val description: Option[String] = Some(
        """Do the hover effects highlight individual boxes  or sample points or both?"""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def pointsOn(): Unit  = { flag_ + "points" }
      def pointsOff(): Unit = { flag_ - "points" }
      def boxesOn(): Unit   = { flag_ + "boxes" }
      def boxesOff(): Unit  = { flag_ - "boxes" }

    }

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val opacity: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = Some("""1.0"""),
      description = Some("""Sets the opacity of the trace.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val q1src: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  q1 .""")
    )

    object xcalendar extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """gregorian"""
      val description: Option[String] = Some(
        """Sets the calendar system to use with `x` date data."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def ummalqura(): Unit = { selected = TString("ummalqura") }

      def thai(): Unit = { selected = TString("thai") }

      def taiwan(): Unit = { selected = TString("taiwan") }

      def jalali(): Unit = { selected = TString("jalali") }

      def persian(): Unit = { selected = TString("persian") }

      def nepali(): Unit = { selected = TString("nepali") }

      def nanakshahi(): Unit = { selected = TString("nanakshahi") }

      def mayan(): Unit = { selected = TString("mayan") }

      def julian(): Unit = { selected = TString("julian") }

      def islamic(): Unit = { selected = TString("islamic") }

      def hebrew(): Unit = { selected = TString("hebrew") }

      def ethiopian(): Unit = { selected = TString("ethiopian") }

      def discworld(): Unit = { selected = TString("discworld") }

      def coptic(): Unit = { selected = TString("coptic") }

      def chinese(): Unit = { selected = TString("chinese") }

      def gregorian(): Unit = { selected = TString("gregorian") }

    }

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val sd: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the standard deviation values. There should be as many items as the number of boxes desired. This attribute has effect only under the q1/median/q3 signature. If `sd` is not provided but a sample (in `y` or `x`) is set, we compute the standard deviation for each box using the sample values."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ids: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns id labels to each datum. These ids for object constancy of data points during animation. Should be an array of strings, not numbers or any other type."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dx: PReal = PReal(
      min = None,
      max = None,
      dflt = None,
      description = Some(
        """Sets the x coordinate step for multi-box traces set using q1/median/q3."""
      )
    )

    /* §#
     * generateSubPlotIDDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val yaxis: SubPlotID = SubPlotID(
      dflt = Some("""y"""),
      description = Some(
        """Sets a reference between this trace's y coordinates and a 2D cartesian y axis. If *y* (the default value), the y coordinates refer to `layout.yaxis`. If *y2*, the y coordinates refer to `layout.yaxis2`, and so on."""
      )
    )

    /* §#
     * generateSubPlotIDDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val xaxis: SubPlotID = SubPlotID(
      dflt = Some("""x"""),
      description = Some(
        """Sets a reference between this trace's x coordinates and a 2D cartesian x axis. If *x* (the default value), the x coordinates refer to `layout.xaxis`. If *x2*, the x coordinates refer to `layout.xaxis2`, and so on."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val q1: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the Quartile 1 values. There should be as many items as the number of boxes desired."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val upperfencesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  upperfence .""")
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val width: PReal = PReal(
      min = Some(0.0),
      max = None,
      dflt = Some("""0.0"""),
      description = Some(
        """Sets the width of the box in data coordinate If *0* (default value) the width is automatically selected based on the positions of other box traces in the same subplot."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val metasrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  meta .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdata: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns extra data each datum. This may be useful when listening to hover, click and selection events. Note that, *scatter* traces also appends customdata items in the markers DOM elements"""
      )
    )

    /* §#
     * generateBooleanDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val showlegend: PBool = PBool(
      dflt = Some(true),
      description = Some(
        """Determines whether or not an item corresponding to this trace is shown in the legend."""
      )
    )

    object boxmean extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val description: Option[String] = Some(
        """If *true*, the mean of the box(es)' underlying distribution is drawn as a dashed line inside the box(es). If *sd* the standard deviation is also drawn. Defaults to *true* when `mean` is set. Defaults to *sd* when `sd` is set Otherwise defaults to *false*."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) => t.toString
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => TBoolean(s)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def true_(): Unit = { selected = TBoolean(true) }

      def sd(): Unit = { selected = TString("sd") }

      def false_(): Unit = { selected = TBoolean(false) }

    }

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val lowerfence: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the lower fence values. There should be as many items as the number of boxes desired. This attribute has effect only under the q1/median/q3 signature. If `lowerfence` is not provided but a sample (in `y` or `x`) is set, we compute the lower as the last sample point below 1.5 times the IQR."""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val notchspan: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Sets the notch span from the boxes' `median` values. There should be as many items as the number of boxes desired. This attribute has effect only under the q1/median/q3 signature. If `notchspan` is not provided but a sample (in `y` or `x`) is set, we compute it as 1.57 * IQR / sqrt(N), where N is the sample size."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdatasrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  customdata .""")
    )

    case object line extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """plot"""

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = Some("""Sets the color of line bounding the box(es).""")
      )

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val width: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""2.0"""),
        description =
          Some("""Sets the width (in px) of line bounding the box(es).""")
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v1 =
          if (width.json().length > 0) "\"width\"" + ":" + width.json() else ""
        val vars: List[String] = List(v0, v1)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("color")
          in0.map(ve => color.fromJson(ve))

          val in1 = mapKV.get("width")
          in1.map(ve => width.fromJson(ve))

        }
        this
      }

    }

    case object unselected extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """style"""

      case object marker extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """style"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val opacity: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = None,
          description = Some(
            """Sets the marker opacity of unselected points, applied only when a selection exists."""
          )
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = Some(
            """Sets the marker color of unselected points, applied only when a selection exists."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = None,
          description = Some(
            """Sets the marker size of unselected points, applied only when a selection exists."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (opacity.json().length > 0) "\"opacity\"" + ":" + opacity.json()
            else ""
          val v1 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v2 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("opacity")
            in0.map(ve => opacity.fromJson(ve))

            val in1 = mapKV.get("color")
            in1.map(ve => color.fromJson(ve))

            val in2 = mapKV.get("size")
            in2.map(ve => size.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (marker.json().length > 0) "\"marker\"" + ":" + marker.json()
          else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("marker")
          in0.map(ve => marker.fromJson(ve))

        }
        this
      }

    }

    case object marker extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """plot"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val size: PReal = PReal(
        min = Some(0.0),
        max = None,
        dflt = Some("""6.0"""),
        description = Some("""Sets the marker size (in px).""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val outliercolor: PColor = PColor(
        dflt = Some("""rgba(0, 0, 0, 0)"""),
        description = Some("""Sets the color of the outlier sample points.""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val color: PColor = PColor(
        dflt = None,
        description = Some(
          """Sets themarkercolor. It accepts either a specific color or an array of numbers that are mapped to the colorscale relative to the max and min values of the array or relative to `marker.cmin` and `marker.cmax` if set."""
        )
      )

      object symbol extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """circle"""
        val description: Option[String] = Some(
          """Sets the marker symbol type. Adding 100 is equivalent to appending *-open* to a symbol name. Adding 200 is equivalent to appending *-dot* to a symbol name. Adding 300 is equivalent to appending *-open-dot* or *dot-open* to a symbol name."""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) => t.toString
            case TDouble(t)  => t.toString
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => TBoolean(s)
            case Num(s)  => TDouble(s)
            case _       => err(in)
          }
          selected = read
          this
        }

        def D0_0(): Unit = { selected = TDouble(0.0) }

        def circle(): Unit = { selected = TString("circle") }

        def D100_0(): Unit = { selected = TDouble(100.0) }

        def circle_open(): Unit = { selected = TString("circle-open") }

        def D200_0(): Unit = { selected = TDouble(200.0) }

        def circle_dot(): Unit = { selected = TString("circle-dot") }

        def D300_0(): Unit = { selected = TDouble(300.0) }

        def circle_open_dot(): Unit = { selected = TString("circle-open-dot") }

        def D1_0(): Unit = { selected = TDouble(1.0) }

        def square(): Unit = { selected = TString("square") }

        def D101_0(): Unit = { selected = TDouble(101.0) }

        def square_open(): Unit = { selected = TString("square-open") }

        def D201_0(): Unit = { selected = TDouble(201.0) }

        def square_dot(): Unit = { selected = TString("square-dot") }

        def D301_0(): Unit = { selected = TDouble(301.0) }

        def square_open_dot(): Unit = { selected = TString("square-open-dot") }

        def D2_0(): Unit = { selected = TDouble(2.0) }

        def diamond(): Unit = { selected = TString("diamond") }

        def D102_0(): Unit = { selected = TDouble(102.0) }

        def diamond_open(): Unit = { selected = TString("diamond-open") }

        def D202_0(): Unit = { selected = TDouble(202.0) }

        def diamond_dot(): Unit = { selected = TString("diamond-dot") }

        def D302_0(): Unit = { selected = TDouble(302.0) }

        def diamond_open_dot(): Unit = {
          selected = TString("diamond-open-dot")
        }

        def D3_0(): Unit = { selected = TDouble(3.0) }

        def cross(): Unit = { selected = TString("cross") }

        def D103_0(): Unit = { selected = TDouble(103.0) }

        def cross_open(): Unit = { selected = TString("cross-open") }

        def D203_0(): Unit = { selected = TDouble(203.0) }

        def cross_dot(): Unit = { selected = TString("cross-dot") }

        def D303_0(): Unit = { selected = TDouble(303.0) }

        def cross_open_dot(): Unit = { selected = TString("cross-open-dot") }

        def D4_0(): Unit = { selected = TDouble(4.0) }

        def x(): Unit = { selected = TString("x") }

        def D104_0(): Unit = { selected = TDouble(104.0) }

        def x_open(): Unit = { selected = TString("x-open") }

        def D204_0(): Unit = { selected = TDouble(204.0) }

        def x_dot(): Unit = { selected = TString("x-dot") }

        def D304_0(): Unit = { selected = TDouble(304.0) }

        def x_open_dot(): Unit = { selected = TString("x-open-dot") }

        def D5_0(): Unit = { selected = TDouble(5.0) }

        def triangle_up(): Unit = { selected = TString("triangle-up") }

        def D105_0(): Unit = { selected = TDouble(105.0) }

        def triangle_up_open(): Unit = {
          selected = TString("triangle-up-open")
        }

        def D205_0(): Unit = { selected = TDouble(205.0) }

        def triangle_up_dot(): Unit = { selected = TString("triangle-up-dot") }

        def D305_0(): Unit = { selected = TDouble(305.0) }

        def triangle_up_open_dot(): Unit = {
          selected = TString("triangle-up-open-dot")
        }

        def D6_0(): Unit = { selected = TDouble(6.0) }

        def triangle_down(): Unit = { selected = TString("triangle-down") }

        def D106_0(): Unit = { selected = TDouble(106.0) }

        def triangle_down_open(): Unit = {
          selected = TString("triangle-down-open")
        }

        def D206_0(): Unit = { selected = TDouble(206.0) }

        def triangle_down_dot(): Unit = {
          selected = TString("triangle-down-dot")
        }

        def D306_0(): Unit = { selected = TDouble(306.0) }

        def triangle_down_open_dot(): Unit = {
          selected = TString("triangle-down-open-dot")
        }

        def D7_0(): Unit = { selected = TDouble(7.0) }

        def triangle_left(): Unit = { selected = TString("triangle-left") }

        def D107_0(): Unit = { selected = TDouble(107.0) }

        def triangle_left_open(): Unit = {
          selected = TString("triangle-left-open")
        }

        def D207_0(): Unit = { selected = TDouble(207.0) }

        def triangle_left_dot(): Unit = {
          selected = TString("triangle-left-dot")
        }

        def D307_0(): Unit = { selected = TDouble(307.0) }

        def triangle_left_open_dot(): Unit = {
          selected = TString("triangle-left-open-dot")
        }

        def D8_0(): Unit = { selected = TDouble(8.0) }

        def triangle_right(): Unit = { selected = TString("triangle-right") }

        def D108_0(): Unit = { selected = TDouble(108.0) }

        def triangle_right_open(): Unit = {
          selected = TString("triangle-right-open")
        }

        def D208_0(): Unit = { selected = TDouble(208.0) }

        def triangle_right_dot(): Unit = {
          selected = TString("triangle-right-dot")
        }

        def D308_0(): Unit = { selected = TDouble(308.0) }

        def triangle_right_open_dot(): Unit = {
          selected = TString("triangle-right-open-dot")
        }

        def D9_0(): Unit = { selected = TDouble(9.0) }

        def triangle_ne(): Unit = { selected = TString("triangle-ne") }

        def D109_0(): Unit = { selected = TDouble(109.0) }

        def triangle_ne_open(): Unit = {
          selected = TString("triangle-ne-open")
        }

        def D209_0(): Unit = { selected = TDouble(209.0) }

        def triangle_ne_dot(): Unit = { selected = TString("triangle-ne-dot") }

        def D309_0(): Unit = { selected = TDouble(309.0) }

        def triangle_ne_open_dot(): Unit = {
          selected = TString("triangle-ne-open-dot")
        }

        def D10_0(): Unit = { selected = TDouble(10.0) }

        def triangle_se(): Unit = { selected = TString("triangle-se") }

        def D110_0(): Unit = { selected = TDouble(110.0) }

        def triangle_se_open(): Unit = {
          selected = TString("triangle-se-open")
        }

        def D210_0(): Unit = { selected = TDouble(210.0) }

        def triangle_se_dot(): Unit = { selected = TString("triangle-se-dot") }

        def D310_0(): Unit = { selected = TDouble(310.0) }

        def triangle_se_open_dot(): Unit = {
          selected = TString("triangle-se-open-dot")
        }

        def D11_0(): Unit = { selected = TDouble(11.0) }

        def triangle_sw(): Unit = { selected = TString("triangle-sw") }

        def D111_0(): Unit = { selected = TDouble(111.0) }

        def triangle_sw_open(): Unit = {
          selected = TString("triangle-sw-open")
        }

        def D211_0(): Unit = { selected = TDouble(211.0) }

        def triangle_sw_dot(): Unit = { selected = TString("triangle-sw-dot") }

        def D311_0(): Unit = { selected = TDouble(311.0) }

        def triangle_sw_open_dot(): Unit = {
          selected = TString("triangle-sw-open-dot")
        }

        def D12_0(): Unit = { selected = TDouble(12.0) }

        def triangle_nw(): Unit = { selected = TString("triangle-nw") }

        def D112_0(): Unit = { selected = TDouble(112.0) }

        def triangle_nw_open(): Unit = {
          selected = TString("triangle-nw-open")
        }

        def D212_0(): Unit = { selected = TDouble(212.0) }

        def triangle_nw_dot(): Unit = { selected = TString("triangle-nw-dot") }

        def D312_0(): Unit = { selected = TDouble(312.0) }

        def triangle_nw_open_dot(): Unit = {
          selected = TString("triangle-nw-open-dot")
        }

        def D13_0(): Unit = { selected = TDouble(13.0) }

        def pentagon(): Unit = { selected = TString("pentagon") }

        def D113_0(): Unit = { selected = TDouble(113.0) }

        def pentagon_open(): Unit = { selected = TString("pentagon-open") }

        def D213_0(): Unit = { selected = TDouble(213.0) }

        def pentagon_dot(): Unit = { selected = TString("pentagon-dot") }

        def D313_0(): Unit = { selected = TDouble(313.0) }

        def pentagon_open_dot(): Unit = {
          selected = TString("pentagon-open-dot")
        }

        def D14_0(): Unit = { selected = TDouble(14.0) }

        def hexagon(): Unit = { selected = TString("hexagon") }

        def D114_0(): Unit = { selected = TDouble(114.0) }

        def hexagon_open(): Unit = { selected = TString("hexagon-open") }

        def D214_0(): Unit = { selected = TDouble(214.0) }

        def hexagon_dot(): Unit = { selected = TString("hexagon-dot") }

        def D314_0(): Unit = { selected = TDouble(314.0) }

        def hexagon_open_dot(): Unit = {
          selected = TString("hexagon-open-dot")
        }

        def D15_0(): Unit = { selected = TDouble(15.0) }

        def hexagon2(): Unit = { selected = TString("hexagon2") }

        def D115_0(): Unit = { selected = TDouble(115.0) }

        def hexagon2_open(): Unit = { selected = TString("hexagon2-open") }

        def D215_0(): Unit = { selected = TDouble(215.0) }

        def hexagon2_dot(): Unit = { selected = TString("hexagon2-dot") }

        def D315_0(): Unit = { selected = TDouble(315.0) }

        def hexagon2_open_dot(): Unit = {
          selected = TString("hexagon2-open-dot")
        }

        def D16_0(): Unit = { selected = TDouble(16.0) }

        def octagon(): Unit = { selected = TString("octagon") }

        def D116_0(): Unit = { selected = TDouble(116.0) }

        def octagon_open(): Unit = { selected = TString("octagon-open") }

        def D216_0(): Unit = { selected = TDouble(216.0) }

        def octagon_dot(): Unit = { selected = TString("octagon-dot") }

        def D316_0(): Unit = { selected = TDouble(316.0) }

        def octagon_open_dot(): Unit = {
          selected = TString("octagon-open-dot")
        }

        def D17_0(): Unit = { selected = TDouble(17.0) }

        def star(): Unit = { selected = TString("star") }

        def D117_0(): Unit = { selected = TDouble(117.0) }

        def star_open(): Unit = { selected = TString("star-open") }

        def D217_0(): Unit = { selected = TDouble(217.0) }

        def star_dot(): Unit = { selected = TString("star-dot") }

        def D317_0(): Unit = { selected = TDouble(317.0) }

        def star_open_dot(): Unit = { selected = TString("star-open-dot") }

        def D18_0(): Unit = { selected = TDouble(18.0) }

        def hexagram(): Unit = { selected = TString("hexagram") }

        def D118_0(): Unit = { selected = TDouble(118.0) }

        def hexagram_open(): Unit = { selected = TString("hexagram-open") }

        def D218_0(): Unit = { selected = TDouble(218.0) }

        def hexagram_dot(): Unit = { selected = TString("hexagram-dot") }

        def D318_0(): Unit = { selected = TDouble(318.0) }

        def hexagram_open_dot(): Unit = {
          selected = TString("hexagram-open-dot")
        }

        def D19_0(): Unit = { selected = TDouble(19.0) }

        def star_triangle_up(): Unit = {
          selected = TString("star-triangle-up")
        }

        def D119_0(): Unit = { selected = TDouble(119.0) }

        def star_triangle_up_open(): Unit = {
          selected = TString("star-triangle-up-open")
        }

        def D219_0(): Unit = { selected = TDouble(219.0) }

        def star_triangle_up_dot(): Unit = {
          selected = TString("star-triangle-up-dot")
        }

        def D319_0(): Unit = { selected = TDouble(319.0) }

        def star_triangle_up_open_dot(): Unit = {
          selected = TString("star-triangle-up-open-dot")
        }

        def D20_0(): Unit = { selected = TDouble(20.0) }

        def star_triangle_down(): Unit = {
          selected = TString("star-triangle-down")
        }

        def D120_0(): Unit = { selected = TDouble(120.0) }

        def star_triangle_down_open(): Unit = {
          selected = TString("star-triangle-down-open")
        }

        def D220_0(): Unit = { selected = TDouble(220.0) }

        def star_triangle_down_dot(): Unit = {
          selected = TString("star-triangle-down-dot")
        }

        def D320_0(): Unit = { selected = TDouble(320.0) }

        def star_triangle_down_open_dot(): Unit = {
          selected = TString("star-triangle-down-open-dot")
        }

        def D21_0(): Unit = { selected = TDouble(21.0) }

        def star_square(): Unit = { selected = TString("star-square") }

        def D121_0(): Unit = { selected = TDouble(121.0) }

        def star_square_open(): Unit = {
          selected = TString("star-square-open")
        }

        def D221_0(): Unit = { selected = TDouble(221.0) }

        def star_square_dot(): Unit = { selected = TString("star-square-dot") }

        def D321_0(): Unit = { selected = TDouble(321.0) }

        def star_square_open_dot(): Unit = {
          selected = TString("star-square-open-dot")
        }

        def D22_0(): Unit = { selected = TDouble(22.0) }

        def star_diamond(): Unit = { selected = TString("star-diamond") }

        def D122_0(): Unit = { selected = TDouble(122.0) }

        def star_diamond_open(): Unit = {
          selected = TString("star-diamond-open")
        }

        def D222_0(): Unit = { selected = TDouble(222.0) }

        def star_diamond_dot(): Unit = {
          selected = TString("star-diamond-dot")
        }

        def D322_0(): Unit = { selected = TDouble(322.0) }

        def star_diamond_open_dot(): Unit = {
          selected = TString("star-diamond-open-dot")
        }

        def D23_0(): Unit = { selected = TDouble(23.0) }

        def diamond_tall(): Unit = { selected = TString("diamond-tall") }

        def D123_0(): Unit = { selected = TDouble(123.0) }

        def diamond_tall_open(): Unit = {
          selected = TString("diamond-tall-open")
        }

        def D223_0(): Unit = { selected = TDouble(223.0) }

        def diamond_tall_dot(): Unit = {
          selected = TString("diamond-tall-dot")
        }

        def D323_0(): Unit = { selected = TDouble(323.0) }

        def diamond_tall_open_dot(): Unit = {
          selected = TString("diamond-tall-open-dot")
        }

        def D24_0(): Unit = { selected = TDouble(24.0) }

        def diamond_wide(): Unit = { selected = TString("diamond-wide") }

        def D124_0(): Unit = { selected = TDouble(124.0) }

        def diamond_wide_open(): Unit = {
          selected = TString("diamond-wide-open")
        }

        def D224_0(): Unit = { selected = TDouble(224.0) }

        def diamond_wide_dot(): Unit = {
          selected = TString("diamond-wide-dot")
        }

        def D324_0(): Unit = { selected = TDouble(324.0) }

        def diamond_wide_open_dot(): Unit = {
          selected = TString("diamond-wide-open-dot")
        }

        def D25_0(): Unit = { selected = TDouble(25.0) }

        def hourglass(): Unit = { selected = TString("hourglass") }

        def D125_0(): Unit = { selected = TDouble(125.0) }

        def hourglass_open(): Unit = { selected = TString("hourglass-open") }

        def D26_0(): Unit = { selected = TDouble(26.0) }

        def bowtie(): Unit = { selected = TString("bowtie") }

        def D126_0(): Unit = { selected = TDouble(126.0) }

        def bowtie_open(): Unit = { selected = TString("bowtie-open") }

        def D27_0(): Unit = { selected = TDouble(27.0) }

        def circle_cross(): Unit = { selected = TString("circle-cross") }

        def D127_0(): Unit = { selected = TDouble(127.0) }

        def circle_cross_open(): Unit = {
          selected = TString("circle-cross-open")
        }

        def D28_0(): Unit = { selected = TDouble(28.0) }

        def circle_x(): Unit = { selected = TString("circle-x") }

        def D128_0(): Unit = { selected = TDouble(128.0) }

        def circle_x_open(): Unit = { selected = TString("circle-x-open") }

        def D29_0(): Unit = { selected = TDouble(29.0) }

        def square_cross(): Unit = { selected = TString("square-cross") }

        def D129_0(): Unit = { selected = TDouble(129.0) }

        def square_cross_open(): Unit = {
          selected = TString("square-cross-open")
        }

        def D30_0(): Unit = { selected = TDouble(30.0) }

        def square_x(): Unit = { selected = TString("square-x") }

        def D130_0(): Unit = { selected = TDouble(130.0) }

        def square_x_open(): Unit = { selected = TString("square-x-open") }

        def D31_0(): Unit = { selected = TDouble(31.0) }

        def diamond_cross(): Unit = { selected = TString("diamond-cross") }

        def D131_0(): Unit = { selected = TDouble(131.0) }

        def diamond_cross_open(): Unit = {
          selected = TString("diamond-cross-open")
        }

        def D32_0(): Unit = { selected = TDouble(32.0) }

        def diamond_x(): Unit = { selected = TString("diamond-x") }

        def D132_0(): Unit = { selected = TDouble(132.0) }

        def diamond_x_open(): Unit = { selected = TString("diamond-x-open") }

        def D33_0(): Unit = { selected = TDouble(33.0) }

        def cross_thin(): Unit = { selected = TString("cross-thin") }

        def D133_0(): Unit = { selected = TDouble(133.0) }

        def cross_thin_open(): Unit = { selected = TString("cross-thin-open") }

        def D34_0(): Unit = { selected = TDouble(34.0) }

        def x_thin(): Unit = { selected = TString("x-thin") }

        def D134_0(): Unit = { selected = TDouble(134.0) }

        def x_thin_open(): Unit = { selected = TString("x-thin-open") }

        def D35_0(): Unit = { selected = TDouble(35.0) }

        def asterisk(): Unit = { selected = TString("asterisk") }

        def D135_0(): Unit = { selected = TDouble(135.0) }

        def asterisk_open(): Unit = { selected = TString("asterisk-open") }

        def D36_0(): Unit = { selected = TDouble(36.0) }

        def hash(): Unit = { selected = TString("hash") }

        def D136_0(): Unit = { selected = TDouble(136.0) }

        def hash_open(): Unit = { selected = TString("hash-open") }

        def D236_0(): Unit = { selected = TDouble(236.0) }

        def hash_dot(): Unit = { selected = TString("hash-dot") }

        def D336_0(): Unit = { selected = TDouble(336.0) }

        def hash_open_dot(): Unit = { selected = TString("hash-open-dot") }

        def D37_0(): Unit = { selected = TDouble(37.0) }

        def y_up(): Unit = { selected = TString("y-up") }

        def D137_0(): Unit = { selected = TDouble(137.0) }

        def y_up_open(): Unit = { selected = TString("y-up-open") }

        def D38_0(): Unit = { selected = TDouble(38.0) }

        def y_down(): Unit = { selected = TString("y-down") }

        def D138_0(): Unit = { selected = TDouble(138.0) }

        def y_down_open(): Unit = { selected = TString("y-down-open") }

        def D39_0(): Unit = { selected = TDouble(39.0) }

        def y_left(): Unit = { selected = TString("y-left") }

        def D139_0(): Unit = { selected = TDouble(139.0) }

        def y_left_open(): Unit = { selected = TString("y-left-open") }

        def D40_0(): Unit = { selected = TDouble(40.0) }

        def y_right(): Unit = { selected = TString("y-right") }

        def D140_0(): Unit = { selected = TDouble(140.0) }

        def y_right_open(): Unit = { selected = TString("y-right-open") }

        def D41_0(): Unit = { selected = TDouble(41.0) }

        def line_ew(): Unit = { selected = TString("line-ew") }

        def D141_0(): Unit = { selected = TDouble(141.0) }

        def line_ew_open(): Unit = { selected = TString("line-ew-open") }

        def D42_0(): Unit = { selected = TDouble(42.0) }

        def line_ns(): Unit = { selected = TString("line-ns") }

        def D142_0(): Unit = { selected = TDouble(142.0) }

        def line_ns_open(): Unit = { selected = TString("line-ns-open") }

        def D43_0(): Unit = { selected = TDouble(43.0) }

        def line_ne(): Unit = { selected = TString("line-ne") }

        def D143_0(): Unit = { selected = TDouble(143.0) }

        def line_ne_open(): Unit = { selected = TString("line-ne-open") }

        def D44_0(): Unit = { selected = TDouble(44.0) }

        def line_nw(): Unit = { selected = TString("line-nw") }

        def D144_0(): Unit = { selected = TDouble(144.0) }

        def line_nw_open(): Unit = { selected = TString("line-nw-open") }

        def false_(): Unit = { selected = TBoolean(false) }

      }

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val opacity: PReal = PReal(
        min = Some(0.0),
        max = Some(1.0),
        dflt = Some("""1.0"""),
        description = Some("""Sets the marker opacity.""")
      )

      case object line extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """style"""

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val outliercolor: PColor = PColor(
          dflt = None,
          description = Some(
            """Sets the border line color of the outlier sample points. Defaults to marker.color"""
          )
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = Some("""#444"""),
          description = Some(
            """Sets themarker.linecolor. It accepts either a specific color or an array of numbers that are mapped to the colorscale relative to the max and min values of the array or relative to `marker.line.cmin` and `marker.line.cmax` if set."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val outlierwidth: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""1.0"""),
          description = Some(
            """Sets the border line width (in px) of the outlier sample points."""
          )
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val width: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = Some("""0.0"""),
          description = Some(
            """Sets the width (in px) of the lines bounding the marker points."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (outliercolor.json().length > 0)
              "\"outliercolor\"" + ":" + outliercolor.json()
            else ""
          val v1 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v2 =
            if (outlierwidth.json().length > 0)
              "\"outlierwidth\"" + ":" + outlierwidth.json()
            else ""
          val v3 =
            if (width.json().length > 0) "\"width\"" + ":" + width.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("outliercolor")
            in0.map(ve => outliercolor.fromJson(ve))

            val in1 = mapKV.get("color")
            in1.map(ve => color.fromJson(ve))

            val in2 = mapKV.get("outlierwidth")
            in2.map(ve => outlierwidth.fromJson(ve))

            val in3 = mapKV.get("width")
            in3.map(ve => width.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
        val v1 =
          if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
        val v2 =
          if (outliercolor.json().length > 0)
            "\"outliercolor\"" + ":" + outliercolor.json()
          else ""
        val v3 =
          if (color.json().length > 0) "\"color\"" + ":" + color.json() else ""
        val v4 =
          if (symbol.json().length > 0) "\"symbol\"" + ":" + symbol.json()
          else ""
        val v5 =
          if (opacity.json().length > 0) "\"opacity\"" + ":" + opacity.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("line")
          in0.map(ve => line.fromJson(ve))

          val in1 = mapKV.get("size")
          in1.map(ve => size.fromJson(ve))

          val in2 = mapKV.get("outliercolor")
          in2.map(ve => outliercolor.fromJson(ve))

          val in3 = mapKV.get("color")
          in3.map(ve => color.fromJson(ve))

          val in4 = mapKV.get("symbol")
          in4.map(ve => symbol.fromJson(ve))

          val in5 = mapKV.get("opacity")
          in5.map(ve => opacity.fromJson(ve))

        }
        this
      }

    }

    case object selected extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """style"""

      case object marker extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String     = """object"""
        val editType: String = """style"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val opacity: PReal = PReal(
          min = Some(0.0),
          max = Some(1.0),
          dflt = None,
          description = Some("""Sets the marker opacity of selected points.""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = Some("""Sets the marker color of selected points.""")
        )

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal = PReal(
          min = Some(0.0),
          max = None,
          dflt = None,
          description = Some("""Sets the marker size of selected points.""")
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (opacity.json().length > 0) "\"opacity\"" + ":" + opacity.json()
            else ""
          val v1 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v2 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val vars: List[String] = List(v0, v1, v2)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("opacity")
            in0.map(ve => opacity.fromJson(ve))

            val in1 = mapKV.get("color")
            in1.map(ve => color.fromJson(ve))

            val in2 = mapKV.get("size")
            in2.map(ve => size.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (marker.json().length > 0) "\"marker\"" + ":" + marker.json()
          else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("marker")
          in0.map(ve => marker.fromJson(ve))

        }
        this
      }

    }

    case object stream extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val maxpoints: PReal = PReal(
        min = Some(0.0),
        max = Some(10000.0),
        dflt = Some("""500.0"""),
        description = Some(
          """Sets the maximum number of points to keep on the plots from an incoming stream. If `maxpoints` is set to *50*, only the newest 50 points will be displayed on the plot."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val token: PChars = PChars(
        dflt = None,
        description = Some(
          """The stream id number links a data trace on a plot with a stream. See https://plot.ly/settings for more details."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (maxpoints.json().length > 0)
            "\"maxpoints\"" + ":" + maxpoints.json()
          else ""
        val v1 =
          if (token.json().length > 0) "\"token\"" + ":" + token.json() else ""
        val vars: List[String] = List(v0, v1)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("maxpoints")
          in0.map(ve => maxpoints.fromJson(ve))

          val in1 = mapKV.get("token")
          in1.map(ve => token.fromJson(ve))

        }
        this
      }

    }

    case object hoverlabel extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """none"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val alignsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  align .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolor: PColor = PColor(
        dflt = None,
        description = Some(
          """Sets the background color of the hover labels for this trace"""
        )
      )

      object align extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """auto"""
        val description: Option[String] = Some(
          """Sets the horizontal alignment of the text content within hover label box. Has an effect only if the hover label text spans more two or more lines"""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def auto(): Unit = { selected = TString("auto") }

        def right(): Unit = { selected = TString("right") }

        def left(): Unit = { selected = TString("left") }

      }

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelength: PInt = PInt(
        min = Some(-1),
        max = None,
        dflt = Some(15),
        description = Some(
          """Sets the default length (in number of characters) of the trace name in the hover labels for all traces. -1 shows the whole name regardless of length. 0-3 shows the first 0-3 characters, and an integer >3 will show the whole name if it is less than that many characters, but if it is longer, will truncate to `namelength - 3` characters and add an ellipsis."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelengthsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  namelength .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolor: PColor = PColor(
        dflt = None,
        description =
          Some("""Sets the border color of the hover labels for this trace.""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bordercolor .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bgcolor .""")
      )

      case object font extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Sets the font used in hover labels."""
        val editType: String    = """none"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val familysrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  family .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val colorsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  color .""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val sizesrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  size .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val v1 =
            if (familysrc.json().length > 0)
              "\"familysrc\"" + ":" + familysrc.json()
            else ""
          val v2 =
            if (colorsrc.json().length > 0)
              "\"colorsrc\"" + ":" + colorsrc.json()
            else ""
          val v3 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v4 =
            if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
            else ""
          val v5 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("size")
            in0.map(ve => size.fromJson(ve))

            val in1 = mapKV.get("familysrc")
            in1.map(ve => familysrc.fromJson(ve))

            val in2 = mapKV.get("colorsrc")
            in2.map(ve => colorsrc.fromJson(ve))

            val in3 = mapKV.get("color")
            in3.map(ve => color.fromJson(ve))

            val in4 = mapKV.get("sizesrc")
            in4.map(ve => sizesrc.fromJson(ve))

            val in5 = mapKV.get("family")
            in5.map(ve => family.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
        val v1 =
          if (alignsrc.json().length > 0) "\"alignsrc\"" + ":" + alignsrc.json()
          else ""
        val v2 =
          if (bgcolor.json().length > 0) "\"bgcolor\"" + ":" + bgcolor.json()
          else ""
        val v3 =
          if (align.json().length > 0) "\"align\"" + ":" + align.json() else ""
        val v4 =
          if (namelength.json().length > 0)
            "\"namelength\"" + ":" + namelength.json()
          else ""
        val v5 =
          if (namelengthsrc.json().length > 0)
            "\"namelengthsrc\"" + ":" + namelengthsrc.json()
          else ""
        val v6 =
          if (bordercolor.json().length > 0)
            "\"bordercolor\"" + ":" + bordercolor.json()
          else ""
        val v7 =
          if (bordercolorsrc.json().length > 0)
            "\"bordercolorsrc\"" + ":" + bordercolorsrc.json()
          else ""
        val v8 =
          if (bgcolorsrc.json().length > 0)
            "\"bgcolorsrc\"" + ":" + bgcolorsrc.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6, v7, v8)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("font")
          in0.map(ve => font.fromJson(ve))

          val in1 = mapKV.get("alignsrc")
          in1.map(ve => alignsrc.fromJson(ve))

          val in2 = mapKV.get("bgcolor")
          in2.map(ve => bgcolor.fromJson(ve))

          val in3 = mapKV.get("align")
          in3.map(ve => align.fromJson(ve))

          val in4 = mapKV.get("namelength")
          in4.map(ve => namelength.fromJson(ve))

          val in5 = mapKV.get("namelengthsrc")
          in5.map(ve => namelengthsrc.fromJson(ve))

          val in6 = mapKV.get("bordercolor")
          in6.map(ve => bordercolor.fromJson(ve))

          val in7 = mapKV.get("bordercolorsrc")
          in7.map(ve => bordercolorsrc.fromJson(ve))

          val in8 = mapKV.get("bgcolorsrc")
          in8.map(ve => bgcolorsrc.fromJson(ve))

        }
        this
      }

    }

    case object transforms extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String = """object"""

      case object items extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        case object transform extends JSON {
          /* §#
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          val role: String = """object"""
          val description: String =
            """An array of operations that manipulate the trace data, for example filtering or sorting the data arrays."""
          val editType: String = """calc"""

          def json(): String = {
            /* §#
             * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/

            val vars: List[String] = List()
            val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
            if (cleanVars.length > 0) {
              // values set
              "{" + cleanVars + "}"
            } else {
              // no values set
              ""
            }
          }

          def fromJson(in: Value): this.type = {
            /* §#
             * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
             * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generateObjects(obj: ObjectDefinition)
             * generateCompound(obj: ObjectDefinition)
             * generate(obj: CompoundDefinition)
             * generate(obj: Either[Error, CompoundDefinition])*/
            def err = throw new RuntimeException(s"Expected an Obj but got $in")
            in.objOpt.fold(err) { mapKV => }
            this
          }

        }

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (transform.json().length > 0)
              "\"transform\"" + ":" + transform.json()
            else ""
          val vars: List[String] = List(v0)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("transform")
            in0.map(ve => transform.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (items.json().length > 0) "\"items\"" + ":" + items.json() else ""
        val vars: List[String] = List(v0)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("items")
          in0.map(ve => items.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val v71 = "\"type\" : \"box\""
      val v0 =
        if (line.json().length > 0) "\"line\"" + ":" + line.json() else ""
      val v1 =
        if (unselected.json().length > 0)
          "\"unselected\"" + ":" + unselected.json()
        else ""
      val v2 =
        if (marker.json().length > 0) "\"marker\"" + ":" + marker.json() else ""
      val v3 =
        if (selected.json().length > 0) "\"selected\"" + ":" + selected.json()
        else ""
      val v4 =
        if (stream.json().length > 0) "\"stream\"" + ":" + stream.json() else ""
      val v5 =
        if (hoverlabel.json().length > 0)
          "\"hoverlabel\"" + ":" + hoverlabel.json()
        else ""
      val v6 =
        if (transforms.json().length > 0)
          "\"transforms\"" + ":" + transforms.json()
        else ""
      val v7 =
        if (hovertemplate.json().length > 0)
          "\"hovertemplate\"" + ":" + hovertemplate.json()
        else ""
      val v8 =
        if (xsrc.json().length > 0) "\"xsrc\"" + ":" + xsrc.json() else ""
      val v9 = if (x.json().length > 0) "\"x\"" + ":" + x.json() else ""
      val v10 =
        if (name.json().length > 0) "\"name\"" + ":" + name.json() else ""
      val v11 =
        if (notched.json().length > 0) "\"notched\"" + ":" + notched.json()
        else ""
      val v12 = if (dy.json().length > 0) "\"dy\"" + ":" + dy.json() else ""
      val v13 =
        if (hovertemplatesrc.json().length > 0)
          "\"hovertemplatesrc\"" + ":" + hovertemplatesrc.json()
        else ""
      val v14 =
        if (ycalendar.json().length > 0)
          "\"ycalendar\"" + ":" + ycalendar.json()
        else ""
      val v15 =
        if (hoverinfo.json().length > 0)
          "\"hoverinfo\"" + ":" + hoverinfo.json()
        else ""
      val v16 =
        if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
        else ""
      val v17 = if (y.json().length > 0) "\"y\"" + ":" + y.json() else ""
      val v18 =
        if (meansrc.json().length > 0) "\"meansrc\"" + ":" + meansrc.json()
        else ""
      val v19 =
        if (boxpoints.json().length > 0)
          "\"boxpoints\"" + ":" + boxpoints.json()
        else ""
      val v20 =
        if (jitter.json().length > 0) "\"jitter\"" + ":" + jitter.json() else ""
      val v21 =
        if (uirevision.json().length > 0)
          "\"uirevision\"" + ":" + uirevision.json()
        else ""
      val v22 =
        if (notchspansrc.json().length > 0)
          "\"notchspansrc\"" + ":" + notchspansrc.json()
        else ""
      val v23 =
        if (mean.json().length > 0) "\"mean\"" + ":" + mean.json() else ""
      val v24 = if (y0.json().length > 0) "\"y0\"" + ":" + y0.json() else ""
      val v25 =
        if (ysrc.json().length > 0) "\"ysrc\"" + ":" + ysrc.json() else ""
      val v26 =
        if (idssrc.json().length > 0) "\"idssrc\"" + ":" + idssrc.json() else ""
      val v27 =
        if (lowerfencesrc.json().length > 0)
          "\"lowerfencesrc\"" + ":" + lowerfencesrc.json()
        else ""
      val v28 =
        if (text.json().length > 0) "\"text\"" + ":" + text.json() else ""
      val v29 =
        if (mediansrc.json().length > 0)
          "\"mediansrc\"" + ":" + mediansrc.json()
        else ""
      val v30 =
        if (whiskerwidth.json().length > 0)
          "\"whiskerwidth\"" + ":" + whiskerwidth.json()
        else ""
      val v31 =
        if (hoverinfosrc.json().length > 0)
          "\"hoverinfosrc\"" + ":" + hoverinfosrc.json()
        else ""
      val v32 =
        if (hovertextsrc.json().length > 0)
          "\"hovertextsrc\"" + ":" + hovertextsrc.json()
        else ""
      val v33 = if (x0.json().length > 0) "\"x0\"" + ":" + x0.json() else ""
      val v34 =
        if (alignmentgroup.json().length > 0)
          "\"alignmentgroup\"" + ":" + alignmentgroup.json()
        else ""
      val v35 =
        if (selectedpoints.json().length > 0)
          "\"selectedpoints\"" + ":" + selectedpoints.json()
        else ""
      val v36 =
        if (notchwidth.json().length > 0)
          "\"notchwidth\"" + ":" + notchwidth.json()
        else ""
      val v37 =
        if (pointpos.json().length > 0) "\"pointpos\"" + ":" + pointpos.json()
        else ""
      val v38 =
        if (meta.json().length > 0) "\"meta\"" + ":" + meta.json() else ""
      val v39 =
        if (quartilemethod.json().length > 0)
          "\"quartilemethod\"" + ":" + quartilemethod.json()
        else ""
      val v40 =
        if (upperfence.json().length > 0)
          "\"upperfence\"" + ":" + upperfence.json()
        else ""
      val v41 =
        if (offsetgroup.json().length > 0)
          "\"offsetgroup\"" + ":" + offsetgroup.json()
        else ""
      val v42 =
        if (median.json().length > 0) "\"median\"" + ":" + median.json() else ""
      val v43 =
        if (textsrc.json().length > 0) "\"textsrc\"" + ":" + textsrc.json()
        else ""
      val v44 =
        if (hovertext.json().length > 0)
          "\"hovertext\"" + ":" + hovertext.json()
        else ""
      val v45 =
        if (fillcolor.json().length > 0)
          "\"fillcolor\"" + ":" + fillcolor.json()
        else ""
      val v46 =
        if (sdsrc.json().length > 0) "\"sdsrc\"" + ":" + sdsrc.json() else ""
      val v47 =
        if (orientation.json().length > 0)
          "\"orientation\"" + ":" + orientation.json()
        else ""
      val v48 =
        if (q3src.json().length > 0) "\"q3src\"" + ":" + q3src.json() else ""
      val v49 = if (q3.json().length > 0) "\"q3\"" + ":" + q3.json() else ""
      val v50 = if (uid.json().length > 0) "\"uid\"" + ":" + uid.json() else ""
      val v51 =
        if (legendgroup.json().length > 0)
          "\"legendgroup\"" + ":" + legendgroup.json()
        else ""
      val v52 =
        if (hoveron.json().length > 0) "\"hoveron\"" + ":" + hoveron.json()
        else ""
      val v53 =
        if (opacity.json().length > 0) "\"opacity\"" + ":" + opacity.json()
        else ""
      val v54 =
        if (q1src.json().length > 0) "\"q1src\"" + ":" + q1src.json() else ""
      val v55 =
        if (xcalendar.json().length > 0)
          "\"xcalendar\"" + ":" + xcalendar.json()
        else ""
      val v56 = if (sd.json().length > 0) "\"sd\"" + ":" + sd.json() else ""
      val v57 = if (ids.json().length > 0) "\"ids\"" + ":" + ids.json() else ""
      val v58 = if (dx.json().length > 0) "\"dx\"" + ":" + dx.json() else ""
      val v59 =
        if (yaxis.json().length > 0) "\"yaxis\"" + ":" + yaxis.json() else ""
      val v60 =
        if (xaxis.json().length > 0) "\"xaxis\"" + ":" + xaxis.json() else ""
      val v61 = if (q1.json().length > 0) "\"q1\"" + ":" + q1.json() else ""
      val v62 =
        if (upperfencesrc.json().length > 0)
          "\"upperfencesrc\"" + ":" + upperfencesrc.json()
        else ""
      val v63 =
        if (width.json().length > 0) "\"width\"" + ":" + width.json() else ""
      val v64 =
        if (metasrc.json().length > 0) "\"metasrc\"" + ":" + metasrc.json()
        else ""
      val v65 =
        if (customdata.json().length > 0)
          "\"customdata\"" + ":" + customdata.json()
        else ""
      val v66 =
        if (showlegend.json().length > 0)
          "\"showlegend\"" + ":" + showlegend.json()
        else ""
      val v67 =
        if (boxmean.json().length > 0) "\"boxmean\"" + ":" + boxmean.json()
        else ""
      val v68 =
        if (lowerfence.json().length > 0)
          "\"lowerfence\"" + ":" + lowerfence.json()
        else ""
      val v69 =
        if (notchspan.json().length > 0)
          "\"notchspan\"" + ":" + notchspan.json()
        else ""
      val v70 =
        if (customdatasrc.json().length > 0)
          "\"customdatasrc\"" + ":" + customdatasrc.json()
        else ""
      val vars: List[String] = List(
        v71,
        v0,
        v1,
        v2,
        v3,
        v4,
        v5,
        v6,
        v7,
        v8,
        v9,
        v10,
        v11,
        v12,
        v13,
        v14,
        v15,
        v16,
        v17,
        v18,
        v19,
        v20,
        v21,
        v22,
        v23,
        v24,
        v25,
        v26,
        v27,
        v28,
        v29,
        v30,
        v31,
        v32,
        v33,
        v34,
        v35,
        v36,
        v37,
        v38,
        v39,
        v40,
        v41,
        v42,
        v43,
        v44,
        v45,
        v46,
        v47,
        v48,
        v49,
        v50,
        v51,
        v52,
        v53,
        v54,
        v55,
        v56,
        v57,
        v58,
        v59,
        v60,
        v61,
        v62,
        v63,
        v64,
        v65,
        v66,
        v67,
        v68,
        v69,
        v70
      )
      val cleanVars = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("line")
        in0.map(ve => line.fromJson(ve))

        val in1 = mapKV.get("unselected")
        in1.map(ve => unselected.fromJson(ve))

        val in2 = mapKV.get("marker")
        in2.map(ve => marker.fromJson(ve))

        val in3 = mapKV.get("selected")
        in3.map(ve => selected.fromJson(ve))

        val in4 = mapKV.get("stream")
        in4.map(ve => stream.fromJson(ve))

        val in5 = mapKV.get("hoverlabel")
        in5.map(ve => hoverlabel.fromJson(ve))

        val in6 = mapKV.get("transforms")
        in6.map(ve => transforms.fromJson(ve))

        val in7 = mapKV.get("hovertemplate")
        in7.map(ve => hovertemplate.fromJson(ve))

        val in8 = mapKV.get("xsrc")
        in8.map(ve => xsrc.fromJson(ve))

        val in9 = mapKV.get("x")
        in9.map(ve => x.fromJson(ve))

        val in10 = mapKV.get("name")
        in10.map(ve => name.fromJson(ve))

        val in11 = mapKV.get("notched")
        in11.map(ve => notched.fromJson(ve))

        val in12 = mapKV.get("dy")
        in12.map(ve => dy.fromJson(ve))

        val in13 = mapKV.get("hovertemplatesrc")
        in13.map(ve => hovertemplatesrc.fromJson(ve))

        val in14 = mapKV.get("ycalendar")
        in14.map(ve => ycalendar.fromJson(ve))

        val in15 = mapKV.get("hoverinfo")
        in15.map(ve => hoverinfo.fromJson(ve))

        val in16 = mapKV.get("visible")
        in16.map(ve => visible.fromJson(ve))

        val in17 = mapKV.get("y")
        in17.map(ve => y.fromJson(ve))

        val in18 = mapKV.get("meansrc")
        in18.map(ve => meansrc.fromJson(ve))

        val in19 = mapKV.get("boxpoints")
        in19.map(ve => boxpoints.fromJson(ve))

        val in20 = mapKV.get("jitter")
        in20.map(ve => jitter.fromJson(ve))

        val in21 = mapKV.get("uirevision")
        in21.map(ve => uirevision.fromJson(ve))

        val in22 = mapKV.get("notchspansrc")
        in22.map(ve => notchspansrc.fromJson(ve))

        val in23 = mapKV.get("mean")
        in23.map(ve => mean.fromJson(ve))

        val in24 = mapKV.get("y0")
        in24.map(ve => y0.fromJson(ve))

        val in25 = mapKV.get("ysrc")
        in25.map(ve => ysrc.fromJson(ve))

        val in26 = mapKV.get("idssrc")
        in26.map(ve => idssrc.fromJson(ve))

        val in27 = mapKV.get("lowerfencesrc")
        in27.map(ve => lowerfencesrc.fromJson(ve))

        val in28 = mapKV.get("text")
        in28.map(ve => text.fromJson(ve))

        val in29 = mapKV.get("mediansrc")
        in29.map(ve => mediansrc.fromJson(ve))

        val in30 = mapKV.get("whiskerwidth")
        in30.map(ve => whiskerwidth.fromJson(ve))

        val in31 = mapKV.get("hoverinfosrc")
        in31.map(ve => hoverinfosrc.fromJson(ve))

        val in32 = mapKV.get("hovertextsrc")
        in32.map(ve => hovertextsrc.fromJson(ve))

        val in33 = mapKV.get("x0")
        in33.map(ve => x0.fromJson(ve))

        val in34 = mapKV.get("alignmentgroup")
        in34.map(ve => alignmentgroup.fromJson(ve))

        val in35 = mapKV.get("selectedpoints")
        in35.map(ve => selectedpoints.fromJson(ve))

        val in36 = mapKV.get("notchwidth")
        in36.map(ve => notchwidth.fromJson(ve))

        val in37 = mapKV.get("pointpos")
        in37.map(ve => pointpos.fromJson(ve))

        val in38 = mapKV.get("meta")
        in38.map(ve => meta.fromJson(ve))

        val in39 = mapKV.get("quartilemethod")
        in39.map(ve => quartilemethod.fromJson(ve))

        val in40 = mapKV.get("upperfence")
        in40.map(ve => upperfence.fromJson(ve))

        val in41 = mapKV.get("offsetgroup")
        in41.map(ve => offsetgroup.fromJson(ve))

        val in42 = mapKV.get("median")
        in42.map(ve => median.fromJson(ve))

        val in43 = mapKV.get("textsrc")
        in43.map(ve => textsrc.fromJson(ve))

        val in44 = mapKV.get("hovertext")
        in44.map(ve => hovertext.fromJson(ve))

        val in45 = mapKV.get("fillcolor")
        in45.map(ve => fillcolor.fromJson(ve))

        val in46 = mapKV.get("sdsrc")
        in46.map(ve => sdsrc.fromJson(ve))

        val in47 = mapKV.get("orientation")
        in47.map(ve => orientation.fromJson(ve))

        val in48 = mapKV.get("q3src")
        in48.map(ve => q3src.fromJson(ve))

        val in49 = mapKV.get("q3")
        in49.map(ve => q3.fromJson(ve))

        val in50 = mapKV.get("uid")
        in50.map(ve => uid.fromJson(ve))

        val in51 = mapKV.get("legendgroup")
        in51.map(ve => legendgroup.fromJson(ve))

        val in52 = mapKV.get("hoveron")
        in52.map(ve => hoveron.fromJson(ve))

        val in53 = mapKV.get("opacity")
        in53.map(ve => opacity.fromJson(ve))

        val in54 = mapKV.get("q1src")
        in54.map(ve => q1src.fromJson(ve))

        val in55 = mapKV.get("xcalendar")
        in55.map(ve => xcalendar.fromJson(ve))

        val in56 = mapKV.get("sd")
        in56.map(ve => sd.fromJson(ve))

        val in57 = mapKV.get("ids")
        in57.map(ve => ids.fromJson(ve))

        val in58 = mapKV.get("dx")
        in58.map(ve => dx.fromJson(ve))

        val in59 = mapKV.get("yaxis")
        in59.map(ve => yaxis.fromJson(ve))

        val in60 = mapKV.get("xaxis")
        in60.map(ve => xaxis.fromJson(ve))

        val in61 = mapKV.get("q1")
        in61.map(ve => q1.fromJson(ve))

        val in62 = mapKV.get("upperfencesrc")
        in62.map(ve => upperfencesrc.fromJson(ve))

        val in63 = mapKV.get("width")
        in63.map(ve => width.fromJson(ve))

        val in64 = mapKV.get("metasrc")
        in64.map(ve => metasrc.fromJson(ve))

        val in65 = mapKV.get("customdata")
        in65.map(ve => customdata.fromJson(ve))

        val in66 = mapKV.get("showlegend")
        in66.map(ve => showlegend.fromJson(ve))

        val in67 = mapKV.get("boxmean")
        in67.map(ve => boxmean.fromJson(ve))

        val in68 = mapKV.get("lowerfence")
        in68.map(ve => lowerfence.fromJson(ve))

        val in69 = mapKV.get("notchspan")
        in69.map(ve => notchspan.fromJson(ve))

        val in70 = mapKV.get("customdatasrc")
        in70.map(ve => customdatasrc.fromJson(ve))

      }
      this
    }

  }

}
