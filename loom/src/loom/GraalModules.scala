package loom

import java.io.File
import mill._
import mill.scalalib.publish._
import scalalib._
import coursier.MavenRepository
import mill.define.{Command, Input, Sources, Target}
import os.{Path, PermSet, ProcessOutput}

trait GraalModules extends JavaModule {

  val graalvmVersion = "19.3.0"
  var useGraal = true

  override def ivyDeps = T {
    Agg(
      ivy"org.graalvm.sdk:graal-sdk:$graalvmVersion", // https://github.com/graalvm/graal-js-jdk11-maven-demo
      ivy"org.graalvm.js:js:$graalvmVersion", // https://github.com/hmf/graal-js-jdk11-maven-demo
      ivy"org.graalvm.js:js-scriptengine:$graalvmVersion",
      ivy"org.graalvm.tools:profiler:$graalvmVersion",
      ivy"org.graalvm.tools:chromeinspector:$graalvmVersion",
      ivy"org.graalvm.sdk:graal-sdk:$graalvmVersion" // graal-sdk.jar, NOT required - just for IDE
    )
  }

  def graalToolsDeps: T[Agg[Dep]] = T {
    Agg(
      ivy"org.graalvm.compiler:compiler:$graalvmVersion",    // compiler.jar
      ivy"org.graalvm.truffle:truffle-api:$graalvmVersion",  // truffle-api.jar
      ivy"org.graalvm.sdk:graal-sdk:$graalvmVersion"         // graal-sdk.jar
    )
  }

  def graalToolsClasspath: T[Agg[PathRef]] = T {
    resolveDeps(graalToolsDeps)
  }

  /**
   * Sets the correct JVM parameters in order to enable or disable the GraalVM
   * modules, including the compiler. Note that this value cannot be cached
   * because it is always reset by the `run`, `runBackground`,
   * `runMainBackground` and `runMain` commands. We must either use a `T.task`
   * or `T.input`. We opted for a `T.input` because, unlike the task, it is
   * not cached in a cached target and is accessible as Mill command.
   *
   * We use a T.input soi that no caching is used.
   *
   * @see https://github.com/TestFX/TestFX
   *      https://github.com/TestFX/Monocle
   * @return
   */
  def graalArgs: Input[Seq[String]] = T.input {
    // Path to Graal JDK tools
    val graalDeps = graalToolsClasspath().map(_.path.toIO.getAbsolutePath)
    val libPath = graalDeps.mkString(java.io.File.pathSeparator)
    val baseModuleConfig =
      Seq(
        // For Graal use
        "-XX:+UnlockExperimentalVMOptions",
        "-XX:+EnableJVMCI",
        s"--module-path=$libPath"
      )

    if (useGraal) {
      val compiler = graalDeps.filter( _.matches(".+compiler.+\\.jar")).seq.toSeq.head
      Seq(s"--upgrade-module-path=$compiler") ++ baseModuleConfig
    } else {
      baseModuleConfig
    }
  }

  override def forkArgs: Target[Seq[String]] = T { graalArgs() }

  val NOGRAAL = "nograal"

  /**
   * The GraavVM is used by default. It may be deactivated by passing the
   * "noGraal" parameter to the run and test commands. These fork arguments
   * are also used by the test module. However if we pass an invalid flag
   * to the test framework such as JUnit, it will fail silently. So we
   * remove the Graal flag so that the test can execute correctly.
   *
   * @param args command arguments to run and test
   */
  def setForkArgs(args: Seq[String]): Seq[String] = {
    useGraal = true
    // Index the lowercase version of the arguments
    val trimmedArgs = args.zipWithIndex.map(e => (e._1.trim.toLowerCase, e._2))
    // Check if the flag has been set
    val idx = trimmedArgs.find( _._1.equals(NOGRAAL))
    // If not set leave the args as is
    idx.fold(args)( { e =>
      useGraal = false
      // get the original non-lowercase arguments
      val name = args(e._2)
      // If set, then remove it
      args.filterNot(_.equals(name))
    } )
  }
  override def run(args: String*): Command[Unit] = T.command {
    super.run( setForkArgs(args):_* )
  }

  override def runBackground(args: String*): Command[Unit] = T.command {
    super.runBackground( setForkArgs(args):_* )
  }

  override def runMainBackground(mainClass: String, args: String*): Command[Unit] = T.command {
    super.runMainBackground(mainClass, setForkArgs(args):_*)
  }

  override def runMain(mainClass: String, args: String*): Command[Unit] = T.command {
    super.runMain(mainClass, setForkArgs(args):_*)
  }

}
