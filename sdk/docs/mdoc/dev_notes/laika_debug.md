# Laika Debug 

[Code fence parsing error](https://github.com/planet42/Laika/issues/94):

```
<div>
  <script>
        var trace1 = {};

        var trace2 = {};
  </script>
  <p>
    Text
  </p>
</div>
```


```
<div style="width:100%; height:100%; text-align:center;">
  <div id="myDiv3"><!-- Plotly chart will be drawn inside this DIV --></div>
  <script type="text/javascript" defer>
    <!-- JAVASCRIPT CODE GOES HERE -->
        var trace1 = {
          x: [1, 2, 3, 4],
          y: [10, 15, 13, 17],
          mode: 'markers',
          type: 'scatter'
        };

        var trace2 = {
          x: [2, 3, 4, 5],
          y: [16, 5, 11, 9],
          mode: 'lines',
          type: 'scatter'
        };

        var trace3 = {
          x: [1, 2, 3, 4],
          y: [12, 9, 15, 12],
          mode: 'lines+markers',
          type: 'scatter'
        };

        var data = [trace1, trace2, trace3];

        Plotly.newPlot('myDiv3', data);
  </script>
  <p style="text-align:center; font-weight: bold;">
    Figure 3: Embedded scatter plot example via an inlined script
  </p>
</div>
```

<div style="width:100%; height:100%; text-align:center;">
  <div id="myDiv3"><!-- Plotly chart will be drawn inside this DIV --></div>
  <script type="text/javascript" defer>
    <!-- JAVASCRIPT CODE GOES HERE -->
        var trace1 = {
          x: [1, 2, 3, 4],
          y: [10, 15, 13, 17],
          mode: 'markers',
          type: 'scatter'
        };

        var trace2 = {
          x: [2, 3, 4, 5],
          y: [16, 5, 11, 9],
          mode: 'lines',
          type: 'scatter'
        };

        var trace3 = {
          x: [1, 2, 3, 4],
          y: [12, 9, 15, 12],
          mode: 'lines+markers',
          type: 'scatter'
        };

        var data = [trace1, trace2, trace3];

        Plotly.newPlot('myDiv3', data);
  </script>
  <p style="text-align:center; font-weight: bold;">
    Figure 3: Embedded scatter plot example via an inlined script
  </p>
</div>
