package splotly

import utest._
import better.files._
import better.files.Dsl._
import java.nio.file.{Paths => JPaths}
import java.io.{File => JFile}

import webkit._
import splotly.LPlotly.Local

/**
 * Here we run WebKit to load and take snapshots of several plots.
 * These tests are executed synchronously. It is important that the tests use
 * uTests's assert method.
 *
 * IMPORTANT NOTE: JavaFX or OpenJFX applications can only be instantiated once.
 * There is a RobotFX. But for now we use a custom uTest [[CustomFramework]] to
 * launch and use a single application.
 *
 * ./mill mill.scalalib.GenIdea/idea
 *
 * ./mill -i plots.test
 * ./mill -i plots.test.testLocal
 * ./mill -i plots.test HeadlessWebKitSpec  // Does not work
 * ./mill -i plots.test splotly.HeadlessWebKitSpec
 * ./mill -i plots.test splotly.HeadlessWebKitSpec.Violin
 * ./mill -i plots.test splotly.HeadlessWebKitSpec.Violin.Sample2
 *
 * Note: use this version to access the JavaFX libraries
 * ./jfxmill.sh -i plots.test splotly.HeadlessWebKitSpec
 * ./jfxmill.sh -i plots.test splotly.HeadlessWebKitSpec.Layout
 * ./jfxmill.sh -i plots.test splotly.HeadlessWebKitSpec.Violin
 *
 * @see http://mundrisoft.com/tech-bytes/compare-images-using-java/
 *      https://stackoverflow.com/questions/8567905/how-to-compare-images-for-similarity-using-java
 */
object JsonSpec extends TestSuite {

  val tests: Tests = Tests {


    val layout1 =
      """
        |{
        |  "mapbox": {
        |    "domain": {
        |      "column": 15
        |    },
        |    "layers": {
        |      "items": {
        |        "layer": {
        |          "line": {
        |            "dash": [
        |              18,
        |              19,
        |              20
        |            ]
        |          },
        |          "coordinates": "coordinates",
        |          "name": "name",
        |          "opacity": 15
        |        }
        |      }
        |    },
        |    "style": 6,
        |    "uirevision": "version5"
        |  },
        |  "updatemenus": {
        |    "items": {
        |      "updatemenu": {
        |        "name": "updatemenu.name",
        |        "bgcolor": "updatemenu.bgcolor"
        |      }
        |    }
        |  },
        |  "polar": {
        |    "domain": {
        |      "x": [
        |        45,
        |        46
        |      ]
        |    },
        |    "angularaxis": {
        |      "tickangle": 50
        |    }
        |  },
        |  "coloraxis": {
        |    "colorscale": [
        |      0,
        |      "black"
        |    ]
        |  },
        |  "datarevision": "version1",
        |  "editrevision": "version2",
        |  "autosize": true,
        |  "colorway": "blue"
        |}
        |""".stripMargin

    val l1 = splotly.gen.Layout()
    l1.layoutAttributes.clickmode.noneOn()
    l1.layoutAttributes.autosize.setValue(true)
    l1.layoutAttributes.datarevision.setValue("version1")
    l1.layoutAttributes.editrevision.setValue("version2")
    l1.layoutAttributes.mapbox.style.setValue(6)
    l1.layoutAttributes.mapbox.uirevision.setValue("version5")
    l1.layoutAttributes.mapbox.layers.items.layer.coordinates.setValue("coordinates")
    l1.layoutAttributes.mapbox.layers.items.layer.name.setValue("name")
    l1.layoutAttributes.mapbox.layers.items.layer.opacity.setValue(15)
    l1.layoutAttributes.mapbox.domain.column.setValue(15)
    l1.layoutAttributes.updatemenus.items.updatemenu.name.setValue("updatemenu.name")
    l1.layoutAttributes.updatemenus.items.updatemenu.bgcolor.setValue("updatemenu.bgcolor")
    l1.layoutAttributes.polar.angularaxis.tickangle.setValue(50.0)
    l1.layoutAttributes.polar.domain.x.int_0(45)
    l1.layoutAttributes.polar.domain.x.int_1(46)
    l1.layoutAttributes.mapbox.layers.items.layer.line.dash.setValue(Array(18, 19, 20))
    l1.layoutAttributes.colorway.setValue("blue")
    l1.layoutAttributes.coloraxis.colorscale.setValue(0.0, "black")

    test ("Layout") {

      test ("Small Sample out") {
        val layoutJson = ujson.read(layout1)
        // https://youtrack.jetbrains.com/issue/SCL-17179#newissue=yes
        // Get JSON output of layout
        val v1Out = l1.layoutAttributes.json()
        val v1OutJson = ujson.read(v1Out)
        val v1In = ujson.read(v1OutJson)

        // Same as the original
        assert( Utils.sameJson(layoutJson,v1OutJson) )
        // Also checks for numeric difference int vs double
        Utils.checkJsonDifference(layout1, v1Out)

      }

      test ("Small Sample out") {
        val layoutJson = ujson.read(layout1)
        // https://youtrack.jetbrains.com/issue/SCL-17179#newissue=yes
        // Read l2 from JSON
        val l2 = splotly.gen.Layout()
        l2.layoutAttributes.fromJson(layoutJson)
        val v2Out = l2.layoutAttributes.json()
        // Parse it into JSON
        val v2OutJson = ujson.read(v2Out)

        // Must be the same as original
        assert( Utils.sameJson(layoutJson,v2OutJson) )
        // Also checks for numeric difference int vs double
        Utils.checkJsonDifference(layout1, v2Out)
      }

      test ("Large Sample") {
        val l1 = splotly.gen.Layout()
        l1.layoutAttributes.clickmode.noneOn()
        l1.layoutAttributes.datarevision.setValue("version1")
        l1.layoutAttributes.editrevision.setValue("version2")
        l1.layoutAttributes.hidesources.setValue(true)
        l1.layoutAttributes.selectionrevision.setValue("version3")
        l1.layoutAttributes.paper_bgcolor.setValue("white")
        l1.layoutAttributes.hoverdistance.setValue(2)
        l1.layoutAttributes.uirevision.setValue("version4")
        l1.layoutAttributes.selectdirection.any()
        l1.layoutAttributes.height.setValue(1.0)
        l1.layoutAttributes.direction.clockwise()
        l1.layoutAttributes.calendar.gregorian()
        l1.layoutAttributes.meta.setValue("trace.name")
        l1.layoutAttributes.spikedistance.setValue(3)
        l1.layoutAttributes.separators.setValue("s")
        l1.layoutAttributes.orientation.setValue(4.0)
        l1.layoutAttributes.plot_bgcolor.setValue("template")
        l1.layoutAttributes.plot_bgcolor.setValue("creme")
        l1.layoutAttributes.hovermode.closest()
        l1.layoutAttributes.dragmode.pan()
        l1.layoutAttributes.autosize.setValue(true)
        l1.layoutAttributes.width.setValue(5.0)
        l1.layoutAttributes.metasrc.setValue("metasrc")
        l1.layoutAttributes.showlegend.setValue(true)
        l1.layoutAttributes.colorway.setValue("blue")
        l1.layoutAttributes.mapbox.style.setValue(6)
        l1.layoutAttributes.mapbox.uirevision.setValue("version5")
        l1.layoutAttributes.mapbox.pitch.setValue(7.0)
        l1.layoutAttributes.mapbox.zoom.setValue(8.0)
        l1.layoutAttributes.mapbox.bearing.setValue(9.0)
        l1.layoutAttributes.mapbox.accesstoken.setValue("accesstoken")
        l1.layoutAttributes.mapbox.domain.y.double_0(10.0)
        l1.layoutAttributes.mapbox.domain.y.double_1(11.0)
        l1.layoutAttributes.mapbox.domain.x.double_0(12.0)
        l1.layoutAttributes.mapbox.domain.x.double_1(13.0)
        l1.layoutAttributes.mapbox.domain.row.setValue(14)
        l1.layoutAttributes.mapbox.domain.column.setValue(15)
        l1.layoutAttributes.mapbox.layers.items.layer.coordinates.setValue("coordinates")
        l1.layoutAttributes.mapbox.layers.items.layer.name.setValue("name")
        l1.layoutAttributes.mapbox.layers.items.layer.source.setValue("source")
        l1.layoutAttributes.mapbox.layers.items.layer.below.setValue("below")
        l1.layoutAttributes.mapbox.layers.items.layer.visible.setValue(true)
        l1.layoutAttributes.mapbox.layers.items.layer.sourceattribution.setValue("sourceattribution")
        l1.layoutAttributes.mapbox.layers.items.layer.sourcetype.image()
        l1.layoutAttributes.mapbox.layers.items.layer.color.setValue("pink")
        l1.layoutAttributes.mapbox.layers.items.layer.sourcelayer.setValue("sourcelayer")
        l1.layoutAttributes.mapbox.layers.items.layer.opacity.setValue(15.0)
        l1.layoutAttributes.mapbox.layers.items.layer.minzoom.setValue(16.0)
        l1.layoutAttributes.mapbox.layers.items.layer.type_.raster()
        l1.layoutAttributes.mapbox.layers.items.layer.maxzoom.setValue(17.0)
        l1.layoutAttributes.mapbox.layers.items.layer.templateitemname.setValue("templateitemname")
        l1.layoutAttributes.mapbox.layers.items.layer.circle.radius.setValue(18.0)
        l1.layoutAttributes.mapbox.layers.items.layer.line.dash.setValue(Array(18.0, 19.0, 20.0))
        l1.layoutAttributes.mapbox.layers.items.layer.line.dashsrc.setValue("dashsrc")
        l1.layoutAttributes.mapbox.layers.items.layer.line.width.setValue(21.0)
        l1.layoutAttributes.mapbox.layers.items.layer.symbol.iconsize.setValue(22.0)
        l1.layoutAttributes.mapbox.layers.items.layer.symbol.icon.setValue("icon")
        l1.layoutAttributes.mapbox.layers.items.layer.symbol.text.setValue("text")
        l1.layoutAttributes.mapbox.layers.items.layer.symbol.textposition.middle_center()
        l1.layoutAttributes.mapbox.layers.items.layer.symbol.placement.line_center()
        l1.layoutAttributes.mapbox.layers.items.layer.symbol.textfont.color.setValue("textfont.color")
        l1.layoutAttributes.mapbox.layers.items.layer.symbol.textfont.family.setValue("textfont.family")
        l1.layoutAttributes.mapbox.layers.items.layer.symbol.textfont.size.setValue(23)
        l1.layoutAttributes.mapbox.layers.items.layer.fill.outlinecolor.setValue("fill.outlinecolor")
        l1.layoutAttributes.mapbox.center.lat.setValue(24.0)
        l1.layoutAttributes.mapbox.center.lon.setValue(25.0)
        l1.layoutAttributes.updatemenus.items.updatemenu.x.setValue(26.0)
        l1.layoutAttributes.updatemenus.items.updatemenu.name.setValue("updatemenu.name")
        l1.layoutAttributes.updatemenus.items.updatemenu.bgcolor.setValue("updatemenu.bgcolor")
        l1.layoutAttributes.updatemenus.items.updatemenu.visible.setValue(false)
        l1.layoutAttributes.updatemenus.items.updatemenu.y.setValue(27.0)
        l1.layoutAttributes.updatemenus.items.updatemenu.yanchor.middle()
        l1.layoutAttributes.updatemenus.items.updatemenu.showactive.setValue(false)
        l1.layoutAttributes.updatemenus.items.updatemenu.direction.up()
        l1.layoutAttributes.updatemenus.items.updatemenu.bordercolor.setValue("updatemenu.bordercolor")
        l1.layoutAttributes.margin.t.setValue(28.0)
        l1.layoutAttributes.margin.pad.setValue(29.0)
        l1.layoutAttributes.margin.b.setValue(30.0)
        l1.layoutAttributes.margin.l.setValue(31.0)
        l1.layoutAttributes.margin.r.setValue(32.0)
        l1.layoutAttributes.margin.autoexpand.setValue(true)
        l1.layoutAttributes.angularaxis.tickorientation.vertical()
        l1.layoutAttributes.angularaxis.visible.setValue(true)
        l1.layoutAttributes.angularaxis.domain.double_0(33.0)
        l1.layoutAttributes.angularaxis.domain.double_1(34.0)
        l1.layoutAttributes.angularaxis.ticksuffix.setValue("angularaxis.ticksuffix")
        l1.layoutAttributes.angularaxis.showline.setValue(true)
        l1.layoutAttributes.angularaxis.ticklen.setValue(35.0)
        l1.layoutAttributes.angularaxis.endpadding.setValue(36.0)
        l1.layoutAttributes.angularaxis.range.double_0(37.0)
        l1.layoutAttributes.angularaxis.range.double_1(38.0)
        l1.layoutAttributes.angularaxis.tickcolor.setValue("angularaxis.tickcolor")
        l1.layoutAttributes.angularaxis.showticklabels.setValue(true)

        l1.layoutAttributes.modebar.bgcolor.setValue("modebar.bgcolor")
        l1.layoutAttributes.modebar.uirevision.setValue("modebar.uirevision")
        l1.layoutAttributes.modebar.color.setValue("modebar.color")
        l1.layoutAttributes.modebar.activecolor.setValue("modebar.activecolor")
        l1.layoutAttributes.modebar.orientation.h()

        l1.layoutAttributes.polar.bgcolor.setValue("polar.bgcolor")
        l1.layoutAttributes.polar.gridshape.circular()
        l1.layoutAttributes.polar.uirevision.setValue("polar.uirevision")
        l1.layoutAttributes.polar.sector.double_0(39.0)
        l1.layoutAttributes.polar.sector.double_1(40.0)
        l1.layoutAttributes.polar.hole.setValue(41.0)
        l1.layoutAttributes.polar.domain.column.setValue(42)
        l1.layoutAttributes.polar.domain.y.double_0(43.0)
        l1.layoutAttributes.polar.domain.y.double_1(44.0)
        l1.layoutAttributes.polar.domain.x.double_0(45.0)
        l1.layoutAttributes.polar.domain.x.double_1(46.0)
        l1.layoutAttributes.polar.domain.row.setValue(47)

        l1.layoutAttributes.polar.angularaxis.linewidth.setValue(48.0)
        l1.layoutAttributes.polar.angularaxis.tick0.setValue("angularaxis.tick0")
        l1.layoutAttributes.polar.angularaxis.nticks.setValue(49)
        l1.layoutAttributes.polar.angularaxis.tickangle.setValue(50.0)
        l1.layoutAttributes.polar.angularaxis.showexponent.all()
        l1.layoutAttributes.polar.angularaxis.linecolor.setValue("angularaxis.linecolor")
        l1.layoutAttributes.polar.angularaxis.visible.setValue(true)
        l1.layoutAttributes.polar.angularaxis.categoryarray.setValue(Array(51.0, 52.0))
        l1.layoutAttributes.polar.angularaxis.uirevision.setValue("angularaxis.uirevision")
        l1.layoutAttributes.polar.angularaxis.categoryarraysrc.setValue("angularaxis.categoryarraysrc")
        l1.layoutAttributes.polar.angularaxis.ticksuffix.setValue("angularaxis.ticksuffix")
        l1.layoutAttributes.polar.angularaxis.tickwidth.setValue(53.0)
        l1.layoutAttributes.polar.angularaxis.tickvals.setValue(Array(54.0, 55.0))
        l1.layoutAttributes.polar.angularaxis.direction.counterclockwise()
        l1.layoutAttributes.polar.angularaxis.rotation.setValue(56.0)
        l1.layoutAttributes.polar.angularaxis.categoryorder.category_descending()
        l1.layoutAttributes.polar.angularaxis.color.setValue("angularaxis.color")
        l1.layoutAttributes.polar.angularaxis.showline.setValue(true)
        l1.layoutAttributes.polar.angularaxis.ticklen.setValue(57.0)
        l1.layoutAttributes.polar.angularaxis.layer.below_traces()
        l1.layoutAttributes.polar.angularaxis.tickmode.linear()
        l1.layoutAttributes.polar.angularaxis.dtick.setValue("angularaxis.dtick")
        l1.layoutAttributes.polar.angularaxis.tickvalssrc.setValue("angularaxis.tickvalssrc")
        l1.layoutAttributes.polar.angularaxis.showticksuffix.all()
        l1.layoutAttributes.polar.angularaxis.ticktext.setValue(Array(58.0, 59.0))
        l1.layoutAttributes.polar.angularaxis.ticks.inside()
        l1.layoutAttributes.polar.angularaxis.tickcolor.setValue("angularaxis.tickcolor")
        l1.layoutAttributes.polar.angularaxis.ticktextsrc.setValue("angularaxis.ticktextsrc")
        l1.layoutAttributes.polar.angularaxis.showgrid.setValue(true)
        l1.layoutAttributes.polar.angularaxis.exponentformat.power()
        l1.layoutAttributes.polar.angularaxis.showtickprefix.none()
        l1.layoutAttributes.polar.angularaxis.tickprefix.setValue("angularaxis.tickprefix")
        l1.layoutAttributes.polar.angularaxis.type_.-()
        l1.layoutAttributes.polar.angularaxis.gridcolor.setValue("angularaxis.gridcolor")
        l1.layoutAttributes.polar.angularaxis.separatethousands.setValue(true)
        l1.layoutAttributes.polar.angularaxis.hoverformat.setValue("angularaxis.hoverformat")
        l1.layoutAttributes.polar.angularaxis.tickformat.setValue("angularaxis.tickformat")
        l1.layoutAttributes.polar.angularaxis.showticklabels.setValue(true)
        l1.layoutAttributes.polar.angularaxis.period.setValue(60.0)
        l1.layoutAttributes.polar.angularaxis.gridwidth.setValue(61.0)
        l1.layoutAttributes.polar.angularaxis.thetaunit.degrees()
        l1.layoutAttributes.polar.angularaxis.tickfont.color.setValue("tickfont.color")
        l1.layoutAttributes.polar.angularaxis.tickfont.family.setValue("tickfont.family")
        l1.layoutAttributes.polar.angularaxis.tickfont.size.setValue(62.0)
        l1.layoutAttributes.polar.angularaxis.tickformatstops.items.tickformatstop.dtickrange.int_0(63)
        l1.layoutAttributes.polar.angularaxis.tickformatstops.items.tickformatstop.dtickrange.double_0(64.0)
        l1.layoutAttributes.polar.angularaxis.tickformatstops.items.tickformatstop.name.setValue("tickformatstop.name")
        l1.layoutAttributes.polar.angularaxis.tickformatstops.items.tickformatstop.enabled.setValue(true)
        l1.layoutAttributes.polar.angularaxis.tickformatstops.items.tickformatstop.value.setValue("tickformatstop.value")
        l1.layoutAttributes.polar.angularaxis.tickformatstops.items.tickformatstop.templateitemname.setValue("tickformatstop.templateitemname")
        l1.layoutAttributes.polar.radialaxis.linewidth.setValue(65.0)
        l1.layoutAttributes.polar.radialaxis.side.counterclockwise()
        l1.layoutAttributes.polar.radialaxis.tick0.setValue("radialaxis.tick0")
        l1.layoutAttributes.polar.radialaxis.nticks.setValue(66)
        l1.layoutAttributes.polar.radialaxis.tickangle.setValue(67.0)
        l1.layoutAttributes.polar.radialaxis.showexponent.first()

        l1.layoutAttributes.polar.radialaxis.linecolor.setValue("radialaxis.linecolor")
        l1.layoutAttributes.polar.radialaxis.visible.setValue(true)
        l1.layoutAttributes.polar.radialaxis.categoryarray.setValue(Array(68.0, 69.0))
        l1.layoutAttributes.polar.radialaxis.angle.setValue(70.0)
        l1.layoutAttributes.polar.radialaxis.uirevision.setValue("uirevision.setValue")
        l1.layoutAttributes.polar.radialaxis.categoryarraysrc.setValue("radialaxis.categoryarraysrc")
        l1.layoutAttributes.polar.radialaxis.ticksuffix.setValue("radialaxis.ticksuffix")
        l1.layoutAttributes.polar.radialaxis.tickwidth.setValue(60.0)
        l1.layoutAttributes.polar.radialaxis.tickvals.setValue(Array(71.0, 72.0))
        l1.layoutAttributes.polar.radialaxis.categoryorder.sum_ascending()
        l1.layoutAttributes.polar.radialaxis.color.setValue("radialaxis.color")
        l1.layoutAttributes.polar.radialaxis.calendar.gregorian()
        l1.layoutAttributes.polar.radialaxis.showline.setValue(true)
        l1.layoutAttributes.polar.radialaxis.ticklen.setValue(73.0)
        l1.layoutAttributes.polar.radialaxis.layer.below_traces()
        l1.layoutAttributes.polar.radialaxis.rangemode.nonnegative()
        l1.layoutAttributes.polar.radialaxis.tickmode.linear()
        l1.layoutAttributes.polar.radialaxis.dtick.setValue("radialaxis.dtick")
        l1.layoutAttributes.polar.radialaxis.tickvalssrc.setValue("radialaxis.tickvalssrc")
        l1.layoutAttributes.polar.radialaxis.range.boolean_0(false)
        l1.layoutAttributes.polar.radialaxis.range.int_1(74)
        l1.layoutAttributes.polar.radialaxis.showticksuffix.all()
        l1.layoutAttributes.polar.radialaxis.ticktext.setValue(Array(75.0, 76.0))
        l1.layoutAttributes.polar.radialaxis.ticks.default()
        l1.layoutAttributes.polar.radialaxis.tickcolor.setValue("radialaxis.tickcolor")
        l1.layoutAttributes.polar.radialaxis.ticktextsrc.setValue("radialaxis.ticktextsrc")
        l1.layoutAttributes.polar.radialaxis.showgrid.setValue(true)
        l1.layoutAttributes.polar.radialaxis.exponentformat.SI()
        l1.layoutAttributes.polar.radialaxis.showtickprefix.first()
        l1.layoutAttributes.polar.radialaxis.tickprefix.setValue("radialaxis.tickprefix")
        l1.layoutAttributes.polar.radialaxis.autorange.false_()
        l1.layoutAttributes.polar.radialaxis.type_.date()
        l1.layoutAttributes.polar.radialaxis.gridcolor.setValue("radialaxis.gridcolor")
        l1.layoutAttributes.polar.radialaxis.separatethousands.setValue(true)
        l1.layoutAttributes.polar.radialaxis.hoverformat.setValue("radialaxis.hoverformat")
        l1.layoutAttributes.polar.radialaxis.tickformat.setValue("radialaxis.tickformat")
        l1.layoutAttributes.polar.radialaxis.showticklabels.setValue(true)
        l1.layoutAttributes.polar.radialaxis.gridwidth.setValue(77.0)

        l1.layoutAttributes.polar.radialaxis.tickfont.color.setValue("tickfont.color")
        l1.layoutAttributes.polar.radialaxis.tickfont.family.setValue("tickfont.family")
        l1.layoutAttributes.polar.radialaxis.tickfont.size.setValue(78)
        l1.layoutAttributes.polar.radialaxis.title.text.setValue("radialaxis.text")
        l1.layoutAttributes.polar.radialaxis.title.font.color.setValue("radialaxis.font.color")
        l1.layoutAttributes.polar.radialaxis.title.font.family.setValue("radialaxis.font.family")
        l1.layoutAttributes.polar.radialaxis.title.font.size.setValue(79)

        l1.layoutAttributes.polar.radialaxis.tickformatstops.items.tickformatstop.name.setValue("tickformatstop.name")
        l1.layoutAttributes.polar.radialaxis.tickformatstops.items.tickformatstop.enabled.setValue(true)
        l1.layoutAttributes.polar.radialaxis.tickformatstops.items.tickformatstop.value.setValue("tickformatstop.value")
        l1.layoutAttributes.polar.radialaxis.tickformatstops.items.tickformatstop.templateitemname.setValue("tickformatstop.templateitemname")

        l1.layoutAttributes.coloraxis.colorscale.setValue(0.0, "black")

        val v1Out = l1.layoutAttributes.json()
        val v1OutJson = ujson.read(v1Out)

        val l2 = splotly.gen.Layout()
        l2.layoutAttributes.fromJson(v1Out)
        //l2.layoutAttributes.datarevision.setValue("versionx") // sanity checking
        val v2Out = l2.layoutAttributes.json()
        val v2OutJson = ujson.read(v2Out)

        assert( Utils.sameJson(v2OutJson,v1OutJson) )
        // TODO: we need to know when we have integers/reals
        // See for example l1.layoutAttributes.mapbox.domain.y.double_0(10.0)
        // Also checks for numeric difference int vs double
        // Utils.checkJsonDifference(v1Out, v2Out)

        /* TODO: Object comparison does not work
        println(l1.layoutAttributes.datarevision.getValue)
        println(l2.layoutAttributes.datarevision.getValue)
        assert(l1  == l2)
        assert(l1.equals(l2))
         */
      }

    }

    test("Violin") {

      test("Layout attributes comparison") {
        // @see https://plot.ly/javascript/violin/
        val layout =
          """
            |{
            |  "violingap": 1,
            |  "violingroupgap": 2,
            |  "violinmode": "overlay"
            |}
            |""".stripMargin
        val layoutJson = ujson.read(layout)

        val v1 = gen.Violin()
        v1.layoutAttributes.violingap.setValue(1)
        v1.layoutAttributes.violingroupgap.setValue(2)
        v1.layoutAttributes.violinmode.overlay()

        val v1Out = v1.layoutAttributes.json()
        val v1OutJson = ujson.read(v1Out)
        assert( Utils.sameJson(layoutJson,v1OutJson) )
        // Also checks for numeric difference int vs double
        Utils.checkJsonDifference(layout, v1Out)
      }

      test("Plot attributes comparison") {
        // @see https://plot.ly/javascript/violin/
        // @see https://github.com/plotly/plotly.js/issues/4617
        // BUG: missing
        // "boxpoints": false,
        // BUG: not in enumerated
        // "points": "none",

        val layout =
          """
            |{
            |  "type": "violin",
            |  "points": false,
            |  "box": {
            |    "visible": true
            |  },
            |  "line": {
            |    "color": "black"
            |  },
            |  "fillcolor": "#8dd3c7",
            |  "opacity": 0.6,
            |  "meanline": {
            |    "visible": true
            |  },
            |  "x0": "Total Bill"
            |}
            |
            |""".stripMargin
        val layoutJson = ujson.read(layout)

        val v1 = gen.Violin()
        v1.attributes.points.false_()
        v1.attributes.box.visible.setValue(true)
        v1.attributes.line.color.setValue("black")
        v1.attributes.fillcolor.setValue("#8dd3c7")
        v1.attributes.opacity.setValue(0.6)
        v1.attributes.meanline.visible.setValue(true)
        v1.attributes.x0.setValue("Total Bill")

        val v1Out = v1.attributes.json()
        val v1OutJson = ujson.read(v1Out)

        assert( Utils.sameJson(layoutJson,v1OutJson) )
        // Also checks for numeric difference int vs double
        Utils.checkJsonDifference(layout, v1Out)
      }

      test("Layout attributes merge") {
        // @see https://plot.ly/javascript/violin/
        // @see https://github.com/plotly/plotly.js/issues/4617
        val layout =
        """
          |{
          |  "title": {
          |    "text":"Split Violin Plot",
          |    "font": {
          |      "family": "Courier New, monospace",
          |      "size": 24
          |    },
          |    "xref": "paper",
          |    "x": 0.05
          |  },
          |  "yaxis": {
          |    "zeroline": false
          |  },
          |  "violingap": 0,
          |  "violingroupgap": 0,
          |  "violinmode": "overlay"
          |}
          |""".stripMargin

        val layoutJson = ujson.read(layout)

        val v1 = gen.Violin()
        v1.layoutAttributes.violingap.setValue(0)
        v1.layoutAttributes.violingroupgap.setValue(0)
        v1.layoutAttributes.violinmode.overlay()

        val l1 = splotly.gen.Layout()
        l1.layoutAttributes.title.text.setValue("Split Violin Plot")
        l1.layoutAttributes.title.font.family.setValue("Courier New, monospace")
        l1.layoutAttributes.title.font.size.setValue(24)
        l1.layoutAttributes.title.xref.paper()
        l1.layoutAttributes.title.x.setValue(0.05)
        l1.layoutAttributes.yaxis.zeroline.setValue(false)

        val l3 = LPlotly.mergeLayout(l1, v1)
        assert(l3.isRight)
        l3.fold(_ => assert(false), {e =>
          val mergedJson = ujson.read(e)
          assert( Utils.sameJson(layoutJson,mergedJson) )
          // Also checks for numeric difference int vs double
          Utils.checkJsonDifference(layout, e)
        })
      }

    }
  }

}
