package loom

import java.io.File
import mill._
import mill.scalalib.publish._
import scalalib._
import coursier.MavenRepository
import mill.define.{Command, Input, Sources, Target}
import os.{Path, PermSet, ProcessOutput}

trait OpenFXModules extends JavaModule {
  val monocleVersion = "jdk-11+26"
  val jfxVersion = "11.0.2"

  // Add JavaFX dependencies
  // https://github.com/scalafx/scalafx-hello-world/blob/master/build.sbt
  private lazy val javaFXModules = Seq("base", "controls", "fxml", "graphics", "media", "swing", "web")
  private lazy val jfxMonocle = ivy"org.testfx:openjfx-monocle:$monocleVersion"
  private lazy val javaFX = javaFXModules.map(m =>
    ivy"org.openjfx:javafx-$m:$jfxVersion"
    //ivy"org.openjfx:javafx-$m:11.0.2"
    //ivy"org.openjfx:javafx-$m:12.0.2"
    //ivy"org.openjfx:javafx-$m:13"
  ) ++ Seq(jfxMonocle)

  override def ivyDeps = T{ Agg(javaFX:_*) }

  def monocleToolsNoDeps: T[Agg[Dep]] = T {
    Agg(jfxMonocle.exclude(("*","*")))
  }

  def monocleLibClasspath: T[Agg[PathRef]] = T {
    resolveDeps(monocleToolsNoDeps)
  }

  // We use a T.input soi that no caching is used
  def openFXArgs: Input[Seq[String]] = T.input {
    // Get just the Monocle jar (no other dependencies)
    //val monocleDeps = monocleLibClasspath().map(_.path.toIO.getAbsolutePath).filter(_.toLowerCase.contains("monocle"))
    val monocleDeps = monocleLibClasspath().map(_.path.toIO.getAbsolutePath).toList.mkString(File.separator)
    // Get the Monocle library path
    val monocle = monocleLibClasspath().map(_.path.toNIO.getParent.toAbsolutePath).toList.head
    // Full module package names
    val fxModules = javaFXModules.map("javafx."+_)
    val monoclePackage = "org.testfx.monocle"
    val modules = (Seq(monoclePackage) ++ fxModules).mkString(",")
    Seq(
      // Monocle must also be included here
      s"--module-path=$monocleDeps:/usr/share/openjfx/lib", // TODO: auto create
      // javafx.graphics, not required
      s"--add-modules=$modules",
      // For javafx Monocle use
      s"--patch-module=javafx.graphics=$monocle",
      s"--add-exports=javafx.graphics/com.sun.glass.ui=$monoclePackage",
      //"--add-exports=javafx.graphics/com.sun.glass.ui=ALL-UNNAMED",
      "--add-reads=javafx.graphics=ALL-UNNAMED",
      "--add-opens=javafx.graphics/com.sun.glass.ui=ALL-UNNAMED",
      "--add-opens=javafx.controls/com.sun.javafx.charts=ALL-UNNAMED",
      "--add-opens=javafx.graphics/com.sun.javafx.iio=ALL-UNNAMED",
      "--add-opens=javafx.graphics/com.sun.javafx.iio.common=ALL-UNNAMED",
      "--add-opens=javafx.graphics/com.sun.javafx.css=ALL-UNNAMED",
      "--add-opens=javafx.graphics/com.sun.javafx.application=ALL-UNNAMED",
      "--add-opens=javafx.base/com.sun.javafx.runtime=ALL-UNNAMED"
    )
  }

  override def forkArgs: Target[Seq[String]] = T { openFXArgs() }

}
