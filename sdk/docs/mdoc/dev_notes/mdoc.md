# MDoc Preprocessor 

## Introduction

[MDoc](https://scalameta.org/mdoc/) is a Scala Markdown code-fence preprocessor. It is used to compile, type
check and execute code snippets embedded in the Markdown as code fences. For example the following had been
compiled and executed by MDoc:  

```scala mdoc
val valx = 1
List(valx, valx)
```

The output of the MDoc preprocessor is another Markdown document that be further processed, visualized and 
possibly printed. 

## Invoking the preprocessor

MDoc can be [execute](https://scalameta.org/mdoc/docs/installation.html) directly via command-line (using
[coursier](https://github.com/coursier/coursier/#command-line)), via an [SBT](https://www.scala-sbt.org/) 
plugin-in or programmatically using the library's API. This project's Mill `build.sc` file (more 
information found [here](mill.html)) has two targets `mDoc`and `mDocLocal` that use the libraries API 
to execute the preprocessor programmatically. 

We can call the `mDoc` target so: 

```bash
 ./mill -i sdk.mDoc
```

We can also call the `mDocLocal` target so: 

```bash
 mill -i sdk.mDocLocal
```

These targets perform the same function and use the same parameters. However the `mDocLocal` target executes 
using [Ammonite's](https://github.com/lihaoyi/Ammonite) `$ivy` import directive. In other words it uses the 
fixed version of the MDoc library defined in the script. On the other hand `mDoc` downloads and uses the version 
of MDoc defined in the `mdocVersion` override [parameter](#mdoc-module-parameters). 
 
## MDoc Module parameters

These targets do not have any Mill call parameters. However when defining and using the `MDocModule` we must 
define and override the following variables:

```scala
import $ivy.`org.scalameta::mdoc:1.3.2`

object sdk extends MDocModule with LaikaModule {
  val appVersion = "0.1.0"

  override def scalaVersion = "2.12.8"

  // PlugIns

  // Markdown documentation: Scala code parsing
  override def mdocVersion = "1.3.2"
  override def mdocVerbose = 1
  override def mdocScalacOptions: T[Seq[String]] = super.mdocScalacOptions()
  override def mdocOpts: T[Seq[String]] = super.mdocOpts()
}
```

These parameters can be use to configure MDoc as follows:

* `mdocVersion` is the version of MDoc to download when using the mDoc target
* `mdocVerbose` for values greater than 0, activates the printout of debug information.
* `mdocScalacOptions` allows you to add flags to the Scala compiler used to compile the code snippets. 
* `mdocOpts` allows you to set any of MDoc's options using the command line interface. The flags and values 
that can be used are documented [here](https://scalameta.org/mdoc/docs/installation.html#help). Note that 
the `--in` and `--out` should **not** be used. They are automatically set. Note also that a default value for 
the filter is already provided via the scripts `MDocModule` variable `mdocNameFilter`. When you do set the
options set them as you would in the command line. Here is an example os setting MDoc's options:

```scala
  override def mdocOpts: T[Seq[String]] = super.mdocOpts() ++ List("--verbose")
```

