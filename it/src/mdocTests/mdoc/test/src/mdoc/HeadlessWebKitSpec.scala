package mdoc

import better.files.Dsl.cwd
import utest._
import webkit.{HeadlessWebKit, ImageTestUtils}


/**
 * Here we run WebKit to load and take snapshots of several plots.
 * These tests are executed synchronously. It is important that the tests use
 * uTests's assert method.
 *
 * IMPORTANT NOTE: JavaFX or OpenJFX applications can only be instantiated once.
 * There is a RobotFX. But for now we use a custom uTest [[CustomFramework]] to
 * launch and use a single application.
 *
 * mill mill.scalalib.GenIdea/idea
 * 
 * mill -i loom.test
 * mill -i loom.test.testLocal
 * mill -i loom.test loom.HeadlessWebKitSpec
 * mill -i loom.test loom.HeadlessWebKitSpec.URITests
 * mill -i loom.test loom.HeadlessWebKitSpec.ImageCapture.PNG
 *
 * Note: use this version to access the JavaFX libraries
 * ./jfxmill.sh -i loom.test loom.HeadlessWebKitSpec
 *
 * @see http://mundrisoft.com/tech-bytes/compare-images-using-java/
 *      https://stackoverflow.com/questions/8567905/how-to-compare-images-for-similarity-using-java
 */
object HeadlessWebKitSpec extends TestSuite {

  /*
  // https://github.com/lihaoyi/utest#running-code-before-and-after-test-cases
  HeadlessWebKit.launchBackground(Array[String]())

  // https://github.com/lihaoyi/utest#running-code-before-and-after-test-suites
  // https://github.com/lihaoyi/utest#054
  override def utestAfterAll(): Unit = {
    println("on after all")
    HeadlessWebKit.stop()
  }
  */
  val tests: Tests = Tests {
    throw new RuntimeException("Bla")
    assert(false)
  }

/*
  val tests: Tests = Tests {

    test("URITests") {

      test("relativeFileURI") {
        val relativeToCWD = HeadlessWebKit.file("./webkit/src/webkit/test.html")
        assert(relativeToCWD == s"""file://$cwd/./webkit/src/webkit/test.html""")
      }

      test("absoluteFileURI") {
        val absoluteFile = HeadlessWebKit.file("/home/hmf/IdeaProjects/srllab/webkit/src/webkit/test.html")
        assert(absoluteFile == """file:///home/hmf/IdeaProjects/srllab/webkit/src/webkit/test.html""")
      }

      test("absoluteHTTPURI") {
        val absoluteFile = HeadlessWebKit.http("www.google.com/")
        assert(absoluteFile == """http://www.google.com/""")
      }

      test("absoluteHTTSPURI") {
        val absoluteFile = HeadlessWebKit.https("www.google.com/")
        assert(absoluteFile == """https://www.google.com/""")
      }

    }

    test("ImageCapture SVG Color") {

      val color = Mocks.SVGcolor.files

      test("PNG Color") {
        ImageTestUtils.captureImage(htmlFile, pngFileTmp, goldPng)
      }

      test("GIF Color") {
        ImageTestUtils.captureImage(htmlFile, gifFileTmp, goldGif)
      }

      test("TIFF Color") {
        ImageTestUtils.captureImage(htmlFile, tifFileTmp, goldTif)
      }

      // Not supported
      test("JPEG Color") {
        ImageTestUtils.captureImage(htmlFile, jpegFileTmp, goldJpeg, false)
      }

      // Not supported
      test("BMP Color") {
        ImageTestUtils.captureImage(htmlFile, bmpFileTmp, goldBmp, false)
      }

      // Not supported
      test("WBMP Color") {
        ImageTestUtils.captureImage(htmlFile, wbmpFileTmp, goldWbmp, false)
      }
    }

    test("ImageCapture SVG B&W") {

      val bw = Mocks.SVG_BW.files

      test("PNG B&W") {
        ImageTestUtils.captureImage(htmlFile, pngFileTmp, goldPng)
      }

      test("GIF B&W") {
        ImageTestUtils.captureImage(htmlFile, gifFileTmp, goldGif)
      }

      test("TIFF B&W") {
        ImageTestUtils.captureImage(htmlFile, tifFileTmp, goldTif)
      }
    }

    test("ImageCapture SVG Plot") {

      val plot = Mocks.SVG_Plot.files

      test("PNG B&W") {
        ImageTestUtils.captureImage(htmlFile, pngFileTmp, goldPng)
      }

      test("GIF B&W") {
        ImageTestUtils.captureImage(htmlFile, gifFileTmp, goldGif)
      }

      test("TIFF B&W") {
        ImageTestUtils.captureImage(htmlFile, tifFileTmp, goldTif)
      }
    }

  }
*/
}
