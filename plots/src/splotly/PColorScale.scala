package splotly

import ujson.Value

// TODO: override toString to generate an object with methods?
case class PColorScale(dflt: Option[Array[(Double, String)]],
                       description: Option[String]
                ) extends JSON {

  private var value: Option[(Double, String)] = None

  def setValue(d: Double, s: String): Unit = value = Some((d, s))
  def setDefault(i: Int): Unit = value = dflt.map(arr => arr(i))
  def getValue: Option[(Double, String)] = value

  override def json(): String = value.fold(""){e =>
    val num = if (e._1.isValidInt) e._1.toInt.toString else e._1.toString
    s"""[$num, "${e._2}"]"""
  }

  override def fromJson(in: Value): PColorScale.this.type = {
    in match {
      case ujson.Arr(v) =>
        if (v.length <= 1 || v.length > 2)
          throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
        else {
          val scaleValue = v(0)
          val colorValue = v(1)
          (scaleValue, colorValue) match {
            case (ujson.Num(scale), ujson.Str(color)) => setValue(scale, color)
            case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected array values $in")
          }
        }
      case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
    }
    this
  }
}
