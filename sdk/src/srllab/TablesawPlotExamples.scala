package srllab

import tech.tablesaw.api.{ColumnType, IntColumn, NumberColumn, NumericColumn, Table}
import tech.tablesaw.columns.numbers.IntColumnType
import tech.tablesaw.selection.Selection
import tech.tablesaw.table.TableSliceGroup
import tech.tablesaw.aggregate.AggregateFunctions.mean
import tech.tablesaw.aggregate.AggregateFunctions.sum
import tech.tablesaw.plotly.Plot
import tech.tablesaw.plotly.components.Figure
import tech.tablesaw.plotly.components.Layout
import tech.tablesaw.plotly.components.Marker
import tech.tablesaw.plotly.api.{AreaPlot, BoxPlot, BubblePlot, CandlestickPlot, Histogram, Histogram2D, HorizontalBarPlot, LinePlot, OHLCPlot, PiePlot, QQPlot, Scatter3DPlot, ScatterPlot, TimeSeriesPlot, VerticalBarPlot}
import tech.tablesaw.plotly.traces.ScatterTrace
import tech.tablesaw.plotly.traces.BarTrace
import tech.tablesaw.plotly.traces.HistogramTrace
import tech.tablesaw.plotly.traces.BoxTrace
import java.io.{File, IOException}
import java.nio.file.{Files, Path, Paths}
import java.util.UUID

import org.apache.commons.math3.distribution.NormalDistribution


/**
 * mill mill.scalalib.GenIdea/idea
 *
 * mill sdk.compile
 * mill sdk.run
 * mill sdk.{compile, run}
 * mill --watch sdk.run
 *
 * mill -i sdk.console
 * mill -i sdk.repl
 *
 * mill -i sdk.runMain srllab.TablesawPlotExamples
 * mill --watch sdk.runMain srllab.TablesawPlotExamples
 *
 *
*/
object TablesawPlotExamples {

  def randomizedFileName(name: String = "output"): String = name + UUID.randomUUID.toString + ".html"

  def randomFile(dir: String = "" ): File = {
    val property = "java.io.tmpdir"
    val tempDir = if (dir == "") System.getProperty(property) else dir

    val path = Paths.get(tempDir, randomizedFileName())
    try Files.createDirectories(path.getParent)
    catch {
      case e: IOException =>
        e.printStackTrace()
    }
    path.toFile
  }

  def show(figure: Figure, outputFile: File = randomFile()): Unit = {
    Plot.show(figure, outputFile)
  }

  def bubblePlot(): Unit = {
    val marketShare: Table = Table.read().csv("./data/tablesaw/market_share.csv")
    val sub: Table = marketShare.where(Selection.withRange(0, 4))
    val x: NumericColumn[_] = sub.nCol("Products")
    val y: NumericColumn[_] = sub.nCol("Sales")
    val data: NumericColumn[_] = sub.nCol("Market_Share")

    val layout: Layout = Layout.builder().title("Market Share").build()
    val marker: Marker = Marker.builder()
      .size(data)
      .sizeMode(Marker.SizeMode.AREA)
      .build()
    val trace: ScatterTrace = ScatterTrace
      .builder(x, y)
      .marker(marker)
      .build()

    show(new Figure(layout, trace))
  }

  def bubblePlotJScript(): String = {
    val marketShare: Table = Table.read().csv("./data/tablesaw/market_share.csv")
    val sub: Table = marketShare.where(Selection.withRange(0, 4))
    val x: NumericColumn[_] = sub.nCol("Products")
    val y: NumericColumn[_] = sub.nCol("Sales")
    val data: NumericColumn[_] = sub.nCol("Market_Share")

    val layout: Layout = Layout.builder().title("Market Share").build()
    val marker: Marker = Marker.builder()
      .size(data)
      .sizeMode(Marker.SizeMode.AREA)
      .build()
    val trace: ScatterTrace = ScatterTrace
      .builder(x, y)
      .marker(marker)
      .build()

    val fig = new Figure(layout, trace)
    fig.asJavascript("bubblePlot")
  }

  def barPieAndParetoPlots(): Unit = {
    // Load the tornado data set into a table
    val tornadoes: Table = Table.read().csv("./data/tablesaw/tornadoes_1950-2014.csv")

    // Get the scale column and replace any values of -9 with the column's missing value indicator
    val scale: IntColumn = tornadoes.intColumn("scale")
    scale.set(scale.isEqualTo(-9), IntColumnType.missingValueIndicator())

    // Sum the number of fatalities from each tornado, grouping by scale
    val fatalities1: Table = tornadoes.summarize("fatalities", sum).by("scale")

    // Bar plots
    val bar = HorizontalBarPlot.create(
      "Fatalities by scale", // plot title
      fatalities1,           // table
      "scale",               // grouping column name
      "sum [fatalities]")    // numeric column name
    show( bar )

    // Plot the mean injuries rather than a sum.
    val injuries1: Table = tornadoes.summarize("injuries", mean).by("scale")

    val horizontal = VerticalBarPlot.create(
      "Average number of tornado injuries by scale",
      injuries1,
      "scale",
      "mean [injuries]")
    show( horizontal )


    // Pie plot
    val pie = PiePlot.create(
      "Fatalities by scale",
      fatalities1,
      "scale",
      "sum [fatalities]")
    show( pie )

    // Pareto plot
    val t2: Table = tornadoes.summarize("fatalities", sum).by("State")

    val t3 = t2.sortDescendingOn(t2.column(1).name())

    val layout: Layout = Layout.builder().title("Tornado Fatalities by State").build()
    val trace: BarTrace = BarTrace.builder(
      t3.categoricalColumn(0),
      t3.numberColumn(1))
      .build()
    show(new Figure(layout, trace))


  }

  def histogramAndBoxPlots(): Unit = {
    val property: Table = Table.read().csv("./data/tablesaw/sacramento_real_estate_transactions.csv")
    val histogram1 = Histogram.create("Distribution of prices", property, "price")
    show( histogram1 )

    // Make sure you use the correct type column
    // If you copy the columns using for example "NumericColumn("sq__ft").asDoubleColumn()"
    // then the table's "property.dropRowsWithMissingValues()" below will not work
    assert(property.column("sq__ft").`type` eq ColumnType.INTEGER)
    val sqftt: IntColumn = property.intColumn("sq__ft")
    sqftt.set(sqftt.isEqualTo(0), IntColumnType.missingValueIndicator())

    //val property1: Table = property.where(property.NumericColumn("sq__ft").isNotEqualTo(0.0))
    assert(property.rowCount == 985)
    val property1 = property.dropRowsWithMissingValues()
    assert(property1.rowCount == 814)
    val histogram2 = Histogram.create("Distribution of property sizes", property1, "sq__ft")
    show( histogram2 )

    val layout1: Layout = Layout.builder().title("Distribution of property sizes (Trace)").build()
    val trace1: HistogramTrace = HistogramTrace.builder(property.nCol("sq__ft")).build()
    val fig = new Figure(layout1, trace1)
    show( fig )

    val histogram2D = Histogram2D.create("Distribution of price and size", property, "price", "sq__ft")
    show( histogram2D )

    val box1 = BoxPlot.create("Prices by property type", property, "type", "price")
    show( box1 )

    val layout2: Layout = Layout.builder().title("Prices by property type (Trace)").build()

    val trace2:BoxTrace = BoxTrace.builder(
      property.categoricalColumn("type"),
      property.nCol("price"))
      .build()

    show(new Figure(layout2, trace2))

  }

  def scatterPlots2D3D() : Unit = {

    val wines = Table.read.csv("./data/tablesaw/test_wines.csv")
    wines.column("year").setName("vintage")

    // Two variables
    val champagne = wines.where(
                          wines.stringColumn("wine type").isEqualTo("Champagne & Sparkling")
                            .and(wines.stringColumn("region").isEqualTo("California")))
    show(ScatterPlot.create("Champagne prices by vintage", champagne, "mean retail", "vintage"))

    // Three variables
    show(
      ScatterPlot.create("Wine prices and ratings",
        wines, "Mean Retail", "highest pro score", "wine type"))

    show(
      BubblePlot.create("Average retail price for champagnes by vintage and rating",
        champagne,				                         // table
        "highest pro score",  	             // x
        "vintage",  				                   // y
        "Mean Retail"))  		             // bubble size

    // 3D
    show(
      Scatter3DPlot.create("Average retail price for champagnes by vintage and rating",
        champagne,				                        // table
        "highest pro score",  	            // x
        "vintage", 				                  // y
        "Mean Retail"))  		                // z

    // Four variables
    show(
      BubblePlot.create("Average retail price for champagnes by vintage and rating",
        champagne,
        "highest pro score",
        "vintage",
        "Mean Retail",
        "appellation"))

    show(
      Scatter3DPlot.create("Average retail price for champagnes by vintage and rating",
        champagne,
        "highest pro score",
        "vintage",
        "Mean Retail",
        "appellation"))

    // Five variables
    show(
      Scatter3DPlot.create("High & low retail price for champagne by vintage and rating",
        champagne,
        "vintage",
        "highest pro score",
        "highest retail",
        "lowest retail",
        "appellation"))

  }

  def lineAndCandleStickPlots(): Unit = {
    val bush = Table.read().csv("./data/tablesaw/bush.csv")
    val foxOnly = bush.where(bush.stringColumn("who").equalsIgnoreCase("fox"))

    show(
      TimeSeriesPlot.create("Fox approval ratings",
        foxOnly,
        "date",
        "approval"))

    show(
      TimeSeriesPlot.create("George W. Bush approval",
        bush,
        "date",
        "approval",
        "who"))

    val robberies = Table.read.csv("./data/tablesaw/boston-robberies.csv")

    show(
      LinePlot.create("Monthly Boston Robberies: Jan 1966-Oct 1975",
        robberies,
        "Record",
        "Robberies"))

    show(
      AreaPlot.create("Monthly Boston Robberies: Jan 1966-Oct 1975",
        robberies,
        "Record",
        "Robberies"))

    val priceTable = Table.read.csv("./data/tablesaw/ohlcdata.csv")

    show(OHLCPlot.create("Prices", 	// The plot title
      priceTable,                         // the table we loaded earlier
      "date",	                      // our time variable
      "open",                     // the price data...
      "high",
      "low",
      "close"))

    show(CandlestickPlot.create("Prices",
      priceTable,
      "date",
      "open",
      "high",
      "low",
      "close"))
  }

  // TODO: add more examples
  // ParetoPlot
  // QQPlot
  // QuantilePlot
  // TukeyMeanDifferencePlot
  // BarPlot
  // BarTrace
  //	BoxTrace
  // 	HeatmapTrace
  //	Histogram2DTrace
  //	HistogramTrace
  //	PieTrace
  //	Scatter3DTrace
  //	ScatterTrace

  def QQPlots(): Unit = {
    val baseball = Table.read.csv("./data/tablesaw/baseball.csv")
    show(
      QQPlot.create("batting averages and On-base percent",
        baseball,
        "BA",
        "SLG"))

    // example with different sized arrays;// example with different sized arrays;

    val first = new NormalDistribution().sample(100)
    val second = new NormalDistribution().sample(200)
    show(
      QQPlot.create("Test of different sized arrays",
        "short array",
        first,
        "long array",
        second))

    // example with different sized arrays;
    val first1 = new NormalDistribution().sample(20000);
    val second1 = new NormalDistribution().sample(19990);
    show(
      QQPlot.create(
        "Test of different sized arrays",
        "long array",
        first1,
        "short array",
        second1));
  }

  def histogramOverlay(): Unit = {
    val baseball: Table = Table.read().csv("./data/tablesaw/baseball.csv")

    val layout: Layout = Layout.builder()
      .title("Distribution of team batting averages")
      .barMode(Layout.BarMode.OVERLAY)
      .showLegend(true)
      .build()

    val groups: TableSliceGroup = baseball.splitOn("league")
    val t1: Table = groups.get(0).asTable()

    val trace1: HistogramTrace = HistogramTrace
      .builder(t1.nCol("BA"))
      .name("American Leage")
      .opacity(.75)
      .nBinsX(24)
      .marker(Marker.builder().color("#FF4136").build())
      .build()

    val t2: Table = groups.get(1).asTable()
    val trace2: HistogramTrace = HistogramTrace
      .builder(t2.nCol("BA"))
      .name("National League")
      .opacity(.75)
      .nBinsX(24)
      .marker(Marker.builder().color("#7FDBFF").build())
      .build()

    val fig = new Figure(layout, trace1, trace2)
    show(fig)

  }

  def main(args: Array[String]): Unit = {
    println(System.getProperty("user.dir"))

    //bubblePlot()
    //barPieAndParetoPlots()
    //histogramAndBoxPlots()
    //scatterPlots2D3D()
    //lineAndCandleStickPlots()
    QQPlots()
    //histogramOverlay()
  }

}
