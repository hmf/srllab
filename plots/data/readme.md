# Plotly Data Sets

* Data sets found [here](https://github.com/plotly/datasets)
* Documentaton found [here](https://plotly.github.io/datasets/)
 
## Examples Data sets

* **Violin**: https://github.com/plotly/datasets/blob/master/violin_data.csv
