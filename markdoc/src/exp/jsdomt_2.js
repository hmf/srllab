(function(f){
  if (typeof exports==="object"&&typeof module!=="undefined"){
    console.log("module.exports=f()")
    module.exports=f()
  }
  else if (typeof define==="function" && define.amd){
    console.log("define([],f)")
    define([],f)
  }
  else {
    var g;
    if (typeof window!=="undefined"){
      console.log("g = window = "+window)
      g=window
    } else if (typeof global!=="undefined"){
      console.log("g = global = "+global)
      g=global
    } else if (typeof self!=="undefined"){
      console.log("g = self = "+self)
      g=self
    } else {
      console.log("g = this = "+this)
      g=this
    }
    //console.log("g.jsdom = f()")
    //console.log(f)
    g.jsdom = f()
  }
  })
  (function(){
    var define,module,exports;
    return (function(){
              function r(e,n,t){
                function o(i,f){
                  if (!n[i]) {
                    if(!e[i]) {
                       var c="function"==typeof require&&require;
                       if (!f&&c) {
                         console.log("c = "+c)
                         return c(i,!0);
                       }
                       if(u) {
                         console.log("u = "+u)
                         return u(i,!0);
                       }
                       var a = new Error("Cannot find module '"+i+"'");
                       throw a.code="MODULE_NOT_FOUND",a
                    }
                    var p=n[i]={exports:{}};
                    //console.log("n[i] = "+n[i])
                    //console.log("e[i][0] = "+e[i][0])
                    e[i][0].call( p.exports,
                                 function(r){
                                   var n=e[i][1][r];
                                   return o(n||r)
                                 }, p, p.exports,r,e,n,t)
                 }
                 //console.log("n[i].exports = " + n[i].exports)
                 return n[i].exports
              }
              for(var u="function"==typeof require&&require,i=0; i<t.length; i++) {
                console.log("o(t[i]) = " + o(t[i]))
                o(t[i]);
              }
             return o
           }
           //console.log("r = " + r)
           return r
         }
       )()(
           {
            "jsdom":[
              function(require,module,exports){
                 (
                  function (String){
                     "use strict";

                     class JSDOM {
                       constructor(input, options = {}) {}
                     }

                     exports.JSDOM = JSDOM;
                  }
                ).call(this,"buffer")
              },
              {} ]

          },{},[]
         )("jsdom")
  }
 );
