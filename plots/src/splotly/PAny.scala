package splotly

import ujson.Value

case class PAny(dflt: Option[String],
                description: Option[String]
                ) extends JSON {

  private var value: OfPrimitive = NAPrimitive

  def setValue(): Unit = {
    value = NAPrimitive
  }

  def setValue(v: Boolean): Unit = {
    value = TBoolean(v)
  }

  def setValue(v: Int): Unit = {
    value = TInt(v)
  }

  def setValue(v: Double): Unit = {
    value = TDouble(v)
  }

  def setValue(v: String): Unit = {
    value = TString(v)
  }

  def setDefault(): Unit = {
    value = dflt.fold(NAPrimitive: OfPrimitive)(e => TString(e))
  }
  def getValue: OfPrimitive = value

  override def json(): String = {
    value match {
      case NAPrimitive => ""
      case TDouble(v) => v.toString
      case TInt(v) => v.toString
      case TBoolean(v) => v.toString
      case TString(v) => s""""${v.toString}""""
      case _ => throw new RuntimeException(s"Unexpected type: $value")
    }
  }

  override def fromJson(in: Value): PAny.this.type = {
    in match {
      case ujson.Bool(v) => setValue(v)
      case ujson.Num(v) => if (v.isValidInt) setValue(v.toInt) else setValue(v)
      case ujson.Str(v) => setValue(v)
      case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
    }
    this
  }
}
