package splotly

import ujson.Value

// TODO: override toString to generate an object with methods?
case class PColorList(dflt: Option[Array[String]],
                      description: Option[String]
                ) extends JSON {

  private var value: Option[String] = None

  def setValue(d: String): Unit = value = Some(d)
  def setDefault(i:Int): Unit = value = dflt.map( v => v(i))
  def getValue: Option[String] = value

  override def json(): String = value.fold("")(e => s""""${e.toString}"""")

  override def fromJson(in: Value): PColorList.this.type = {
    in match {
      case ujson.Str(v) => setValue(v)
      case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
    }
    this
  }
}
