package splotly

import os.Path
import ujson.{Arr, Bool, Null, Num, Obj, Str, Value}

import scala.collection.immutable
import scala.collection.immutable.TreeSet
import scala.util.matching.Regex

/**
 * type            --> top case class (ex. Violin)
 * valType         --> primitive type (number, arrays, enums, etc.)
 * role = "object"
 *   no type
 *   no valType    --> visible member object (Violin.box, Violin.meanline, Violin.line)
 * no role +
 *   no type
 *   no valType    --> hidden member object (Violin.attributes, Violin.layoutAttributes)
 */
object Generator {

  def checkError[T](values: immutable.Iterable[Either[Error, T]], default: T)
  : Either[Error, Iterable[T]] = {
    val (left, right) = values.partition( v => v.isLeft )
    if (left.nonEmpty) {
      val tmp = left.map({ b =>
        b.swap.getOrElse(Error(s"Unknown error")).errs
      })
      val errs = tmp.flatten.toList
      Left(Error(errs))
    } else {
      val oks = right.map({ b =>
        if (b.isRight)
          b.getOrElse(default)
        else
          throw new RuntimeException(s"Unexpected Left : $b")
      })
      Right(oks)
    }
  }

  val reserved: Set[String] = TreeSet(
    "abstract",
    "case",
    "catch",
    "class",
    "def",
    "do",
    "else",
    "extends",
    "false",
    "final",
    "finally",
    "for",
    "forSome",
    "if",
    "implicit",
    "import",
    "lazy",
    "match",
    "new",
    "null",
    "object",
    "override",
    "package",
    "private",
    "protected",
    "return",
    "sealed",
    "super",
    "this",
    "throw",
    "trait",
    "true",
    "try",
    "type",
    "val",
    "var",
    "while",
    "with",
    "yield"
  )

  val def_json_String = "def json(): String"

  def changeReservedNames(name: String): String = {
    if (reserved.contains(name)) name + "_" else name
  }

  def doubleQuotes(s: String): String = """"\"""" + s + """\"""""
  def addQuotes(s: String): String = """"""""" + s + """""""""
  def addQuotes(s: Option[String]): String = s.fold("None")(e => s"Some(${addQuotes(e)})")
  def capitalize(s: String): String = s.substring(0, 1).toUpperCase + s.substring(1)

  def encodeValueType(current: SchemaPath, typedValue: OfType, trace:List[String]): Either[Error, String] = {
    val t = "encodeValueType" :: trace
    typedValue match {
      case TBoolean(v) => Right(s""": Boolean = $v""")
      case TDouble(v) => Right(s""": Double = $v""")
      case TInt(v) => Right(s""": Int = $v""")
      case TString(v) => Right(s""": String = \"\"\"$v\"\"\" """)
      case TBooleanArray(v) => Right( s""": Array[Boolean] = Array(${v.mkString(",")}) """)
      case TDoubleArray(v) => Right( s""": Array[Double] = Array(${v.mkString(",")}) """)
      case TIntArray(v) => Right( s""": Array[Int] = Array(${v.mkString(",")}) """)
      case TStringArray(v) => Right( s""": Array[String] = Array(${v.map(addQuotes).mkString(",")}) """)
      case TPrimtivesArray(v) =>
        val t1 = "TPrimtivesArray(v)" :: t
        Right( s""": Array[Any] = Array(${v.map(e => encodeValueType(current, e, t1)).mkString(",")}) """)
      case NA => Right("") // Left(Error(List("Encoded type is NA")))
        /* TODO: BUG
        Error in orientation.dflt @ attributes<-violin<-traces<-schema<-*;
        Encoded type is NA
         */
      case TArrayAttrRegexpsArray(v) =>
        val tmp: Array[String] = v.map(e => addQuotes(e.t))
        Right(s""": Array[String] = Array(${tmp.mkString(",")}) """)

      case e: OfArray => Left(Error(s"""encodeValueType Unknown type (1): $e @ $current \n ${t.mkString(",\n")}"""))
      case e: OfPrimitive => Left(Error(s"""encodeValueType Unknown type (2): $e @ $current \n ${t.mkString(",\n")}"""))
    }
  }

  case class DeclarationID(valid: String, original: String)

  /**
   * Declaration of an object/class
   * @param members - members of the class/object
   * @param code - methods of the class/object
   */
  case class CodeSnippet(members: List[DeclarationID], code: String)

  private val defaultID = DeclarationID("","")

  def generateValueDeclaration(in: ValueDefinition, trace:List[String]): Either[Error, CodeSnippet] = {
    val t = "generateValueDeclaration" :: trace
    encodeValueType(from(in.name, in.parent), in.member, t).map({ value =>
      val id = changeReservedNames(in.name)
      val out = s"$id ${value}"
      val code = "val " + out
      CodeSnippet(List(DeclarationID(id, in.name)), code)
    })
  }

  def generateNumberDeclaration(in: PrimitiveNumber, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateNumberDeclaration(in: PrimitiveDefinition)" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      min_ <- in.min
      max_ <- in.max
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      // @see https://github.com/plotly/plotly.js/issues/4625
      val default = addQuotes(dflt_)
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: PReal = PReal(min = $min_,
           |                      max = $max_,
           |                      dflt = $default,
           |                      description = ${addQuotes(description_)}
           |                     )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name, in.name)), code)
    }
  }

  def generateIntDeclaration(in: PrimitiveInt, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateIntDeclaration(in: PrimitiveDefinition)" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      min_ <- in.min
      max_ <- in.max
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: PInt = PInt(min = $min_,
           |                      max = $max_,
           |                      dflt = $dflt_,
           |                      description = ${addQuotes(description_)}
           |                     )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def generateBooleanDeclaration(in: PrimitiveBoolean, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateBooleanDeclaration(in: PrimitiveDefinition)" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: PBool = PBool(
           |                      dflt = $dflt_,
           |                      description = ${addQuotes(description_)}
           |                     )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def generateStringDeclaration(in: PrimitiveString, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateStringDeclaration(in: PrimitiveDefinition)" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: PChars = PChars(
           |                      dflt = ${addQuotes(dflt_)},
           |                      description = ${addQuotes(description_)}
           |                     )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def generateColorDeclaration(in: PrimitiveColor, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateColor(in: PrimitiveDefinition)" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: PColor = PColor(
           |                      dflt = ${addQuotes(dflt_)},
           |                      description = ${addQuotes(description_)}
           |                     )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def generateAnyDeclaration(in: PrimitiveAny, trace: List[String]): Either[Error, CodeSnippet] = {
    // Get the optional fields, if they have a value use them else set to None
    for {
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val t = "generateAny(in: PrimitiveDefinition)" :: trace
      val dfltString = dflt_.map(e => addQuotes(e.toString))
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: PAny = PAny(
           |                      dflt = $dfltString,
           |                      description = ${addQuotes(description_)}
           |                     )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def generateSubPlotIDDeclaration(in: PrimitiveSubPlotId, trace:List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateSubPlotIDDeclaration(in: PrimitiveDefinition" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: SubPlotID = SubPlotID(
           |                      dflt = ${addQuotes(dflt_)},
           |                      description = ${addQuotes(description_)}
           |                     )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def declareOnOffMethod(name: String): String = {
    val defName = mkDefName(name)
    s"""def ${defName}On(): Unit = { flag_ + "$name" }
       |def ${defName}Off(): Unit = { flag_ - "$name" }""".stripMargin
  }

  def generateFlagListFromJson(trace: List[String]): String = {
    val t = "generateFlagListFromJson()" :: trace
    s"""
       |override def fromJson(in: Value): this.type = {
       | ${commentCallTrace(t)}
       |  def err(j: ujson.Value): OfPrimitive =
       |    throw new RuntimeException(s\"\"\"Parsing of $$j failed: $$in\"\"\")
       |  in match {
       |    case Str(value) =>
       |      flag_ += value
       |      this
       |    case Bool(value) =>
       |      flag_ += value.toString
       |      this
       |    case _ =>
       |      err(in)
       |      this
       |  }
       |}
       |""".stripMargin
  }

  def generateFlagListDeclaration(in: PrimitiveFlagList, trace: List[String]):
  Either[Error, CodeSnippet] = {
    // Get the optional fields, if they have a value use them else set to None
    val flagsMethods = in.flagsArr.map { n =>
      n.map(_.map(declareOnOffMethod).mkString("\n"))
       .fold("")(e => e)
    }
    val extrasMethods = in.extrasArr.map { n =>
      n.map(_.map(declareOnOffMethod).mkString("\n"))
        .fold("")(e => e)
    }
    for {
      dflt_ <- in.dflt
      description_ <- in.description
      flagsMethods_ <- flagsMethods
      extrasMethods_ <- extrasMethods
    } yield {
      val name = changeReservedNames(in.name)
      val t = "generateFlagListDeclaration(in: PrimitiveDefinition)" :: trace
      val code =
        s"""
           |object $name extends JSON {
           | ${commentCallTrace(t)}
           | val dflt: Option[String] = ${addQuotes(dflt_)}
           | val description: Option[String] = ${addQuotes(description_)}
           |
           | private val flag_ : mutable.Set[String] = mutable.Set()
           | //dflt.map(e => flag_ + e)
           | def flag: String = flag_.mkString("+")
           | $def_json_String = {
           |    if (flag.contains("true"))
           |      "true"
           |    else if (flag.contains("false"))
           |      "false"
           |    else if (flag.length > 0)
           |      "\\"" + flag + "\\""
           |    else
           |      ""
           |  }
           | ${generateFlagListFromJson(t)}
           |
           | $flagsMethods_
           | $extrasMethods_
           |}
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def generateAngleDeclaration(in: PrimitiveAngle, trace:List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateAngleDeclaration(in: PrimitiveDefinition" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val default = addQuotes(dflt_)
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: PAngle = PAngle( dflt = $default,
           |                            description = ${addQuotes(description_)}
           |                           )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def generateColorScaleDeclaration(in: PrimitiveColorScale, trace:List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateColorScaleDeclaration(in: PrimitiveDefinition" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val dflts = dflt_.fold("None"){arr =>
        val data = arr.map(e => (e._1, addQuotes(e._2))).mkString(",")
        s"Some(Array($data))"
      }
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: PColorScale = PColorScale( dflt = $dflts,
           |                            description = ${addQuotes(description_)}
           |                           )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def generateColorListDeclaration(in: PrimitiveColorList, trace:List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateColorListDeclaration(in: PrimitiveDefinition" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val default = dflt_.fold("None"){a =>
        if (a.isEmpty)
          "None"
        else {
          val strs = a.map(addQuotes)
          strs.mkString("Some(Array(", ",", "))")
        }
      }
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: PColorList = PColorList( dflt = $default,
           |                            description = ${addQuotes(description_)}
           |                           )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  //val decimal = "/^\\d*\\.?\\d*$/".r
  val decimal = "^[+-]?((\\d+(\\.\\d*)?)|(\\.\\d+))$"
  def isNumeric(s: String): Boolean =  s.matches(decimal)

  // Constraint operations of contour.attributes.contours.operation
  val constraintOps = List(")[", "](", ")(", "][", "(]", "[)", "()", "[]", "=",
                           "}{", "{}", ")[", "}{"
                           // "<=", ">", ">=", "<"
                           // "\\",  "/", "|"
    )

  def quoteOps(s: String): String = {
    //if (constraintOps.contains(s)) "`" + s + "`" else s
    val tmp = if (constraintOps.contains(s)) "`" + s + "`" else s
    if (tmp.contains("+")) "`" + tmp + "`" else tmp
  }

  def mkDefName(s: String): String = {
    // check for numeric values
    val defName = if (isNumeric(s)) {
      val value = s.toDouble
      val h = if (s.contains(".") ) "D" else "I"
      val s1 = s.replace(".", "_")
      val s2 = h + s1
      if (value >= 0) s2 else s"`$s2`"
    } else {
      // make sure Scala reserved words are not used
      //val s1 = s.substring(0, 1).toUpperCase() + s.substring(1)
      val s1 = changeReservedNames(s)
      // replace all "-" to "_"
      val s2 = s1.replace("-", "_")
      // Just one `-`, keep it, its valid
      val s3 = if (s2 == "_") "-" else s2
      val s4 = s3.replace(" ", "_")
      val s5 = s4.replace("\\", "\\\\")
      s5
    }
    if (defName != "")
      quoteOps(defName)
    else
      "default"
  }


  def enumTypeVar(enumType:OfPrimitive): String = {
    enumType match {
      case NAPrimitive => ""
      case TBoolean(_) => "boolean"
      case TDouble(_) => "double"
      case TInt(_) => "int"
      case TString(_) => "string"
      case TImpliedEdits(_) => "impliedEdits"
      case TAttrRegexps(_) => "attrRegexps"
    }
  }

  def enumType(enumType:OfPrimitive): String = {
    enumType match {
      case NAPrimitive => ""
      case TBoolean(_) => "Boolean"
      case TDouble(_) => "Double"
      case TInt(_) => "Int"
      case TString(_) => "String"
      case TImpliedEdits(_) => "TImpliedEdits"
      case TAttrRegexps(_) => "TAttrRegexps"
    }
  }

  def enumTypeDefault(enumType:OfPrimitive): String = {
    enumType match {
      case NAPrimitive => ""
      case TBoolean(_) => OfType.CBoolean.t.toString
      case TDouble(_) => OfType.CDouble.t.toString
      case TInt(_) => OfType.CInt.t.toString
      case TString(_) => OfType.CString.t
      case TImpliedEdits(_) => OfType.CImpliedEdits.t.mkString(",")
      case TAttrRegexps(_) => OfType.CAttrRegexps.t
    }
  }

  def enumTypeName(enumType:OfPrimitive): String = {
    enumType match {
      case NAPrimitive => ""
      case TBoolean(t) => t.toString
      case TDouble(t) => t.toString
      case TInt(t) => t.toString
      case TString(t) => t
      case TImpliedEdits(t) => t.mkString(",")
      case TAttrRegexps(t) => t
    }
  }

  def enumOfPrimitiveValue(enumType:OfPrimitive): String = {
    enumType match {
      case NAPrimitive => ""
      case TBoolean(t) => s"TBoolean($t)"
      case TDouble(t) => s"TDouble($t)"
      case TInt(t) => s"TInt($t)"
      case TString(t) =>
        val tmp = t.replace("\\", "\\\\")
        s"""TString("$tmp")"""
      case TImpliedEdits(t) => s"""TImpliedEdits($t)"""
      case TAttrRegexps(t) => s"""TAttrRegexps($t)"""
    }
  }

  def enumTypeValueConstant(enumType:OfPrimitive): OfPrimitive = {
    enumType match {
      case NAPrimitive => OfType.CNAPrimitive
      case TBoolean(_) => OfType.CBoolean
      case TDouble(_) => OfType.CDouble
      case TInt(_) => OfType.CInt
      case TString(_) => OfType.CString
      case TImpliedEdits(_) => OfType.CImpliedEdits
      case TAttrRegexps(_) => OfType.CAttrRegexps
    }
  }

  def enumOfTypeValue(addVar: String, enumType:OfPrimitive): String = {
    enumType match {
      case NAPrimitive => "NAPrimitive"
      case TBoolean(_) => s"TBoolean($addVar)"
      case TDouble(_) => s"TDouble($addVar)"
      case TInt(_) => s"TInt($addVar)"
      case TString(_) =>
        val tmp = addVar.replace("\\", "\\\\")
        s"TString($tmp)"
      case TImpliedEdits(_) => s"TImpliedEdits($addVar)"
      case TAttrRegexps(_) => s"TAttrRegexps($addVar)"
    }
  }

  def enumOfTypeClass(addVar: String, enumType:OfPrimitive): String = {
    enumType match {
      case NAPrimitive => s"NAPrimitive"
      case TBoolean(_) => s"$addVar:TBoolean"
      case TDouble(_) => s"$addVar:TDouble"
      case TInt(_) => s"$addVar:TInt"
      case TString(_) => s"$addVar:TString"
      case TImpliedEdits(_) => s"$addVar:TImpliedEdits"
      case TAttrRegexps(_) => s"$addVar:TAttrRegexps"
    }
  }

  def enumToCase(exists: Boolean, value:OfPrimitive): String = {
    def err(t:OfPrimitive) = s"""throw new RuntimeException(\"\"\"Unexpected OfPrimitive type: $t\"\"\")"""
    val s1 = value match {
      case NAPrimitive       => "\"\""
      case TBoolean(_)       => s"""${if (exists) s"t.toString" else s"${err(value)}"}"""
      case TDouble(_)        => s"""${if (exists) s"t.toString" else s"${err(value)}"}"""
      case TInt(_)           => s"""${if (exists) s"t.toString" else s"${err(value)}"}"""
      case TString(_)        => s"""${if (exists) s""" "\\"" + t.toString + "\\"" """ else s"${err(value)}"}"""
      case TImpliedEdits(_)  => s"""${if (exists) s""" "\\"" + t.toString + "\\"" """ else s"${err(value)}"}"""
      case TAttrRegexps(_)   => s"""${if (exists) s""" "\\"" + t.toString + "\\"" """ else s"${err(value)}"}"""
    }
    s"case ${enumOfTypeValue("t", value)} => $s1"
  }

  def declareEnumerateToJsonMethod(usedMethods: List[OfPrimitive], trace: List[String]):
  String = {
    // Add case checks for those enums
    val cases = OfType.allPrimitives.map( t => enumToCase(usedMethods.contains(t), t) )
    // each case generates the json string according to the enum type
    val options = cases.mkString("\n")
    val t = "declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])" :: trace
    s"""
       |$def_json_String = {
       |  ${commentCallTrace(t)}
       |  selected match {
       |    $options
       |  }
       |}
       |""".stripMargin
  }

  def enumOfTypeJsonX(addVar: String, jsonType:Value): String = {
    jsonType match {
      case ujson.Null => "Null"
      case ujson.Obj(_) => s"Obj($addVar)"
      case ujson.Arr(_) => s"Arr($addVar)"
      case ujson.Bool(_) => s"Bool($addVar)"
      case ujson.Str(_) => s"Str($addVar)"
      case ujson.Num(_) => s"Num($addVar)"
    }
  }

  def enumFromCaseX(value: Value, usedMethods: List[OfPrimitive]): String = {
    val s1 = value match {
      case Bool(_) =>
        val exists = usedMethods.contains(OfType.CBoolean)
        if (exists) s"TBoolean(s)" else s"err(in)"
      case Str(_) =>
        val exists = usedMethods.contains(OfType.CString)
        if (exists) s"TString(s)" else s"err(in)"
      case Num(_) =>
        val existsDouble = usedMethods.contains(OfType.CDouble)
        val existsInt = usedMethods.contains(OfType.CInt)
        if (existsDouble)
          s"TDouble(s)"
        else if (existsInt)
           s"if (s.isValidInt) TInt(s.toInt) else err(in)"
        else
          s"err(in)"
      case Obj(_) => s"err(in)"
      case Arr(_) => s"err(in)"
      case Null => s"err(in)"
    }
    s"case ${enumOfTypeJsonX("s", value)} => $s1"
  }

  def declareEnumerateFromJsonMethod(usedMethods: List[OfPrimitive], trace: List[String]):
  String = {
    // Add case checks for those enums
    val json = List[ujson.Value](Num(0), Bool(false), Str(""))
    val cases = json.map( t => enumFromCaseX(t, usedMethods) )
    val unexpected = "case _ => err(in)"
    // each case generates the json string according to the enum type
    val options = (unexpected :: cases).reverse.mkString("\n")
    val t = "declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])" :: trace
    s"""
       |def fromJson(in: Value): this.type = {
       |  ${commentCallTrace(t)}
       |  def err(j: ujson.Value): OfPrimitive = throw new RuntimeException(s\"\"\"Parsing of $$j failed: $$in\"\"\")
       |  val read = in match {
       |    $options
       |  }
       |  selected = read
       |  this
       |}
       |""".stripMargin
  }

  def setOfEnumTypes(values: Option[OfArray]): List[OfPrimitive] = {
    values match {
      case Some(TPrimtivesArray(t)) =>
        // Get he set of types used
        t.map( e => enumTypeValueConstant(e)).toSet.toList
      case Some(TBooleanArray(_)) => List(OfType.CBoolean)
      case Some(TDoubleArray(_)) => List(OfType.CDouble)
      case Some(TIntArray(_)) => List(OfType.CInt)
      case Some(TStringArray(_)) => List(OfType.CString)
      case Some(TArrayAttrRegexpsArray(_))  => List(OfType.CAttrRegexps)
      case None => List()
    }
  }

  def declareEnumMethod(enumValue:OfPrimitive): String = {
    val name = enumTypeName(enumValue)
    val flag = enumOfPrimitiveValue(enumValue)
    // /^x([2_9]|[1_9][0_9]+)?$/
    if (name.startsWith("/^") && name.endsWith("$/")){
      // If a method has a regular expression
      val defN = name.substring(2,3)
      val regexStr = enumValue match {
        case TString(e) => e
        case _ => throw new RuntimeException(s"declareEnumMethod : Expected a TString but got $enumValue")
      }
      val regexPrint = regexStr.replaceAllLiterally("$", "$$")
      // Create a method that check if the value is a match
      val defName = mkDefName(defN)
      s"""
         |def $defName($defName:String): Unit = {
         |  if ($defName.matches("$regexStr") || ($defName == ""))
         |    selected = TString($defName)
         |  else
         |    throw new RuntimeException(s"String did not match expected regexp: $regexPrint")
         |}
         |
         |""".stripMargin
    }
    else {
      // otherwise each value is set by a parameter-less enumeration method
      val defName = mkDefName(name)
      s"""
         |def $defName(): Unit = { selected = $flag }
         |
         |""".stripMargin
    }
  }

  def generateEnumSetMethods(values: Option[OfArray]): Array[String] = {
    values.fold(Array[String]()) {
      case TPrimtivesArray(t) => t.map(e => declareEnumMethod(e))
      case TBooleanArray(t) => t.map(e => declareEnumMethod(TBoolean(e)))
      case TDoubleArray(t) => t.map(e => declareEnumMethod(TDouble(e)))
      case TIntArray(t) => t.map(e => declareEnumMethod(TInt(e)))
      case TStringArray(t) => t.map(e => declareEnumMethod(TString(e)))
      case TArrayAttrRegexpsArray(t) => t.map(e => declareEnumMethod(e))
    }
  }

  def addSwitch(values: Option[OfArray], hasSwitch:Boolean): Option[OfArray] = {
    values.fold(None:Option[OfArray]) {
      case TPrimtivesArray(t) =>
        // Get the set of types used
        val t0: Set[OfPrimitive] = t.map(e => enumTypeValueConstant(e)).toSet
        // Add a boolean if it does not exist
        val t1 = if (!t0.contains(OfType.CBoolean)) t :+ OfType.CBoolean else t
        Some(TPrimtivesArray(t1))
      case e@TBooleanArray(_) => Some(e)
      case e@TDoubleArray(t) =>
        if (hasSwitch)
          Some(TPrimtivesArray(t.map(TDouble) :+ OfType.CDouble ))
        else
          Some(e)
      case e@TIntArray(t) =>
        if (hasSwitch)
          Some(TPrimtivesArray(t.map(TInt) :+ OfType.CInt ))
        else
          Some(e)
      case e@TStringArray(t) =>
        if (hasSwitch)
          Some(TPrimtivesArray(t.map(TString) :+ OfType.CBoolean ))
        else
          Some(e)
      case e@TArrayAttrRegexpsArray(_) => Some(e)
    }
  }

  def generateEnumeratedDeclaration( in: PrimitiveEnumerate, trace:List[String]):
  Either[Error, CodeSnippet] = {
    val t = "def generateEnumeratedDeclaration(in: PrimitiveDefinition)" :: trace
    val description = in.description
    for {
      dflt_ <- in.dflt
      v0 <- in.values
      description_ <- description
    } yield {
      val current = from(in.name, in.parent)
      val default = if (dflt_.isDefined) encodeValueType(current, dflt_.getOrElse(NAPrimitive), t) else Right("")
      val d = if (dflt_.isDefined) s"val dflt ${default.fold(e => e.errs.mkString(","), s => s)}" else ""
      val v = addSwitch(v0, in.hasSwitch)
      val methods_ = generateEnumSetMethods(v)
      val allMethods = methods_.mkString("\n")
      // Get the enum types used in methods
      val types = setOfEnumTypes(v)
      val toJson = declareEnumerateToJsonMethod(types, t)
      val fromJson = declareEnumerateFromJsonMethod(types, t)
      val name = changeReservedNames(in.name)
      val code =
        s"""
           |object $name extends JSON {
           |${commentCallTrace(t)}
           | $d
           | val description: Option[String] = ${addQuotes(description_)}
           |
           | private var selected: OfPrimitive = NAPrimitive
           | //allDeclarations
           |
           | $toJson
           | $fromJson
           |
           | $allMethods
           |}
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def declareEnumArrayMethod(i: Int, enumtype:OfPrimitive ): String = {
    val typeVar = enumTypeVar(enumtype)
    val name = typeVar.toString.replace("Opt", "")
    val defName = mkDefName(name)
    val typeDecl = enumType(enumtype)
    def fun(i: Int): String = {
      s"""
         |// Set the value
         |def ${defName}_$i(v:$typeDecl): Unit = { $typeVar($i) = v ; selected += ($i -> ${enumOfTypeValue("v",enumtype)}) }
         |// Delete the value (use default)
         |def ${defName}_$i(): Unit = { selected -= $i}
         |""".stripMargin
    }
    fun(i)
  }

  def declareSetArrayMethod(id: Int, enumtype: OfPrimitive):
  (Int, OfPrimitive, String) = {
    enumtype match {
      case t@NAPrimitive => (id, t, "")
      case t@TBoolean(_) => (id, t, declareEnumArrayMethod(id, t))
      case t@TDouble(_) => (id, t, declareEnumArrayMethod(id, t))
      case t@TInt(_) => (id, t, declareEnumArrayMethod(id, t))
      case t@TString(_) => (id, t, declareEnumArrayMethod(id, t))
      case t@TImpliedEdits(_) => (id, t, declareEnumArrayMethod(id, t))
      case t@TAttrRegexps(_) => (id, t, declareEnumArrayMethod(id, t))
    }
  }

  def generateArraySetMethods(valTypes: Map[Int, List[OfPrimitive]]):
  immutable.Iterable[(Int, OfPrimitive, String)] = {
    valTypes.flatMap { kv =>
      val k = kv._1
      val v = kv._2
      v.map( t => declareSetArrayMethod(k, t) )
    }
  }

  def enumArrayVar(enumtype:OfPrimitive, n:Int): String = {
    s"private var ${enumTypeVar(enumtype)} : Array[${enumType(enumtype)}] = Array.fill($n)(${enumTypeDefault(enumtype)})"
  }

  def generateArraySetVariables(usedMethods: Map[Int, List[OfPrimitive]]):
  List[String] = {

    // Max length of value array
    val nx = usedMethods.keys.max + 1
    // Types used, we need one variable for each typed value
    val types: Set[OfPrimitive] = usedMethods.values.flatten.toSet
    // Add variable declarations for those enums
    val tmp = types.map(e => enumArrayVar(e, nx)).toList
    // Add helper variables/values
    val selector = if (nx > 0) """private var selected: Map[Int, OfPrimitive] = Map()""" else ""
    val len = if (nx > 0) s"""private val n: Integer = $nx""" else ""
    val vars = tmp ++ List(selector, len)
    vars.filter(_.length > 0)
  }


  def enumArrayCase(exists: Boolean, enumType:OfPrimitive): String = {
    val throwStr = """throw new RuntimeException(s"Unexpected type: $t")"""
    val s1 = enumType match {
      case NAPrimitive        => throwStr
      case TBoolean(_)       => if (exists) "boolean(i).toString" else throwStr
      case TDouble(_)        => if (exists) "double(i).toString" else throwStr
      case TInt(_)           => if (exists) "int(i).toString" else throwStr
      case TString(_)        => if (exists) "string(i)" else throwStr
      case TImpliedEdits(_)  => if (exists) "impliedEdits(i)" else throwStr
      case TAttrRegexps(_)   => if (exists) "attrRegexps(i)" else throwStr
    }
    s"case ${enumOfTypeValue("_", enumType)} => $s1"
  }

  def generateSetEnum(enumtype: OfPrimitive):String = {
    val v = enumtype match {
      case _:TInt => "v.toInt"
      case _ => "v"
    }
    s"${enumTypeVar(enumtype)}(i) = $v ; selected += (i -> ${enumOfTypeValue(v,enumtype)})"
  }

  def generateInfoArrayFromJson(usedMethods: List[OfPrimitive], trace: List[String]):
  String = {
    val hasBool = usedMethods.contains(OfType.CBoolean)
    val hasStr = usedMethods.contains(OfType.CString)
    val hasDouble = usedMethods.contains(OfType.CDouble)
    val hasInt = usedMethods.contains(OfType.CInt)
    val bool = if (hasBool) s"""case Bool(v) => ${generateSetEnum(OfType.CBoolean)}""" else ""
    val str = if (hasStr) s"""case Str(v) => ${generateSetEnum(OfType.CString)}""" else ""
    val nulls = s"case Null => () // nulls are valid"
    val num = if (hasInt && hasDouble)
      s"""
         |case Num(v) =>
         |            if (v.isValidInt)
         |              { ${generateSetEnum(OfType.CInt)} }
         |            else
         |              { ${generateSetEnum(OfType.CDouble)} }
         |""".stripMargin
    else if (hasInt && !hasDouble)
      s"""
        |case Num(v) => ${generateSetEnum(OfType.CInt)}
        |""".stripMargin
    else if (!hasInt && hasDouble)
      s"""
        |case Num(v) => ${generateSetEnum(OfType.CDouble)}
        |""".stripMargin
    else
    ""
    val t = "generateInfoArrayFromJson()" :: trace
    s"""
       |override def fromJson(in: Value): this.type = {
       |    ${commentCallTrace(t)}
       |  def err(j: ujson.Value): OfPrimitive =
       |    throw new RuntimeException(s\"\"\"Parsing of $$j failed: $$in\"\"\")
       |  in match {
       |    case Arr(values) =>
       |      values.zipWithIndex.foreach { kv =>
       |        val v = kv._1
       |        val i = kv._2
       |        v match {
       |          $bool
       |          $str
       |          $num
       |          $nulls
       |          case _ => err(in)
       |        }
       |      }
       |      this
       |    case _ =>
       |      err(in)
       |      this
       |  }
       |}
       |""".stripMargin
  }

  def generateInfoArrayToJson(usedMethods: List[OfPrimitive], trace: List[String]):
  String = {
    // Add case checks for those enums.
    // Note that to avoid compilation warnings we use all OfPrimitive
    val cases = OfType.allPrimitives.map( t => enumArrayCase(usedMethods.contains(t), t) )
    // each case generates the json string according to the enum type
    val options = cases.mkString("\n")
    val t = "generateInfoArrayToJson(methods: Map[OfPrimitive, String])" :: trace
    val method =
      s"""
         |$def_json_String = {
         |    ${commentCallTrace(t)}
         |    val nulls = "null"
         |    val values = (0 until n) map { i =>
         |    val t = selected.get(i)
         |    t.fold(nulls)({
         |      $options
         |    })
         |  }
         |  val allNull = values.forall(_.toLowerCase.equals(nulls))
         |  if (allNull)
         |    ""
         |  else {
         |    values.filter(_.length > 0).mkString("[", ",", "]")
         |  }
         |}
         |""".stripMargin
    method
  }

  def generateInfoArrayDeclaration(in: PrimitiveInfoArray, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateInfoArrayDeclaration(in: PrimitiveInfoArray)" :: trace
    // These are the info_array items
    //val items = in.items
    // If we have doubles add support for integer
    val items = in.items.map{ e =>
      val t1 = e._2.exists(p => p.isInstanceOf[TDouble])
      val t2 = e._2.exists(p => p.isInstanceOf[TInt])
      if (t1 && !t2)
        (e._1, OfType.CInt :: e._2)
      else
        e
    }
    val description = in.description
    for {
      description_ <- description
    } yield {
      // generate the methods to set the array values
      val methods = generateArraySetMethods(items).toList
      val methodsPerLine = methods.map(_._3).mkString("\n")
      // Generate the values which hold the array values
      val declarationsX = generateArraySetVariables(items).mkString("\n")
      // Get the enum types used in methods
      val types = items.values.flatten.toSet.toList
      val toJson = generateInfoArrayToJson(types, t)
      val fromJson = generateInfoArrayFromJson(types, t)
      val name = changeReservedNames(in.name)
      val code =
        s"""
           |object $name extends JSON {
           |  ${commentCallTrace(t)}
           |  val description: Option[String] = ${addQuotes(description_)}
           |
           |$declarationsX
           |$toJson
           |$fromJson
           |
           |
           | $methodsPerLine
           |}
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }


  def generateDataArrayDeclaration(in: PrimitiveDataArray, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateDataArrayDeclaration(in: PrimitiveDefinition)" :: trace
    // Get the optional fields, if they have a value use them else set to None
    for {
      dflt_ <- in.dflt
      description_ <- in.description
    } yield {
      val name = changeReservedNames(in.name)
      val arr = dflt_.fold("None")(e => s"Some(${e.mkString("Array(", "," ,")")})")
      val code =
        s"""
           |${commentCallTrace(t)}
           |val $name: DataArray = DataArray(
           |                      dflt = $arr,
           |                      description = ${addQuotes(description_)}
           |                     )
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }

  def generateRegExpDeclaration(in: PrimitiveRegExEnumerate, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateRegExpDeclaration(in: PrimitiveInfoArray)" :: trace
    // These are the enumerate items
    for {
      description_ <- in.description
      v <- in.values
      dflt_ <- in.dflt
    } yield {
      val methods_ = generateEnumSetMethods(v)
      val allMethods = methods_.mkString("\n")
      val current = from(in.name, in.parent)
      val default = if (dflt_.isDefined) encodeValueType(current, dflt_.getOrElse(NAPrimitive), t) else Right("")
      val d = if (dflt_.isDefined) s"val dflt ${default.fold(e => e.errs.mkString(","), s => s)}" else ""
      // Get the enum types used in methods
      val types = setOfEnumTypes(v)
      val toJson = declareEnumerateToJsonMethod(types, t)
      val fromJson = declareEnumerateFromJsonMethod(types, t)
      val name = changeReservedNames(in.name)
      val code =
        s"""
           |object ${name} extends JSON {
           |  ${commentCallTrace(t)}
           |  val description: Option[String] = ${addQuotes(description_)}
           |
           |  private var selected: OfPrimitive = NAPrimitive
           |
           | $toJson
           | $fromJson
           |
           |  $allMethods
           |}
           |""".stripMargin
      CodeSnippet(List(DeclarationID(name,in.name)), code)
    }
  }


  def generatePrimitiveDeclaration(in: PrimitiveDefinition, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val trc = "generatePrimitiveDeclaration(in: PrimitiveDefinition)" :: trace
    in match {
      case v:PrimitiveNumber => generateNumberDeclaration(v, trc)
      case v:PrimitiveInt => generateIntDeclaration(v, trc)
      case v:PrimitiveString => generateStringDeclaration(v, trc)
      case v:PrimitiveBoolean => generateBooleanDeclaration(v, trc)
      case v:PrimitiveSubPlotId => generateSubPlotIDDeclaration(v, trc)
      case v:PrimitiveFlagList => generateFlagListDeclaration(v, trc)
      case v:PrimitiveEnumerate => generateEnumeratedDeclaration(v, trc)
      case v:PrimitiveInfoArray => generateInfoArrayDeclaration(v, trc)
      case v:PrimitiveColor => generateColorDeclaration(v, trc)
      case v:PrimitiveAny => generateAnyDeclaration(v, trc)
      case v:PrimitiveDataArray => generateDataArrayDeclaration(v, trc)
      case v:PrimitiveAngle => generateAngleDeclaration(v, trc)
      case v:PrimitiveColorList => generateColorListDeclaration(v, trc)
      case v:PrimitiveColorScale => generateColorScaleDeclaration(v, trc)
      case v:PrimitiveRegExEnumerate => generateRegExpDeclaration(v, trc)
      case v:BaseEnumerate => Left(Error("Invalid class usage $v" :: trace))
    }
  }


  def split(in: Either[Error, Iterable[CodeSnippet]]): Either[Error, CodeSnippet] = {
    in.map{ e =>
      val members = e.flatMap(_.members)
      val codes = e.map(_.code)
      val code = codes.mkString("\n")
      CodeSnippet(members.toList, code)
    }
  }

  def join(c: Either[Error, Iterable[CodeSnippet]]): Either[Error, CodeSnippet] = {
    c.map { snippets =>
      val code = snippets.map(_.code).mkString("\n")
      val members = snippets.flatMap(_.members).toList
      CodeSnippet(members, code)
    }
  }

  def generateValues(obj: CompoundDefinition, trace:List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateValues" :: trace
    // process the values
    val in = obj.metaValues
    // generate code for each value
    val s1 = in.map(e => generateValueDeclaration(e._2, t))
    // check for errors
    val default = CodeSnippet(List(), s"Unexpected Left for values: ${obj.parent}, ${obj.name}")
    val s2 = checkError( s1, default)
    // compose code for all the values
    join(s2)
  }

  def generatePrimitives(obj: CompoundDefinition, trace:List[String]): Either[Error, CodeSnippet] = {
    // process the values
    val in = obj.member
    val t = "generateMembers(obj: ObjectDefinition)" :: trace
    // generate code for each value
    val s1 = in.map(e => generatePrimitiveDeclaration(e._2, trace))
    // check for errors
    val default = CodeSnippet( List(defaultID), s"Unexpected Left for primitives: ${obj.parent}, ${obj.name}" )
    val s2: Either[Error, Iterable[CodeSnippet]] = checkError(s1, default)
    // compose code for all the values
    split(s2)
  }


  def generateArrayValues(obj: CompoundDefinition, trace:List[String]):
  Either[Error, CodeSnippet] = {
    // process the values
    val in = obj.arrs
    val t = "generateArrayValues(obj: ObjectDefinition)" :: trace
    // generate code for each value
    val s1: immutable.Iterable[Either[Error, CodeSnippet]] = in.map(e => generateArraysValue(obj, e._2, t))
    // check for errors
    val default = CodeSnippet(List(), s"Unexpected Left for values: ${obj.parent}, ${obj.name}")
    val s2: Either[Error, Iterable[CodeSnippet]] = checkError(s1, default)
    // compose code for all the values
    val s3 = s2.map( e => CodeSnippet(List(),e.map(_.code).mkString("\n")))
    s3
  }

  def generateArraysValue(obj: CompoundDefinition, in: List[ValueDefinition], trace:List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateArraysValue(in: List[ValueDefinition])" :: trace
    // generate code for each value
    val s1: List[Either[Error, CodeSnippet]] = in.map(e => generateValueDeclaration(e,t) )
    // check for errors
    val default = CodeSnippet( List(defaultID), s"Unexpected Left for array value: ${obj.parent}, ${obj.name}" )
    val s2: Either[Error, Iterable[CodeSnippet]] = checkError(s1, default)
    // compose code for all the values
    split(s2)
  }

  def declareJsonMember(depth:Int, i: Int, e: DeclarationID): String = {
    val memberKey = s""" ${doubleQuotes(e.original)} + ":" + ${e.valid}.json()"""
    s""" val v$i = if (${e.valid}.json().length > 0) $memberKey else "" """
  }


  def declareToJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[DeclarationID], trace: List[String]):
  String = {
    val idxs = members.indices.toList
    val subs = idxs
      .zip(members)
      .map({ case (i, e) => declareJsonMember(depth, i, e)})
      .mkString("\n")
    // Do we have type information? If yes, add it.
    val allIdxs = if (typeName.isDefined) idxs.length :: idxs else idxs
    // Generate one variable for each Json output, place these into a list
    val list = "val vars: List[String] = " + allIdxs
      .map(i => s"v$i")
      .mkString("List(", ",", ")")
    // Update generator trace for debugging
    val t = "declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])" :: trace
    s"""
       |$def_json_String = {
       |  ${commentCallTrace(t)}
       |  ${if (typeName.isDefined) s"""val v${idxs.length} = "\\"type\\" : \\"${typeName.get}\\"" """ else ""}
       |  $subs
       |  $list
       |  val cleanVars = vars.filter(_.length > 0).mkString(",\\n")
       |  if (cleanVars.length > 0) {
       |    // values set
       |    "{" + cleanVars + "}"
       |  }
       |  else {
       |    // no values set
       |    ${ if(depth == 1) "\"{}\"" else "\"\"" }
       |  }
       |}
       |""".stripMargin
  }


  def declareJsonParser(depth:Int, i: Int, e: DeclarationID): String = {
    s"""
       |val in$i = mapKV.get(\"${e.original}\")
       |in$i.map(ve => ${e.valid}.fromJson(ve))
       |""".stripMargin
  }

  def declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[DeclarationID], trace: List[String]):
  String = {
    // Update generator trace for debugging
    val t = "declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])" :: trace
    val idxs = members.indices.toList
    val subs = idxs
      .zip(members)
      .map({ case (i, e) => declareJsonParser(depth, i, e)})
      .mkString("\n")
    s"""
       |
       |def fromJson(in:Value): this.type  = {
       |  ${commentCallTrace(t)}
       |  def err = throw new RuntimeException(s"Expected an Obj but got $$in")
       |  in.objOpt.fold( err ){ mapKV =>
       |    $subs
       |  }
       |  this
       |}
       |
       |""".stripMargin
  }

  def generateObjects(obj: CompoundDefinition, trace: List[String]):
  Either[Error, CodeSnippet] = {
    // process the values
    val in = obj.objs
    val t = "generateObjects(obj: ObjectDefinition)" :: trace
    // generate code for each value
    val s1: immutable.Iterable[Either[Error, CodeSnippet]] = in.map(e => generateCompound(e._2, t))
    // check for errors
    val default = CodeSnippet( List(defaultID), s"Unexpected Left for values: ${obj.parent}, ${obj.name}")
    val s2: Either[Error, Iterable[CodeSnippet]] = checkError(s1, default)
    // compose code for all the values
    val s3 = split(s2)
    s3
  }

  val emptyLayoutAttributes: String =
    """
      |object layoutAttributes extends JSON {
      |  def json(): String = ""
      |  def fromJson(in:Value): this.type = this
      |}
      |""".stripMargin

  def generateCaseClassCode(name: String,
                            isTrace: Boolean,
                            hasLayoutAttributes: Boolean,
                            constants: CodeSnippet,
                            arrays: CodeSnippet,
                            members: CodeSnippet,
                            objs: CodeSnippet,
                            trace: List[String]): CodeSnippet = {
    val t = "generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)" :: trace
    val className = capitalize(changeReservedNames(name))
    val code = constants.code + "\n" + arrays.code + "\n" + members.code + "\n" + objs.code
    val tmp =
      s"""
         |case class ${className}() ${if (isTrace) "extends Trace" else "extends Layouts"} {
         |${commentCallTrace(t)}
         |$code
         |${if (!hasLayoutAttributes) emptyLayoutAttributes else ""}
         |}
         |""".stripMargin
    CodeSnippet(List(DeclarationID(className,name)), tmp)
  }

  def generateObjCode(depth:Int,
                      name: String,
                      typeName: Option[String],
                      constants: CodeSnippet,
                      arrays: CodeSnippet,
                      members: CodeSnippet,
                      objs: CodeSnippet,
                      trace: List[String]): CodeSnippet = {
    val t = "generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)" :: trace
    //val className = capitalize(obj.typeName)
    val className = changeReservedNames(name)
    val code = constants.code + "\n" + arrays.code + "\n" + members.code + "\n" + objs.code
    val allDecl = objs.members ++ members.members
    val toJson = declareToJsonMethod(depth, name, typeName, allDecl, t)
    val fromJson = declareFromJsonMethod(depth, name, typeName, allDecl, t)
    val tmp =
      s"""
         |case object ${className} extends JSON {
         |${commentCallTrace(t)}
         |$code
         |$toJson
         |$fromJson
         |}
         |""".stripMargin
    CodeSnippet(List(DeclarationID(className,name)), tmp)
  }

  def generateCompound(obj: CompoundDefinition, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val t = "generateCompound(obj: ObjectDefinition)" :: trace
    for {
      values <- generateValues(obj, t)
      arrays <- generateArrayValues(obj, t)
      members <- generatePrimitives(obj, t)
      objs <- generateObjects(obj, t)
    } yield {
      obj match {
        case e:ClassDefinition =>
          val isTrace = e.parent.contains(Constants.TRACES)
          val hasLayout = obj.objs.contains(Constants.LAYOUTATTRIBUTES)
          generateCaseClassCode(e.name, isTrace, hasLayout, values, arrays, members, objs, t)
        case e:ObjectDefinition =>
          generateObjCode(e.depth, e.name, e.classType, values, arrays, members, objs, t)
      }
    }
  }

  def commentCallTrace(trace: List[String]): String = {
    "/* §#\n" + trace.map(" * " + _).mkString("\n") + "*/"
  }

  def generate(outPath: Path, name: String, obj: CompoundDefinition, trace: List[String]):
  Either[Error, CodeSnippet] = {
    val fileName = capitalize(name)
    val out = outPath / (fileName + ".scala")
    val t = "generate(obj: CompoundDefinition)" :: trace
    val code = generateCompound(obj, t)
    os.write.over(out, s"""
                          |package splotly.gen
                          |
                          |import splotly._
                          |import ujson._
                          |
                          |${commentCallTrace(t)}
                          |
                          |import scala.collection.mutable
                          |
                          |${code.fold(e => s"val error = $e", s => s.code)}
                          |
                          |""".stripMargin)
    code
  }

  def generate(outPath: Path, name: String, obj: Either[Error, CompoundDefinition], trace: List[String]):
  Either[Error, CodeSnippet]
  = {
    val t = "generate(obj: Either[Error, CompoundDefinition])" :: trace
    obj.flatMap( f => generate(outPath, name, f, t))
  }



  def generateWrapperList(outPath: Path,
                          name: String,
                          wrappers: Either[Error, Iterable[(String, String)]], trace: List[String]): String = {
    val fileName = capitalize(name)
    val out = outPath / (fileName + ".scala")
    val t = "generateWrapperList(outPath: Path, name: String, wrappers: Either[Error, Iterable[(List[String], String)]])" :: trace
    val maps = wrappers.map( traces =>
      traces.map{ kv => s""""${kv._1}" -> ${kv._2}"""}
            .mkString(",\n") )
    val code =
      s"""
        |package splotly.gen
        |
        |import splotly._
        |import ujson._
        |
        |case object Traces {
        |    val build: Map[String, () => Trace] = Map(
        |       ${maps.fold(e => e.errs.mkString("\n"), e => e)}
        |    )
        |}
        |
        |""".stripMargin
    os.write.over(out, code)
    code
  }

}
