package webkit

import better.files.Dsl.cwd
import utest.{TestSuite, Tests, assert, test}

/**
 * Here we run WebKit to load and take snapshots of several plots.
 * These tests are executed synchronously. It is important that the tests use
 * uTests's assert method.
 *
 * IMPORTANT NOTE: JavaFX or OpenJFX applications can only be instantiated once.
 * There is a RobotFX. But for now we use a custom uTest [[CustomFramework]] to
 * launch and use a single application.
 *
 * mill mill.scalalib.GenIdea/idea
 *
 * ./mill -i webkit.test
 * ./mill -i webkit.test.testLocal
 * ./mill -i webkit.test webkit.HeadlessWebKitSpec
 * ./mill -i webkit.test webkit.HeadlessWebKitSpec.URITests
 * ./mill -i webkit.test webkit.HeadlessWebKitSpec.ImageCapture.PNG
 *
 * Note: use this version to access the JavaFX libraries
 * ./jfxmill.sh -i webkit.test webkit.HeadlessWebKitSpec
 *
 * @see http://mundrisoft.com/tech-bytes/compare-images-using-java/
 *      https://stackoverflow.com/questions/8567905/how-to-compare-images-for-similarity-using-java
 */
object HeadlessWebKitSpec extends TestSuite {

  /*
  // https://github.com/lihaoyi/utest#running-code-before-and-after-test-cases
  HeadlessWebKit.launchBackground(Array[String]())

  // https://github.com/lihaoyi/utest#running-code-before-and-after-test-suites
  // https://github.com/lihaoyi/utest#054
  override def utestAfterAll(): Unit = {
    println("on after all")
    HeadlessWebKit.stop()
  }
  */

  val tests: Tests = Tests {

    test("URITests") {

      test("relativeFileURI") {
        val relativeToCWD = HeadlessWebKit.file("./webkit/src/webkit/test.html")
        assert(relativeToCWD == s"""file://$cwd/./webkit/src/webkit/test.html""")
      }

      test("absoluteFileURI") {
        val absoluteFile = HeadlessWebKit.file("/home/hmf/IdeaProjects/srllab/webkit/src/webkit/test.html")
        assert(absoluteFile == """file:///home/hmf/IdeaProjects/srllab/webkit/src/webkit/test.html""")
      }

      test("absoluteHTTPURI") {
        val absoluteFile = HeadlessWebKit.http("www.google.com/")
        assert(absoluteFile == """http://www.google.com/""")
      }

      test("absoluteHTTSPURI") {
        val absoluteFile = HeadlessWebKit.https("www.google.com/")
        assert(absoluteFile == """https://www.google.com/""")
      }

    }

    test("ImageCapture SVG Color") {

      val color = Mocks.SVGcolor.files
      import color._

      test("PNG Color") {
        ImageTestUtils.captureAndCompareImage(htmlFile, pngFileTmp, goldPng)
      }

      test("GIF Color") {
        ImageTestUtils.captureAndCompareImage(htmlFile, gifFileTmp, goldGif)
      }

      test("TIFF Color") {
        ImageTestUtils.captureAndCompareImage(htmlFile, tifFileTmp, goldTif)
      }

      // Not supported
      test("JPEG Color") {
        ImageTestUtils.captureAndCompareImage(htmlFile, jpegFileTmp, goldJpeg, false)
      }

      // Not supported
      test("BMP Color") {
        ImageTestUtils.captureAndCompareImage(htmlFile, bmpFileTmp, goldBmp, false)
      }

      // Not supported
      test("WBMP Color") {
        ImageTestUtils.captureAndCompareImage(htmlFile, wbmpFileTmp, goldWbmp, false)
      }
    }

    test("ImageCapture SVG B&W") {

      val bw = Mocks.SVG_BW.files
      import bw._

      test("PNG B&W") {
        ImageTestUtils.captureAndCompareImage(htmlFile, pngFileTmp, goldPng)
      }

      test("GIF B&W") {
        ImageTestUtils.captureAndCompareImage(htmlFile, gifFileTmp, goldGif)
      }

      test("TIFF B&W") {
        ImageTestUtils.captureAndCompareImage(htmlFile, tifFileTmp, goldTif)
      }
    }

    test("ImageCapture SVG Plot") {

      val plot = Mocks.SVG_Plot.files
      import plot._

      test("PNG B&W") {
        ImageTestUtils.captureAndCompareImage(htmlFile, pngFileTmp, goldPng)
      }

      test("GIF B&W") {
        ImageTestUtils.captureAndCompareImage(htmlFile, gifFileTmp, goldGif)
      }

      test("TIFF B&W") {
        ImageTestUtils.captureAndCompareImage(htmlFile, tifFileTmp, goldTif)
      }
    }

  }

}
