package splotly.gen

import splotly._
import ujson._

/* §#
 * generate(obj: CompoundDefinition)
 * generate(obj: Either[Error, CompoundDefinition])*/

import scala.collection.mutable

case class Image() extends Trace {
  /* §#
   * generateCaseClass(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
   * generateCompound(obj: ObjectDefinition)
   * generate(obj: CompoundDefinition)
   * generate(obj: Either[Error, CompoundDefinition])*/
  val type_ : String      = """image"""
  val animatable: Boolean = false
  val categories: Array[String] =
    Array("""noSortingByValue""", """2dMap""", """svg""", """cartesian""")

  case object meta extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val description: String =
      """Display an image, i.e. data on a 2D regular raster. By default, when an image is displayed in a subplot, its y axis will be reversed (ie. `autorange: 'reversed'`), constrained to the domain (ie. `constrain: 'domain'`) and it will have the same scale as its x axis (ie. `scaleanchor: 'x,`) in order for pixels to be rendered as squares."""

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/

      val vars: List[String] = List()
      val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV => }
      this
    }

  }

  case object attributes extends JSON {
    /* §#
     * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val type_ : String = """image"""

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplate: PChars = PChars(
      dflt = Some(""""""),
      description = Some(
        """Template string used for rendering the information that appear on hover box. Note that this will override `hoverinfo`. Variables are inserted using %{variable}, for example "y: %{y}". Numbers are formatted using d3-format's syntax %{variable:d3-format}, for example "Price: %{y:$.2f}". https://github.com/d3/d3-3.x-api-reference/blob/master/Formatting.md#d3_format for details on the formatting syntax. Dates are formatted using d3-time-format's syntax %{variable|d3-time-format}, for example "Day: %{2019-01-01|%A}". https://github.com/d3/d3-3.x-api-reference/blob/master/Time-Formatting.md#format for details on the date formatting syntax. The variables available in `hovertemplate` are the ones emitted as event data described at this link https://plot.ly/javascript/plotlyjs-events/#event-data. Additionally, every attributes that can be specified per-point (the ones that are `arrayOk: true`) are available. variables `z`, `color` and `colormodel`. Anything contained in tag `<extra>` is displayed in the secondary box, for example "<extra>{fullData.name}</extra>". To hide the secondary box completely, use an empty tag `<extra></extra>`."""
      )
    )

    object colormodel extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: String = """rgb"""
      val description: Option[String] = Some(
        """Color model used to map the numerical color components described in `z` into colors."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TBoolean(false)"""
            )
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => err(in)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def hsla(): Unit = { selected = TString("hsla") }

      def hsl(): Unit = { selected = TString("hsl") }

      def rgba(): Unit = { selected = TString("rgba") }

      def rgb(): Unit = { selected = TString("rgb") }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val name: PChars = PChars(
      dflt = None,
      description = Some(
        """Sets the trace name. The trace name appear as the legend item and on hover."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dy: PReal = PReal(
      min = None,
      max = None,
      dflt = Some("""1.0"""),
      description = Some("""Set the pixel's vertical size""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertemplatesrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertemplate .""")
    )

    object hoverinfo extends JSON {
      /* §#
       * generateFlagListDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Option[String] = Some("""x+y+z+text+name""")
      val description: Option[String] = Some(
        """Determines which trace information appear on hover. If `none` or `skip` are set, no information is displayed upon hovering. But, if `none` is set, click and hover events are still fired."""
      )

      private val flag_ : mutable.Set[String] = mutable.Set()
      //dflt.map(e => flag_ + e)
      def flag: String = flag_.mkString("+")
      def json(): String = {
        if (flag.contains("true"))
          "true"
        else if (flag.contains("false"))
          "false"
        else if (flag.length > 0)
          "\"" + flag + "\""
        else
          ""
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateFlagListFromJson()
         * generateFlagListDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Str(value) =>
            flag_ += value
            this
          case Bool(value) =>
            flag_ += value.toString
            this
          case _ =>
            err(in)
            this
        }
      }

      def textOn(): Unit   = { flag_ + "text" }
      def textOff(): Unit  = { flag_ - "text" }
      def nameOn(): Unit   = { flag_ + "name" }
      def nameOff(): Unit  = { flag_ - "name" }
      def colorOn(): Unit  = { flag_ + "color" }
      def colorOff(): Unit = { flag_ - "color" }
      def zOn(): Unit      = { flag_ + "z" }
      def zOff(): Unit     = { flag_ - "z" }
      def yOn(): Unit      = { flag_ + "y" }
      def yOff(): Unit     = { flag_ - "y" }
      def xOn(): Unit      = { flag_ + "x" }
      def xOff(): Unit     = { flag_ - "x" }
      def skipOn(): Unit   = { flag_ + "skip" }
      def skipOff(): Unit  = { flag_ - "skip" }
      def noneOn(): Unit   = { flag_ + "none" }
      def noneOff(): Unit  = { flag_ - "none" }
      def allOn(): Unit    = { flag_ + "all" }
      def allOff(): Unit   = { flag_ - "all" }
    }

    object visible extends JSON {
      /* §#
       * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val dflt: Boolean = true
      val description: Option[String] = Some(
        """Determines whether or not this trace is visible. If *legendonly*, the trace is not drawn, but can appear as a legend item (provided that the legend itself is visible)."""
      )

      private var selected: OfPrimitive = NAPrimitive
      //allDeclarations

      def json(): String = {
        /* §#
         * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        selected match {
          case NAPrimitive => ""
          case TString(t)  => "\"" + t.toString + "\""
          case TBoolean(t) => t.toString
          case TDouble(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TDouble(0.0)"""
            )
          case TInt(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TInt(0)"""
            )
          case TImpliedEdits(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TImpliedEdits()"""
            )
          case TAttrRegexps(t) =>
            throw new RuntimeException(
              """Unexpected OfPrimitive type: TAttrRegexps()"""
            )
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        val read = in match {
          case Str(s)  => TString(s)
          case Bool(s) => TBoolean(s)
          case Num(s)  => err(in)
          case _       => err(in)
        }
        selected = read
        this
      }

      def true_(): Unit = { selected = TBoolean(true) }

      def false_(): Unit = { selected = TBoolean(false) }

      def legendonly(): Unit = { selected = TString("legendonly") }

    }

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uirevision: PAny = PAny(
      dflt = None,
      description = Some(
        """Controls persistence of some user-driven changes to the trace: `constraintrange` in `parcoords` traces, as well as some `editable: true` modifications such as `name` and `colorbar.title`. Defaults to `layout.uirevision`. Note that other user-driven trace attribute changes are controlled by `layout` attributes: `trace.visible` is controlled by `layout.legend.uirevision`, `selectedpoints` is controlled by `layout.selectionrevision`, and `colorbar.(x|y)` (accessible with `config: {editable: true}`) is controlled by `layout.editrevision`. Trace changes are tracked by `uid`, which only falls back on trace index if no `uid` is provided. So if your app can add/remove traces before the end of the `data` array, such that the same trace has a different index, you can still preserve user-driven changes if you give each trace a `uid` that stays with it as it moves."""
      )
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val y0: PAny = PAny(
      dflt = Some("""0.0"""),
      description = Some("""Set the image's y position.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val idssrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  ids .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val text: DataArray = DataArray(
      dflt = None,
      description =
        Some("""Sets the text elements associated with each z value.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hoverinfosrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hoverinfo .""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertextsrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  hovertext .""")
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val x0: PAny = PAny(
      dflt = Some("""0.0"""),
      description = Some("""Set the image's x position.""")
    )

    /* §#
     * generateAny(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val meta: PAny = PAny(
      dflt = None,
      description = Some(
        """Assigns extra meta information associated with this trace that can be used in various text attributes. Attributes such as trace `name`, graph, axis and colorbar `title.text`, annotation `text` `rangeselector`, `updatemenues` and `sliders` `label` text all support `meta`. To access the trace `meta` values in an attribute in the same trace, simply use `%{meta[i]}` where `i` is the index or key of the `meta` item in question. To access trace `meta` in layout attributes, use `%{data[n[.meta[i]}` where `i` is the index or key of the `meta` and `n` is the trace index."""
      )
    )

    object zmin extends JSON {
      /* §#
       * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val description: Option[String] = Some(
        """Array defining the lower bound for each color component. Note that the default value will depend on the colormodel. For the `rgb` colormodel, it is [0, 0, 0]. For the `rgba` colormodel, it is [0, 0, 0, 0]. For the `hsl` colormodel, it is [0, 0, 0]. For the `hsla` colormodel, it is [0, 0, 0, 0]."""
      )

      private var int: Array[Int]                 = Array.fill(4)(0)
      private var double: Array[Double]           = Array.fill(4)(0.0)
      private var selected: Map[Int, OfPrimitive] = Map()
      private val n: Integer                      = 4

      def json(): String = {
        /* §#
         * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val nulls = "null"
        val values = (0 until n) map { i =>
          val t = selected.get(i)
          t.fold(nulls)({
            case NAPrimitive =>
              throw new RuntimeException(s"Unexpected type: $t")
            case TString(_) =>
              throw new RuntimeException(s"Unexpected type: $t")
            case TBoolean(_) =>
              throw new RuntimeException(s"Unexpected type: $t")
            case TDouble(_) => double(i).toString
            case TInt(_)    => int(i).toString
            case TImpliedEdits(_) =>
              throw new RuntimeException(s"Unexpected type: $t")
            case TAttrRegexps(_) =>
              throw new RuntimeException(s"Unexpected type: $t")
          })
        }
        val allNull = values.forall(_.toLowerCase.equals(nulls))
        if (allNull)
          ""
        else {
          values.filter(_.length > 0).mkString("[", ",", "]")
        }
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateInfoArrayFromJson()
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Arr(values) =>
            values.zipWithIndex.foreach { kv =>
              val v = kv._1
              val i = kv._2
              v match {

                case Num(v) =>
                  if (v.isValidInt) {
                    int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                  } else { double(i) = v; selected += (i -> TDouble(v)) }

                case Null => () // nulls are valid
                case _    => err(in)
              }
            }
            this
          case _ =>
            err(in)
            this
        }
      }

// Set the value
      def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
      def int_0(): Unit = { selected -= 0 }

// Set the value
      def double_0(v: Double): Unit = {
        double(0) = v; selected += (0 -> TDouble(v))
      }
// Delete the value (use default)
      def double_0(): Unit = { selected -= 0 }

// Set the value
      def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
      def int_1(): Unit = { selected -= 1 }

// Set the value
      def double_1(v: Double): Unit = {
        double(1) = v; selected += (1 -> TDouble(v))
      }
// Delete the value (use default)
      def double_1(): Unit = { selected -= 1 }

// Set the value
      def int_2(v: Int): Unit = { int(2) = v; selected += (2 -> TInt(v)) }
// Delete the value (use default)
      def int_2(): Unit = { selected -= 2 }

// Set the value
      def double_2(v: Double): Unit = {
        double(2) = v; selected += (2 -> TDouble(v))
      }
// Delete the value (use default)
      def double_2(): Unit = { selected -= 2 }

// Set the value
      def int_3(v: Int): Unit = { int(3) = v; selected += (3 -> TInt(v)) }
// Delete the value (use default)
      def int_3(): Unit = { selected -= 3 }

// Set the value
      def double_3(v: Double): Unit = {
        double(3) = v; selected += (3 -> TDouble(v))
      }
// Delete the value (use default)
      def double_3(): Unit = { selected -= 3 }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val textsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  text .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val hovertext: DataArray = DataArray(
      dflt = None,
      description = Some("""Same as `text`.""")
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val uid: PChars = PChars(
      dflt = None,
      description = Some(
        """Assign an id to this trace, Use this to provide object constancy between traces during animations and transitions."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val zsrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  z .""")
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val opacity: PReal = PReal(
      min = Some(0.0),
      max = Some(1.0),
      dflt = Some("""1.0"""),
      description = Some("""Sets the opacity of the trace.""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val ids: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns id labels to each datum. These ids for object constancy of data points during animation. Should be an array of strings, not numbers or any other type."""
      )
    )

    /* §#
     * generateNumberDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val dx: PReal = PReal(
      min = None,
      max = None,
      dflt = Some("""1.0"""),
      description = Some("""Set the pixel's horizontal size.""")
    )

    /* §#
     * generateSubPlotIDDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val yaxis: SubPlotID = SubPlotID(
      dflt = Some("""y"""),
      description = Some(
        """Sets a reference between this trace's y coordinates and a 2D cartesian y axis. If *y* (the default value), the y coordinates refer to `layout.yaxis`. If *y2*, the y coordinates refer to `layout.yaxis2`, and so on."""
      )
    )

    /* §#
     * generateSubPlotIDDeclaration(in: PrimitiveDefinition
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val xaxis: SubPlotID = SubPlotID(
      dflt = Some("""x"""),
      description = Some(
        """Sets a reference between this trace's x coordinates and a 2D cartesian x axis. If *x* (the default value), the x coordinates refer to `layout.xaxis`. If *x2*, the x coordinates refer to `layout.xaxis2`, and so on."""
      )
    )

    object zmax extends JSON {
      /* §#
       * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val description: Option[String] = Some(
        """Array defining the higher bound for each color component. Note that the default value will depend on the colormodel. For the `rgb` colormodel, it is [255, 255, 255]. For the `rgba` colormodel, it is [255, 255, 255, 1]. For the `hsl` colormodel, it is [360, 100, 100]. For the `hsla` colormodel, it is [360, 100, 100, 1]."""
      )

      private var int: Array[Int]                 = Array.fill(4)(0)
      private var double: Array[Double]           = Array.fill(4)(0.0)
      private var selected: Map[Int, OfPrimitive] = Map()
      private val n: Integer                      = 4

      def json(): String = {
        /* §#
         * generateInfoArrayToJson(methods: Map[OfPrimitive, String])
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val nulls = "null"
        val values = (0 until n) map { i =>
          val t = selected.get(i)
          t.fold(nulls)({
            case NAPrimitive =>
              throw new RuntimeException(s"Unexpected type: $t")
            case TString(_) =>
              throw new RuntimeException(s"Unexpected type: $t")
            case TBoolean(_) =>
              throw new RuntimeException(s"Unexpected type: $t")
            case TDouble(_) => double(i).toString
            case TInt(_)    => int(i).toString
            case TImpliedEdits(_) =>
              throw new RuntimeException(s"Unexpected type: $t")
            case TAttrRegexps(_) =>
              throw new RuntimeException(s"Unexpected type: $t")
          })
        }
        val allNull = values.forall(_.toLowerCase.equals(nulls))
        if (allNull)
          ""
        else {
          values.filter(_.length > 0).mkString("[", ",", "]")
        }
      }

      override def fromJson(in: Value): this.type = {
        /* §#
         * generateInfoArrayFromJson()
         * generateInfoArrayDeclaration(in: PrimitiveInfoArray)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err(j: ujson.Value): OfPrimitive =
          throw new RuntimeException(s"""Parsing of $j failed: $in""")
        in match {
          case Arr(values) =>
            values.zipWithIndex.foreach { kv =>
              val v = kv._1
              val i = kv._2
              v match {

                case Num(v) =>
                  if (v.isValidInt) {
                    int(i) = v.toInt; selected += (i     -> TInt(v.toInt))
                  } else { double(i) = v; selected += (i -> TDouble(v)) }

                case Null => () // nulls are valid
                case _    => err(in)
              }
            }
            this
          case _ =>
            err(in)
            this
        }
      }

// Set the value
      def int_0(v: Int): Unit = { int(0) = v; selected += (0 -> TInt(v)) }
// Delete the value (use default)
      def int_0(): Unit = { selected -= 0 }

// Set the value
      def double_0(v: Double): Unit = {
        double(0) = v; selected += (0 -> TDouble(v))
      }
// Delete the value (use default)
      def double_0(): Unit = { selected -= 0 }

// Set the value
      def int_1(v: Int): Unit = { int(1) = v; selected += (1 -> TInt(v)) }
// Delete the value (use default)
      def int_1(): Unit = { selected -= 1 }

// Set the value
      def double_1(v: Double): Unit = {
        double(1) = v; selected += (1 -> TDouble(v))
      }
// Delete the value (use default)
      def double_1(): Unit = { selected -= 1 }

// Set the value
      def int_2(v: Int): Unit = { int(2) = v; selected += (2 -> TInt(v)) }
// Delete the value (use default)
      def int_2(): Unit = { selected -= 2 }

// Set the value
      def double_2(v: Double): Unit = {
        double(2) = v; selected += (2 -> TDouble(v))
      }
// Delete the value (use default)
      def double_2(): Unit = { selected -= 2 }

// Set the value
      def int_3(v: Int): Unit = { int(3) = v; selected += (3 -> TInt(v)) }
// Delete the value (use default)
      def int_3(): Unit = { selected -= 3 }

// Set the value
      def double_3(v: Double): Unit = {
        double(3) = v; selected += (3 -> TDouble(v))
      }
// Delete the value (use default)
      def double_3(): Unit = { selected -= 3 }

    }

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val metasrc: PChars = PChars(
      dflt = None,
      description = Some("""Sets the source reference on plot.ly for  meta .""")
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdata: DataArray = DataArray(
      dflt = None,
      description = Some(
        """Assigns extra data each datum. This may be useful when listening to hover, click and selection events. Note that, *scatter* traces also appends customdata items in the markers DOM elements"""
      )
    )

    /* §#
     * generateDataArrayDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val z: DataArray = DataArray(
      dflt = None,
      description = Some(
        """A 2-dimensional array in which each element is an array of 3 or 4 numbers representing a color."""
      )
    )

    /* §#
     * generateStringDeclaration(in: PrimitiveDefinition)
     * generatePrimitiveDeclaration(in: PrimitiveDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generateObjects(obj: ObjectDefinition)
     * generateCompound(obj: ObjectDefinition)
     * generate(obj: CompoundDefinition)
     * generate(obj: Either[Error, CompoundDefinition])*/
    val customdatasrc: PChars = PChars(
      dflt = None,
      description =
        Some("""Sets the source reference on plot.ly for  customdata .""")
    )

    case object stream extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """calc"""

      /* §#
       * generateNumberDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val maxpoints: PReal = PReal(
        min = Some(0.0),
        max = Some(10000.0),
        dflt = Some("""500.0"""),
        description = Some(
          """Sets the maximum number of points to keep on the plots from an incoming stream. If `maxpoints` is set to *50*, only the newest 50 points will be displayed on the plot."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val token: PChars = PChars(
        dflt = None,
        description = Some(
          """The stream id number links a data trace on a plot with a stream. See https://plot.ly/settings for more details."""
        )
      )

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (maxpoints.json().length > 0)
            "\"maxpoints\"" + ":" + maxpoints.json()
          else ""
        val v1 =
          if (token.json().length > 0) "\"token\"" + ":" + token.json() else ""
        val vars: List[String] = List(v0, v1)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("maxpoints")
          in0.map(ve => maxpoints.fromJson(ve))

          val in1 = mapKV.get("token")
          in1.map(ve => token.fromJson(ve))

        }
        this
      }

    }

    case object hoverlabel extends JSON {
      /* §#
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val role: String     = """object"""
      val editType: String = """none"""

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val alignsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  align .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolor: PColor = PColor(
        dflt = None,
        description = Some(
          """Sets the background color of the hover labels for this trace"""
        )
      )

      object align extends JSON {
        /* §#
         * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val dflt: String = """auto"""
        val description: Option[String] = Some(
          """Sets the horizontal alignment of the text content within hover label box. Has an effect only if the hover label text spans more two or more lines"""
        )

        private var selected: OfPrimitive = NAPrimitive
        //allDeclarations

        def json(): String = {
          /* §#
           * declareEnumerateToJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          selected match {
            case NAPrimitive => ""
            case TString(t)  => "\"" + t.toString + "\""
            case TBoolean(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TBoolean(false)"""
              )
            case TDouble(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TDouble(0.0)"""
              )
            case TInt(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TInt(0)"""
              )
            case TImpliedEdits(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TImpliedEdits()"""
              )
            case TAttrRegexps(t) =>
              throw new RuntimeException(
                """Unexpected OfPrimitive type: TAttrRegexps()"""
              )
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareEnumerateFromJsonMethod( methods: Map[OfPrimitive, String])
           * def generateEnumeratedDeclaration(in: PrimitiveDefinition)
           * generatePrimitiveDeclaration(in: PrimitiveDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err(j: ujson.Value): OfPrimitive =
            throw new RuntimeException(s"""Parsing of $j failed: $in""")
          val read = in match {
            case Str(s)  => TString(s)
            case Bool(s) => err(in)
            case Num(s)  => err(in)
            case _       => err(in)
          }
          selected = read
          this
        }

        def auto(): Unit = { selected = TString("auto") }

        def right(): Unit = { selected = TString("right") }

        def left(): Unit = { selected = TString("left") }

      }

      /* §#
       * generateIntDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelength: PInt = PInt(
        min = Some(-1),
        max = None,
        dflt = Some(15),
        description = Some(
          """Sets the default length (in number of characters) of the trace name in the hover labels for all traces. -1 shows the whole name regardless of length. 0-3 shows the first 0-3 characters, and an integer >3 will show the whole name if it is less than that many characters, but if it is longer, will truncate to `namelength - 3` characters and add an ellipsis."""
        )
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val namelengthsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  namelength .""")
      )

      /* §#
       * generateColor(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolor: PColor = PColor(
        dflt = None,
        description =
          Some("""Sets the border color of the hover labels for this trace.""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bordercolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bordercolor .""")
      )

      /* §#
       * generateStringDeclaration(in: PrimitiveDefinition)
       * generatePrimitiveDeclaration(in: PrimitiveDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val bgcolorsrc: PChars = PChars(
        dflt = None,
        description =
          Some("""Sets the source reference on plot.ly for  bgcolor .""")
      )

      case object font extends JSON {
        /* §#
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val role: String        = """object"""
        val description: String = """Sets the font used in hover labels."""
        val editType: String    = """none"""

        /* §#
         * generateNumberDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val size: PReal =
          PReal(min = Some(1.0), max = None, dflt = None, description = None)

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val familysrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  family .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val colorsrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  color .""")
        )

        /* §#
         * generateColor(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val color: PColor = PColor(
          dflt = None,
          description = None
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val sizesrc: PChars = PChars(
          dflt = None,
          description =
            Some("""Sets the source reference on plot.ly for  size .""")
        )

        /* §#
         * generateStringDeclaration(in: PrimitiveDefinition)
         * generatePrimitiveDeclaration(in: PrimitiveDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        val family: PChars = PChars(
          dflt = None,
          description = Some(
            """HTML font family - the typeface that will be applied by the web browser. The web browser will only be able to apply a font if it is available on the system which it operates. Provide multiple font families, separated by commas, to indicate the preference in which to apply fonts if they aren't available on the system. The plotly service (at https://plot.ly or on-premise) generates images on a server, where only a select number of fonts are installed and supported. These include *Arial*, *Balto*, *Courier New*, *Droid Sans*,, *Droid Serif*, *Droid Sans Mono*, *Gravitas One*, *Old Standard TT*, *Open Sans*, *Overpass*, *PT Sans Narrow*, *Raleway*, *Times New Roman*."""
          )
        )

        def json(): String = {
          /* §#
           * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/

          val v0 =
            if (size.json().length > 0) "\"size\"" + ":" + size.json() else ""
          val v1 =
            if (familysrc.json().length > 0)
              "\"familysrc\"" + ":" + familysrc.json()
            else ""
          val v2 =
            if (colorsrc.json().length > 0)
              "\"colorsrc\"" + ":" + colorsrc.json()
            else ""
          val v3 =
            if (color.json().length > 0) "\"color\"" + ":" + color.json()
            else ""
          val v4 =
            if (sizesrc.json().length > 0) "\"sizesrc\"" + ":" + sizesrc.json()
            else ""
          val v5 =
            if (family.json().length > 0) "\"family\"" + ":" + family.json()
            else ""
          val vars: List[String] = List(v0, v1, v2, v3, v4, v5)
          val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
          if (cleanVars.length > 0) {
            // values set
            "{" + cleanVars + "}"
          } else {
            // no values set
            ""
          }
        }

        def fromJson(in: Value): this.type = {
          /* §#
           * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
           * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generateObjects(obj: ObjectDefinition)
           * generateCompound(obj: ObjectDefinition)
           * generate(obj: CompoundDefinition)
           * generate(obj: Either[Error, CompoundDefinition])*/
          def err = throw new RuntimeException(s"Expected an Obj but got $in")
          in.objOpt.fold(err) { mapKV =>
            val in0 = mapKV.get("size")
            in0.map(ve => size.fromJson(ve))

            val in1 = mapKV.get("familysrc")
            in1.map(ve => familysrc.fromJson(ve))

            val in2 = mapKV.get("colorsrc")
            in2.map(ve => colorsrc.fromJson(ve))

            val in3 = mapKV.get("color")
            in3.map(ve => color.fromJson(ve))

            val in4 = mapKV.get("sizesrc")
            in4.map(ve => sizesrc.fromJson(ve))

            val in5 = mapKV.get("family")
            in5.map(ve => family.fromJson(ve))

          }
          this
        }

      }

      def json(): String = {
        /* §#
         * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/

        val v0 =
          if (font.json().length > 0) "\"font\"" + ":" + font.json() else ""
        val v1 =
          if (alignsrc.json().length > 0) "\"alignsrc\"" + ":" + alignsrc.json()
          else ""
        val v2 =
          if (bgcolor.json().length > 0) "\"bgcolor\"" + ":" + bgcolor.json()
          else ""
        val v3 =
          if (align.json().length > 0) "\"align\"" + ":" + align.json() else ""
        val v4 =
          if (namelength.json().length > 0)
            "\"namelength\"" + ":" + namelength.json()
          else ""
        val v5 =
          if (namelengthsrc.json().length > 0)
            "\"namelengthsrc\"" + ":" + namelengthsrc.json()
          else ""
        val v6 =
          if (bordercolor.json().length > 0)
            "\"bordercolor\"" + ":" + bordercolor.json()
          else ""
        val v7 =
          if (bordercolorsrc.json().length > 0)
            "\"bordercolorsrc\"" + ":" + bordercolorsrc.json()
          else ""
        val v8 =
          if (bgcolorsrc.json().length > 0)
            "\"bgcolorsrc\"" + ":" + bgcolorsrc.json()
          else ""
        val vars: List[String] = List(v0, v1, v2, v3, v4, v5, v6, v7, v8)
        val cleanVars          = vars.filter(_.length > 0).mkString(",\n")
        if (cleanVars.length > 0) {
          // values set
          "{" + cleanVars + "}"
        } else {
          // no values set
          ""
        }
      }

      def fromJson(in: Value): this.type = {
        /* §#
         * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
         * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generateObjects(obj: ObjectDefinition)
         * generateCompound(obj: ObjectDefinition)
         * generate(obj: CompoundDefinition)
         * generate(obj: Either[Error, CompoundDefinition])*/
        def err = throw new RuntimeException(s"Expected an Obj but got $in")
        in.objOpt.fold(err) { mapKV =>
          val in0 = mapKV.get("font")
          in0.map(ve => font.fromJson(ve))

          val in1 = mapKV.get("alignsrc")
          in1.map(ve => alignsrc.fromJson(ve))

          val in2 = mapKV.get("bgcolor")
          in2.map(ve => bgcolor.fromJson(ve))

          val in3 = mapKV.get("align")
          in3.map(ve => align.fromJson(ve))

          val in4 = mapKV.get("namelength")
          in4.map(ve => namelength.fromJson(ve))

          val in5 = mapKV.get("namelengthsrc")
          in5.map(ve => namelengthsrc.fromJson(ve))

          val in6 = mapKV.get("bordercolor")
          in6.map(ve => bordercolor.fromJson(ve))

          val in7 = mapKV.get("bordercolorsrc")
          in7.map(ve => bordercolorsrc.fromJson(ve))

          val in8 = mapKV.get("bgcolorsrc")
          in8.map(ve => bgcolorsrc.fromJson(ve))

        }
        this
      }

    }

    def json(): String = {
      /* §#
       * declareCaseClassJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      val v32 = "\"type\" : \"image\""
      val v0 =
        if (stream.json().length > 0) "\"stream\"" + ":" + stream.json() else ""
      val v1 =
        if (hoverlabel.json().length > 0)
          "\"hoverlabel\"" + ":" + hoverlabel.json()
        else ""
      val v2 =
        if (hovertemplate.json().length > 0)
          "\"hovertemplate\"" + ":" + hovertemplate.json()
        else ""
      val v3 =
        if (colormodel.json().length > 0)
          "\"colormodel\"" + ":" + colormodel.json()
        else ""
      val v4 =
        if (name.json().length > 0) "\"name\"" + ":" + name.json() else ""
      val v5 = if (dy.json().length > 0) "\"dy\"" + ":" + dy.json() else ""
      val v6 =
        if (hovertemplatesrc.json().length > 0)
          "\"hovertemplatesrc\"" + ":" + hovertemplatesrc.json()
        else ""
      val v7 =
        if (hoverinfo.json().length > 0)
          "\"hoverinfo\"" + ":" + hoverinfo.json()
        else ""
      val v8 =
        if (visible.json().length > 0) "\"visible\"" + ":" + visible.json()
        else ""
      val v9 =
        if (uirevision.json().length > 0)
          "\"uirevision\"" + ":" + uirevision.json()
        else ""
      val v10 = if (y0.json().length > 0) "\"y0\"" + ":" + y0.json() else ""
      val v11 =
        if (idssrc.json().length > 0) "\"idssrc\"" + ":" + idssrc.json() else ""
      val v12 =
        if (text.json().length > 0) "\"text\"" + ":" + text.json() else ""
      val v13 =
        if (hoverinfosrc.json().length > 0)
          "\"hoverinfosrc\"" + ":" + hoverinfosrc.json()
        else ""
      val v14 =
        if (hovertextsrc.json().length > 0)
          "\"hovertextsrc\"" + ":" + hovertextsrc.json()
        else ""
      val v15 = if (x0.json().length > 0) "\"x0\"" + ":" + x0.json() else ""
      val v16 =
        if (meta.json().length > 0) "\"meta\"" + ":" + meta.json() else ""
      val v17 =
        if (zmin.json().length > 0) "\"zmin\"" + ":" + zmin.json() else ""
      val v18 =
        if (textsrc.json().length > 0) "\"textsrc\"" + ":" + textsrc.json()
        else ""
      val v19 =
        if (hovertext.json().length > 0)
          "\"hovertext\"" + ":" + hovertext.json()
        else ""
      val v20 = if (uid.json().length > 0) "\"uid\"" + ":" + uid.json() else ""
      val v21 =
        if (zsrc.json().length > 0) "\"zsrc\"" + ":" + zsrc.json() else ""
      val v22 =
        if (opacity.json().length > 0) "\"opacity\"" + ":" + opacity.json()
        else ""
      val v23 = if (ids.json().length > 0) "\"ids\"" + ":" + ids.json() else ""
      val v24 = if (dx.json().length > 0) "\"dx\"" + ":" + dx.json() else ""
      val v25 =
        if (yaxis.json().length > 0) "\"yaxis\"" + ":" + yaxis.json() else ""
      val v26 =
        if (xaxis.json().length > 0) "\"xaxis\"" + ":" + xaxis.json() else ""
      val v27 =
        if (zmax.json().length > 0) "\"zmax\"" + ":" + zmax.json() else ""
      val v28 =
        if (metasrc.json().length > 0) "\"metasrc\"" + ":" + metasrc.json()
        else ""
      val v29 =
        if (customdata.json().length > 0)
          "\"customdata\"" + ":" + customdata.json()
        else ""
      val v30 = if (z.json().length > 0) "\"z\"" + ":" + z.json() else ""
      val v31 =
        if (customdatasrc.json().length > 0)
          "\"customdatasrc\"" + ":" + customdatasrc.json()
        else ""
      val vars: List[String] = List(
        v32,
        v0,
        v1,
        v2,
        v3,
        v4,
        v5,
        v6,
        v7,
        v8,
        v9,
        v10,
        v11,
        v12,
        v13,
        v14,
        v15,
        v16,
        v17,
        v18,
        v19,
        v20,
        v21,
        v22,
        v23,
        v24,
        v25,
        v26,
        v27,
        v28,
        v29,
        v30,
        v31
      )
      val cleanVars = vars.filter(_.length > 0).mkString(",\n")
      if (cleanVars.length > 0) {
        // values set
        "{" + cleanVars + "}"
      } else {
        // no values set
        "{}"
      }
    }

    def fromJson(in: Value): this.type = {
      /* §#
       * declareFromJsonMethod(depth:Int, name:String, typeName: Option[String], members: List[String])
       * generateObj(obj: ObjectDefinition, typeName: String, constants: String, arrays: String, members: Declaration, objs: Declaration)
       * generateCompound(obj: ObjectDefinition)
       * generateObjects(obj: ObjectDefinition)
       * generateCompound(obj: ObjectDefinition)
       * generate(obj: CompoundDefinition)
       * generate(obj: Either[Error, CompoundDefinition])*/
      def err = throw new RuntimeException(s"Expected an Obj but got $in")
      in.objOpt.fold(err) { mapKV =>
        val in0 = mapKV.get("stream")
        in0.map(ve => stream.fromJson(ve))

        val in1 = mapKV.get("hoverlabel")
        in1.map(ve => hoverlabel.fromJson(ve))

        val in2 = mapKV.get("hovertemplate")
        in2.map(ve => hovertemplate.fromJson(ve))

        val in3 = mapKV.get("colormodel")
        in3.map(ve => colormodel.fromJson(ve))

        val in4 = mapKV.get("name")
        in4.map(ve => name.fromJson(ve))

        val in5 = mapKV.get("dy")
        in5.map(ve => dy.fromJson(ve))

        val in6 = mapKV.get("hovertemplatesrc")
        in6.map(ve => hovertemplatesrc.fromJson(ve))

        val in7 = mapKV.get("hoverinfo")
        in7.map(ve => hoverinfo.fromJson(ve))

        val in8 = mapKV.get("visible")
        in8.map(ve => visible.fromJson(ve))

        val in9 = mapKV.get("uirevision")
        in9.map(ve => uirevision.fromJson(ve))

        val in10 = mapKV.get("y0")
        in10.map(ve => y0.fromJson(ve))

        val in11 = mapKV.get("idssrc")
        in11.map(ve => idssrc.fromJson(ve))

        val in12 = mapKV.get("text")
        in12.map(ve => text.fromJson(ve))

        val in13 = mapKV.get("hoverinfosrc")
        in13.map(ve => hoverinfosrc.fromJson(ve))

        val in14 = mapKV.get("hovertextsrc")
        in14.map(ve => hovertextsrc.fromJson(ve))

        val in15 = mapKV.get("x0")
        in15.map(ve => x0.fromJson(ve))

        val in16 = mapKV.get("meta")
        in16.map(ve => meta.fromJson(ve))

        val in17 = mapKV.get("zmin")
        in17.map(ve => zmin.fromJson(ve))

        val in18 = mapKV.get("textsrc")
        in18.map(ve => textsrc.fromJson(ve))

        val in19 = mapKV.get("hovertext")
        in19.map(ve => hovertext.fromJson(ve))

        val in20 = mapKV.get("uid")
        in20.map(ve => uid.fromJson(ve))

        val in21 = mapKV.get("zsrc")
        in21.map(ve => zsrc.fromJson(ve))

        val in22 = mapKV.get("opacity")
        in22.map(ve => opacity.fromJson(ve))

        val in23 = mapKV.get("ids")
        in23.map(ve => ids.fromJson(ve))

        val in24 = mapKV.get("dx")
        in24.map(ve => dx.fromJson(ve))

        val in25 = mapKV.get("yaxis")
        in25.map(ve => yaxis.fromJson(ve))

        val in26 = mapKV.get("xaxis")
        in26.map(ve => xaxis.fromJson(ve))

        val in27 = mapKV.get("zmax")
        in27.map(ve => zmax.fromJson(ve))

        val in28 = mapKV.get("metasrc")
        in28.map(ve => metasrc.fromJson(ve))

        val in29 = mapKV.get("customdata")
        in29.map(ve => customdata.fromJson(ve))

        val in30 = mapKV.get("z")
        in30.map(ve => z.fromJson(ve))

        val in31 = mapKV.get("customdatasrc")
        in31.map(ve => customdatasrc.fromJson(ve))

      }
      this
    }

  }

  object layoutAttributes extends JSON {
    def json(): String                 = ""
    def fromJson(in: Value): this.type = this
  }

}
