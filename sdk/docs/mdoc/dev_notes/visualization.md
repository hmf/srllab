# Visualization of Data

## References

1. [The Data Visualisation Catalogue ](https://datavizcatalogue.com)
1. [Task-based effectiveness of basic visualizations](https://blog.acolyer.org/2019/10/25/task-based-effectiveness-of-basic-visualizations/)
   1. [Task-Based Effectiveness of Basic Visualizations; Bahador Saket, Alex Endert, Cagatay Demiralp](https://arxiv.org/abs/1709.08546)
   1. [Low-Level Components of Analytic Activity in Information Visualization Robert; Amar, James Eagan, and John Stasko](https://www.cc.gatech.edu/~stasko/papers/infovis05.pdf)









## Note Taking

1. [QOwnNotes](https://github.com/pbek/QOwnNotes)

