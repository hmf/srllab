package webkit

import java.awt.image.DataBuffer

import better.files.File
import javax.imageio.ImageIO

object ImageTestUtils {

  private def capture(htmlIn: File, imageOut: File): Boolean = {
    val in = s"""file://${htmlIn.path.toAbsolutePath.toString}"""
    val image = HeadlessWebKit.capture(in, imageOut)
    image
  }

  def captureAndCompareImage(htmlIn: File, imageOut: File, goldImage: File, result: Boolean = true): Unit = {
    assert(htmlIn.exists)

    val image = capture(htmlIn, imageOut)

    assert(image == result)
    assert(imageOut.exists == result)
    if (result) assert(goldImage.exists == result)

    if (image)
      assert(ImageTestUtils.equals(imageOut, goldImage) == result)
    else
      assert(!result)
  }


  def captureAndDiffImage(htmlIn: File, imageOut: File, goldImage: File, maxDiff: Double, result: Boolean = true): Unit = {
    assert(htmlIn.exists)

    val image = capture(htmlIn, imageOut)

    assert(image == result)
    assert(imageOut.exists == result)
    if (result) assert(goldImage.exists == result)

    if (image) {
      val (diff, total) = ImageTestUtils.diff(imageOut, goldImage)
      val ratio = diff / (total * 1.0)
      println(s"diff = $diff; total = $total; ratio = $ratio ; maxDiff = $maxDiff")
      assert( ratio <= maxDiff)
    } else
      assert(!result)
  }

  def equals(fileA: File, fileB: File): Boolean = {
    val biA = ImageIO.read(fileA.toJava)
    val dbA: DataBuffer = biA.getData.getDataBuffer
    val sizeA = dbA.getSize

    val biB = ImageIO.read(fileB.toJava)
    val dbB = biB.getData.getDataBuffer
    val sizeB = dbB.getSize

    // compare data-buffer objects
    var r = true
    if (sizeA == sizeB) {
      var i = 0
      while (i < sizeA) {
        if (dbA.getElem(i) != dbB.getElem(i)) {
          i = sizeA
          r = false
        }
        i += 1
      }
    }
    else
      r = false
    r
  }

  def diff(fileA: File, fileB: File): (Long, Long) = {
    val biA = ImageIO.read(fileA.toJava)
    val dbA: DataBuffer = biA.getData.getDataBuffer
    val sizeA = dbA.getSize

    val biB = ImageIO.read(fileB.toJava)
    val dbB = biB.getData.getDataBuffer
    val sizeB = dbB.getSize

    // compare data-buffer objects
    var diff = 0L
    var total = 0L
    if (sizeA == sizeB) {
      var i = 0
      while (i < sizeA) {
        diff += (if (dbA.getElem(i) != dbB.getElem(i)) 1 else 0)
        total += 1
        i += 1
      }
    }
    else
      (Long.MaxValue, Long.MaxValue)
    (diff, total)
  }


}
