package splotly

import ujson.Value

// TODO: override toString to generate an object with methods?
// TODO: add check for color values/formats
case class PColor(dflt: Option[String],
                  description: Option[String]
                ) extends JSON {

  private var value: Option[String] = None

  def setValue(string: String): Unit = value = Some(string)
  def setDefault(): Unit = value = dflt
  def getValue: Option[String] = value

  override def json(): String = value.fold("")(e => s""""${e.toString}"""")

  override def fromJson(in: Value): PColor.this.type = {
    in match {
      case ujson.Str(v) => setValue(v)
      case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
    }
    this
  }
}
