package splotly

import ujson.Value

// TODO: override toString to generate an object with methods?
case class PAngle(dflt: Option[String],
                  description: Option[String]
                ) extends JSON {

  private var value: Option[Double] = None
  private val none: Option[Double] = None

  def setValue(d: Double): Unit = value = Some(d)

  def setDefault(): Unit = {
    value = dflt.fold(none) { f =>
      try {
        Some(f.toDouble)
      }
      catch {
        case e: Throwable =>
          if (f.equals("auto") || f.equals("")) none else throw e
      }
    }
  }
  def getValue: Option[Double] = value

  override def json(): String = value.fold(""){e =>
    if (e.isValidInt) e.toInt.toString else e.toString
  }

  override def fromJson(in: Value): PAngle.this.type = {
    in match {
      case ujson.Num(v) => setValue(v)
      case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
    }
    this
  }
}
