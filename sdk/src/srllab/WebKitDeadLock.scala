package srllab

import java.util.ServiceLoader

import mdoc.PostModifier
import webkit.HeadlessWebKit


object WebKitBlocker {
  def initWebKit():Unit = {
    println(s"initWebKit start $this @ ${Thread.currentThread().getId}")
    HeadlessWebKit.launchBackground(Array[String]())
    println(s"initWebKit finished $this @ ${Thread.currentThread().getId}")
  }
}

class WebKitBlocker extends {
  println(s"Blocker start $this @ ${Thread.currentThread().getId}")
  WebKitBlocker.initWebKit()
  println(s"Blocker init finished $this @ ${Thread.currentThread().getId}")

  def sayHello(): Unit = println("Hello\n")
}


/**
 * mill sdk.compile
 * mill sdk.run
 * mill sdk.{compile, run}
 * mill --watch sdk.run
 *
 * mill -i sdk.console
 * mill -i sdk.repl
 *
 * mill -i sdk.runMain srllab.WebKitDeadLock
 * mill --watch sdk.runMain srllab.WebKitDeadLock
 *
 */
object WebKitDeadLock {

  def noBlock(): Unit = {
    // Initialize the app
    val b = new WebKitBlocker
    b.sayHello()
    // Give JavFX time to initialize
    //Thread.sleep(200)
    // Wait for start
    HeadlessWebKit.waitStart()
    // Stop app
    HeadlessWebKit.stop()
  }

  def scalaReflectNoBlock() : Unit = {
    // Initialize the app
    import scala.reflect.runtime.{universe => ru}
    val m = ru.runtimeMirror(getClass.getClassLoader)
    val classWebKitBlocker = ru.typeOf[WebKitBlocker].typeSymbol.asClass
    val cm = m.reflectClass(classWebKitBlocker)
    val ctor = ru.typeOf[WebKitBlocker].decl(ru.termNames.CONSTRUCTOR).asMethod
    val ctorm = cm.reflectConstructor(ctor)
    val c = ctorm()
    //c.sayHello()
    val blockerTermSymb = ru.typeOf[WebKitBlocker].decl(ru.TermName("sayHello")).asMethod
    val im = m.reflect(c)
    val blockerFieldMirror = im.reflectMethod(blockerTermSymb)
    blockerFieldMirror.apply(null)
    // Give JavFX time to initialize
    //Thread.sleep(200)
    // Wait for start
    HeadlessWebKit.waitStart()
    // Stop app
    HeadlessWebKit.stop()
  }

  def serviceLoaderInit(): Unit = {
    import scala.jdk.CollectionConverters._
    val posts : List[PostModifier] = ServiceLoader.load(classOf[PostModifier], this.getClass.getClassLoader).iterator().asScala.toList
    println(posts.mkString(","))
    val post = posts.head
    println(s"post.name = ${post.name}")

    // May not have been started, may get a null pointer exception
    // (variable uninitialized)
    HeadlessWebKit.stop()
  }


  def waitDeadlock(): Unit = {
    //HeadlessWebKit.init()
    HeadlessWebKit.launchBackground(Array[String]())
    // Waiting on start
    HeadlessWebKit.waitStart()
    // Now we can stop
    HeadlessWebKit.stop()
  }

  def serviceLoaderInitBypass(): Unit = {
    import scala.jdk.CollectionConverters._
    val posts : List[PostModifier] = ServiceLoader.load(classOf[PostModifier], this.getClass.getClassLoader).iterator().asScala.toList
    println(posts.mkString(","))
    val post = posts.head
    println(s"post.name = ${post.name}")

    // Waiting on start
    HeadlessWebKit.waitStart()
    // Now we can stop
    HeadlessWebKit.stop()
  }

  def main(args: Array[String]): Unit = {

    // Works correctly
    //noBlock()
    /*
      Blocker start srllab.WebKitBlocker@3b938003 @ 1
      initWebKit start srllab.WebKitBlocker$@b62fe6d @ 1
      Start Monocle
      Initiated Monocle
      initWebKit finished srllab.WebKitBlocker$@b62fe6d @ 1
      Blocker init finished srllab.WebKitBlocker@3b938003 @ 1
      Hello

      HeadlessWebKit Init -------------------- 1
      HeadlessWebKit Start --------------------
      ---------------------- HeadlessWebKit Start
     */

    // Same as above and works
    //scalaReflectNoBlock()
    /*
      Blocker start srllab.WebKitBlocker@33d05366 @ 1
      initWebKit start srllab.WebKitBlocker$@4b770e40 @ 1
      Start Monocle
      Initiated Monocle
      initWebKit finished srllab.WebKitBlocker$@4b770e40 @ 1
      Blocker init finished srllab.WebKitBlocker@33d05366 @ 1
      Hello

      HeadlessWebKit Init -------------------- 1
      HeadlessWebKit Start --------------------
      ---------------------- HeadlessWebKit Start
     */

    // Test blocks, notice how the Monocle initialization block does not execute
    serviceLoaderInit()
    /*
      PlotlyModifier init
      Start Monocle
      Initiated Monocle
      HeadlessWebKit Init -------------------- 1
      HeadlessWebKit Start --------------------
      mdocs.PlotlyModifier@4f80542f
      post.name = plotly

      ---------------------- HeadlessWebKit Start
     */

    // Ok if the waitStart is not synchronized
    //waitDeadlock()

    // The MDoc PostModifier correctly initializes jFX and OpenFX Monocle
    // The call to the WebKit may occur before the start has finished
    // This call must not block the start
    //serviceLoaderInitBypass()

    println()
  }

}
