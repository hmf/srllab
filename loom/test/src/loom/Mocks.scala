package loom

import better.files.Dsl._
import better.files._

object Mocks {

  val baseMockDir: File = cwd / "webkit" / "test" / "image" / "mocks"
  val baseDir: File = cwd / "webkit" / "test" / "image" / "baselines"

  case class TestFiles(fileName: String) {
    val htmlFile: File = baseMockDir / s"$fileName.html"
    val pngFile = s"$fileName.png"
    val gifFile = s"$fileName.gif"
    val tifFile = s"$fileName.tif"
    val jpegFile = s"$fileName.jpeg"
    val bmpFile = s"$fileName.bmp"
    val wbmpFile = s"$fileName.wbmp"
    val pngFileTmp: File = webkit.Utils.temporaryDirFile(pngFile)
    val gifFileTmp: File = webkit.Utils.temporaryDirFile(gifFile)
    val tifFileTmp: File = webkit.Utils.temporaryDirFile(tifFile)
    val jpegFileTmp: File = webkit.Utils.temporaryDirFile(jpegFile)
    val bmpFileTmp: File = webkit.Utils.temporaryDirFile(bmpFile)
    val wbmpFileTmp: File = webkit.Utils.temporaryDirFile(wbmpFile)
    val goldPng: File = baseDir / pngFile
    val goldGif: File = baseDir / gifFile
    val goldTif: File = baseDir / tifFile
    val goldJpeg: File = baseDir / pngFile
    val goldBmp: File = baseDir / gifFile
    val goldWbmp: File = baseDir / tifFile
  }

  object SVGcolor {
    val files: TestFiles = TestFiles("test1")
  }

  object SVG_BW {
    val files: TestFiles = TestFiles("test2")
  }

  object SVG_Plot {
    val files: TestFiles = TestFiles("test3")
  }

}
