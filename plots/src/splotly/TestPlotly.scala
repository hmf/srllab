package splotly

/**
 * TODO: BUG 5 - the variable should use parent to return full json line with member label?
 *
 * mill mill.scalalib.GenIdea/idea
 *
 * mill mdocs.compile
 * mill mdocs.run
 * mill mdocs.run noGraal
 *
 * mill mdocs.{compile, run}
 * mill --watch mdocs.run
 *
 * mill -i mdocs.console
 * mill -i mdocs.repl
 *
 * mill -i plots.runMain splotly.TestPlotly
 * mill --watch mill -i splots.runMain plotly.TestPlotly
 * ./jfxmill.sh -i splots.runMain plotly.TestPlotly
 */
object TestPlotly {

  def violinTest1(): Unit = {
    val csv = CSV("./plots/data/violin_data.csv")
    val (nrows, ncols) = csv.size
    //println(s"size(rows, columns) = ($nrows, $ncols)")
    //println(s"header = ${csv.header.mkString(",")}")
    val total_bill = csv("total_bill")
    //println(s"total_bill = ${csv("total_bill").map(_.mkString(","))}")
    //println(s"json total_bill = ${csv.json("total_bill")}")
    //val y = csv.json("total_bill").fold("[]")(e => e)
    // TODO: add this to CSV class
    val y = total_bill.map(_.map(_.toDouble))

    val v1 = splotly.gen.Violin()

    v1.attributes.name.setValue("v1")
    v1.attributes.span.boolean_0(true)
    //v1.attributes.span.double_1()
    //v1.attributes.span.string_1("str")
    //v1.attributes.visible.legendonly()

    y.foreach( d => v1.attributes.y.setValue(d) )
    v1.attributes.points.false_()
    v1.attributes.box.visible.setValue(true)
    v1.attributes.line.color.setValue("black")
    v1.attributes.meanline.visible.setValue(true)
    v1.attributes.fillcolor.setValue("#8dd3c7")
    v1.attributes.opacity.setValue(0.6)
    v1.attributes.x0.setValue("Total Bill")
    println(s"v1.attributes.box.visible.json(true) = ${v1.attributes.box.visible.json()}")
    v1.layoutAttributes.violinmode.group()

    println(v1.attributes.json())
    println(v1.layoutAttributes.json())

    // divId: String,
    //            traces: Seq[Trace],
    //            layout: String,
    //            title: Option[String], // TODO: can we get it from Trace?
    //            path: Option[String],
    //            useCdn: PlotlyJS = UseLatestCDN,
    //            openInBrowser: Boolean = true,
    //            overwrite: Boolean = true
    //
    // TODO: LPlotly.plot("div1", Seq(v1), v1.layoutAttributes.json())

    println("Test 1")
    val tmp = splotly.gen.Violin()
    val test1In = ujson.read(v1.layoutAttributes.json())
    //val reader1 = ujson.Readable.fromString("""{"violinmode":"group"}""")
    //val test1In = ujson.read(reader1)
    //val test1In = ujson.read("""{"violinmode":"group"}""")
    val test1 = ujson.read("{}")
    val layout: tmp.layoutAttributes.type = tmp.layoutAttributes.fromJson( test1In )
    println(layout.json())
    val test1Out = ujson.read(layout.json())
    println(s"test 1a : ${Utils.sameJson(test1Out,test1In)}")
    println(s"test 1b : ${Utils.sameJson(test1Out,test1)}")

  }

  def main(args: Array[String]): Unit = {

    violinTest1()

  }

}
