package splotly

import ujson.Value

case class PBool(dflt: Option[Boolean],
                 description: Option[String]
                ) extends JSON {

  private var value: Option[Boolean] = None

  def setValue(boolean: Boolean): Unit = value = Some(boolean)
  def setDefault(): Unit = value = dflt
  def getValue: Option[Boolean] = value

  override def json(): String = value.fold("")(_.toString)

  override def fromJson(in: Value): PBool.this.type = {
    in match {
      case ujson.Bool(v) => setValue(v)
      case _ => throw new RuntimeException(s"${getClass.getCanonicalName}: unexpected value $in")
    }
    this
  }
}
