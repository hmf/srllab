# Plotting

## Introduction

In order to show plots we have to load data and generate the plots. Plots may be
rendered as images, which can be embedded using the Markdown image link. 
Additionally we may use (possibly interactive) JS plots that can either be 
statically linked from an HTML file or embedded via an `<iFrame>` element. We 
considered several options that included (Java options were not considered):

* [Plotly.js](https://plot.ly/javascript/) is a Javascript library that 
supports 2D and 3D plots that can be embedded into HTML pages. It is a mature 
and complete library with commercial offerings based on the [D3JS](https://d3js.org/) 
and [StackGL](http://stack.gl/). StackGL is an [open source](https://github.com/stackgl)
library that provides WebGL components to JS scripts. D3JS has simpler wrapper 
libraries such as [Dimple](http://dimplejs.org/) and [NvD3](http://nvd3.org/) 
but these were not analysed. A [JS facade](https://github.com/spaced/scala-js-d3) 
also exists for [Scala JS](https://www.scala-js.org/). Note that the open 
source version of Plotly only offers saving plots to the 
[PNG format](https://plot.ly/javascript/static-image-export/). 

* Plotly's [Scala](https://plot.ly/scala/) client API. This API can be easily 
accessed via a Scala [library](https://plot.ly/scala/getting-started/) however
it is a client library that needs a working Internet connection. Although it is
referenced in the Plotly site, is is a [3rd party library](https://github.com/facultyai/scala-plotly-client).  

* [Plotly-scala](http://plotly-scala.org/) is an [open source library](https://github.com/alexarchambault/plotly-scala)
that outputs JSON that can be passed directly to [Plotly.js](https://plot.ly/javascript/). 
Its API is very similar to the plotly.js API, so that one can its documentation.
The library was developed to work in a Scala REPL like [Ammonite](https://github.com/lihaoyi/Ammonite) 
and [Almond](https://almond.sh/), which is an [open source](https://github.com/almond-sh/almond) 
Scala kernel for [Jupyter](https://jupyter.org/) that is based on the Scalameta [browser](https://github.com/scalameta/metabrowse). 
For an overview on Almond see this [video](https://www.youtube.com/watch?v=pgVtdveelAQ).  
 
* [EvilPlot](https://cibotech.github.io/evilplot/) is an [open source](https://github.com/cibotech/evilplot) 
library allow for the construction of plots a little like GGPlot2. Not 
considered because it is unmaintained.   

* The [Vegas](https://github.com/vegas-viz/Vegas) base on [Vega-Lite](https://vega.github.io/vega-lite/) 
is GGPlot2 like plotting library for Scala that is supposed to be Scala's 
MatPlotLib. However it is currently unsupported and the latest pull requests to
upgrade it to Scala 2.12 have not been accepted. It does have an interesting 
rendering interface that may facilitate its use in markdown. [Vega-lite source](https://github.com/vega/vega-lite) 
code is however being actively updated.  

* [New Scala Plotting Library](https://pityka.github.io/nspl/) is [active](https://github.com/pityka/nspl).
The author has also taken upon himself to maintain [Scala Saddle](https://saddle.github.io/)
and [forked the source](https://github.com/pityka/saddle) form [here](https://github.com/saddle/saddle). 
See also [this](https://repo1.maven.org/maven2/io/github/pityka/saddle-core-fork_2.12/). 

* The [Scala Chart Library](https://github.com/wookietreiber/scala-chart) is 
based on [JFreeChart](http://jfree.org/jfreechart/). It seems to be in 
maintenance mode.

* The [Wisp is Scala Plotting](https://github.com/quantifind/wisp) seems to be dead.

* The Scala [Breeze-Viz](https://github.com/scalanlp/breeze/wiki/Quickstart#breeze-viz) 
library from the Scala [Breeze ML](https://github.com/scalanlp/breeze/wiki/Quickstart) 
library is not supported anymore. 

* The Java [zyY3d](http://www.jzy3d.org/) is an open source API for 3d charts. 
We list here as an interesting fall back in case we need some better performing 
non-JS library with 2D and 3D capabilities. 

For more information on plotting libraries for Scala see [this](https://users.scala-lang.org/t/any-scala-plotting-libraries-with-3d-plots-scientific-libraries/2995/21) discussion.

## Plotly Scripts 

In order to use a Plotly.js plot, we can create an HTML file with the required 
script, which can then be linked to via a URL and viewed. [This file](test.html) 
is a link to an interactive plot that is exemplified in the documentation 
[here](https://plot.ly/javascript/line-charts/#line-and-scatter-plot). By 
clicking on it we can see the plot.

The script can also be embedded directly using the `<iFrame>` HTML element. The 
following is an example of what can be used (for debugging purposes use the 
attribute `border:1px solid red` to see the borders):

``` 
<div style="width:100%; height:100%; text-align:center; border:1px solid red">
  <iframe id="plotlyFrame" src="test.html" height="300px" width="100%" style="border:none;" scrolling="no"></iframe>
  <p style="text-align:center; font-weight: bold;">
   Figure 1: Embedded scatter plot example via iFrame 
  </p>
</div>
```
 
Note that we can also use CSS to [format](https://www.w3schools.com/html/html_iframe.asp) 
the embedded element either directly or using tags.  
 
<div style="width:100%; height:100%; text-align:center">
 <iframe id="plotlyFrame" src="test.html" height="500px" width="900px"></iframe> 
 <p style="text-align:center; font-weight: bold;">
  Figure 1: Embedded scatter plot example via iFrame and HTML file 
 </p>
</div>

Note that to use these embedded HTML snippets in the Markdown file, [Laika's](https://github.com/planet42/Laika) 
`withRawContent` must be used. We can also include HTML files 
[with a script](https://www.w3schools.com/howto/howto_html_include.asp). 
However this will not work due to the Cross-Origin Resource Sharing ([CORS](https://developer.mozilla.org/en-US/docs/Glossary/CORS))
that is explained [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS). 
Specifically when we try to load the file locally we get this 
[error](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp).

**IMPORTANT:** When using Laika's `withRawContent` we will not be able to 
generate other formats such as PDF. This is because these elements cannot be 
converted and rendered in anything but HTML. In these cases we must generate 
and embed the lots as images (JPEG, PNG, SVG, PS or even PDF). To generate
PDFs do not embed HTML.  

Next we see how we can embed (or inline) a Plotly graph using the `<script>` 
element to load an external script file. **Make sure the script and the `div` 
use the same id (in this case we have `id="myDiv2"`).** A note on sizing: in 
order to ensure that the Plotly `div` centers the plot one can use 
`margin:0 auto;`. For debugging purposes use the attribute 
`border:1px solid red` to see the borders. The following HTML snippet in the 
Markdown document: 

```html
<div style="width:100%; height:100%; border:1px solid red">
    <div id="myDiv2"
         style="margin:0 auto; height:500px; width:900px; border:1px solid black;">
        <!-- Fig 2: Pl margin:0 auto; tly chart will be drawn inside this DIV -->
    </div>
    <script src="scatterplotexample.js"></script>
    <p style="text-align:center; font-weight: bold;">
        Figure 2: Embedded scatter plot example via a script file
    </p>
</div>
```

generates this output:

<div style="width:100%; height:100%;">
    <div id="myDiv2"
         style="margin:0 auto; height:500px; width:900px; border:1px solid black;">
        <!-- Plotly chart will be drawn inside this DIV -->
    </div>
    <script src="scatterplotexample.js"></script>
    <p style="text-align:center; font-weight: bold;">
        Figure 2: Embedded scatter plot example via a script file
    </p>
</div>

Another options is to embed the plot directly however Laika is [escaping the colon](https://github.com/planet42/Laika/issues/93)
and this has been corrected, but it has yet to be released. As a workaround the technique above can be used to 
load an external script or alternatively the inline HTML elements and script could be generated dynamically 
using [MDocs pass-through modifier](laika.md#using-mdoc-passthrough) possibly in combination with for example
[ScalaTags](https://github.com/lihaoyi/scalatags). See the section on [Plotly Scala](#plotly-Scala) for 
more information.

```html
<div style="width:100%; height:100%; text-align:center;">
  <div id="myDiv3"><!-- Plotly chart will be drawn inside this DIV --></div>
  <script type="text/javascript" defer>
    <!-- JAVASCRIPT CODE GOES HERE -->
        var trace1 = {
          x: [1, 2, 3, 4],
          y: [10, 15, 13, 17],
          mode: 'markers',
          type: 'scatter'
        };
                                   
        var trace2 = {
          x: [2, 3, 4, 5],
          y: [16, 5, 11, 9],
          mode: 'lines',
          type: 'scatter'
        };
                                   
        var trace3 = {
          x: [1, 2, 3, 4],
          y: [12, 9, 15, 12],
          mode: 'lines+markers',
          type: 'scatter'
        };
                                   
        var data = [trace1, trace2, trace3];
                                   
        Plotly.newPlot('myDiv3', data);
  </script>
  <p style="text-align:center; font-weight: bold;">
    Figure 3: Embedded scatter plot example via an inlined script
  </p>
</div>
```

generates this output:

<div style="width:100%; height:100%; text-align:center;">
  <div id="myDiv3"><!-- Plotly chart will be drawn inside this DIV --></div>
  <script type="text/javascript" defer>
    <!-- JAVASCRIPT CODE GOES HERE -->
        var trace1 = {
          x: [1, 2, 3, 4],
          y: [10, 15, 13, 17],
          mode: 'markers',
          type: 'scatter'
        };
                                   
        var trace2 = {
          x: [2, 3, 4, 5],
          y: [16, 5, 11, 9],
          mode: 'lines',
          type: 'scatter'
        };
                                   
        var trace3 = {
          x: [1, 2, 3, 4],
          y: [12, 9, 15, 12],
          mode: 'lines+markers',
          type: 'scatter'
        };
                                   
        var data = [trace1, trace2, trace3];
                                   
        Plotly.newPlot('myDiv3', data);
  </script>
  <p style="text-align:center; font-weight: bold;">
    Figure 3: Embedded scatter plot example via an inlined script
  </p>
</div>

We suggested that the plot sizes (*width and height*) be defined in percentage 
(`%`) and not pixels (`px`). It is **important** to note that **embedding 
(inlining HTML) does not allow using Laika to generate PDF files**. In these 
cases stick to creating a `PNG` (or other static format) file and embed that 
into the document using Markdown only. Embedding files in the Markdown file 
also prevents us from using dynamic (reactive) plots. We consider in the future
[Mill](https://github.com/lihaoyi/mill) and MDoc support to use flags that 
can be used to generate dynamic or static images based on whether or not we 
want to generate an HTML pages or a PDF document.    

Finally we can also embed (or inline) JavaScript code to 
[save and view static images](https://plot.ly/javascript/static-image-export/). 
However this requires care indicating were the files need to be saved. See the 
section on [Plotly Scala](#plotly-Scala) for an alternate solution. **It also 
means that we have to have some way of executing the JS and use those results
before using those sme results in the Markdown source**. 

## Plotly-Scala

[Plotly-Scala](https://github.com/alexarchambault/plotly-scala) is a Scala library 
able to output JSON that can be passed to [Plotly.js](https://plot.ly/javascript/). 
The following is a code snippet in Scala that is used to generate a JavaScript 
code snippet with a dynamic plot.  

```scala mdoc:reset:silent
    import plotly._, layout._, Plotly._
    
    val labels = Seq("Banana", "Banano", "Grapefruit")
    val valuesA = labels.map(_ => util.Random.nextGaussian())
    val valuesB = labels.map(_ => 0.5 + util.Random.nextGaussian())
    
    val traces = Seq(
      Bar(labels, valuesA, name = "A"),
      Bar(labels, valuesB, name = "B")
    )
    val playout = Layout()
    val jsSnippet = Plotly.jsSnippet("myDiv4", traces, playout)
```

The `jsSnippet` variable contains the JavaScript with the plot. The MDoc 
`passthrough` modifier can be used to embed the JavaScript that is dynamically
generated during parsing. For more information see [MDocs pass-through modifier](laika.html#using-mdoc-passthrough). 
The following code snippet:

```
val snippet1 = s"""
<div style="width:100%; height:100%;">
    <div id="myDiv4"
         style="margin:0 auto; height:500px; width:900px; border:1px solid black;">
    </div>
    <script>
      $jsSnippet
    </script>
    <p style="text-align:center; font-weight: bold;">
        Figure 4: Embedded bar plot example via a Plotly-Scala generated script 
    </p>
</div>

println(snippet1)
"""
```

Generates the following output: 

```scala mdoc:passthrough

val snippet1 = s"""
<div style="width:100%; height:100%;">
    <div id="myDiv4"
         style="margin:0 auto; height:500px; width:900px; border:1px solid black;">
    </div>
    <script>
      $jsSnippet
    </script>
    <p style="text-align:center; font-weight: bold;">
        Figure 4: Embedded bar plot example via a Plotly-Scala generated script 
    </p>
</div>
"""
println(snippet1)
```

The alternate and possibly better way to use use Plotly is to use the MDoc 
Plotly utilities. This uses MDoc's `PostModifier` to obtain the filename of 
the element we want to generate. Depending on the file's extension a file
with the corresponding file type is generated. The following sections show 
what options are available:       

### Embedding to an HTML file 

The example code is shown below. The `mdoc:plotly:html` is used by MDoc and the 
MDoc Plotly PostModifier to process the code fence. The `mdoc` indicate that the 
code fence is processed by MDoc. The `plotly` section indicates that it will be
processed by the MDoc Plotly PostModifier. The file extension `html` indicates 
that we want to generate a HTML file containing the plot. We use Scala-plotly 
to generate the HTML file and return an `IFrame` in a `div` that includes this 
HTML file.  

The `:passthrough` modifier is used to output the code example verbatim as a 
code block. This allows us to show the code that is used to generate the 
corresponding plot. 
 
The Modifler's `ScalaPlotly` indicates that we want to generate the Plotly plot 
with the layout `playout` and the Plotly `traces`. It also sets the HTML `div`
element to contain the element name `sample5` and the plot's title. Note that
it is imperative that the `div` name be unique in any given Markdown document 
(file).        

```scala mdoc:plotly:html:passthrough
    import mdocs.ScalaPlotly
    import mdocs.Div

    ScalaPlotly(traces, playout, 
                 Div("Figure 5: Embedded bar plot example via a Plotly-Scala generated script", 
                     "sample5"))
```

### Embedding to an HTML snippet with a JS file

The `mdoc:plotly:hjs` is used by MDoc and the MDoc Plotly PostModifier to 
process the code fences. The `mdoc` indicate that the code fence is processed 
by MDoc. The `plotly` section indicates that it will be processed by the MDoc 
Plotly PostModifier. The file extension `hjs` indicates that we want to generate 
a JS file containing the plot. We use Scala-plotly to generate the JS snippet 
file and return a `div` that includes this JS script file.  

Unlike the example above we also have a `:debug` modifier that is used to 
output the generated code (not the source code we use in the source file). 
 
The example code is shown below. The Modifler's `ScalaPlotly` indicates that 
we want to generate the Plotly plot with the layout `playout` and the Plotly 
`traces`. It also sets the HTML `div` element to contain the element name 
`sample6` and the plot's title. Note that it is imperative that the `div` name
be unique in any given Markdown document (file).        

```scala mdoc:plotly:hjs:debug
    ScalaPlotly(traces, playout, 
                 Div( "Figure 6: Embedded bar plot example via a Plotly-Scala generated script", 
                      "sample6"))
```

### Embedding to an HTML snippet with JS code


The `mdoc:plotly:.js` is used by MDoc and the MDoc Plotly PostModifier to 
process the code fence. The `mdoc` indicate that the code fence is processed by
MDoc. The `plotly` section indicates that it will be processed by the MDoc 
Plotly PostModifier. The file extension `js` indicates that we want to generate
a JS script that generates the plot. We use Scala-plotly to generate the JS 
snippet and return a `div` that embeds this JS script.  
 
Note that if we use both the `:debug` and the `:passthrough` then the debug 
modifier takes precedence an only the generated code is shown. Below the 
only the debug information will be shown.  

The `Div` construct can be used to assign properties to the HTML `div` element.
We can add a title, name the div and indicate its the height and width. So far 
we set the div name explicitly but this is not required. Below we elide this
information.  
 
The Modifler's `ScalaPlotly` indicates that we want to generate the Plotly plot 
with the layout `playout` and the Plotly `traces`. It also sets the HTML `div`
element to contain the element name `sample7` and the plot's title. Note that
it is imperative that the `div` name be unique in any given Markdown document 
(file).        

```scala mdoc:plotly:js:debug:passthrough
    ScalaPlotly(traces, playout, 
                 Div("Figure 7: Embedded bar plot example via a Plotly-Scala generated script"))
```



**TODO:** Use MDoc's PostModifier to get the output directory. Write the file 
to that directory. 

### Linking to an PNG image

This is an example of generating a file image.
 

```scala mdoc:plotly:png
    ScalaPlotly(traces, playout, 
                 Div( "Figure 8: Embedded bar plot example via a Plotly-Scala generated script"))
```

### Linking to an JPEG image


### Linking to an SVG image

```scala mdoc:plotly:svg
    ScalaPlotly(traces, playout, 
                 Div( "Figure 9: Embedded bar plot example via a Plotly-Scala generated script", 
                      "sample9"))
```



[Plotly streaming](https://plot.ly/javascript/streaming/)

## Vega

### Vega verss Plotly

1. Less plot types available
1. No 3D plots
1. No WebGL support

1. Easy generation of static images via JS calls
1. Easy embedding JS call  


### Vega Static Images

1. [vega-cli](https://github.com/vega/vega/tree/master/packages/vega-cli)
1. [JSON schema for Vega and Vega-Lit](https://github.com/vega/schema)
  1. [JSON Schema docs](https://vega.github.io/schema/) 
  
  
## Data Frame Integration

1. https://github.com/pityka/saddle/
  
  
## Graphics Files Support

1. http://openimaj.org/
1. https://robcast.github.io/digilib/java-settings.html
  1. https://robcast.github.io/digilib/
1. http://boofcv.org/index.php?title=Main_Page
1. https://github.com/bytedeco/javacv
1. https://algart.net/java/AlgART/
1. https://github.com/zxing/zxing
1. https://square.github.io/picasso/
1. http://marvinproject.sourceforge.net/en/index.html
1. https://github.com/rkalla/imgscalr

1. https://github.com/sksamuel/scrimage
1. https://imagej.net/ImgLib2
1. https://commons.apache.org/proper/commons-imaging/
1. https://imagej.nih.gov/ij/
1. https://github.com/jai-imageio/jai-imageio-core
1. https://github.com/techblue/jmagick


1. https://github.com/facebook/rebound 