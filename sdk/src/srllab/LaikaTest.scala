package srllab

import java.nio.file.Paths
import better.files._
import File._
import java.io.{File => JFile}

import cats.effect.{Blocker, ContextShift, IO}
import laika.ast.MessageLevel
import laika.markdown.github.GitHubFlavor
import laika.io.model.RenderedTreeRoot
import laika.api.{MarkupParser, Renderer, Transformer}
import laika.format.{AST, EPUB, HTML, Markdown, PDF, ReStructuredText, XSLFO}


/**
 * mill sdk.compile
 * mill sdk.run
 * mill sdk.{compile, run}
 * mill --watch sdk.run
 *
 * mill -i sdk.console
 * mill -i sdk.repl
 *
 * mill -i sdk.runMain srllab.LaikaTest
 * mill --watch sdk.runMain srllab.LaikaTest
 *
 *
 */
object LaikaTest {

  def test2(): RenderedTreeRoot[IO] = {

    import laika.io.implicits._
    import scala.concurrent.ExecutionContext
    import java.util.concurrent.Executors
    implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

    val blocker = Blocker.liftExecutionContext(
      ExecutionContext.fromExecutor(Executors.newCachedThreadPool())
    )

    val currentRelativePath = Paths.get("")
    val root = currentRelativePath.toAbsolutePath.toString
    val s = JFile.separatorChar

    val docs = s.toString + "sdk" + s + "docs" + s + "mdoc"
    val in = root + s.toString + "sdk" + s + "docs" + s + "mdoc"
    //val in = "/home/hmf/IdeaProjects/srllab/out/sdk/mdocTargetDirectory/dest"
    println("in = " + in)

    //val tmp = "/home/hmf/IdeaProjects/srllab/out/sdk/laikaTargetDirectory/dest"
    val tmp = File(System.getProperty("java.io.tmpdir") + docs).path.toAbsolutePath
    val out = tmp.toString
    println("out = " + out)

    tmp.delete(true)
    tmp.createDirectoryIfNotExists(true)

    val level = MessageLevel.Info

    val transformerHTML = Transformer
      .from(Markdown)
      .to(HTML)
      .using(GitHubFlavor)
      .withMessageLevel(level)
      .withRawContent
      .io(blocker)
      .parallel[IO]
      .build

    val res = transformerHTML
      .fromDirectory(in)
      .toDirectory(out)
      .transform
    res.unsafeRunSync()
  }

  def test1(): RenderedTreeRoot[IO] = {

    import laika.io.implicits._
    import scala.concurrent.ExecutionContext
    import java.util.concurrent.Executors
    implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

    val blocker = Blocker.liftExecutionContext(
      ExecutionContext.fromExecutor(Executors.newCachedThreadPool())
    )

    val currentRelativePath = Paths.get("")
    val root = currentRelativePath.toAbsolutePath.toString
    val s = JFile.separatorChar

    val docs = s.toString + "sdk" + s + "docs" + s + "mdoc"
    val in = root + s + "sdk" + s + "docs" + s + "mdoc"
    //val in = "/home/hmf/IdeaProjects/srllab/out/sdk/mdocTargetDirectory/dest"
    println("in = " + in)

    //val tmp = "/home/hmf/IdeaProjects/srllab/out/sdk/laikaTargetDirectory/dest"
    val tmp = File(System.getProperty("java.io.tmpdir") + docs).path.toAbsolutePath
    val out = tmp.toString
    println("out = " + out)

    tmp.delete(true)
    tmp.createDirectoryIfNotExists(true)

    //val level = MessageLevel.Error
    val level = MessageLevel.Info

    val parser = MarkupParser
      .of(Markdown)
      .withRawContent
      .using(GitHubFlavor)
      .io(blocker)
      .parallel[IO]
      .withAlternativeParser(MarkupParser.of(ReStructuredText).withRawContent)
      .build

    val renderer = Renderer
      .of(HTML)
      .withConfig(parser.config)
      .withMessageLevel(level)
      .io(blocker)
      .parallel[IO]
      .build

    val op = parser.fromDirectory(in).parse.flatMap { tree =>
      renderer
        .from(tree.root)
        .copying(tree.staticDocuments)
        .toDirectory(out)
        .render
    }
    op.unsafeRunSync()
  }

  def test3(): Unit = {

    object W {
      import cats.effect.{Blocker, ContextShift, IO}
      import laika.ast.MessageLevel
      import laika.markdown.github.GitHubFlavor
      import laika.io.model.RenderedTreeRoot
      import laika.api.{MarkupParser, Renderer, Transformer}
      import laika.format.{AST, EPUB, HTML, Markdown, PDF, ReStructuredText, XSLFO}

      import laika.io.implicits._
      import scala.concurrent.ExecutionContext
      import java.util.concurrent.Executors
      implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

      private val blocker = Blocker.liftExecutionContext(
        ExecutionContext.fromExecutor(Executors.newCachedThreadPool())
      )

      def transform(in: String, out: String): Unit = {

        val transformerPDF = Transformer
          .from(Markdown)
          .to(PDF)
          .using(GitHubFlavor)
          .withRawContent
          .withMessageLevel(MessageLevel.Info)
          .io(blocker)
          .parallel[IO]
          .build

        val res = transformerPDF
          .fromDirectory(in)
          .toFile(out)
          .transform

        res.unsafeRunSync()
      }
    }

    val currentRelativePath = Paths.get("")
    val root = currentRelativePath.toAbsolutePath.toString
    val s = JFile.separatorChar

    val docs = s.toString + "sdk" + s + "docs" + s + "mdoc"
    val in = root + s + "sdk" + s + "docs" + s + "mdoc"
    //val in = "/home/hmf/IdeaProjects/srllab/out/sdk/mdocTargetDirectory/dest"
    println("in = " + in)

    //val tmp = "/home/hmf/IdeaProjects/srllab/out/sdk/laikaTargetDirectory/dest"
    val tmp = File(System.getProperty("java.io.tmpdir") + docs).path.toAbsolutePath
    val out = tmp.toString
    println("out = " + out)
    val file = File(out + s + "laikatest.pdf").path.toAbsolutePath.toString
    println("file = " + file)

    tmp.delete(true)
    tmp.createDirectoryIfNotExists(true)

    //val level = MessageLevel.Error
    val level = MessageLevel.Info

    W.transform(in, file)
  }

  def main(args: Array[String]): Unit = {
    test1()
    test2()
    test3()
  }
}
