package splotly

import better.files.Dsl._
import better.files._
import splotly.LPlotly.Local

object Mocks {

  val base: File = cwd / "plots"
  val ploltyMockDir: File =  base / "test" / "image" / "mocks"
  val plotlyImageDir: File = base / "test" / "image" / "baselines"

  val file: File = base / "dist" / "plotly.min.js"
  val localScripts: Local = Local(file.path.toAbsolutePath.toString)

  case class TestFiles(fileName: String,
                       baseMockDir: File = ploltyMockDir,
                       baseImageDir: File = plotlyImageDir) {
    val jsonFile: File = baseMockDir / s"$fileName.json"
    val pngFile = s"$fileName.png"
    val htmlFile = s"$fileName.html"
    val pngFileTmp: File = webkit.Utils.temporaryDirFile(pngFile)
    val htmlFileTmp: File = webkit.Utils.temporaryDirFile(htmlFile)
    val goldPng: File =  baseImageDir / pngFile
    val goldJpeg: File =  baseImageDir / pngFile
  }



}
