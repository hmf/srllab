package exp

import java.nio.file.Paths
import java.io.{File => JFile}

import better.files._
import better.files.Dsl.cwd
import webkit.HeadlessWebKit

/**
 * mill sdk.compile
 * mill sdk.run
 * mill sdk.{compile, run}
 * mill --watch sdk.run
 *
 * mill -i sdk.console
 * mill -i sdk.repl
 *
 * mill -i sdk.runMain mdocs.RestartFx
 * mill --watch sdk.runMain mdocs.RestartFx
 */
object RestartFx {
  val cwd = Paths.get(".").toAbsolutePath.normalize().toString
  val property = "java.io.tmpdir"
  val tempDir = System.getProperty(property)
  val tmp = tempDir + JFile.separator

  /*
    https://gist.github.com/bugabinga/ce9e0ae2328ba34133bd  <---------
    https://stackoverflow.com/questions/53038530/how-do-i-kill-the-javafx-platform-thread-without-having-to-call-system-exit
    https://stackoverflow.com/questions/24320014/how-to-call-launch-more-than-once-in-java  <------------------
    https://www.youtube.com/watch?v=1cobvRlRHic
      java.util.concurrent.Phaser
      java.lang.reference.WeakReference
    https://stackoverflow.com/questions/30443322/javafx-launch-application-and-continue
    https://stackoverflow.com/questions/34788026/how-to-restart-a-javafx-application-when-a-button-is-clicked
    https://stackoverflow.com/questions/35858903/how-to-reload-an-application-in-javafx    <------------------
   */

  /*
    https://stackoverflow.com/questions/22436498/how-to-stop-webengine-after-closing-stage-javafx
   */

  def main(args: Array[String]): Unit = {

    HeadlessWebKit.launchBackground(Array[String]())
    HeadlessWebKit.waitStart()
    println("Capture 1 start")
    val image1 = HeadlessWebKit.capture(s"""http://ww.sapo.pt""", tmp / "sapo.png")
    println("Capture 1 done")
    HeadlessWebKit.stop()

    // Make sure JFX has time to stop
    println("Start wait...")
    Thread.sleep(200)
    println("... wait finished")

    // At this point the primary stage has been stopped

    // We cannot initialize the platform more than once
    //HeadlessWebKit.launchBackground(Array[String]())

    // But we can restart the main stage
    println("Restarting")
    HeadlessWebKit.restart()
    println("Restarted")

    // This will wait for a restart
    HeadlessWebKit.waitStart()
    // If we did not wait for a restart, capture would live-lock
    // It would be waiting for a load from an inactive WebView
    println("Capture 2 start")
    val image2 = HeadlessWebKit.capture(s"""http://ww.google.com""", tmp / "google.png")
    println("Capture 2 done")
    HeadlessWebKit.stop()

    // We deactivate the Application queue and stop the Platform daemon thread
    // We cannot restart after this again
    HeadlessWebKit.kill()


  }
}
