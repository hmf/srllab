package webkit

/**
 * This class is usd to start and stop the JavaFX (OpenFX) Platform and
 * WebKit application. It is also used to stop the JavaFX application daemon
 * thread. If this thread is not stopped the tests will block the Mill test
 * task.
 *
 * Used to implement a `utest.runner.Framework`
 *
 * @see https://github.com/lihaoyi/utest#running-code-before-and-after-test-cases
 */
trait JFXPlatform {
  /**
   * Start the JavaFX Platform and WebKit application
   */
  def setup(): Unit = {
    //println("Setting up CustomFramework")
    // https://github.com/lihaoyi/utest#running-code-before-and-after-test-cases
    HeadlessWebKit.launchBackground(Array[String]())
    HeadlessWebKit.waitStart()
  }

  /**
   * Stop the JavaFX Platform and WebKit application
   */
  def teardown(): Unit = {
    //println("Tearing down CustomFramework")
    HeadlessWebKit.stop()
    HeadlessWebKit.kill()
  }

}
