package splotly

case class CSV(pathName: String, splitChar: String = ",", hasHeader: Boolean = true) {

  val ((nrows, ncols), header, rows) = load()
  private val colIdx = (0 until ncols).toArray

  private def split(data: String): Array[String] = {
    data.split(splitChar).map(_.trim)
  }

  private def load(): ((Int, Int), Array[String], Array[Array[String]]) = {
    val bufferedSource = scala.io.Source.fromFile(pathName)
    val data: Iterator[String] = bufferedSource.getLines
    val header = if (hasHeader) split(data.next()).map(_.trim) else Array[String]()
    val zero = List[Array[String]]()
    val (nrows, ncols, lines) = data.foldLeft((0,0,zero))
    { case (acc, line) =>
      val (nLines, nCols, lines) = acc
      val cols = split(line)
      (nLines+1, Math.max(cols.length, nCols), cols::lines)
    }
    bufferedSource.close

    val rows = lines.reverse.toArray
    if (!hasHeader){
      val header = (0 until ncols).map(_.toString).toArray
      ((nrows, ncols), header, rows)
    } else {
      ((nrows, ncols), header, rows)
    }
  }

  def size: (Int, Int) = (nrows, ncols)

  def apply(j:Int): Option[Array[String]] = {
    if ((j > -1) && (j < ncols)) {
      val column = Array.fill(nrows)("")
      rows.indices.foreach(i => column(i) = rows(i)(j))
      Some(column)
    } else {
      None
    }
  }

  private def toJson(a: Array[String]) = a.mkString("[", ",", "]")

  def json(j: Int): Option[String] = apply(j).map(toJson)

  def apply(k:String): Option[Array[String]] = {
    val key = k.trim
    val j = colIdx.zip(header).find(_._2.equals(key))
    j.flatMap(i => apply(i._1))
  }

  def json(k:String): Option[String] = apply(k).map(toJson)
}
