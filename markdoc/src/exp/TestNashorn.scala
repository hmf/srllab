package exp

import java.io.File

/**
 * mill mill.scalalib.GenIdea/idea
 *
 * mill mdocs.compile
 * mill mdocs.run
 * mill mdocs.{compile, run}
 * mill --watch mdocs.run
 *
 * mill -i mdocs.console
 * mill -i mdocs.repl
 *
 * mill -i sdk.runMain mdocs.TestNashorn
 * mill --watch sdk.runMain mdocs.TestNashorn
 *
 * This is a test to check if the ECMAScript 6 is running correctly.
 * We need to have support for version 6 because we will execute the
 * Plotly JS library in the Nashorn JS engine. This library expects ES6.
 *
 * Other Java JS engines exists but none are active.
 * IMPORTANT: Nashorn seems have been deprecated for the JDK above version 11.
 * Suggestions is that GraalVM be used for the next version.
 *
 * @see https://en.wikipedia.org/wiki/List_of_ECMAScript_engines
 *      https://github.com/graalvm/graaljs
 *      https://www.programcreek.com/java-api-examples/?api=jdk.nashorn.api.scripting.NashornScriptEngineFactory
 *      https://stackoverflow.com/questions/46898540/unable-to-execute-es6-on-java-8-with-nashornscriptengine
 *      http://openjdk.java.net/jeps/292
 */
object TestNashorn {

  def main(args: Array[String]): Unit = {
    import java.io.FileReader
    import java.nio.file.Paths

    val currentRelativePath = Paths.get("")
    val root = currentRelativePath.toAbsolutePath.toString
    println(root)
    val s = File.separatorChar
    val jsPath = root + s + "exp" + s + "src" + s + "exp"

    import jdk.nashorn.api.scripting.NashornScriptEngineFactory
    val factory = new NashornScriptEngineFactory
    val engine = factory.getScriptEngine("--language=es6")

    //Javascript function
    engine.eval("function sum(a, b) { return a + b; }")
    System.out.println(engine.eval("sum(1, 2);"))

    //Template strings
    engine.eval("let name = 'Sanaulla'")
    System.out.println(engine.eval("print(`Hello Mr. ${name}`)"))

    //Set
    engine.eval("var s = new Set(); s.add(1).add(2).add(3).add(4).add(5).add(6);")
    System.out.println("Set elements")
    engine.eval("for (let e of s) { print(e); }")

    //Reading Javascript source
    engine.eval(new FileReader(jsPath + s + "embeddable.js"))
    val difference = engine.eval("difference(1, 2);").asInstanceOf[Int]
    System.out.println("Difference between 1, 2 is: " + difference)

    engine.eval("const x = 44; print(x);")
  }
}
