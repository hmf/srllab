package splotly

import java.io

import sys.process._
import java.net.URL
import java.io.{File => JFile}
import java.nio.charset.Charset

import ujson._
import os._
import better.files._
import File._
import better.files.Dsl._
import org.scalafmt.interfaces.Scalafmt
import splotly.Generator.CodeSnippet

import scala.io.Codec
import scala.util.matching.Regex


/**
 *
 * This class is used to help update the Plotly.js wrapper as follows:
 * - Download the latest version of Plotly.js archive
 * - Uncompress contents to a temporary directory
 * - Copy the JS scripts to the project module for use in the HTML rendering
 * - Copy the test files to the project test module (mocks and images)
 * - Copy the JSON schema to the project module for use in the HTML rendering
 * - Generates the Scala wrappers using JSON schema
 *
 * ./mill mill.scalalib.GenIdea/idea
 *
 * ./mill mdocs.compile
 * ./mill mdocs.run
 * ./mill mdocs.run noGraal
 *
 * ./mill mdocs.{compile, run}
 * ./mill --watch mdocs.run
 *
 * ./mill -i mdocs.console
 * ./mill -i mdocs.repl
 *
 * ./mill -i plots.runMain splotly.Updater
 * ./mill --watch mill -i plots.runMain splotly.Updater
 * ./jfxmill.sh -i plots.runMain splotly.Updater
*/
object Updater {

  def showJsonAST(json: ujson.Value): Unit = {
    json match {
      case Null       => println("isNull")
      case Bool(bool) => println(s"Boolean($bool)")
      case Num(value) => println(s"Number($value)")
      case Str(value) => println(s"String($value)")
      case Arr(value) => println(s"Array")
      case Obj(value) => println("Object")
    }
  }

  def renderJson(r: Boolean, d1: ujson.Value, d2: ujson.Value, show: Boolean, offset: Int): Unit = {
    if (!r && show) println(s"$d1:${d1.render(offset)} <> $d2:${d2.render(offset)}")
  }

  val baseDir: File = cwd / "plots" / "src" / "splotly"
  val genDir: File = baseDir / "gen"
  val config: File = baseDir / "scalafmt.conf"

  val schemaFile = "./plots/dist/plot-schema.json"
  val schema_ : Value = Parser.loadSchema( schemaFile )
  val traces_ : Value = schema_("traces")
  val genDest: String = "./plots/src/splotly/gen"
  //println(traces_.getClass.getCanonicalName)
  //Parser.collectObjects(traces_.obj)

  def generateTrace(traceName: String): Either[Error, Generator.CodeSnippet] = {

    val trace_ = traces_(traceName)
    val ir = Parser.parse(from("traces", from("schema", top)), traceName, trace_)

    if (ir.isLeft) println(ir)
    val out = os.pwd / RelPath(genDest)
    Generator.generate(out, traceName, ir, List())
  }

  def mapToNames(in: Either[Error, CodeSnippet], out: Path): Either[Error, (List[String], String)] = {
    in.map{snippet =>
      val members = snippet.members.map(_.valid)
      (members, out.toNIO.toAbsolutePath.toString)
    }
  }

  def generate(name: String): Either[Error, (List[String], String)] = {

    val data = schema_(name)
    val ir = Parser.parse(from("schema", top), name, data)

    //if (ir.isLeft) println(ir)
    val out = os.pwd / RelPath(genDest)
    val r = Generator.generate(out, name, ir, List())
    mapToNames(r, out)
  }

  def generateLayout(): Either[Error, (List[String], String)] = generate("layout")

  def generateATrace(traceName: String, trace: Value): Either[Error, (List[String], String)] = {
    val current = from("traces", from("schema", top))
    val ir = Parser.parse(current, traceName, trace)

    val out = os.pwd / RelPath(genDest)
    val r = Generator.generate(out, traceName, ir, List())
    mapToNames(r, out)
  }

  def generateTraces(): Either[Error, Iterable[(String, String)]] = {

    def g = generateATrace _

    traces_ match {
      case Obj(v) =>
        val result = v.toMap.map { e =>
          val k = e._1
          val v = e._2
          print(s"trace($k) gen ")
          val r = g(k, v)
          println(s" : ${r.map(_._2)}")
          r.map( e => k -> e._1.head)
        }
        val err = ("", "")
        Generator.checkError(result, err)
      case _ =>
        Left(Error(s"Expected an Obj but got ${showJsonAST(traces_)}"))
    }
  }

  def generateNonTraces(): Either[Error, Iterable[(String, String)]] = {
    schema_ match {
      case Obj(v) =>
        val result = v.toMap.flatMap { e =>
          val k = e._1
          if (!((k == Constants.DEFS) || (k == Constants.TRACES))){
            print(s"non-trace($k) gen ")
            val r = generate(k)
            println(s" : ${r.map(_._2)}")
            r.fold( e => Some(Left(e)), e => Some(Right(k -> e._1.head)) )
          } else
            None
        }
        val err = ("", "")
        Generator.checkError(result, err)
      case _ =>
        Left(Error(s"Expected an Obj but got ${showJsonAST(traces_)}"))
    }
  }


  def formatCode(config: File, file: File): String = {
    val scalafmt = Scalafmt.create(this.getClass.getClassLoader)
    //val config = Paths.get(".scalafmt.conf")
    //val file = Paths.get("Main.scala")
    val code = file.contentAsString

    //scalafmt.format(config.path, file.path, code)
    scalafmt.format(config.path, file.path, code)
  }

  def formatCodeAndOverwrite(config: File, file: File): Unit = {
    val formatted = formatCode(config, file)
    file.overwrite(formatted)
  }

  def formatFiles(config: File, dir: File): Unit = {
    val matches = dir.glob("*.scala").toList
    matches.foreach{ f =>
      formatCodeAndOverwrite(config, f)
      println(s"ScalaFMT: formatted file ${f.toString}")
    }
    println(s"Processed a total of ${matches.length}")
  }

  /**
   * Gets the latest version of the GitHub repo using the RESTFUL API. It does
   * this by using `curl` to download the JSON file and then uses grep with a
   * regular expression to extract the value of the "tag_name":
   *
   * `curl --silent "https://api.github.com/repos/$1/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
   *
   * @see https://gist.github.com/lukechilds/a83e1d7127b78fef38c2914c4ececc3c
   *
   * @param repo name of the GitHub repo
   * @return the tag_name if found otherwise an empty string
   */
  def getLatestVersionViaLinuxOS(repo: String) : String = {
     val command =
       s"""
         |curl --silent "https://api.github.com/repos/$repo/releases/latest" | grep -Po '"tag_name": "\\K.*?(?=")'
         |""".stripMargin
    val curl = os.proc("curl", "--silent", s"https://api.github.com/repos/$repo/releases/latest").spawn(stderr = os.Inherit)
    val grep = os.proc("grep", "-Po", """"tag_name": "\K(.*)(?=")""".stripMargin).spawn(stdin = curl.stdout)
    grep.stdout.trim
  }

  /**
   * Get Plotly.js version tag from the GitHub.
   *
   * @return Plotly's version tag or empty string
   */
  def getLatestPlotlyVersionTagViaLinuxOS: String = {
    getLatestVersionViaLinuxOS("plotly/plotly.js")
  }

  // https://stackoverflow.com/questions/3050907/scala-capture-group-using-regex
  // Note that this regular expression differs from the original because the <=
  // shorthand does not exist. In addition to this we match the JSON value's first
  // double quotes non-greedily (lazy) - second ? in the <= group.
  val regExVersion: Regex = """(?<="tag_name": ?").*?(?=")""".r

  val test1 =
    """
      |{
      |  "url": "https://api.github.com/repos/plotly/plotly.js/releases/24147429",
      |  "assets_url": "https://api.github.com/repos/plotly/plotly.js/releases/24147429/assets",
      |  "upload_url": "https://uploads.github.com/repos/plotly/plotly.js/releases/24147429/assets{?name,label}",
      |  "html_url": "https://github.com/plotly/plotly.js/releases/tag/v1.52.3",
      |  "id": 24147429,
      |  "node_id": "MDc6UmVsZWFzZTI0MTQ3NDI5",
      |  "tag_name": "v1.52.3",
      |  "target_commitish": "master",
      |  "name": "v1.52.3",
      |  "draft": false,
      |  "author": {
      |    "login": "archmoj",
      |    "id": 33888540,
      |    "node_id": "MDQ6VXNlcjMzODg4NTQw",
      |    "avatar_url": "https://avatars1.githubusercontent.com/u/33888540?v=4",
      |    "gravatar_id": "",
      |    "url": "https://api.github.com/users/archmoj",
      |    "html_url": "https://github.com/archmoj",
      |    "followers_url": "https://api.github.com/users/archmoj/followers",
      |    "following_url": "https://api.github.com/users/archmoj/following{/other_user}",
      |    "gists_url": "https://api.github.com/users/archmoj/gists{/gist_id}",
      |    "starred_url": "https://api.github.com/users/archmoj/starred{/owner}{/repo}",
      |    "subscriptions_url": "https://api.github.com/users/archmoj/subscriptions",
      |    "organizations_url": "https://api.github.com/users/archmoj/orgs",
      |    "repos_url": "https://api.github.com/users/archmoj/repos",
      |    "events_url": "https://api.github.com/users/archmoj/events{/privacy}",
      |    "received_events_url": "https://api.github.com/users/archmoj/received_events",
      |    "type": "User",
      |    "site_admin": false
      |  },
      |  "prerelease": false,
      |  "created_at": "2020-03-02T21:22:22Z",
      |  "published_at": "2020-03-02T21:27:52Z",
      |  "assets": [
      |
      |  ],
      |  "tarball_url": "https://api.github.com/repos/plotly/plotly.js/tarball/v1.52.3",
      |  "zipball_url": "https://api.github.com/repos/plotly/plotly.js/zipball/v1.52.3",
      |  "body": "## Fixed\r\n- Make identical bundles on different nodes [#4601]\r\n- Fix (regression introduced in 1.52.1) and improve interactive display of narrow points of `bar`-like traces [#4568]\r\n- Ensure text fits inside `sunburst` sectors with zero values [#4580]\r\n- Reset `splom` selectBatch and unselectBatch on updates [#4595]\r\n- Retry different mobile/tablet config to render gl3d subplots on various devices & browsers e.g. Brave [#4549]\r\n- Bump `is-mobile` to handle iPad Pro & iPad 7th + iOs v13 + Safari [#4548]\r\n- Fix `orthographic` hover after scroll zoom [#4562]\r\n- Preserve gl3d `scene aspectratio` after `orthographic` scroll zoom [#4578]\r\n- Include gl3d `scene.aspectmode` changes in relayout updates [#4579]\r\n- Apply utf-8 charset in test_dashboard [#4554]"
      |}
      |
      |""".stripMargin

  val test2 = """{"url":"https://api.github.com/repos/plotly/plotly.js/releases/24147429","assets_url":"https://api.github.com/repos/plotly/plotly.js/releases/24147429/assets","upload_url":"https://uploads.github.com/repos/plotly/plotly.js/releases/24147429/assets{?name,label}","html_url":"https://github.com/plotly/plotly.js/releases/tag/v1.52.3","id":24147429,"node_id":"MDc6UmVsZWFzZTI0MTQ3NDI5","tag_name":"v1.52.3","target_commitish":"master","name":"v1.52.3","draft":false,"author":{"login":"archmoj","id":33888540,"node_id":"MDQ6VXNlcjMzODg4NTQw","avatar_url":"https://avatars1.githubusercontent.com/u/33888540?v=4","gravatar_id":"","url":"https://api.github.com/users/archmoj","html_url":"https://github.com/archmoj","followers_url":"https://api.github.com/users/archmoj/followers","following_url":"https://api.github.com/users/archmoj/following{/other_user}","gists_url":"https://api.github.com/users/archmoj/gists{/gist_id}","starred_url":"https://api.github.com/users/archmoj/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/archmoj/subscriptions","organizations_url":"https://api.github.com/users/archmoj/orgs","repos_url":"https://api.github.com/users/archmoj/repos","events_url":"https://api.github.com/users/archmoj/events{/privacy}","received_events_url":"https://api.github.com/users/archmoj/received_events","type":"User","site_admin":false},"prerelease":false,"created_at":"2020-03-02T21:22:22Z","published_at":"2020-03-02T21:27:52Z","assets":[],"tarball_url":"https://api.github.com/repos/plotly/plotly.js/tarball/v1.52.3","zipball_url":"https://api.github.com/repos/plotly/plotly.js/zipball/v1.52.3","body":"## Fixed\r\n- Make identical bundles on different nodes [#4601]\r\n- Fix (regression introduced in 1.52.1) and improve interactive display of narrow points of `bar`-like traces [#4568]\r\n- Ensure text fits inside `sunburst` sectors with zero values [#4580]\r\n- Reset `splom` selectBatch and unselectBatch on updates [#4595]\r\n- Retry different mobile/tablet config to render gl3d subplots on various devices & browsers e.g. Brave [#4549]\r\n- Bump `is-mobile` to handle iPad Pro & iPad 7th + iOs v13 + Safari [#4548]\r\n- Fix `orthographic` hover after scroll zoom [#4562]\r\n- Preserve gl3d `scene aspectratio` after `orthographic` scroll zoom [#4578]\r\n- Include gl3d `scene.aspectmode` changes in relayout updates [#4579]\r\n- Apply utf-8 charset in test_dashboard [#4554]"}"""

  /**
   * For debugginf regexp.
   *
   * @param test test string
   */
  def testRegEx(test: String): Unit = {
    val regExVersion: Regex = """(?<="tag_name": ?").*?(?=")""".r
    val r = regExVersion.findFirstMatchIn(test)
    val g: Option[String] = r.map(m => m.group(0))
    println(g)
  }

  private def getVersionData(repo: String): String = {
    import scala.io.Source
    val url = s"https://api.github.com/repos/$repo/releases/latest"
    val html = Source.fromURL(url) //(Codec.ISO8859.name)
    html.mkString
  }

  // https://developer.github.com/v3/rate_limit
  // curl -H "Authorization: token OAUTH-TOKEN" -X GET https://api.github.com/rate_limit
  // https://developer.github.com/v3/#rate-limiting
  /**
   * Gets the latest version of the GitHub repo using the RESTFUL API. It does
   * this by using Scala `io` to download the JSON file and then uses grep with a
   * regular expression to extract the value of the "tag_name".
   *
   * @see [getLatestVersionViaLinuxOS]
   * @param repo name of the GitHub repo
   * @return the tag_name match if found otherwise None
   */
  def getLatestVersion(repo: String): Either[String, Regex.Match] = {
    try {
      val s = getVersionData(repo)
      val m = regExVersion.findFirstMatchIn(s)
      m.toRight("getLatestVersion: No match found")
    } catch {
      case e:Throwable => Left(e.getMessage)
    }
  }

  def getLatestVersionJSON(repo: String): Either[String, Value] = {
    try {
      val data = getVersionData(repo)
      val json = ujson.read(data)
      val tag = json("tag_name")
      Right(tag)
    } catch {
      case e:Throwable => Left(e.getMessage)
    }
  }

  /**
   * Get Plotly.js version tag from the GitHub.
   *
   * @return Plotly's version tag or None
   */
  def getLatestPlotlyVersionTag: Either[String, String] = {
    val r = getLatestVersion("plotly/plotly.js")
    r.map(m => m.group(0))
  }

  def getLatestPlotlyVersionJSON: Either[String, String] = {
    val r = getLatestVersionJSON("plotly/plotly.js")
    try {
      r.fold(
        s => Left(s),
        tag => tag.strOpt.toRight(s"getLatestPlotlyVersionJSON: expected a string but got $tag"))
    } catch {
      case e: Throwable => Left(s"getLatestPlotlyVersionJSON: tag name not found: $e")
    }
  }

  def downloadToFile(url: String, file: String): Either[String, String] = {
    import scala.language.postfixOps
    val out = webkit.Utils.temporaryDirFile(file).toJava
    try {
      (new URL(url) #> out).!!
      Right(out.toPath.toString)
    } catch {
      case e: Throwable => Left(e.getMessage)
    }
  }

  // https://github.com/plotly/plotly.js/archive/v1.52.3.zip
  def downloadPlotly(base: String, version: String): Either[String, String] = {
    val fileName = version + ".zip"
    val url = base + "/" + fileName
    downloadToFile(url, fileName)
  }

  def copyFiles(baseFrom: String, baseTo:File ): List[(File, File)] = {
    List(
      baseFrom / "dist"                         -> baseTo / "plots" / "dist",
      baseFrom / "test" / "image" / "mocks"     -> baseTo / "plots" / "test" / "image" / "mocks",
      baseFrom / "test" / "image" / "baselines" -> baseTo / "plots" / "test" / "image" / "baselines"
    )
  }

  def copyContent(zipFile: File, tmpDestDir: File, baseDirDest: File): Either[String, String] = {
    val unzippedTo = zipFile.unzipTo(destination = tmpDestDir)(Charset.forName("UTF-8"))
    val fileNames = zipFile.newZipInputStream.mapEntries(_.getName)
    if (fileNames.isEmpty){
      Left(s"No data in unzipped archive: $unzippedTo")
    } else {
      val topDir = tmpDestDir / fileNames.next()
      val top = topDir.toString
      val copy = copyFiles(top, baseDirDest)
        copy.foreach{ p =>
        val from = p._1
        val to = p._2
        println(s"${from}.copyTo($to, overwrite = true)")
        from.copyTo(to, overwrite = true)
      }
      Right(s"Copied ${copy.length} files/directories from $unzippedTo")
    }
  }

  /**
   * Determines what is the latest version of Plotly via GitHub's API.
   * Downloads the Zip file to OS's temporary directory. Decompresses
   * the contents to the same directory were the archive is. Then copies
   * the selected contents to the source and test directories.
   *
   * @return Either an error or a success message
   */
  def downloadLatestPlotly(): Either[String, String] = {
    /*
    // Get the latest Plotly version (Linux)
    val plotlyVersionOS = getLatestPlotlyVersionTagViaLinuxOS
    println(s"Latest Plotly.js version (Linux OS) = $plotlyVersionOS")
     */

    /*
    // Get the latest Plotly version (Scala Regex)
    val plotlyVersionRegex = getLatestPlotlyVersionTag
    println(s"Latest Plotly.js version (Scala RegEx) = $plotlyVersionRegex")
     */

    // Get the latest Plotly version (Scala JSON)
    println("Getting latest version tag from GutHub")
    val plotlyVersion = getLatestPlotlyVersionJSON
    println(s"Latest Plotly.js version (Scala JSON) = $plotlyVersion")

    // Download the latest Plotly version
    println("Starting download...")
    val plotlyGithubArchive = "https://github.com/plotly/plotly.js/archive"
    val download = webkit.Utils.time { plotlyVersion.flatMap( v => downloadPlotly(plotlyGithubArchive, v) ) }
    println(s".... downloaded archive = $download")

    // Copy the required files to the proper destination
    println("Starting to copy files...")
    val r = for {
      zip <- download
      archive = File(zip)
      destination = archive.path.getParent
      r <- copyContent(archive, destination, cwd)
    } yield {
       r
    }
    println("... files copied.")
    r
  }

  def generateSourceFromSchema(format: Boolean = true): Unit = {
    println("Generating Non-traces")
    webkit.Utils.time { generateNonTraces() }

    //generateTrace("violin")
    //generateTrace("bar")
    //generateTrace("barpolar")

    println("Generating traces")
    val traces = webkit.Utils.time { generateTraces() }

    println("Generating Traces")
    val out = os.pwd / RelPath(genDest)
    Generator.generateWrapperList(out, "Traces", traces, List())

    println("Formatting generated files")
    //println(config.path.toAbsolutePath.toString)
    //val genFile = genDir / "Violin.scala"
    //val genFile = genDir / "Volume.scala"
    //val genFile = genDir / "Layout.scala"
    //println(genFile.path.toAbsolutePath.toString)
    //val formatted = formatCode(config, genFile)
    //println(formatted)
    //formatCodeAndOverwrite(config, genFile)
    if (format) webkit.Utils.time { formatFiles(config, genDir) }
  }

  /**
    * uPickle, uJson and uPack examples.
    *
    * @see http://www.lihaoyi.com/upickle/
    *
    * @param args command line options
    */
  def main(args: Array[String]): Unit = {
    //Utils.time { generate("layout") }

    // TODO: uncomment
    //downloadLatestPlotly()
    generateSourceFromSchema(format = true)
  }

}
