package splotly

import scala.annotation.tailrec

final case class Error(errs: List[String]) {
  override def toString: String = {
    s"""Error( errs =
       |${errs.mkString(";\n")}
       |)""".stripMargin
  }
}
object Error {
  def apply(err: String): Error = new Error(List(err))
}

sealed trait SchemaPath {
  def contains(name: String): Boolean
}

case class from(name:String, parent: SchemaPath) extends SchemaPath {
  override def toString: String = s"$name<-${parent.toString}"

  def contains(name: String): Boolean = {
    @tailrec
    def contains(next: SchemaPath): Boolean = {
        next match {
          case `top` => false
          case from(nextName, nextNext) =>
            if (name == nextName)
              true
            else
              contains(nextNext)
        }
    }
    if (name == this.name)
      true
    else
      contains(this)
  }
}

case object top extends SchemaPath {
  override def toString: String = "*"
  def contains(name: String): Boolean = false
}

// TODO: add declaration code here for generator
// TODO: add toJason code here
sealed trait OfType
case object NA extends OfType

sealed trait OfPrimitiveOrArray extends OfType
sealed trait OfArray extends OfPrimitiveOrArray
sealed trait OfPrimitive extends OfPrimitiveOrArray

case object NAPrimitive extends OfPrimitive
case class TString(t: String) extends OfPrimitive
case class TBoolean(t: Boolean) extends OfPrimitive
case class TDouble(t: Double) extends OfPrimitive
case class TInt(t: Int) extends OfPrimitive

// TODO: add parent: SchemaPath,
//            name: String,
case class TPrimtivesArray(t: Array[OfPrimitive]) extends OfArray {
  override def toString: String = t.mkString("TPrimtivesArray(", ",", ")")
}
case class TBooleanArray(t: Array[Boolean]) extends OfArray {
  override def toString: String = t.mkString("TBooleanArray(", ",", ")")
}
case class TDoubleArray(t: Array[Double]) extends OfArray {
  override def toString: String = t.mkString("TDoubleArray(", ",", ")")
}
case class TIntArray(t: Array[Int]) extends OfArray {
  override def toString: String = t.mkString("TIntArray(", ",", ")")
}
case class TStringArray(t: Array[String]) extends OfArray {
  override def toString: String = t.mkString("TStringArray(", ",", ")")
}

/**
 * Place holder for a non-standard primitive
 * @param t mapping indicating which attributes will change automatically
 */
case class TImpliedEdits(t:Map[String,Any]) extends OfPrimitive {
  override def toString: String = t.mkString("TImpliedEdits(", ",", ")")
}

/**
 * Place holder for a non-standard primitive
 * @param t
 */
case class TAttrRegexps(t:String) extends OfPrimitive {
  override def toString: String = t.mkString("TAttrRegexps(", ",", ")")
}
case class TArrayAttrRegexpsArray(t: Array[TAttrRegexps]) extends OfArray {
  override def toString: String = t.mkString("TArrayAttrRegexps(", ",", ")")
}

object OfType {

  val CNAPrimitive: OfPrimitive = NAPrimitive
  val CString: TString = TString("\"\"")
  val CBoolean: TBoolean = TBoolean(false)
  val CDouble: TDouble = TDouble(0.0)
  val CInt: TInt = TInt(0)
  val CImpliedEdits: TImpliedEdits = TImpliedEdits(Map())
  val CAttrRegexps: TAttrRegexps = TAttrRegexps("")

  val simplePrimitives: List[OfPrimitive] = List(
    CString,
    CBoolean,
    CDouble,
    CInt
  )

  val allPrimitives: List[OfPrimitive] = List(
    CNAPrimitive,
    CString,
    CBoolean,
    CDouble,
    CInt,
    CImpliedEdits,
    CAttrRegexps
  )

  def stringValue(enumType:OfPrimitive): String = {
    enumType match {
      case NAPrimitive => ""
      case TBoolean(t) => t.toString
      case TDouble(t) => t.toString
      case TInt(t) => t.toString
      case TString(t) => "\"" + t + "\""
      case TImpliedEdits(t) => t.mkString(",")
      case TAttrRegexps(t) => t
    }
  }

}

object Constants {
  val TRACES = "traces"
  val DEFS = "defs"
  val LAYOUTATTRIBUTES = "layoutAttributes"
  val DEPRECATED = "_deprecated"
  val ARRAYREGS = "_arrayAttrRegexps"
  val SWITCH = "*false*"

  val OBJECT = "object"
  val ATTRIBUTES = "attributes"
  val NUMERIC = "numeric" // TODO: see if necessary
  val IMPLIEDEDITS = "impliedEdits"

  val TYPE = "type"
  val ROLE = "role"
  val REGEX = "regex"
  val DFLT = "dflt"
  val VALTYPE = "valType"
  val EDITTYPE = "editType"
  val DESCRIPTION = "description"
  val ANIM = "anim"
  val VALUES = "values"
  val ARRAY_OK = "arrayOk"
  val FLAGS = "flags"
  val EXTRAS = "extras"
  val NO_BLANK = "noBlank"
  val STRICT = "strict"
  val MIN = "min"
  val MAX = "max"
  val ITEMS = "items"

  val STRING = "string"
  val BOOLEAN = "boolean"
  val NUMBER = "number"
  val INTEGER = "integer"

  val DATA_ARRAY = "data_array"
  val INFO_ARRAY = "info_array"
  val ENUMERATED = "enumerated"

  val SUBPLOT_ID = "subplotid"
  val FLAG_LIST = "flaglist"
  val ANGLE = "angle"
  val COLOR = "color"
  val COLOR_SCALE = "colorscale"
  val COLOR_LIST = "colorlist"
  val ANY = "any"

}
