package splotly

import sun.awt.image.OffScreenImageSource

import scala.language.implicitConversions
import ujson.{Arr, Bool, Null, Num, Obj, Readable, Str, Value}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Parser {
  def showTypes(json: Value.Value): String = {
    json match {
      case Null       => "isNull"
      case Bool(bool) => s"Boolean($bool)"
      case Num(value) => s"Number($value)"
      case Str(value) => s"String($value)"
      case Arr(_)     => s"Array"
      case Obj(_)     => "Object"
    }

  }

  def collectObjects(objs: Obj): Unit = {
    objs.value.foreach {
      case (name, value) =>
        println(s"Object($name)")
        println(s"\t${showTypes(value)}")
    }
  }

  /*
   * Basic parsing
   */

  def object_(json: Value.Value): Either[Error, mutable.LinkedHashMap[String, Value]] = {
    json match {
      case Obj(value) => Right(value)
      case e @ _      => Left(Error(s"Expected an object but got $e"))
    }
  }

  def isObject(json: Value.Value): Boolean = object_(json).fold(_ => false, _ => true)

  def isObjectValType(json: Value.Value): Boolean =
    object_(json)
      .map(e => e.contains(Constants.VALTYPE) )
      .fold(_ => false, e => e)

  def isObjectNonValType(json: Value.Value): Boolean =
    object_(json)
      .map(e => !e.contains(Constants.VALTYPE) )
      .fold(_ => false, e => e )

  def array(current: SchemaPath,
            json: Value.Value): Either[Error, ArrayBuffer[Value]] = {
    json match {
      case Arr(value) => Right(value)
      case e @ _      => Left(Error(s"Expected an array but got $e @ $current"))
    }
  }

  def isArray(current: SchemaPath,
              json: Value.Value): Boolean = array(current, json).fold(_ => false, _ => true)


  /**
   * Alternate "[+-]?[0-9][0-9]*".
   * Note that the start (`^`) and end (`$`) are not required wth matches method.
   *
   * @param s string to check if it represents an integer
   * @return true if an integer else false
   */
  def isInteger(s: String): Boolean = s.matches("^\\d+$")

  /**
   * Alternate "[+-]?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?"
   * Note that the start (`^`) and end (`$`) are not required wth matches method.
   *
   * @param s string to check if it represents a real number
   * @return true if a real number else false
   */
  def isDouble(s: String): Boolean =
    s.matches("^[+-]?\\d+(\\.\\d+)?([Ee][+-]?\\d+)?$")

  /**
   *
   * @param s string to check if it represents a JSON string
   * @return true if a real number else false
   */
  def isString(s: String): Boolean = s.trim.matches("^\".*\"$")


  def assignPrimitiveTypes(parent: SchemaPath, json: Value.Value): Either[Error, List[OfPrimitive]] = {
    json match {
      case Str(Constants.ANY) => Right(OfType.simplePrimitives)
      case Str(Constants.NUMERIC) => Right(List(TDouble(0.0)))
      case Str(Constants.NUMBER) => Right(List(TDouble(0.0)))
      case _              => Left(Error(s"Expected a Str(any/numeric) but got (${showTypes(json)}) $json @ $parent"))
    }
  }

  /**
   * The `info_array` `valType` is a vector objects that have a `valType`
   * member. We want to collect these `valType` s. If this a single type like
   * `numeric` we record as such. However if it is an `any` then the single
   * value is mapped to all of the basic types.
   *
   * TODO: return a specialized type (see typedArrays in Generator)
   *
   * @param json
   * @return
   */
  def infoItemsArray(parent: SchemaPath, json: Value.Value):
  Either[Error, Map[Int, List[OfPrimitive]]] = {
    json match {
      case Arr(value) =>
        // Items contains a list of value objects (valtype = "any" or something else not checked)
        val items = value.map { case Obj(e) => e; case _ => mutable.LinkedHashMap[String, Value]() }
        // get the expected valType
        val valtypes: Map[Int, Value] = items
          .indices // get one for each array entry
          // If no valType is found assume numeric
          .map( i => i -> items(i).getOrElse(Constants.VALTYPE, Str(Constants.NUMERIC)) )
          .toMap
        // Map the valType description into one or more primitive types
        val methodTypes = valtypes.map(kv => kv._1 -> assignPrimitiveTypes(from(s"Arr[${kv._1}]", parent), kv._2) )
        checkError(parent, methodTypes)
      case e @ _ =>
        Left(Error(s"Expected an Info Arr but got $e @ $parent"))
    }
  }

  case class TypeCounter(nulls: List[Value] = List(),
                         bools: List[TBoolean] = List(),
                         nums: List[TDouble] = List(),
                         strings: List[TString] = List(),
                         arrays: List[Value] = List(),
                         objs: List[Value] = List()) {

    def add(value: Value.Value): TypeCounter = {
      value match {
        case Null        => copy(nulls = value :: nulls)
        case Bool(value) => copy(bools = TBoolean(value) :: bools)
        case Num(value)  => copy(nums = TDouble(value) :: nums)
        case Str(value)  => copy(strings = TString(value) :: strings)
        case Arr(_)      => copy(arrays = value :: arrays)
        case Obj(_)      => copy(objs = value :: objs)
      }
    }

    def countEachType: List[Int] =
      List(
        nulls.length,
        bools.length,
        nums.length,
        strings.length,
        arrays.length,
        objs.length
      )

    def numberUniqueTypes: Int = {
      val count = countEachType
      count.sum
    }

    def onlyPrimitives: Boolean = nulls.isEmpty && arrays.isEmpty && objs.isEmpty
  }

  // TODO: the arrays should contain OfType not the Scala types
  type OptionBooleanArray = Option[TBooleanArray]
  type OptionDoubleArray = Option[TDoubleArray]
  type OptionStringArray = Option[TStringArray]

  def uniquePrimitiveTypeArrays(current: SchemaPath,
                                jsons: ArrayBuffer[Value])
  : Either[Error,
    (OptionBooleanArray, OptionDoubleArray, OptionStringArray)] = {

    val s1: TypeCounter = jsons.foldLeft(TypeCounter()) {
      case (acc, e) => acc.add(e)
    }
    if (!s1.onlyPrimitives) {
      println("34343434343434")
      println(current)
      println(s1)
      println(s1.onlyPrimitives)
      println(s1.nulls.isEmpty)
      println(s1.arrays.isEmpty)
      println(s1.objs.isEmpty)
      println(s1.objs.mkString(","))
      println(s1.objs.length)
      Left(Error(s"Found non primitive elements in array $s1 @ $current"))
    } else {
      val bools =
        if (s1.bools.nonEmpty) Some(TBooleanArray(s1.bools.map(_.t).toArray))
        else None
      val nums =
        if (s1.nums.nonEmpty) Some(TDoubleArray(s1.nums.map(_.t).toArray))
        else None
      val strs =
        if (s1.strings.nonEmpty) Some(TStringArray(s1.strings.map(_.t).toArray))
        else None
      Right(bools, nums, strs)
    }
  }


  // TODO: return OfArray
  def arrayOfPrimitivesX(current: SchemaPath,
                         json: Value.Value): Either[Error, (OptionBooleanArray, OptionDoubleArray, OptionStringArray)] = {
    val arr = array(current, json)
    val arrays = arr.flatMap { e =>
      uniquePrimitiveTypeArrays(current, e)
    }
    arrays
  }

  // TODO: return OfArray
  def arrayOfPrimitivesTypes(name: String,
                             parent: SchemaPath,
                             json: Value.Value): Either[Error, List[OfArray]] = {
    val arrays = arrayOfPrimitivesX(from(name, parent), json)
    val nonEmptyArrays = arrays.map { arrs =>
      val s1 = if (arrs._1.isDefined) List(arrs._1.get) else List[OfArray]()
      val s2 = if (arrs._2.isDefined) arrs._2.get :: s1 else s1
      val s3 = if (arrs._3.isDefined) arrs._3.get :: s2 else s2
      s3
    }
    nonEmptyArrays
  }

  def primitiveType(json: Value.Value, forceInteger: Boolean = false): Either[Error, OfPrimitive] = {
    json match {
      case Null => Right(NAPrimitive)
      case Bool(bool) => Right(TBoolean(bool))
      case Num(value) => Right(if (forceInteger) TInt(value.toInt) else TDouble(value))
      case Str(value) => Right(TString(value))
      case _ => Left(Error(s"Expected OfPrimitive got $json"))
    }
  }

  def primitiveOrArrayType(current: SchemaPath,
                           json: Value.Value, valType: PrimitiveDefinition): Either[Error, OfPrimitiveOrArray] = {
    json match {
      case Null       => Right(NAPrimitive)
      case Bool(bool) => Right(TBoolean(bool))
      case Num(value) => Right(if (valType.isInstanceOf[PrimitiveInt] ) TInt(value.toInt) else TDouble(value))
      case Str(value) => Right(TString(value))
      case Arr(arr)   =>

        // Get all primitives
        val r1: Either[Error, (OptionBooleanArray, OptionDoubleArray, OptionStringArray)] =
          uniquePrimitiveTypeArrays(current, arr)
        val nt: Either[Error, OfArray] = r1.flatMap{ e =>
          // Count how many different primitive types we have
          val nBool = e._1.fold(0)(_ => 1)
          val nDouble = e._2.fold(0)(_ => 1)
          val nString = e._3.fold(0)(_ => 1)
          val n = nBool + nDouble + nString
          if (n == 1){
            // If only one, return that one
            if (nBool > 0) Right(e._1.get)
            else if (nDouble > 0) Right(e._2.get)
            else Right(e._3.get)
          } else {
            // More than one return a multi-typed array
            val arrT: ArrayBuffer[Either[Error, OfPrimitive]] = arr.map(e => primitiveType(e))
            val (left, right) = arrT.partition { e => e.isLeft }
            val tmp: Either[Error, OfArray] = if (left.nonEmpty) {
              val tmp = left.map( f => f.swap.getOrElse(Error("")).errs )
              val errs = tmp.flatten.toList
              Left(Error(errs))
            } else {
              val tmp: ArrayBuffer[OfPrimitive] = right.map(f => f.getOrElse(NAPrimitive) )
              Right( TPrimtivesArray(tmp.toArray) )
            }
            tmp
          }
        }
        nt
      case Obj(flags) =>
        current match {
          case from(a,_) =>
            // We have an element in the Layout.layoutAttributes that is not
            // a standard primitive. We use a placeholder that may be used later
            // Note that this may be for any valType (boolean, number, etc.)
            if (a.trim.equals(Constants.IMPLIEDEDITS)){
              val nflags = flags.map(e => e._1 -> primitiveType(e._2))
              val rflags = checkError(current, nflags.toMap)
              rflags.map( e => TImpliedEdits(e) )
            } else
            if (flags.isEmpty && valType.isInstanceOf[PrimitiveAny]) {
               Right(NAPrimitive)
            } else
              Left(Error(s"Obj is not a primitive (2) @ $current"))
          case _ => Left(Error(s"Obj is not a primitive (1) @ $current"))
        }

    }
  }

  def string(json: Value.Value): Either[Error, TString] = {
    json match {
      case Str(value) => Right(TString(value))
      case e @ _      => Left(Error(s"Expected a string but got $e"))
    }
  }

  /*
   * Plotly Schema parsing
   */

  def traceElement0[T](
                        name: String,
                        get: Value.Value => Either[Error, T]
                      )(parent: SchemaPath, json: Value.Value): Either[Error, T] = {
    // Get the schema element
    val member: Option[Value] = json.objOpt.flatMap { e =>
      e.get(name)
    }
    val defError = Error(s"$name not found @ $parent $json")
    val default = Left(defError): Either[Error, T]
    // If we found it convert it to the expected type
    val values = member.fold(default)({ f =>
      val v: Either[Error, T] = get(f)
      v.fold(
        e =>
          // Conversion failed, indicate were this happened
          Left(
            Error(
              s"Parsing error of $name @ $parent : " + e.errs
                .mkString(" because of ")
            )
          ),
        f => Right(f)
      )
    })
    values
  }

  def string(name: String,
             parent: SchemaPath,
             json: Value.Value): Either[Error, (String, TString)] = {
    traceElement0(name, j => string(j))(parent, json).map(name -> _)
  }

  def defAttributeError[A](parent: SchemaPath, name: String, json: Value.Value):
  Either[Error, A] =
    Left(Error(s"$name not found @ $parent : $json"))


  type Make = (SchemaPath, String, Map[String, OfPrimitiveOrArray]) => PrimitiveDefinition

  def primitiveOrArrayAttribute(make: Make)
                               (parent: SchemaPath,
                                name: String,
                                json: Value.Value): Either[Error, PrimitiveDefinition] = {
    val current = from(name, parent)
    val maker = make(parent, name, Map())
    json.objOpt.fold(defAttributeError[PrimitiveDefinition](parent, name, json)) { members =>
      val dflt = members.remove(Constants.DFLT)
      val dfltT = dflt.map(d => primitiveOrArrayType(from(Constants.DFLT, current), d , maker) )
      val mapped = members.map{ e =>
        e._1 -> primitiveOrArrayType(from(e._1, current), e._2, maker)
      }.toMap
      val all = dfltT.fold(mapped)( e => mapped + (Constants.DFLT -> e))
      // Make sure json unchanged
      dflt.map(v => json.obj.put(Constants.DFLT, v))
      //val checked = checkError(parent, mapped )
      val checked = checkError(parent, all )
      checked.map(m => make(parent, name, m))
    }
  }

  def arrayAsTuple(parent: SchemaPath, i: Int, colorScale : ArrayBuffer[Value]):
  Either[Error, (TDouble, TString)] = {
    if (colorScale.length < 2)
      Left(Error(s"""Array with color scale elements does not have 2 elements @$parent : ${colorScale.mkString(",")}"""))
    else {
      val first = colorScale(0)
      val second = colorScale(1)
      for {
        num <- first.numOpt.toRight(Error(s"Expected color sale (0) to be a number but got : $first @ $parent"))
        str <- second.strOpt.toRight(Error(s"Expected color sale (1) to be a string but got : $first @ $parent"))
      } yield {
        (TDouble(num), TString(str))
      }
    }
  }

  def colorScale( parent: SchemaPath,
                  json: Value) :
  Either[Error, Map[Int, (TDouble, TString)]] = {
    json match {
      case Arr(value) =>
        // Items contains a list of value objects (valtype = "any" or something else not checked)
        val items = value.map { case Arr(e) => e; case _ => ArrayBuffer[Value]() }
        // get the expected valType
        val valtypes = items
          .indices // get one for each array entry
          // If no valType is found assume numeric
          .map( i => i -> arrayAsTuple( parent, i, items(i) ) )
          .toMap
        val tuples = checkError(parent, valtypes)
        tuples
      case Null =>
        // We have a case in layout.LayoutAttributes that has no defaults set
        Right(Map[Int, (TDouble, TString)]())
      case e @ _ => Left(Error(s"Expected a colorScale Arr in $parent but got $e"))
    }
  }

  def colorScaleAttribute[T](parent: SchemaPath,
                             name: String,
                             json: Value.Value): Either[Error, PrimitiveDefinition] = {
    val current = from(name, parent)
    val next = from(Constants.DFLT, current)
    // The info array items are objects with `valType`, they must be processed separately
    val items: Option[Value] = json.obj.remove(Constants.DFLT)
    val err: Either[Error, Map[Int, (TDouble, TString)]] = Left(Error(s"No ${Constants.DFLT} found in $name @ $parent"))
    val array = items.fold(err)(v => colorScale(next, v))
    // Process the rest of the elements as primitives or arrays
    val primitives = primitiveOrArrayAttribute(PrimitiveColorScale.apply)(parent, name, json)
    // Make sure json unchanged
    items.map(v => json.obj.put(Constants.DFLT, v))
    // Add the items valType information
    for {
      p <- primitives
      valTypes <- array
    } yield {
      PrimitiveColorScale(parent, name, p.member, valTypes)
    }
  }

  def infoArrayAttribute( parent: SchemaPath,
                          name: String,
                          json: Value.Value
                        ): Either[Error, PrimitiveDefinition] = {
    val current = from(name, parent)
    val next = from(Constants.ITEMS, current)
    // The info array items are objects with `valType`, they must be processed separately
    val items: Option[Value] = json.obj.remove(Constants.ITEMS)
    val err: Either[Error, PrimitiveDefinition] = Left(Error(s"InfoArray: No ${Constants.ITEMS} found in $name @ $parent"))
    // Process the rest of the elements as primitives or arrays
    val primitives = primitiveOrArrayAttribute(PrimitiveInfoArray.apply)(parent, name, json)

    val t = items.fold(err) {
      case e: Arr =>
        // Look for an array of Valtypes
        val array = infoItemsArray(next, e)
        // Add the items valType information
        for {
          p <- primitives
          valTypes <- array
        } yield {
          PrimitiveInfoArray(parent, name, p.member, valTypes)
        }
      case e @Obj(_) =>
        // Not an array, most probably a regex enumerate
        // The items of an info array may be described via a regular expression
        val tmp = attributeOfType(parent, name, e)
        tmp.map(enum => PrimitiveRegExEnumerate(enum.parent, enum.name, enum.member) )

      case e@_ => Left(Error(s"InfoArray: ${Constants.ITEMS} in $name @ $parent has unexpected type $e"))
    }
    // Make sure json unchanged
    items.map(v => json.obj.put(Constants.ITEMS, v))
    t
  }

  def getType(parent: SchemaPath, name: String, json: Value.Value):
  Either[ Error,(SchemaPath, String, Value) => Either[Error, PrimitiveDefinition]] = {
    val valType = string(Constants.VALTYPE, parent, json)
    valType.map({ valtype =>
      val t = valtype._2.t.toLowerCase.trim
      t match {

        case Constants.STRING  => primitiveOrArrayAttribute(PrimitiveString)
        case Constants.BOOLEAN => primitiveOrArrayAttribute(PrimitiveBoolean)
        case Constants.NUMBER  => primitiveOrArrayAttribute(PrimitiveNumber)
        case Constants.INTEGER => primitiveOrArrayAttribute(PrimitiveInt)

        case Constants.DATA_ARRAY => primitiveOrArrayAttribute(PrimitiveDataArray)
        case Constants.ENUMERATED => primitiveOrArrayAttribute(PrimitiveEnumerate)
        case Constants.INFO_ARRAY => infoArrayAttribute

        case Constants.SUBPLOT_ID  => primitiveOrArrayAttribute(PrimitiveSubPlotId)
        case Constants.FLAG_LIST   => primitiveOrArrayAttribute(PrimitiveFlagList)
        case Constants.ANGLE       => primitiveOrArrayAttribute(PrimitiveAngle)
        case Constants.COLOR       => primitiveOrArrayAttribute(PrimitiveColor)
        case Constants.COLOR_SCALE => colorScaleAttribute
        case Constants.COLOR_LIST  => primitiveOrArrayAttribute(PrimitiveColorList)

        case Constants.ANY => primitiveOrArrayAttribute(PrimitiveAny)

        case _ => throw new RuntimeException(s"Unknown type: $t")
      }
    })
  }

  def attributeOfType(parent: SchemaPath,
                      name: String,
                      json: Value.Value): Either[Error, PrimitiveDefinition] = {
    for {
      attributeType <- getType(parent, name, json)
      member <- attributeType(parent, name, json)
    } yield {
      member
    }
  }

  def checkError[K, T](
                     parent: SchemaPath,
                     values: Map[K, Either[Error, T]]
                   ): Either[Error, Map[K, T]] = {
    val (left, right) = values.partition({ case (_, v) => v.isLeft })
    if (left.nonEmpty) {
      val tmp = left.map({
        case (a, b) =>
          b.swap.getOrElse(Error(s"Unknown error @ $parent child $a")).errs
      })
      val errs = tmp.flatten.toList
      Left(Error(errs))
    } else {
      val oks = right.map({
        case (a, b) =>
          if (b.isRight)
            a -> b.right.get
          else
            throw new RuntimeException(s"Unexpected Left : $b")
      })
      Right(oks)
    }
  }

  def parseMembers[R](f: (SchemaPath, String, Value.Value) => Either[Error, R])
                     (parent: SchemaPath, e:mutable.Map[String, Value]):
  mutable.Map[String, Either[Error, R]] = {
    e.map{ case (n, json) => n -> f(parent, n, json) }
  }

  def regExpMeta(parent: SchemaPath,
                 name: String,
                 regExpArrays: mutable.Map[String, Value]
                ): Right[Error, List[ValueDefinition]] = {
    val tmp = regExpArrays
      .map{e =>
        val arr = e._2.arr
        val strs = arr.map{e => TAttrRegexps(e.render()) }
        TArrayAttrRegexpsArray(strs.toArray)
      }
    val current = from(name, parent)
    val regexps = tmp.map( r => ValueDefinition(current, Constants.ARRAYREGS, r) )
    if (regexps.nonEmpty) {
      Right(regexps.toList)
    } else {
      Right(List())
    }
  }


  def parseObjectElements( depth: Int,
                           parent: SchemaPath,
                           name: String,
                           elements: mutable.Map[String, Value]
                         ): Either[Error, CompoundDefinition] = {
    val current = from(name, parent)
    val (compounds, primitives_arrays) = elements.partition(e => isObject(e._2) )
    val (arrays, metaValues) = primitives_arrays.partition(e => isArray(current, e._2) )
    val (members, variables) = compounds.partition( e => isObjectNonValType(e._2) )

    val meta = parseMembers(parseValue)(current, metaValues)
    // Add place-holder for non standard array value
    val (valArrays, regExpArrays) = arrays.partition(e => e._1 != Constants.ARRAYREGS)
    val arrs: mutable.Map[String, Either[Error, List[ValueDefinition]]] = parseMembers(parseArrays)(current, valArrays)
    val regExp = regExpMeta(parent, name, regExpArrays)
    // Still an array, so add to rest of arrays
    arrs += Constants.ARRAYREGS -> regExp
    // Ignore what is not used anymore. Those definitions are not complete and cannot be parsed
    val nonDeprecatedMembers = members.filter(_._1 != Constants.DEPRECATED)
    val objs = parseMembers(parseObject(depth+1))(current, nonDeprecatedMembers)
    val vars = parseMembers(attributeOfType)(current, variables)

    val errObjs = checkError(current, objs.toMap)
    val errMeta = checkError(current, meta.toMap)
    val errArrs = checkError(current, arrs.toMap)
    val errVars = checkError(current, vars.toMap)

    /* // Not useful for identifying objects versus classes
    val hasType = elements.contains(Constants.TYPE)
    val hasValType = elements.contains(Constants.VALTYPE)
    val hasRole = elements.contains(Constants.ROLE)
    val hasObjectRole = elements.get(Constants.ROLE).fold(false)({ case Str(Constants.OBJECT) => true; case _ => false})
    println(s"$name hasRole = $hasRole hasType = $hasType hasValType = $hasValType hasObjectRole = $hasObjectRole")
     */

    for {
      objs <- errObjs
      members <- errVars
      meta <- errMeta
      arrs <- errArrs
    } yield {
      // All top level objects that have a type value will be an instantiatable class,
      // all others are (mutable) objects
      val classType: Option[String] = elements.getOrElse(Constants.TYPE, ujson.Null).strOpt
      if (depth > 0)
        ObjectDefinition(depth, parent, name, objs, members, meta, arrs, classType)
      else
        ClassDefinition(depth, parent, name, objs, members, meta, arrs, classType)
    }
  }

  def parseValue(parent: SchemaPath,
                 name: String,
                 json: Value.Value): Either[Error, ValueDefinition] = {
    val current = from(name, parent)
    val any = PrimitiveAny(parent, name)
    primitiveOrArrayType(current, json, any).map(e => ValueDefinition(parent, name, e))
  }

  def parseArrays(parent: SchemaPath,
                  name: String,
                  json: Value.Value): Either[Error, List[ValueDefinition]] = {
    arrayOfPrimitivesTypes(name, parent, json).map { e =>
      e.map(v => ValueDefinition(parent, name, v))
    }
  }

  def parseObject(depth: Int)
                 (
                  parent: SchemaPath,
                  name: String,
                  json: Value.Value): Either[Error, CompoundDefinition] = {
    val objs = json.objOpt
    val defError = Error(s"Jason object not found @ $parent but have $json")
    val default = Left(defError): Either[Error, CompoundDefinition]
    val values = objs.fold(default)(e => parseObjectElements(depth, parent, name, e))
    values
  }

  def parse(parent: SchemaPath,
                  name: String,
                  json: Value.Value): Either[Error, CompoundDefinition] = {
    parseObject(0)(parent, name, json)
  }

  // TODO: can we make this the same as attributes ?
  def traces(parent: SchemaPath, json: Value.Value)
  : Map[String, Either[Error, CompoundDefinition]] = {
    val name = "traces"
    val traces: Option[Value] = json.objOpt.flatMap { e => e.get(name) }
    val default = Map(name -> Left(Error(s"$name not found @ $parent"))): Map[String,Either[Error, CompoundDefinition]]
    val values = traces.fold(default)({
      case Obj(values) =>
        val zero = Map(): Map[String, Either[Error, CompoundDefinition]]
        val current = from(name, parent)
        values.foldLeft(zero) {
          case (acc, value) =>
            val name = value._1
            val jsn = value._2
            acc + (name -> parseObject(0)(current, name, jsn))
        }
      case e @ _ =>
        Map(name -> Left(Error(s"Expected an object but got $e")))
    })
    values
  }


  def loadSchema(pathName: String = "./plots/src/plotly/plot-schema.json"): Value = {
    val f = new java.io.File(pathName)
    val readable = Readable.fromFile(f)
    val json = ujson.read(readable)
    json
  }


}
