package webkit

import utest.{TestSuite, Tests, test}

/**
 * Here we run WebKit to load and take snapshots of several plots.
 * These tests are executed asynchronously. It is important that the tests use
 * uTests's assert method.
 *
 * IMPORTANT NOTE: JavaFX or OpenJFX applications can only be instantiated once.
 * There is a RobotFX. But for now we use a custom uTest [[CustomFramework]] to
 * launch and use a single application.
 *
 * mill mill.scalalib.GenIdea/idea
 *
 * mill -i webkit.test
 * mill -i webkit.test.testLocal
 * mill -i webkit.test webkit.HeadlessWebKitSpec
 * mill -i webkit.test webkit.HeadlessWebKitAsyncSpec.PNG
 *
 * Note: use this version to access the JavaFX libraries
 * ./jfxmill.sh -i webkit.test webkit.HeadlessWebKitAsyncSpec
 *
 * @see http://mundrisoft.com/tech-bytes/compare-images-using-java/
 *      https://stackoverflow.com/questions/8567905/how-to-compare-images-for-similarity-using-java
 */
object HeadlessWebKitAsyncSpec extends TestSuite {

  /*
  // https://github.com/lihaoyi/utest#running-code-before-and-after-test-cases
  HeadlessWebKit.launchBackground(Array[String]())

  // https://github.com/lihaoyi/utest#running-code-before-and-after-test-suites
  // https://github.com/lihaoyi/utest#054
  override def utestAfterAll(): Unit = {
    println("on after all")
    HeadlessWebKit.stop()
  }
  */
  import scala.concurrent.{Future, _}

  // the following is equivalent to `implicit val ec = ExecutionContext.global`
  import ExecutionContext.Implicits.global

  val tests = Tests {

    val testFiles = Mocks.SVGcolor.files
    import testFiles._

    test("PNG") {
      Future( ImageTestUtils.captureAndCompareImage(htmlFile, pngFileTmp, goldPng) )
    }

    test("GIF") {
      Future( ImageTestUtils.captureAndCompareImage(htmlFile, gifFileTmp, goldGif) )
    }

    test("TIFF") {
      Future( ImageTestUtils.captureAndCompareImage(htmlFile, tifFileTmp, goldTif) )
    }

    // Not supported
    test("JPEG") {
      Future( ImageTestUtils.captureAndCompareImage(htmlFile, jpegFileTmp, goldJpeg, false) )
    }

    // Not supported
    test("BMP") {
      Future( ImageTestUtils.captureAndCompareImage(htmlFile, bmpFileTmp, goldBmp, false) )
    }

    // Not supported
    test("WBMP") {
      Future( ImageTestUtils.captureAndCompareImage(htmlFile, wbmpFileTmp, goldWbmp, false) )
    }

  }
}
