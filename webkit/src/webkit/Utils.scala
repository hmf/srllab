package webkit

import java.io.{File => JFile}
import java.nio.file.{FileSystems, Paths}
import java.util.UUID

import better.files._

object Utils {

  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    val delta = t1 - t0
    val seconds = delta / 1.0e9
    val minutes = seconds / 60
    println("Elapsed time: " + delta + " ns")
    println("Elapsed time: " + seconds + " s")
    println("Elapsed time: " + minutes + " m")
    result
  }

  /**
   * This function generates a unique file name.
   *
   * @param tmp - directory wherein the file is created.
   * @return
   */
  def temporaryFilename(tmp: String): String = {
    val uuid = this.synchronized(UUID.randomUUID.toString)
    Paths.get(tmp, uuid).toString
  }

  /**
   * Create a (better) File with a unique name. File path is `tmp`, the file
   * extension is `ext` and the file name is a UUID. We can also include a
   * file name prefix if `tmp` is a path and file name.
   *
   * @param tmp path to file
   * @param ext extension of file name
   * @return a file
   */
  def temporaryFile(tmp: String, ext: String): File = File(temporaryFilename(tmp) + ext)

  /**
   * Places the temporary named file in the system's temporary directory.
   * @param ext end of filename (includes extension)
   * @return
   */
  def temporaryDirFile(ext: String): File = {
    val property = "java.io.tmpdir"
    val sysDir = System.getProperty(property)
    File(temporaryFilename(sysDir) + ext)
  }


  /**
   * Retnr the file extension of file `f`
   * @param f the java File
   * @return the file extension as a string. If none exists returns an empty
   *         string.
   */
  def fileExtension(f: JFile): String = {
    val fileName = f.getAbsolutePath
    val i = fileName.lastIndexOf('.')
    if (i == -1) {
      ""
    } else {
      val separator = FileSystems.getDefault.getSeparator
      val p = fileName.lastIndexOf(separator)
      if ((i > p) && (i < (fileName.length()-1))) {
        fileName.substring(i+1)
      }
      else
        ""
    }
  }

}
